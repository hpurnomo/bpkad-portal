@extends('layout-portal.master')

@section('title', 'Lembaga')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('search.engine')}}">Pencarian</a></li>
					<li class="active">{{$data['lembaga']['0']->nm_lembaga}}</li>
					
				</ol>
			</div>

		</section>
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="heading-block">
						<h2>{{$data['lembaga']['0']->nm_lembaga}}</h2>
						<span></span>
					</div>

					<div class="col_two_third">
						{{-- alamat --}}
						<div class="row">
							<div class="col-md-3">
								<strong>Alamat</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->alamat}}.<b>RT</b> {{$data['lembaga']['0']->noRT}} / <b>RW</b> {{$data['lembaga']['0']->noRW}} 
							</div>
						</div>
						{{-- kota --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Kota</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->nmkota}}
							</div>
						</div>
						{{-- kecamatan --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Kecamatan</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->nmkec}}
							</div>
						</div>
						{{-- kelurahan --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Kelurahan</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->nmkel}}
							</div>
						</div>
						{{-- no telp --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>No Telp</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->no_telepon}}
							</div>
						</div>
						{{-- email --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Email</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->email}}
							</div>
						</div>
						<hr>
						{{-- npwp --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>No NPWP</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->npwp}}
							</div>
						</div>
						{{-- Rekening --}}
						{{-- <div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Rekening</strong>
							</div>
							<div class="col-md-9">
								: {{$data['lembaga']['0']->nama_bank}} {{$data['lembaga']['0']->no_rek}} <b>Atas Nama:</b> {{$data['lembaga']['0']->pemilik_rek}}
							</div>
						</div> --}}
						{{-- Akta Kumham --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Akta Kumham</strong>
							</div>
							<div class="col-md-9">
								: <b>No.</b> <span class="text-color-red">{{$data['lembaga']['0']->no_akta}}</span> /<b>Tgl.</b> {{date('d-M-Y',strtotime($data['lembaga']['0']->tgl_akta))}}
							</div>
						</div>
						{{-- Surat Domisili --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Surat Domisili</strong>
							</div>
							<div class="col-md-9">
								: <b>No.</b> <span class="text-color-red">{{$data['lembaga']['0']->no_surat_domisili}}</span> /<b>Tgl.</b> {{date('d-M-Y',strtotime($data['lembaga']['0']->tgl_surat_domisili))}}
							</div>
						</div>
						{{-- Sertifikat --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>No Sertifikat</strong>
							</div>
							<div class="col-md-9">
								: <b>No.</b> <span class="text-color-red">{{$data['lembaga']['0']->no_sertifikat}}</span> /<b>Tgl.</b> {{date('d-M-Y',strtotime($data['lembaga']['0']->tgl_sertifikat))}}
							</div>
						</div>
						{{-- Izin Operasional --}}
						<div class="row margin-top-15">
							<div class="col-md-3">
								<strong>Izin Operasional</strong>
							</div>
							<div class="col-md-9">
								: <b>No.</b> <span class="text-color-red">{{$data['lembaga']['0']->no_izin_operasional}}</span> /<b>Tgl.</b> {{date('d-M-Y',strtotime($data['lembaga']['0']->tgl_izin_operasional))}}
							</div>
						</div>
					</div>
					<div class="col_one_third col_last">
						{{-- {{ dd($data['lembaga']['0']) }} --}}
						@if($data['lembaga']['0']->fotoLembaga==NULL)
						<img src="{{URL::to('/')}}/assets/images/author/author2.png" alt="author" width="150" class="author-box">
						@else
						<img src="{{url('/')}}/foto_lembaga/{{ $data['lembaga']['0']->fotoLembaga }}" width="150" class="author-box">
						@endif
						
						<h5>Contact Person : </h5>
						<hr>
						<div class="row">
							<div class="col-sm-4"><strong>Ketua</strong></div>
							<div class="col-sm-8">: {{$data['lembaga']['0']->kontak_person}}</div>
						</div>
						<div class="row">
							<div class="col-sm-4"><strong>Bendahara</strong></div>
							<div class="col-sm-8">: {{$data['lembaga']['0']->kontak_person_bendahara}}</div>
						</div>
						<div class="row">
							<div class="col-sm-4"><strong>Sekretaris</strong></div>
							<div class="col-sm-8">: {{$data['lembaga']['0']->kontak_person_sekretaris}}</div>
						</div>
						{{-- <div class="row">
							<div class="col-sm-3"><strong>Phone</strong></div>
							<div class="col-sm-8">: {{$data['lembaga']['0']->no_hp_person}}</div>
						</div> --}}
						{{-- <div class="row">
							<div class="col-sm-3"><strong>Email</strong></div>
							<div class="col-sm-8">: {{$data['lembaga']['0']->email_person}}</div>
						</div> --}}
					</div>
				</div>

				<div class="container clearfix">
					<div class="col_full"> 
						{{-- Lokasi --}}
						<div class="row margin-top-15">
							<div class="col-md-12">
								<div class="fancy-title title-bottom-border">
									<h5>This is a <span>Location Lembaga</span></h5>
								</div>
							</div>
							
							@if(empty($data['lembaga']['0']->longitude)||empty($data['lembaga']['0']->latitude))
							<div class="col-md-12">
								<h4 class="text-color-red">Location Not Available</h4>
							</div>
							@else
							<div class="col-md-12 box-map">
								<div id="google-map" style="height:100%;width:100%;"></div>
							</div>
							@endif
							
							<!-- Google Maps API KEY
							============================================= -->
						    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4B7WgFMAGKhsnmgwOg0CBjgwhSHupw5Q&libraries=places"></script> -->
						    <script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap" async defer></script>
							<!-- <script type="text/javascript" src="{{ url('/') }}/assets/js/jquery.gmap.js"></script> -->
							
							<script type="text/javascript">
							$(function(){
								jQuery('#google-map').gMap({
									controls: {
								           panControl: true,
								           zoomControl: true,
								           mapTypeControl: true,
								           scaleControl: true,
								           streetViewControl: true,
								           overviewMapControl: true
								       },
									scrollwheel: false,
									maptype: 'ROADMAP',
									markers: [
										// {
										// 	address: "Balai Kota Gambir Central Jakarta City 10110, Indonesia",
										// 	html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Hi, Kami <span>Sekretariat BPKD</span></h4><p class="nobottommargin"><strong>Visi Kami : Mewujudkan Penyelenggaraan Pengelolaan Keuangan Daerah yang Transparan,  Akuntabel, Responsif,Partisipatif dan Meningkatkan Pertumbuhan Perekonomian Jakarta</strong></div>',
										// 	icon: {
										// 		image: "{{ url('/') }}/assets/images/icons/map-icon-3.png",
										// 		iconsize: [60, 81],
										// 		iconanchor: [60, 81]
										// 	}
										// }
										{
											latitude: {{ $data['lembaga']['0']->latitude }},
											longitude: {{ $data['lembaga']['0']->longitude }},
											html: '<div style="width: 300px;">{{$data['lembaga']['0']->alamat}} <br/><br/> <center><a href="https://www.google.com/maps/dir/?api=1&destination={{ $data['lembaga']['0']->latitude }},{{ $data['lembaga']['0']->longitude }}" class="btn btn-primary">Go To Directions</a></center></div>',
										}
									],
									icon: {
										image: "{{ url('/') }}/assets/images/icons/map-icon-3.png",
										iconsize: [60, 81],
										iconanchor: [60, 81]
									},
									latitude: {{ $data['lembaga']['0']->latitude }},
									longitude: {{ $data['lembaga']['0']->longitude }},
									zoom: 13,
								});
							})
							</script>

						</div>

						

                    </div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection