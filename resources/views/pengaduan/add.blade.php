@extends('layout-portal.master')

@section('title', 'Pengaduan')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="javascript:void(0);">Pengaduan</a></li>
					<li class="active">Form</li>
					
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					
					<div class="heading-block">
						<h2>Prosedur Pengelolaan Pengaduan Masyarakat</h2>
					</div>
					
					<div class="accordion accordion-bg clearfix">

						<div class="acctitle acctitlec"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Apakah akan dilakukan uji publik dalam proses penganggaran dana hibah dan bansos?</div>
						<div class="acc_content clearfix" style="display: block;">
						<p>Tentu, publik dapat selalu melakukan uji publik dalam proses penganggaran, pencairan, pelaksanaan, dan pertanggungjawaban dana hibah dan bantuan sosial. Jika terdapat penyelewengan, <strong class="text-color-red">Segera LAPORKAN!</strong></p>
						</div>

						<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Bagaimana saya dapat melapor?</div>
						<div class="acc_content clearfix" style="display: none;">
							<p>Laporan dapat dilakukan melalui situs web hibah dan bansos pemerintah daerah, atau secara tertulis. Laporan secara tertulis dapat dilakukan dengan mengirimkan aduan kepada Gubernur:</p>
							<ol>
								<li>Foto copy KTP</li>
								<li>Alamat lengkap dan nomor telepon yang bisa dihubungi</li>
								<li>Bukti-bukti terkait permasalahan yang diadukan</li>
							</ol>
						</div>
					</div>

					<div class="divider divider-short divider-rounded divider-center"><i class="icon-pencil"></i></div>

					<div class="fancy-title title-dotted-border title-center">
						<h2><span>Kirim Laporan Anda</span></h2>
					</div>

					<div class="col_full">
						{{-- alert --}}
						@include('layout-portal/alert')

						<form id="register-form" name="register-form" class="nobottommargin" action="{{route('pengaduan.post')}}" method="post" enctype="multipart/form-data">
							{!! Form::token() !!}
							<div class="col_half">
								<label for="register-form-name">Nama<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="nama" value="{{ old('nama') }}" class="required form-control input-block-level">
							</div>

							<div class="col_half col_last">
								<label for="register-form-email">Email<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="email" value="{{ old('email') }}" class="required form-control input-block-level">
								<small>ex: email@yahoo.com</small>
							</div>

							<div class="clear"></div>

							<div class="col_half">
								<label for="register-form-phone">Phone<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="phone" value="{{ old('phone') }}" class="required form-control input-block-level only-numeric">
								<small>ex: 0812881234xxx</small>
							</div>

							<div class="col_half col_last">
								<label for="register-form-phone">No KTP<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="ktp" value="{{ old('ktp') }}" class="required form-control input-block-level only-numeric">
								<small>ex: 317105xxxxxxxxxx</small>
							</div>

							<div class="clear"></div>

							<div class="col_half">
								<label for="register-form-password">Alamat<strong class="text-color-red">*</strong>:</label>
								<textarea rows="5" name="alamat" class="required form-control">{{ old('alamat') }}</textarea>
							</div>

							<div class="col_half col_last">
								<label for="register-form-password">Isi Laporan<strong class="text-color-red">*</strong>:</label>
								<textarea rows="5" name="content" class="required form-control">{{ old('content') }}</textarea>
							</div>

							<div class="col_full">
								<label for="register-form-phone">Upload KTP(Photo/Scan)<strong class="text-color-red">*</strong>:</label><br/>
								<div class="fileinput fileinput-new" data-provides="fileinput">
								  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
									<div>
										<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="photo"></span>
										<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
									</div>
								</div>
								<em style="display: block;color: red;"> *File yang diizinkan : jpg/png maksimal 1MB</em>
							</div>

							<div class="col_full">
								{!! app('captcha')->display(); !!}
							</div>

							<div class="clear"></div>

							<div class="col_full nobottommargin">
								<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Kirim</button>
							</div>

						</form>
					</div>

					<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>

					<div class="col_full nobottommargin clearfix">

						<div class="col_one_third">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-phone3"></i></a>
								</div>
								<h3>Hotline<span class="subtitle">021 382 3444</span></h3>
							</div>
						</div>

						<div class="col_one_third">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="https://www.facebook.com/ehibahbansos.provinsidkijakarta" target="_blank"><i class="icon-facebook"></i></a>
								</div>
								<h3>Like Us on <span class="subtitle">Facebook</span></h3>
							</div>
						</div>

						<div class="col_one_third col_last">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="https://twitter.com/eHibahBansosDKI" target="_blank"><i class="icon-twitter2"></i></a>
								</div>
								<h3>Follow on<span class="subtitle">Twitter</span></h3>
							</div>
						</div>

					</div>

				</div>

			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection

@if(isset($data['result']))
	@include('layout-portal.message')
@endif

@section('javascript')
	@parent
		<script type="text/javascript">
		$(function(){
			// upload file
			$('.fileinput').fileinput();

			$( "input.only-numeric" ).on( "blur", function() {
			  $( this ).val(function( i, val ) {
			    return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
			  });
			});
		});
		</script>
@endsection