@extends('layout-portal.master')

@section('title', 'Login')

@section('header')
	@parent
	@include('guest.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{ url('/') }}">Beranda</a></li>
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li class="active">Kategori Penerima</li>
				</ol>
			</div>

		</section>

		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">

					<div class="fancy-title title-dotted-border title-center">
                        <h3>Kategori Penerima Yang Tersedia</h3>
                    </div>

                    <div class="pricing bottommargin clearfix">

                        <div class="col-sm-4">

                            <div class="pricing-box">
                                <div class="pricing-title">
                                    <h3>Individu</h3>
                                </div>
                                <div class="pricing-features">
                                    <ul>
                                        <li><strong>Persyaratan</strong>:</li>
                                        <li>a. Fotokopi Kartu Tanda Penduduk (KTP)</li>
                                        <li>b. Surat Pernyataan Tanggung Jawab</li>
                                    </ul>
                                </div>
                                <div class="pricing-action">
                                    <a href="{{ route('signup.individu.form') }}" class="btn btn-danger btn-block btn-lg bgcolor border-color">Daftar Sekarang</a>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-4">

                            <div class="pricing-box best-price">
                                <div class="pricing-title">
                                    <h3>Badan, <br>Lembaga/Organisasi Kemasyarakatan</h3>
                                </div>
                                <div class="pricing-features">
                                    <ul>
                                        <li><strong>Persyaratan</strong>:</li>
                                        <li>a. Fotokopi Kartu Tanda Penduduk (KTP) Ketua/pimpinan badan,lembaga atau organisasi kemasyarakatan</li>
                                        <li>b. Fotokopi Akta Notaris pendirian badan hukum yang telah mendapat pengesahan dari Kementerian yang membidangi hukum atau Keputusan Gubernur tentang pembentukan organisasi/lembaga atau dokumen lain yang dipersamakan</li>
                                        <li>c. Fotokopi Nomor Pokok Wajib Pajak (NPWP)</li>
                                        <li>d. Fotokopi surat keterangan domisili organisasi kemasyarakatan dari Kelurahan setempat</li>
                                        <li>e. Fotokopi izin operasional/tanda daftar lembaga dari instansi yang berwenang</li>
                                        <li>f. Fotokopi sertifikat tanah/bukti kepemilikan tanah atau bukti perjanjian sewa bangunan/gedung atau dokumen lain yang dipersamakan</li>
                                    </ul>
                                </div>
                                <div class="pricing-action">
                                    <a href="{{ route('signup.form') }}" class="btn btn-danger btn-block btn-lg bgcolor border-color">Daftar Sekarang</a>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-4">

                            <div class="pricing-box">
                                <div class="pricing-title">
                                    <h3>Kelompok</h3>
                                </div>
                                <div class="pricing-features">
                                    <ul>
                                        <li><strong>Persyaratan</strong>:</li>
                                        <li>a. Fotokopi Kartu Tanda Penduduk (KTP) Ketua/pimpinan badan,lembaga atau organisasi kemasyarakatan</li>
                                        <li>b. Fotokopi surat keterangan domisili kesekretariatan atau kepengurusan dari Kelurahan setempat</li>
                                        <li>c. Fotokopi sertifikat tanah/bukti kepemilikan tanah atau bukti perjanjian sewa bangunan/gedung atau dokumen lain yang dipersamakan</li>
                                    </ul>
                                </div>
                                <div class="pricing-action">
                                    <a href="{{ route('signup.form') }}" class="btn btn-danger btn-block btn-lg bgcolor border-color">Daftar Sekarang</a>
                                </div>
                            </div>

                        </div>

                    </div>

				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection