@extends('layout-portal.master')

@section('title', 'Beranda')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
	@include($roles.'.sub-menu')
@endsection

@section('content')
<style type="text/css">
	.flex-viewport{
		height: 121px !important;
	}
</style>
	<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<section id="section-work" class="page-section">

					<div class="heading-block center">
						<h2>Prosedur Tahapan</h2>
						<span>Bagi para calon penerima Bansos harus mematuhi prosedur tahapan yang telah ditentukan</span>
					</div>

					<div class="container-fullwidth clearfix">
						<center>
							<img src="assets/images/prosedur/Tahapan_Prosedur_eHibah.png" alt="Image" style="width:70%;">
							<br><br><br>
						</center>
                	</div>

				</section>
				
				<div class="section dark parallax nobottommargin" style="padding: 80px 0;background-image: url('assets/images/footer_lodyas.png');background-repeat: repeat;margin-top:0;" data-stellar-background-ratio="0.3">

					<div class="container clearfix">
						<div class="fancy-title title-dotted-border title-center">
							<h2>REKAP PROSES</h3>
						</div>
						<a href="{{route('penerima.tahun',['tahun'=>\Carbon\Carbon::now('Asia/Jakarta')->format('Y')])}}" style="font-weight:bold;color:white;">
							<div class="col_one_third nobottommargin center" data-animate="bounceIn" data-delay="200">
								<i class="i-plain i-xlarge divcenter nobottommargin icon-time"></i>
								<div class="counter counter-lined"><span data-from="0" data-to="{{$data['penerimaTahunIni']}}" data-refresh-interval="10" data-speed="2000"></span></div>
								<h5>Penerima Tahun {{ \Carbon\Carbon::now('Asia/Jakarta')->format('Y') }}</h5>
							</div>
						</a>

						<a href="{{route('penerima.tahun',['tahun'=>\Carbon\Carbon::now('Asia/Jakarta')->format('Y')+1])}}" style="font-weight:bold;color:white;">
							<div class="col_one_third nobottommargin center" data-animate="bounceIn" data-delay="200">
								<i class="i-plain i-xlarge divcenter nobottommargin icon-time"></i>
								<div class="counter counter-lined"><span data-from="0" data-to="{{$data['penerimaTahunDepan']}}" data-refresh-interval="10" data-speed="2000"></span></div>
								<h5>Proses Tahun {{ \Carbon\Carbon::now('Asia/Jakarta')->format('Y')+1 }}</h5>
							</div>
						</a>

						<a href="{{route('penerima.tahun',['tahun'=>\Carbon\Carbon::now('Asia/Jakarta')->format('Y')+2])}}" style="font-weight:bold;color:white;">
							<div class="col_one_third nobottommargin center col_last" data-animate="bounceIn" data-delay="400">
								<i class="i-plain i-xlarge divcenter nobottommargin icon-time"></i>
								<div class="counter counter-lined"><span data-from="0" data-to="{{$data['penerimaDuaTahunDepan']}}" data-refresh-interval="10" data-speed="2000"></span></div>
								<h5>Pengajuan Tahun {{ \Carbon\Carbon::now('Asia/Jakarta')->format('Y')+2 }}</h5>
							</div>
						</a>

					</div>

				</div>
				{{-- <a href="{{route('login.form')}}" class="button button-full center tright bottommargin-lg">
					<div class="container clearfix">
						Lembaga Anda ingin ajukan bantuan ? <strong>Ajukan Usulan!</strong> <i class="icon-caret-right" style="top:4px;"></i>
					</div>
				</a> --}}

				<section id="section-faq" class="page-section topmargin-lg">

					<div class="heading-block title-center">
						<h2>Tanya Jawab</h2>
					</div>

					<div class="container clearfix">

						<!-- Contact Form
						============================================= -->
						<div class="col_full">
							<div class="accordion accordion-bg clearfix">

								@foreach ($recFaQ as $element)
									<div class="acctitle">
										<i class="acc-closed icon-ok-circle"></i>
										<i class="acc-open icon-remove-circle"></i>
										{!! $element->question !!}
									</div>
									<div class="acc_content clearfix" style="display: none;">
										<p>{!! $element->answer !!}</p>
									</div>
								@endforeach	

							</div>
						</div>

					</div>

				</section>

				<section id="section-testimonials" class="page-section section parallax dark" style="background-image: url('assets/images/footer_lodyas.png');background-repeat: repeat; padding: 50px 0;height:400px;" data-stellar-background-ratio="0.3">

					<div class="container clearfix">

						<div class="col_full nobottommargin">

							<div class="heading-block center">
								<h4>Bagaimana Tanggapan Lembaga?</h4>
								<span>Mereka yang terbantu dengan hibah bansos Pemprov DKI.</span>
							</div>

							<div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding" data-arrows="false">
								<div class="flexslider">
									<div class="slider-wrap">
										@foreach ($recTestimonial as $element)
											<div class="slide" >
												<div class="testi-content">
													<p>{!! $element->brief !!}</p>
													<div class="testi-meta">
														{{ $element->name }}
														<span>{{ $element->position }}</span>
													</div>
												</div>
											</div>
										@endforeach
										
										
									</div>
								</div>
							</div>

						</div>

					</div>

				</section>

				<section id="section-contact" class="page-section">

					<div class="heading-block title-center">
						<h2>Lebih Dekat Dengan Kami</h2>
						<span>Jika terdapat pertanyaan lainnya? Hubungi dengan menggunakan Form Kontak Kami atau Hubungi Via Hotline yang tersedia dibawah ini.</span>
					</div>

					<div class="container clearfix">

						<!-- Contact Form
						============================================= -->
						<div class="col_half">

							<div class="fancy-title title-dotted-border">
								<h3>Kontak Kami</h3>
							</div>
							<h5 style="color:red;">Isilah field-field yang tersedia dibawah ini dengan benar. Agar mempermudah Kami untuk menghubungi Anda kembali. Terima Kasih!</h5>

							<div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

							<form class="nobottommargin" action="{{route('kirim.pesan')}}" method="post">
							{!! Form::token() !!}
{{-- 							<div class="form-process"></div>
 --}}
							<div class="col_one_third">
								<label for="template-contactform-name">Nama <small>*</small></label>
								<input type="text" id="template-contactform-name" name="name" value="" class="sm-form-control" required/>
							</div>

							<div class="col_one_third">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="email" value="" class="email sm-form-control" required/>
							</div>

							<div class="col_one_third col_last">
								<label for="template-contactform-phone">Ponsel</label>
								<input type="text" id="template-contactform-phone" name="phone" value="" class="sm-form-control only-numeric" required/>
							</div>

							<div class="clear"></div>

							<div class="col_two_third">
								<label for="template-contactform-subject">Perihal <small>*</small></label>
								<input type="text" id="template-contactform-subject" name="subject" value="" class="sm-form-control" required/>
							</div>

							<div class="col_one_third col_last">
								<label for="template-contactform-service">Kategori</label>
								<select id="template-contactform-service" name="category" class="sm-form-control" required>
									<option value="">-- Select One --</option>
									<option value="Hibah">Hibah</option>
									<option value="Bansos">Bansos</option>
									<option value="Benkeu">Benkeu</option>
								</select>
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-message">Pesan <small>*</small></label>
								<textarea class="required sm-form-control" id="template-contactform-message" name="message" rows="6" cols="30" required></textarea>
							</div>

							<div class="col_full">
								{!! app('captcha')->display(); !!}
							</div>

							{{-- <div class="col_full hidden">
								<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
							</div> --}}

							<div class="col_full">
								<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Kirim Pesan</button>
							</div>

						</form>

						<script type="text/javascript">

							$("#template-contactform").validate({
								submitHandler: function(form) {
									$('.form-process').fadeIn();
									$(form).ajaxSubmit({
										target: '#contact-form-result',
										success: function() {
											$('.form-process').fadeOut();
											$('#template-contactform').find('.sm-form-control').val('');
											$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
											SEMICOLON.widget.notifications($('#contact-form-result'));
										}
									});
								}
							});

						</script>

						</div><!-- Contact Form End -->

						<!-- Google Map
						============================================= -->
						<div class="col_half col_last">

							<address>
								<strong>Unit Pengelola Data Informasi dan Belanja PPKD Badan Pengelola Keuangan Daerah</strong><br>
								Gedung Balaikota DKI Jakarta Blog G Lantai 14,<br>
								Jl. Medan Merdeka Selatan No. 8-9 Jakarta Pusat.
							</address>

							<abbr title="Phone Number"><strong>Phone :</strong></abbr> 021 382 2539<br>
							<abbr title="Phone Number"><strong>Fax :</strong></abbr> 021 382 2439<br><br>

							<section id="google-map" class="gmap" style="height: 410px;"></section>

							<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4B7WgFMAGKhsnmgwOg0CBjgwhSHupw5Q&libraries=places"></script>
							<script type="text/javascript" src="assets/js/jquery.gmap.js"></script>

							<script type="text/javascript">

								jQuery('#google-map').gMap({

									address: 'Balai Kota Gambir Central Jakarta City 10110, Indonesia',
									maptype: 'ROADMAP',
									zoom: 16,
									markers: [
										{
											address: "Balai Kota Gambir Central Jakarta City 10110, Indonesia",
											html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Hi, Kami <span>Sekretariat BPKD</span></h4><p class="nobottommargin"><strong>Visi Kami : Mewujudkan Penyelenggaraan Pengelolaan Keuangan Daerah yang Transparan,  Akuntabel, Responsif,Partisipatif dan Meningkatkan Pertumbuhan Perekonomian Jakarta</strong></div>',
											icon: {
												image: "assets/images/icons/map-icon-3.png",
												iconsize: [60, 81],
												iconanchor: [60, 81]
											}
										}
									],
									doubleclickzoom: false,
									controls: {
										panControl: true,
										zoomControl: true,
										mapTypeControl: true,
										scaleControl: false,
										streetViewControl: false,
										overviewMapControl: false
									}
								});

							</script>

						</div><!-- Google Map End -->
						<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
						<!-- Contact Info
						============================================= -->
						<div class="col_full nobottommargin clearfix">

							<div class="col_full">
								<div class="feature-box fbox-center fbox-bg fbox-plain">
									<div class="fbox-icon">
										<a href="#"><i class="icon-phone3"></i></a>
									</div>
									<h3>Hotline<span class="subtitle">021 382 2539</span></h3>
								</div>
							</div>

							{{-- <div class="col_one_third">
								<div class="feature-box fbox-center fbox-bg fbox-plain">
									<div class="fbox-icon">
										<a href="https://www.facebook.com/ehibahbansos.provinsidkijakarta" target="_blank"><i class="icon-facebook"></i></a>
									</div>
									<h3>Like Us on <span class="subtitle">Facebook</span></h3>
								</div>
							</div> --}}

							{{-- <div class="col_one_third col_last">
								<div class="feature-box fbox-center fbox-bg fbox-plain">
									<div class="fbox-icon">
										<a href="https://twitter.com/eHibahBansosDKI" target="_blank"><i class="icon-twitter2"></i></a>
									</div>
									<h3>Follow on<span class="subtitle">Twitter</span></h3>
								</div>
							</div> --}}

						</div><!-- Contact Info End -->

					</div>

				</section>

			</div>

		</section><!-- #content end -->
@stop
@if(isset($data['result']))
	@include('layout-portal.message')
@endif
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection
@section('javascript')
	@parent
	<script type="text/javascript">
		$(function(){
			// only numeric
			$( "input.only-numeric" ).on( "blur", function() {
			  $( this ).val(function( i, val ) {
			    return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
			  });
			});

			var showbg = function(){
				$('#bg-fake').remove();
				$('#bg-real').css('display','inline').show('slow');
			};

			setTimeout(showbg,5000);
		});
	</script>
@endsection