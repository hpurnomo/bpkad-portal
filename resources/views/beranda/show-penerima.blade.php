@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('home')}}">Beranda</a></li>
					<li class="active">Penerima Tahun {{ $tahun }}</li>
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_full">
						<div class="promo promo-border promo-center bottommargin">
							<h3>LIST Proposal Tahun {{ $tahun }}</h3>
							<div class="divider divider-short divider-rounded divider-center"><i class="icon-pencil"></i></div>
							<table class="table table-bordered" id="Lembaga">
						        <thead>
						            <tr>
						            	<th>Nomor</th>
						                <th>Tahun</th>
						                <th>Priode</th>
						                <th>Kode SKPD</th>
						                <th>Nama SKPD Koordinator</th>
						                {{-- <th>Kode Lembaga</th> --}}
						                <th>Nama Lembaga</th>
						                <th>Status Kirim</th>
						                <th>Judul Proposal</th>
						                <th>Nominal Pengajuan</th>
						                <th>Status Rekomendasi</th>
						            </tr>
						        </thead>
						        <tbody>
						        	@foreach ($rekap as $element)
						        	<tr>
						        		<td>{{ $element->no_prop }}</td>
						        		<td>{{ $element->tahun }}</td>
						        		<td>{{ $element->priodeKeterangan }}</td>
						        		<td>{{ $element->kd_skpd }}</td>
						        		<td>{{ $element->nm_skpd }}</td>
						        		{{-- <td>{{ $element->kd_lembaga }}</td> --}}
						        		<td>{{ $element->nm_lembaga }}</td>
						        		<td>{{ ($element->status_kirim==1)?'Terkirim':'Belum Terkirim' }}</td>
						        		<td>{{ $element->jdl_prop }}</td>
						        		<td>Rp.{{ number_format($element->nominal_pengajuan,0,',','.') }}</td>
						        		<td>
						        			@if ($element->status_prop == 0)
						        				Menunggu
						        			@elseif ($element->status_prop == 1)
						        				Sudah Direkomendasi
						        			@elseif ($element->status_prop == 2)
						        				Ditolak
						        			@elseif ($element->status_prop == 3)
						        				Dikoreksi
						        			@endif
						        		</td>
						        	</tr>
						        	@endforeach
						        </tbody>
						    </table>
					    </div>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection

@section('javascript')
	@parent
   	<script type="text/javascript">
   		$(function() {
		    // $('#Lembaga').DataTable({
		    //     processing: true,
		    //     serverSide: true,
		    //     ajax: '{!! route('datatables.trx.proposal',['tahun'=> $tahun ]) !!}',
		    //     columns: [
		    //     	{ data: 'no_prop', name: 'no_prop'},
		    //         { data: 'status_input', name: 'status_input'},
		    //         { data: 'tahun', name: 'tahun'},
		    //         { data: 'kd_skpd', name: 'ms.kd_skpd'},
		    //         { data: 'nm_skpd', name: 'nm_skpd'},
		    //         { data: 'kd_lembaga', name: 'kd_lembaga'},
		    //         { data: 'nm_lembaga', name: 'nm_lembaga'},
		    //         { data: 'jdl_prop', name: 'jdl_prop'},
		    //         { data: 'nominal_pengajuan', name: 'tp.nominal_pengajuan'}
		    //     ]
		    // });
		    $('#Lembaga').DataTable();
		});
   	</script>
@endsection