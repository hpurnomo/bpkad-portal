@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.pengaturan.token.manage') }}">Pengaturan Token</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Pengaturan Nomor Token</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Pengaturan Nomor Token</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.pengaturan.token.delete') }}" id="form_sample_1" class="form-horizontal delete" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                <input type="hidden" name="_method" value="DELETE">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{ $recTokenByID->id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3">SKPD
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="token" type="text" data-required="1" class="form-control" value="{{ $recTokenByID->nm_skpd }}" required="true" readonly="true"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nomor Token
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="token" type="text" id="field-token" data-required="1" class="form-control" value="{{ $recTokenByID->token }}" required="true" readonly="true"> 
                                        <span class="help-block"> Nomor Token di <i>generate</i> otomatis untuk menghindari <i>human error</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn grey-salsa btn-outline" onclick="javascript:window.history.back();">Kembali</a>
                                        <button type="submit" class="btn red-mint">Hapus</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>

    @if( !empty(Session::get('status')) )
    <script type="text/javascript">
        $(function(){
                $(window).load(function(){
                    Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                    toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }
                });
        });
    </script>
    @endif

    <script type="text/javascript">
        $(function(){
            $('.select2me').change(function(){
                $('#field-token').val($(this).val());
            });
        });

        $(function(){
		    $(".delete").on("submit", function(){
		        return confirm("Anda yakin ingin menghapus data ini ?");
		    });
        });
    </script>
@stop