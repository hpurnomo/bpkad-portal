@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.testimonial.manage') }}">Testimonial</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah Testimonial Baru</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
            <!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Ubah Testimonial Baru</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        
                        @include('admin.part.alert')

                        <form action="{{ route('ppkd.testimonial.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{ $recTestimonialByID->id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="name" class="form-control" required="">{{ $recTestimonialByID->name }}</textarea>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Position (Jabatan)
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="position" class="form-control" required="">{{ $recTestimonialByID->position }}</textarea>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Brief
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="brief" class="form-control" required="" rows="10">{{ $recTestimonialByID->brief }}</textarea>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Upload Dokumen
                                    </label>
                                    <div class="col-md-4">
                                        <a href="{{ url('/foto_user/avatar') }}/{{ $recTestimonialByID->file_name }}" class="btn btn-circle blue btn-outline" style="margin-bottom: 10px;" download="">{{ $recTestimonialByID->file_name }} <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                        <input type="file" name="file_name" class="form-control">
                                        <i>*Untuk mengubah dokumen yang telah di-upload cukup dengan meng-upload kembali dokumen yang baru.</i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select name="status" class="form-control">
                                            <option value="0" {{ ($recTestimonialByID->status==0)?'selected':'' }}>Disembunyikan</option>
                                            <option value="1" {{ ($recTestimonialByID->status==1)?'selected':'' }}>Ditampilkan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form class="delete" action="{{ route('ppkd.testimonial.delete') }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="id" value="{{ $recTestimonialByID->id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="submit" class="btn btn-danger" value="Hapus">
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop