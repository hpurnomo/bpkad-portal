@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper]) }}">{{ ucwords($fase) }} Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Detil Proposal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>

        {{-- jika proposal telah dirokemdasi maka akan muncul tombol unduh --}}
        @if($recProposalByID->status_prop==1)
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('ppkd.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper]) }}" class="btn grey-salsa btn-outline" style="float: right;margin-left: 10px;">Kembali</a>

                <a href="{{route('ppkd.proposal.verifikasi.berkas.pencairan',['fase'=>$fase,'no_prop'=>$recProposalByID->no_prop])}}" class="btn red" style="float: right;">Verifikasi Berkas Pencairan</a> 
                {{-- <a href="{{route('ppkd.proposal.naskah.perjanjian.nphd.show',['no_prop'=>$recProposalByID->no_prop,'fase'=>$fase])}}" class="btn green-meadow" style="float: right;"><i class="fa fa-cogs" aria-hidden="true"></i> Buat Naskah Perjanjian (NPHD)</a> --}}
                
            </div>
        </div>
        <br>
        @endif

        <div class="portlet light bordered">
            

            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_0" data-toggle="tab">Proposal Pengajuan </a>
                    </li>

                    {{-- Proposal Penebalan Tahap TAPD --}}
                    @if ($recProposalByID->is_penebalan_nominal_tapd==1)
                    <li {{-- class="{{ ($recUserLembagaByNoLembaga->is_approve==0)?'active':'' }}" --}}>
                        <a href="#tab_1_1" data-toggle="tab">Penebalan TAPD </a>
                    </li>
                    @endif
                    
                    {{-- Proposal Penebalan Tahap BANGGAR --}}
                    @if ($recProposalByID->is_penebalan_nominal_banggar==1)
                    <li>
                        <a href="#tab_1_2" data-toggle="tab">Penebalan BANGGAR </a>
                    </li>
                    @endif

                    {{-- Proposal Penebalan Tahap DDN --}}
                    @if ($recProposalByID->is_penebalan_nominal_ddn==1)
                    <li>
                        <a href="#tab_1_3" data-toggle="tab">Penebalan DDN </a>
                    </li>
                    @endif

                    {{-- Proposal Definitif Tahap APBD --}}
                    @if ($recProposalByID->is_penebalan_nominal_apbd==1)
                    <li>
                        <a href="#tab_1_4" data-toggle="tab">Proposal Definitif </a>
                    </li>
                    @endif

                    <li>
                        <a href="#tab_1_5" data-toggle="tab">Proses Anggaran </a>
                    </li>
                </ul>
                <div class="tab-content">
                    {{-- Tahap Pemberian Rekomendasi Oleh SKPD --}}
                    <div class="tab-pane fade active in" id="tab_1_0">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><b>Tahap Pemberian Rekomendasi Oleh SKPD</b></h4>
                                <a href="{{ route('ppkd.cetak.show',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase,'tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
                                <hr>
                                <!-- BEGIN FORM-->
                                <form action="{{ route('ppkd.proposal.update') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
                                {{ csrf_field() }}
                                    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                    <input type="hidden" name="proposalID" value="{{ $proposalID }}">
                                    <div class="form-group form-md-radios">
                                        <label class="col-md-3 control-label" for="form_control_1"> Status Proposal: </label>
                                        <div class="col-md-9">
                                            <div class="md-radio-inline">
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_8" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalByID->status_prop==0)?'checked':'disabled' }}>
                                                    <label for="checkbox1_8">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Menunggu </label>
                                                </div>
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_11" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalByID->status_prop==3)?'checked':'disabled' }}>
                                                    <label for="checkbox1_11">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Dikoreksi </label>
                                                </div>
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_9" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalByID->status_prop==1)?'checked':'disabled' }}>
                                                    <label for="checkbox1_9">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Sudah Direkomendasi </label>
                                                </div>
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_10" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalByID->status_prop==2)?'checked':'disabled' }}>
                                                    <label for="checkbox1_10">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Di Tolak </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                        </label>
                                        <div class="col-md-7">
                                            <hr>
                                        </div>
                                    </div>
                                    {{-- for status diterima --}}
                                    <div id="nominal-rekomendasi" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==2) style="display:none;" @elseif($recProposalByID->status_prop==3) style="display:none;" @endif>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pilih Kategori
                                            </label>
                                            <div class="col-md-7">
                                                <select class="form-control select2me" name="kategoriProposalID" disabled="">
                                                    <option value="">Pilih Kategori</option>
                                                    @foreach($recAllKategoriProposal as $key=>$value)
                                                        <option value="{{ $value->id }}" {{ ($recProposalByID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pilih Program
                                            </label>
                                            <div class="col-md-7">
                                                <select class="form-control select2me" name="no_prg" disabled="">
                                                    <option value="">Pilih Program</option>
                                                    @foreach($recAllProgram as $key=>$value)
                                                        <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->nm_program }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pilih Rekening
                                            </label>
                                            <div class="col-md-7">
                                                <select class="form-control select2me" name="no_rek" disabled="">
                                                    <option value="">Pilih Rekening</option>
                                                    @foreach($recAllRekening as $key=>$value)
                                                        <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->nm_rekening }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">
                                            </label>
                                            <div class="col-md-7">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                                            <div class="col-md-6">
                                                <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral1" value="{{ str_replace(".", ",", $recProposalByID->nominal_rekomendasi) }}" disabled="">
                                                @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                                                <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                                                @else
                                                <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                                            <div class="col-md-4">
                                                <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_rekomendasi }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                                            <div class="col-md-4">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" disabled="" name="tgl_rekomendasi" value="{{ $recProposalByID->tgl_rekomendasi }}">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <!-- /input-group -->
                                                <span class="help-block"> Pilih tanggal </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">
                                            </label>
                                            <div class="col-md-7">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                                            <div class="col-md-4">
                                                <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_survei_lap }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                                            <div class="col-md-4">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalByID->tgl_survei_lap }}" disabled="">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <!-- /input-group -->
                                                <span class="help-block"> Pilih tanggal </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">
                                            </label>
                                            <div class="col-md-7">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                                            <div class="col-md-4">
                                                <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_kelengkapan_administrasi }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                                            <div class="col-md-4">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalByID->tgl_kelengkapan_administrasi }}" disabled="">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <!-- /input-group -->
                                                <span class="help-block"> Pilih tanggal </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">
                                            </label>
                                            <div class="col-md-7">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                                            <div class="col-md-4">
                                                <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_surat_skpd }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                                            <div class="col-md-4">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalByID->tgl_surat_skpd }}" disabled="">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <!-- /input-group -->
                                                <span class="help-block"> Pilih tanggal </span>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- for status ditolak --}}
                                    <div id="alasan" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==1) style="display:none;" @elseif($recProposalByID->status_prop==3) style="display:none;" @endif>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                                            <div class="col-md-5">
                                                <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalByID->alasan_ditolak }}"> 
                                            </div>
                                        </div>
                                    </div>

                                    {{-- dikembalikan --}}
                                    <div id="alasan-dikembalikan" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==1) style="display:none;" @elseif($recProposalByID->status_prop==2) style="display:none;" @endif>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                                            <div class="col-md-5">
                                                <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value=""> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <br>
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-eye"></i>Detil Proposal Usulan</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light">
                                            <a href="{{route('ppkd.proposal.manage',['fase'=>$fase])}}" class="btn btn-default" style="float: right;">Kembali</a>
                                            <div class="tabbable-line">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_1_0_0" data-toggle="tab">1. Data Proposal </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    
                                                    {{-- Data Proposal --}}
                                                    <div class="tab-pane fade active in" id="tab_1_0_0">
                                                        <!-- BEGIN FORM-->
                                                        <form class="form-horizontal" role="form">
                                                            <div class="form-body" style="padding: 0;margin: 0;">
                                                                <h3 class="form-section">Detil Lembaga</h3>
                                                                @inject('metrics', 'BPKAD\Services\LembagaService')
                                                                {{-- {{ dd($metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkota) }} --}}
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-6"><b>Nama Lembaga :</b></label>
                                                                            <div class="col-md-6">
                                                                                <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nm_lembaga }} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-6"><b>Alamat Lembaga :</b></label>
                                                                            <div class="col-md-6">
                                                                                <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->alamat }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-6"><b>RT/RW :</b></label>
                                                                            <div class="col-md-6">
                                                                                <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->RT }}/{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->RW }} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-6"><b>Kelurahan :</b></label>
                                                                            <div class="col-md-6">
                                                                                <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkel }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-6"><b>Kecamatan :</b></label>
                                                                            <div class="col-md-6">
                                                                                <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkec }} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-6"><b>Kota :</b></label>
                                                                            <div class="col-md-6">
                                                                                <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkota }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalByID->tgl_prop)) }} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static"> {{ $recProposalByID->no_proposal }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static"> {{ $recProposalByID->jdl_prop }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                                <h3 class="form-section">Isi Proposal</h3>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static"> {!! $recProposalByID->latar_belakang !!} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static"> {!! $recProposalByID->maksud_tujuan !!} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static"> {!! $recProposalByID->keterangan_prop !!} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                                <h3 class="form-section">Nominal Permohonan</h3>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                                            <div class="col-md-9">
                                                                                <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <h3 class="form-section">Data Pendukung</h3>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                                            <div class="col-md-9">
                                                                                @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawab')
                                                                                @foreach($berkasPernyataanTanggungJawab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                                <p class="form-control-static"> 
                                                                                    <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </p>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                                            <div class="col-md-9">
                                                                                @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusan')
                                                                                @foreach($berkasKepengurusan->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                                <p class="form-control-static"> 
                                                                                    <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </p>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                                            <div class="col-md-9">
                                                                                @inject('fotoProposal','BPKAD\Http\Entities\FotoProposal')
                                                                                @foreach($fotoProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                                <p class="form-control-static"> 
                                                                                    <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </p>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                                            <div class="col-md-9">
                                                                                @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposal')
                                                                                @foreach($berkasProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                                <p class="form-control-static"> 
                                                                                    <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </p>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                                            <div class="col-md-9">
                                                                                @inject('berkasRab','BPKAD\Http\Entities\BerkasRab')
                                                                                @foreach($berkasRab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                                <p class="form-control-static"> 
                                                                                    <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </p>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                                            <div class="col-md-9">
                                                                                @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekening')
                                                                                @foreach($berkasRekening->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                                <p class="form-control-static"> 
                                                                                    <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </p>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                                            <div class="col-md-9">
                                                                                <div class="portlet-body">
                                                                                    <div class="input-group">
                                                                                        <input type="hidden" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                                                        {{-- <span class="input-group-btn">
                                                                                            <button class="btn blue" id="gmap_geocoding_btn">
                                                                                                <i class="fa fa-search"></i>
                                                                                        </span> --}}
                                                                                    </div>
                                                                                    <div id="gmap_basic" class="gmaps"> </div>
                                                                                    <b>Latitude : </b>
                                                                                    <input type="text" class="form-control" name="latitude" id="latVal" value="{{$recProposalByID->latitude}}" >
                                                                                    <b>Longitude : </b>
                                                                                    <input type="text" class="form-control" name="longitude" id="longVal" value="{{$recProposalByID->longitude}}" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/row-->
                                                            </div>
                                                        </form>
                                                        <!-- END FORM-->
                                                    </div>
                                                    
                                                    {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                                    <div class="tab-pane fade" id="tab_1_1_1">
                                                        <div class="col-md-12">
                                                            <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                                            @if($recProposalByID->nominal_pengajuan != $rencana_anggaran)
                                                            <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                                            @else
                                                            <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                                            @endif
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                                        <div class="col-md-8">
                                                                            <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                                                        <div class="col-md-8">
                                                                            <p class="form-control-static">Rp. {{ number_format($rencana_anggaran,2,',','.') }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4"><b>Nilai Rekomendasi RAB (Rp):</b></label>
                                                                        <div class="col-md-8">
                                                                            <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_rekomendasi,2,',','.') }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <br><br><br>
                                                            <!--/row-->
                                                            {{-- <div class="table-toolbar">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="btn-group">
                                                                            <a href="{{ route('member.proposal.create.rencana',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase]) }}" class="btn sbold green"> Buat Rencana Kegiatan
                                                                                <i class="fa fa-plus"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> --}}
                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                                                <thead>
                                                                    <tr>
                                                                        <th> No. </th>
                                                                        <th> Rencana Kegiatan/Rincian </th>
                                                                        <th> Vol/Satuan (jumlah) </th>
                                                                        <th> Harga Satuan (Rp) </th>
                                                                        <th> Anggaran Pengajuan(Rp) </th>
                                                                        <th> Anggaran Rekomendasi(Rp) </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @if(count($recRencanaByNoProp)==0)
                                                                        <tr>
                                                                            <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                                        </tr>
                                                                    @else
                                                                        @inject('metrics', 'BPKAD\Services\RincianService')
                                                                        @foreach ($recRencanaByNoProp as $key=>$value)
                                                                        <tr>
                                                                            <td>{{$key+1}}</td>
                                                                            <td><b>{{$value->rencana}}</b></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                                            <td><b>{{ number_format($value->rencana_anggaran_skpd,2,",",".")}}</b></td>
                                                                        </tr>
                                                                        {!! $metrics->outputByRencanaID($value->id,$value->no_prop,$fase) !!}
                                                                        @endforeach
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_2_0" data-toggle="tab">1. Kelengkapan Administrasi </a>
                                            </li>
                                            <li>
                                                <a href="#tab_2_1" data-toggle="tab">2. Peninjauan Lapangan </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            {{-- Kelengkapan Administrasi --}}
                                            <div class="tab-pane fade active in" id="tab_2_0">
                                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                                <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
                                                <div class="form-group">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-radios">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No</th>
                                                                            <th width="50%"><center>Data Administrasi</center></th>
                                                                            <th><center>Ada</center></th>
                                                                            <th><center>Tidak Ada</center></th>
                                                                            <th><center>Keterangan</center></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>Aktivitas</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_aktivitas1" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_aktivitas) && $recProposalAdministrasiByNoProp->cek_aktivitas==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_aktivitas1">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_aktivitas2" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_aktivitas) && $recProposalAdministrasiByNoProp->cek_aktivitas==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_aktivitas2">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_aktivitas))
                                                                            <textarea name="keterangan_cek_aktivitas" class="form-control" disabled="">{{ $recProposalAdministrasiByNoProp->keterangan_cek_aktivitas }}</textarea>
                                                                            @else
                                                                            <textarea name="keterangan_cek_aktivitas" class="form-control" disabled=""></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2</td>
                                                                            <td>Kepengurusan</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_kepengurusan1" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_kepengurusan) && $recProposalAdministrasiByNoProp->cek_kepengurusan==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_kepengurusan1">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_kepengurusan2" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_kepengurusan) && $recProposalAdministrasiByNoProp->cek_kepengurusan==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_kepengurusan2">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_kepengurusan))
                                                                            <textarea name="keterangan_cek_kepengurusan" class="form-control" disabled="">{{ $recProposalAdministrasiByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                                                            @else
                                                                            <textarea name="keterangan_cek_kepengurusan" class="form-control" disabled=""></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>3</td>
                                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_rab1" name="cek_rab" value="1" class="md-radiobtn" 
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_rab) && $recProposalAdministrasiByNoProp->cek_rab==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_rab1">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_rab2" name="cek_rab" value="2" class="md-radiobtn"
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_rab) && $recProposalAdministrasiByNoProp->cek_rab==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_rab2">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_rab))
                                                                            <textarea name="keterangan_cek_rab" class="form-control" disabled="">{{ $recProposalAdministrasiByNoProp->keterangan_cek_rab }}</textarea>
                                                                            @else
                                                                            <textarea name="keterangan_cek_rab" class="form-control" disabled=""></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>4</td>
                                                                            <td>Waktu Pelaksanaan</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_waktu_pelaksanaan1" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_waktu_pelaksanaan1">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" disabled="" id="cek_waktu_pelaksanaan2" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_waktu_pelaksanaan2">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_waktu_pelaksanaan))
                                                                            <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" disabled="">{{ $recProposalAdministrasiByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                                            @else
                                                                            <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" disabled=""></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <center>
                                                            <input type="checkbox" disabled="" name="is_approve"
                                                                @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
                                                                checked
                                                                @endif
                                                            > 
                                                            <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                                        </center>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                </div>
                                                </form>
                                            </div>
                                            
                                            {{-- Kelengkapan Lapangan --}}
                                            <div class="tab-pane fade" id="tab_2_1">
                                                @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
                                                    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                                <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
                                                <div class="form-group">
                                                    <div class="form-body">
                                                        {!! csrf_field() !!}
                                                        <div class="form-group form-md-radios">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No</th>
                                                                            <th width="50%"><center>Data Lapangan</center></th>
                                                                            <th><center>Sesuai</center></th>
                                                                            <th><center>Tidak Sesuai</center></th>
                                                                            <th><center>Keterangan</center></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>Aktivitas</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_aktivitas11" name="cek_aktivitas" value="1" disabled="" class="md-radiobtn" 
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_aktivitas11">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_aktivitas22" name="cek_aktivitas" value="2" disabled="" class="md-radiobtn"
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_aktivitas22">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_aktivitas))
                                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_aktivitas }}</textarea>
                                                                            @else
                                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2</td>
                                                                            <td>Kepengurusan</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_kepengurusan11" name="cek_kepengurusan" value="1" disabled="" class="md-radiobtn" 
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_kepengurusan11">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_kepengurusan22" name="cek_kepengurusan" value="2" disabled="" class="md-radiobtn"
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_kepengurusan22">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_kepengurusan))
                                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                                                            @else
                                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>3</td>
                                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_rab11" name="cek_rab" value="1" disabled="" class="md-radiobtn" 
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_rab11">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_rab22" name="cek_rab" value="2" disabled="" class="md-radiobtn"
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_rab22">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_rab))
                                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_rab }}</textarea>
                                                                            @else
                                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>4</td>
                                                                            <td>Waktu Pelaksanaan</td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_waktu_pelaksanaan11" name="cek_waktu_pelaksanaan" value="1" disabled="" class="md-radiobtn" 
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==1)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_waktu_pelaksanaan11">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="cek_waktu_pelaksanaan22" name="cek_waktu_pelaksanaan" value="2" disabled="" class="md-radiobtn"
                                                                                    @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==2)
                                                                                    checked
                                                                                    @endif
                                                                                    >
                                                                                    <label for="cek_waktu_pelaksanaan22">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan))
                                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                                            @else
                                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                                            @endif
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <center>
                                                            <input type="checkbox" disabled="" name="is_approve"
                                                                @if(isset($recProposalLapanganByNoProp->is_approve) && $recProposalLapanganByNoProp->is_approve==1)
                                                                checked
                                                                @endif
                                                            > 
                                                            <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                                        </center>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                </div>
                                                </form>
                                                @else
                                                <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Proposal Penebalan Tahap TAPD --}}
                    @if ($recProposalByID->is_penebalan_nominal_tapd==1)
                    <div class="tab-pane fade" id="tab_1_1">
                        @if (empty($recProposalPenebalanTAPDByUUID))
                        <h5 style="color: red;font-weight: bolder;">*Saat ini Anda belum memasuki tahapan ini</h5>
                        @else
<div class="portlet-body form">
    <a href="{{ route('ppkd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>'tapd','tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
    <hr>
    <form action="{{ route('skpd.proposal.updateTAPD') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
    {{ csrf_field() }}
        <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan }}">
        <input type="hidden" name="jdl_prop" value="{{$recProposalPenebalanTAPDByUUID->jdl_prop}}">
        <input type="hidden" name="proposalID" value="{{ $proposalID }}">
        <input type="hidden" name="fase" value="{{$fase}}">
        <input type="hidden" name="nm_lembaga" value="{{$recProposalPenebalanTAPDByUUID->nm_lembaga}}">
        <input type="hidden" name="no_lembaga" value="{{$recProposalPenebalanTAPDByUUID->no_lembaga}}">
        <input type="hidden" name="email_lembaga" value="{{$recProposalPenebalanTAPDByUUID->email_lembaga}}">
        <div class="form-group form-md-radios">
            <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
            <div class="col-md-9">
                <div class="md-radio-inline">
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_81" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalPenebalanTAPDByUUID->status_prop==0)?'checked':'disabled' }}>
                        <label for="checkbox1_81">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Menunggu </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_111" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalPenebalanTAPDByUUID->status_prop==3)?'checked':'disabled' }}>
                        <label for="checkbox1_111">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Dikoreksi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_91" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalPenebalanTAPDByUUID->status_prop==1)?'checked':'disabled' }}>
                        <label for="checkbox1_91">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Sudah Direkomendasi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_101" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalPenebalanTAPDByUUID->status_prop==2)?'checked':'disabled' }}>
                        <label for="checkbox1_101">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Di Tolak </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">
            </label>
            <div class="col-md-7">
                <hr>
            </div>
        </div>
        {{-- for status diterima --}}
        <div id="nominal-rekomendasi1" @if($recProposalPenebalanTAPDByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanTAPDByUUID->status_prop==2) style="display:none;" @elseif($recProposalPenebalanTAPDByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Kategori
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="kategoriProposalID" disabled>
                        <option value="">Pilih Kategori</option>
                        @foreach($recAllKategoriProposal as $key=>$value)
                            <option value="{{ $value->id }}" {{ ($recProposalPenebalanTAPDByUUID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Program
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_prg" disabled>
                        <option value="">Pilih Program</option>
                        @foreach($recAllProgram as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalPenebalanTAPDByUUID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Rekening
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_rek" disabled>
                        <option value="">Pilih Rekening</option>
                        @foreach($recAllRekening as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalPenebalanTAPDByUUID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                <div class="col-md-6">
                    <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral-tapd" value="{{ str_replace(".", ",", $recProposalPenebalanTAPDByUUID->nominal_rekomendasi) }}" disabled>
                    @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                    <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                    @else
                    <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                <div class="col-md-4">
                    <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanTAPDByUUID->no_rekomendasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalPenebalanTAPDByUUID->tgl_rekomendasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                <div class="col-md-4">
                    <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanTAPDByUUID->no_survei_lap }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalPenebalanTAPDByUUID->tgl_survei_lap }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanTAPDByUUID->no_kelengkapan_administrasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalPenebalanTAPDByUUID->tgl_kelengkapan_administrasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                <div class="col-md-4">
                    <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanTAPDByUUID->no_surat_skpd }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalPenebalanTAPDByUUID->tgl_surat_skpd }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
        </div>
        

        {{-- for status ditolak --}}
        <div id="alasan1" @if($recProposalPenebalanTAPDByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanTAPDByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanTAPDByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                <div class="col-md-5">
                    <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalPenebalanTAPDByUUID->alasan_ditolak }}"> 
                </div>
            </div>
        </div>

        <div id="alasan-dikembalikan1" @if($recProposalPenebalanTAPDByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanTAPDByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanTAPDByUUID->status_prop==2) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                <div class="col-md-5">
                    <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalPenebalanTAPDByUUID->alasan_dikembalikan}}"> 
                </div>
            </div>
        </div>
    </form>
</div>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-eye"></i>Detil Proposal TAPD</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_01" data-toggle="tab">1. Data Proposal </a>
                            </li>
                            <li>
                                <a href="#tab_1_11" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                            </li>
                        </ul>
                    <div class="tab-content">
                        
                        {{-- Data Proposal --}}
                        <div class="tab-pane fade active in" id="tab_1_01">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body" style="padding: 0;margin: 0;">
                                    <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalPenebalanTAPDByUUID->tgl_prop)) }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanTAPDByUUID->no_proposal }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanTAPDByUUID->jdl_prop }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Isi Proposal</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanTAPDByUUID->latar_belakang !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanTAPDByUUID->maksud_tujuan !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanTAPDByUUID->keterangan_prop !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Nominal Permohonan</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanTAPDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="form-section">Data Pendukung</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                    @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan)->where('tahap','tapd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                    @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan)->where('tahap','tapd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                <div class="col-md-9">
                                                    @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                    @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan)->where('tahap','tapd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                    @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan)->where('tahap','tapd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                    @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan)->where('tahap','tapd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                    @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanTAPDByUUID->uuid_proposal_pengajuan)->where('tahap','tapd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                <div class="col-md-9">
                                                    <div class="portlet-body">
                                                        <div class="input-group">
                                                            <input type="hidden" class="form-control" id="pac-input-tapd" placeholder="Cari Alamat">
                                                            {{-- <span class="input-group-btn">
                                                                <button class="btn blue" id="gmap_geocoding_btn">
                                                                    <i class="fa fa-search"></i>
                                                            </span> --}}
                                                        </div>
                                                        <div id="gmap_basic_tapd" class="gmaps"> </div>
                                                        <b>Latitude : </b>
                                                        <input type="text" class="form-control" name="latitude" id="latValTAPD" value="{{$recProposalPenebalanTAPDByUUID->latitude}}" readonly="" required="">
                                                        <b>Longitude : </b>
                                                        <input type="text" class="form-control" name="longitude" id="longValTAPD" value="{{$recProposalPenebalanTAPDByUUID->longitude}}" readonly="" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                        
                        {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                        <div class="tab-pane fade" id="tab_1_11">
                            <div class="col-md-12">
                                <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                @if($recProposalPenebalanTAPDByUUID->nominal_pengajuan != $rencana_anggaran_tapd)
                                <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                @else
                                <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanTAPDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_tapd,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <br><br><br>
                                <!--/row-->
                                
                                <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                    <thead>
                                        <tr>
                                            <th> No. </th>
                                            <th> Rencana Kegiatan/Rincian </th>
                                            <th> Vol/Satuan (jumlah) </th>
                                            <th> Harga Satuan (Rp) </th>
                                            <th> Anggaran (Rp) </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($recRencanaByUUIDTAPD)==0)
                                            <tr>
                                                <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                            </tr>
                                        @else
                                            @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                            @foreach ($recRencanaByUUIDTAPD as $key=>$value)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td><b>{{$value->rencana}}</b></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                            </tr>
                                            {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,'tapd','rencana') !!}
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_01" data-toggle="tab">1. Kelengkapan Administrasi </a>
                    </li>
                    <li>
                        <a href="#tab_2_11" data-toggle="tab">2. Peninjauan Lapangan </a>
                    </li>
                </ul>

                <div class="tab-content">
                    {{-- Kelengkapan Administrasi --}}
                    <div class="tab-pane fade active in" id="tab_2_01">
                        <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="tapd">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Administrasi</center></th>
                                                    <th><center>Ada</center></th>
                                                    <th><center>Tidak Ada</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas1111" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas1111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas2111" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas2111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan1111" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan1111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan2111" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan2111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab1111" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab1111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab2111" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab2111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan1111" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan1111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan2111" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan2111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" disabled="" name="is_approve"
                                        @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                        </div>
                        </form>
                    </div>
                    
                    {{-- Kelengkapan Lapangan --}}
                    <div class="tab-pane fade" id="tab_2_11">
                        @if(isset($recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanTAPDByUUIDAndTahap->is_approve==1)
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="tapd">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Lapangan</center></th>
                                                    <th><center>Sesuai</center></th>
                                                    <th><center>Tidak Sesuai</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas11111" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas11111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas22111" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas22111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan11111" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan11111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan22111" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan22111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab11111" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab11111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab22111" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab22111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan11111" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan11111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan22111" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan22111">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanTAPDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" name="is_approve"
                                        @if(isset($recProposalLapanganPenebalanTAPDByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanTAPDByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-11">
                                        <button type="submit" class="btn green-meadow">Kirim</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        @else
                        <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        @endif
                    </div>
                    @endif
                    
                    {{-- Proposal Penebalan Tahap BANGGAR --}}
                    @if ($recProposalByID->is_penebalan_nominal_banggar==1)
                    <div class="tab-pane fade" id="tab_1_2">
                        @if (empty($recProposalPenebalanBANGGARByUUID))
                        <h5 style="color: red;font-weight: bolder;">*Saat ini Anda belum memasuki tahapan ini</h5>
                        @else
<div class="portlet-body form">
    <a href="{{ route('ppkd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>'banggar','tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
    <hr>
    <form action="{{ route('skpd.proposal.updateBANGGAR') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
    {{ csrf_field() }}
        <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan }}">
        <input type="hidden" name="jdl_prop" value="{{$recProposalPenebalanBANGGARByUUID->jdl_prop}}">
        <input type="hidden" name="proposalID" value="{{ $proposalID }}">
        <input type="hidden" name="fase" value="{{$fase}}">
        <input type="hidden" name="nm_lembaga" value="{{$recProposalPenebalanBANGGARByUUID->nm_lembaga}}">
        <input type="hidden" name="no_lembaga" value="{{$recProposalPenebalanBANGGARByUUID->no_lembaga}}">
        <input type="hidden" name="email_lembaga" value="{{$recProposalPenebalanBANGGARByUUID->email_lembaga}}">
        <div class="form-group form-md-radios">
            <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
            <div class="col-md-9">
                <div class="md-radio-inline">
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_82" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==0)?'checked':'disabled' }}>
                        <label for="checkbox1_82">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Menunggu </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_112" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==3)?'checked':'disabled' }}>
                        <label for="checkbox1_112">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Dikoreksi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_92" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==1)?'checked':'disabled' }}>
                        <label for="checkbox1_92">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Sudah Direkomendasi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_102" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==2)?'checked':'disabled' }}>
                        <label for="checkbox1_102">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Di Tolak </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">
            </label>
            <div class="col-md-7">
                <hr>
            </div>
        </div>
        {{-- for status diterima --}}
        <div id="nominal-rekomendasi2" @if($recProposalPenebalanBANGGARByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==2) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Kategori
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="kategoriProposalID" disabled>
                        <option value="">Pilih Kategori</option>
                        @foreach($recAllKategoriProposal as $key=>$value)
                            <option value="{{ $value->id }}" {{ ($recProposalPenebalanBANGGARByUUID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Program
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_prg" disabled>
                        <option value="">Pilih Program</option>
                        @foreach($recAllProgram as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalPenebalanBANGGARByUUID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Rekening
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_rek" disabled>
                        <option value="">Pilih Rekening</option>
                        @foreach($recAllRekening as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalPenebalanBANGGARByUUID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                <div class="col-md-6">
                    <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral-banggar" value="{{ str_replace(".", ",", $recProposalPenebalanBANGGARByUUID->nominal_rekomendasi) }}" disabled>
                    @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                    <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                    @else
                    <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                <div class="col-md-4">
                    <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_rekomendasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_rekomendasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                <div class="col-md-4">
                    <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_survei_lap }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_survei_lap }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_kelengkapan_administrasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_kelengkapan_administrasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                <div class="col-md-4">
                    <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_surat_skpd }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_surat_skpd }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
        </div>
        

        {{-- for status ditolak --}}
        <div id="alasan2" @if($recProposalPenebalanBANGGARByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                <div class="col-md-5">
                    <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalPenebalanBANGGARByUUID->alasan_ditolak }}"> 
                </div>
            </div>
        </div>

        <div id="alasan-dikembalikan2" @if($recProposalPenebalanBANGGARByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==2) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                <div class="col-md-5">
                    <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalPenebalanBANGGARByUUID->alasan_dikembalikan}}"> 
                </div>
            </div>
        </div>
    </form>
</div>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-eye"></i>Detil Proposal BANGGAR</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_012" data-toggle="tab">1. Data Proposal </a>
                            </li>
                            <li>
                                <a href="#tab_1_112" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                            </li>
                        </ul>
                    <div class="tab-content">
                        
                        {{-- Data Proposal --}}
                        <div class="tab-pane fade active in" id="tab_1_012">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body" style="padding: 0;margin: 0;">
                                    <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalPenebalanBANGGARByUUID->tgl_prop)) }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanBANGGARByUUID->no_proposal }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanBANGGARByUUID->jdl_prop }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Isi Proposal</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->latar_belakang !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->maksud_tujuan !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->keterangan_prop !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Nominal Permohonan</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanBANGGARByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="form-section">Data Pendukung</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                    @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                    @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                <div class="col-md-9">
                                                    @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                    @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                    @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                    @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                    @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                <div class="col-md-9">
                                                    <div class="portlet-body">
                                                        <div class="input-group">
                                                            <input type="hidden" class="form-control" id="pac-input-banggar" placeholder="Cari Alamat">
                                                            {{-- <span class="input-group-btn">
                                                                <button class="btn blue" id="gmap_geocoding_btn">
                                                                    <i class="fa fa-search"></i>
                                                            </span> --}}
                                                        </div>
                                                        <div id="gmap_basic_banggar" class="gmaps"> </div>
                                                        <b>Latitude : </b>
                                                        <input type="text" class="form-control" name="latitude" id="latValBANGGAR" value="{{$recProposalPenebalanBANGGARByUUID->latitude}}" readonly="" required="">
                                                        <b>Longitude : </b>
                                                        <input type="text" class="form-control" name="longitude" id="longValBANGGAR" value="{{$recProposalPenebalanBANGGARByUUID->longitude}}" readonly="" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                        
                        {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                        <div class="tab-pane fade" id="tab_1_112">
                            <div class="col-md-12">
                                <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                @if($recProposalPenebalanBANGGARByUUID->nominal_pengajuan != $rencana_anggaran_banggar)
                                <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                @else
                                <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanBANGGARByUUID->nominal_pengajuan,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_banggar,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <br><br><br>
                                <!--/row-->
                                
                                <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                    <thead>
                                        <tr>
                                            <th> No. </th>
                                            <th> Rencana Kegiatan/Rincian </th>
                                            <th> Vol/Satuan (jumlah) </th>
                                            <th> Harga Satuan (Rp) </th>
                                            <th> Anggaran (Rp) </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($recRencanaByUUIDBANGGAR)==0)
                                            <tr>
                                                <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                            </tr>
                                        @else
                                            @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                            @foreach ($recRencanaByUUIDBANGGAR as $key=>$value)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td><b>{{$value->rencana}}</b></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                            </tr>
                                            {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,'banggar','rencana') !!}
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_012" data-toggle="tab">1. Kelengkapan Administrasi </a>
                    </li>
                    <li>
                        <a href="#tab_2_112" data-toggle="tab">2. Peninjauan Lapangan </a>
                    </li>
                </ul>

                <div class="tab-content">
                    {{-- Kelengkapan Administrasi --}}
                    <div class="tab-pane fade active in" id="tab_2_012">
                        <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="banggar">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Administrasi</center></th>
                                                    <th><center>Ada</center></th>
                                                    <th><center>Tidak Ada</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas11112" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas21112" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan11112" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan21112" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab11112" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab21112" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan11112" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan21112" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" disabled="" name="is_approve"
                                        @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                        </div>
                        </form>
                    </div>
                    
                    {{-- Kelengkapan Lapangan --}}
                    <div class="tab-pane fade" id="tab_2_112">
                        @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="banggar">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Lapangan</center></th>
                                                    <th><center>Sesuai</center></th>
                                                    <th><center>Tidak Sesuai</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas111112" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas221112" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan111112" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan221112" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab111112" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab221112" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan111112" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan221112" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" name="is_approve"
                                        @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-11">
                                        <button type="submit" class="btn green-meadow">Kirim</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        @else
                        <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        @endif
                    </div>
                    @endif

                    {{-- Proposal Penebalan Tahap DDN --}}
                    @if ($recProposalByID->is_penebalan_nominal_ddn==1)
                    <div class="tab-pane fade" id="tab_1_3">
                        @if (empty($recProposalPenebalanDDNByUUID))
                        <h5 style="color: red;font-weight: bolder;">*Saat ini Anda belum memasuki tahapan ini</h5>
                        @else
<div class="portlet-body form">
    <a href="{{ route('ppkd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>'ddn','tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
    <hr>
    <form action="{{ route('skpd.proposal.updateDDN') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
    {{ csrf_field() }}
        <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan }}">
        <input type="hidden" name="jdl_prop" value="{{$recProposalPenebalanDDNByUUID->jdl_prop}}">
        <input type="hidden" name="proposalID" value="{{ $proposalID }}">
        <input type="hidden" name="fase" value="{{$fase}}">
        <input type="hidden" name="nm_lembaga" value="{{$recProposalPenebalanDDNByUUID->nm_lembaga}}">
        <input type="hidden" name="no_lembaga" value="{{$recProposalPenebalanDDNByUUID->no_lembaga}}">
        <input type="hidden" name="email_lembaga" value="{{$recProposalPenebalanDDNByUUID->email_lembaga}}">
        <div class="form-group form-md-radios">
            <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
            <div class="col-md-9">
                <div class="md-radio-inline">
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_83" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalPenebalanDDNByUUID->status_prop==0)?'checked':'disabled' }}>
                        <label for="checkbox1_83">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Menunggu </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_113" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalPenebalanDDNByUUID->status_prop==3)?'checked':'disabled' }}>
                        <label for="checkbox1_113">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Dikoreksi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_93" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalPenebalanDDNByUUID->status_prop==1)?'checked':'disabled' }}>
                        <label for="checkbox1_93">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Sudah Direkomendasi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_103" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalPenebalanDDNByUUID->status_prop==2)?'checked':'disabled' }}>
                        <label for="checkbox1_103">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Di Tolak </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">
            </label>
            <div class="col-md-7">
                <hr>
            </div>
        </div>
        {{-- for status diterima --}}
        <div id="nominal-rekomendasi3" @if($recProposalPenebalanDDNByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanDDNByUUID->status_prop==2) style="display:none;" @elseif($recProposalPenebalanDDNByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Kategori
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="kategoriProposalID" disabled>
                        <option value="">Pilih Kategori</option>
                        @foreach($recAllKategoriProposal as $key=>$value)
                            <option value="{{ $value->id }}" {{ ($recProposalPenebalanDDNByUUID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Program
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_prg" disabled>
                        <option value="">Pilih Program</option>
                        @foreach($recAllProgram as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalPenebalanDDNByUUID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Rekening
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_rek" disabled>
                        <option value="">Pilih Rekening</option>
                        @foreach($recAllRekening as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalPenebalanDDNByUUID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                <div class="col-md-6">
                    <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral-ddn" value="{{ str_replace(".", ",", $recProposalPenebalanDDNByUUID->nominal_rekomendasi) }}" disabled>
                    @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                    <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                    @else
                    <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                <div class="col-md-4">
                    <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanDDNByUUID->no_rekomendasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalPenebalanDDNByUUID->tgl_rekomendasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                <div class="col-md-4">
                    <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanDDNByUUID->no_survei_lap }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalPenebalanDDNByUUID->tgl_survei_lap }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanDDNByUUID->no_kelengkapan_administrasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalPenebalanDDNByUUID->tgl_kelengkapan_administrasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                <div class="col-md-4">
                    <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanDDNByUUID->no_surat_skpd }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalPenebalanDDNByUUID->tgl_surat_skpd }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
        </div>
        

        {{-- for status ditolak --}}
        <div id="alasan3" @if($recProposalPenebalanDDNByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanDDNByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanDDNByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                <div class="col-md-5">
                    <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalPenebalanDDNByUUID->alasan_ditolak }}"> 
                </div>
            </div>
        </div>

        <div id="alasan-dikembalikan3" @if($recProposalPenebalanDDNByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanDDNByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanDDNByUUID->status_prop==2) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                <div class="col-md-5">
                    <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalPenebalanDDNByUUID->alasan_dikembalikan}}"> 
                </div>
            </div>
        </div>
    </form>
</div>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-eye"></i>Detil Proposal DDN</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_0123" data-toggle="tab">1. Data Proposal </a>
                            </li>
                            <li>
                                <a href="#tab_1_1123" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                            </li>
                        </ul>
                    <div class="tab-content">
                        
                        {{-- Data Proposal --}}
                        <div class="tab-pane fade active in" id="tab_1_0123">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body" style="padding: 0;margin: 0;">
                                    <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalPenebalanDDNByUUID->tgl_prop)) }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanDDNByUUID->no_proposal }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanDDNByUUID->jdl_prop }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Isi Proposal</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanDDNByUUID->latar_belakang !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanDDNByUUID->maksud_tujuan !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanDDNByUUID->keterangan_prop !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Nominal Permohonan</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanDDNByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="form-section">Data Pendukung</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                    @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                    @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                <div class="col-md-9">
                                                    @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                    @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                    @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                    @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                    @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanDDNByUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                <div class="col-md-9">
                                                    <div class="portlet-body">
                                                        <div class="input-group">
                                                            <input type="hidden" class="form-control" id="pac-input-ddn" placeholder="Cari Alamat">
                                                            {{-- <span class="input-group-btn">
                                                                <button class="btn blue" id="gmap_geocoding_btn">
                                                                    <i class="fa fa-search"></i>
                                                            </span> --}}
                                                        </div>
                                                        <div id="gmap_basic_ddn" class="gmaps"> </div>
                                                        <b>Latitude : </b>
                                                        <input type="text" class="form-control" name="latitude" id="latValDDN" value="{{$recProposalPenebalanDDNByUUID->latitude}}" readonly="" required="">
                                                        <b>Longitude : </b>
                                                        <input type="text" class="form-control" name="longitude" id="longValDDN" value="{{$recProposalPenebalanDDNByUUID->longitude}}" readonly="" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                        
                        {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                        <div class="tab-pane fade" id="tab_1_1123">
                            <div class="col-md-12">
                                <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                @if($recProposalPenebalanDDNByUUID->nominal_pengajuan != $rencana_anggaran_ddn)
                                <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                @else
                                <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanDDNByUUID->nominal_pengajuan,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_ddn,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <br><br><br>
                                <!--/row-->
                                
                                <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                    <thead>
                                        <tr>
                                            <th> No. </th>
                                            <th> Rencana Kegiatan/Rincian </th>
                                            <th> Vol/Satuan (jumlah) </th>
                                            <th> Harga Satuan (Rp) </th>
                                            <th> Anggaran (Rp) </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($recRencanaByUUIDDDN)==0)
                                            <tr>
                                                <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                            </tr>
                                        @else
                                            @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                            @foreach ($recRencanaByUUIDDDN as $key=>$value)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td><b>{{$value->rencana}}</b></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                            </tr>
                                            {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,'ddn','rencana') !!}
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_0123" data-toggle="tab">1. Kelengkapan Administrasi </a>
                    </li>
                    <li>
                        <a href="#tab_2_1123" data-toggle="tab">2. Peninjauan Lapangan </a>
                    </li>
                </ul>

                <div class="tab-content">
                    {{-- Kelengkapan Administrasi --}}
                    <div class="tab-pane fade active in" id="tab_2_0123">
                        <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="ddn">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Administrasi</center></th>
                                                    <th><center>Ada</center></th>
                                                    <th><center>Tidak Ada</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas111123" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas211123" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan111123" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan211123" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab111123" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab211123" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan111123" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan211123" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" disabled="" name="is_approve"
                                        @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                        </div>
                        </form>
                    </div>
                    
                    {{-- Kelengkapan Lapangan --}}
                    <div class="tab-pane fade" id="tab_2_1123">
                        @if(isset($recProposalAdministrasiPenebalanDDNByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanDDNByUUIDAndTahap->is_approve==1)
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="ddn">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Lapangan</center></th>
                                                    <th><center>Sesuai</center></th>
                                                    <th><center>Tidak Sesuai</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas1111123" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas1111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas2211123" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas2211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan1111123" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan1111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan2211123" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan2211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab1111123" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab1111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab2211123" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab2211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan1111123" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan1111123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan2211123" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan2211123">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanDDNByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" name="is_approve"
                                        @if(isset($recProposalLapanganPenebalanDDNByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanDDNByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-11">
                                        <button type="submit" class="btn green-meadow">Kirim</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        @else
                        <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        @endif
                    </div>
                    @endif

                    {{-- Proposal Definitif Tahap APBD --}}
                    @if ($recProposalByID->is_penebalan_nominal_apbd==1)
                    <div class="tab-pane fade" id="tab_1_4">
                        @if (empty($recProposalDefinitifAPBDByUUID))
                        <h5 style="color: red;font-weight: bolder;">*Saat ini Anda belum memasuki tahapan ini</h5>
                        @else
<div class="portlet-body form">
    <a href="{{ route('ppkd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>'apbd','tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
    <hr>
    <form action="{{ route('skpd.proposal.updateAPBD') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
    {{ csrf_field() }}
        <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan }}">
        <input type="hidden" name="jdl_prop" value="{{$recProposalDefinitifAPBDByUUID->jdl_prop}}">
        <input type="hidden" name="proposalID" value="{{ $proposalID }}">
        <input type="hidden" name="fase" value="{{$fase}}">
        <input type="hidden" name="nm_lembaga" value="{{$recProposalDefinitifAPBDByUUID->nm_lembaga}}">
        <input type="hidden" name="no_lembaga" value="{{$recProposalDefinitifAPBDByUUID->no_lembaga}}">
        <input type="hidden" name="email_lembaga" value="{{$recProposalDefinitifAPBDByUUID->email_lembaga}}">
        <div class="form-group form-md-radios">
            <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
            <div class="col-md-9">
                <div class="md-radio-inline">
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_84" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==0)?'checked':'disabled' }}>
                        <label for="checkbox1_84">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Menunggu </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_114" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==3)?'checked':'disabled' }}>
                        <label for="checkbox1_113">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Dikoreksi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_94" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==1)?'checked':'disabled' }}>
                        <label for="checkbox1_94">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Sudah Direkomendasi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_104" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==2)?'checked':'disabled' }}>
                        <label for="checkbox1_104">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Di Tolak </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">
            </label>
            <div class="col-md-7">
                <hr>
            </div>
        </div>
        {{-- for status diterima --}}
        <div id="nominal-rekomendasi4" @if($recProposalDefinitifAPBDByUUID->status_prop==0) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==2) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Kategori
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="kategoriProposalID" disabled>
                        <option value="">Pilih Kategori</option>
                        @foreach($recAllKategoriProposal as $key=>$value)
                            <option value="{{ $value->id }}" {{ ($recProposalDefinitifAPBDByUUID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Program
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_prg" disabled>
                        <option value="">Pilih Program</option>
                        @foreach($recAllProgram as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalDefinitifAPBDByUUID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Rekening
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_rek" disabled>
                        <option value="">Pilih Rekening</option>
                        @foreach($recAllRekening as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalDefinitifAPBDByUUID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                <div class="col-md-6">
                    <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral-apbd" value="{{ str_replace(".", ",", $recProposalDefinitifAPBDByUUID->nominal_rekomendasi) }}" disabled>
                    @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                    <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                    @else
                    <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                <div class="col-md-4">
                    <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_rekomendasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalDefinitifAPBDByUUID->tgl_rekomendasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                <div class="col-md-4">
                    <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_survei_lap }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalDefinitifAPBDByUUID->tgl_survei_lap }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_kelengkapan_administrasi }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalDefinitifAPBDByUUID->tgl_kelengkapan_administrasi }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                <div class="col-md-4">
                    <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_surat_skpd }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalDefinitifAPBDByUUID->tgl_surat_skpd }}" disabled>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
        </div>
        

        {{-- for status ditolak --}}
        <div id="alasan4" @if($recProposalDefinitifAPBDByUUID->status_prop==0) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==1) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                <div class="col-md-5">
                    <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalDefinitifAPBDByUUID->alasan_ditolak }}"> 
                </div>
            </div>
        </div>

        <div id="alasan-dikembalikan4" @if($recProposalDefinitifAPBDByUUID->status_prop==0) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==1) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==2) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                <div class="col-md-5">
                    <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalDefinitifAPBDByUUID->alasan_dikembalikan}}"> 
                </div>
            </div>
        </div>
    </form>
</div>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-eye"></i>Detil Proposal Definitif</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_01234" data-toggle="tab">1. Data Proposal </a>
                            </li>
                            <li>
                                <a href="#tab_1_11234" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                            </li>
                        </ul>
                    <div class="tab-content">
                        
                        {{-- Data Proposal --}}
                        <div class="tab-pane fade active in" id="tab_1_01234">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body" style="padding: 0;margin: 0;">
                                    <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalDefinitifAPBDByUUID->tgl_prop)) }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalDefinitifAPBDByUUID->no_proposal }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalDefinitifAPBDByUUID->jdl_prop }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Isi Proposal</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->latar_belakang !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->maksud_tujuan !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->keterangan_prop !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Nominal Permohonan</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($recProposalDefinitifAPBDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="form-section">Data Pendukung</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                    @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                    @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                <div class="col-md-9">
                                                    @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                    @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                    @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                    @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                    @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                <div class="col-md-9">
                                                    <div class="portlet-body">
                                                        <div class="input-group">
                                                            <input type="hidden" class="form-control" id="pac-input-apbd" placeholder="Cari Alamat">
                                                            {{-- <span class="input-group-btn">
                                                                <button class="btn blue" id="gmap_geocoding_btn">
                                                                    <i class="fa fa-search"></i>
                                                            </span> --}}
                                                        </div>
                                                        <div id="gmap_basic_apbd" class="gmaps"> </div>
                                                        <b>Latitude : </b>
                                                        <input type="text" class="form-control" name="latitude" id="latValAPBD" value="{{$recProposalDefinitifAPBDByUUID->latitude}}" readonly="" required="">
                                                        <b>Longitude : </b>
                                                        <input type="text" class="form-control" name="longitude" id="longValAPBD" value="{{$recProposalDefinitifAPBDByUUID->longitude}}" readonly="" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                        
                        {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                        <div class="tab-pane fade" id="tab_1_11234">
                            <div class="col-md-12">
                                <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                @if($recProposalDefinitifAPBDByUUID->nominal_pengajuan != $rencana_anggaran_apbd)
                                <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                @else
                                <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($recProposalDefinitifAPBDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_apbd,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <br><br><br>
                                <!--/row-->
                                
                                <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                    <thead>
                                        <tr>
                                            <th> No. </th>
                                            <th> Rencana Kegiatan/Rincian </th>
                                            <th> Vol/Satuan (jumlah) </th>
                                            <th> Harga Satuan (Rp) </th>
                                            <th> Anggaran (Rp) </th>
                                            <th> Anggaran Terpakai (Rp) </th>
                                            <th> Sisa Anggaran (Rp) </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($recRencanaByUUIDAPBD)==0)
                                            <tr>
                                                <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                            </tr>
                                        @else
                                            @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                            @foreach ($recRencanaByUUIDAPBD as $key=>$value)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td><b>{{$value->rencana}}</b></td>
                                                <td></td>
                                                <td></td>
                                                 <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                        <td><b> {{ number_format($value->anggaran_digunakan,2,",",".")}} </b></td>
                                                        <td><b> </b></td>
                                            </tr>
                                            {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,'apbd','rencana') !!}
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_01234" data-toggle="tab">1. Kelengkapan Administrasi </a>
                    </li>
                    <li>
                        <a href="#tab_2_11234" data-toggle="tab">2. Peninjauan Lapangan </a>
                    </li>
                </ul>

                <div class="tab-content">
                    {{-- Kelengkapan Administrasi --}}
                    <div class="tab-pane fade active in" id="tab_2_01234">
                        <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="apbd">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Administrasi</center></th>
                                                    <th><center>Ada</center></th>
                                                    <th><center>Tidak Ada</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas1111234" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas1111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas2111234" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas2111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan1111234" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan1111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan2111234" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan2111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab1111234" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab1111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab2111234" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab2111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan1111234" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan1111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan2111234" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan2111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" disabled="" name="is_approve"
                                        @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->is_approve) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                        </div>
                        </form>
                    </div>
                    
                    {{-- Kelengkapan Lapangan --}}
                    <div class="tab-pane fade" id="tab_2_11234">
                        @if(isset($recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->is_approve) && $recProposalAdministrasiDefinitifAPBDByUUIDAndTahap->is_approve==1)
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="apbd">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Lapangan</center></th>
                                                    <th><center>Sesuai</center></th>
                                                    <th><center>Tidak Sesuai</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas11111234" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas11111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_aktivitas22111234" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas22111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan11111234" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan11111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_kepengurusan22111234" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan22111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab11111234" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_rab) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab11111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_rab22111234" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_rab) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab22111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan11111234" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan11111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" disabled="" id="cek_waktu_pelaksanaan22111234" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan22111234">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganDefinitifAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" name="is_approve"
                                        @if(isset($recProposalLapanganDefinitifAPBDByUUIDAndTahap->is_approve) && $recProposalLapanganDefinitifAPBDByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-11">
                                        <button type="submit" class="btn green-meadow">Kirim</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        @else
                        <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        @endif
                    </div>
                    @endif

                    {{-- Proses Anggaran --}}
                    <div class="tab-pane fade" id="tab_1_5">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><b>Proses Anggaran</b></h4>
                                <hr>
                                <!-- BEGIN FORM-->
                                <form action="{{ route('ppkd.proposal.updateProsesAnggaran') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
                                {{ csrf_field() }}
                                    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                    <input type="hidden" name="proposalID" value="{{ $proposalID }}">
                                    <input type="hidden" name="fase" value="{{ $fase }}">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal Pengajuan (Rp): </label>
                                        <div class="col-md-4">                                
                                            <input name="nominal_pengajuan" type="text" data-required="1" class="form-control input-numeral2" value="{{ str_replace(".", ",", $recProposalByID->nominal_pengajuan) }}" disabled="">

                                            <span class="help-block" style="color: green;"><i>Diinput oleh Lembaga.</i></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1"></label>
                                        <div class="col-md-4">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral3" value="{{ str_replace(".", ",", $recProposalByID->nominal_rekomendasi) }}" disabled="">
                                            <span class="help-block" style="color: green;"><i>Diinput oleh SKPD.</i></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1"></label>
                                        <div class="col-md-4">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal TAPD (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_tapd" type="text" class="form-control input-numeral4" value="{{ str_replace(".", ",", $recProposalByID->nominal_tapd) }}" {{ ($recDetailPeriodeTahapanByNoper[2]->status==0)?'disabled':'' }}>
                                            
                                            @if($recDetailPeriodeTahapanByNoper[2]->status==1)
                                            <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                                            @else
                                            <span class="help-block" style="color: red;"><i>Pembahasan TAPD telah berakhir.</i></span>
                                            @endif

                                            <input type="checkbox" name="is_penebalan_nominal_tapd" {{ ($recProposalByID->is_penebalan_nominal_tapd==1)?'checked':'' }} {{-- {{ ($recDetailPeriodeTahapanByNoper[2]->status==0)?'disabled':'' }} --}} disabled=""> Setuju Proposal Penebalan
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1"></label>
                                        <div class="col-md-4">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal BANGGAR (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_banggar" type="text" class="form-control input-numeral5" value="{{ str_replace(".", ",", $recProposalByID->nominal_banggar) }}" {{ ($recDetailPeriodeTahapanByNoper[3]->status==0)?'disabled':'' }}>
                                            
                                            @if($recDetailPeriodeTahapanByNoper[3]->status==1)
                                            <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                                            @else
                                            <span class="help-block" style="color: red;"><i>Pembahasan Banggar telah berakhir.</i></span>
                                            @endif

                                            <input type="checkbox" name="is_penebalan_nominal_banggar" {{ ($recProposalByID->is_penebalan_nominal_banggar==1)?'checked':'' }} {{-- {{ ($recDetailPeriodeTahapanByNoper[3]->status==0)?'disabled':'' }} --}} disabled=""> Setuju Proposal Penebalan
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1"></label>
                                        <div class="col-md-4">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal DDN (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_ddn" type="text" class="form-control input-numeral6" value="{{ str_replace(".", ",", $recProposalByID->nominal_ddn) }}" {{ ($recDetailPeriodeTahapanByNoper[4]->status==0)?'disabled':'' }}>
                                            
                                            @if($recDetailPeriodeTahapanByNoper[4]->status==1)
                                            <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                                            @else
                                            <span class="help-block" style="color: red;"><i>Pembahasan DDN telah berakhir.</i></span>
                                            @endif

                                            <input type="checkbox" name="is_penebalan_nominal_ddn" {{ ($recProposalByID->is_penebalan_nominal_ddn==1)?'checked':'' }} {{-- {{ ($recDetailPeriodeTahapanByNoper[4]->status==0)?'disabled':'' }} --}} disabled=""> Setuju Proposal Penebalan
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1"></label>
                                        <div class="col-md-4">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal APBD (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_apbd" type="text" class="form-control input-numeral7" value="{{ str_replace(".", ",", $recProposalByID->nominal_apbd) }}" {{ ($recDetailPeriodeTahapanByNoper[5]->status==0)?'disabled':'' }}>
                                            
                                            @if($recDetailPeriodeTahapanByNoper[5]->status==1)
                                            <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                                            @else
                                            <span class="help-block" style="color: red;"><i>Pembahasan APBD telah berakhir.</i></span>
                                            @endif
                                            
                                            

                                            <input type="checkbox" name="is_penebalan_nominal_apbd" id="emailCheck" data-id="APBD" {{ ($recProposalByID->is_penebalan_nominal_apbd==1)?'checked':'' }} {{-- {{ ($recDetailPeriodeTahapanByNoper[5]->status==0)?'disabled':'' }} --}} disabled=""> Setuju Proposal Definitif
                                        </div>
                                    </div>
                                    {{-- <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal Pencairan (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_pencairan" type="text" data-required="1" class="form-control input-numeral8" value="{{ str_replace(".", ",", $recProposalByID->nominal_pencairan) }}">
                                        </div>
                                    </div> --}}
                                    {{-- <div class="form-group">
                                        <label class="col-md-3 control-label" for="form_control_1">Nominal LPJ (Rp): </label>
                                        <div class="col-md-4">
                                            <input name="nominal_lpj" type="text" data-required="1" class="form-control input-numeral9" value="{{ str_replace(".", ",", $recProposalByID->nominal_lpj) }}">
                                        </div>
                                    </div> --}}
                                    <hr>
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- Modal ===============================--> 
<div class="modal fade text-left" id="confirm-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header ">
          <h3 class="modal-title" id="myModalLabel35"> Konfirmasi </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          </div>
          <form>
          <div class="modal-body">
             <input type="hidden" id="tipe">
             <input type="hidden" id="table_id">
             <h5 class="mt-1" id="confirm-message"></h5>
          </div>
          <div class="modal-footer">
              <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal">
                  <i class="ft-x"></i> Batal
              </a>
              <a href="javascript:void(0)" class="btn  btn-primary" id="btn_save">
                  <i class="ft-trash"></i> KIRIM
              </a>
          </div>
          </form>
        </div>
    </div>
</div> 
<!-- End of modal --> 

@stop
@section('javascript')
    @parent

    <script type="text/javascript">
            $(document).ready(function(){
                  $("#emailCheck").click(function(){
  
                    var no_prop         = "{{ $recProposalByID->no_prop  }}";
                    var tahapan         =  $(this).attr("data-id"); 
                   
                    if ($(this).is(":checked"))
                    {
                       var status=1;
                       $("#confirm-message").html(" Apakah anda yakin untuk mengirimkan notifikasi APBD kepada SKPD dan Lembaga ?"); 
                       $("#btn_save").show();
                       $("#confirm-popup").modal('show');

                        $("#btn_save").click(function(e){ 
                        e.preventDefault(); 
                             $.ajax({
                              async: true,
                              type: "POST",
                              url: "{{ route('ppkd.notif.approval') }}",
                              dataType: "json",
                              data: {_token: "{{ csrf_token() }}",no_prop:no_prop,tahapan:tahapan},
                              success : function(res){
                                if(res.status == true){ 
                                   Command: toastr["success"]("Sukeses",res.message) 
                                          toastr.options = {
                                            "closeButton": false,
                                            "debug": false,
                                            "newestOnTop": false,
                                            "progressBar": false,
                                            "positionClass": "toast-top-right",
                                            "preventDuplicates": false,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "5000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "swing",
                                            "hideEasing": "linear",
                                            "showMethod": "fadeIn",
                                            "hideMethod": "fadeOut"
                                          }
                                    // location.reload();
                                    $("#confirm-popup").modal('hide');
                                }
                                else{ 
                                 Command: toastr["error"]("Error", "  gagal disimpan") 
                                            toastr.options = {
                                              "closeButton": false,
                                              "debug": false,
                                              "newestOnTop": false,
                                              "progressBar": false,
                                              "positionClass": "toast-top-right",
                                              "preventDuplicates": false,
                                              "onclick": null,
                                              "showDuration": "300",
                                              "hideDuration": "1000",
                                              "timeOut": "5000",
                                              "extendedTimeOut": "1000",
                                              "showEasing": "swing",
                                              "hideEasing": "linear",
                                              "showMethod": "fadeIn",
                                              "hideMethod": "fadeOut"
                                            }
                                            
                                }
                              },
                              error : function(err, ajaxOptions, thrownError){
                                 Command: toastr["error"]("Error", " gagal disimpan") 
                                            toastr.options = {
                                              "closeButton": false,
                                              "debug": false,
                                              "newestOnTop": false,
                                              "progressBar": false,
                                              "positionClass": "toast-top-right",
                                              "preventDuplicates": false,
                                              "onclick": null,
                                              "showDuration": "300",
                                              "hideDuration": "1000",
                                              "timeOut": "5000",
                                              "extendedTimeOut": "1000",
                                              "showEasing": "swing",
                                              "hideEasing": "linear",
                                              "showMethod": "fadeIn",
                                              "hideMethod": "fadeOut"
                                            }
                              }
                            });  
                        });  //end status 1
                      }else{
                        var status=0;
                      } 
                      // alert(status);
                  });

            });
          
        </script>

        <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            // autosubmit
            // $('#checkbox1_8,#checkbox1_9,#checkbox1_10').click(function(){
            //     $('form#form_sample_1').submit();
            // });
            $(".btn-pdf").on("submit", function(){
                return confirm("Apakah Anda yakin ?");
            });
            

            $('#checkbox1_8').click(function(){
                $('#nominal-rekomendasi,#alasan').css('display','none');
            });
            $('#checkbox1_9').click(function(){
                $('#nominal-rekomendasi').css('display','block');
                $('#alasan').css('display','none');
            });
            $('#checkbox1_10').click(function(){
                $('#nominal-rekomendasi').css('display','none');
                $('#alasan').css('display','block');
            });
        });
    </script>

    @if( !empty(Session::get('status')) )
    <script type="text/javascript">
        $(function(){
                $(window).load(function(){
                    Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                    toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }
                });
        });
    </script>
    @endif

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = '{{ $recProposalByID->latitude }}';
            var longValue = '{{ $recProposalByID->longitude }}';
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>

    @if (!empty($recProposalPenebalanTAPDByUUID))
    {{-- tapd --}}
    <script type="text/javascript">
        var MapsGoogleTAPD = function () {
            
            var latValue = {{ ($recProposalPenebalanTAPDByUUID->latitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanTAPDByUUID->longitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalPenebalanTAPDByUUID->latitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->latitude }},{{ ($recProposalPenebalanTAPDByUUID->longitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_tapd'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-tapd'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValTAPD').val(data.lat());
                $('#longValTAPD').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleTAPD.init();
    });
    </script>
    @endif

    @if ($recProposalPenebalanBANGGARByUUID)
    {{-- banggar --}}
    <script type="text/javascript">
        var MapsGoogleBANGGAR = function () {
            
            var latValue = {{ ($recProposalPenebalanBANGGARByUUID->latitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanBANGGARByUUID->longitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalPenebalanBANGGARByUUID->latitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->latitude }},{{ ($recProposalPenebalanBANGGARByUUID->longitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_banggar'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-banggar'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValBANGGAR').val(data.lat());
                $('#longValBANGGAR').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleBANGGAR.init();
    });
    </script>
    @endif

    @if ($recProposalPenebalanDDNByUUID)
    {{-- ddn --}}
    <script type="text/javascript">
        var MapsGoogleDDN = function () {
            
            var latValue = {{ ($recProposalPenebalanDDNByUUID->latitude==NULL)?'0':$recProposalPenebalanDDNByUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanDDNByUUID->longitude==NULL)?'0':$recProposalPenebalanDDNByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalPenebalanDDNByUUID->latitude==NULL)?'0':$recProposalPenebalanDDNByUUID->latitude }},{{ ($recProposalPenebalanDDNByUUID->longitude==NULL)?'0':$recProposalPenebalanDDNByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_ddn'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-ddn'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValDDN').val(data.lat());
                $('#longValDDN').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleDDN.init();
    });
    </script>
    @endif

    @if ($recProposalDefinitifAPBDByUUID)
    {{-- apbd --}}
    <script type="text/javascript">
        var MapsGoogleAPBD = function () {
            
            var latValue = {{ ($recProposalDefinitifAPBDByUUID->latitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->latitude }};
            var longValue = {{ ($recProposalDefinitifAPBDByUUID->longitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalDefinitifAPBDByUUID->latitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->latitude }},{{ ($recProposalDefinitifAPBDByUUID->longitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_apbd'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-apbd'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValAPBD').val(data.lat());
                $('#longValAPBD').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleAPBD.init();
    });
    </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function(){
            // numeral
            new Cleave('.input-numeral1', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral2', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral3', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral4', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral5', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral6', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral7', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            // tapd
            @if ($recProposalByID->is_penebalan_nominal_tapd==1)
            new Cleave('.input-numeral-tapd', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif

            // banggar
            @if ($recProposalByID->is_penebalan_nominal_banggar==1)
            new Cleave('.input-numeral-banggar', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif

            // ddn
            @if ($recProposalByID->is_penebalan_nominal_ddn==1)
            new Cleave('.input-numeral-ddn', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif

            // apbd
            @if ($recProposalByID->is_penebalan_nominal_apbd==1)
            new Cleave('.input-numeral-apbd', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif
           
        });
    </script>
@stop