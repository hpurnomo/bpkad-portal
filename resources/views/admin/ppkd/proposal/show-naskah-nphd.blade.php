@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.manage',['fase'=>$fase]) }}">{{ ucwords($fase) }} Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Naskah Perjanjian NPHD</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <div class="portlet-body form">
        	<div class="row">
                <div class="col-md-6">
	                <form class="btn-pdf-nphd" action="{{ route('ppkd.proposal.cetak.naskah.perjanjian.nphd.generateNPHDToPDF') }}" method="POST" style="float: left;">
		                <input type="hidden" name="_method" value="POST">
		                <input type="hidden" name="no_prop" value="{{ $no_prop }}">
		                <input type="hidden" name="fase" value="{{$fase}}">
		                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
		                @if($dataCNPHD['id']!="")
		                <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
		                @endif
		            </form>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('ppkd.proposal.edit',['fase'=>$fase,'id'=>$no_prop]) }}" class="btn btn-default" style="float: right;">Kembali</a>
                </div>
            </div>

            <form action="{{ route('ppkd.proposal.cetak.naskah.perjanjian.nphd.update') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="no_prop" value="{{ $no_prop }}">
                <input type="hidden" name="fase" value="{{$fase}}">
                <center>
                    <h4>PERJANJIAN <br><br> 
                        ANTARA <br><br>
                        PEMERINTAH PROVINSI DAERAH KHUSUS <br><br> 
                        IBUKOTA JAKARTA <br><br>
                        DAN <br><br>
                        {{ strtoupper($recProposalByID->nm_lembaga) }} <br><br> 
                        TENTANG <br><br>
                        PEMBERIAN HIBAH DALAM BENTUK UANG
                    </h4>
                </center>
                <p style="padding: 30px 30px 0 100px;">
                Pada hari ini 
                <select name="hari1">
                    <option value="Senin" {{($dataCNPHD['hari1']=='Senin')?'selected':''}}>Senin</option>
                    <option value="Selasa" {{($dataCNPHD['hari1']=='Selasa')?'selected':''}}>Selasa</option>
                    <option value="Rabu" {{($dataCNPHD['hari1']=='Rabu')?'selected':''}}>Rabu</option>
                    <option value="Kamis" {{($dataCNPHD['hari1']=='Kamis')?'selected':''}}>Kamis</option>
                    <option value="Jumat" {{($dataCNPHD['hari1']=='Jumat')?'selected':''}}>Jumat</option>
                </select>
                tanggal
                <select name="tanggal1">
                    @for ($i = 1; $i <= 31; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select>
                bulan
                <select name="bulan1">
                    <option value="Januari" {{($dataCNPHD['bulan1']=="Januari")?'selected':''}}>Januari</option>
                    <option value="Februari" {{($dataCNPHD['bulan1']=="Februari")?'selected':''}}>Februari</option>
                    <option value="Maret" {{($dataCNPHD['bulan1']=="Maret")?'selected':''}}>Maret</option>
                    <option value="April" {{($dataCNPHD['bulan1']=="April")?'selected':''}}>April</option>
                    <option value="Mei" {{($dataCNPHD['bulan1']=="Mei")?'selected':''}}>Mei</option>
                    <option value="Juni" {{($dataCNPHD['bulan1']=="Juni")?'selected':''}}>Juni</option>
                    <option value="Juli" {{($dataCNPHD['bulan1']=="Juli")?'selected':''}}>Juli</option>
                    <option value="Agustus" {{($dataCNPHD['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                    <option value="September" {{($dataCNPHD['bulan1']=="September")?'selected':''}}>September</option>
                    <option value="Oktober" {{($dataCNPHD['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                    <option value="November" {{($dataCNPHD['bulan1']=="November")?'selected':''}}>November</option>
                    <option value="Desember" {{($dataCNPHD['bulan1']=="Desember")?'selected':''}}>Desember</option>
                </select>
                tahun
                <select name="tahun1">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> 
                yang bertanda tangan di bawah ini: 
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;">
                    <tr>
                        <td>I.</td>
                        <td style="padding: 0 200px 0 10px;">Nama : <input type="text" name="nama1"   value="{{$dataCNPHD['nama1']}}"  readonly=""></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Dalam Jabatan Kepala Bidang Perbendaharaan dan Kas Daerah Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta selaku Kuasa Bendahara Umum Daerah berkantor di Jln. Medan Merdeka Selatan No. 8-9, Kota Administrasi Jakarta Pusat;<br>
                        Oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah Provinsi DKI Jakarta, untuk selanjutnya disebut PIHAK PERTAMA</td>
                    </tr>
                    <tr>
                        <td>II.</td>
                        <td style="padding: 0 200px 0 10px;">Nama : <input type="text" name="nama2" value="{{$dataCNPHD['nama2']}}"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Jabatan <input type="text" name="jabatan1" value="{{$dataCNPHD['jabatan1']}}"> berkantor di Jln. <input type="text" name="alamat" value="{{$dataCNPHD['alamat']}}"> dalam hal ini menjalani jabatannya sesuai <textarea name="deskripsi_jabatan" rows="10" cols="117">{{$dataCNPHD['deskripsi_jabatan']}}</textarea>, 
                        oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah/Pemerintah Daerah/Kelompok Masyarakat/Organisasi Kemasyarakatan untuk selanjutnya disebut PIHAK KEDUA.</td>
                    </tr>
                </table>
                <p style="padding: 0 200px 0 100px;text-align: justify;">Bahwa masing-masing pihak bertindak dalam jabatannya sebagaimana tersebut di atas, secara bersama-sama disebut PARA PIHAK dengan terlebih dahulu memperhatikan ketentuan sebagai berikut :</p>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;">
                    <tr>
                        <td style="vertical-align: top;">1.</td>
                        <td style="padding: 0 200px 0 10px;">Undang-Undang  Nomor 29 Tahun 2007 tentang Pemerintahan Provinsi Daerah Khusus Ibukota Jakarta sebagai Ibukota Negara Kesatuan Republik Indonesia;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">2.</td>
                        <td style="padding: 0 200px 0 10px;">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah sebagaimana telah diubah dengan Undang-Undang Nomor 9 Tahun 2015;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">3.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan Pemerintah Nomor 58 Tahun 2005 tentang Pengelolaan Keuangan Daerah;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">4.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan Menteri Dalam Negeri Nomor 13 Tahun 2006 tentang Pedoman Pengelolaan 
Keuangan Daerah sebagaimana telah diubah dengan Peraturan Menteri Dalam Negeri 
Nomor 21 Tahun 2011;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">5.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan Daerah Nomor 5 Tahun 2007 tentang Pokok-pokok Pengelolaan Keuangan Daerah;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">6.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan Daerah Nomor 5 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Provinsi Daerah Khusus Ibukota Jakarta;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">7.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan <input type="text" name="peraturan4" value="{{$dataCNPHD['peraturan4']}}"> Nomor <input type="text" name="nomor4" value="{{$dataCNPHD['nomor4']}}"> Tahun 
                        <select name="tahun4">
                            @for ($i = 2016; $i <= 2020; $i++)
                                <option value="{{$i}}" {{($dataCNPHD['tahun4']==$i)?'selected':''}}>{{$i}}</option>
                            @endfor
                            {{-- <option value="2016">2016</option> --}}
                        </select>
                        tentang <textarea name="tentang4" cols="100">{{$dataCNPHD['tentang4']}}</textarea>;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">8.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan Gubernur Nomor 142 Tahun 2013 tentang Sistem dan Prosedur Pengelolaan Keuangan Daerah;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">9.</td>
                        <td style="padding: 0 200px 0 10px;">Peraturan Gubernur Provinsi DKI Jakarta Nomor <input type="text" name="nomor5" {{-- value="{{$dataCNPHD['nomor5']}}" --}} value="55" readonly="">
                        Tahun 
                        <select name="tahun5">
                            {{-- @for ($i = 2016; $i <= 2020; $i++)
                                <option value="{{$i}}" {{($dataCNPHD['tahun5']==$i)?'selected':''}}>{{$i}}</option>
                            @endfor --}}
                            <option value="2013">2013</option>
                        </select> 
                        tentang Tata Cara Pengusulan, Evaluasi, Penganggaran, Pelaksanaan, Penatausahaan, Pertanggungjawaban, Pelaporan dan  Monitoring Hibah, Bantuan Sosial dan Bantuan Keuangan yang Bersumber dari Anggaran Pendapatan dan Belanja Daerah;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">10.</td>
                        <td style="padding: 0 200px 0 10px;">Keputusan  Gubernur    Nomor <input type="text" name="nomor6" value="{{$dataCNPHD['nomor6']}}"> 
                        Tahun
                        <select name="tahun6">
                            @for ($i = 2016; $i <= 2020; $i++)
                                <option value="{{$i}}" {{($dataCNPHD['tahun6']==$i)?'selected':''}}>{{$i}}</option>
                            @endfor
                        </select> 
                        tentang <textarea name="tentang6" cols="100">{{$dataCNPHD['tentang6']}}</textarea>;</td>
                    </tr>
                </table>
                
                <p style="padding: 0 200px 0 100px;text-align: justify;">Bahwa berdasarkan hal tersebut dan sesuai dengan rekomendasi {{$dataCNPHD['namaSKPD']}} Nomor <input type="text" name="nomor7" value="{{$dataCNPHD['nomor7']}}"> tanggal 
                <select name="tanggal7">
                    @for ($i = 1; $i <= 31; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tanggal7']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> 
                bulan
                <select name="bulan7">
                    <option value="Januari" {{($dataCNPHD['bulan7']=="Januari")?'selected':''}}>Januari</option>
                    <option value="Februari" {{($dataCNPHD['bulan7']=="Februari")?'selected':''}}>Februari</option>
                    <option value="Maret" {{($dataCNPHD['bulan7']=="Maret")?'selected':''}}>Maret</option>
                    <option value="April" {{($dataCNPHD['bulan7']=="April")?'selected':''}}>April</option>
                    <option value="Mei" {{($dataCNPHD['bulan7']=="Mei")?'selected':''}}>Mei</option>
                    <option value="Juni" {{($dataCNPHD['bulan7']=="Juni")?'selected':''}}>Juni</option>
                    <option value="Juli" {{($dataCNPHD['bulan7']=="Juli")?'selected':''}}>Juli</option>
                    <option value="Agustus" {{($dataCNPHD['bulan7']=="Agustus")?'selected':''}}>Agustus</option>
                    <option value="September" {{($dataCNPHD['bulan7']=="September")?'selected':''}}>September</option>
                    <option value="Oktober" {{($dataCNPHD['bulan7']=="Oktober")?'selected':''}}>Oktober</option>
                    <option value="November" {{($dataCNPHD['bulan7']=="November")?'selected':''}}>November</option>
                    <option value="Desember" {{($dataCNPHD['bulan7']=="Desember")?'selected':''}}>Desember</option>
                </select>
                tahun
                <select name="tahun7">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tahun7']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select>,
                PARA PIHAK sepakat untuk melakukan Perjanjian Hibah dalam bentuk uang, dengan syarat dan ketentuan sebagai berikut :</p>
                
                <center>
                    Pasal 1
                </center>
                <p style="padding: 0 200px 0 100px;text-align: justify;">PIHAK PERTAMA memberikan Hibah berupa uang melalui transfer dana kepada PIHAK 
KEDUA sebagaimana PIHAK KEDUA menerima dari PIHAK PERTAMA senilai Rp <input type="text" name="senilai_angka" value="{{$dataCNPHD['senilai_angka']}}"> 
( <input type="text" name="senilai_teks" value="{{$dataCNPHD['senilai_teks']}}"> rupiah ) {{-- pada hari <select name="hari2">
                    <option value="Senin" {{($dataCNPHD['hari2']=='Senin')?'selected':''}}>Senin</option>
                    <option value="Selasa" {{($dataCNPHD['hari2']=='Selasa')?'selected':''}}>Selasa</option>
                    <option value="Rabu" {{($dataCNPHD['hari2']=='Rabu')?'selected':''}}>Rabu</option>
                    <option value="Kamis" {{($dataCNPHD['hari2']=='Kamis')?'selected':''}}>Kamis</option>
                    <option value="Jumat" {{($dataCNPHD['hari2']=='Jumat')?'selected':''}}>Jumat</option>
                </select> tanggal <select name="tanggal3">
                    @for ($i = 1; $i <= 31; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tanggal3']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> --}} untuk kegiatan :</p>

                <table width="90%" border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;">
                    <tr>
                        {{-- <td style="vertical-align: top;">a.</td> --}}
                        <td style="padding: 0 200px 0 10px;">
                            {{-- <input type="text" name="kegiatan1" value="{{$dataCNPHD['kegiatan1']}}" style="width: 600px;"> --}}
                            <textarea name="kegiatan1" class="wysihtml5 form-control" rows="6">{!! $dataCNPHD['kegiatan1']!!}</textarea>
                        </td>
                    </tr>
                    {{-- <tr>
                        <td style="vertical-align: top;">b.</td>
                        <td style="padding: 0 200px 0 10px;"><input type="text" name="kegiatan2" value="{{$dataCNPHD['kegiatan2']}}" style="width: 600px;"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">c.</td>
                        <td style="padding: 0 200px 0 10px;"><input type="text" name="kegiatan3" value="{{$dataCNPHD['kegiatan3']}}" style="width: 600px;"></td>
                    </tr> --}}
                </table>
                <br>
                <center>
                    Pasal 2
                </center>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;">Dalam rangka pemberian Hibah sebagaimana dimaksud dalam Pasal 1, PIHAK 
PERTAMA bertanggung jawab terhadap penyerahan Hibah yang disampaikan melalui 
rekening Bank PIHAK KEDUA, sedangkan PIHAK KEDUA bertanggung jawab 
sepenuhnya atas pelaksanaan penggunaan Hibah dimaksud.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;">PIHAK KEDUA dilarang mengalihkan dana Hibah sebagaimana dimaksud pada ayat (1) 
kepada pihak lain.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="padding: 0 200px 0 10px;">Apabila PIHAK KEDUA tidak melaksanakan kegiatan sebagaimana dimaksud dalam Pasal 
1 maka harus mengembalikan Hibah yang telah diterima kepada PIHAK PERTAMA 
melalui rekening Kas Daerah.</td>
                    </tr>
                </table>
                <br>
                <center>
                    Pasal 3
                </center>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;">PIHAK KEDUA paling lambat 1 (satu) bulan setelah menyelesaikan pekerjaan atau 
paling lambat tanggal 31 Desember tahun anggaran berjalan, wajib menyampaikan 
laporan kegiatan dan keuangan sesuai ketentuan peraturan perundang-undangan 
kepada PIHAK PERTAMA.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;">Laporan keuangan sebagaimana dimaksud pada ayat (1) terhadap PIHAK KEDUA yang 
menerima belanja dengan nilai di atas Rp 200.000.000,00 (dua ratus juta rupiah) wajib 
dilampirkan hasil audit oleh Akuntan Publik kecuali yang telah dilakukan audit oleh 
Aparat Pengawas Fungsional.</td>
                    </tr>
                </table>

                <p style="padding: 0 200px 0 100px;text-align: justify;">Demikian Perjanjian Hibah ini dibuat dan ditandatangani di Jakarta pada hari dan tanggal 
tersebut di atas dalam rangkap 2 (dua), masing-masing bermeterai cukup dan mempunyai 
kekuatan hukum yang sama, 1 (satu) eksemplar untuk PIHAK PERTAMA dan 1 (satu) 
eksemplar untuk PIHAK KEDUA.</p>
                <br>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <center>
                            <h4>PIHAK KEDUA</h4>
                            <input type="text" name="jabatan2" value="{{$dataCNPHD['jabatan2']}}">,
                            <br><br><br>
                            <h4><input type="text" name="nama_pihak_kedua" value="{{$dataCNPHD['nama_pihak_kedua']}}"></h4>
                            <h4>NIP <input type="text" name="nip_pihak_kedua" value="{{$dataCNPHD['nip_pihak_kedua']}}"></h4>
                            </center>
                        </td>
                        <td width="30%"></td>
                        <td><center>
                            <h4>PIHAK PERTAMA <br>KEPALA BIDANG PERBENDAHARAAN DAN KAS DAERAH<br> BADAN PENGELOLA KEUANGAN DAERAH PROVINSI DKI JAKARTA<br>selaku <br>KUASA BENDAHARA UMUM DAERAH,</h4>
                            <br><br><br>
                            <h4><input type="text" name="nama_kepala" {{-- value="{{$dataCNPHD['nama_kepala']}}" --}} value="YANNI SURYANI, SE" readonly=""></h4>
                            <h4>NIP <input type="text" name="nip_kepala" {{-- value="{{$dataCNPHD['nip_kepala']}}" --}} value="196101261987032002" readonly=""></h4>
                            </center>
                        </td>
                    </tr>
                </table>

                <hr>
                @if($dataCNPHD['is_generate']==0)
                <button type="submit" class="btn green" id="btn-simpan-nphd">Simpan</button>
                @endif
            </form>
            
            <div class="row">
	            
            </div>
        </div>
        
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            $(".btn-pdf-nphd").on("submit", function(){
	            $("#btn-simpan-nphd").hide();
	            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui data cetak naskah perjanjian NPHD tidak dapat di update kembali. ");
	        });
        });
    </script>
    @if( !empty(Session::get('status')) )
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
@stop