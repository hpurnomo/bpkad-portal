@extends('admin/layout/template')
@section('content')
<style type="text/css">
.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-100{
    width:100px;
}
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.manage',['fase'=>$fase,'noper'=>$noper]) }}">{{ ucwords($fase) }} Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p> Data dibawah ini merupakan proposal dari seluruh lembaga yang telah direkomendasikan/di-verifikasi oleh SKPD terkait. <br> <strong>Klik pada judul proposal untuk melihat detil proposal</strong></p>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                             @if(Auth::user()->email != 'bpk_dki@jakarta.go.id')
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('ppkd.export.to.excel',['noper'=>$noper]) }}" target="_blank" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Export e-budgeting
                                            <i class="fa fa-file-excel-o"></i>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="#" class="btn btn-circle sbold default upload" style="box-shadow: 2px 2px 5px #88898A !important;">  &nbsp;Import e-budgeting
                                            <i class="fa fa-file-excel-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        
                        <table class="table table-striped table-bordered table-hover table-checkable order-column display responsive nowrap" id="example">
                            <thead>
                                <tr>
                                    <th> No. </th>
                                    <th> Judul Proposal </th>
                                    <th> Status </th>
                                    <th> Lembaga </th>
                                    <th> SKPD </th>
                                    <th> Nominal Permohonan (Rp) </th>
                                    <th> Nominal Rekomendasi (Rp) </th>
                                    <th> Tgl Submit </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recProposalDisetujui as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td> 
                                    @if(Auth::user()->email == 'bpk_dki@jakarta.go.id')
                                     <td>  {{ ucwords(strtolower($value->jdl_prop)) }} </td>
                                    @else
                                     <td><a href="{{ route('ppkd.proposal.edit',['fase'=>$fase,'id'=>$value->no_prop]) }}"> {{ ucwords(strtolower($value->jdl_prop)) }} </a></td>
                                    @endif

                                    <td>
                                        @if($value->status_prop==0)
                                            Belum Direkomendasi
                                        @elseif($value->status_prop==1)
                                            Sudah Direkomendasi
                                        @elseif($value->status_prop==2)
                                            Di Tolak
                                            <p class="bg-red bg-font-red" style="padding:5px;">Alasan : {{ $value->alasan_ditolak }} </p>
                                        @endif
                                    </td>
                                    <td> {{ ucwords(strtolower($value->nm_lembaga)) }} </td>
                                    <td> {{ $value->nm_skpd }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_pengajuan,'2',',','.') }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_rekomendasi,'2',',','.') }} </td>
                                    <td> @if($value->tgl_prop != NULL){{ date('d M Y',strtotime($value->tgl_prop)) }} @endif</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

 <!-- Modal ===============================--> 
  <div class="modal fade text-left" id="confirm-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header ">
      <h3 class="modal-title" id="myModalLabel35"> Import E-budgeting </h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      </div>
   
      <div class="modal-body"> 

         <form action="" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label class="control-label col-md-3"> Tahapan  </label>
                <div class="col-md-9">
                    <select class="form-control ">
                        <option value="TAPD">TAPD</option>
                        <option value="BANGAR">BANGAR</option>
                        <option value="DDN">DDN </option>
                        <option value="APBD">APBD</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"> File Excel E-budgeting
                <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <div class="clearfix margin-top-10">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat Excel </small></span>
                    </div>
                    <input type="file" name="file_ebudgeting" class="btn default form-control">
                </div>
            </div>
         </form>

      </div>
      <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal">
              <i class="ft-x"></i> Batal
          </a>
          <a href="javascript:void(0)" class="btn  btn-primary" id="btn_save">
              <i class="ft-trash"></i> Upload
          </a>
      </div>
      </form>
    </div>
    </div>
  </div> 

@stop
@section('javascript')
    @parent
    {{-- <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                responsive: true,
                columnDefs: [
                    { responsivePriority: 1, targets: 1 },
                    { responsivePriority: 2, targets: 3 },
                    { responsivePriority: 3, targets: 4 },
                    { responsivePriority: 4, targets: 5 },
                ]
            } );
        } );
    </script> --}}
    <script type="text/javascript">

          $(".upload").click(function(e){ 
            $("#confirm-popup").modal('show');
          });

        $(document).ready(function() {



            $('#example').DataTable( {
                responsive: true,
                columnDefs: [
                    {
                        render: function (data, type, full, meta) {
                            return "<div class='text-wrap width-100'>" + data + "</div>";
                        },
                        targets: [1,2,3,4]
                    }
                 ]
            } );
        } );
    </script>
@stop