@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Cetak</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{dd($recProposalByID)}} --}}
        
        <a href="{{ route('ppkd.proposal.edit',['fase'=>$fase,'id'=>$recProposalByID->no_prop]) }}" class="btn btn-default" style="float: right;">Kembali</a><br><br>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-print" aria-hidden="true"></i> Pengaturan Cetak Laporan </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <ul class="nav nav-tabs tabs-left">
                            <li class="{{($tab=='kelengkapan-administrasi')?'active':''}}">
                                <a href="#tab_6_1" data-toggle="tab" aria-expanded="true"> Kelengkapan Administrasi </a>
                            </li>
                            <li class="{{($tab=='peninjauan-lapangan')?'active':''}}">
                                <a href="#tab_6_2" data-toggle="tab" aria-expanded="false"> Berita Acara Peninjauan Lapangan </a>
                            </li>
                            <li class="{{($tab=='rekomendasi')?'active':''}}">
                                <a href="#tab_6_3" data-toggle="tab"> Rekomendasi Pengusulan</a>
                            </li>
                            <li class="{{($tab=='nphd')?'active':''}}">
                                <a href="#tab_6_5" data-toggle="tab"> NPHD</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <div class="tab-content">
                            {{-- Kelengkapan Administrasi --}}
                            <div class="tab-pane {{($tab=='kelengkapan-administrasi')?'active':''}} in" id="tab_6_1">
                            <form action="{{ route('ppkd.cetak.updateKal') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                            <input type="hidden" name="fase" value="{{$fase}}">
                            <input type="hidden" name="tab" value="kelengkapan-administrasi">
                                <center>
                                    <h4>HASIL PENELITIAN KELENGKAPAN ADMINISTRASI</h4>
                                    <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_lembaga}}</h4>
                                    <h4>TAHUN ANGGARAN {{$recProposalByID->tahun_anggaran}}</h4>
                                    <h4>NOMOR <input type="text" name="nomor" value="{{$recProposalByID->no_kelengkapan_administrasi}}" disabled="" style="width: 400px;"></h4>
                                </center>
                                <br><br>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada hari ini 
                                <select name="hari" disabled="">
                                    <option value="Senin" {{($dataCka['hari']=='Senin')?'selected':''}}>Senin</option>
                                    <option value="Selasa" {{($dataCka['hari']=='Selasa')?'selected':''}}>Selasa</option>
                                    <option value="Rabu" {{($dataCka['hari']=='Rabu')?'selected':''}}>Rabu</option>
                                    <option value="Kamis" {{($dataCka['hari']=='Kamis')?'selected':''}}>Kamis</option>
                                    <option value="Jumat" {{($dataCka['hari']=='Jumat')?'selected':''}}>Jumat</option>
                                </select>
                                tanggal
                                <select name="tanggal1" disabled="">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}" {{($dataCka['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                bulan
                                <select name="bulan1" disabled="">
                                    <option value="Januari" {{($dataCka['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                    <option value="Februari" {{($dataCka['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                    <option value="Maret" {{($dataCka['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                    <option value="April" {{($dataCka['bulan1']=="April")?'selected':''}}>April</option>
                                    <option value="Mei" {{($dataCka['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                    <option value="Juni" {{($dataCka['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                    <option value="Juli" {{($dataCka['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                    <option value="Agustus" {{($dataCka['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                    <option value="September" {{($dataCka['bulan1']=="September")?'selected':''}}>September</option>
                                    <option value="Oktober" {{($dataCka['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                    <option value="November" {{($dataCka['bulan1']=="November")?'selected':''}}>November</option>
                                    <option value="Desember" {{($dataCka['bulan1']=="Desember")?'selected':''}}>Desember</option>
                                </select>
                                tahun
                                <select name="tahun1" disabled="">
                                    @for ($i = 2016; $i <= 2020; $i++)
                                        <option value="{{$i}}" {{($dataCka['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                yang bertanda tangan di bawah ini :</p>              
                                <table border="1" width="100%" cellspacing="0" cellpadding="0">
                                  <thead>
                                    <tr>
                                      <th style="padding: 10px;"><center>No</center></th>
                                      <th style="padding: 10px;"><center>Nama</center></th>
                                      <th style="padding: 10px;"><center>Jabatan Dalam Tim</center></th>  
                                      <th style="padding: 10px;"><center>Tanda Tangan</center></th>
                                      <th style="padding: 10px;"><center></center></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($dataDcka as $key=>$value)
                                    <tr>
                                      <td style="padding: 10px;"><center>{{ $value->no_urut }}</center></td>
                                      <td style="padding: 10px;">{{ $value->nama }}</td>
                                      <td style="padding: 10px;">{{ $value->jabatan }}</td>  
                                      <td style="padding: 10px;"></td>
                                      <td style="padding: 10px;"><center><a href="{{ route('skpd.cetak.delete.detail.peserta.kelengkapan.administrasi',['id'=>$value->id,'no_prop'=>$recProposalByID->no_prop,'fase'=>$fase,'tab'=>$tab]) }}" title="hapus"><i class="fa fa-trash"></i></a></center></td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot id="items">
                                    
                                  </tfoot>
                                </table>
                                {{-- <a class="btn btn-primary" href="javascript:;" id="add">Add Field</a> --}}

                                <br><br>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan Surat Tugas Kepala {{$recProposalByID->nm_skpd}}, Nomor <input type="text" name="nomor2" value="{{$dataCka['nomor2']}}" disabled=""> 
                                tanggal
                                <select name="tanggal2" disabled="">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}" {{($dataCka['tanggal2']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                bulan
                                <select name="bulan2" disabled="">
                                    <option value="Januari" {{($dataCka['bulan2']=="Januari")?'selected':''}}>Januari</option>
                                    <option value="Februari" {{($dataCka['bulan2']=="Februari")?'selected':''}}>Februari</option>
                                    <option value="Maret" {{($dataCka['bulan2']=="Maret")?'selected':''}}>Maret</option>
                                    <option value="April" {{($dataCka['bulan2']=="April")?'selected':''}}>April</option>
                                    <option value="Mei" {{($dataCka['bulan2']=="Mei")?'selected':''}}>Mei</option>
                                    <option value="Juni" {{($dataCka['bulan2']=="Juni")?'selected':''}}>Juni</option>
                                    <option value="Juli" {{($dataCka['bulan2']=="Juli")?'selected':''}}>Juli</option>
                                    <option value="Agustus" {{($dataCka['bulan2']=="Agustus")?'selected':''}}>Agustus</option>
                                    <option value="September" {{($dataCka['bulan2']=="September")?'selected':''}}>September</option>
                                    <option value="Oktober" {{($dataCka['bulan2']=="Oktober")?'selected':''}}>Oktober</option>
                                    <option value="November" {{($dataCka['bulan2']=="November")?'selected':''}}>November</option>
                                    <option value="Desember" {{($dataCka['bulan2']=="Desember")?'selected':''}}>Desember</option>
                                </select>
                                tahun
                                <select name="tahun2" disabled="">
                                    @for ($i = 2016; $i <= 2020; $i++)
                                        <option value="{{$i}}" {{($dataCka['tahun2']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select> telah melakukan penelitian administrasi terhadap:</p>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama(Lembaga)</td>
                                        <td>:</td>
                                        <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
                                    </tr>
                                    <tr>
                                        <td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat(Lembaga)</td>
                                        <td>:</td>
                                        <td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
                                    </tr>
                                </table>
                                <br>
                                <p>Dengan hasil sebagai berikut :</p>
                                <table  border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="padding: 10px;"><center>No</center></td>
                                        <td style="padding: 10px;"><center>Data Administrasi</center></td>
                                        <td style="padding: 10px;"><center>Ada</center></td>
                                        <td style="padding: 10px;"><center>Tidak Ada</center></td>
                                        <td style="padding: 10px;"><center>Keterangan</center></td>
                                    </tr>
                                    <tr>
                                        <td><center>1</center></td>
                                        <td style="padding-left: 10px;">Nama dan Identitas</td>
                                        <td><center>@if($dataKal['cek_ktp_ketua']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_ktp_ketua']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_ktp_ketua'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>2</center></td>
                                        <td style="padding-left: 10px;">Alamat</td>
                                        <td><center>@if($dataKal['cek_alamat_lembaga']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_alamat_lembaga']==0)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_alamat_lembaga'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>3</center></td>
                                        <td style="padding-left: 10px;">Aktivitas</td>
                                        <td><center>@if($dataKap['cek_aktivitas']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKap['cek_aktivitas']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_aktivitas'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>4</center></td>
                                        <td style="padding-left: 10px;">Kepengurusan</td>
                                        <td><center>@if($dataKap['cek_kepengurusan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKap['cek_kepengurusan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_kepengurusan'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>5</center></td>
                                        <td style="padding-left: 10px;">Rencana Anggaran Biaya</td>
                                        <td><center>@if($dataKap['cek_rab']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKap['cek_rab']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_rab'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>6</center></td>
                                        <td style="padding-left: 10px;">Saldo akhir tahun lalu beserta rekening Bank</td>
                                        <td><center>@if($dataKal['cek_rekening_lembaga']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_rekening_lembaga']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_rekening_lembaga'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>7</center></td>
                                        <td style="padding-left: 10px;">Waktu pelaksanaan</td>
                                        <td><center>@if($dataKap['cek_waktu_pelaksanaan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKap['cek_waktu_pelaksanaan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_waktu_pelaksanaan'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>8</center></td>
                                        <td style="padding-left: 10px;">Bantuan yang pernah diterima 1(satu) tahun sebelumnya apabila ada</td>
                                        <td><center>@if($dataKal['cek_bantuan_tahun_sebelum']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_bantuan_tahun_sebelum']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_bantuan_tahun_sebelum'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>9</center></td>
                                        <td style="padding-left: 10px;">Nomor Pokok Wajib Pajak (aslinya)*</td>
                                        <td><center>@if($dataKal['cek_npwp']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_npwp']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_npwp'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>10</center></td>
                                        <td style="padding-left: 10px;">Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau pembentukan Organisasi/Lembaga (aslinya)*</td>
                                        <td><center>@if($dataKal['cek_akta']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_akta']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_akta'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>11</center></td>
                                        <td style="padding-left: 10px;">Surat Keterangan Domisili dari Kelurahan setempat (aslinya)*</td>
                                        <td><center>@if($dataKal['cek_skd']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_skd']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_skd'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>12</center></td>
                                        <td style="padding-left: 10px;">Sertifikat Tanah/Bukti Kepemilikan Tanah atau perjanjian kontrak atau sewa gedung/bangunan (aslinya)*</td>
                                        <td><center>@if($dataKal['cek_sertifikat']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_sertifikat']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_sertifikat'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>13</center></td>
                                        <td style="padding-left: 10px;">Izin Operasioanl/tanda daftar bagi Lembaga/Yayasan dari instansi yang berwenang (aslinya)*</td>
                                        <td><center>@if($dataKal['cek_izin_operasional']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataKal['cek_izin_operasional']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_izin_operasional'] !!}</td>
                                    </tr>
                                </table>
                                <br>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian laporan ini dibuat dengan sebenarnya dan agar dapat dipergunakan sebagaimana mestinya.</p>
                                <p>Catatan:<br>*disesuikan dengan status/kedudukan kelembagaan Pemohon.</p>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                            <center>
                                                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                                            </center>
                                        </td>
                                        <td width="30%"></td>
                                        <td><center>
                                            <h4>KETUA TIM EVALUASI</h4>
                                            <br><br><br>
                                            <h4><input type="text" name="nama_ketua" value="{{$dataCka['nama_ketua']}}" disabled=""></h4>
                                            <h4>NIP <input type="text" name="nip" value="{{$dataCka['nip']}}" disabled=""></h4>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                                <hr>
                                @if($dataCka['is_generate']==0)
                                {{-- <button type="submit" class="btn green" id="btn-simpan-administrasi">Simpan</button> --}}
                                Laporan Belum Didownload Oleh SKPD
                                @else

                                Laporan Sudah Didownload Oleh SKPD
                                <br>
                                 @if(Auth::user()->email != 'bpk_dki@jakarta.go.id' 
                                    || Auth::user()->email != 'riantawidya.rwi@gmail.com'
                                    || Auth::user()->email != 'dki.inspektorat@gmail.com'
                                    || Auth::user()->email != 'sobarmuhamad68@gmail.com'
                                    || Auth::user()->email != 'ahmadsofyankgb@gmail.com'
                                    || Auth::user()->email != 'hesti.dianingrum@yahoo.co.id'
                                    )
                                <button type="submit" class="btn green" id="btn-simpan-administrasi">Buka Laporan Ini Agar Dapat Di Ubah Oleh SKPD</button>
                                @endif

                                @endif
                            </form>
                            
                            <form class="btn-pdf-administrasi-" action="{{ route('ppkd.cetak.to.pdf.kelengkapan.administrasi') }}" method="POST" style="float: right;">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="tab" value="{{ $tab }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                @if($dataCka['id'] != "")
                                <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
                                @endif
                            </form>
                            </div>

                            {{-- Berita Acara Peninjauan Lapangan --}}
                            <div class="tab-pane fade {{($tab=='peninjauan-lapangan')?'active':''}} in" id="tab_6_2">
                                <form action="{{ route('ppkd.cetak.updatePL') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                <input type="hidden" name="fase" value="{{$fase}}">
                                <input type="hidden" name="tab" value="peninjauan-lapangan">
                                <center>
                                    <h4>BERITA ACARA PENINJAUAN LAPANGAN</h4>
                                    <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_lembaga}}</h4>
                                    <h4>TAHUN ANGGARAN {{$recProposalByID->tahun_anggaran}}</h4>
                                    <h4>NOMOR <input type="text" name="nomor" value="{{$recProposalByID->no_survei_lap}}" disabled=""></h4>
                                </center>
                                <br><br>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada hari ini 
                                <select name="hari" disabled="">
                                    <option value="Senin" {{($dataCpl['hari']=='Senin')?'selected':''}}>Senin</option>
                                    <option value="Selasa" {{($dataCpl['hari']=='Selasa')?'selected':''}}>Selasa</option>
                                    <option value="Rabu" {{($dataCpl['hari']=='Rabu')?'selected':''}}>Rabu</option>
                                    <option value="Kamis" {{($dataCpl['hari']=='Kamis')?'selected':''}}>Kamis</option>
                                    <option value="Jumat" {{($dataCpl['hari']=='Jumat')?'selected':''}}>Jumat</option>
                                </select>
                                tanggal
                                <select name="tanggal1" disabled="">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}" {{($dataCpl['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                bulan
                                <select name="bulan1" disabled="">
                                    <option value="Januari" {{($dataCpl['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                    <option value="Februari" {{($dataCpl['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                    <option value="Maret" {{($dataCpl['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                    <option value="April" {{($dataCpl['bulan1']=="April")?'selected':''}}>April</option>
                                    <option value="Mei" {{($dataCpl['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                    <option value="Juni" {{($dataCpl['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                    <option value="Juli" {{($dataCpl['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                    <option value="Agustus" {{($dataCpl['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                    <option value="September" {{($dataCpl['bulan1']=="September")?'selected':''}}>September</option>
                                    <option value="Oktober" {{($dataCpl['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                    <option value="November" {{($dataCpl['bulan1']=="November")?'selected':''}}>November</option>
                                    <option value="Desember" {{($dataCpl['bulan1']=="Desember")?'selected':''}}>Desember</option>
                                </select>
                                tahun
                                <select name="tahun1" disabled="">
                                    @for ($i = 2016; $i <= 2020; $i++)
                                        <option value="{{$i}}" {{($dataCpl['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                yang bertanda tangan di bawah ini :</p>
                                <table border="1" width="100%" cellspacing="0" cellpadding="0">
                                  <thead>
                                    <tr>
                                      <th style="padding: 10px;"><center>No</center></th>
                                      <th style="padding: 10px;"><center>Nama</center></th>
                                      <th style="padding: 10px;"><center>Jabatan Dalam Tim</center></th>  
                                      <th style="padding: 10px;"><center>Tanda Tangan</center></th>
                                      <th style="padding: 10px;"><center></center></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($dataDcpl as $key=>$value)
                                    <tr>
                                      <td style="padding: 10px;"><center>{{ $value->no_urut }}</center></td>
                                      <td style="padding: 10px;">{{ $value->nama }}</td>
                                      <td style="padding: 10px;">{{ $value->jabatan }}</td>  
                                      <td style="padding: 10px;"></td>
                                      <td style="padding: 10px;"><center><a href="{{ route('skpd.cetak.delete.detail.peserta.peninjauan.lapangan',['id'=>$value->id,'no_prop'=>$recProposalByID->no_prop,'fase'=>$fase,'tab'=>$tab]) }}" title="hapus"><i class="fa fa-trash"></i></a></center></td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot id="items-peninjauan">
                                    
                                  </tfoot>
                                </table>
                                
                                {{-- <a class="btn btn-primary" href="javascript:;" id="add-peserta-peninjauan">Add Field</a> --}}
                                
                                <br><br>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan Surat Tugas Kepala {{$recProposalByID->nm_skpd}}, Nomor <input type="text" name="nomor2" value="{{$dataCpl['nomor2']}}" disabled=""> 
                                tanggal
                                <select name="tanggal2" disabled="">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}" {{($dataCpl['tanggal2']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                bulan
                                <select name="bulan2" disabled="">
                                    <option value="Januari" {{($dataCpl['bulan2']=="Januari")?'selected':''}}>Januari</option>
                                    <option value="Februari" {{($dataCpl['bulan2']=="Februari")?'selected':''}}>Februari</option>
                                    <option value="Maret" {{($dataCpl['bulan2']=="Maret")?'selected':''}}>Maret</option>
                                    <option value="April" {{($dataCpl['bulan2']=="April")?'selected':''}}>April</option>
                                    <option value="Mei" {{($dataCpl['bulan2']=="Mei")?'selected':''}}>Mei</option>
                                    <option value="Juni" {{($dataCpl['bulan2']=="Juni")?'selected':''}}>Juni</option>
                                    <option value="Juli" {{($dataCpl['bulan2']=="Juli")?'selected':''}}>Juli</option>
                                    <option value="Agustus" {{($dataCpl['bulan2']=="Agustus")?'selected':''}}>Agustus</option>
                                    <option value="September" {{($dataCpl['bulan2']=="September")?'selected':''}}>September</option>
                                    <option value="Oktober" {{($dataCpl['bulan2']=="Oktober")?'selected':''}}>Oktober</option>
                                    <option value="November" {{($dataCpl['bulan2']=="November")?'selected':''}}>November</option>
                                    <option value="Desember" {{($dataCpl['bulan2']=="Desember")?'selected':''}}>Desember</option>
                                </select>
                                tahun
                                <select name="tahun2" disabled="">
                                    @for ($i = 2016; $i <= 2020; $i++)
                                        <option value="{{$i}}" {{($dataCpl['tahun2']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select> telah peninjauan lapangan terhadap:</p>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama(Lembaga)</td>
                                        <td>:</td>
                                        <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
                                    </tr>
                                    <tr>
                                        <td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat(Lembaga)</td>
                                        <td>:</td>
                                        <td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
                                    </tr>
                                </table>
                                <br>
                                <p>Dengan hasil sebagai berikut :</p>
                                <table  border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="padding: 10px;"><center>No</center></td>
                                        <td style="padding: 10px;"><center>Data Lapangan</center></td>
                                        <td style="padding: 10px;"><center>Sesuai</center></td>
                                        <td style="padding: 10px;"><center>Tidak Sesuai</center></td>
                                        <td style="padding: 10px;"><center>Keterangan</center></td>
                                    </tr>
                                    <tr>
                                        <td><center>1</center></td>
                                        <td style="padding-left: 10px;">Nama dan Identitas</td>
                                        <td><center>@if($dataPll['cek_ktp_ketua']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_ktp_ketua']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_ktp_ketua'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>2</center></td>
                                        <td style="padding-left: 10px;">Alamat</td>
                                        <td><center>@if($dataPll['cek_alamat_lembaga']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_alamat_lembaga']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_alamat_lembaga'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>3</center></td>
                                        <td style="padding-left: 10px;">Aktivitas</td>
                                        <td><center>@if($dataPlp['cek_aktivitas']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPlp['cek_aktivitas']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPlp['keterangan_cek_aktivitas'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>4</center></td>
                                        <td style="padding-left: 10px;">Kepengurusan</td>
                                        <td><center>@if($dataPlp['cek_kepengurusan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPlp['cek_kepengurusan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPlp['keterangan_cek_kepengurusan'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>5</center></td>
                                        <td style="padding-left: 10px;">Rencana Anggaran Biaya</td>
                                        <td><center>@if($dataPlp['cek_rab']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPlp['cek_rab']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPlp['keterangan_cek_rab'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>6</center></td>
                                        <td style="padding-left: 10px;">Saldo akhir tahun lalu beserta rekening Bank</td>
                                        <td><center>@if($dataPll['cek_rekening_lembaga']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_rekening_lembaga']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_rekening_lembaga'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>7</center></td>
                                        <td style="padding-left: 10px;">Waktu pelaksanaan</td>
                                        <td><center>@if($dataPlp['cek_waktu_pelaksanaan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPlp['cek_waktu_pelaksanaan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPlp['keterangan_cek_waktu_pelaksanaan'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>8</center></td>
                                        <td style="padding-left: 10px;">Bantuan yang pernah diterima 1(satu) tahun sebelumnya apabila ada</td>
                                        <td><center>@if($dataPll['cek_bantuan_tahun_sebelum']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_bantuan_tahun_sebelum']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_bantuan_tahun_sebelum'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>9</center></td>
                                        <td style="padding-left: 10px;">Nomor Pokok Wajib Pajak (aslinya)*</td>
                                        <td><center>@if($dataPll['cek_npwp']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_npwp']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_npwp'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>10</center></td>
                                        <td style="padding-left: 10px;">Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau pembentukan Organisasi/Lembaga (aslinya)*</td>
                                        <td><center>@if($dataPll['cek_akta']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_akta']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_akta'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>11</center></td>
                                        <td style="padding-left: 10px;">Surat Keterangan Domisili dari Kelurahan setempat (aslinya)*</td>
                                        <td><center>@if($dataPll['cek_skd']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_skd']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_skd'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>12</center></td>
                                        <td style="padding-left: 10px;">Sertifikat Tanah/Bukti Kepemilikan Tanah atau perjanjian kontrak atau sewa gedung/bangunan (aslinya)*</td>
                                        <td><center>@if($dataPll['cek_sertifikat']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_sertifikat']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_sertifikat'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td><center>13</center></td>
                                        <td style="padding-left: 10px;">Izin Operasioanl/tanda daftar bagi Lembaga/Yayasan dari instansi yang berwenang (aslinya)*</td>
                                        <td><center>@if($dataPll['cek_izin_operasional']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td><center>@if($dataPll['cek_izin_operasional']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                                        <td style="padding-left: 10px;">{!! $dataPll['keterangan_cek_izin_operasional'] !!}</td>
                                    </tr>
                                </table>
                                <br>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian laporan ini dibuat dengan sebenarnya dan agar dapat dipergunakan sebagaimana mestinya.</p>
                                <p>Catatan:<br>*disesuikan dengan status/kedudukan kelembagaan Pemohon.</p>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                            <center>
                                            <h4>PENGUSUL HIBAH</h4>
                                            <br><br><br>
                                            <h4><input type="text" name="nama_pengusul" value="{{$dataCpl['nama_pengusul']}}" disabled=""></h4>
                                            <h4>&nbsp;</h4>
                                            </center>
                                        </td>
                                        <td width="30%">
                                            <center>
                                                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                                            </center>
                                        </td>
                                        <td><center>
                                            <h4>KETUA TIM EVALUASI</h4>
                                            <br><br><br>
                                            <h4><input type="text" name="nama_ketua" value="{{$dataCpl['nama_ketua']}}" disabled=""></h4>
                                            <h4>NIP <input type="text" name="nip" value="{{$dataCpl['nip']}}" disabled=""></h4>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                                <hr>
                                @if($dataCpl['is_generate']==0)
                                {{-- <button type="submit" class="btn green" id="btn-simpan-administrasi">Simpan</button> --}}
                                Laporan Belum Didownload Oleh SKPD
                                @else
                                Laporan Sudah Didownload Oleh SKPD
                                <br>
                                <button type="submit" class="btn green" id="btn-simpan-administrasi">Buka Laporan Ini Agar Dapat Di Ubah Oleh SKPD</button>
                                @endif
                            </form>
                            
                            <form class="btn-pdf-lapangan-" action="{{ route('ppkd.cetak.to.pdf.peninjauan.lapangan') }}" method="POST" style="float: right;">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="tab" value="{{ $tab }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                @if($dataCpl['id'] != "")
                                <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
                                @endif
                            </form>
                            </div>

                            {{-- Bentuk Rekomendasi --}}
                            <div class="tab-pane fade {{($tab=='rekomendasi')?'active':''}} in" id="tab_6_3">
                               <form action="{{ route('ppkd.cetak.updateRekomendasi') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                <input type="hidden" name="fase" value="{{$fase}}">
                                <input type="hidden" name="tab" value="rekomendasi">
                                <center>
                                    <h4>SURAT REKOMENDASI PENGUSULAN</h4>
                                    <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
                                </center>
                                <br><br>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="50%">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>Nomor</td>
                                                    <td>:</td>
                                                    <td><input type="text" name="nomor" value="{{$dataCrekomendasi['nomor'] }}" disabled="" style="width: 80%"></td>
                                                </tr>
                                                <tr>
                                                    <td>Sifat</td>
                                                    <td>:</td>
                                                    <td><input type="text" name="sifat" value="{{$dataCrekomendasi['sifat']}}" style="width: 80%" disabled=""></td>
                                                </tr>
                                                <tr>
                                                    <td>Lampiran</td>
                                                    <td>:</td>
                                                    <td><input type="text" name="lampiran" value="{{$dataCrekomendasi['lampiran']}}" style="width: 80%" disabled=""></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top">Hal</td>
                                                    <td style="vertical-align: top;">:</td>
                                                    <td>Rekomendasi Pengusulan<br> Permohonan Hibah/Bantuan<br>
                                                    Sosial/Bantuan Keuangan</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="50%">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <center>
                                                        <select name="tanggal1" disabled="">
                                                            @for ($i = 1; $i <= 31; $i++)
                                                                <option value="{{$i}}" {{($dataCrekomendasi['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                                            @endfor
                                                        </select>
                                                        <select name="bulan1" disabled="">
                                                            <option value="Januari" {{($dataCrekomendasi['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                                            <option value="Februari" {{($dataCrekomendasi['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                                            <option value="Maret" {{($dataCrekomendasi['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                                            <option value="April" {{($dataCrekomendasi['bulan1']=="April")?'selected':''}}>April</option>
                                                            <option value="Mei" {{($dataCrekomendasi['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                                            <option value="Juni" {{($dataCrekomendasi['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                                            <option value="Juli" {{($dataCrekomendasi['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                                            <option value="Agustus" {{($dataCrekomendasi['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                                            <option value="September" {{($dataCrekomendasi['bulan1']=="September")?'selected':''}}>September</option>
                                                            <option value="Oktober" {{($dataCrekomendasi['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                                            <option value="November" {{($dataCrekomendasi['bulan1']=="November")?'selected':''}}>November</option>
                                                            <option value="Desember" {{($dataCrekomendasi['bulan1']=="Desember")?'selected':''}}>Desember</option>
                                                        </select>
                                                        <select name="tahun1" disabled="">
                                                            @for ($i = 2016; $i <= 2020; $i++)
                                                                <option value="{{$i}}" {{($dataCrekomendasi['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                                            @endfor
                                                        </select>  
                                                        </center> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px;"><center>Kepada</center></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px;">Yth. Gubernur Provinsi DKI Jakarta melalui Tim Anggaran Pemerintah Daerah</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px;">di Jakarta</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                
                                <p style="padding: 30px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan pemohon Hibah/Bantuan Sosial/Bantuan Keuangan yang di ajukan kepada Gubernur sesuai surat Nomor <input type="text" name="nomor2" value="{{$dataCrekomendasi['nomor2']}}" disabled=""> tanggal 
                                <select name="tanggal2" disabled="">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}" {{($dataCrekomendasi['tanggal2']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                <select name="bulan2" disabled="">
                                    <option value="Januari" {{($dataCrekomendasi['bulan2']=="Januari")?'selected':''}}>Januari</option>
                                    <option value="Februari" {{($dataCrekomendasi['bulan2']=="Februari")?'selected':''}}>Februari</option>
                                    <option value="Maret" {{($dataCrekomendasi['bulan2']=="Maret")?'selected':''}}>Maret</option>
                                    <option value="April" {{($dataCrekomendasi['bulan2']=="April")?'selected':''}}>April</option>
                                    <option value="Mei" {{($dataCrekomendasi['bulan2']=="Mei")?'selected':''}}>Mei</option>
                                    <option value="Juni" {{($dataCrekomendasi['bulan2']=="Juni")?'selected':''}}>Juni</option>
                                    <option value="Juli" {{($dataCrekomendasi['bulan2']=="Juli")?'selected':''}}>Juli</option>
                                    <option value="Agustus" {{($dataCrekomendasi['bulan2']=="Agustus")?'selected':''}}>Agustus</option>
                                    <option value="September" {{($dataCrekomendasi['bulan2']=="September")?'selected':''}}>September</option>
                                    <option value="Oktober" {{($dataCrekomendasi['bulan2']=="Oktober")?'selected':''}}>Oktober</option>
                                    <option value="November" {{($dataCrekomendasi['bulan2']=="November")?'selected':''}}>November</option>
                                    <option value="Desember" {{($dataCrekomendasi['bulan2']=="Desember")?'selected':''}}>Desember</option>
                                </select>
                                <select name="tahun2" disabled="">
                                    @for ($i = 2016; $i <= 2020; $i++)
                                        <option value="{{$i}}" {{($dataCrekomendasi['tahun2']==$i)?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                                hal <input type="text" name="hal" value="{{$dataCrekomendasi['hal']}}" disabled=""> dan setelah dilakukan penelitian administrasi dan peninjauan lapangan, dengan ini direkomendasikan kepada : 
                                <table width="100%">
                                    <tr>
                                        <td width="190" style="padding-left: 30px;">Nama</td>
                                        <td>:</td>
                                        <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
                                    </tr>
                                    <tr>
                                        <td width="190" style="padding-left: 30px;">Alamat</td>
                                        <td>:</td>
                                        <td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
                                    </tr>
                                    <tr>
                                        <td width="190" style="padding-left: 30px;">Sebesar</td>
                                        <td>:</td>
                                        <td>Rp. {{ number_format($recProposalByID->nominal_rekomendasi,2,',','.') }}</td>
                                    </tr>
                                </table>
                                <br>
                                <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk dapat diproses lebih lanjut sesuai ketentuan peraturan perundang-undangan.</p>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                            <center>
                                                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                                            </center>
                                        </td>
                                        <td width="30%"></td>
                                        <td><center>
                                            <h4>KEPALA SKPD/UKPD</h4>
                                            <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
                                            <br><br>
                                            <h4><input type="text" name="nama_kepala" value="{{$dataCrekomendasi['nama_kepala']}}" disabled=""></h4>
                                            <h4>NIP <input type="text" name="nip" value="{{$dataCrekomendasi['nip']}}" disabled=""></h4>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                    Tembusan : 
                                    <ol>
                                        <li>Sekretaris Daerah Provinsi DKI Jakarta</li>
                                        <li>Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta</li>
                                        <li>Kepala Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta</li>
                                    </ol>
                                </p>
                                <hr>
                                @if($dataCrekomendasi['is_generate']==0)
                                {{-- <button type="submit" class="btn green" id="btn-simpan-administrasi">Simpan</button> --}}
                                Laporan Belum Didownload Oleh SKPD
                                @else
                                Laporan Sudah Didownload Oleh SKPD
                                <br>
                                <button type="submit" class="btn green" id="btn-simpan-administrasi">Buka Laporan Ini Agar Dapat Di Ubah Oleh SKPD</button>
                                @endif
                            </form>
                            
                            <form class="btn-pdf-rekomendasi-" action="{{ route('ppkd.cetak.to.pdf.rekomendasi') }}" method="POST" style="float: right;">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="tab" value="{{ $tab }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                @if($dataCrekomendasi['id'] != "")
                                <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
                                @endif
                            </form>
                            </div>

                            {{-- NPHD --}}
                            <div class="tab-pane fade {{($tab=='nphd')?'active':''}} in" id="tab_6_5">            
                                <div class="row">
                                    <div class="col-md-12">
                                        @if($dataCNPHD['id'])
                                        <a href="{{ route('ppkd.cetak.naskah.perjanjian.nphd.generateNPHDToPDF',['id'=>$recProposalByID->no_prop]) }}" class="btn red confirmLink"><i class="fa fa-download" aria-hidden="true"></i> Unduh Naskah Perjanjian NPHD</a>
                                        <a href="{{ route('ppkd.cetak.naskah.perjanjian.nphd.bukafituredit',['id'=>$recProposalByID->no_prop,'fase'=>$fase,'tab'=>'nphd']) }}" class="btn green"> Buka Laporan Ini Agar Dapat Di Ubah Oleh Lembaga</a>
                                        @endif
                                    </div>
                                </div>

                                <form action="" method="post">
                                    <center>
                                        <h4>PERJANJIAN <br><br> 
                                            ANTARA <br><br>
                                            PEMERINTAH PROVINSI DAERAH KHUSUS <br><br> 
                                            IBUKOTA JAKARTA <br><br>
                                            DAN <br><br>
                                            {{ strtoupper($recProposalByID->nm_lembaga) }} <br><br> 
                                            TENTANG <br><br>
                                            PEMBERIAN HIBAH DALAM BENTUK UANG
                                        </h4>
                                    </center>
                                    <p style="padding: 30px 0 0 0;">
                                    Pada hari ini ...................................
                                    <select name="hari1" style="display: none;">
                                        <option value="Senin" {{($dataCNPHD['hari1']=='Senin')?'selected':''}}>Senin</option>
                                        <option value="Selasa" {{($dataCNPHD['hari1']=='Selasa')?'selected':''}}>Selasa</option>
                                        <option value="Rabu" {{($dataCNPHD['hari1']=='Rabu')?'selected':''}}>Rabu</option>
                                        <option value="Kamis" {{($dataCNPHD['hari1']=='Kamis')?'selected':''}}>Kamis</option>
                                        <option value="Jumat" {{($dataCNPHD['hari1']=='Jumat')?'selected':''}}>Jumat</option>
                                    </select>
                                    tanggal .........
                                    <select name="tanggal1" style="display: none;">
                                        @for ($i = 1; $i <= 31; $i++)
                                            <option value="{{$i}}" {{($dataCNPHD['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    bulan ............................................
                                    <select name="bulan1" style="display: none;">
                                        <option value="Januari" {{($dataCNPHD['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                        <option value="Februari" {{($dataCNPHD['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                        <option value="Maret" {{($dataCNPHD['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                        <option value="April" {{($dataCNPHD['bulan1']=="April")?'selected':''}}>April</option>
                                        <option value="Mei" {{($dataCNPHD['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                        <option value="Juni" {{($dataCNPHD['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                        <option value="Juli" {{($dataCNPHD['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                        <option value="Agustus" {{($dataCNPHD['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                        <option value="September" {{($dataCNPHD['bulan1']=="September")?'selected':''}}>September</option>
                                        <option value="Oktober" {{($dataCNPHD['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                        <option value="November" {{($dataCNPHD['bulan1']=="November")?'selected':''}}>November</option>
                                        <option value="Desember" {{($dataCNPHD['bulan1']=="Desember")?'selected':''}}>Desember</option>
                                    </select>
                                    tahun .................
                                    <select name="tahun1" style="display: none;">
                                        @for ($i = 2016; $i <= 2020; $i++)
                                            <option value="{{$i}}" {{($dataCNPHD['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select> 
                                    yang bertanda tangan di bawah ini: 
                                    
                                    <table width="100%">
                                        <tr>
                                            <td>I.</td>
                                            <td style="padding: 0 0 0 10px;">Nama : <input type="text" name="nama1" {{-- value="{{$dataCNPHD['nama1']}}" --}} value="Yanni Suryani, SE" disabled=""></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="padding: 0 0 0 10px;">Dalam Jabatan Kepala Bidang Perbendaharaan dan Kas Daerah Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta selaku Kuasa Bendahara Umum Daerah berkantor di Jln. Medan Merdeka Selatan No. 8-9, Kota Administrasi Jakarta Pusat;<br>
                                            Oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah Provinsi DKI Jakarta, untuk selanjutnya disebut PIHAK PERTAMA</td>
                                        </tr>
                                        <tr>
                                            <td>II.</td>
                                            <td style="padding: 0 0 0 10px;">Nama : <input type="text" name="nama2" value="{{$dataCNPHD['nama2']}}" disabled=""></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="padding: 0 0 0 10px;">Jabatan <input type="text" name="jabatan1" value="{{$dataCNPHD['jabatan1']}}" disabled=""> berkantor di Jln. <input type="text" name="alamat" value="{{$dataCNPHD['alamat']}}" disabled=""> dalam hal ini menjalani jabatannya sesuai <textarea name="deskripsi_jabatan" rows="10" style="width: 100%" disabled="">{{$dataCNPHD['deskripsi_jabatan']}}</textarea>, oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah/Pemerintah Daerah/Kelompok Masyarakat/Organisasi Kemasyarakatan untuk selanjutnya disebut PIHAK KEDUA.</td>
                                        </tr>
                                    </table>

                                    <p style="padding: 10px 0 0 10px;">Bahwa masing-masing pihak bertindak dalam jabatannya sebagaimana tersebut di atas, secara bersama-sama disebut PARA PIHAK dengan terlebih dahulu memperhatikan ketentuan sebagai berikut :</p>
                                    <table width="100%">
                                        <tr>
                                            <td style="vertical-align: top;">1.</td>
                                            <td style="padding: 0 0 0 10px;">Undang-Undang  Nomor 29 Tahun 2007 tentang Pemerintahan Provinsi Daerah Khusus Ibukota Jakarta sebagai Ibukota Negara Kesatuan Republik Indonesia;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">2.</td>
                                            <td style="padding: 0 0 0 10px;">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Undang-Undang Nomor 9 Tahun 2015;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">3.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan Pemerintah Nomor 58 Tahun 2005 tentang Pengelolaan Keuangan Daerah;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">4.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan Menteri Dalam Negeri Nomor 13 Tahun 2006 tentang Pedoman Pengelolaan 
                            Keuangan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Peraturan Menteri Dalam Negeri 
                            Nomor 21 Tahun 2011;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">5.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan Daerah Nomor 5 Tahun 2007 tentang Pokok-pokok Pengelolaan Keuangan Daerah;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">6.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan Daerah Nomor 5 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Provinsi Daerah Khusus Ibukota Jakarta;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">7.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan <input type="text" name="peraturan4" value="{{$dataCNPHD['peraturan4']}}" disabled=""> Nomor <input type="text" name="nomor4" value="{{$dataCNPHD['nomor4']}}" disabled=""> Tahun 
                                            <select name="tahun4" disabled="">
                                                @for ($i = 2016; $i <= 2020; $i++)
                                                    <option value="{{$i}}" {{($dataCNPHD['tahun4']==$i)?'selected':''}}>{{$i}}</option>
                                                @endfor
                                                {{-- <option value="2016">2016</option> --}}
                                            </select>
                                            tentang <textarea name="tentang4" style="width: 100%;" disabled="">{{$dataCNPHD['tentang4']}}</textarea>;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">8.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan Gubernur Nomor 142 Tahun 2013 tentang Sistem dan Prosedur Pengelolaan Keuangan Daerah sebagaimana telah diubah dengan Peraturan Gubernur Nomor 161 Tahun 2014;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">9.</td>
                                            <td style="padding: 0 0 0 10px;">Peraturan Gubernur Provinsi DKI Jakarta Nomor <input type="text" name="nomor5" {{-- value="{{$dataCNPHD['nomor5']}}" --}} value="55" disabled="">
                                            Tahun 
                                            <select name="tahun5" disabled="">
                                                {{-- @for ($i = 2016; $i <= 2020; $i++)
                                                    <option value="{{$i}}" {{($dataCNPHD['tahun5']==$i)?'selected':''}}>{{$i}}</option>
                                                @endfor --}}
                                                <option value="2013">2013</option>
                                            </select> 
                                            tentang Tata Cara Pengusulan, Evaluasi, Penganggaran, Pelaksanaan, Penatausahaan, Pertanggungjawaban, Pelaporan dan  Monitoring Hibah, Bantuan Sosial dan Bantuan Keuangan yang Bersumber dari Anggaran Pendapatan dan Belanja Daerah;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">10.</td>
                                            <td style="padding: 0 0 0 10px;">Keputusan  Gubernur    Nomor <input type="text" name="nomor6" value="{{$dataCNPHD['nomor6']}}" disabled=""> 
                                            Tahun
                                            <select name="tahun6" disabled="">
                                                @for ($i = 2016; $i <= 2020; $i++)
                                                    <option value="{{$i}}" {{($dataCNPHD['tahun6']==$i)?'selected':''}}>{{$i}}</option>
                                                @endfor
                                            </select> 
                                            tentang <textarea name="tentang6" style="width: 100%;" disabled="">{{$dataCNPHD['tentang6']}}</textarea>;</td>
                                        </tr>
                                    </table>
                                    
                                    <p style="padding: 0 0 0 0;">Bahwa berdasarkan hal tersebut dan sesuai dengan rekomendasi {{ $recProposalByID->nm_skpd }} Nomor <input type="text" name="nomor7" value="{{$dataCNPHD['nomor7']}}" disabled=""> tanggal 
                                    <select name="tanggal7" disabled="">
                                        @for ($i = 1; $i <= 31; $i++)
                                            <option value="{{$i}}" {{($dataCNPHD['tanggal7']==$i)?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select> 
                                    bulan
                                    <select name="bulan7" disabled="">
                                        <option value="Januari" {{($dataCNPHD['bulan7']=="Januari")?'selected':''}}>Januari</option>
                                        <option value="Februari" {{($dataCNPHD['bulan7']=="Februari")?'selected':''}}>Februari</option>
                                        <option value="Maret" {{($dataCNPHD['bulan7']=="Maret")?'selected':''}}>Maret</option>
                                        <option value="April" {{($dataCNPHD['bulan7']=="April")?'selected':''}}>April</option>
                                        <option value="Mei" {{($dataCNPHD['bulan7']=="Mei")?'selected':''}}>Mei</option>
                                        <option value="Juni" {{($dataCNPHD['bulan7']=="Juni")?'selected':''}}>Juni</option>
                                        <option value="Juli" {{($dataCNPHD['bulan7']=="Juli")?'selected':''}}>Juli</option>
                                        <option value="Agustus" {{($dataCNPHD['bulan7']=="Agustus")?'selected':''}}>Agustus</option>
                                        <option value="September" {{($dataCNPHD['bulan7']=="September")?'selected':''}}>September</option>
                                        <option value="Oktober" {{($dataCNPHD['bulan7']=="Oktober")?'selected':''}}>Oktober</option>
                                        <option value="November" {{($dataCNPHD['bulan7']=="November")?'selected':''}}>November</option>
                                        <option value="Desember" {{($dataCNPHD['bulan7']=="Desember")?'selected':''}}>Desember</option>
                                    </select>
                                    tahun
                                    <select name="tahun7" disabled="">
                                        @for ($i = 2016; $i <= 2020; $i++)
                                            <option value="{{$i}}" {{($dataCNPHD['tahun7']==$i)?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select> ,
                                    PARA PIHAK sepakat untuk melakukan Perjanjian Hibah dalam bentuk uang, dengan syarat dan ketentuan sebagai berikut :</p>
                                    
                                    <center>
                                        Pasal 1
                                    </center>
                                    <p style="padding: 0 0 0 0;">PIHAK PERTAMA memberikan Hibah berupa uang melalui transfer dana kepada PIHAK 
                            KEDUA sebagaimana PIHAK KEDUA menerima dari PIHAK PERTAMA senilai Rp <input type="text" name="senilai_angka" value="{{$dataCNPHD['senilai_angka']}}" disabled=""> 
                            ( <input type="text" name="senilai_teks" value="{{$dataCNPHD['senilai_teks']}}" disabled=""> rupiah ) {{-- pada hari <select name="hari2">
                                        <option value="Senin" {{($dataCNPHD['hari2']=='Senin')?'selected':''}}>Senin</option>
                                        <option value="Selasa" {{($dataCNPHD['hari2']=='Selasa')?'selected':''}}>Selasa</option>
                                        <option value="Rabu" {{($dataCNPHD['hari2']=='Rabu')?'selected':''}}>Rabu</option>
                                        <option value="Kamis" {{($dataCNPHD['hari2']=='Kamis')?'selected':''}}>Kamis</option>
                                        <option value="Jumat" {{($dataCNPHD['hari2']=='Jumat')?'selected':''}}>Jumat</option>
                                    </select> tanggal <select name="tanggal3">
                                        @for ($i = 1; $i <= 31; $i++)
                                            <option value="{{$i}}" {{($dataCNPHD['tanggal3']==$i)?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select> --}} untuk kegiatan :</p>

                                    <table width="90%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            {{-- <td style="vertical-align: top;">a.</td> --}}
                                            <td style="padding: 0 0 0 10px;">
                                                {{-- <input type="text" name="kegiatan1" value="{{$dataCNPHD['kegiatan1']}}" style="width: 600px;"> --}}
                                                <textarea name="kegiatan1" class="wysihtml5 form-control" rows="6" disabled="">{!! $dataCNPHD['kegiatan1']!!}</textarea>
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                            <td style="vertical-align: top;">b.</td>
                                            <td style="padding: 0 200px 0 10px;"><input type="text" name="kegiatan2" value="{{$dataCNPHD['kegiatan2']}}" style="width: 600px;"></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">c.</td>
                                            <td style="padding: 0 200px 0 10px;"><input type="text" name="kegiatan3" value="{{$dataCNPHD['kegiatan3']}}" style="width: 600px;"></td>
                                        </tr> --}}
                                    </table>
                                    <br>
                                    <center>
                                        Pasal 2
                                    </center>
                                    <br>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="vertical-align: top;">(1)</td>
                                            <td style="padding: 0 0 0 10px;">Dalam rangka pemberian Hibah sebagaimana dimaksud dalam Pasal 1, PIHAK 
                            PERTAMA bertanggung jawab terhadap penyerahan Hibah yang disampaikan melalui 
                            rekening Bank PIHAK KEDUA, sedangkan PIHAK KEDUA bertanggung jawab 
                            sepenuhnya atas pelaksanaan penggunaan Hibah dimaksud.</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">(2)</td>
                                            <td style="padding: 0 0 0 10px;">PIHAK KEDUA dilarang mengalihkan dana Hibah sebagaimana dimaksud pada ayat (1) 
                            kepada pihak lain.</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">(3)</td>
                                            <td style="padding: 0 0 0 10px;">Apabila PIHAK KEDUA tidak melaksanakan kegiatan sebagaimana dimaksud dalam Pasal 
                            1 maka harus mengembalikan Hibah yang telah diterima kepada PIHAK PERTAMA 
                            melalui rekening Kas Daerah.</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <center>
                                        Pasal 3
                                    </center>
                                    <br>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="vertical-align: top;">(1)</td>
                                            <td style="padding: 0 0 0 10px;">PIHAK KEDUA paling lambat 1 (satu) bulan setelah menyelesaikan pekerjaan atau 
                            paling lambat tanggal 31 Desember tahun anggaran berjalan, wajib menyampaikan 
                            laporan kegiatan dan keuangan sesuai ketentuan peraturan perundang-undangan 
                            kepada PIHAK PERTAMA.</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">(2)</td>
                                            <td style="padding: 0 0 0 10px;">Laporan keuangan sebagaimana dimaksud pada ayat (1) terhadap PIHAK KEDUA yang 
                            menerima belanja dengan nilai di atas Rp 200.000.000,00 (dua ratus juta rupiah) wajib 
                            dilampirkan hasil audit oleh Akuntan Publik kecuali yang telah dilakukan audit oleh 
                            Aparat Pengawas Fungsional.</td>
                                        </tr>
                                    </table>

                                    <p style="padding: 0 0 0 0;">Demikian Perjanjian Hibah ini dibuat dan ditandatangani di Jakarta pada hari dan tanggal 
                            tersebut di atas dalam rangkap 2 (dua), masing-masing bermeterai cukup dan mempunyai 
                            kekuatan hukum yang sama, 1 (satu) eksemplar untuk PIHAK PERTAMA dan 1 (satu) 
                            eksemplar untuk PIHAK KEDUA.</p>
                                    <br>
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">
                                                <center>
                                                <h4>PIHAK KEDUA</h4>
                                                <input type="text" name="jabatan2" value="{{$dataCNPHD['jabatan2']}}" disabled="">,
                                                <br><br><br>
                                                <h4><input type="text" name="nama_pihak_kedua" value="{{$dataCNPHD['nama_pihak_kedua']}}" disabled=""></h4>
                                                <h4>NIP <input type="text" name="nip_pihak_kedua" value="{{$dataCNPHD['nip_pihak_kedua']}}" disabled=""></h4>
                                                </center>
                                            </td>
                                            <td width="30%">
                                                <center>
                                                    <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                                                </center>
                                            </td>
                                            <td><center>
                                                <h4>PIHAK PERTAMA <br>KEPALA BIDANG PERBENDAHARAAN DAN KAS DAERAH<br> BADAN PENGELOLA KEUANGAN DAERAH PROVINSI DKI JAKARTA<br>selaku <br>KUASA BENDAHARA UMUM DAERAH,</h4>
                                                <br><br><br>
                                                <h4><input type="text" name="nama_kepala" {{-- value="{{$dataCNPHD['nama_kepala']}}" --}} value="YANNI SURYANI, SE" disabled=""></h4>
                                                <h4>NIP <input type="text" name="nip_kepala" {{-- value="{{$dataCNPHD['nip_kepala']}}" --}} value="196101261987032002" disabled=""></h4>
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                    
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
    <script>
        $(".btn-pdf-administrasi").on("submit", function(){
            $("#btn-simpan-administrasi").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Kelengkapan Administrasi. ");
        });

        $(".btn-pdf-lapangan").on("submit", function(){
            $("#btn-simpan-lapangan").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Peninjauan Lapangan. ");
        });

        $(".btn-pdf-rekomendasi").on("submit", function(){
            $("#btn-simpan-rekomendasi").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Surat Rekomendasi. ");
        });

        $(".btn-pdf-rekomendasi-pencairan").on("submit", function(){
            $("#btn-simpan-rekomendasi-pencairan").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Surat Rekomendasi Pencairan. ");
        });
    </script>
    <script type="text/javascript">
        $(function(){
            //when the Add Field button is clicked
            $("#add").click(function (e) {
                //Append a new row of code to the "#items" div
                $("#items").append('<tr><td><input type="number" name="no_urut[]" placeholder="No Urut" style="width:100%" required></td><td><input type="text" name="nama[]" placeholder="Masukan Nama" style="width:100%" required></td><td><input type="text" name="jabatan[]" placeholder="Masukan Jabatan" style="width:100%" required></td><td></td><td></td></tr>');
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            //when the Add Field button is clicked
            $("#add-peserta-peninjauan").click(function (e) {
                //Append a new row of code to the "#items" div
                $("#items-peninjauan").append('<tr><td><input type="number" name="no_urut[]" placeholder="No Urut" style="width:100%" required></td><td><input type="text" name="nama[]" placeholder="Masukan Nama" style="width:100%" required></td><td><input type="text" name="jabatan[]" placeholder="Masukan Jabatan" style="width:100%" required></td><td></td><td></td></tr>');
            });
        });
    </script>
@stop