@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.proposal.index') }}">Proposal</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p> Silahkan Pilih Fase Yang Sedang Berjalan.</p>
                </div>

                <div class="portlet-body">
                    <div class="mt-element-step">
                        <div class="row step-default">
                            @foreach ($recPeriode as $element)
                            <a href="{{ route('ppkd.proposal.manage',['fase'=>($element->jenisPeriodeID==1)?'penetapan':'perubahan','noper'=>$element->noper]) }}">
                                <div class="col-md-4 bg-green-dark mt-step-col" style="background: {{ $element->bg_color }} !important;">
                                    <div class="mt-step-number bg-white font-yellow"><i class="fa fa-folder"></i></div>
                                    <div class="mt-step-title uppercase bg-font-green-dark">Proposal</div>
                                    <div class="mt-step-title uppercase bg-font-green-dark"><b>{{ $element->keterangan }}</b></div>
                                </div>
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop