@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.periode.tahapan.manage') }}">Periode Tahapan</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah Periode</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Ubah Periode </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.periode.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Priode
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="noper" type="text" data-required="1" class="form-control" value="{{ $recPeriodeByID->noper }}" readonly=""> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tahun Anggaran
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input name="tahun" type="text" data-required="1" class="form-control only-numeric" value="{{ $recPeriodeByID->tahun }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tahun Proses
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input name="tahun_proses" type="text" data-required="1" class="form-control only-numeric" value="{{ $recPeriodeByID->tahun_proses }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Jenis Priode
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-3">
                                    	<select name="jenisPriodeID" class="form-control">
                                    		<option value="1" {{ ($recPeriodeByID->jenisPeriodeID==1)?'selected':'' }}>Penetapan</option>
                                    		<option value="2" {{ ($recPeriodeByID->jenisPeriodeID==2)?'selected':'' }}>Perubahan</option>
                                    	</select>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Priode
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-7">
                                        <input name="keterangan" type="text" data-required="1" class="form-control" value="{{ $recPeriodeByID->keterangan }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tgl Mulai
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="mulai" value="{{ $recPeriodeByID->mulai }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tgl Selesai
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="selesai" value="{{ $recPeriodeByID->selesai }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Color
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input type="color" name="bg_color" class="form-control" value="{{ $recPeriodeByID->bg_color }}">   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Aktif
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                    	<select name="status" class="form-control">
                                    		<option value="0" {{ ($recPeriodeByID->status==0)?'selected':'' }}>Tidak</option>
                                    		<option value="1" {{ ($recPeriodeByID->status==1)?'selected':'' }}>Ya</option>
                                    	</select>    
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Pengajuan Prop
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <select name="pengajuan_prop" class="form-control select2me">
                                            <option value="N" {{ ($recPeriodeByID->pengajuan_prop=='N')?'selected':'' }}>Tidak</option>
                                            <option value="Y" {{ ($recPeriodeByID->pengajuan_prop=='Y')?'selected':'' }}>Ya</option>
                                        </select>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Rekomendasi Anggaran
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <select name="rek_anggaran" class="form-control select2me">
                                            <option value="N" {{ ($recPeriodeByID->rek_anggaran=='N')?'selected':'' }}>Tidak</option>
                                            <option value="Y" {{ ($recPeriodeByID->rek_anggaran=='Y')?'selected':'' }}>Ya</option>
                                        </select>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Proposal Definitif
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <select name="definitif" class="form-control select2me">
                                            <option value="N" {{ ($recPeriodeByID->definitif=='N')?'selected':'' }}>Tidak</option>
                                            <option value="Y" {{ ($recPeriodeByID->definitif=='Y')?'selected':'' }}>Ya</option>
                                        </select>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Rekomendasi Kolektif
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <select name="rekomendasiKolektif" class="form-control select2me">
                                            <option value="N" {{ ($recPeriodeByID->rekomendasiKolektif=='N')?'selected':'' }}>Tidak</option>
                                            <option value="Y" {{ ($recPeriodeByID->rekomendasiKolektif=='Y')?'selected':'' }}>Ya</option>
                                        </select>    
                                    </div>
                                </div>

                            </div>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan Perubahan</button>
                                        <a href="javascript:;" onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

            {{-- <div class="col-md-12">
                <div class="note note-warning">
                    <h4 class="block"><b>Aktifkan Proposal Definitif : </b><br><br> 
                        <a href="{{ route('ppkd.migrate.proposal.to.definitif',['noper'=>$noper]) }}" class="btn green" data-method="PUT" data-confirm="Anda yakin untuk membuka fase Proposal Definitif ?">SETUJU</a>
                    </h4>
                </div>
            </div> --}}
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop

@section('javascript')
    @parent
	<script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    <script>
        window.csrfToken = '<?php echo csrf_token(); ?>';
    </script>
    <script type="text/javascript">
        (function() {
 
            var laravel = {
                initialize: function() {
                    this.registerEvents();
                },
         
                registerEvents: function() {
                    $('body').on('click', 'a[data-method]', this.handleMethod);
                },
         
                handleMethod: function(e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;
         
                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }
         
                    // Allow user to optionally provide data-confirm="Are you sure?"
                    if ( link.data('confirm') ) {
                        if ( ! laravel.verifyConfirm(link) ) {
                            return false;
                        }
                    }
         
                    form = laravel.createForm(link);
                    form.submit();
         
                    e.preventDefault();
                },
         
                verifyConfirm: function(link) {
                    return confirm(link.data('confirm'));
                },
         
                createForm: function(link) {
                    var form =
                        $('<form>', {
                            'method': 'POST',
                            'action': link.attr('href')
                        });
         
                    var token =
                        $('<input>', {
                            'name': '_token',
                            'type': 'hidden',
                            'value': window.csrfToken
                        });
         
                    var hiddenInput =
                        $('<input>', {
                            'name': '_method',
                            'type': 'hidden',
                            'value': link.data('method')
                        });
         
                    return form.append(token, hiddenInput)
                        .appendTo('body');
                }
            };
         
            laravel.initialize();
         
        })();
    </script>
@stop