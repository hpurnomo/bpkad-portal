@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.periode.tahapan.manage') }}">Periode Tahapan</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah Periode Tahapan</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        {{-- {{ dd($recDetailPeriodeTahapanByID) }} --}}
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Ubah Periode Tahapan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.periode.tahapan.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{ $id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Periode
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-7">
                                        <input name="namaPeriode" type="text" data-required="1" class="form-control" value="{{ $recDetailPeriodeTahapanByID->namaPeriode }}" readonly=""> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tahun Anggaran
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-7">
                                        <input name="tahunAnggaran" type="text" data-required="1" class="form-control" value="{{ $recDetailPeriodeTahapanByID->tahunAnggaran }}" readonly=""> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tahapan
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="name" type="text" data-required="1" class="form-control" value="{{ $recDetailPeriodeTahapanByID->name }}" readonly=""> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tgl Mulai
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="mulai" value="{{ $recDetailPeriodeTahapanByID->mulai }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tgl Selesai
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="selesai" value="{{ $recDetailPeriodeTahapanByID->selesai }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Keterangan
                                    </label>
                                    <div class="col-md-7">
                                        <input name="keterangan" type="text" data-required="1" class="form-control" value="{{ $recDetailPeriodeTahapanByID->keterangan }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Aktif
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                    	<select name="status" class="form-control select2me">
                                    		<option value="0" {{ ($recDetailPeriodeTahapanByID->status==0)?'selected':'' }}>Tidak</option>
                                    		<option value="1" {{ ($recDetailPeriodeTahapanByID->status==1)?'selected':'' }}>Ya</option>
                                    	</select>    
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Ubah</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop

@section('javascript')
    @parent
	<script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
@stop