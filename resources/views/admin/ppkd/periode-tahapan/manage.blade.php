@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.periode.tahapan.manage') }}">Periode Tahapan</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <ol>
                        <li>Untuk Tahapan, otomatis ditambahkan pada saat pembuatan Periode Baru.</li>
                        <li>Untuk mengubah status klik pada kolom Nama Periode Pengajuan.</li>
                    </ol>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('ppkd.periode.create') }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Periode Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" {{-- id="sample_3" --}}>
                            <thead>
                            	<tr>
                            		<th style="text-align: center;font-weight: bolder" rowspan="2">Nomor</th>
                            		<th style="text-align: center;font-weight: bolder" colspan="2">Tahun</th>
                            		<th style="text-align: center;font-weight: bolder" rowspan="2">Nama Periode Pengajuan </th>
                            		<th style="text-align: center;font-weight: bolder" rowspan="2">Status</th>
                            		<th style="text-align: center;font-weight: bolder" colspan="2">Tanggal</th>
                            		<th style="text-align: center;font-weight: bolder" rowspan="2">Keterangan</th>
                            	</tr>
                                <tr>
                                    <th style="text-align: center;font-weight: bolder"> Anggaran </th>
                                    <th style="text-align: center;font-weight: bolder"> Proses </th>
                                    <th style="text-align: center;font-weight: bolder"> Mulai </th>
                                    <th style="text-align: center;font-weight: bolder"> Selesai </th>
                                </tr>
                            </thead>
                            <tbody>
                            @inject('getDetailPeriodeTahapan','BPKAD\Services\DetailPeriodeTahapanService')
                            	@foreach($recPeriode as $key=>$value)
                            	<tr style="font-weight: bolder;">
                            		<td>{{ $key+1 }}</td>
                            		<td>{{ $value->tahun }}</td>
                            		<td>{{ $value->tahun_proses }}</td>
                            		<td><a href="{{ route('ppkd.periode.edit',['noper'=>$value->noper]) }}">{{ $value->keterangan }}</a></td>
                            		<td>{{ ($value->status==1)?'Aktif':'Histori' }}</td>
                            		<td>{{ date('d M-Y',strtotime($value->mulai)) }}</td>
                            		<td>{{ date('d M-Y',strtotime($value->selesai)) }}</td>
                            		<td></td>
                            	</tr>
                            	{!! $getDetailPeriodeTahapan->getByPeriodeID($value->noper) !!}
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop