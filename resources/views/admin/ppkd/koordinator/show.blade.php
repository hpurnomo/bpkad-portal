@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.skpd.koordinator.manage') }}">SKPD Koordinator</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        {{-- {{dd($recSkpdKoordinatorByKdSKPD)}} --}}
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p> Data dibawah ini merupakan koordinator untuk lembaga. <b>Klik pada nama skpd untuk melihat detil</b></p>
                </div>
                <form action="#" id="form_sample_1" class="form-horizontal" novalidate="novalidate">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Nama SKPD
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" name="name" data-required="1" class="form-control" value="{{ $recSkpdKoordinatorByKdSKPD[0]->nm_skpd }}" disabled=""> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Kode SKPD
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" name="name" data-required="1" class="form-control" value="{{ $recSkpdKoordinatorByKdSKPD[0]->kd_skpd }}" disabled=""> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Status Aktif SKPD
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-1">
                            	<select name="IsAktif" class="form-control" disabled="">
                            		<option value="0" {{ ($recSkpdKoordinatorByKdSKPD[0]->IsAktif==0)?'selected':'' }}>Tidak</option>
                            		<option value="1" {{ ($recSkpdKoordinatorByKdSKPD[0]->IsAktif==1)?'selected':'' }}>Ya</option>
                            	</select>
                            	{{-- <a href=""><i class="fa fa-pencil"></i> edit</a> --}}
                            </div>
                        </div>
                    </div>
                </form>

                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                        <a href="{{ route('ppkd.skpd.koordinator.manage') }}" class="btn grey-salsa btn-outline" style="float: right;">Kembali</a>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                	<th> No. </th>
                                	<th> Nama Lembaga </th>
                                    <th> Alamat </th>
                                    <th> NPWP </th>
                                    <th> Status Aktif </th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recSkpdKoordinatorByKdSKPD as $key=>$value)
                                <tr>
                                	<td> {{ $key+1 }} </td>
                                	<td> <a href="{{ route('ppkd.skpd.koordinator.edit',['nomor'=>$value->nomor]) }}">{{ $value->nm_lembaga }}</a> </td>
                                    <td> {{ $value->alamat }} </a></td>
                                    <td> {{ $value->npwp }}</td>
                                    <td> {{ ($value->is_verify==1)?'Ya':'Tidak' }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop