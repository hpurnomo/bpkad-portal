@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.skpd.koordinator.manage') }}">SKPD Koordinator</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                	<a href="{{ route('ppkd.skpd.koordinator.manage') }}">Daftar Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah SKPD Koordinator Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        {{-- {{dd($recLembagaByNomor)}} --}}
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	
        </h3>
        <div class="row">
            <div class="col-md-12">
            	<div class="portlet light bordered">
            		<div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">  Ubah SKPD Koordinator Lembaga</span>
                        </div>
                    </div>
            		<div class="portlet-body">
		                <form action="{{ route('ppkd.skpd.koordinator.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
		                	{!! csrf_field() !!}
		                	<input type="hidden" name="nomor" value="{{ $recLembagaByNomor->nomor }}">
		                    <div class="form-body">
		                        <div class="form-group">
		                            <label class="control-label col-md-2">Nama SKPD
		                                <span class="required" aria-required="true"> * </span>
		                            </label>
		                            <div class="col-md-6">
		                            	<select class="form-control" name="kd_skpd">
		                            	@foreach($recSKPDAll as $key=>$value)
		                            		<option value="{{ $value->kd_skpd }}" {{ ($value->kd_skpd==$recLembagaByNomor->kd_skpd)?'selected':'' }}>{{ $value->nm_skpd }}</option>
		                            	@endforeach
		                            	</select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="control-label col-md-2">Nama Lembaga
		                                <span class="required" aria-required="true"> * </span>
		                            </label>
		                            <div class="col-md-6">
		                                <input type="text" name="nm_lembaga" data-required="1" class="form-control" value="{{ $recLembagaByNomor->nm_lembaga }}" disabled=""> 
		                            </div>
		                        </div>
		                        <div class="form-actions">
	                                <div class="row">
	                                    <div class="col-md-offset-2 col-md-9">
	                                        <button type="submit" class="btn green">Ubah</button>
	                                        <a href="{{ route('ppkd.skpd.koordinator.show',['kdskpd'=>$recLembagaByNomor->kd_skpd]) }}" class="btn grey-salsa btn-outline">Kembali</a>
	                                    </div>
	                                </div>
	                            </div>
		                    </div>
		                </form>
		            </div>
	            </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop