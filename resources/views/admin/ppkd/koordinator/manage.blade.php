@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.skpd.koordinator.manage') }}">SKPD Koordinator</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        {{-- {{dd($recSkpdIsAktif)}} --}}
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p> Data dibawah ini merupakan koordinator yang aktif untuk lembaga. <b>Klik pada nama skpd untuk melihat detil</b></p>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        {{-- <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('ppkd.pengaturan.token.create') }}" class="btn sbold green"> Buat Nomor Token Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                	<th> No. </th>
                                	<th> Kode Koordinator</th>
                                    <th width="50%"> SKPD Koordinator </th>
                                    <th> Total Lembaga </th>
                                </tr>
                            </thead>
                            <tbody>
                                @inject('lembaga','BPKAD\Services\LembagaService')
                            	@foreach($recSkpdIsAktif as $key=>$value)
                                <tr>
                                	<td> {{ $key+1 }} </td>
                                	<td> {{ $value->kd_skpd }}</td>
                                    @if(Auth::user()->email == 'bpk_dki@jakarta.go.id')
                                    <td>  {{ $value->nm_skpd }}  </td>
                                    @else
                                    <td><a href="{{ route('ppkd.skpd.koordinator.show',['kdskpd'=>$value->kd_skpd]) }}"> {{ $value->nm_skpd }} </a></td> 
                                    @endif
                                    <td> {{ $lembaga->getCountByKdSKPD($value->kd_skpd) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop