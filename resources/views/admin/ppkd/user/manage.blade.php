@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.user.management.manage') }}">User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        {{-- {{dd($recUserByGroupID)}} --}}
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p> Data dibawah ini merupakan data User yang telah tersimpan didalam System Kami. User yang tampil adalah User {{ $group_name }}.<br>Untuk mengubah User {{ $group_name }}, <b>klik nama {{ $group_name }}.</b></p>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('ppkd.user.management.create',$group_id) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat User {{ $group_name }} Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
                            <thead>
                                <tr>
                                    <th> No. </th>
                                    @if($group_id==10)
                                    <th> Nama {{ $group_name }} </th>
                                    <th> NRK </th>
                                    <th> Username </th>
                                    <th> Name </th>
                                    <th> E-Mail </th>
                                    @elseif($group_id==13)
                                    <th> Username </th>
                                    <th> Name </th>
                                    <th> E-Mail </th>
                                    @endif 
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recUserByGroupID as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    @if($value->group_id==10)
                                    <td><a href="{{ route('ppkd.user.management.edit',['id'=>$value->user_id,'group_id'=>$value->group_id]) }}"> {{ $value->nm_skpd }} </a></td>
                                    <td> {{ $value->nrk }} </td>
                                    <td> {{ $value->username }}</td>
                                    <td> {{ ucwords(strtolower($value->real_name)) }} </td>
                                    <td> {{ $value->email }} </td>
                                    @elseif($value->group_id==13)
                                    <td><a href="{{ route('ppkd.user.management.edit',['id'=>$value->user_id,'group_id'=>$value->group_id]) }}"> {{ $value->username }} </a></td>
                                    <td> {{ ucwords(strtolower($value->real_name)) }} </td>
                                    <td><a href="{{ route('ppkd.user.management.edit',['id'=>$value->user_id,'group_id'=>$value->group_id]) }}"> {{ $value->email }} </a></td> 
                                    @endif 
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop