@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.user.management.manage',$group_id) }}">User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Buat User {{ $group_name }} Baru</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Buat User  {{ $group_name }} Baru</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.user.management.store') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}

                                @if($group_id=='10')
                                <div class="form-group">
                                    <label class="control-label col-md-3">SKPD
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control select2me" name="no_skpd" required>
                                            <option value="">Pilih SKPD</option>
                                            @foreach($recSkdp as $key=>$value)
                                            	<option value="{{ $value->nomor }}">{{ strtoupper($value->nm_skpd) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif

                                @if($group_id=='10')
                                <div class="form-group">
                                    <label class="control-label col-md-3">NRK
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                        <input name="nrk" type="text" class="form-control" value="{{ old('nrk') }}" id="nrk">
                                        <span class="input-group-btn">
                                            <button id="cekNrk" class="btn btn-success" type="button">
                                                <i class="fa fa-check"></i> Cek NRK </button>
                                        </span>
                                        </div>
                                    </div>
                                </div> 
                                @endif 

                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="real_name" type="text" data-required="1" class="form-control" value="{{ old('real_name') }}" readonly="true" id="nama"> </div>
                                </div>
 
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email" type="text" class="form-control" value="{{ old('email') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="password" type="password" class="form-control"> </div>
                                </div>
                            </div>

                            <input type="hidden" name="group_id" value="{{ $group_id }}">
                             @if($group_id=='13')
                            <input type="hidden" name="nrk" value="0">

                             @endif

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            $("#cekNrk").click(function(){
                var nrk = $("#nrk").val();

                if(nrk == ""){
                  alert("nrk tidak boleh kosong");
                }

                 $.ajax({
                      async: true,
                      type: "POST",
                      // url: "{{ route('ppkd.user.management.cek.nrk') }}",
                      url: "http://ehibahbansosdki.jakarta.go.id/ppkd/user-management/cekNrk",
                      dataType: "json",
                      data: { 
                             _token: "{{ csrf_token() }}",
                             nrk:nrk,   
                         },
                      success : function(res){
                        if(res.status == true){ 
                          Command: toastr["success"]("Sukeses", res.msg) 
                          toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          }
  
                          $("#nama").val(res.nama);
                        }
                        else{ 
                            Command: toastr["error"]("Error",res.msg) 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
   
                          
                        }
                      },
                      error : function(err, ajaxOptions, thrownError){
                           $("#confirm-popup").modal('hide');
                            Command: toastr["error"]("Error", "Error ") 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            } 
                      }
                    });  
            });

        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop