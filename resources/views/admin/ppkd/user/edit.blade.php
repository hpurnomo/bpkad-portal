@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.user.management.manage') }}">User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah User {{ $group_name }}</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Ubah User {{ $group_name }} </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.user.management.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                        <input type="hidden" name="user_id" value="{{ $userID }}">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}

                                @if($group_id==10)
                                <div class="form-group">
                                    <label class="control-label col-md-3">SKPD
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control select2me" name="no_skpd" required>
                                            <option value="">Pilih SKPD</option>
                                            @foreach($recSkdp as $key=>$value)
                                                <option value="{{ $value->nomor }}" {{ ($value->nomor==$recUserByID->no_skpd)?'selected':'' }}>{{ strtoupper($value->nm_skpd) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="real_name" type="text" data-required="1" class="form-control" value="{{ $recUserByID->real_name }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Username
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="username" type="text" data-required="1" class="form-control" value="{{ $recUserByID->username }}"> </div>
                                </div>

                                @if($group_id==10)
                                <div class="form-group">
                                    <label class="control-label col-md-3">NRK
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nrk" type="text" class="form-control" value="{{ $recUserByID->nrk }}" readonly="true">
                                    </div>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email" type="text" class="form-control" value="{{ $recUserByID->email }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password
                                    </label>
                                    <div class="col-md-4">
                                        <input name="password_reset" type="password" class="form-control"> </div>
                                </div>
                            </div>

                            <input type="hidden" name="group_id" value="{{ $group_id }}">

                            @if($group_id==13)
                            <input type="hidden" name="no_skpd" value="0">
                            <input type="hidden" name="nrk" value="{{ $recUserByID->user_id }}">
                            @endif

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Ubah</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop