@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Detail Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <a href="{{ route('ppkd.lembaga.manage') }}" class="btn grey-salsa btn-outline" style="float: right;">Kembali</a>
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_0" data-toggle="tab">1. Detil Lembaga </a>
                            </li>
                            <li {{-- class="{{ ($recUserLembagaByNoLembaga->is_approve==0)?'active':'' }}" --}}>
                                <a href="#tab_1_1" data-toggle="tab">2. Hasil Verifikasi Kelengkapan Administrasi </a>
                            </li>
                            <li {{-- class="{{ ($recUserLembagaByNoLembaga->is_approve==1)?'active':'' }}" --}}>
                                <a href="#tab_1_2" data-toggle="tab">3. Hasil Verifikasi Peninjauan Lapangan </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab_1_0">
                                <div class="portlet-body">
                                    @include('admin.part.alert')
                                    <form id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                            {!! csrf_field() !!}
                                            <input type="hidden" name="nomor" value="{{ $recLembagaByNomor->nomor }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <img src="{{url('/')}}/assets/images/default-logo-lembaga.jpg" class="img-responsive" width="140">
                                                    </div>
                                                    <div class="col-md-10">
                                                        <h3 style="height: 140px;padding-top: 33px;font-weight: bolder;">{{ $recLembagaByNomor->nm_lembaga }} </h3>
                                                    </div>
                                                </div>
                                            </div>

                                            <h3 class="form-section">Data Utama</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Alamat :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->alamat }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>NPWP :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->npwp }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>RT/RW :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->noRT }}/{{ $recLembagaByNomor->noRW }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan NPWP :</b></label>
                                                        <div class="col-md-6">
                                                            @if(!empty($recLembagaByNomor->scanNPWP))
                                                            <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->scanNPWP }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->scanNPWP,15) }}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Kelurahan :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->nmkel }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>EMail :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->email }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Kecamatan :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>No Telp :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_telepon }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Kota :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>No Fax :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_fax }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h3 class="form-section">Data Khusus</h3>
                                            <hr>
                                            <h4><u>Akta Kumham</u></h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nomor :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_akta }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Tanggal :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->tgl_akta }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan Dokumen :</b></label>
                                                        <div class="col-md-6">
                                                            @if(!empty($recLembagaByNomor->akta))
                                                            <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->akta }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->akta,15) }}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h4><u>Sertifikat</u></h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nomor :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_sertifikat }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Tanggal :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->tgl_sertifikat }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan Dokumen :</b></label>
                                                        <div class="col-md-6">
                                                            @if(!empty($recLembagaByNomor->sertifikat))
                                                            <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->sertifikat }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->sertifikat,15) }}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h4><u>Surat Domisili</u></h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nomor :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_surat_domisili }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Tanggal :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->tgl_surat_domisili }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan Dokumen :</b></label>
                                                        <div class="col-md-6">
                                                            @if(!empty($recLembagaByNomor->skd))
                                                            <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->skd }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->skd,15) }}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h4><u>Izin Operasional</u></h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nomor :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_izin_operasional }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Tanggal :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->tgl_izin_operasional }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan Dokumen :</b></label>
                                                        <div class="col-md-6">
                                                            @if(!empty($recLembagaByNomor->izin_operasional))
                                                            <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->izin_operasional }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->izin_operasional,15) }}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h3 class="form-section">Rekening Lembaga</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nama Bank :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->nama_bank }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Atas Nama :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->pemilik_rek }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>No Rekening :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_rek }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan Rekening :</b></label>
                                                        <div class="col-md-6">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h3 class="form-section">Kontak Pengurus</h3>
                                            <hr>
                                            <h4><u>Ketua</u></h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nama :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->kontak_person }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>NIK :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->nik_person }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Scan KTP :</b></label>
                                                        <div class="col-md-6">
                                                            @if(!empty($recLembagaByNomor->ktp_ketua))
                                                            <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->ktp_ketua }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->ktp_ketua,15) }}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Alamat :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->alamat_person }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>No Telp/HP :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_hp_person }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>EMail :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->email_person }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Photo :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h4><u>Bendahara</u></h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nama :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->kontak_person_bendahara }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>EMail :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->email_person_bendahara }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Alamat :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->alamat_person_bendahara }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>No Telp/HP :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_hp_person_bendahara }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <h4><u>Sekretaris</u></h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nama :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->kontak_person_sekretaris }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>EMail :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->email_person_sekretaris }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Alamat :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->alamat_person_sekretaris }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>No Telp/HP :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $recLembagaByNomor->no_hp_person_sekretaris }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="portlet light bordered">
                                                <div class="portlet-body form">
                                                    <h3>Lokasi Lembaga
                                                    @if($recLembagaByNomor->latitude==NULL || $recLembagaByNomor->latitude==0)
                                                    <b style="color: red;">Belum Tersedia</b>
                                                    @endif
                                                    </h3>
                                                    <hr>
                                                    <div class="input-group">
                                                        <input type="hidden" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                    </div>
                                                    <div id="gmap_basic" class="gmaps"> </div>
                                                    <b>Latitude : </b>
                                                    <input type="text" class="form-control" name="latitude" id="latVal" value="{{ $recLembagaByNomor->latitude }}">
                                                    <b>Longitude : </b>
                                                    <input type="text" class="form-control" name="longitude" id="longVal" value="{{ $recLembagaByNomor->longitude }}">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        {{-- <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <a href="{{ route('ppkd.lembaga.manage') }}" class="btn grey-salsa btn-outline">Kembali</a>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </form>
                                </div>
                            </div>

                            {{-- Kelengkapan Administrasi --}}
                            <div class="tab-pane fade" id="tab_1_1">
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Administrasi</center></th>
                                                            <th><center>Ada</center></th>
                                                            <th><center>Tidak Ada</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Nama dan Identitas</td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_ktp_ketua==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_ktp_ketua==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_ktp_ketua !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Alamat</td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_alamat_lembaga==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_alamat_lembaga==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_alamat_lembaga !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Saldo akhir tahun lalu beserta rekening Bank</td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_rekening_lembaga==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_rekening_lembaga==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_rekening_lembaga !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila ada</td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_bantuan_tahun_sebelum==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_bantuan_tahun_sebelum==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_bantuan_tahun_sebelum !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>Nomor Pokok Wajib Pajak <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_npwp==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_npwp==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_npwp !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td>Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_akta==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_akta==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_akta !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>Surat Keterangan Domisili dari Kelurahan setempat <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_skd==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_skd==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_skd !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>8</td>
                                                            <td>Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_sertifikat==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_sertifikat==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_sertifikat !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>9</td>
                                                            <td>Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_izin_operasional==1)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($recUserLembagaByNoLembaga->cek_izin_operasional==2)
                                                                    <center><i class="fa fa-check-square-o"></i></center>
                                                                @endif
                                                            </td>
                                                            <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_izin_operasional !!}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="checkbox" name="is_approve" {{ ($recUserLembagaByNoLembaga->is_approve==1)?'checked':'' }} disabled=""> <span style="color:red;font-weight: bolder;">Dengan ini Dinyatakan Lembaga Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                </div>
                                </form>
                            </div>
                            
                            {{-- Peninjauan Lapangan --}}
                            <div class="tab-pane fade" id="tab_1_2">
                                @if($recUserLembagaByNoLembaga->is_approve==0)
                                    <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                                @else
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="{{ route('skpd.lembaga.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="no_lembaga" value="{{ $recUserLembagaByNoLembaga->no_lembaga }}">
                                        <input type="hidden" name="email" value="{{ $recLembagaByNomor->email }}">
                                        <input type="hidden" name="nm_lembaga" value="{{ $recLembagaByNomor->nm_lembaga }}">
                                        <input type="hidden" name="nm_skpd" value="{{ $recLembagaByNomor->nm_skpd }}">
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Lapangan</center></th>
                                                            <th><center>Sesuai</center></th>
                                                            <th><center>Tidak Sesuai</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Nama dan Identitas</td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_ktp_ketua==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_ktp_ketua==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>{!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_ktp_ketua !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Alamat</td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_alamat_lembaga==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_alamat_lembaga==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>{!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_alamat_lembaga !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Saldo akhir tahun lalu beserta rekening Bank</td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_rekening_lembaga==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_rekening_lembaga==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_rekening_lembaga !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila ada</td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_bantuan_tahun_sebelum==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_bantuan_tahun_sebelum==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_bantuan_tahun_sebelum !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>Nomor Pokok Wajib Pajak <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_npwp==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_npwp==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_npwp !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td>Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_akta==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_akta==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_akta !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>Surat Keterangan Domisili dari Kelurahan setempat <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_skd==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_skd==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_skd !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>8</td>
                                                            <td>Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_sertifikat==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_sertifikat==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_sertifikat !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>9</td>
                                                            <td>Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang <span style="color:red;">(aslinya)*</span></td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_izin_operasional==1)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_izin_operasional==2)?"<center><i class=\"fa fa-check-square-o\"></i></center>":'' !!}
                                                            </td>
                                                            <td>
                                                                {!! (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_izin_operasional !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="checkbox" name="is_approve" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->is_approve==1)?'checked':'' }} disabled=""> <span style="color:red;font-weight: bolder;">Dengan ini Dinyatakan Lembaga Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Berita Acara Peninjauan Lapangan.</span>
                                        </center>
                                    </div>
                                </div>
                                </form>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = {{ ($recLembagaByNomor->latitude==NULL)?'0':$recLembagaByNomor->latitude }}; 
            var longValue = {{ ($recLembagaByNomor->longitude==NULL)?'0':$recLembagaByNomor->longitude }}; 
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng({{ ($recLembagaByNomor->latitude==NULL)?'0':$recLembagaByNomor->latitude }},{{ ($recLembagaByNomor->longitude==NULL)?'0':$recLembagaByNomor->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
@stop