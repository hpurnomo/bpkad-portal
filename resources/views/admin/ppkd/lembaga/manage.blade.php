@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p><strong>Klik pada nama lembaga untuk melihat detil lembaga</strong></p>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        {{-- <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('ppkd.lembaga.create') }}" class="btn sbold green"> Buat Lembaga Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th width="10%"> No. </th>
                                    <th> Nama Lembaga </th>
                                    <th> Kode Lembaga </th>
                                    <th> Nama SKPD </th>
                                    <th> NPWP </th>
                                    <th> Alamat </th>
                                    <th> Tgl Input </th>
                                    <th> Verified </th>
                                    <th> Tgl Verified </th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recAllLembaga as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    @if(Auth::user()->email == 'bpk_dki@jakarta.go.id') 
                                    <td>{{ ucwords(strtolower($value->nm_lembaga)) }}</td>
                                    @else
                                    <td><a href="{{ route('ppkd.lembaga.show',['nomor'=>$value->nomor]) }}">{{ ucwords(strtolower($value->nm_lembaga)) }}</a></td>
                                    @endif 
                                    <td> {{ $value->kd_skpd }}.{{ $value->nomor }} </td>
                                    <td> {{ strtoupper($value->nm_skpd) }} </td>
                                    <td> {{ $value->npwp }} </td>
                                    <td> {{ ucwords(strtolower($value->alamat)) }} </td>
                                    <td> {{ date('d-M-Y',strtotime($value->crd)) }} </td>
                                    <td>
                                        {!! ($value->is_verify==1)?'<span class="label label-sm label-success"> Yes </span>':'<span class="label label-sm label-danger"> No </span>' !!}
                                    </td>
                                    <td> {{ date('d-M-Y',strtotime($value->upd)) }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop