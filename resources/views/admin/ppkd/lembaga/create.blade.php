@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Buat Lembaga Baru</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Buat Lembaga Baru</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.lembaga.store') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label class="control-label col-md-3">SKPD
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control select2me" name="kd_skpd" required>
                                            <option value="">Pilih SKPD</option>
                                            @foreach($recSkdp as $key=>$value)
                                            	<option value="{{ $value->kd_skpd }}" {{ ($value->nomor==old('kd_skpd'))?'selected':'' }}>{{ strtoupper($value->nm_skpd) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tipe Lembaga
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control select2me" name="tipeLembagaID" required>
                                            <option value="">Pilih Tipe Lembaga</option>
                                            @foreach($recTipeLembaga as $key=>$value)
                                                <option value="{{ $value->id }}">{{ ucfirst($value->nama) }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lembaga
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="nm_lembaga" type="text" data-required="1" class="form-control" value="{{ old('nm_lembaga') }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. NPWP
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="npwp" type="text" class="form-control" value="{{ old('npwp') }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat Lengkap
                                    </label>
                                    <div class="col-md-8">
                                        <input name="alamat" type="text" data-required="1" class="form-control" value="{{ old('alamat') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Pilih Kelurahan
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control select2me" name="nokel">
                                            <option value="">Pilih Kelurahan</option>
                                            @foreach($recKelurahan as $key=>$value)
                                                <option value="{{ $value->nokel }}">{{ $value->nmkel }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. AKTA KUMHAM
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_akta" type="text" class="form-control" value="{{ old('no_akta') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal AKTA
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_akta" value="{{ old('tgl_akta') }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Surat Domisili
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_surat_domisili" type="text" class="form-control" value="{{ old('no_surat_domisili') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Surat Domisili
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_surat_domisili" value="{{ old('tgl_surat_domisili') }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Sertifikat
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_sertifikat" type="text" class="form-control" value="{{ old('no_sertifikat') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Sertifikat
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_sertifikat" value="{{ old('tgl_sertifikat') }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Izin Operasional
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_izin_operasional" type="text" class="form-control" value="{{ old('no_izin_operasional') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Izin Operasional
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_izin_operasional" value="{{ old('tgl_izin_operasional') }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Telepon
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_telepon" type="text" class="form-control" value="{{ old('no_telepon') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Fax
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_fax" type="text" class="form-control" value="{{ old('no_fax') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email Lembaga
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email1" type="text" class="form-control" value="{{ old('email1') }}"> </div>
                                </div>
                                <h3 class="form-section">Akun Bank</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Bank
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nama_bank" type="text" class="form-control" value="{{ old('nama_bank') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Rekening
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_rek" type="text" class="form-control" value="{{ old('no_rek') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rekening a/n
                                    </label>
                                    <div class="col-md-4">
                                        <input name="pemilik_rek" type="text" class="form-control" value="{{ old('pemilik_rek') }}"> </div>
                                </div>
                                <h3 class="form-section">Kontak Person</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                    </label>
                                    <div class="col-md-4">
                                        <input name="kontak_person" type="text" class="form-control" value="{{ old('kontak_person') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email_person" type="email" class="form-control" value="{{ old('email_person') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat
                                    </label>
                                    <div class="col-md-4">
                                        <input name="alamat_person" type="text" class="form-control" value="{{ old('alamat_person') }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. HandPhone
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_hp_person" type="text" class="form-control" value="{{ old('no_hp_person') }}"> </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop