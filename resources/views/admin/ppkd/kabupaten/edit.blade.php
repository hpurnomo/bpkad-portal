@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.master.kabupaten.index') }}">Kota/Kab</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah Kota/Kab </span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Ubah Kota/Kab </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.master.kabupaten.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST"> 
                            <input type="hidden" name="nokota" value="{{ $kabupaten->nokota }}">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}

                                  <div class="form-group">
                                     <label class="control-label col-md-3"> Provinsi
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                         {!! Form::select('noprov',$list_prov,$kabupaten->noprov, $attributes = array('maxlength'=>'100','class'=>'form-control')) !!}
                                    </div> 
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-md-3">Kode Kota/Kab
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="kdkota" type="text" data-required="1" class="form-control" value="{{ $kabupaten->kdkota }}" required="true" > </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Nama Kota/Kab
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nmkota" type="text" class="form-control" value="{{ $kabupaten->nmkota }}" required="true" data-required="2" > </div>
                                </div> 

                            </div>
 
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Ubah</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop