@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.download.rekap.index') }}">Download Rekap</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="javascript:;">{{ucwords($recPeriodeByNoper->keterangan)}}</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <h4>Link Download Rekap</strong></h4>
                    <ol>
                    	<li><a href="{{route('ppkd.download.rekap.lampiran.sk.gubernur',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Lampiran SK Gubernur</a></li>
                    	<li><a href="{{route('ppkd.download.rekap.laporan.realisasi.anggaran.hibahbansos',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Laporan Realisasi Anggaran HibahBansos</a></li>
                    	<li><a href="{{route('ppkd.download.rekap.laporan.realisasi.perskpd',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Laporan Realisasi per SKPD/UKPD</a></li>
                    	<li><a href="{{route('ppkd.download.rekap.laporan.lembaga.not.realisasi',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Laporan lembaga yang tidak merealisasikan Hibah,Bansos Per-SKPD/UKPD dan Per-Kode Rekening</a></li>
                    	<li><a href="{{route('ppkd.download.rekap.laporan.lembaga.lpj',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Laporan lembaga yang sudah menyapaikan LPJ</a></li>
                    	<li><a href="{{route('ppkd.download.rekap.jumlah.lembaga',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Rekap jumlah lembaga yang mendapatkan Hibah dan Bansos per-SKPD/UKPD dan Per-Kode Rekening</a></li>
                    	
                    	<li><a href="{{route('ppkd.download.rekap.perkode.rekening',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Rekap Hibah dan Bansos Per-Kode Rekening</a></li>
                    	<li><a href="{{route('ppkd.download.rekap.lembaga.usulan.tolak',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Rekap lembaga yang sudah mengusulkan tetapi tidak mendapatkan Hibah dan Bansos</a></li>

                        <li><a href="{{route('ppkd.download.rekap.pengajuan.2020',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Rekap Proposal Pengajuan</a></li>

                        <li><a href="{{route('ppkd.download.rekap.penebalan.2020',['noper'=>$noper])}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i>Rekap Proposal Definitif </a></li>

                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop