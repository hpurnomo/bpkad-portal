@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.master.kelurahan.index') }}">Master Kelurahan</a>
                    <i class="fa fa-circle"></i>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12"> 
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Data Master Kelurahan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                            <div class="row" style="padding-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('ppkd.master.kelurahan.create') }}" class="btn sbold green"> Tambah Kelurahan
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                          <table class="table table-striped table-bordered table-hover table-common" >
                            <thead>
                                <tr>
                                    <th> No Kelurahan </th>  
                                    <th> Kode Kelurahan </th>  
                                    <th> Nama Kelurahan </th>   
                                    <th> Kecamatan </th> 
                                    <th> Kabupaten </th> 
                                    <th> Provinsi </th> 
                                    <th> Action </th>   
                                </tr>
                            </thead> 
                        </table> 
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
      <script type="text/javascript"> 
                var table = $('.table-common').DataTable({
                serverSide: true,
                processing: true, 
                scrollY: 500,
                lengthMenu: [[15, 30, 50, 100, 150, 200, -1], [15, 30, 50,100,150,200, "All"]], 
                ajax: {
                    headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    url: "http://ehibahbansosdki.jakarta.go.id/ppkd/master/kelurahan/get-data",
                    // url: "{{ route('ppkd.master.kelurahan.getdata') }}",
                    type: "POST"
                },
                columns: [
                  {!! $column !!}
                ], 
                columnDefs: [
                  {!! $columnConf !!}
                ],
                dom: 'lfrtip'
          });

        </script> 


    @if( !empty(Session::get('status')) )
      
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop