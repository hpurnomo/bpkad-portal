@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.master.kelurahan.index') }}">Kelurahan</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Ubah Kelurahan </span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Ubah Kelurahan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('ppkd.master.kelurahan.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST"> 
                            <input type="hidden" name="nokel" value="{{ $kelurahan->nokel }}">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}

                                <div class="form-group">
                                     <label class="control-label col-md-3"> Provinsi
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                         {!! Form::select('noprov',$list_prov,$kelurahan->noprov, $attributes = array('maxlength'=>'100','class'=>'form-control','id'=>'prov')) !!}
                                    </div> 
                                </div> 

                                <div class="form-group">
                                     <label class="control-label col-md-3"> Kab/Kota
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                         {!! Form::select('nokota',$list_kab,$kelurahan->nokota, $attributes = array('maxlength'=>'100','class'=>'form-control','id'=>'kab')) !!}
                                    </div> 
                                </div> 

                                 <div class="form-group">
                                     <label class="control-label col-md-3"> Kecamatan
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                         {!! Form::select('nokec',$list_kec,$kelurahan->nokec, $attributes = array('maxlength'=>'100','class'=>'form-control','id'=>'kec')) !!}
                                    </div> 
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-md-3">Kode Kelurahan
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="kdkel" type="text" data-required="1" class="form-control" value="{{ $kelurahan->kdkel }}" required="true" > </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Nama Kelurahan
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nmkel" type="text" class="form-control" value="{{ $kelurahan->nmkel }}" required="true" data-required="2" > </div>
                                </div> 

                            </div>
 
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Ubah</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        
            $(function(){
             
            $('#prov').on('change', function(){ 
                $.ajax({
                  type: "POST",
                  headers: {
                      "X-CSRF-TOKEN": "{{ csrf_token() }}"
                  },
                  url: "{{ route('ppkd.master.kabupaten.list') }}",
                  data: {noprov: $('#prov').val()},
                  success: function(data){
                      $('#kab').html(data);   
                  }
                });  
            });

             $('#kab').on('change', function(){ 
                $.ajax({
                  type: "POST",
                  headers: {
                      "X-CSRF-TOKEN": "{{ csrf_token() }}"
                  },
                  url: "{{ route('ppkd.master.kecamatan.list') }}",
                  data: {nokota: $('#kab').val()},
                  success: function(data){
                      $('#kec').html(data);   
                  }
                });  
            });


        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop