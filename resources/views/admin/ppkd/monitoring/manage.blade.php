@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('ppkd.monitoring.manage') }}">Monitoring SP2D</a>
                    <i class="fa fa-circle"></i>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Data Realisasi Anggaran SIPKD</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                          <table class="table table-striped table-bordered table-hover table-common" >
                            <thead>
                                <tr>
                                    <th> Tahun </th> 
                                    <th> Kode SKPD </th>  
                                    <th> Nama SKPD </th>  
                                    <th> Kode Kegiatan </th> 
                                    <th> Nama Kegiatan </th> 
                                    <th> Alamat Kegiatan </th>  
                                    <th> Kode AKUN </th>  
                                    <th> Nama AKUN </th>  
                                    <th> Nilai Anggaran Murni </th>  
                                    <th> Nilai Anggaran Perubahan </th>  
                                    <th> Tgl SP2D </th>  
                                    <th> Nilai SP2D </th>  
                                </tr>
                            </thead> 
                        </table> 
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
      <script type="text/javascript"> 
                var table = $('.table-common').DataTable({
                serverSide: true,
                processing: true, 
                scrollY: 500,
                lengthMenu: [[15, 30, 50, 100, 150, 200, -1], [15, 30, 50,100,150,200, "All"]], 
                ajax: {
                    headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    // url: "{{ URL::to('ppkd/monitoring/getData') }}",
                    url: "http://ehibahbansosdki.jakarta.go.id/ppkd/monitoring/getData",
                    type: "POST"
                },
                columns: [
                  {!! $column !!}
                ], 
                columnDefs: [
                  {!! $columnConf !!}
                ],
                dom: 'lfrtip'
          });

        </script> 


    @if( !empty(Session::get('status')) )
      
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop