@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('dashboard.member') }}">Dashboard</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Dashboard
            {{-- <small>dashboard & statistics</small> --}}
        </h3>
        
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS 1-->
       
        <div class="row widget-row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Feeds</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" class="active" data-toggle="tab" aria-expanded="true"> Proposal </a>
                            </li>
                            <li class="">
                                <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> Rekap </a>
                            </li>
                        </ul>
                    </div>
                    {{-- {{ dd($feedPPKD) }} --}}
                    <div class="portlet-body">
                        <!--BEGIN TABS-->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <select name="jdl_prop" id="jdl_prop" class="form-control select2me">
                                    <option value="">Search</option>
                                    @foreach ($feedPPKD as $element)
                                    <option value="{{ ($element->jenisPeriodeID==1)?'penetepan':'perubahan' }}/{{ $element->no_prop }}">{{ $element->jdl_prop }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <div class="slimScrollDiv">
                                    <div class="scroller" data-always-visible="1" data-rail-visible="0" data-initialized="1">
                                        <ul class="feeds">
                                            @foreach ($feedPPKD as $element)
                                            <li style="border-bottom: 1px dashed #2A3140;">
                                                <div class="col-md-6">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-info">
                                                                    <i class="fa fa-bullhorn"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                @if ($element->status_kirim==1)
                                                                    <div class="desc"><a href="{{ route('ppkd.proposal.edit',['fase'=>($element->jenisPeriodeID==1)?'penetepan':'perubahan','id'=>$element->no_prop]) }}">{{ $element->jdl_prop }} [{{ $element->keterangan_periode }}]</a></div>
                                                                @endif
                                                                <div class="date"> {{ date('d/m/Y',strtotime($element->tgl_prop)) }} </div>
                                                                <br><br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <p><b>Status:</b></p>
                                                    @if($element->status_prop==0)
                                                        <span class="label label-warning">Belum Direkomendasi</span>
                                                    @elseif($element->status_prop==1)
                                                        <span class="label label-success">Sudah Direkomendasi</span>

                                                        @if ($element->is_penebalan_nominal_tapd==1||$element->is_penebalan_nominal_banggar==1||$element->is_penebalan_nominal_ddn==1||$element->is_penebalan_nominal_apbd==1)
                                                            <span class="label label-warning">Butuh Persetujuan Penebalan</span>
                                                        @endif
                                                    @elseif($element->status_prop==3)
                                                        <span class="label label-warning">Di Koreksi</span>
                                                    @else
                                                        <span class="label label-danger">Di Tolak</span>
                                                        <p><u><b>Alasan ditolak karena, {{ $element->alasan_ditolak }}</b></u></p>
                                                    @endif
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_1_2">
                                <div class="slimScrollDiv">
                                    <div class="scroller" data-always-visible="1" data-rail-visible1="1" data-initialized="1"> 
                                        {!! $dashboard !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop

@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            $('#jdl_prop').on( "change", function() {
                console.log($(this).val());

                window.location.replace("{{ url('/ppkd/proposal/edit/fase') }}/"+$(this).val());
            });
        });
    </script>
@stop