@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('dashboard.member') }}">Dashboard</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Dashboard
            {{-- <small>dashboard & statistics</small> --}}
        </h3>
        @if(Session::get('is_verify') != 1)
            <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <strong>Warning!</strong> Akun Anda Belum Terverifikasi. Akun yang belum Terverifikasi tidak dapat Membuat Proposal.</div>
        @endif
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS 1-->
       
        <div class="row widget-row">
            <div class="col-md-12">
                <!-- BEGIN WIDGET THUMB -->
                {{-- <div class="note note-info">
                    <h4 class="block">Info : Hanya Lembaga yang <b>Telah Diverifikasi</b> yang dapat mengajukan Proposal.</h4>
                </div> --}}
                <div class="widget-thumb widget-bg-color-white text-uppercase bordered">
                    <h4 class="widget-thumb-heading">Status Member SKPD</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-yellow bg-font-yellow icon-layers"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-body-stat">
                            @if(Session::get('is_verify') == 1)
                                <span class="widget-thumb-subtitle">&nbsp;</span>
                                <span class="font-green-jungle sbold" style="padding-top:10px;">Telah Diverifikasi</span>
                            @else
                                <span class="widget-thumb-subtitle">&nbsp;</span>
                                <span class="font-red-thunderbird sbold" style="padding-top:10px;">Belum Diverifikasi</span>
                            @endif
                            </span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
        </div>
        <hr>
        <div class="row widget-row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Feeds</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" class="active" data-toggle="tab" aria-expanded="true"> Proposal </a>
                            </li>
                            <li class="">
                                <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> Rekap </a>
                            </li>
                        </ul>
                    </div>
                    {{-- {{ dd($feedSKPD) }} --}}
                    <div class="portlet-body">
                        <!--BEGIN TABS-->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <select name="jdl_prop" id="jdl_prop" class="form-control select2me">
                                    <option value="">Search</option>
                                    @foreach ($feedSKPD as $element)
                                    <option value="{{ base64_encode($element->no_prop) }}/{{ ($element->jenisPeriodeID==1)?'penetepan':'perubahan' }}">{{ $element->jdl_prop }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <div class="slimScrollDiv">
                                    <div class="scroller" data-always-visible="1" data-rail-visible="0" data-initialized="1">
                                        <ul class="feeds">
                                            @foreach ($feedSKPD as $element)
                                            <li style="border-bottom: 1px dashed #2A3140;">
                                                <div class="col-md-6">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-info">
                                                                    <i class="fa fa-bullhorn"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                @if ($element->status_kirim==1)
                                                                    <div class="desc"><a href="{{ route('skpd.proposal.show',['id'=>base64_encode($element->no_prop),'fase'=>($element->jenisPeriodeID==1)?'penetepan':'perubahan']) }}">{{ $element->jdl_prop }} [{{ $element->keterangan_periode }}]</a></div>
                                                                @endif
                                                                <div class="date" style="color: #d8d8d8;"> {{ date('d/m/Y',strtotime($element->tgl_prop)) }} 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <p><b>Status:</b></p>
                                                    @if($element->status_prop==0)
                                                        <span class="label label-warning">Belum Direkomendasi</span>
                                                    @elseif($element->status_prop==1)
                                                        <span class="label label-success">Sudah Direkomendasi</span>

                                                        @if ($element->is_penebalan_nominal_tapd==1||$element->is_penebalan_nominal_banggar==1||$element->is_penebalan_nominal_ddn==1||$element->is_penebalan_nominal_apbd==1)
                                                            <span class="label label-warning">Butuh Persetujuan Penebalan</span>
                                                        @endif
                                                        
                                                    @elseif($element->status_prop==3)
                                                        <span class="label label-warning">Di Koreksi</span>
                                                    @else
                                                        <span class="label label-danger">Di Tolak</span>
                                                        <p><u><b>Alasan ditolak karena, {{ $element->alasan_ditolak }}</b></u></p>
                                                    @endif
                                                    <br><br>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_1_2">
                                <div class="slimScrollDiv">
                                    <div class="scroller" data-always-visible="1" data-rail-visible1="1" data-initialized="1"> 
                                        {!! $dashboard !!} 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop

@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            $('#jdl_prop').on( "change", function() {
                console.log($(this).val());

                window.location.replace("{{ url('skpd/proposal/show/') }}/"+$(this).val());
            });
        });
    </script>
@stop