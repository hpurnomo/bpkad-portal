<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>eHibahbansos DKI Jakarta</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="1.2" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {!! Html::style('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/uniform/css/uniform.default.css') !!}
        {!! Html::style('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {!! Html::style('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/morris/morris.css') !!}
        {!! Html::style('admin/assets/global/plugins/fullcalendar/fullcalendar.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/jqvmap/jqvmap/jqvmap.css') !!}
        {!! Html::style('admin/assets/pages/css/profile-2.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}
        {!! Html::style('admin/assets/global/plugins/bootstrap-toastr/toastr.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/datatables/datatables.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}
        {!! Html::style('admin/assets/global/plugins/select2/css/select2.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css') !!}
        {!! Html::style('admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
        {!! Html::style('admin/assets/global/plugins/fileuploader/src/jquery.fileuploader.css') !!}
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        {!! Html::style('admin/assets/global/css/components.min.css') !!}
        {!! Html::style('admin/assets/global/css/plugins.min.css') !!}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        {!! Html::style('admin/assets/layouts/layout/css/layout.min.css') !!}
        {!! Html::style('admin/assets/layouts/layout/css/themes/blue.min.css') !!}
        {!! Html::style('admin/assets/layouts/layout/css/custom.css') !!}
        <!-- END THEME LAYOUT STYLES -->
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- Part Header -->
        @include('admin/part/header')
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- Part Sidebar -->
            @if(Session::get('group_id') == 12) <!-- role lembaga -->
                @include('admin/part/lembaga/sidebar')
            @elseif(Session::get('group_id') == 10) <!-- role skpd -->
                @include('admin/part/skpd/sidebar')
            @elseif(Session::get('group_id') == 8) <!-- role ppkd -->
                @include('admin/part/ppkd/sidebar')
            @endif
            <!-- BEGIN CONTENT -->
            @yield('content')
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- Part Footer -->
        @include('admin/part/footer')

        @section('javascript')
        <!--[if lt IE 9]>
		<script src="../assets/global/plugins/respond.min.js"></script>
		<script src="../assets/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        {!! Html::script('admin/assets/global/plugins/jquery.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/js.cookie.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/jquery.blockui.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/uniform/jquery.uniform.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {!! Html::script('admin/assets/global/plugins/moment.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/morris/morris.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/morris/raphael-min.js') !!}
        {!! Html::script('admin/assets/global/plugins/counterup/jquery.waypoints.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/counterup/jquery.counterup.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/fullcalendar/fullcalendar.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/flot/jquery.flot.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/flot/jquery.flot.resize.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/flot/jquery.flot.categories.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/jquery.sparkline.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-toastr/toastr.min.js') !!}
        {!! Html::script('admin/assets/global/scripts/datatable.js') !!}
        {!! Html::script('admin/assets/global/plugins/datatables/datatables.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}
        {!! Html::script('admin/assets/global/plugins/dataTables.responsive.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/select2/js/select2.full.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
        <!-- {!! Html::script('admin/assets/global/plugins/jquery.mask.min.js') !!} -->
        {!! Html::script('admin/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') !!}
        {!! Html::script('assets/bower_components/chained/jquery.chained.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/fileuploader/src/jquery.fileuploader.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/ckeditor/ckeditor.js') !!}


        {{-- {!! Html::script('admin/assets/global/plugins/gmaps/gmaps.min.js') !!} --}}
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        {!! Html::script('admin/assets/global/scripts/app.min.js') !!}
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        {{-- {!! Html::script('admin/assets/pages/scripts/maps-google.min.js') !!} --}}
        {!! Html::script('admin/assets/pages/scripts/dashboard.min.js') !!}
        {!! Html::script('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') !!}
        {!! Html::script('admin/assets/pages/scripts/form-validation.js') !!}
        {!! Html::script('admin/assets/pages/scripts/ui-toastr.min.js') !!}
        {!! Html::script('admin/assets/pages/scripts/table-datatables-managed.js') !!}
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        {!! Html::script('admin/assets/layouts/layout/scripts/layout.min.js') !!}
        {!! Html::script('admin/assets/layouts/layout/scripts/demo.min.js') !!}
        {!! Html::script('admin/assets/layouts/global/scripts/quick-sidebar.min.js') !!}
        {!! Html::script('admin/assets/layouts/global/scripts/idle-timer.min.js') !!}
        {!! Html::script('admin/assets/global/scripts/cleave.min.js') !!} 
        
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">
            $(function(){
                $('.sidebar-toggler').click(function(){
                    $('#logo-center').toggle('slow');
                });

            });

            $(function(){
                // idleTimer() takes an optional argument that defines the idle timeout
                // timeout is in milliseconds; defaults to 30000
                // $.idleTimer(60000);


                // $(document).bind("idle.idleTimer", function(){
                //     alert('Waktu Idle Anda Telah Melebihi Batas Maksimum (1 Menit). Terima Kasih.');

                //     window.location.assign('{{ url('/logout') }}');
                // });

                // $('.mask_currency').mask('000.000.000', {reverse: true});
                 $(".mask_currency").inputmask('99.999.999.999.999', {
                    numericInput: true
                }); 


            });
        </script>

         <script type="text/javascript">
            $(document).ready(function(){
                  $(".checkProposal").click(function(e){
                     // e.preventDefault();
                    e.stopPropagation();
                    var jdl_prop        = $("#jdl_prop").val();
                    var no_lembaga      = $("#no_lembaga").val();
                    var email_lembaga   = $("#email_lembaga").val();
                    var email           = $("#email").val();
                    var real_name       = $("#real_name").val();
                    var no_prop         = $("#no_prop").val();
                    var noper           = $("#noper").val();
                    var uuid           = $("#uuid").val();
                    var tahapan         = $(this).attr("data-id");
                   

                    if ($(this).is(":checked"))
                      {
                       var status=1;
                       
                       if(tahapan !='Definitif'){
                        $("#confirm-message").html("Apakah anda yakin melakukan persetujuan proses penebalan proposal ke lembaga? Mohon perhatikan setelah anda klik setuju maka sistem ehibah akan otomatis langsung mengirimkan pemberitahuan ke email lembaga berisikan instruksi untuk segera menindak lanjuti proses penebalan ini. Secara otomatis juga status persetujuan penebalan SKPD akan terkunci (tidak dapat dihilangkan centangnya lagi)"); 
                       }else{
                        $("#confirm-message").html("Apakah anda yakin untuk melakukan persetujuan proses definitif ke lembaga? Mohon perhatikan setelah anda klik setuju maka sistem ehibah akan otomatis langsung mengirimkan pemberitahuan ke email lembaga berisikan instruksi untuk segera menindak lanjuti perbaikan proposal sesuai dengan nilai definitif ini. Secara otomatis juga status persetujuan definitif SKPD akan terkunci (tidak dapat dihilangkan centangnya lagi)");
                       }
                       

                       $("#btn_save").show();
                       $("#confirm-popup").modal('show');

                        $("#btn_save").unbind('click').bind('click',function(e){ 
                            e.preventDefault();
                            e.stopPropagation();
                             $.ajax({
                              async: true,
                              type: "POST",
                              // url: "{{ route('skpd.notif.approval') }}",
                              url: "http://ehibahbansosdki.jakarta.go.id/skpd/proposal/notif/approval",
                              dataType: "json",
                              data: {_token: "{{ csrf_token() }}",jdl_prop:jdl_prop,no_lembaga:no_lembaga,email_lembaga:email_lembaga,email:email,real_name:real_name,no_prop:no_prop,tahapan:tahapan,noper:noper,uuid:uuid},
                              success : function(res){
                                if(res.status == true){ 
                                   Command: toastr["success"]("Sukeses",res.message) 
                                          toastr.options = {
                                            "closeButton": false,
                                            "debug": false,
                                            "newestOnTop": false,
                                            "progressBar": false,
                                            "positionClass": "toast-top-right",
                                            "preventDuplicates": false,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "5000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "swing",
                                            "hideEasing": "linear",
                                            "showMethod": "fadeIn",
                                            "hideMethod": "fadeOut"
                                          }
                                    // location.reload();
                                    $("#confirm-popup").modal('hide');
                                }
                                else{ 
                                 Command: toastr["error"]("Error", "  gagal disimpan") 
                                            toastr.options = {
                                              "closeButton": false,
                                              "debug": false,
                                              "newestOnTop": false,
                                              "progressBar": false,
                                              "positionClass": "toast-top-right",
                                              "preventDuplicates": false,
                                              "onclick": null,
                                              "showDuration": "300",
                                              "hideDuration": "1000",
                                              "timeOut": "5000",
                                              "extendedTimeOut": "1000",
                                              "showEasing": "swing",
                                              "hideEasing": "linear",
                                              "showMethod": "fadeIn",
                                              "hideMethod": "fadeOut"
                                            }
                                            
                                }
                              },
                              error : function(err, ajaxOptions, thrownError){
                                 Command: toastr["error"]("Error", " gagal disimpan") 
                                            toastr.options = {
                                              "closeButton": false,
                                              "debug": false,
                                              "newestOnTop": false,
                                              "progressBar": false,
                                              "positionClass": "toast-top-right",
                                              "preventDuplicates": false,
                                              "onclick": null,
                                              "showDuration": "300",
                                              "hideDuration": "1000",
                                              "timeOut": "5000",
                                              "extendedTimeOut": "1000",
                                              "showEasing": "swing",
                                              "hideEasing": "linear",
                                              "showMethod": "fadeIn",
                                              "hideMethod": "fadeOut"
                                            }
                              }
                            });  
                        });  //end status 1
                      }else{
                        var status=0;
                      } 
                      // alert(status);
                  });

            });
          
        </script>
        @show
    </body>

</html>