@extends('admin/layout/template')
@section('content')
<style type="text/css">
    .fileuploader-theme-default{
        margin-top: 0 !important;
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Ubah Usulan Proposal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                    	<div class="row">
                    		<div class="col-xs-6">
		                        <div class="caption" style="font-size: 16px;">
		                            <i class="icon-settings font-red"></i>
		                            <span class="caption-subject font-red sbold uppercase">Form Ubah Usulan Proposal</span>
		                        </div>
	                        </div>
	                        <div class="col-xs-6" style="text-align:right;">
		                       {{--  <form class="delete" action="{{ route('member.proposal.delete') }}" method="POST">
							        <input type="hidden" name="_method" value="DELETE">
							        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
							        <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                    @if($recProposalByID->status_kirim==0)
							        <button type="submit" class="btn red-mint" value="Delete"><i class="icon-trash"></i> Hapus Proposal Ini</button>
                                    @endif
							    </form> --}}
                                <a href="javascript:window.history.back();" class="btn btn-default">Kembali</a>
					    	</div>
				    	</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs">
                                    <li class="{{ ($status==null)?'active':'' }}">
                                        <a href="#tab_1_0" data-toggle="tab">1. Data Proposal </a>
                                    </li>
                                    <li class="{{ ($status=='rencana')?'active':'' }}">
                                        <a href="#tab_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                    </li>

                                    <li class="{{ ($status=='kirim')?'active':'' }}">
                                        <a href="#tab_1_2" data-toggle="tab">3. Kirim Proposal & Selesai </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    
                                    {{-- Data Proposal --}}
                                    @include('admin.member.proposal.edit-data-proposal')
                                    
                                    {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                    @include('admin.member.proposal.edit-rab')

                                    {{-- Kirim Proposal & Selesai --}}
                                    @include('admin.member.proposal.edit-kirim-proposal')
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr style="border-top: 1px dashed #d4d4d4;">
        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_2_0" data-toggle="tab">1. Kelengkapan Administrasi </a>
                            </li>
                            <li>
                                <a href="#tab_2_1" data-toggle="tab">2. Peninjauan Lapangan </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            
                            {{-- Kelengkapan Administrasi --}}
                            @include('admin.member.proposal.kelengkapan-administrasi')
                            
                            {{-- Peninjauan Lapangan --}}
                            @include('admin.member.proposal.peninjauan-lapangan')
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
    
            // enable fileuploader plugin
            $('input[name="surat_pernyataan_tanggung_jawab"],input[name="berkas_kepengurusan"],input[name="berkas_proposal"],input[name="berkas_rab"],input[name="berkas_rekening"]').fileuploader({
                limit: 1,
                extensions: ['pdf'],
                fileMaxSize: 10
            });

            $('input[name="photo_lembaga"]').fileuploader({
                limit: 1,
                extensions: ['jpg', 'jpeg', 'png'],
                fileMaxSize: 10
            });
            
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });

        $(function(){
            $('#status-kirim').on('click',function(){
                if($('#status-kirim:checked').length == 1){
                    $('#btn-submit').html('Kirim');
                }
                else{
                    $('#btn-submit').html('Simpan Sebagai Draf');   
                }
            });
        });

        $(function(){
        	$(".delete").on("submit", function(){
		        return confirm("Anda yakin menghapus proposal ini?");
		    });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = {{ ($recProposalByID->latitude==NULL)?'0':$recProposalByID->latitude }};
            var longValue = {{ ($recProposalByID->longitude==NULL)?'0':$recProposalByID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            var cleaveNumeral = new Cleave('.input-numeral2', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            var cleaveNumeral = new Cleave('.input-numeral3', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop