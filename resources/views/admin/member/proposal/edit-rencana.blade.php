@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Buat Rencana Kegiatan</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{dd($recRencanaByID)}} --}}
        <div class="portlet light bordered">
            <div class="portlet-title">
            	<div class="row">
                    <div class="col-xs-6">
                        <div class="caption" style="font-size: 16px;">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Form Buat Rencana Kegiatan</span>
                        </div>
                    </div>
                    <div class="col-xs-6" style="text-align:right;">
                        <a href="{{route('member.proposal.edit',['id'=>base64_encode($recRencanaByID->no_prop),'fase'=>$fase,'status'=>'rencana'])}}" class="btn btn-default">Kembali</a>
                    </div>
                </div>
            </div>
            <div class="portlet-body form">
                @include('admin.part.alert')
                <!-- BEGIN FORM-->
                <div class="row">
                    <div class="col-md-12" style="text-align: right;">
                        <form class="delete" action="{{ route('member.proposal.delete.rencana') }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" value="{{ $recRencanaByID->id }}">
                            <input type="hidden" name="no_prop" value="{{ $recRencanaByID->no_prop }}">
                            <input type="hidden" name="fase" value="{{ $fase }}">
                            <button type="submit" class="btn red-mint" value="Delete"><i class="icon-trash"></i> Hapus Rencana Kegiatan Ini</button>
                        </form>
                    </div>
                </div>
                <form action="{{ route('member.proposal.update.rencana') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $recRencanaByID->id }}">
                    <input type="hidden" name="no_prop" value="{{ $recRencanaByID->no_prop }}">
                    <input type="hidden" name="fase" value="{{ $fase }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rencana Kegiatan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <textarea name="rencana" class="form-control" rows="3" required="">{{$recRencanaByID->rencana}}</textarea> 
                                        <br>
                                        <button type="submit" class="btn green">Ubah Rencana Kegiatan</button>
                                        <button type="reset" class="btn default">Batal</button>
                                        <br><br>
                                        <hr>
                                        <p>Anda ingin menambahkan detail Rincian Anggaran & Biaya ? Klik tombol di bawah ini.</p> 
                                        <a href="{{ route('member.proposal.create.rincian',['rencanaID'=>$recRencanaByID->id,'no_prop'=>base64_encode($recRencanaByID->no_prop),'fase'=>$fase]) }}" class="btn btn-circle sbold blue" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Rincian Anggaran & Biaya (RAB)
                                                <i class="fa fa-plus"></i>    
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            $(".delete").on("submit", function(){
                return confirm("Anda yakin menghapus rencana kegiatan ini?");
            });
        });
    </script>
@stop