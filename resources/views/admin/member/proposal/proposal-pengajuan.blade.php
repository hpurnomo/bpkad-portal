<div class="tab-pane fade active in" id="tab_1">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-eye"></i>Detil Proposal Usulan</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        {{-- {{ dd($recProposalByID->email_lembaga) }} --}}
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <a href="{{route('member.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper])}}" class="btn btn-default" style="float: right;margin-right: 5px;">Kembali</a>
                        @if ($cekPerjanjianNPHD['id'] != "" || $cekPerjanjianNPHD['id'] != NULL)
                            {{-- <a href="{{ route('member.proposal.cetak.perjanjian.nphd',['id'=>$recProposalByID->no_prop]) }}" class="btn red" style="float: right;margin-right: 5px;"><i class="fa fa-download" aria-hidden="true"></i> Unduh Naskah Perjanjian NPHD</a>
                            <a href="{{ route('member.proposal.naskah.perjanjian.nphd.show',['no_prop'=>$recProposalByID->no_prop,'fase'=>$fase]) }}" class="btn green-meadow">
                               <i class="fa fa-cogs" aria-hidden="true"></i> Edit Naskah Perjanjian NPHD
                            </a> --}}
                        @elseif($recProposalByID->status_prop==1)
                            {{-- <a href="{{ route('member.proposal.naskah.perjanjian.nphd.show',['no_prop'=>$recProposalByID->no_prop,'fase'=>$fase]) }}" class="btn green-meadow">
                               <i class="fa fa-cogs" aria-hidden="true"></i> Buat Naskah Perjanjian NPHD
                            </a> --}}
                        @elseif($recProposalByID->status_prop==0)
                       {{--  <span class="text-info">*Naskah Perjanjian NPHD belum tersedia. Karna Proposal ini belum direkomendasi oleh SKPD.</span> --}}
                        @endif
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_0" data-toggle="tab">1. Data Proposal </a>
                                </li>
                                <li>
                                    <a href="#tab_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                
                                {{-- Data Proposal --}}
                                <div class="tab-pane fade active in" id="tab_1_0">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body" style="padding: 0;margin: 0;">
                                            <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalByID->tgl_prop)) }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalByID->no_proposal }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalByID->jdl_prop }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Isi Proposal</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalByID->latar_belakang !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                             <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalByID->maksud_tujuan !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalByID->keterangan_prop !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Nominal Permohonan</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                        <div class="col-md-8">
                                                            <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="form-section">Data Pendukung</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawab')
                                                            @foreach($berkasPernyataanTanggungJawab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusan')
                                                            @foreach($berkasKepengurusan->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('fotoProposal','BPKAD\Http\Entities\FotoProposal')
                                                            @foreach($fotoProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposal')
                                                            @foreach($berkasProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRab','BPKAD\Http\Entities\BerkasRab')
                                                            @foreach($berkasRab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekening')
                                                            @foreach($berkasRekening->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                        <div class="col-md-9">
                                                            <div class="portlet-body">
                                                                <div class="input-group">
                                                                    <input type="hidden" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                                    {{-- <span class="input-group-btn">
                                                                        <button class="btn blue" id="gmap_geocoding_btn">
                                                                            <i class="fa fa-search"></i>
                                                                    </span> --}}
                                                                </div>
                                                                <div id="gmap_basic" class="gmaps"> </div>
                                                                <b>Latitude : </b>
                                                                <input type="text" class="form-control" name="latitude" id="latVal" value="{{$recProposalByID->latitude}}"  >
                                                                <b>Longitude : </b>
                                                                <input type="text" class="form-control" name="longitude" id="longVal" value="{{$recProposalByID->longitude}}"   >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Status Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">
                                                                @if($recProposalByID->status_prop==0)
                                                                    <span class="label label-warning">Belum Direkomendasi</span>
                                                                @elseif($recProposalByID->status_prop==1)
                                                                    <span class="label label-success">Sudah Direkomendasi</span>
                                                                @elseif($recProposalByID->status_prop==3)
                                                                    <span class="label label-warning">Di Koreksi</span>
                                                                @else
                                                                    <span class="label label-danger">Di Tolak</span>
                                                                    <p><u><b>Alasan ditolak karena, {{ $recProposalByID->alasan_ditolak }}</b></u></p>
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Status Terkirim:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> 
                                                                {!! ($recProposalByID->status_kirim==1)?'Terkirim <i class="icon-paper-plane"></i>':'Belum Terkirim' !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <p>
                                                                <center>
                                                                <em><b><u>Jika Anda sudah mengecek dan tetap tidak menemukan email dari kami silahkan klik tombol dibawah ini. <br> Bukti kirim proposal akan terkirim kepada email lembaga dan email koordinator lembaga.</u></b></em></center>
                                                            </p>
                                                            <p>
                                                                <center>
                                                                <a href="{{ route('member.proposal.resendEmail',['id'=>base64_encode($recProposalByID->no_prop),'email'=>Auth::user()->email,'real_name'=>Auth::user()->real_name,'email_lembaga'=>$recProposalByID->email_lembaga,'no_lembaga'=>$recProposalByID->no_lembaga,'tahun_anggaran'=>$recProposalByID->tahun_anggaran,'fase'=>$fase])}}" class="btn btn-circle red"> KIRIM ULANG BUKTI KIRIM PROPOSAL VIA EMAIL
                                                                <i class="icon-paper-plane"></i></a>
                                                                </center>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                
                                {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                <div class="tab-pane fade" id="tab_1_1">
                                    <div class="col-md-12">
                                        <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                        @if($recProposalByID->nominal_pengajuan != $rencana_anggaran)
                                        <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                        @else
                                        <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($rencana_anggaran,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <br><br><br>
                                        <!--/row-->
                                        {{-- <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{ route('member.proposal.create.rencana',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase]) }}" class="btn sbold green"> Buat Rencana Kegiatan
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th> Rencana Kegiatan/Rincian </th>
                                                    <th> Vol/Satuan (jumlah) </th>
                                                    <th> Harga Satuan (Rp) </th>
                                                    <th> Anggaran (Rp) </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($recRencanaByNoProp)==0)
                                                    <tr>
                                                        <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                    </tr>
                                                @else
                                                    @inject('metrics', 'BPKAD\Services\RincianService')
                                                    @foreach ($recRencanaByNoProp as $key=>$value)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td><b>{{$value->rencana}}</b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                    </tr>
                                                    {!! $metrics->outputByRencanaIDWithoutEdit($value->id,$value->no_prop,$fase) !!}
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_2_0" data-toggle="tab">1. Kelengkapan Administrasi </a>
                        </li>
                        <li>
                            <a href="#tab_2_1" data-toggle="tab">2. Peninjauan Lapangan </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        
                        {{-- Kelengkapan Administrasi --}}
                        @include('admin.member.proposal.kelengkapan-administrasi')
                        
                        {{-- Peninjauan Lapangan --}}
                        @include('admin.member.proposal.peninjauan-lapangan')
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>