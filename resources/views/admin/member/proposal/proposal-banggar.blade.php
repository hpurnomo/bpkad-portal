<div class="tab-pane fade" id="tab_3">
    @if (empty($recProposalPenebalanBANGGARByUUID))
    <h5 style="color: red;font-weight: bolder;">*Saat ini Anda memasuki tahapan proposal penebalan tahap Badan Anggaran. Silahkan Anda buat proposal penebalan dengan klik tombol di bawah ini.</h5>

    <a href="{{ route('member.proposal.penebalan.create',['fase'=>$fase,'noper'=>$recProposalByID->noper,'uuid'=>$recProposalByID->uuid,'tahap'=>'banggar','lembaga'=>'null']) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Usulan Proposal Penebalan
        <i class="icon-note"></i>
    </a>
    @else
        @if ($recProposalPenebalanBANGGARByUUID->status_kirim==0)
            <h5 style="color: red;font-weight: bolder;">*Saat ini proposal Anda belum terkirim, Segera lengkapi proposal Anda lalu kirim proposal Anda.</h5>

            <a href="{{ route('member.proposal.penebalan.edit',['fase'=>$fase,'noper'=>$recProposalPenebalanBANGGARByUUID->noper,'uuid'=>$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan,'tahap'=>'banggar','status'=>null]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Pengaturan Usulan Proposal Penebalan
                <i class="icon-note"></i>
            </a>
        @else
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-eye"></i>Detil Proposal BANGGAR</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <a href="{{route('member.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper])}}" class="btn btn-default" style="float: right;margin-right: 5px;">Kembali</a>
                            <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_012" data-toggle="tab">1. Data Proposal </a>
                                </li>
                                <li>
                                    <a href="#tab_1_112" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                
                                {{-- Data Proposal --}}
                                <div class="tab-pane fade active in" id="tab_1_012">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body" style="padding: 0;margin: 0;">
                                            <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalPenebalanBANGGARByUUID->tgl_prop)) }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalPenebalanBANGGARByUUID->no_proposal }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalPenebalanBANGGARByUUID->jdl_prop }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Isi Proposal</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->latar_belakang !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                             <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->maksud_tujuan !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->keterangan_prop !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Nominal Permohonan</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                        <div class="col-md-8">
                                                            <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanBANGGARByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="form-section">Data Pendukung</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                            @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                            @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                            @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                            @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                            @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                            @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                        <div class="col-md-9">
                                                            <div class="portlet-body">
                                                                <div class="input-group">
                                                                    <input type="hidden" class="form-control" id="pac-input-banggar" placeholder="Cari Alamat">
                                                                    {{-- <span class="input-group-btn">
                                                                        <button class="btn blue" id="gmap_geocoding_btn">
                                                                            <i class="fa fa-search"></i>
                                                                    </span> --}}
                                                                </div>
                                                                <div id="gmap_basic_banggar" class="gmaps"> </div>
                                                                <b>Latitude : </b>
                                                                <input type="text" class="form-control" name="latitude" id="latValBANGGAR" value="{{$recProposalPenebalanBANGGARByUUID->latitude}}">
                                                                <b>Longitude : </b>
                                                                <input type="text" class="form-control" name="longitude" id="longValBANGGAR" value="{{$recProposalPenebalanBANGGARByUUID->longitude}}"   >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Status Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">
                                                                @if($recProposalPenebalanBANGGARByUUID->status_prop==0)
                                                                    <span class="label label-warning">Belum Direkomendasi</span>
                                                                @elseif($recProposalPenebalanBANGGARByUUID->status_prop==1)
                                                                    <span class="label label-success">Sudah Direkomendasi</span>
                                                                @elseif($recProposalPenebalanBANGGARByUUID->status_prop==3)
                                                                    <span class="label label-warning">Di Koreksi</span>
                                                                @else
                                                                    <span class="label label-danger">Di Tolak</span>
                                                                    <p><u><b>Alasan ditolak karena, {{ $recProposalPenebalanBANGGARByUUID->alasan_ditolak }}</b></u></p>
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Status Terkirim:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> 
                                                                {!! ($recProposalPenebalanBANGGARByUUID->status_kirim==1)?'Terkirim <i class="icon-paper-plane"></i>':'Belum Terkirim' !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <p>
                                                                <em><b><u>Jika Anda sudah mengecek dan tetap tidak menemukan email dari kami silahkan klik tombol kirim ulang bukti dibawah ini.</u></b></em>
                                                            </p>
                                                            <p>
                                                                <a href="{{ route('member.proposal.penebalan.resendEmail',['id'=>base64_encode($recProposalPenebalanBANGGARByUUID->no_prop),'email'=>Auth::user()->email,'real_name'=>Auth::user()->real_name,'no_lembaga'=>$recProposalPenebalanBANGGARByUUID->no_lembaga,'tahun_anggaran'=>$recProposalPenebalanBANGGARByUUID->tahun_anggaran,'fase'=>$fase,'tahap'=>'banggar'])}}" class="btn btn-circle green"> KIRIM ULANG BUKTI KIRIM PROPOSAL
                                                                <i class="icon-paper-plane"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                
                                {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                <div class="tab-pane fade" id="tab_1_112">
                                    <div class="col-md-12">
                                        <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                        @if($recProposalPenebalanBANGGARByUUID->nominal_pengajuan != $rencana_anggaran_banggar)
                                        <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                        @else
                                        <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanBANGGARByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_banggar,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <br><br><br>
                                        <!--/row-->
                                        
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th> Rencana Kegiatan/Rincian </th>
                                                    <th> Vol/Satuan (jumlah) </th>
                                                    <th> Harga Satuan (Rp) </th>
                                                    <th> Anggaran (Rp) </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($recRencanaByUUIDBANGGAR)==0)
                                                    <tr>
                                                        <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                    </tr>
                                                @else
                                                    @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                                    @foreach ($recRencanaByUUIDBANGGAR as $key=>$value)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td><b>{{$value->rencana}}</b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                    </tr>
                                                    {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,'banggar','rencana') !!}
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_2_012" data-toggle="tab">1. Kelengkapan Administrasi </a>
                            </li>
                            <li>
                                <a href="#tab_2_112" data-toggle="tab">2. Peninjauan Lapangan </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            {{-- Kelengkapan Administrasi --}}
                            <div class="tab-pane fade active in" id="tab_2_012">
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Administrasi</center></th>
                                                            <th><center>Ada</center></th>
                                                            <th><center>Tidak Ada</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas1" name="cek_aktivitas" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas2" name="cek_aktivitas" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas))
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Kepengurusan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan1" name="cek_kepengurusan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan2" name="cek_kepengurusan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab1" name="cek_rab" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab2" name="cek_rab" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab))
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Waktu Pelaksanaan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan1" name="cek_waktu_pelaksanaan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan2" name="cek_waktu_pelaksanaan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="checkbox" disabled="" name="is_approve"
                                                @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                                checked
                                                @endif
                                            > 
                                            <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                                </form>
                            </div>
                            
                            {{-- Kelengkapan Lapangan --}}
                            <div class="tab-pane fade" id="tab_2_112">
                                @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        {!! csrf_field() !!}
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Lapangan</center></th>
                                                            <th><center>Sesuai</center></th>
                                                            <th><center>Tidak Sesuai</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas11" name="cek_aktivitas" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas22" name="cek_aktivitas" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas))
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Kepengurusan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan11" name="cek_kepengurusan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan22" name="cek_kepengurusan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab11" name="cek_rab" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab22" name="cek_rab" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab))
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Waktu Pelaksanaan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan11" name="cek_waktu_pelaksanaan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan22" name="cek_waktu_pelaksanaan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="checkbox" disabled="" name="is_approve"
                                                @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                                checked
                                                @endif
                                            > 
                                            <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                                </form>
                                @else
                                <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endif
</div>