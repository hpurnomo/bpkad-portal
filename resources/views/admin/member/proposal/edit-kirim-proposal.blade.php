<div class="tab-pane fade {{ ($status=='kirim')?'active in':'' }}" id="tab_1_2">
    <div class="col-md-12">
    	<form action="{{ route('member.proposal.setujui.kirim') }}" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
		    {{ csrf_field() }}
    		<input type="hidden" name="tahun_anggaran" value="{{ $recProposalByID->tahun_anggaran }}">
		    <input type="hidden" name="noper" value="{{ $recProposalByID->noper }}">
		    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
		    <input type="hidden" name="no_lembaga" value="{{ Auth::user()->no_lembaga }}">
		    <input type="hidden" name="email" value="{{ Auth::user()->email }}">
		    <input type="hidden" name="email_lembaga" value="{{$recProposalByID->email_lembaga}}">
		    <input type="hidden" name="fase" value="{{$fase}}">
		    <input type="hidden" name="jdl_prop" value="{{ $recProposalByID->jdl_prop }}">
	        	<div class="form-body" style="padding-top: 0;">
	           
	            <h3 style="margin-top: 0;">Kirim Proposal</h3>
	           	@if($recProposalByID->status_kirim==0)
		            @if($rencana_anggaran == null || $rencana_anggaran == 0 )
		            <hr>
		            <div class="form-group">
		                <div class="col-md-6">
		                    <label>
		                        <i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">Agar proposal ini dapat dikirim/dilanjutkan ke SKPD terkait. <br> Anda wajib mengisi Rencana Penggunaan Bantuan & RAB.</i>
		                    </label>
		                </div>
		            </div>
		            {{-- Jika Tahapan Pengajuan Proposal Dibuka --}}
		            @elseif($getPengajuanProposalStatus == 1)
		            <hr>
		            <div class="form-group">
		                <div class="col-md-6">
		                    <label>
		                        <input type="checkbox" name="status_kirim" id="status-kirim" required=""> <b>Anda Setuju Kirim Langsung Proposal Ke SKPD Koordinator?</b><i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">(Proposal yang telah dikirim tidak dapat di-ubah, Karna akan di-review oleh SKPD Koordinator)</i>
		                    </label>
		                </div>
		            </div>
		            <div class="form-group">
		                <div class="col-md-6">
		                    <button type="submit" class="btn green" id="btn-submit">Kirim</button>
		                </div>
		            </div>
		            @else
		            <hr>
		            <div class="form-group">
		                <div class="col-md-6">
		                    <label>
		                        <b>Pengajuan Proposal saat ini telah ditutup.</b>
		                    </label>
		                </div>
		            </div>
		            @endif
		    	</div>
		        @else
		            <hr>
		            <div class="form-group">
		                <label class="control-label col-md-3"></label>
		                <div class="col-md-6">
		                    <label>
		                        <b>Proposal Ini Telah Terkirim</b>
		                    </label>
		                </div>
		            </div>
	        	</div>
		        <div class="form-actions">
		            <div class="row">
		                <div class="col-md-offset-3 col-md-9">
		                    <a href="{{ route('member.proposal.index') }}" class="btn grey-salsa btn-outline">Kembali</a>
		                </div>
		            </div>
		        </div>
		        @endif 
		</form>
    </div>
</div>