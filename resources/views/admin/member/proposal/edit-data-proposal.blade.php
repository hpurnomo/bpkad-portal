<div class="tab-pane fade {{ ($status==null)?'active in':'' }}" id="tab_1_0">
    <!-- BEGIN FORM-->
    <form action="{{ route('member.proposal.update') }}" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="tahun_anggaran" value="{{ $recProposalByID->tahun_anggaran }}">
    <input type="hidden" name="noper" value="{{ $recProposalByID->noper }}">
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="no_lembaga" value="{{ Auth::user()->no_lembaga }}">
    <input type="hidden" name="upb" value="{{ Auth::user()->user_id }}">
    <input type="hidden" name="upd" value="{{ Carbon\Carbon::now() }}">
    <input type="hidden" name="email" value="{{ Auth::user()->email }}">
    <input type="hidden" name="email_lembaga" value="{{$recProposalByID->email_lembaga}}">
    <input type="hidden" name="fase" value="{{$fase}}">
        <div class="form-body" style="padding-top: 0;">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
            <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
            
            <h3 style="margin-top: 0;">Data Utama</h3>
            <hr>
            <div class="form-group">
                <label class="control-label col-md-3">Tanggal Proposal
                <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_prop" value="{{ $recProposalByID->tgl_prop }}" required>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">No Proposal
                    <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <input name="no_proposal" class="form-control" value="{{ $recProposalByID->no_proposal }}" required></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Judul Proposal
                    <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <textarea name="jdl_prop" class="form-control">{{ $recProposalByID->jdl_prop }}</textarea> </div>
            </div>
            <h3>Isi Proposal</h3>
            <hr>
            <div class="form-group">
                <label class="control-label col-md-3">Latar Belakang
                    <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <textarea name="latar_belakang" class="form-control wysihtml5" rows="10">{{ $recProposalByID->latar_belakang }}</textarea> </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Maksud Tujuan
                    <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <textarea name="maksud_tujuan" class="form-control wysihtml5" rows="10">{{ $recProposalByID->maksud_tujuan }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Keterangan
                    <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <textarea name="keterangan_prop" class="form-control wysihtml5" rows="10">{{ $recProposalByID->keterangan_prop }}</textarea>
                </div>
            </div>
            <h3>Nominal Permohonan</h3>
            <hr>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">Nominal Permohonan Bantuan (Rp)
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-4">
                        <input name="nominal_pengajuan" type="text" class="form-control input-numeral" value="{{ str_replace(".", ",", $recProposalByID->nominal_pengajuan) }}" /> </div>
                </div>
            </div>
            <br>
            <br>
            <h3>Data Pendukung</h3>
            <hr>
            <div class="form-group">
                <label class="control-label col-md-3">Surat Pernyataan Tanggung Jawab Bermeterai
                <span class="required"> * </span>
                </label>
                <div class="col-md-8">
                    @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawab')
                    @foreach($berkasPernyataanTanggungJawab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                        <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                    @endforeach
                    <br><br>
                    <div class="clearfix margin-top-12">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                    </div>
                    <input type="file" name="surat_pernyataan_tanggung_jawab" class="btn default form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-md-3">Struktur Pengurus
                <span class="required"> * </span>
                </label>
                <div class="col-md-8">
                    @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusan')
                    @foreach($berkasKepengurusan->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                        <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                    @endforeach
                    <br><br>
                    <div class="clearfix margin-top-12">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                    </div>
                    <input type="file" name="berkas_kepengurusan" class="btn default form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-md-3">Foto Kondisi Saat Ini
                <span class="required"> * </span>
                </label>
                <div class="col-md-8">
                    @inject('fotoProposal','BPKAD\Http\Entities\FotoProposal')
                    @foreach($fotoProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                    @endforeach
                    <br><br>
                    <div class="clearfix margin-top-10">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat JPG/JPEG/PNG dan Max 10MB </small></span>
                    </div>
                    <input type="file" name="photo_lembaga" class="btn default form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-md-3">Berkas Proposal
                <span class="required"> * </span>
                </label>
                <div class="col-md-8">
                    @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposal')
                    @foreach($berkasProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                    @endforeach
                    <br><br>
                    <div class="clearfix margin-top-10">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                    </div>
                    <input type="file" name="berkas_proposal" class="btn default form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-md-3">Berkas Rancangan Anggaran Biaya (RAB)
                <span class="required"> * </span>
                </label>
                <div class="col-md-8">
                    @inject('berkasRab','BPKAD\Http\Entities\BerkasRab')
                    @foreach($berkasRab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                        <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                    @endforeach
                    <br><br>
                    <div class="clearfix margin-top-10">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                    </div>
                    <input type="file" name="berkas_rab" class="btn default form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-md-3">Saldo Akhir Rekening Bank 
                <span class="required"> * </span>
                </label>
                <div class="col-md-8">
                    @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekening')
                    @foreach($berkasRekening->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                        <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                    @endforeach
                    <br><br>
                    <div class="clearfix margin-top-10">
                        <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                    </div>
                    <input type="file" name="berkas_rekening" class="btn default form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-md-3">Titik Peta
                    <span class="required"> * </span>
                </label>
                <div class="col-md-9">
                    <div class="portlet-body">
                        <div class="input-group">
                            <input type="text" class="form-control" id="pac-input" placeholder="Cari Alamat">
                            <span class="input-group-btn">
                                <button class="btn blue" id="gmap_geocoding_btn">
                                    <i class="fa fa-search"></i>
                            </span>
                        </div>
                        <div id="gmap_basic" class="gmaps"> </div>
                        <b>Latitude : </b>
                        <input type="text" class="form-control" name="latitude" id="latVal" value="{{ $recProposalByID->latitude }}" >
                        <b>Longitude : </b>
                        <input type="text" class="form-control" name="longitude" id="longVal" value="{{ $recProposalByID->longitude }}"  >
                    </div>
                </div>
            </div>
            <br>
        </div>
        @if($recProposalByID->status_kirim==0)
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green" id="btn-submit">Simpan Sebagai Draft</button>
                    <button type="button" onclick="javascript:window.history.back();" class="btn grey-salsa btn-outline">Batal</button>
                </div>
            </div>
        </div>
        @else
            <hr>
            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-6">
                    <label>
                        <b>Proposal Ini Telah Terkirim</b>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <a href="{{ route('member.proposal.index') }}" class="btn grey-salsa btn-outline">Kembali</a>
                </div>
            </div>
        </div>
        @endif
    </form>
    <!-- END FORM-->
</div>