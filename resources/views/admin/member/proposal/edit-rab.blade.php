<div class="tab-pane fade {{ ($status=='rencana')?'active in':'' }}" id="tab_1_1">
    <div class="col-md-12">
        <h3>Rencana Penggunaan Anggaran (Rp)</h3>
        <hr>
        @if($recProposalByID->nominal_pengajuan != $rencana_anggaran)
        <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
        @else
        <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
        @endif
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-3">Nominal Permohonan Bantuan (Rp)
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="nominal_pengajuan" type="text" class="form-control input-numeral3" value="{{ str_replace(".", ",", $recProposalByID->nominal_pengajuan) }}" disabled="" /> </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="form-group">
                <label class="control-label col-md-3">Nilai Input RAB (Rp)
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="asdasd" type="text" class="form-control input-numeral2" value="{{ str_replace(".", ",", $rencana_anggaran) }}" readonly="" />
                    <i style="display: block;padding-top: 10px;color: #9e9e9e;">(Nilai ini akan tampil setelah RAB telah di-input)</i>
                </div>
            </div>
        </div>
        <div class="table-toolbar" style="margin-top: 150px;">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="{{ route('member.proposal.create.rencana',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Rencana Kegiatan
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column">
            <thead>
                <tr>
                    <th> No. </th>
                    <th> Rencana Kegiatan/Rincian </th>
                    <th> Vol/Satuan (jumlah) </th>
                    <th> Harga Satuan (Rp) </th>
                    <th> Anggaran (Rp) </th>
                </tr>
            </thead>
            <tbody>
                @if(count($recRencanaByNoProp)==0)
                    <tr>
                        <td colspan="5"><i><center>data belum tersedia</center></i></td>
                    </tr>
                @else
                    @inject('metrics', 'BPKAD\Services\RincianService')
                    @foreach ($recRencanaByNoProp as $key=>$value)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><b><a href="{{route('member.proposal.edit.rencana',['id'=>$value->id,'fase'=>$fase])}}">{{$value->rencana}}</a></b></td>
                        <td></td>
                        <td></td>
                        <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                    </tr>
                    {!! $metrics->outputByRencanaID($value->id,$value->no_prop,$fase) !!}
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>