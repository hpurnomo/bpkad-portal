@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Naskah Perjanjian NPHD</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <div class="portlet-body form">
        	{{-- <div class="row">
                <div class="col-md-6">
	                <form class="btn-pdf-nphd" action="{{ route('ppkd.proposal.cetak.naskah.perjanjian.nphd.generateNPHDToPDF') }}" method="POST" style="float: left;">
		                <input type="hidden" name="_method" value="POST">
		                <input type="hidden" name="no_prop" value="{{ $no_prop }}">
		                <input type="hidden" name="fase" value="{{$fase}}">
		                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
		                @if($dataCNPHD['id']!="")
		                <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
		                @endif
		            </form>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('ppkd.proposal.manage',['fase'=>$fase]) }}" class="btn btn-default" style="float: right;">Kembali</a>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-md-6">
                    @if($dataCNPHD['id'])
	                <a href="{{ route('member.proposal.cetak.perjanjian.nphd',['id'=>$no_prop]) }}" class="btn red confirmLink"><i class="fa fa-download" aria-hidden="true"></i> Unduh Naskah Perjanjian NPHD</a>
                    @endif
                </div>
                <div class="col-md-6">
                    <a href="{{ route('member.proposal.show',['id'=>base64_encode($no_prop
                    	),'fase'=>$fase]) }}" class="btn btn-default" style="float: right;">Kembali</a>
                </div>
            </div>
            {{-- {{ dd($recProposalByID) }} --}}
            <form action="{{ route('member.proposal.naskah.perjanjian.nphd.update') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="no_prop" value="{{ $no_prop }}">
                <input type="hidden" name="fase" value="{{$fase}}">
                <center>
                   <h4>NASKAH PERJANJIAN HIBAH DAERAH DALAM BENTUK UANG <BR>PERJANJIAN <br><br> 
						ANTARA <br><br>
						PEMERINTAH PROVINSI DAERAH KHUSUS <br><br> 
						IBUKOTA JAKARTA <br><br>
						DAN <br><br>
						{{ strtoupper($recProposalByID->nm_lembaga) }} <br><br> 
						TENTANG <br><br>
						PEMBERIAN HIBAH DALAM BENTUK UANG
					</h4>
                </center>
                <p style="padding: 30px 30px 0 100px;">
                Pada hari ini ...................................
                <select name="hari1" style="display: none;">
                    <option value="Senin" {{($dataCNPHD['hari1']=='Senin')?'selected':''}}>Senin</option>
                    <option value="Selasa" {{($dataCNPHD['hari1']=='Selasa')?'selected':''}}>Selasa</option>
                    <option value="Rabu" {{($dataCNPHD['hari1']=='Rabu')?'selected':''}}>Rabu</option>
                    <option value="Kamis" {{($dataCNPHD['hari1']=='Kamis')?'selected':''}}>Kamis</option>
                    <option value="Jumat" {{($dataCNPHD['hari1']=='Jumat')?'selected':''}}>Jumat</option>
                </select>
                tanggal .........
                <select name="tanggal1" style="display: none;">
                    @for ($i = 1; $i <= 31; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select>
                bulan ............................................
                <select name="bulan1" style="display: none;">
                    <option value="Januari" {{($dataCNPHD['bulan1']=="Januari")?'selected':''}}>Januari</option>
                    <option value="Februari" {{($dataCNPHD['bulan1']=="Februari")?'selected':''}}>Februari</option>
                    <option value="Maret" {{($dataCNPHD['bulan1']=="Maret")?'selected':''}}>Maret</option>
                    <option value="April" {{($dataCNPHD['bulan1']=="April")?'selected':''}}>April</option>
                    <option value="Mei" {{($dataCNPHD['bulan1']=="Mei")?'selected':''}}>Mei</option>
                    <option value="Juni" {{($dataCNPHD['bulan1']=="Juni")?'selected':''}}>Juni</option>
                    <option value="Juli" {{($dataCNPHD['bulan1']=="Juli")?'selected':''}}>Juli</option>
                    <option value="Agustus" {{($dataCNPHD['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                    <option value="September" {{($dataCNPHD['bulan1']=="September")?'selected':''}}>September</option>
                    <option value="Oktober" {{($dataCNPHD['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                    <option value="November" {{($dataCNPHD['bulan1']=="November")?'selected':''}}>November</option>
                    <option value="Desember" {{($dataCNPHD['bulan1']=="Desember")?'selected':''}}>Desember</option>
                </select>
                tahun .................
                <select name="tahun1" style="display: none;">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> 
                yang bertanda tangan di bawah ini: 
                  <table width="100%">
                	<tr>
                		<td>I.</td>
                		<td style="padding: 0 200px 0 10px;">Nama : <input type="text" name="nama1" {{-- value="{{$dataCNPHD['nama1']}}" --}} value="Yanni Suryani, SE" readonly=""></td>
                	</tr>
                	<tr>
                		<td></td>
                		<td style="padding: 0 200px 0 10px;text-align: justify;">Dalam Jabatan Kepala Bidang Perbendaharaan dan Kas Daerah Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta selaku Kuasa Bendahara Umum Daerah berkantor di Jln. Medan Merdeka Selatan No. 8-9, Kota Administrasi Jakarta Pusat;<br>
                		Oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah Provinsi DKI Jakarta, untuk selanjutnya disebut PIHAK PERTAMA</td>
                	</tr>
                	<tr>
                		<td>II.</td>
                		<td style="padding: 0 200px 0 10px;">Nama : <input type="text" name="nama2" value="{{$dataCNPHD['nama2']}}"></td>
                	</tr>
                	<tr>
                		<td></td>
                		<td style="padding: 0 200px 0 10px;text-align: justify;">Jabatan <input type="text" name="jabatan1" value="{{$dataCNPHD['jabatan1']}}"> berkantor di Jln. <input type="text" name="alamat" value="{{$dataCNPHD['alamat']}}"> dalam hal ini menjalani jabatannya sesuai <textarea name="deskripsi_jabatan" rows="10" cols="117">{{$dataCNPHD['deskripsi_jabatan']}}</textarea>, 
		                oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah/Pemerintah Daerah/Kelompok Masyarakat/Organisasi Kemasyarakatan untuk selanjutnya disebut PIHAK KEDUA.</td>
                	</tr>
                </table>
                <p style="padding: 0 200px 0 100px;text-align: justify;">Bahwa masing-masing pihak bertindak dalam jabatannya sebagaimana tersebut di atas, secara bersama-sama disebut PARA PIHAK dengan terlebih dahulu memperhatikan ketentuan sebagai berikut :</p>
                  <table width="100%">
                	<tr>
                		<td style="vertical-align: top;">1.</td>
                		<td style="padding: 0 200px 0 10px;">Undang-Undang 	Nomor 29 Tahun 2007 tentang Pemerintahan Provinsi Daerah Khusus Ibukota Jakarta sebagai Ibukota Negara Kesatuan Republik Indonesia;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">2.</td>
                		<td style="padding: 0 200px 0 10px;">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Undang-Undang Nomor 9 Tahun 2015;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">3.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan Pemerintah Nomor 58 Tahun 2005 tentang Pengelolaan Keuangan Daerah;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">4.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan Menteri Dalam Negeri Nomor 13 Tahun 2006 tentang Pedoman Pengelolaan Keuangan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Peraturan Menteri Dalam Negeri Nomor 21 Tahun 2011;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">5.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan Daerah Nomor 5 Tahun 2007 tentang Pokok-pokok Pengelolaan Keuangan Daerah;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">6.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan Daerah Nomor 5 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Provinsi Daerah Khusus Ibukota Jakarta;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">7.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan <input type="text" name="peraturan4" value="{{$dataCNPHD['peraturan4']}}"> Nomor <input type="text" name="nomor4" value="{{$dataCNPHD['nomor4']}}"> Tahun 
                		<select name="tahun4">
		                    @for ($i = 2016; $i <= 2020; $i++)
		                        <option value="{{$i}}" {{($dataCNPHD['tahun4']==$i)?'selected':''}}>{{$i}}</option>
		                    @endfor
                            {{-- <option value="2016">2016</option> --}}
		                </select>
		                tentang <textarea name="tentang4" cols="100">{{$dataCNPHD['tentang4']}}</textarea>;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">8.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan Gubernur Nomor 142 Tahun 2013 tentang Sistem dan Prosedur Pengelolaan Keuangan Daerah;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">9.</td>
                		<td style="padding: 0 200px 0 10px;">Peraturan Gubernur Nomor 142 tahun 2018 tentang Pedoman Pemberian Hibah dan Bantuan Sosial yang Bersumber dari Anggaran Pendapatan dan Belanja Daerah sebagaimana telah beberapa kali diubah terakhir dengan Peraturan Gubernur Nomor 20 tahun 2020;</td>
                	</tr>
                	<tr>
                		<td style="vertical-align: top;">10.</td>
                		<td style="padding: 0 200px 0 10px;">Keputusan 	Gubernur 	Nomor <input type="text" name="nomor6" value="{{$dataCNPHD['nomor6']}}"> 
                		Tahun
                		<select name="tahun6">
		                    @for ($i = 2016; $i <= 2020; $i++)
		                        <option value="{{$i}}" {{($dataCNPHD['tahun6']==$i)?'selected':''}}>{{$i}}</option>
		                    @endfor
		                </select> 
		                  tentang  <textarea name="tentang6" cols="100">{{$dataCNPHD['tentang6']}}</textarea>;</td>
                	</tr>
                </table>
                
                <p style="padding: 0 200px 0 100px;text-align: justify;">Bahwa berdasarkan hal tersebut dan sesuai dengan rekomendasi <input type="text" name="namaSKPD" value="{{$dataCNPHD['namaSKPD']}}">  Nomor <input type="text" name="nomor7" value="{{$dataCNPHD['nomor7']}}"> tanggal 
                <select name="tanggal7">
                    @for ($i = 1; $i <= 31; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tanggal7']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> 
                bulan
                <select name="bulan7">
                    <option value="Januari" {{($dataCNPHD['bulan7']=="Januari")?'selected':''}}>Januari</option>
                    <option value="Februari" {{($dataCNPHD['bulan7']=="Februari")?'selected':''}}>Februari</option>
                    <option value="Maret" {{($dataCNPHD['bulan7']=="Maret")?'selected':''}}>Maret</option>
                    <option value="April" {{($dataCNPHD['bulan7']=="April")?'selected':''}}>April</option>
                    <option value="Mei" {{($dataCNPHD['bulan7']=="Mei")?'selected':''}}>Mei</option>
                    <option value="Juni" {{($dataCNPHD['bulan7']=="Juni")?'selected':''}}>Juni</option>
                    <option value="Juli" {{($dataCNPHD['bulan7']=="Juli")?'selected':''}}>Juli</option>
                    <option value="Agustus" {{($dataCNPHD['bulan7']=="Agustus")?'selected':''}}>Agustus</option>
                    <option value="September" {{($dataCNPHD['bulan7']=="September")?'selected':''}}>September</option>
                    <option value="Oktober" {{($dataCNPHD['bulan7']=="Oktober")?'selected':''}}>Oktober</option>
                    <option value="November" {{($dataCNPHD['bulan7']=="November")?'selected':''}}>November</option>
                    <option value="Desember" {{($dataCNPHD['bulan7']=="Desember")?'selected':''}}>Desember</option>
                </select>
                tahun
                <select name="tahun7">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCNPHD['tahun7']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> ,
                PARA PIHAK sepakat untuk melakukan Perjanjian Hibah dalam bentuk uang, dengan syarat dan ketentuan sebagai berikut :</p>
                
               <center>
                    Pasal 1
                    <br><br>
                    JUMLAH DAN TUJUAN HIBAH  
                </center>
                <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;"> PIHAK PERTAMA memberikan Hibah berupa uang kepada PIHAK KEDUA sebagaimana PIHAK KEDUA menerima dari PIHAK PERTAMA senilai Rp <input type="text" name="senilai_angka" value="{{$dataCNPHD['senilai_angka']}}">( <input type="text" name="senilai_teks" value="{{$dataCNPHD['senilai_teks']}}"> rupiah ).</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;"> Hibah sebagaimana dimaksud pada ayat (1) dipergunakan sesuai dengan Rencana Penggunaan Hibah/Proposal yang merupakan bagian yang tidak terpisahkan dari naskah perjanjian Hibah daerah ini.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Penggunaan belanja hibah sebagaimana dimaksud pada ayat (2) bertujuan untuk</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"></td>
                        <td><textarea name="kegiatan2" class="wysihtml5" rows="6">{!! $dataCNPHD['kegiatan2'] !!}</textarea></td>
                    </tr>
                </table>
                <br>
                <center>
                    Pasal 2
                    <br><br>
                    PENGGUNAAN
                </center>
                <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">PIHAK KEDUA menggunakan belanja Hibah berupa uang sebagaimana dimaksud dalam Pasal 2 ayat (1) sesuai dengan Rencana Penggunaan Rencana Penggunaan Hibah/Proposal.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Belanja Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk :</td>
                    </tr>
                </table>
                <br>
                @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                <table width="90%" border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;">
                    <tr>
                        <td style="padding: 0 200px 0 10px;">                
                            <table border="1" cellpadding="1" cellspacing="1" width="100%">
                                <thead>
                                    <tr>
                                        <th><center> No. </center></th>
                                        <th><center> Uraian Kegiatan/Penggunaan </center></th>
                                        <th><center> Jumlah Rp. </center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total=0; ?>
                                    @foreach ($recRencanaByNoProp as $key=>$value)
                                    <tr>
                                        <td><center>{{$key+1}}</center></td>
                                        <td style="padding: 10px;">{{$value->rencana}}</td>
                                        <td style="padding: 10px;">{{ number_format($value->rencana_anggaran,2,",",".")}}</td>
                                    </tr>
                                    <?php $total = $total+$value->rencana_anggaran; ?>
                                    {!! $metrics->outputByRencanaIDForNPHD($value->id) !!}
                                    @endforeach

                                    <tr>
                                        <td><center> </center></td>
                                        <td style="padding: 10px;text-align: right;"> JUMLAH KESELURUHAN </td>
                                        <td style="padding: 10px;">{{ number_format($total,2,",",".")}}</td>
                                    </tr>

                                    <tr>
                                        <td><center> </center></td>
                                        <td style="padding: 10px;" colspan="2">   
                                             <textarea id="terbilangA" class="form-control" rows="1"> {{ $dataCNPHD['totalTerbilangKegiatan1'] }} </textarea> 
                                        </td> 
                                    </tr>

                                </tbody>
                            </table>
                            

                            <input id="hasilTerbilang" value="" name="terbilangTotal" type="hidden">   
                            <textarea name="kegiatan1" class="form-control" rows="6" style="display: none;">
                                <table border="1" cellpadding="1" cellspacing="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th><center> No. </center></th>
                                            <th><center> Uraian Kegiatan/Penggunaan </center></th>
                                            <th><center> Jumlah Rp. </center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($recRencanaByNoProp as $key=>$value)
                                        <tr>
                                            <td><center>{{$key+1}}</center></td>
                                            <td style="padding: 10px;">{{$value->rencana}}</td>
                                            <td style="padding: 10px;">{{ number_format($value->rencana_anggaran,2,",",".")}}</td>
                                        </tr>
                                        {!! $metrics->outputByRencanaIDForNPHD($value->id) !!}  
                                        @endforeach 
                                        <tr>
                                            <td><center> </center></td>
                                            <td style="padding: 10px;text-align: right;"> JUMLAH KESELURUHAN </td>
                                            <td style="padding: 10px;">{{ number_format($total,2,",",".")}}</td>
                                        </tr>  

                                    </tbody>
                                </table>
                                
                            </textarea>
                        </td>
                    </tr>
                    {{-- <tr>
                        <td style="vertical-align: top;">b.</td>
                        <td style="padding: 0 200px 0 10px;"><input type="text" name="kegiatan2" value="{{$dataCNPHD['kegiatan2']}}" style="width: 600px;"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">c.</td>
                        <td style="padding: 0 200px 0 10px;"><input type="text" name="kegiatan3" value="{{$dataCNPHD['kegiatan3']}}" style="width: 600px;"></td>
                    </tr> --}}
                </table>

                  <br>
        <center>
            Pasal 3
            <br><br>
            HAK DAN KEWAJIBAN PIHAK KEDUA
        </center> 
                <br>
                <br>
                <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">PIHAK KEDUA menerima dana Hibah dari PIHAK PERTAMA untuk digunakan sesuai dengan penggunaan sebagaimana dimaksud dalam Pasal 2.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Dalam hal PIHAK KEDUA telah menerima Hibah 1 (satu) tahun sebelumnya, pencairan dan penyaluran dana Hibah sebagaimana dimaksud pada ayat (1) akan diperhitungkan sisa dana penggunaan Hibah yang tertuang dalam Laporan Penggunaan Hibah tahun sebelumnya yaitu sebesar Rp. {{ $dataCNPHD['total1'] }} </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">PIHAK KEDUA wajib menandatangani Pakta Integritas yang menyatakan bahwa Hibah yang diterimakan digunakan sesuai dengan Naskah Perjanjian Hibah daerah ini. </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(4)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;"> PIHAK KEDUA selaku penerima Hibah dan objek pemeriksaan, wajib menyimpan bukti-bukti pengeluaran yang lengkap dan sah sesuai peraturan perundang-undangan. </td> 
                    </tr>

                </table>
                <br>
                <center>
                    Pasal 4
                    <br><br>
                    HAK DAN KEWAJIBAN PIHAK PERTAMA
                </center>
                <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;"> PIHAK PERTAMA wajib memberikan dana Hibah kepada PIHAK KEDUA melalui transfer ke rekening Bank PIHAK KEDUA apabila seluruh persyaratan dan kelengkapan berkas pengajuan dana Hibah telah dipenuhi oleh PIHAK KEDUA. </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;"> Dalam hal PIHAK KEDUA telah menerima Hibah 1 (satu) tahun sebelumnya, pencairan dan penyaluran dana Hibah sebagaimana dimaksud pada ayat (1) dilakukan setelah diperhitungkan sisa dana penggunaan Hibah yang tertuang dalam laporan penggunaan Hibah tahun sebelumnya yaitu sebesar Rp. {{ $dataCNPHD['total2'] }}</td>
                    </tr>

                     <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">  PIHAK PERTAMA Menunda pencairan belanja Hibah apabila PIHAK KEDUA tidak/ belum memenuhi persyaratan dan kelengkapan berkas pengajuan dana sesuai dengan peraturan perundang-undangan. </td>
                    </tr>
                </table>
                <br>
                <center>
                    Pasal 5
                    <br><br>
                    TATA CARA PELAPORAN 
                </center>
                <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">PIHAK KEDUA membuat dan menyampaikan laporan penggunaan Hibah kepada Gubernur melalui Kepala SKPD/UKPD Pemberi Rekomendasi dengan tembusan Kepala BPKD selaku PPKD paling lambat 1 (satu) bulan berikutnya setelah pelaksanaan kegiatan selesai atau tanggal 10 Maret Tahun Anggaran berikutnya. 
                    </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">) Laporan penggunaan Hibah sebagaimana dimaksud dilengkapi dengan surat pernyataan tanggung jawab bermeterai cukup dari PIHAK KEDUA yang menyatakan bahwa Hibah yang telah diterima telah dipergunakan sesuai dengan NPHD. </td>
                    </tr>
                </table>
                <br>

                 <center>
                    Pasal 
                    <br><br>
                    AUDIT
                </center>
                <br>
                  <table width="100%">
                    <tr> 
                        <td>Dalam kondisi tertentu penerima Hibah dan Bantuan Sosial dapat dilakukan audit oleh aparat pengawas fungsional. 
                    </td> 
                </table>
                <br>

                 <table width="100%">
                    <tr> 
                        <td> <p>Demikian Perjanjian Hibah ini dibuat dan ditandatangani di Jakarta pada hari dan tanggal tersebut di atas dalam rangkap 2 (dua), masing-masing bermeterai cukup dan mempunyai kekuatan hukum yang sama, 1 (satu) eksemplar untuk PIHAK PERTAMA dan 1 (satu) eksemplar untuk PIHAK KEDUA. </p> 
                    </td> 
                </table>
                
                <br>
				<table width="100%">
                    <tr>
                        <td width="30%">
                            <center>
                            <h4>PIHAK KEDUA</h4>
                            <input type="text" name="jabatan2" value="{{$dataCNPHD['jabatan2']}}">,
                            <br><br><br>
                            <h4><input type="text" name="nama_pihak_kedua" value="{{$dataCNPHD['nama_pihak_kedua']}}"></h4>
                            <h4>NIP <input type="text" name="nip_pihak_kedua" value="{{$dataCNPHD['nip_pihak_kedua']}}"></h4>
                            </center>
                        </td>
                        <td width="30%">
                            <center>
                                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                            </center>
                        </td>
                        <td><center>
                            <h4>PIHAK PERTAMA <br>KEPALA BIDANG PERBENDAHARAAN DAN KAS DAERAH<br> BADAN PENGELOLA KEUANGAN DAERAH PROVINSI DKI JAKARTA<br>selaku <br>KUASA BENDAHARA UMUM DAERAH,</h4>
                            <br><br><br>
                            <h4><input type="text" name="nama_kepala" {{-- value="{{$dataCNPHD['nama_kepala']}}" --}} value="YANNI SURYANI, SE" readonly=""></h4>
                            <h4>NIP <input type="text" name="nip_kepala" {{-- value="{{$dataCNPHD['nip_kepala']}}" --}} value="196101261987032002" readonly=""></h4>
                            </center>
                        </td>
                    </tr>
                </table>

                <hr>
                @if($dataCNPHD['is_generate']==0)
                <button type="submit" class="btn green" id="btn-simpan-nphd">Simpan</button>
                @endif
            </form>
            
            <div class="row">
	            
            </div>
        </div>
        
    </div>
    <!-- END CONTENT BODY -->
</div>

{{-- box dialog confirmation --}}
<div id="dialog" title="Confirmation Required" style="display: none;">
    Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui data cetak naskah perjanjian NPHD tidak dapat di update kembali.
</div>

@stop
@section('javascript')
    @parent
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(function(){
            $(".btn-pdf-nphd").on("submit", function(){
	            $("#btn-simpan-nphd").hide();
	            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui data cetak naskah perjanjian NPHD tidak dapat di update kembali. ");
	        });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#dialog").dialog({
                autoOpen: false,
                modal: true
            });
        });

        $(".confirmLink").click(function(e) {
            e.preventDefault();
            var targetUrl = $(this).attr("href");

            $("#dialog").dialog({
              buttons : {
                "Ok" : function() {
                  window.location.href = targetUrl;
                  $(this).dialog("close");
                },
                "Cancel" : function() {
                  $(this).dialog("close");
                }
              }
            });

            $("#dialog").dialog("open");
        });


    </script>
    @if( !empty(Session::get('status')) )
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
@stop