@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Buat Rincian Anggaran & Biaya</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <div class="portlet light bordered">
            <div class="portlet-title">
            	<div class="row">
                    <div class="col-xs-6">
                        <div class="caption" style="font-size: 16px;">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Form Ubah Rincian Anggaran & Biaya</span>
                        </div>
                    </div>
                    <div class="col-xs-6" style="text-align:right;">
                        <a href="{{route('member.proposal.edit',['id'=>$no_prop,'fase'=>$fase,'status'=>'rencana'])}}" class="btn btn-default">Kembali</a>
                    </div>
                </div>
            </div>
            {{-- {{dd($recTrxRincianByID)}} --}}
            <div class="portlet-body form">
                @include('admin.part.alert')
                <div class="row">
                    <div class="col-md-12" style="text-align: right;">
                        <form class="delete" action="{{ route('member.proposal.delete.rincian') }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" value="{{ $recTrxRincianByID->id }}">
                            <input type="hidden" name="rencanaID" value="{{ $recTrxRincianByID->rencanaID }}">
                            <input type="hidden" name="no_prop" value="{{ $no_prop }}">
                    		<input type="hidden" name="fase" value="{{ $fase }}">
                            <button type="submit" class="btn red-mint" value="Delete"><i class="icon-trash"></i> Hapus Rincian Anggaran Ini</button>
                        </form>
                    </div>
                </div>
                <!-- BEGIN FORM-->
                <form action="{{ route('member.proposal.update.rincian') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$recTrxRincianByID->id}}">
                    <input type="hidden" name="rencanaID" value="{{$recTrxRincianByID->rencanaID}}">
                    <input type="hidden" name="no_prop" value="{{ $no_prop }}">
                    <input type="hidden" name="fase" value="{{ $fase }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rincian
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <textarea name="rincian" class="form-control" rows="3" required="">{{$recTrxRincianByID->rincian}}</textarea> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Volume
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input type="text" name="volume1" class="form-control input-numeral1" required="" value="{{ str_replace(".", ",", $recTrxRincianByID->volume1)}}"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Satuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" name="satuan1" class="form-control" required="" value="{{$recTrxRincianByID->satuan1}}"> 
                                       	<i style="display: block;padding-top: 10px;color: #9e9e9e;">contoh: orang, porsi, meter, dst...</i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                    </label>
                                    <div class="col-md-4">
                                        <hr style="width: 70%;display: inline-flex;"> x (kali)
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Volume
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input type="text" name="volume2" class="form-control input-numeral2" required="" value="{{ str_replace(".", ",", $recTrxRincianByID->volume2)}}"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Satuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" name="satuan2" class="form-control" required="" value="{{$recTrxRincianByID->satuan2}}"> 
                                       	<i style="display: block;padding-top: 10px;color: #9e9e9e;">contoh: kali, bulan, sesi, dst...</i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                    </label>
                                    <div class="col-md-4">
                                        <hr style="width: 70%;display: inline-flex;"> x (kali)
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Harga Satuan (Rp)
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" name="harga_satuan" class="form-control input-numeral3" required="" value="{{ str_replace(".", ",", $recTrxRincianByID->harga_satuan)}}"> 
                                    </div>
                                </div>

                                @if(Auth::user()->group_id == 10) 
                                     <div class="form-group">
                                        <label class="control-label col-md-3"> Harga Satuan Rekomendasi (Rp)
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="harga_satuan_skpd" class="form-control input-numeral3" required="" value="{{ str_replace(".", ",", $recTrxRincianByID->harga_satuan_skpd)}}"> 
                                        </div>
                                    </div>

                                @endif


                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6">
                                        <button type="submit" class="btn green">Ubah</button>
                                        <button type="reset" class="btn default">Batal</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral1', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral2', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral3', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            $(".delete").on("submit", function(){
                return confirm("Anda yakin menghapus rencana kegiatan ini?");
            });
        });
    </script>
@stop