@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.manage',['fase'=>'pengusulan']) }}">Pengusulan Proposal {{ Carbon\Carbon::now()->year+1 }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Ubah Usulan Proposal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
            <!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="caption" style="font-size: 16px;">
                                    <i class="icon-settings font-red"></i>
                                    <span class="caption-subject font-red sbold uppercase">Form Ubah Usulan Proposal</span>
                                </div>
                            </div>
                            {{-- <div class="col-xs-6" style="text-align:right;">
                                <form class="delete" action="{{ route('member.proposal.delete') }}" method="POST">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                                    @if($recProposalByID->status_kirim==0)
                                    <button type="submit" class="btn red-mint" value="Delete"><i class="icon-trash"></i> Hapus Proposal Ini</button>
                                    @endif
                                </form>
                            </div> --}}
                        </div>
                    </div>
                    {{-- {{dd($recProposalByID)}} --}}
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{ route('member.proposal.perbaikan.update') }}" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="tahun_anggaran" value="{{ $recProposalByID->tahun_anggaran }}">
                        <input type="hidden" name="noper" value="{{ $recProposalByID->noper }}">
                        <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                        <input type="hidden" name="no_lembaga" value="{{ Auth::user()->no_lembaga }}">
                        <input type="hidden" name="upb" value="{{ Auth::user()->user_id }}">
                        <input type="hidden" name="upd" value="{{ Carbon\Carbon::now() }}">
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                {{-- <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Pengajuan
                                    </label>
                                    <div class="col-md-2">
                                        <input name="" type="text" data-required="1" class="form-control" value="{{ date('d M Y',strtotime(Carbon\Carbon::now())) }}" readonly="" /> </div>
                                </div> --}}
                                <h3>Data Utama</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3" style="color:red;">Tanggal Proposal
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_prop" value="{{ $recProposalByID->tgl_prop }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3" style="color:red;">No Proposal
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input name="no_proposal" class="form-control" value="{{ $recProposalByID->no_proposal }}" required></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Judul Proposal
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="jdl_prop" class="form-control" disabled>{{ $recProposalByID->jdl_prop }}</textarea> </div>
                                </div>
                                <h3>Isi Proposal</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Latar Belakang
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="latar_belakang" class="form-control" rows="10" disabled>{{ $recProposalByID->latar_belakang }}</textarea> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Maksud Tujuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="maksud_tujuan" class="form-control" rows="10" disabled>{{ $recProposalByID->maksud_tujuan }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rencana Penggunaan Bantuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="keterangan_prop" class="form-control" rows="5" disabled>{{ $recProposalByID->keterangan_prop }}</textarea>
                                    </div>
                                </div>
                                <h3>Rencana Penggunaan Anggaran</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nominal Permohonan Bantuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nominal_pengajuan" type="text" class="form-control only-numeric" value="{{ $recProposalByID->nominal_pengajuan }}" disabled/> </div>
                                </div>
                                <h3>Data Pendukung</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3" style="color:red;">Surat Pernyataan Tanggung Jawab Bermeterai
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawab')
                                            @foreach($berkasPernyataanTanggungJawab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" download="">{{ $value->file_name }}</a>
                                            @endforeach
                                            <br><br>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Unggah Dokumen </span>
                                                    <span class="fileinput-exists"> Ganti </span>
                                                    <input type="file" name="surat_pernyataan_tanggung_jawab"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Struktur Pengurus
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusan')
                                            @foreach($berkasKepengurusan->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" download="">{{ $value->file_name }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Foto Kondisi Saat Ini
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            @inject('fotoProposal','BPKAD\Http\Entities\FotoProposal')
                                            @foreach($fotoProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" download="">{{ $value->file_name }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Berkas Proposal
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposal')
                                            @foreach($berkasProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" download="">{{ $value->file_name }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Berkas Rancangan Anggaran Biaya (RAB)
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            @inject('berkasRab','BPKAD\Http\Entities\BerkasRab')
                                            @foreach($berkasRab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" download="">{{ $value->file_name }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Saldo Akhir Rekening Bank 
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekening')
                                            @foreach($berkasRekening->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" download="">{{ $value->file_name }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Titik Peta
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="portlet-body">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                <span class="input-group-btn">
                                                    <button class="btn blue" id="gmap_geocoding_btn">
                                                        <i class="fa fa-search"></i>
                                                </span>
                                            </div>
                                            <div id="gmap_basic" class="gmaps"> </div>
                                            <b>Latitude : </b>
                                            <input type="text" class="form-control" name="latitude" id="latVal" value="{{ $recProposalByID->latitude }}" >
                                            <b>Longitude : </b>
                                            <input type="text" class="form-control" name="longitude" id="longVal" value="{{ $recProposalByID->longitude }}" >
                                        </div>
                                    </div>
                                </div>
                            @if($recProposalByID->status_kirim==0)
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-6">
                                        <label>
                                            <input type="checkbox" name="status_kirim" id="status-kirim"> <b>Anda Setuju Kirim Langsung Proposal Ke SKPD Koordinator?</b><i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">(Proposal yang telah dikirim tidak dapat di-ubah, Karna akan di-review oleh SKPD Koordinator)</i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green" id="btn-submit">Simpan Sebagai Draf</button>
                                        <button type="button" onclick="javascript:window.history.back();" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                            @else
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-6">
                                        <label>
                                            <b>Proposal Ini Telah Terkirim</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green" id="btn-submit">Perbaiki</button>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });

        $(function(){
            $('#status-kirim').on('click',function(){
                if($('#status-kirim:checked').length == 1){
                    $('#btn-submit').html('Kirim');
                }
                else{
                    $('#btn-submit').html('Simpan Sebagai Draf');   
                }
            });
        });

        $(function(){
            $(".delete").on("submit", function(){
                return confirm("Anda yakin menghapus proposal ini?");
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = {{ ($recProposalByID->latitude==NULL)?'0':$recProposalByID->latitude }};
            var longValue = {{ ($recProposalByID->longitude==NULL)?'0':$recProposalByID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
@stop