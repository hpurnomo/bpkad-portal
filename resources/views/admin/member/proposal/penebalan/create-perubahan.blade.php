@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.manage',['fase'=>'perubahan']) }}">Perubahan Proposal {{ Carbon\Carbon::now()->year }}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Buat Usulan Proposal Perubahan Baru</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="caption" style="font-size: 16px;">
                                    <i class="icon-settings font-red"></i>
                                    <span class="caption-subject font-red sbold uppercase">Form Buat Usulan Proposal Perubahan Baru</span>
                                </div>
                            </div>
                            <div class="col-xs-6" style="text-align:right;">
                                <a href="{{route('member.proposal.manage',['fase'=>'perubahan'])}}" class="btn btn-default">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_0" data-toggle="tab">1. Data Proposal </a>
                            </li>
                            <li>
                                <a href="#tab_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            
                            {{-- Detil Lembaga --}}
                            <div class="tab-pane fade active in" id="tab_1_0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN FORM-->
                                        <form action="{{ route('member.proposal.store') }}" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="tahun_anggaran" value="{{ $tahun_anggaran }}">
                                        <input type="hidden" name="noper" value="{{ $noper }}">
                                        <input type="hidden" name="no_skpd" value="{{ Auth::user()->no_skpd }}">
                                        <input type="hidden" name="no_lembaga" value="{{ Auth::user()->no_lembaga }}">
                                        <input type="hidden" name="crb" value="{{ Auth::user()->user_id }}">
                                        <input type="hidden" name="crd" value="{{ Carbon\Carbon::now() }}">
                                        <input type="hidden" name="real_name" value="{{ Auth::user()->real_name }}">
                                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                                        <input type="hidden" name="fase" value="{{ $fase }}">
                                            <div class="form-body">
                                                <div class="alert alert-danger display-hide">
                                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                                <div class="alert alert-success display-hide">
                                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                                {{-- <div class="form-group">
                                                    <label class="control-label col-md-3">Tanggal Pengajuan
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input name="" type="text" data-required="1" class="form-control" value="{{ date('d M Y',strtotime(Carbon\Carbon::now())) }}" readonly="" /> </div>
                                                </div> --}}
                                                <h3>Data Utama</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Tanggal Proposal
                                                    <span class="required" aria-required="true"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control" readonly name="tgl_prop" value="{{ old('tgl_prop') }}" required>
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                        <span class="help-block"> Pilih tanggal </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">No Proposal
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input name="no_proposal" class="form-control" required></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Judul Proposal
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="jdl_prop" class="form-control" required></textarea> </div>
                                                </div>
                                                <h3>Isi Proposal</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Latar Belakang
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="latar_belakang" class="form-control" rows="10"></textarea> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Maksud Tujuan
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="maksud_tujuan" class="form-control" rows="10"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Keterangan
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="keterangan_prop" class="form-control" rows="5"></textarea>
                                                    </div>
                                                </div>
                                                <h3>Rencana Penggunaan Anggaran</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Nominal Permohonan Bantuan (Rp)
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="nominal_pengajuan" type="text" class="form-control input-numeral" /> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Nilai Input RAB (Rp)
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="nominal_pengajuan_vrab" type="text" class="form-control" readonly="" />
                                                        <i style="display: block;padding-top: 10px;color: #9e9e9e;">(Nilai ini akan tampil setelah RAB telah di-input)</i>
                                                    </div>
                                                </div>
                                                <h3>Data Pendukung</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Surat Pernyataan Tanggung Jawab Bermeterai
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="file" name="surat_pernyataan_tanggung_jawab" class="btn default form-control">
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-warning"> <small>Info : File disarankan berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Struktur Pengurus
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="file" name="berkas_kepengurusan" class="btn default form-control">
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-warning"> <small>Info : File disarankan berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Foto Kondisi Saat Ini
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="photo_lembaga" required=""> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-warning"> <small>Info : File disarankan berformat JPG/PNG/PDF Max 10MB </small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Berkas Proposal
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="file" name="berkas_proposal" class="btn default form-control">
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-warning"> <small>Info : File disarankan berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Berkas Rancangan Anggaran Biaya (RAB)
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="file" name="berkas_rab" class="btn default form-control">
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-warning"> <small>Info : File disarankan berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Saldo Akhir Rekening Bank 
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="file" name="berkas_rekening" class="btn default form-control">
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-warning"> <small>Info : File disarankan berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Titik Peta
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="portlet-body">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                                <span class="input-group-btn">
                                                                    <button class="btn blue" id="gmap_geocoding_btn">
                                                                        <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                            <div id="gmap_basic" class="gmaps"> </div>
                                                            <b>Latitude : </b>
                                                            <input type="text" class="form-control" name="latitude" id="latVal" value="" >
                                                            <b>Longitude : </b>
                                                            <input type="text" class="form-control" name="longitude" id="longVal" value="" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="col-md-6">
                                                        <label>
                                                            <input type="checkbox" name="status_kirim" id="status-kirim"> <b>Anda Setuju Kirim Langsung Proposal Ke SKPD Koordinator?</b><i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">(Proposal yang telah dikirim tidak dapat di-ubah, Karna akan di-review oleh SKPD Koordinator)</i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green" id="btn-submit">Simpan Sebagai Draf</button>
                                                        <button type="button" onclick="javascript:window.history.back();" class="btn grey-salsa btn-outline">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                            
                            {{-- Kelengkapan Administrasi --}}
                            <div class="tab-pane fade" id="tab_1_1">
                                <i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">Anda diwajibkan menginput Data Proposal sebelum dapat menginput daftar Rencana Penggunaan Bantuan.</i>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });

        $(function(){
            $('#status-kirim').on('click',function(){
                if($('#status-kirim:checked').length == 1){
                    $('#btn-submit').html('Kirim');
                }
                else{
                    $('#btn-submit').html('Simpan Sebagai Draf');   
                }
            });
        })
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = '';
            var longValue = '';
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop