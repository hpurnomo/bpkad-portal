@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Pakta Integritas {{ strtoupper($tahap) }}</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-6">
                    @if($dataCPI)
	                <a href="{{ route('member.proposal.penebalan.cetak.pakta.integritas',['uuid'=>$uuid,'fase'=>$fase,'tahap'=>$tahap]) }}" class="btn red confirmLink btn-pdf-nphd"><i class="fa fa-download" aria-hidden="true"></i> Unduh Pakta Integritas</a>
                    @endif
                </div>
                <div class="col-md-6">
                    <a href="{{ route('member.proposal.show',['id'=>base64_encode($no_prop
                    	),'fase'=>$fase]) }}" class="btn btn-default" style="float: right;">Kembali</a>
                </div>
            </div>
            {{-- {{ dd($recProposalByID) }} --}}
            {{-- {{ dd($dataCPI) }} --}}
            <form action="{{ route('member.proposal.penebalan.pakta.integritas.update') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="no_prop" value="{{ $no_prop }}">
                <input type="hidden" name="fase" value="{{$fase}}">
                <input type="hidden" name="tahap" value="{{$tahap}}">
                <input type="hidden" name="tahap" value="{{$tahap}}">
                <input type="hidden" name="uuid" value="{{$uuid}}">
                <center>
                    <h4>KOP LEMBAGA <br><br> 
						PAKTA INTEGRITAS <br><br>
					</h4>
                </center>
                <p style="padding: 30px 30px 0 100px;">
                Sesuai Peraturan Menteri Dalam Negeri Nomor 
                <input type="text" name="nomor1" value="{{ $dataCPI['nomor1'] }}"> 
                Tahun 
                <select name="tahun1">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCPI['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> 
                dan Peraturan Gubernur Nomor 
                <input type="text" name="nomor2" value="{{ $dataCPI['nomor2'] }}"> 
                Tahun 
                <select name="tahun2">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCPI['tahun2']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select>, dengan ini kami yang bertanda tangan di bawah ini :
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;width: 90%">
                	<tr>
                		<td width="200">Nama</td>
                		<td> : </td>
                		<td style="padding: 0 200px 0 10px;"><input type="text" name="nama_pemohon" value="{{ $dataCPI['nama_pemohon'] }}" size="71"></td>
                	</tr>
                	<tr>
                		<td width="200">No. KTP/NIP/NRP *)</td>
                		<td> : </td>
                		<td style="padding: 0 200px 0 10px;"><input type="text" name="no_pemohon" value="{{ $dataCPI['no_pemohon'] }}" size="71"></td>
                	</tr>
                	<tr>
                		<td width="200">Jabatan</td>
                		<td> : </td>
                		<td style="padding: 0 200px 0 10px;"><input type="text" name="jabatan_pemohon" value="{{ $dataCPI['jabatan_pemohon'] }}" size="71"></td>
                	</tr>
                	<tr>
                		<td width="200">Nama Lembaga **)</td>
                		<td> : </td>
                		<td style="padding: 0 200px 0 10px;"><textarea name="nama_lembaga" readonly="" rows="3" cols="70">{{ $recProposalByID->nm_lembaga }}</textarea>
                		</td>
                	</tr>
                	<tr>
                		<td width="200">Alamat Lembaga</td>
                		<td> : </td>
                		<td style="padding: 0 200px 0 10px;"><textarea name="alamat_lembaga" readonly="" rows="3" cols="70">{{ $recProposalByID->alamatLembaga }} RT/RW {{ $recProposalByID->namaRT }}/{{ $recProposalByID->namaRW }} Kel.{{ $recProposalByID->nmkel }} Kec.{{ $recProposalByID->nmkec }} Kota.{{ $recProposalByID->nmkota }}  
                        
                         </textarea></td>
                	</tr>
                </table>
                <br>
                 <p style="padding: 0 200px 0 100px;text-align: justify;">Sebagai penerima 
                 <select name="text1">
                    <option value="bantuan sosial" {{($dataCPI['text1']=='bantuan sosial')?'selected':''}}>bantuan sosial</option>
                    <option value="hibah" {{($dataCPI['text1']=='hibah')?'selected':''}}>hibah</option>
                </select> dalam bentuk uang dari Pemerintah Provinsi DKI Jakarta Tahun Anggaran 
                <select name="tahun_anggaran1">
                    @for ($i = 2016; $i <= 2020; $i++)
                        <option value="{{$i}}" {{($dataCPI['tahun_anggaran1']==$i)?'selected':''}}>{{$i}}</option>
                    @endfor
                </select> sebesar Rp.<input type="text" name="nominal" value="{{ $dataCPI['nominal'] }}"> (<input type="text" name="terbilang" value="{{ $dataCPI['terbilang'] }}"> rupiah) yang akan digunakan untuk kegiatan sebagai berikut :</p>
                <br>
                @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;width: 90%">
                    <tr>
                        <td style="padding: 0 200px 0 10px;">                
                            <table border="1" cellpadding="1" cellspacing="1" width="100%">
                                <thead>
                                    <tr>
                                        <th><center> No. </center></th>
                                        <th><center> Uraian Kegiatan/Penggunaan </center></th>
                                        <th><center> Jumlah Rp. </center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total=0; ?>
                                    @foreach ($recRencanaByNoProp as $key=>$value)
                                    <tr>
                                        <td><center>{{$key+1}}</center></td>
                                        <td style="padding: 10px;">{{$value->rencana}}</td>
                                        <td style="padding: 10px;">{{ number_format($value->rencana_anggaran,2,",",".")}}</td>
                                    </tr>
                                    <?php $total = $total + $value->rencana_anggaran; ?>
                                    {!! $metrics->outputByRencanaIDForNPHD($value->id) !!}
                                    @endforeach 
                                    <tr>
                                        <td><center> </center></td>
                                        <td style="padding: 10px;text-align: right;"> JUMLAH KESELURUHAN </td>
                                        <td style="padding: 10px;">{{ number_format($total,2,",",".")}}</td>
                                    </tr>  
                                    <tr>
                                        <td><center> </center></td>
                                        <td style="padding: 10px;" colspan="2">   
                                             <textarea id="terbilangA" class="form-control" rows="1"> {{ $dataCPI['totalTerbilangKegiatan1'] }} </textarea> 
                                        </td> 
                                    </tr> 
                                </tbody>
                            </table>
                             <input id="hasilTerbilang" value="" name="totalTerbilangKegiatan1" type="hidden">   
                            <textarea name="kegiatan1" class="form-control" rows="6" style="display: none;">
                                <table border="1" cellpadding="1" cellspacing="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th><center> No. </center></th>
                                            <th><center> Uraian Kegiatan/Penggunaan </center></th>
                                            <th><center> Jumlah Rp. </center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($recRencanaByNoProp as $key=>$value)
                                        <tr>
                                            <td><center>{{$key+1}}</center></td>
                                            <td style="padding: 10px;">{{$value->rencana}}</td>
                                            <td style="padding: 10px;">{{ number_format($value->rencana_anggaran,2,",",".")}}</td>
                                        </tr>
                                        {!! $metrics->outputByRencanaIDForNPHD($value->id) !!}
                                        @endforeach

                                        <tr>
                                            <td><center> </center></td>
                                            <td style="padding: 10px;text-align: right;"> JUMLAH KESELURUHAN </td>
                                            <td style="padding: 10px;text-align: right;">{{ number_format($total,2,",",".")}}</td>
                                        </tr> 
                                        
                                    </tbody>
                                </table>
                            </textarea>
                        </td>
                    </tr>
                </table>
                <br>
                <p style="padding: 0 200px 0 100px;text-align: justify;">Dengan ini menyatakan bahwa : </p>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px;width: 90%">
                    <tr>
                        <td style="vertical-align: top;">1.</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Akan melaporkan dan mempertanggungjawabkan penggunaan dana 
                <select name="text2">
                    <option value="bantuan sosial" {{($dataCPI['text2']=='bantuan sosial')?'selected':''}}>bantuan sosial</option>
                    <option value="hibah" {{($dataCPI['text2']=='hibah')?'selected':''}}>hibah</option>
                </select> yang diterima dari Pemerintah Provinsi DKI Jakarta kepada Gubernur Provinsi DKI Jakarta melalui BPKD selaku PPKD dengan tembusan <input type="text" name="nm_skpd" value="{{ $recProposalByID->nm_skpd }}" readonly=""> paling lambat tanggal 10 bulan Maret tahun anggaran berikutnya;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">2.</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Akan melaksanakan kegiatan sesuai dengan proposal yang diusulkan 
                <select name="text3">
                    <option value="dan NPHD yang telah ditandatanggani" {{($dataCPI['text3']=='dan NPHD yang telah ditandatanggani')?'selected':''}}>dan NPHD yang telah ditandatanggani</option>
                    <option value="" {{($dataCPI['text3']=='')?'selected':''}}></option>
                </select>, serta bertanggung jawab secara formal dan material atas penggunaan dana 
                <select name="text4">
                    <option value="bantuan sosial" {{($dataCPI['text4']=='bantuan sosial')?'selected':''}}>bantuan sosial</option>
                    <option value="hibah" {{($dataCPI['text4']=='hibah')?'selected':''}}>hibah</option>
                </select> yang diterima.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">3.</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Tidak akan mengalihkan anggaran belanja 
                <select name="text5">
                    <option value="bantuan sosial" {{($dataCPI['text5']=='bantuan sosial')?'selected':''}}>bantuan sosial</option>
                    <option value="hibah" {{($dataCPI['text5']=='hibah')?'selected':''}}>hibah</option>
                </select> kepada pihak lain; dan</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">4.</td>
                        <td style="padding: 0 200px 0 10px;text-align: justify;">Bersedia dituntut sesuai dengan hukum yang berlaku di Republik Indonesia apabila di kemudian hari terdapat penyimpangan/penyalahgunaan dana 
                <select name="text6">
                    <option value="bantuan sosial" {{($dataCPI['text6']=='bantuan sosial')?'selected':''}}>bantuan sosial</option>
                    <option value="hibah" {{($dataCPI['text6']=='hibah')?'selected':''}}>hibah</option>
                </select> yang diterima </td>
                    </tr>
                </table>
                <br>
                <p style="padding: 0 200px 0 100px;text-align: justify;">Demikian surat pernyataan tanggung jawab mutlak ini dibuat di atas meterai secukupnya untuk dapat dipergunakan sebagaimana mestinya.</p>
                <br>
                <table width="100%">
                    <tr>
                        <td width="30%">&nbsp;</td>
                        <td width="30%"></td>
                        <td>
                        	<center>
                        		<p>Jakarta, <input type="text" name="text7" value="{{ $dataCPI['text7'] }}"> </p>
                        		<p><input type="text" name="jabatan_pemberi" value="{{ $dataCPI['jabatan_pemberi'] }}"></p>
                        		<br>
                        		<p>(materai 6000)</p>
                        		<br>
                        		<p><input type="text" name="nama_pemberi" value="{{ $dataCPI['nama_pemberi'] }}"></p>
                        		<p>NIP/NRP <input type="text" name="no_pemberi" value="{{ $dataCPI['no_pemberi'] }}"> *)</p>
                            </center>
                        </td>
                    </tr>
                </table>
                <br>
                <p style="padding: 0 200px 0 100px;text-align: justify;">
                	*) bagi penerima hibah instansi pemerintah <br>
                	**) bagi penerima bantuan sosial selain individu/keluarga
                </p>
                <hr>
                {{-- @if($dataCPI['is_generate']==0) --}}
                <button type="submit" class="btn green" id="btn-simpan-nphd">Simpan</button>
                {{-- @endif --}}
            </form>
            
        </div>
        
    </div>
    <!-- END CONTENT BODY -->
</div>

{{-- box dialog confirmation --}}
<div id="dialog" title="Confirmation Required" style="display: none;">
    Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui data cetak Pakta Integritas tidak dapat di update kembali.
</div>

@stop
@section('javascript')
    @parent
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(function(){
            $(".btn-pdf-nphd").on("click", function(){
	            $("#btn-simpan-nphd").hide();
	            // return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui data cetak naskah perjanjian NPHD tidak dapat di update kembali. ");
	        });


            $("#terbilangA").on('change keyup paste', function() { 
                var nVal = $(this).val();
                
                // $("#hasilTerbilang").val(nVal);
                $("#hasilTerbilang").val(nVal);
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#dialog").dialog({
                autoOpen: false,
                modal: true
            });
        });

        $(".confirmLink").click(function(e) {
            e.preventDefault();
            var targetUrl = $(this).attr("href");

            $("#dialog").dialog({
              buttons : {
                "Ok" : function() {
                  window.location.href = targetUrl;
                  $(this).dialog("close");
                },
                "Cancel" : function() {
                  $(this).dialog("close");
                }
              }
            });

            $("#dialog").dialog("open");
        });


    </script>
    @if( !empty(Session::get('status')) )
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
@stop