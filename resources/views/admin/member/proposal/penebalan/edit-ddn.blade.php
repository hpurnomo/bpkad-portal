@extends('admin/layout/template')
@section('content')
<style type="text/css">
    .fileuploader-theme-default{
        margin-top: 0 !important;
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Ubah Usulan Proposal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
            <!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="caption" style="font-size: 16px;">
                                    <i class="icon-settings font-red"></i>
                                    <span class="caption-subject font-red sbold uppercase">Form Ubah Usulan Proposal {{ $tahap }}</span>
                                </div>
                            </div>
                            <div class="col-xs-6" style="text-align:right;">
                                <a href="{{route('member.proposal.show',['id'=>base64_encode($recProposalByUUID->no_prop),'fase'=>$fase])}}" class="btn btn-default" style="float: right;margin-bottom: 10px;">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs">
                                    <li class="{{ ($status==null)?'active':'' }}">
                                        <a href="#tab_1_0" data-toggle="tab">1. Data Proposal </a>
                                    </li>
                                    <li class="{{ ($status=='rencana')?'active':'' }}">
                                        <a href="#tab_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    
                                    {{-- Detil Lembaga --}}
                                    <div class="tab-pane fade {{ ($status==null)?'active in':'' }}" id="tab_1_0">
                                        <!-- BEGIN FORM-->
                                        <form action="{{ route('member.proposal.penebalan.update') }}" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="no_prop" value="{{ $recProposalByUUID->no_prop }}">
                                        <input type="hidden" name="tahap" value="{{ $tahap }}">
                                        <input type="hidden" name="uuid" value="{{ $recProposalPenebalanUUID->uuid_proposal_pengajuan }}">
                                        <input type="hidden" name="noper" value="{{ $recProposalPenebalanUUID->noper }}">
                                        <input type="hidden" name="no_skpd" value="{{ $recProposalPenebalanUUID->no_skpd }}">
                                        <input type="hidden" name="no_lembaga" value="{{ $recProposalPenebalanUUID->no_lembaga }}">
                                        <input type="hidden" name="crb" value="{{ Auth::user()->user_id }}">
                                        <input type="hidden" name="crd" value="{{ Carbon\Carbon::now() }}">
                                        <input type="hidden" name="real_name" value="{{ Auth::user()->real_name }}">
                                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                                        <input type="hidden" name="fase" value="{{ $fase }}">
                                            <div class="form-body" style="padding-top: 0;">
                                                <div class="alert alert-danger display-hide">
                                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                                <div class="alert alert-success display-hide">
                                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                                
                                                <h3 style="margin-top: 0;">Data Utama</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Tanggal Proposal
                                                    <span class="required" aria-required="true"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control" readonly name="tgl_prop" value="{{ $recProposalPenebalanUUID->tgl_prop }}" required>
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                        <span class="help-block"> Pilih tanggal </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">No Proposal
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input name="no_proposal" class="form-control" value="{{ $recProposalPenebalanUUID->no_proposal }}" required></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Judul Proposal
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="jdl_prop" class="form-control">{{ $recProposalPenebalanUUID->jdl_prop }}</textarea> </div>
                                                </div>
                                                <h3>Isi Proposal</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Latar Belakang
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="latar_belakang" class="form-control wysihtml5" rows="10">{{ $recProposalPenebalanUUID->latar_belakang }}</textarea> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Maksud Tujuan
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="maksud_tujuan" class="form-control wysihtml5" rows="10">{{ $recProposalPenebalanUUID->maksud_tujuan }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Keterangan
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea name="keterangan_prop" class="form-control wysihtml5" rows="10">{{ $recProposalPenebalanUUID->keterangan_prop }}</textarea>
                                                    </div>
                                                </div>
                                                <h3>Nominal Permohonan</h3>
                                                <hr>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Nominal Permohonan Bantuan (Rp)
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="nominal_pengajuan" type="text" class="form-control input-numeral" value="{{ str_replace(".", ",", $recProposalPenebalanUUID->nominal_pengajuan) }}" /> </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <h3>Data Pendukung</h3>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Surat Pernyataan Tanggung Jawab Bermeterai
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                        @foreach($berkasPernyataanTanggungJawab->where('uuid_proposal_pengajuan',$recProposalPenebalanUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                            <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                                                        @endforeach
                                                        <br><br>
                                                        <div class="clearfix margin-top-12">
                                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                        <input type="file" name="surat_pernyataan_tanggung_jawab" class="btn default form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Struktur Pengurus
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                        @foreach($berkasKepengurusan->where('uuid_proposal_pengajuan',$recProposalPenebalanUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                            <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                                                        @endforeach
                                                        <br><br>
                                                        <div class="clearfix margin-top-12">
                                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                        <input type="file" name="berkas_kepengurusan" class="btn default form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Foto Kondisi Saat Ini
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        @inject('fotoProposal','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                        @foreach($fotoProposal->where('uuid_proposal_pengajuan',$recProposalPenebalanUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                            <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                                                        @endforeach
                                                        <br><br>
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat JPG/JPEG/PNG dan Max 10MB </small></span>
                                                        </div>
                                                        <input type="file" name="photo_lembaga" class="btn default form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Berkas Proposal
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                        @foreach($berkasProposal->where('uuid_proposal_pengajuan',$recProposalPenebalanUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                            <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                                                        @endforeach
                                                        <br><br>
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                        <input type="file" name="berkas_proposal" class="btn default form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Berkas Rancangan Anggaran Biaya (RAB)
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        @inject('berkasRab','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                        @foreach($berkasRab->where('uuid_proposal_pengajuan',$recProposalPenebalanUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                            <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                                                        @endforeach
                                                        <br><br>
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                        <input type="file" name="berkas_rab" class="btn default form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Saldo Akhir Rekening Bank 
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                        @foreach($berkasRekening->where('uuid_proposal_pengajuan',$recProposalPenebalanUUID->uuid_proposal_pengajuan)->where('tahap','ddn')->get() as $key=>$value)
                                                            <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" target="_blank">{{ $value->file_name }} <i class="icon-link" aria-hidden="true"></i></a>
                                                        @endforeach
                                                        <br><br>
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                                        </div>
                                                        <input type="file" name="berkas_rekening" class="btn default form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Titik Peta
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="portlet-body">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                                <span class="input-group-btn">
                                                                    <button class="btn blue" id="gmap_geocoding_btn">
                                                                        <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                            <div id="gmap_basic" class="gmaps"> </div>
                                                            <b>Latitude : </b>
                                                            <input type="text" class="form-control" name="latitude" id="latVal" value="{{ $recProposalPenebalanUUID->latitude }}" >
                                                            <b>Longitude : </b>
                                                            <input type="text" class="form-control" name="longitude" id="longVal" value="{{ $recProposalPenebalanUUID->longitude }}" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                            @if($recProposalPenebalanUUID->status_kirim==0)
                                                @if($rencana_anggaran == null || $rencana_anggaran == 0 )
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="col-md-6">
                                                        <label>
                                                            <i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">Agar proposal ini dapat dikirim/dilanjutkan ke SKPD terkait. <br> Anda wajib menginput rencana kegiatan dan rincian anggaran & biaya (RAB).</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                @else
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="col-md-6">
                                                        <label>
                                                            <input type="checkbox" name="status_kirim" id="status-kirim"> <b>Anda Setuju Kirim Langsung Proposal Ke SKPD Koordinator?</b><i style="display: block;padding-top: 10px;color: red;font-weight: bolder;">(Proposal yang telah dikirim tidak dapat di-ubah, Karna akan di-review oleh SKPD Koordinator)</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green" id="btn-submit">Simpan Sebagai Draft</button>
                                                        <button type="button" onclick="javascript:window.history.back();" class="btn grey-salsa btn-outline">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                                <hr>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="col-md-6">
                                                        <label>
                                                            <b>Proposal Ini Telah Terkirim</b>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="{{ route('member.proposal.index') }}" class="btn grey-salsa btn-outline">Kembali</a>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                    
                                    <div class="tab-pane fade {{ ($status=='rencana')?'active in':'' }}" id="tab_1_1">
                                        <div class="col-md-12">
                                            <h3>Rencana Penggunaan Anggaran (Rp)</h3>
                                            <hr>
                                            @if($recProposalPenebalanUUID->nominal_pengajuan != $rencana_anggaran)
                                            <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                            @else
                                            <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                            @endif
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Nominal Permohonan Bantuan (Rp)
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input name="nominal_pengajuan" type="text" class="form-control input-numeral3" value="{{ str_replace(".", ",", $recProposalPenebalanUUID->nominal_pengajuan) }}" disabled="" /> </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-top: 10px;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Nilai Input RAB (Rp)
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input name="asdasd" type="text" class="form-control input-numeral2" value="{{ str_replace(".", ",", $rencana_anggaran) }}" readonly="" />
                                                        <i style="display: block;padding-top: 10px;color: #9e9e9e;">(Nilai ini akan tampil setelah RAB telah di-input)</i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-toolbar" style="margin-top: 150px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="btn-group">
                                                            <a href="{{ route('member.proposal.penebalan.create.rencana',['fase'=>$fase,'noper'=>$noper,'uuid'=>$uuid,'tahap'=>$tahap,'status'=>$status]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Rencana Kegiatan
                                                                <i class="fa fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                                <thead>
                                                    <tr>
                                                        <th> No. </th>
                                                        <th> Rencana Kegiatan/Rincian </th>
                                                        <th> Vol/Satuan (jumlah) </th>
                                                        <th> Harga Satuan (Rp) </th>
                                                        <th> Anggaran (Rp) </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($recRencanaByNoProp)==0)
                                                        <tr>
                                                            <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                        </tr>
                                                    @else
                                                        @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                                        @foreach ($recRencanaByNoProp as $key=>$value)
                                                        <tr>
                                                            <td>{{$key+1}}</td>
                                                            <td><b><a href="{{route('member.proposal.penebalan.edit.rencana',['id'=>$value->id,'fase'=>$fase,'noper'=>$noper,'uuid'=>$value->uuid_proposal_pengajuan,'tahap'=>$tahap,'status'=>$status])}}">{{$value->rencana}}</a></b></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                        </tr>
                                                        {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,$tahap,$status) !!}
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        

    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
    
            // enable fileuploader plugin
            $('input[name="surat_pernyataan_tanggung_jawab"],input[name="berkas_kepengurusan"],input[name="berkas_proposal"],input[name="berkas_rab"],input[name="berkas_rekening"]').fileuploader({
                limit: 1,
                extensions: ['pdf'],
                fileMaxSize: 10
            });

            $('input[name="photo_lembaga"]').fileuploader({
                limit: 1,
                extensions: ['jpg', 'jpeg', 'png'],
                fileMaxSize: 10
            });
            
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });

        $(function(){
            $('#status-kirim').on('click',function(){
                if($('#status-kirim:checked').length == 1){
                    $('#btn-submit').html('Kirim');
                }
                else{
                    $('#btn-submit').html('Simpan Sebagai Draf');   
                }
            });
        });

        $(function(){
            $(".delete").on("submit", function(){
                return confirm("Anda yakin menghapus proposal ini?");
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = {{ ($recProposalPenebalanUUID->latitude==NULL)?'0':$recProposalPenebalanUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanUUID->longitude==NULL)?'0':$recProposalPenebalanUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            var cleaveNumeral = new Cleave('.input-numeral2', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            var cleaveNumeral = new Cleave('.input-numeral3', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop