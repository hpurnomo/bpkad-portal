@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Buat Rencana Kegiatan</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <div class="portlet light bordered">
            <div class="portlet-title">
            	<div class="row">
                    <div class="col-xs-6">
                        <div class="caption" style="font-size: 16px;">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Form Buat Rencana Kegiatan (Proposal {{ $tahap }})</span>
                        </div>
                    </div>
                    <div class="col-xs-6" style="text-align:right;">
                        <a href="{{ route('member.proposal.penebalan.edit',['fase'=>$fase,'noper'=>$noper,'uuid'=>$uuid,'tahap'=>$tahap,'status'=>'rencana']) }}" class="btn btn-default">Kembali</a>
                    </div>
                </div>
            </div>
            <div class="portlet-body form">
                @include('admin.part.alert')
                <!-- BEGIN FORM-->
                <form action="{{ route('member.proposal.penebalan.store.rencana') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $uuid }}">
                    <input type="hidden" name="fase" value="{{ $fase }}">
                    <input type="hidden" name="noper" value="{{ $noper }}">
                    <input type="hidden" name="tahap" value="{{ $tahap }}">
                    <input type="hidden" name="status" value="rencana">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rencana Kegiatan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <textarea name="rencana" class="form-control" rows="3" required=""></textarea> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6">
                                        <button type="submit" class="btn green">Simpan</button>
                                        <button type="reset" class="btn default">Batal</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop