@extends('admin/layout/template')
@section('content')
<style type="text/css">
    .fileuploader-theme-default{
        margin-top: 0 !important;
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form Buat Usulan Proposal Baru</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        {{-- {{ dd($recProposalByUUID) }} --}}
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-form bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Form Buat Usulan Proposal {{ $tahap }}</span>
                        </div>

                        <a href="{{route('member.proposal.show',['id'=>base64_encode($recProposalByUUID->no_prop),'fase'=>$fase])}}" class="btn btn-default" style="float: right;margin-bottom: 10px;">Kembali</a>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{ route('member.proposal.penebalan.store') }}" id="form_sample_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="tahap" value="{{ $tahap }}">
                        <input type="hidden" name="uuid" value="{{ $recProposalByUUID->uuid }}">
                        <input type="hidden" name="noper" value="{{ $recProposalByUUID->noper }}">
                        <input type="hidden" name="no_skpd" value="{{ $recProposalByUUID->no_skpd }}">
                        <input type="hidden" name="no_lembaga" value="{{ $recProposalByUUID->no_lembaga }}">
                        <input type="hidden" name="crb" value="{{ Auth::user()->user_id }}">
                        <input type="hidden" name="crd" value="{{ Carbon\Carbon::now() }}">
                        <input type="hidden" name="real_name" value="{{ Auth::user()->real_name }}">
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        <input type="hidden" name="fase" value="{{ $fase }}">

                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                
                                <h3>Data Utama</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Proposal
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_prop" value="{{ old('tgl_prop') }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No Proposal
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input name="no_proposal" class="form-control" required></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Judul Proposal
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="jdl_prop" class="form-control" required></textarea> </div>
                                </div>
                                <h3>Isi Proposal</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Latar Belakang
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="latar_belakang" class="form-control wysihtml5" rows="10"></textarea> </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Maksud Tujuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="maksud_tujuan" class="form-control wysihtml5" rows="10"></textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rencana Penggunaan Bantuan
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea name="keterangan_prop" class="form-control wysihtml5" rows="10"></textarea>
                                    </div>
                                </div>
                                <h3>Rencana Penggunaan Anggaran</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nominal Permohonan Bantuan(Rp)
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nominal_pengajuan" type="text" class="form-control input-numeral" /> </div>
                                </div>
                                <h3>Data Pendukung</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Surat Pernyataan Tanggung Jawab Bermeterai
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                        </div>
                                        <input type="file" name="surat_pernyataan_tanggung_jawab" class="btn default form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Struktur Pengurus
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                        </div>
                                        <input type="file" name="berkas_kepengurusan" class="btn default form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Foto Kondisi Saat Ini
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat JPG/JPEG/PNG dan Max 10MB </small></span>
                                        </div>
                                        <input type="file" name="photo_lembaga" class="btn default form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Berkas Proposal
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                        </div>
                                        <input type="file" name="berkas_proposal" class="btn default form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Berkas Rancangan Anggaran Biaya (RAB)
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                        </div>
                                        <input type="file" name="berkas_rab" class="btn default form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Saldo Akhir Rekening Bank 
                                    <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF dan Max 10MB </small></span>
                                        </div>
                                        <input type="file" name="berkas_rekening" class="btn default form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Titik Peta
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="portlet-body">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                <span class="input-group-btn">
                                                    <button class="btn blue" id="gmap_geocoding_btn">
                                                        <i class="fa fa-search"></i>
                                                </span>
                                            </div>
                                            <div id="gmap_basic" class="gmaps"> </div>
                                            <b>Latitude : </b>
                                            <input type="text" class="form-control" name="latitude" id="latVal" value="" >
                                            <b>Longitude : </b>
                                            <input type="text" class="form-control" name="longitude" id="longVal" value="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green" id="btn-submit">Simpan dan Lanjut mengisi RAB</button>
                                        <button type="button" onclick="javascript:window.history.back();" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
    
            // enable fileuploader plugin
            $('input[name="surat_pernyataan_tanggung_jawab"],input[name="berkas_kepengurusan"],input[name="berkas_proposal"],input[name="berkas_rab"],input[name="berkas_rekening"]').fileuploader({
                limit: 1,
                extensions: ['pdf'],
                fileMaxSize: 10
            });

            $('input[name="photo_lembaga"]').fileuploader({
                limit: 1,
                extensions: ['jpg', 'jpeg', 'png'],
                fileMaxSize: 10
            });
            
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });

        $(function(){
            $('#status-kirim').on('click',function(){
                if($('#status-kirim:checked').length == 1){
                    $('#btn-submit').html('Kirim');
                }
                else{
                    $('#btn-submit').html('Simpan Sebagai Draf');   
                }
            });
        })
    </script>
    @if( !empty(Session::get('status')) )
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = '';
            var longValue = '';
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
@stop