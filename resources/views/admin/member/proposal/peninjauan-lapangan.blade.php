<div class="tab-pane fade" id="tab_2_1">
    @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
        <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
    <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
    <div class="form-group">
        <div class="form-body">
            {!! csrf_field() !!}
            <div class="form-group form-md-radios">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="50%"><center>Data Lapangan</center></th>
                                <th><center>Sesuai</center></th>
                                <th><center>Tidak Sesuai</center></th>
                                <th><center>Keterangan</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Aktivitas</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_aktivitas11" name="cek_aktivitas" value="1" disabled="" class="md-radiobtn" 
                                        @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==1)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_aktivitas11">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_aktivitas22" name="cek_aktivitas" value="2" disabled="" class="md-radiobtn"
                                        @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==2)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_aktivitas22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                @if(isset($recProposalLapanganByNoProp->keterangan_cek_aktivitas))
                                <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_aktivitas }}</textarea>
                                @else
                                <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Kepengurusan</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_kepengurusan11" name="cek_kepengurusan" value="1" disabled="" class="md-radiobtn" 
                                        @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==1)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_kepengurusan11">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_kepengurusan22" name="cek_kepengurusan" value="2" disabled="" class="md-radiobtn"
                                        @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==2)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_kepengurusan22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                @if(isset($recProposalLapanganByNoProp->keterangan_cek_kepengurusan))
                                <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                @else
                                <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Rencana Anggaran Biaya (RAB)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_rab11" name="cek_rab" value="1" disabled="" class="md-radiobtn" 
                                        @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==1)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_rab11">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_rab22" name="cek_rab" value="2" disabled="" class="md-radiobtn"
                                        @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==2)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_rab22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                @if(isset($recProposalLapanganByNoProp->keterangan_cek_rab))
                                <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_rab }}</textarea>
                                @else
                                <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Waktu Pelaksanaan</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_waktu_pelaksanaan11" name="cek_waktu_pelaksanaan" value="1" disabled="" class="md-radiobtn" 
                                        @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==1)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_waktu_pelaksanaan11">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_waktu_pelaksanaan22" name="cek_waktu_pelaksanaan" value="2" disabled="" class="md-radiobtn"
                                        @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==2)
                                        checked
                                        @endif
                                        >
                                        <label for="cek_waktu_pelaksanaan22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                @if(isset($recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan))
                                <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                @else
                                <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <center>
                <input type="checkbox" disabled="" name="is_approve"
                    @if(isset($recProposalLapanganByNoProp->is_approve) && $recProposalLapanganByNoProp->is_approve==1)
                    checked
                    @endif
                > 
                <span style="color:red;font-weight: bolder;">Dengan ini Dinyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
            </center>
        </div>
        <br>
        <br>
        <br>
    </div>
    </form>
    @else
    <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
    @endif
</div>