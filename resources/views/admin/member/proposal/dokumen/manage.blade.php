@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar Dokumen</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        {{-- {{dd($recUserByGroupID)}} --}}
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                             @if(Auth::user()->group_id==12) 
                            <span class="caption-subject bold uppercase"> Scan Dokumen Pencairan Lembaga & Upload Scan Dokumen LPJ </span>
                            @else
                             <span class="caption-subject bold uppercase"> Dokumen Surat Rekomendasi Basah </span>
                            @endif

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('lembaga.upload.dokumen',['noper'=>$noper,'no_prop'=>$no_prop,'uuid_proposal_pengajuan'=>$uuid]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Dokumen Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
                            <thead>
                                <tr>
                                    <th> No. </th> 
                                    <th> Nama Dokumen </th>
                                    <th> Jenis Dokumen </th>
                                    <th> File Dokumen </th>
                                    <th> Action </th> 
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recDokumenByID as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td> 
                                    <td> {{ $value->name }} </td> 

                                    @if($value->jenis=='srb') 
                                        <td>  Surat Rekomendasi Basah </td> 
                                    @elseif($value->jenis=='lpj')
                                        <td>  Dokumen LPJ</td>  
                                    @elseif($value->jenis=='dpl')
                                        <td> Dokumen Pencairan Lembaga </td>  
                                    @endif 
                                    <td> <a href="{{ URL::to('laporan') }}/{{ $value->file }}" target="_blank"> Download File  </a> </td> 
                                    <td>   
                                        <a href="{{ route('lembaga.upload.dokumen.edit',base64_encode($value->id)) }}"> <i class="fa fa-pencil"></i> Edit </a>
                                        <!-- <a href="{{ route('lembaga.upload.dokumen.delete',base64_encode($value->id)) }}" onClick="return  confirm(\'Yakin akan hapus data ini \');"> <i class="fa fa-trash"></i> Delete </a> -->
 
                                    </td>  
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop