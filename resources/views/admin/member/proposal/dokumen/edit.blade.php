@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
               <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit Document</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
            <!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Edit Dokumen   </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @include('admin.part.alert')
                        <form action="{{ route('member.lembaga.upload.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST" enctype="multipart/form-data">

                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!} 
 
                                  <div class="form-group">
                                    <label class="control-label col-md-3"> Jenis Dokumen
                                    </label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="jenis_dokumen" required="true">
                                            <option value="" selected="true">Pilih Jenis Dokumen</option>
                                            @if($dokumen->jenis=='lpj')
                                            <option value="lpj" selected="true">Dokumen LPJ</option>
                                            @else
                                            <option value="lpj"> Dokumen LPJ</option>
                                            @endif

                                            @if($dokumen->jenis=='dpl')
                                            <option value="dpl" selected="true">Dokumen Pencairan Lembaga</option>
                                            @else
                                            <option value="dpl">Dokumen Pencairan Lembaga</option>
                                            @endif 
                                            
                                            @if(Auth::user()->group_id==10)  
                                                @if($dokumen->jenis=='srb')
                                                <option value="srb" selected="true">Dokumen Surat Rekomendasi Basah</option>
                                                @else
                                                <option value="srb"> Dokumen Surat Rekomendasi Basah</option>
                                                @endif

                                            @endif
                                        </select>
                                    </div>
                                  </div> 

                                  <div class="form-group">
                                    <label class="control-label col-md-3"> Nama Dokumen
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" name="name" class="form-control" required="true" value="{{ $dokumen->name }}"> 
                                    </div>
                                  </div> 
                                  

                                  <div class="form-group">
                                    <label class="control-label col-md-3"> File Dokumen  
                                    </label>
                                    <div class="col-md-6">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;background-color: #f6f6fa;"> <small>Info : File Dokumen   </small></span>
                                            <input type="file" name="file_dokumen" required="true"> 
                                            <a href="{{ URL::to('laporan') }}/{{ $dokumen->file }}" target="_blank"> Download File  </a>
                                        </div>
                                    </div>
                                  </div> 
                               
                                <input type="hidden" name="id" value="{{ $dokumen->id }}">
                                <input type="hidden" name="prev" value="{{ URL::previous() }}">

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Upload</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
 