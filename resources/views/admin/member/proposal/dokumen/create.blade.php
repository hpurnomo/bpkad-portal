@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
               <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Upload Document</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Upload Dokumen   </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('member.lembaga.upload.store') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST" enctype="multipart/form-data">

                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!} 
 
                                  <div class="form-group">
                                    <label class="control-label col-md-3"> Jenis Dokumen
                                    </label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="jenis_dokumen" required="true">
                                            <option value="lpj">Pilih Jenis Dokumen</option>
                                             @if(Auth::user()->group_id==12) 
                                            <option value="lpj">Dokumen LPJ</option>
                                            <option value="dpl">Dokumen Pencairan Lembaga</option>
                                            @else  
                                            <option value="srb">Dokumen Surat Rekomendasi Basah</option>
                                            @endif
                                        </select>
                                    </div>
                                  </div> 

                                  <div class="form-group">
                                    <label class="control-label col-md-3"> Nama Dokumen
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" name="name" class="form-control" required="true"> 
                                    </div>
                                  </div> 
                                  

                                  <div class="form-group">
                                    <label class="control-label col-md-3"> File Dokumen  
                                    </label>
                                    <div class="col-md-6">
                                        <div class="clearfix margin-top-10">
                                            <span class="label" style="color: red;font-size: 15px;font-weight: 400;background-color: #f6f6fa;"> <small>Info : File Dokumen   </small></span>
                                            <input type="file" name="file_dokumen" required="true"> 
                                        </div>
                                    </div>
                                  </div> 
                              

                                <input type="hidden" name="no_prop" value="{{ $no_prop }}">
                                <input type="hidden" name="noper" value="{{ $noper }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="prev" value="{{ URL::previous() }}">

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Upload</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
 