@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{ $recPeriodeByNoper->keterangan }}</span>
                </li>
            </ul>
        </div>
        {{-- {{ dd() }} --}}
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
            <!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        {{-- {{ dd($recDetailPeriodeTahapanByNoper[0]) }} --}}
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p> 
                        <ol>
                            <li>Sesuai dengan Pergub No.142 Tahun 2018 Pasal 6 ayat (2) yang tertulis sebagai berikut: Setiap usulan Hibah sebagaimana dimaksud pada ayat (1) hanya diperkenankan berupa satu surat permohonan dan satu proposal hibah.</li>
                            <li>Data dibawah ini merupakan data yang sudah tersimpan didalam System Kami. Pada <b>Fase {{ $recPeriodeByNoper->keterangan }}</b> Lembaga Anda.</li>
                        </ol>
                    </p>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>

                    <div class="portlet-body">
                        {{-- tombol buat usulan akan aktif jika total proposal pada fase ini masih 0 dan tahapan Pengajuan Proposal 1 --}}
                        @if($totalProposal==0 && $recDetailPeriodeTahapanByNoper[0]->status==1)
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('member.proposal.create',['fase'=>$fase,'noper'=>$recPeriodeByNoper->noper]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Usulan Proposal Baru
                                            <i class="icon-note"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @elseif($totalProposal > 0)
                        <div class="note note-danger">
                            <p>Anda telah mengajukan Proposal di Fase ini.</p>
                        </div>
                        @elseif($recDetailPeriodeTahapanByNoper[0]->status == 0)
                        <div class="note note-danger">
                            <p>Fase Pengajuan Proposal Saat Ini Telah Di TUTUP.</p>
                        </div>
                        @endif
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <th> No. </th>
                                    <th> Action </th>
                                    <th> Judul Proposal </th>
                                    <th> Status </th>
                                    <th> Nominal Pengajuan (Rp) </th>
                                    <th> Nominal Rekomendasi (Rp) </th>
                                    <th> Tgl Pengajuan </th>
                                    <th> TAPD </th>
                                    <th> Banggar </th>
                                    <th> DDN </th>
                                    <th> APBD </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($recProposalByNoLembaga as $key=>$value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>
                                        @if($value->status_kirim==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase">Lihat</a>

                                        @else
                                        <a href="{{ route('member.proposal.edit',['id'=>base64_encode($value->no_prop),'fase'=>'pengusulan']) }}" class="btn btn-circle dark btn-outline sbold uppercase">Pengaturan</a>
                                        @endif
                                        @if($value->status_prop==1)
                                        <a href="{{ route('member.proposal.show.surat.pernyataan',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase">Download Surat Mutlak</a>
                                        @endif
                                       {{--  <br><br>

                                        @if($value->is_penebalan_nominal_tapd==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase">Penebalan TAPD</a>
                                        @endif

                                        <br><br>

                                        @if($value->is_penebalan_nominal_banggar==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase">Penebalan Banggar</a>
                                        @endif

                                        <br><br>

                                        @if($value->is_penebalan_nominal_ddn==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase">Penebalan DDN</a>
                                        @endif

                                        <br><br>

                                        @if($value->is_penebalan_nominal_apbd==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase">Definitif APBD</a>
                                        @endif --}}
                                    </td>
                                    <td>{{ ucwords(strtolower($value->jdl_prop)) }}</td>
                                    <td>
                                        @if($value->status_prop==0)
                                            Belum Direkomendasi
                                        @elseif($value->status_prop==1)
                                            Sudah Direkomendasi
                                        @elseif($value->status_prop==3)
                                            Di Koreksi
                                        @elseif($value->status_prop==2)
                                            Di Tolak
                                        @endif
                                    </td>
                                    <td style="text-align: right;">{{ number_format($value->nominal_pengajuan,2,',','.') }}</td>
                                    @if($value->status_prop==1)
                                    <td style="text-align: right;">{{ number_format($value->nominal_rekomendasi,2,',','.') }}</td>
                                    @else
                                         <td style="text-align: right;"> </td>
                                    @endif
                                    <td>{{ date('d M Y',strtotime($value->tgl_prop)) }}</td>
                                    <td>
                                        @if($value->is_penebalan_nominal_tapd==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase"><i class="fa fa-pencil"></i></a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($value->is_penebalan_nominal_banggar==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase"><i class="fa fa-pencil"></i></a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($value->is_penebalan_nominal_ddn==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase"><i class="fa fa-pencil"></i></a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($value->is_penebalan_nominal_apbd==1)
                                        <a href="{{ route('member.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle green-haze btn-outline sbold uppercase"><i class="fa fa-pencil"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop