<div class="tab-pane fade" id="tab_5">
    @if (empty($recProposalDefinitifAPBDByUUID))
    <h5 style="color: red;font-weight: bolder;">*Saat ini Anda masuk tahapan proposal definitif untuk proses pencairan, silahkan Anda buat proposal definitif dengan klik tombol dibawah ini.</h5>

    <a href="{{ route('member.proposal.penebalan.create',['fase'=>$fase,'noper'=>$recProposalByID->noper,'uuid'=>$recProposalByID->uuid,'tahap'=>'apbd','lembaga'=>$recProposalByID->no_lembaga]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Usulan Proposal Definitif
        <i class="icon-note"></i>
    </a>
    @else
        @if ($recProposalDefinitifAPBDByUUID->status_kirim==0)
            <h5 style="color: red;font-weight: bolder;">*Saat ini proposal Anda belum terkirim, Segera lengkapi proposal Anda lalu kirim proposal Anda.</h5>

            <a href="{{ route('member.proposal.penebalan.edit',['fase'=>$fase,'noper'=>$recProposalDefinitifAPBDByUUID->noper,'uuid'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan,'tahap'=>'apbd','status'=>null]) }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Pengaturan Usulan Proposal Definitif
                <i class="icon-note"></i>
            </a>
        @else
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-eye"></i>Detil Proposal Definitif</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet light">

                            <div  style="float: right;margin-right: 5px;">
                                <div class="btn-group" >
                                    <a href="{{ route('lembaga.upload.dokumen.list',['noper'=>$recProposalByID->noper,'no_prop'=>$recProposalByID->no_prop,'uuid_proposal_pengajuan'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan]) }}" class="btn btn-circle sbold default upload" style="box-shadow: 2px 2px 5px #88898A !important;">  &nbsp;Upload Scan Dokumen Pencairan Lembaga & Upload Scan Dokumen LPJ
                                        <i class="fa fa-file"></i>
                                    </a>
                                </div>
                                <a href="{{route('member.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper])}}" class="btn btn-default">Kembali</a>
                                @if ($cekPerjanjianNPHDAPBD['id'] != "" || $cekPerjanjianNPHDAPBD['id'] != NULL || $cekPaktaIntegritas['id'] != "" || $cekPaktaIntegritas['id'] != NULL)
                            </div> 
                                <!-- Single button -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('member.proposal.penebalan.naskah.perjanjian.nphd.show',['uuid'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan,'fase'=>$fase,'tahap'=>'apbd']) }}"><i class="fa fa-cogs" aria-hidden="true"></i> Edit Naskah Perjanjian NPHD
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('member.proposal.penebalan.pakta.integritas.show',['uuid'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan,'fase'=>$fase,'tahap'=>'apbd']) }}">
                                               <i class="fa fa-cogs" aria-hidden="true"></i> Edit Pakta Integritas
                                            </a>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <a href="{{ route('member.proposal.penebalan.cetak.perjanjian.nphd',['uuid'=>$uuid,'fase'=>$fase,'tahap'=>'apbd']) }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh Naskah Perjanjian NPHD
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('member.proposal.penebalan.cetak.pakta.integritas',['uuid'=>$uuid,'fase'=>$fase,'tahap'=>'apbd']) }}"><i class="fa fa-download" aria-hidden="true"></i> Unduh Pakta Integritas</a>
                                        </li>
                                    </ul>
                                </div>  
                            @elseif($recProposalDefinitifAPBDByUUID->status_prop==1)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('member.proposal.penebalan.naskah.perjanjian.nphd.show',['uuid'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan,'fase'=>$fase,'tahap'=>'apbd']) }}"><i class="fa fa-cogs" aria-hidden="true"></i> Edit Naskah Perjanjian NPHD
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('member.proposal.penebalan.pakta.integritas.show',['uuid'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan,'fase'=>$fase,'tahap'=>'apbd']) }}">
                                               <i class="fa fa-cogs" aria-hidden="true"></i> Edit Pakta Integritas
                                            </a>
                                        </li>
                                    </ul>
                                </div> 
                            @elseif($recProposalDefinitifAPBDByUUID->status_prop==0)
                            <span class="text-info">*Naskah Perjanjian NPHD atau Pakta Integritas belum tersedia. Karna Proposal ini belum direkomendasi oleh SKPD.</span>
                            @endif
                            
                            <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_014" data-toggle="tab">1. Data Proposal </a>
                                </li>
                                <li>
                                    <a href="#tab_1_114" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                
                                {{-- Data Proposal --}}
                                <div class="tab-pane fade active in" id="tab_1_014">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body" style="padding: 0;margin: 0;">
                                            <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalDefinitifAPBDByUUID->tgl_prop)) }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalDefinitifAPBDByUUID->no_proposal }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalDefinitifAPBDByUUID->jdl_prop }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Isi Proposal</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->latar_belakang !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                             <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->maksud_tujuan !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->keterangan_prop !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Nominal Permohonan</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                        <div class="col-md-8">
                                                            <p class="form-control-static">Rp. {{ number_format($recProposalDefinitifAPBDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="form-section">Data Pendukung</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                            @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                            @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                            @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                            @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                            @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                            @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                        <div class="col-md-9">
                                                            <div class="portlet-body">
                                                                <div class="input-group">
                                                                    <input type="hidden" class="form-control" id="pac-input-apbd" placeholder="Cari Alamat">
                                                                </div>
                                                                <div id="gmap_basic_apbd" class="gmaps"> </div>
                                                                <b>Latitude : </b>
                                                                <input type="text" class="form-control" name="latitude" id="latValAPBD" value="{{$recProposalDefinitifAPBDByUUID->latitude}}" >
                                                                <b>Longitude : </b>
                                                                <input type="text" class="form-control" name="longitude" id="longValAPBD" value="{{$recProposalDefinitifAPBDByUUID->longitude}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Status Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">
                                                                @if($recProposalDefinitifAPBDByUUID->status_prop==0)
                                                                    <span class="label label-warning">Belum Direkomendasi</span>
                                                                @elseif($recProposalDefinitifAPBDByUUID->status_prop==1)
                                                                    <span class="label label-success">Sudah Direkomendasi</span>
                                                                @elseif($recProposalDefinitifAPBDByUUID->status_prop==3)
                                                                    <span class="label label-warning">Di Koreksi</span>
                                                                @else
                                                                    <span class="label label-danger">Di Tolak</span>
                                                                    <p><u><b>Alasan ditolak karena, {{ $recProposalDefinitifAPBDByUUID->alasan_ditolak }}</b></u></p>
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Status Terkirim:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> 
                                                                {!! ($recProposalDefinitifAPBDByUUID->status_kirim==1)?'Terkirim <i class="icon-paper-plane"></i>':'Belum Terkirim' !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <p>
                                                                <center>
                                                                <em><b><u>Jika Anda sudah mengecek dan tetap tidak menemukan email dari kami silahkan klik tombol dibawah ini. <br> Bukti kirim proposal akan terkirim kepada email lembaga dan email koordinator lembaga.</u></b></em></center>
                                                            </p>
                                                            <p>
                                                                <center>
                                                                <a href="{{ route('member.proposal.penebalan.resendEmail',['id'=>base64_encode($recProposalDefinitifAPBDByUUID->no_prop),'email'=>Auth::user()->email,'real_name'=>Auth::user()->real_name,'email_lembaga'=>$recProposalDefinitifAPBDByUUID->email_lembaga,'no_lembaga'=>$recProposalDefinitifAPBDByUUID->no_lembaga,'tahun_anggaran'=>$recProposalDefinitifAPBDByUUID->tahun_anggaran,'fase'=>$fase,'tahap'=>'apbd'])}}" class="btn btn-circle red">  KIRIM ULANG BUKTI KIRIM PROPOSAL VIA EMAIL
                                                                <i class="icon-paper-plane"></i></a>
                                                                </center>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                
                                {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                <div class="tab-pane fade" id="tab_1_114">
                                    <div class="col-md-12">
                                        <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                        @if($recProposalDefinitifAPBDByUUID->nominal_pengajuan != $rencana_anggaran_apbd)
                                        <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                        @else
                                        <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($recProposalDefinitifAPBDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_apbd,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <br><br><br>
                                        <!--/row-->
                                      
                                        <form  action="{{ route('member.proposal.updateAll.rincian.definitif') }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
                                        <input type="hidden" name="no_prop" value="{{ $recProposalDefinitifAPBDByUUID->no_prop }}" /> 
                                        <input type="hidden" name="uuid" value="{{ $recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan }}" /> 

                                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th> Rencana Kegiatan/Rincian </th>
                                                    <th> Vol/Satuan (jumlah) </th>
                                                    <th> Harga Satuan (Rp) </th>
                                                    <th> Anggaran (Rp) </th>
                                                    <th> Anggaran Terpakai (Rp) </th>
                                                    <th> Sisa Anggaran (Rp) </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($recRencanaByUUIDAPBD)==0)
                                                    <tr>
                                                        <td colspan="7"><i><center>data belum tersedia</center></i></td>
                                                    </tr>
                                                @else
                                                    @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                                    @foreach ($recRencanaByUUIDAPBD as $key=>$value)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td><b>{{$value->rencana}}</b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                        <td><b> {{ number_format($value->anggaran_digunakan,2,",",".")}} </b></td>
                                                        <td><b> </b></td>
                                                    </tr>
                                                    {!! $metrics->outputByRencanaIDWithoutEdit($value->id,$fase,$noper,$uuid,'apbd','rencana') !!}
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                          @if(Auth::user()->group_id==12)
                                             @if($recProposalByID->noper>= 9 )
                                             <button style="margin: 10px" type="submit" class="btn green-meadow">Simpan Anggaran Terpakai</button> 
                                             @endif
                                        @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_2_014" data-toggle="tab">1. Kelengkapan Administrasi </a>
                            </li>
                            <li>
                                <a href="#tab_2_114" data-toggle="tab">2. Peninjauan Lapangan </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            {{-- Kelengkapan Administrasi --}}
                            <div class="tab-pane fade active in" id="tab_2_014">
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Administrasi</center></th>
                                                            <th><center>Ada</center></th>
                                                            <th><center>Tidak Ada</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas1" name="cek_aktivitas" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas2" name="cek_aktivitas" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Kepengurusan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan1" name="cek_kepengurusan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan2" name="cek_kepengurusan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab1" name="cek_rab" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab2" name="cek_rab" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab))
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Waktu Pelaksanaan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan1" name="cek_waktu_pelaksanaan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan2" name="cek_waktu_pelaksanaan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="checkbox" disabled="" name="is_approve"
                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve==1)
                                                checked
                                                @endif
                                            > 
                                            <span style="color:red;font-weight: bolder;">Dengan ini Dinyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                                </form>
                            </div>
                            
                            {{-- Kelengkapan Lapangan --}}
                            <div class="tab-pane fade" id="tab_2_114">
                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve==1)
                                    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        {!! csrf_field() !!}
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Lapangan</center></th>
                                                            <th><center>Sesuai</center></th>
                                                            <th><center>Tidak Sesuai</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas11" name="cek_aktivitas" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_aktivitas22" name="cek_aktivitas" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Kepengurusan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan11" name="cek_kepengurusan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_kepengurusan22" name="cek_kepengurusan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab11" name="cek_rab" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_rab22" name="cek_rab" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab))
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Waktu Pelaksanaan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan11" name="cek_waktu_pelaksanaan" value="1" disabled="" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="cek_waktu_pelaksanaan22" name="cek_waktu_pelaksanaan" value="2" disabled="" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="checkbox" disabled="" name="is_approve"
                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->is_approve==1)
                                                checked
                                                @endif
                                            > 
                                            <span style="color:red;font-weight: bolder;">Dengan ini Dinyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                                </form>
                                @else
                                <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endif
</div>