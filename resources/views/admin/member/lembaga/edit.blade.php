@extends('admin/layout/template')
@section('content')
<style type="text/css">
    .help-block-error{
        color: red;
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('member.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Perbaiki Detil Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Perbaiki Detil Lembaga</span>
                        </div>
                    </div>

                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('member.lembaga.update') }}" id="form_sample_1" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}
                                <input type="hidden" name="nomor" value="{{ $recLembagaByNomor->nomor }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3">SKPD
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="nm_skpd" type="text" data-required="1" class="form-control" value="{{ $recLembagaByNomor->nm_skpd }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tipe Lembaga
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="tipeLembagaID" type="text" data-required="1" class="form-control" value="{{ ucwords($recLembagaByNomor->namaTipeLembaga) }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lembaga
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="nm_lembaga" type="text" data-required="1" class="form-control" value="{{ $recLembagaByNomor->nm_lembaga }}" required>
                                    </div>
                                </div>
                                @if($recLembagaByNomor->tipeLembagaID == 1 )
                               
                                 <div class="form-group">
                                    <label class="control-label col-md-3">No. NPWP 
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                        <input name="npwp" type="text" class="form-control" value="{{ $recLembagaByNomor->npwp }}" id="NPWP"> 
                                        <span class="input-group-btn">
                                            <button id="cekNPWP" class="btn btn-success" type="button">
                                                <i class="fa fa-check"></i> Cek NPWP </button>
                                        </span>
                                        </div>  
                                    </div>
                                </div>

                                @else
                                    <div class="form-group">
                                        <label class="control-label col-md-3">No. NPWP
                                            <span class="required" aria-required="true"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                            <input name="npwp" type="text" class="form-control" value="{{ $recLembagaByNomor->npwp }}" required  id="NPWP"> 
                                            <span class="input-group-btn">
                                                <button id="cekNPWP" class="btn btn-success" type="button">
                                                    <i class="fa fa-check"></i> Cek NPWP </button>
                                            </span>
                                            </div>  
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat Lengkap
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <textarea name="alamat" class="form-control" required="">{{ $recLembagaByNomor->alamat }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Pilih Kota/Kecamatan
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="nokota" name="nokota" required>
                                            <option value="">Pilih Kota</option>
                                            @foreach($recKota as $key=>$value)
                                                <option value="{{ $value->nokota }}" {{ ($value->nokota == $recLembagaByNomor->nokota)?'selected':'' }}>{{ $value->nmkota }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" id="nokec" name="nokec" required="">
                                            <option value="">Pilih Kecamatan</option>
                                            @foreach($recKecamatan as $key=>$value)
                                                <option value="{{ $value->nokec }}" class="{{ $value->nokota }}" {{ ($value->nokec == $recLembagaByNomor->nokec)?'selected':'' }}>{{ $value->nmkec }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Pilih Kelurahan/RT/RW
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="nokel" name="nokel" required="">
                                            <option value="">Pilih Kelurahan</option>
                                            @foreach($recKelurahan as $key=>$value)
                                                <option value="{{ $value->nokel }}" class="{{ $value->nokec }}" {{ ($value->nokel == $recLembagaByNomor->nokel)?'selected':'' }}>{{ $value->nmkel }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control select2me" name="rtID" required="">
                                            <option value="NULL">Pilih RT</option>
                                            @foreach($recMstRT as $key=>$value)
                                                <option value="{{ $value->id }}" {{ ($value->id == $recLembagaByNomor->rtID)?'selected':'' }}>{{ $value->nama }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control select2me" name="rwID" required>
                                            <option value="NULL">Pilih RW</option>
                                            @foreach($recMstRW as $key=>$value)
                                                <option value="{{ $value->id }}" {{ ($value->id == $recLembagaByNomor->rwID)?'selected':'' }}>{{ $value->nama }}</option>
                                            @endforeach
                                        </select>    
                                    </div>
                                </div>
                                @if($recLembagaByNomor->tipeLembagaID == 2)
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. AKTA KUMHAM
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_akta" type="text" class="form-control" value="{{ $recLembagaByNomor->no_akta }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal AKTA
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd" {{-- data-date-end-date="-3y" --}}>
                                            <input type="text" class="form-control" readonly name="tgl_akta" value="{{ $recLembagaByNomor->tgl_akta }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <span class="required">* Validasi akta pendirian minimal sudah 3 tahun berdiri (tanggal hari ini di kurangi 3 tahun).</span>
                                    </div> --}}
                                </div>
                                @else
                                 <div class="form-group">
                                    <label class="control-label col-md-3">No. AKTA KUMHAM 
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_akta" type="text" class="form-control" value="{{ $recLembagaByNomor->no_akta }}" > </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal AKTA 
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd" {{-- data-date-end-date="-3y" --}}>
                                            <input type="text" class="form-control" readonly name="tgl_akta" value="{{ $recLembagaByNomor->tgl_akta }}" >
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <span class="required">* Validasi akta pendirian minimal sudah 3 tahun berdiri (tanggal hari ini di kurangi 3 tahun).</span>
                                    </div> --}}
                                </div>

                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Surat Domisili
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_surat_domisili" type="text" class="form-control" value="{{ $recLembagaByNomor->no_surat_domisili }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Surat Domisili
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_surat_domisili" value="{{ $recLembagaByNomor->tgl_surat_domisili }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                @if($recLembagaByNomor->tipeLembagaID == 1)
                                
                                 <div class="form-group">
                                    <label class="control-label col-md-3">No. Sertifikat 
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_sertifikat" type="text" class="form-control" value="{{ $recLembagaByNomor->no_sertifikat }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Sertifikat 
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="tgl_sertifikat" value="{{ $recLembagaByNomor->tgl_sertifikat }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div> 
                                @else
                                 <div class="form-group">
                                    <label class="control-label col-md-3">No. Sertifikat
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_sertifikat" type="text" class="form-control" value="{{ $recLembagaByNomor->no_sertifikat }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Sertifikat
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_sertifikat" value="{{ $recLembagaByNomor->tgl_sertifikat }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                @endif

                                @if($recLembagaByNomor->tipeLembagaID == 3 )
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Izin Operasional 
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_izin_operasional" type="text" class="form-control" value="{{ $recLembagaByNomor->no_izin_operasional }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Izin Operasional 
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_izin_operasional" value="{{ $recLembagaByNomor->tgl_izin_operasional }}"  >
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div>
                                @else
                                 <div class="form-group">
                                    <label class="control-label col-md-3">No. Izin Operasional
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input name="no_izin_operasional" type="text" class="form-control" value="{{ $recLembagaByNomor->no_izin_operasional }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tanggal Izin Operasional
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" readonly name="tgl_izin_operasional" value="{{ $recLembagaByNomor->tgl_izin_operasional }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block"> Pilih tanggal </span>
                                    </div>
                                </div> 
                                @endif
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Telepon
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_telepon" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->no_telepon }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Fax
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_fax" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->no_fax }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email Lembaga
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email1" type="text" class="form-control" value="{{ $recLembagaByNomor->email }}" required> </div>
                                </div>
                                <h3 class="form-section">Akun Bank</h3>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Bank
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="nama_bank" type="text" class="form-control" value="{{ $recLembagaByNomor->nama_bank }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. Rekening
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_rek" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->no_rek }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rekening a/n
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="pemilik_rek" type="text" class="form-control" value="{{ $recLembagaByNomor->pemilik_rek }}" required> </div>
                                </div>
                                <h3 class="form-section">Kontak Pengurus</h3>
                                <hr>
                                <h4>Ketua</h4>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="kontak_person" type="text" class="form-control" value="{{ $recLembagaByNomor->kontak_person }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">NIK
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        

                                         <div class="input-group">
                                      <input name="nik_person" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->nik_person }}" required id="NIK"> 
                                        <span class="input-group-btn">
                                            <button id="cekNIK" class="btn btn-success" type="button">
                                                <i class="fa fa-check"></i> Cek NIK </button>
                                        </span>
                                        </div>  

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email_person" type="email" class="form-control" value="{{ $recLembagaByNomor->email_person }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="alamat_person" class="form-control" rows="5" required="">{{ $recLembagaByNomor->alamat_person }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. HandPhone
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_hp_person" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->no_hp_person }}" required> </div>
                                </div>
                                <h4>Sekretaris</h4>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="kontak_person_sekretaris" type="text" class="form-control" value="{{ $recLembagaByNomor->kontak_person_sekretaris }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email_person_sekretaris" type="email" class="form-control" value="{{ $recLembagaByNomor->email_person_sekretaris }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="alamat_person_sekretaris" class="form-control" rows="5" required="">{{ $recLembagaByNomor->alamat_person_sekretaris }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. HandPhone
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_hp_person_sekretaris" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->no_hp_person_sekretaris }}" required> </div>
                                </div>
                                <h4>Bendahara</h4>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="kontak_person_bendahara" type="text" class="form-control" value="{{ $recLembagaByNomor->kontak_person_bendahara }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email_person_bendahara" type="email" class="form-control" value="{{ $recLembagaByNomor->email_person_bendahara }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="alamat_person_bendahara" class="form-control" rows="5" required="">{{ $recLembagaByNomor->alamat_person_bendahara }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">No. HandPhone
                                    <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="no_hp_person_bendahara" type="text" class="form-control only-numeric" value="{{ $recLembagaByNomor->no_hp_person_bendahara }}" required> </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan Perubahan</button>
                                        <button onclick="javascript:window.history.back()" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $( "input.only-numeric" ).on( "blur", function() {
          $( this ).val(function( i, val ) {
            return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
          });
        });

        $("#nokec").chained("#nokota");
        $("#nokel").chained("#nokec");
    </script>

     <script type="text/javascript">
        $(function(){ 

            $("#cekNPWP").click(function(){
                var npwp = $("#NPWP").val();

                if(npwp == ""){
                  alert("NPWP tidak boleh kosong");
                }

                 $.ajax({
                      async: true,
                      type: "POST",
                      // url: "{{ route('lembaga.cek.npwp') }}",
                      url: "http://ehibahbansosdki.jakarta.go.id/member/lembaga/cek/npwp",
                      dataType: "json",
                      data: { 
                             _token: "{{ csrf_token() }}",
                             npwp:npwp,   
                         },
                      success : function(res){
                        if(res.status == true){ 
                          Command: toastr["success"]("Sukses", res.msg) 
                          toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          }
  
                          $("#nama").val(res.nama);
                        }
                        else{ 
                            Command: toastr["error"]("NPWP Tidak Ditemukan",res.msg) 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
   
                          
                        }
                      },
                      error : function(err, ajaxOptions, thrownError){
                           $("#confirm-popup").modal('hide');
                            Command: toastr["error"]("Error", "Not respond please try again ") 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            } 
                      }
                    });  
            });

             $("#cekNIK").click(function(){
                var nik = $("#NIK").val();

                if(nik == ""){
                  alert("nik tidak boleh kosong");
                }

                 $.ajax({
                      async: true,
                      type: "POST",
                      // url: "{{ route('lembaga.cek.nik') }}",
                      url: "http://ehibahbansosdki.jakarta.go.id/member/lembaga/cek/nik",
                      dataType: "json",
                      data: { 
                             _token: "{{ csrf_token() }}",
                             nik:nik,   
                         },
                      success : function(res){
                        if(res.status == true){ 
                          Command: toastr["success"]("Sukeses", res.msg) 
                          toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          }
  
                          $("#nama").val(res.nama);
                        }
                        else{ 
                            Command: toastr["error"]("NIK Tidak ditemukan",res.msg) 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
   
                          
                        }
                      },
                      error : function(err, ajaxOptions, thrownError){
                           $("#confirm-popup").modal('hide');
                            Command: toastr["error"]("Error", "Not respond please try again ") 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            } 
                      }
                    });  
            });

        });
    </script>
@stop