<div class="tab-pane fade active in" id="tab_1_0">
    <div class="portlet-title">
        <div class="caption" style="width: 95%;">
            <a href="{{ route('member.lembaga.edit',['id'=>base64_encode($recLembagaByNomor->nomor)]) }}" class="btn btn-circle btn-info" style="float:right;box-shadow: 2px 2px 5px #88898A !important;"><i class="fa fa-bank"></i> Perbaiki Detil Lembaga</a>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <!-- BEGIN FORM-->
    <form class="form-horizontal" role="form">
        <div class="form-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        @if($recLembagaByNomor->fotoLembaga==NULL)
                        <img src="{{url('/')}}/assets/images/default-logo-lembaga.jpg" class="img-responsive" width="140">
                        @else
                        <img src="{{url('/')}}/foto_lembaga/{{ $recLembagaByNomor->fotoLembaga }}" class="img-responsive" width="140" style="border: 1px solid #c3c3c3;border-radius: 10px !important;padding: 10px;">
                        @endif
                        <center>
                        <a href="{{ route('member.lembaga.foto.lembaga') }}" class="btn btn-circle green btn-sm" style="font-size: 10px;box-shadow: 2px 2px 5px #88898A !important;margin-top: 5px;"><i class="fa fa-file-photo-o"></i> Ubah Logo Lembaga</a>
                        </center>
                    </div>
                    <div class="col-md-10">
                        <h3 style="height: 140px;padding-top: 33px;font-weight: bolder;">{{ $recLembagaByNomor->nm_lembaga }}</h3>
                    </div>
                </div>
            </div>
            <h3 class="form-section">Data Utama</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>Alamat:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->alamat }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>NPWP:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->npwp }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>RT/RW:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->noRT }}/{{$recLembagaByNomor->noRW}} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>Scan NPWP:</b></label>
                        <div class="col-md-9">
                            <div class="form-group">
                                @if(empty($recLembagaByNomor->npwp))
                                <a href="{{ route('member.lembaga.npwp') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                    <i class="m-icon-swapup m-icon-white"></i>
                                </a>
                                @else
                                    <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->scanNPWP }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->scanNPWP,20) }} <i class="fa fa-external-link"></i></a>
                                    @if($recUserLembagaByNoLembaga->is_approve!=999)
                                    <a href="{{ route('member.lembaga.npwp') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>Kelurahan:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->nmkel }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>EMail:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->email }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>Kecamatan:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->nmkec }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>No Telp:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_telepon }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>Kota:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->nmkota }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><b>No Fax:</b></label>
                        <div class="col-md-9">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_fax }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <h3 class="form-section">Data Khusus</h3>
            <hr>
            <h4><u>Akta Kumham</u></h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nomor:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_akta }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Tanggal:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->tgl_akta }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Scan Dokumen:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->akta))
                            <a href="{{ route('member.lembaga.akta') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->akta }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->akta,20) }} <i class="fa fa-external-link"></i></a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.akta') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <h4><u>Sertifikat</u></h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nomor:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_sertifikat }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Tanggal:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->tgl_sertifikat }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Scan Dokumen:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->sertifikat))
                            <a href="{{ route('member.lembaga.sertifikat') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->sertifikat }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->sertifikat,20) }} <i class="fa fa-external-link"></i></a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.sertifikat') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <h4><u>Surat Domisili</u></h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nomor:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_surat_domisili }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Tanggal:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->tgl_surat_domisili }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Scan Dokumen:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->skd))
                            <a href="{{ route('member.lembaga.skd') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->skd }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->skd,20) }} <i class="fa fa-external-link"></i></a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.skd') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <h4><u>Izin Operasional</u></h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nomor:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_izin_operasional }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Tanggal:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->tgl_izin_operasional }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Scan Dokumen:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->izin_operasional))
                            <a href="{{ route('member.lembaga.izin.operasional') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->izin_operasional }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->izin_operasional,20) }} <i class="fa fa-external-link"></i></a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.izin.operasional') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <h3 class="form-section">Rekening Lembaga</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nama Bank:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->nama_bank }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Atas Nama:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->pemilik_rek }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>No Rekening:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_rek }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Scan Rekening:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->rekening_lembaga))
                            <a href="{{ route('member.lembaga.rekening') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/berkas_rekening/{{ $recLembagaByNomor->rekening_lembaga }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->rekening_lembaga,25) }}</a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.rekening') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <h3 class="form-section">Kontak Pengurus</h3>
            <hr>
            <h4><u>Ketua</u></h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nama:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->kontak_person }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>NIK:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->nik_person }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Scan KTP:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->ktp_ketua))
                            <a href="{{ route('member.lembaga.ktp.ketua') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/user_lembaga/{{ $recLembagaByNomor->ktp_ketua }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->ktp_ketua,25) }}</a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.ktp.ketua') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Alamat:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->alamat_person }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>No Telp/HP:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_hp_person }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>EMail:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->email_person }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Foto:</b></label>
                        <div class="col-md-8">
                            @if(empty($recLembagaByNomor->foto_person))
                            <a href="{{ route('member.lembaga.foto.ketua') }}" class="btn btn-circle dark m-icon"> Upload Dokumen
                                <i class="m-icon-swapup m-icon-white"></i>
                            </a>
                            @else
                                <a href="{{ url('/') }}/foto_ketua_lembaga/{{ $recLembagaByNomor->foto_person }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recLembagaByNomor->foto_person,25) }}</a>
                                @if($recUserLembagaByNoLembaga->is_approve!=999)
                                <a href="{{ route('member.lembaga.foto.ketua') }}" class="btn btn-link" style="color:red;"><u><i class="fa fa-upload"></i> re-upload</u></a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>

            <h4><u>Bendahara</u></h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nama:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->kontak_person_bendahara }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>EMail:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->email_person_bendahara }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Alamat:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->alamat_person_bendahara }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>No Telp/HP:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_hp_person_bendahara }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>

            <h4><u>Sekretaris</u></h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Nama:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->kontak_person_sekretaris }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>EMail:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->email_person_sekretaris }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>Alamat:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->alamat_person_sekretaris }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><b>No Telp/HP:</b></label>
                        <div class="col-md-8">
                            <p class="form-control-static"> {{ $recLembagaByNomor->no_hp_person_sekretaris }} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
        </div>
    </form>
    <!-- END FORM-->
    <div class="portlet light bordered">
        <div class="portlet-body form">
            <h3>Lokasi Lembaga
            @if($recLembagaByNomor->latitude==NULL || $recLembagaByNomor->latitude==0)
            <b style="color: red;">Belum Tersedia</b>
            @endif
            <a href="{{ route('member.lembaga.lokasi.edit',['id'=>base64_encode($recLembagaByNomor->nomor)]) }}" class="btn btn-circle btn-info" style="float:right;box-shadow: 2px 2px 5px #88898A !important;"><i class="icon-pointer"></i> Perbaiki Lokasi Lembaga</a>
            </h3>
            <hr>
            <div class="input-group">
                <input type="hidden" class="form-control" id="pac-input" placeholder="Cari Alamat">
            </div>
            <div id="gmap_basic" class="gmaps"> </div>
            <b>Latitude : </b>
            <input type="text" class="form-control" name="latitude" id="latVal" value="{{ $recLembagaByNomor->latitude }}" >
            <b>Longitude : </b>
            <input type="text" class="form-control" name="longitude" id="longVal" value="{{ $recLembagaByNomor->longitude }}" >
        </div>
    </div>
</div>