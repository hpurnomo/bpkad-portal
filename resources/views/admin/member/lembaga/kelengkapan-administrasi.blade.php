<div class="tab-pane fade" id="tab_1_1">
    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
    <form action="{{ route('skpd.lembaga.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
    <div class="form-group">
        <div class="form-body">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="{{ $recUserLembagaByNoLembaga->id }}">
            <input type="hidden" name="no_lembaga" value="{{ $recUserLembagaByNoLembaga->no_lembaga }}">
            <input type="hidden" name="email" value="{{ $recLembagaByNomor->email }}">
            <input type="hidden" name="nm_lembaga" value="{{ $recLembagaByNomor->nm_lembaga }}">
            <input type="hidden" name="nm_skpd" value="{{ $recLembagaByNomor->nm_skpd }}">
            <div class="form-group form-md-radios">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="50%"><center>Data Administrasi</center></th>
                                <th><center>Ada</center></th>
                                <th><center>Tidak Ada</center></th>
                                <th><center>Keterangan</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Nama dan Identitas</td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_ktp_ketua==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_ktp_ketua==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_ktp_ketua !!}</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Alamat</td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_alamat_lembaga==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_alamat_lembaga==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_alamat_lembaga !!}</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Saldo akhir tahun lalu beserta rekening Bank</td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_rekening_lembaga==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_rekening_lembaga==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_rekening_lembaga !!}</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila ada</td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_bantuan_tahun_sebelum==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_bantuan_tahun_sebelum==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_bantuan_tahun_sebelum !!}</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Nomor Pokok Wajib Pajak <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_npwp==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_npwp==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_npwp !!}</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_akta==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_akta==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_akta !!}</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Surat Keterangan Domisili dari Kelurahan setempat <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_skd==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_skd==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_skd !!}</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_sertifikat==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_sertifikat==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_sertifikat !!}</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_izin_operasional==1)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>
                                    @if ($recUserLembagaByNoLembaga->cek_izin_operasional==2)
                                        <center><i class="fa fa-check-square-o"></i></center>
                                    @endif
                                </td>
                                <td>{!! $recUserLembagaByNoLembaga->keterangan_cek_izin_operasional !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <center>
                <input type="checkbox" name="is_approve" {{ ($recUserLembagaByNoLembaga->is_approve==1)?'checked':'' }} disabled=""> <span style="color:red;font-weight: bolder;">Dengan ini Dinyatakan Lembaga Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
            </center>
        </div>
    </div>
    </form>
</div>