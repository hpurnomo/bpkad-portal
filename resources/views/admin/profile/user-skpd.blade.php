@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="background-color: #eef1f5 !important;">
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Profil Saya
            <small></small>
        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="profile">
            <div class="tabbable-line tabbable-full-width">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" data-toggle="tab"> Info </a>
                    </li>
                    <li>
                        <a href="#tab_1_3" data-toggle="tab"> Pengaturan Akun </a>
                    </li>
                    {{-- <li>
                        <a href="#tab_1_6" data-toggle="tab"> Help </a>
                    </li> --}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="list-unstyled profile-nav">
                                    <li>
                                        @if(empty($recUser->avatar))
                                            <img alt="avatar"  class="img-responsive pic-bordered" alt="" src="admin/assets/pages/media/profile/avatar_2.jpg" />
                                        @else
                                            <img alt="avatar"  class="img-responsive pic-bordered" alt="" src="{{ URL::to('/') }}/foto_user/avatar/{{ $recUser->avatar }}" />
                                        @endif
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12 profile-info">
                                        <h1 class="font-green sbold uppercase">{{ Session::get('real_name') }}</h1>
                                        <p>&nbsp;</p>
                                        <div class="portlet sale-summary">
                                            <div class="portlet-title">
                                                {{-- <div class="caption font-red sbold"> Member Summary </div> --}}
                                                <div class="tools">
                                                    <!-- <a class="reload" href="javascript:;"> </a> -->
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <span class="sale-info"> Status
                                                            <i class="fa fa-img-up"></i>
                                                        </span>
                                                        <span class="sale-num"><span class="label label-{{ (Session::get('is_verify') == 0)?'danger':'success' }}"> {{ (Session::get('is_verify') == 0)?'Belum Terverifikasi':'Terverifikasi' }} </span></span>
                                                    </li>
                                                    <li>
                                                        <span class="sale-info"> Kategori
                                                            <i class="fa fa-img-up"></i>
                                                        </span>
                                                        <span class="sale-num"> {{ $recUserGroup['group_name'] }}</span>
                                                    </li>
                                                    <li>
                                                        <span class="sale-info"> Nama SKPD
                                                            <i class="fa fa-img-up"></i>
                                                        </span>
                                                        <span class="sale-num"> {{ ucwords($recUser->nm_skpd) }} </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end row-->
                            </div>
                        </div>
                    </div>
                    <!--tab_1_2-->
                    <div class="tab-pane" id="tab_1_3">
                        <div class="row profile-account">
                            <div class="col-md-3">
                                <ul class="ver-inline-menu tabbable margin-bottom-10">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab_1-1">
                                            <i class="fa fa-cog"></i> Pengaturan Biodata Diri </a>
                                        <span class="after"> </span>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_2-2">
                                            <i class="fa fa-picture-o"></i> Pengaturan Avatar </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_3-3">
                                            <i class="fa fa-lock"></i> Pengaturan Password </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                @include('admin.part.alert')
                                <div class="tab-content">
                                    <div id="tab_1-1" class="tab-pane active">
                                        <form id="form_sample_1" role="form" action="{{ route('profile.user.lembaga.update') }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="user_id" value="{{ $recUser->user_id }}">
                                            <div class="form-group">
                                                <label class="control-label">No. Registrasi<span class="required" aria-required="true"> * </span></label>
                                                <input type="text" name="no_registrasi" value="{{ $recUser->no_registrasi }}" class="form-control" readonly/> </div>
                                            <div class="form-group">
                                                <label class="control-label">NRK<span class="required" aria-required="true"> * </span></label>
                                                <input type="text" name="nrk" value="{{ $recUser->nrk }}" class="form-control" readonly="" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">E-Mail<span class="required" aria-required="true"> * </span></label>
                                                <input type="text" name="email" value="{{ $recUser->email }}" class="form-control" readonly="" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Nama Sesuai KTP<span class="required" aria-required="true"> * </span></label>
                                                <input type="text" name="real_name" value="{{ $recUser->real_name }}" class="form-control" required/> </div>
                                            <div class="form-group">
                                                <label class="control-label">Telepon<span class="required" aria-required="true"> * </span></label>
                                                <input type="text" name="telp" value="{{ $recUser->telp }}" class="form-control only-numeric" required/> </div>
                                            <div class="form-group">
                                                <label class="control-label">No. KTP<span class="required" aria-required="true"> * </span></label>
                                                <input type="text" name="ktp" value="{{ $recUser->ktp }}" class="form-control only-numeric" required/> </div>
                                            <div class="form-group">
                                                <label class="control-label">Alamat Sesuai KTP <span class="required" aria-required="true"> * </span></label>
                                                <textarea class="form-control" rows="3" name="address" required>{{ $recUser->address }}</textarea> </div>
                                            <div class="form-group">
                                                <label class="control-label">Foto KTP</label><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 300; height: 300px;">
                                                        @if(empty($recUser->photo))
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                        @else
                                                            <img alt="avatar" src="{{ URL::to('/') }}/foto_user/{{ $recUser->photo }}" style="max-width: 300px; max-height: 300px;"/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Pilih Gambar </span>
                                                            <span class="fileinput-exists"> Pilih Gambar Lainnya </span>
                                                            <input type="file" name="photo"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger"> <em>NOTE : File harus berformat JPG/PNG Max 1MB </em></span>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="margiv-top-10">
                                                <button type="submit" class="btn green"> Simpan Perubahan </button>
                                                <button type="reset" class="btn default"> Reset </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="tab_2-2" class="tab-pane">
                                        <form role="form" action="{{ route('profile.user.lembaga.update.avatar') }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="user_id" value="{{ $recUser->user_id }}">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 300; height: 300px;">
                                                        @if(empty($recUser->avatar))
                                                            <img src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                        @else
                                                            <img alt="avatar" src="{{ URL::to('/') }}/foto_user/avatar/{{ $recUser->avatar }}" style="max-width: 300px; max-height: 300px;"/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Pilih Gambar </span>
                                                            <span class="fileinput-exists"> Pilih Gambar Lainnya </span>
                                                            <input type="file" name="avatar"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger"><em> NOTE : File harus berformat JPG/PNG Max 1MB </em></span>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="margin-top-10">
                                                <button type="submit" class="btn green"> Simpan Perubahan </button>
                                                <button type="reset" class="btn default"> Reset </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="tab_3-3" class="tab-pane">
                                        <form action="{{ route('profile.user.lembaga.reset.password') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="user_id" value="{{ $recUser->user_id }}">
                                            <div class="form-group">
                                                <label class="control-label">New Password</label>
                                                <input type="password" name="password" class="form-control" required="" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Re-type New Password</label>
                                                <input type="password" name="password_confirmation" class="form-control" required="" /> </div>
                                            <hr>
                                            <div class="margin-top-10">
                                                <button type="submit" class="btn green"> Simpan Perubahan </button>
                                                <button type="reset" class="btn default"> Reset </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--end col-md-9-->
                        </div>
                    </div>
                    <!--end tab-pane-->
                    {{-- <div class="tab-pane" id="tab_1_6">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="ver-inline-menu tabbable margin-bottom-10">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab_1">
                                            <i class="fa fa-group"></i> Membership </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <div id="tab_1" class="tab-pane active">
                                        <div id="accordion2" class="panel-group">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1"> 1. Bagaimana Membuat Akun Anda Ter-Verifikasi ? </a>
                                                    </h4>
                                                </div>
                                                <div id="accordion2_1" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p>  
                                                            <ol>
                                                                <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</li>
                                                                <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</li>
                                                                <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</li>
                                                                <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</li>
                                                                <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</li>
                                                            </ol>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_2"> 2. Hal-hal apa saja yang didapatkan oleh <b>User</b> yang <b>Belum Ter-Verifikasi</b> ? </a>
                                                    </h4>
                                                </div>
                                                <div id="accordion2_2" class="panel-collapse collapse">
                                                    <div class="panel-body"> 
                                                        <p>  
                                                            <ol>
                                                                <li>Update Profile</li>
                                                                <li>Upload Dokumen Lembaga</li>
                                                                <li>Input Proposal</li>
                                                            </ol>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!--end tab-pane-->
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop