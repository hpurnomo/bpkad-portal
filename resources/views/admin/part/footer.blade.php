<!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> {{ date('Y') }} &copy; All Rights Reserved by BPKD.
                <b><a href="http://klakklik.id" title="klakklik" target="_blank" style="color:white;">Contact Developer</a></b>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->