<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="nav-item start {{ ( Route::getCurrentRoute()->getPath() == 'dashboard' )?'active':'' }}">
                <a href="{{ route('dashboard.member') }}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Fitur SKPD</h3>
            </li>
            <li class="nav-item start {{ ( Route::getCurrentRoute()->getPath() == 'skpd/profil' )?'active':'' }}">
                <a href="{{ route('skpd.profil') }}" class="nav-link ">
                    <i class="icon-user"></i>
                    <span class="title">Profil SKPD</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'skpd/user-management') != NULL)?'active':'' }}">
                <a href="{{ route('skpd.user.management.manage') }}" class="nav-link ">
                    <i class="icon-users"></i>
                    <span class="title">Users Management</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'skpd/lembaga' ) !=NULL)?'active':'' }}">
                <a href="{{ route('skpd.lembaga.manage') }}" class="nav-link ">
                    <i class="icon-rocket"></i>
                    <span class="title">Lembaga</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'skpd/proposal') !=NULL )?'active':'' }}">
                <a href="{{ route('skpd.proposal.index') }}" class="nav-link ">
                    <i class="icon-docs"></i>
                    <span class="title">Proposal</span>
                    <span class="selected"></span>
                </a>
            </li>
             <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/monitoring') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.monitoring.manage') }}" class="nav-link ">
                    <i class="icon-rocket"></i>
                    <span class="title">Monitoring SP2D</span>
                    <span class="selected"></span>
                </a>
            </li>
            
            {{-- <li class="heading">
                <h3 class="uppercase">Administrasi</h3>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'skpd/tim-peneliti') !=NULL )?'active':'' }}">
                <a href="{{ route('skpd.tim.peneliti.manage') }}" class="nav-link ">
                    <i class="icon-settings"></i>
                    <span class="title">Tim Peneliti Administrasi</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'skpd/proposal') !=NULL )?'active':'' }}">
                <a href="{{ route('skpd.proposal.manage') }}" class="nav-link ">
                    <i class="icon-settings"></i>
                    <span class="title">Tim Peninjauan Lapangan</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'skpd/proposal') !=NULL )?'active':'' }}">
                <a href="{{ route('skpd.proposal.manage') }}" class="nav-link ">
                    <i class="icon-settings"></i>
                    <span class="title">Pengaturan Pejabat Terkait</span>
                    <span class="selected"></span>
                </a>
            </li> --}}
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR