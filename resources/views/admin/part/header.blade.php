		<!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ route('dashboard.member') }}">
                        <img src="{{ url('/') }}/admin/assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <div id="logo-center" style="display:none;position: absolute;right: 650px;top: 15px;">
                        <a href="{{ route('dashboard.member') }}">
                        <img src="{{ url('/') }}/admin/assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                    </div>
                    @inject('user','BPKAD\User')
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if(empty($user->where('user_id',Session::get('userID'))->get()[0]['avatar']))
                                    <img alt="avatar" class="img-circle" src="{{ URL::to('/') }}/admin/assets/layouts/layout/img/avatar.png" />
                                @else
                                    <img alt="avatar" class="img-circle" src="{{ URL::to('/') }}/foto_user/avatar/{{ $user->where('user_id',Session::get('userID'))->get()[0]['avatar'] }}" />
                                @endif

                                 @if(Auth::user()->email=='bpk_dki@jakarta.go.id')
                                     <span class="username username-hide-on-mobile"> {{ ucwords($user->where('user_id',Session::get('userID'))->get()[0]['real_name']) }} (BPK)
                                 @else
                                    <span class="username username-hide-on-mobile"> {{ ucwords($user->where('user_id',Session::get('userID'))->get()[0]['real_name']) }}
                                    @if(ucwords($user->where('user_id',Session::get('userID'))->get()[0]['group_id'])==8)
                                        (PPKD)
                                    @elseif(ucwords($user->where('user_id',Session::get('userID'))->get()[0]['group_id'])==10)
                                        (SKPD)
                                    @else
                                        (LEMBAGA)
                                    @endif
                                @endif
                                </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ route('profile.user') }}">
                                        <i class="icon-user"></i> Profil Saya  </a>
                                </li>
                                <!-- <li>
                                    <a href="app_todo.html">
                                        <i class="icon-wrench"></i> Ubah Password
                                    </a>
                                </li> -->
                                <li class="divider"> </li>
                                <li>
                                    <a href="{{ URL::to('logout') }}">
                                        <i class="icon-logout"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->