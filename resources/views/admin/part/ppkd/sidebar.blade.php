<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="nav-item start {{ ( Route::getCurrentRoute()->getPath() == 'dashboard' )?'active':'' }}">
                <a href="{{ route('dashboard.member') }}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            @if(Auth::user()->email=='bpk_dki@jakarta.go.id' 
            || Auth::user()->email=='riantawidya.rwi@gmail.com'
            || Auth::user()->email=='dki.inspektorat@gmail.com'
            || Auth::user()->email=='sobarmuhamad68@gmail.com'
            || Auth::user()->email=='ahmadsofyankgb@gmail.com'
            || Auth::user()->email=='hesti.dianingrum@yahoo.co.id'
            )
            <li class="heading">
                <h3 class="uppercase">Fitur BPK</h3>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/skpd-koordinator') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.skpd.koordinator.manage') }}" class="nav-link ">
                    <i class="icon-share"></i>
                    <span class="title">SKPD Koordinator</span>
                    <span class="selected"></span>
                </a>
            </li> 
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/monitoring') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.monitoring.manage') }}" class="nav-link ">
                    <i class="icon-rocket"></i>
                    <span class="title">Monitoring SP2D</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/lembaga') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.lembaga.manage') }}" class="nav-link ">
                    <i class="icon-rocket"></i>
                    <span class="title">Lembaga</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/proposal') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.proposal.index') }}" class="nav-link ">
                    <i class="icon-docs"></i>
                    <span class="title">Proposal</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/download-rekap') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.download.rekap.index') }}" class="nav-link ">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    <span class="title">Download Rekap</span>
                    <span class="selected"></span>
                </a>
            </li>
            @else
             <li class="heading">
                <h3 class="uppercase">Fitur PPKD</h3>
            </li>
          
           {{-- <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/user-management') != NULL)? 'active':'' }}">
                <a href="javascript:;"  class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">User Management</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ">
                        <a href="{{ route('ppkd.user.management.manage','10') }}" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">User SKPD</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ route('ppkd.user.management.manage','13') }}" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">User BPK</span> 
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="dashboard_3.html" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">User PPKD (Simpeg) </span> 
                        </a>
                    </li>
                </ul>
            </li> --}}
          <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/user-management') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.user.management.manage','10') }}" class="nav-link ">
                    <i class="icon-users"></i>
                    <span class="title">Users Management</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/periode-tahapan') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.periode.tahapan.manage') }}" class="nav-link ">
                    <i class="icon-calendar"></i>
                    <span class="title">Periode/Tahapan</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/skpd-koordinator') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.skpd.koordinator.manage') }}" class="nav-link ">
                    <i class="icon-share"></i>
                    <span class="title">SKPD Koordinator</span>
                    <span class="selected"></span>
                </a>
            </li>
           {{-- <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/pengaturan-token') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.pengaturan.token.manage') }}" class="nav-link ">
                    <i class="icon-social-tumblr"></i>
                    <span class="title">Pengaturan Token</span>
                    <span class="selected"></span>
                </a>
            </li> --}}
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/monitoring') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.monitoring.manage') }}" class="nav-link ">
                    <i class="icon-rocket"></i>
                    <span class="title">Monitoring SP2D</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/lembaga') != NULL)? 'active':'' }}">
                <a href="{{ route('ppkd.lembaga.manage') }}" class="nav-link ">
                    <i class="icon-rocket"></i>
                    <span class="title">Lembaga</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/proposal') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.proposal.index') }}" class="nav-link ">
                    <i class="icon-docs"></i>
                    <span class="title">Proposal</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/download-rekap') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.download.rekap.index') }}" class="nav-link ">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    <span class="title">Download Rekap</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/running-text') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.running.text.manage') }}" class="nav-link ">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="title">Pengaturan Running Text</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/info-terkini') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.info.terkini.manage') }}" class="nav-link ">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="title">Pengaturan Info Terkini</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/pengumuman') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.pengumuman.manage') }}" class="nav-link ">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="title">Pengaturan Pengumuman</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/testimonial') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.testimonial.manage') }}" class="nav-link ">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="title">Pengaturan Testimonial</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start {{ (starts_with(Route::getCurrentRoute()->getPath(),'ppkd/faq') != NULL )? 'active':'' }}">
                <a href="{{ route('ppkd.faq.manage') }}" class="nav-link ">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="title">Pengaturan FAQ</span>
                    <span class="selected"></span>
                </a>
            </li>
             <li class="nav-item start {{ ( starts_with(Route::getCurrentRoute()->getPath(), 'ppkd/user-management') != NULL)? 'active':'' }}">
                <a href="javascript:;"  class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">Master Data</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ">
                        <a href="{{ route('ppkd.master.kelurahan.index') }}" class="nav-link ">
                            <i class="icon-folder"></i>
                            <span class="title">Kelurahan</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ route('ppkd.master.kecamatan.index') }}" class="nav-link ">
                            <i class="icon-folder"></i>
                            <span class="title">Kecamatan</span> 
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ route('ppkd.master.kabupaten.index') }}" class="nav-link ">
                            <i class="icon-folder"></i>
                            <span class="title">Kabupaten/Kota</span> 
                        </a>
                    </li>
                     <li class="nav-item start ">
                        <a href="{{ route('ppkd.master.provinsi.index') }}" class="nav-link ">
                            <i class="icon-folder"></i>
                            <span class="title">Provinsi</span> 
                        </a>
                    </li>
                </ul>
            </li>
              @endif

        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->