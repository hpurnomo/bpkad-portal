@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Cetak</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{dd($recProposalByID)}} --}}
        
        <a href="{{ route('skpd.proposal.show',['id'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase]) }}" class="btn btn-default" style="float: right;">Kembali</a><br><br>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-print" aria-hidden="true"></i> Pengaturan Cetak Laporan </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <ul class="nav nav-tabs tabs-left">
                            <li class="{{($tab=='kelengkapan-administrasi')?'active':''}}">
                                <a href="#tab_6_1" data-toggle="tab" aria-expanded="true"> Kelengkapan Administrasi </a>
                            </li>
                            <li class="{{($tab=='peninjauan-lapangan')?'active':''}}">
                                <a href="#tab_6_2" data-toggle="tab" aria-expanded="false"> Berita Acara Peninjauan Lapangan </a>
                            </li>
                            @if($recProposalByID->noper=='9') 
                            <li class="{{($tab=='rekomendasi')?'active':''}}">
                                <a href="#tab_6_3" data-toggle="tab"> Rekomendasi Pengusulan</a>
                            </li>
                           @endif 

                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <div class="tab-content">
                            {{-- Kelengkapan Administrasi --}}
                            @include('admin.skpd.cetak.kelengkapan-administrasi')

                            {{-- Berita Acara Peninjauan Lapangan --}}
                            @include('admin.skpd.cetak.peninjauan-lapangan')

                            {{-- Bentuk Rekomendasi --}}
                            @if($recProposalByID->noper=='9')
                            @include('admin.skpd.cetak.rekomendasi-pengusulan') 
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
    <script>
        $(".btn-pdf-administrasi").on("submit", function(){
            $("#btn-simpan-administrasi").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Kelengkapan Administrasi. ");
        });

        $(".btn-pdf-lapangan").on("submit", function(){
            $("#btn-simpan-lapangan").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Peninjauan Lapangan. ");
        });

        $(".btn-pdf-rekomendasi").on("submit", function(){
            $("#btn-simpan-rekomendasi").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Surat Rekomendasi. ");
        });

        $(".btn-pdf-rekomendasi-pencairan").on("submit", function(){
            $("#btn-simpan-rekomendasi-pencairan").hide();
            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui mencetak Surat Rekomendasi Pencairan. ");
        });
    </script>
    <script type="text/javascript">
        $(function(){
            //when the Add Field button is clicked
            $("#add").click(function (e) {
                //Append a new row of code to the "#items" div
                $("#items").append('<tr><td><input type="number" name="no_urut[]" placeholder="No Urut" style="width:100%" required></td><td><input type="text" name="nama[]" placeholder="Masukan Nama" style="width:100%" required></td><td><input type="text" name="jabatan[]" placeholder="Masukan Jabatan" style="width:100%" required></td><td></td><td></td></tr>');
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            //when the Add Field button is clicked
            $("#add-peserta-peninjauan").click(function (e) {
                //Append a new row of code to the "#items" div
                $("#items-peninjauan").append('<tr><td><input type="number" name="no_urut[]" placeholder="No Urut" style="width:100%" required></td><td><input type="text" name="nama[]" placeholder="Masukan Nama" style="width:100%" required></td><td><input type="text" name="jabatan[]" placeholder="Masukan Jabatan" style="width:100%" required></td><td></td><td></td></tr>');
            });
        });
    </script>
@stop