<div class="tab-pane fade {{($tab=='rekomendasi')?'active':''}} in" id="tab_6_3">
   <form action="{{ route('skpd.cetak.updateRekomendasi') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="fase" value="{{$fase}}">
    <input type="hidden" name="tab" value="rekomendasi">
    <center>
        <h4>SURAT REKOMENDASI PENGUSULAN</h4>
        <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
    </center>
    <br><br>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td><input type="text" name="nomor" value="{{$recProposalByID->no_rekomendasi}}" readonly="" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td>Sifat</td>
                        <td>:</td>
                        <td><input type="text" name="sifat" value="{{$dataCrekomendasi['sifat']}}" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td>:</td>
                        <td><input type="text" name="lampiran" value="{{$dataCrekomendasi['lampiran']}}" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">Hal</td>
                        <td style="vertical-align: top;">:</td>
                        <td>Rekomendasi Pengusulan<br> Permohonan Hibah/Bantuan<br>
                        Sosial/Bantuan Keuangan</td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <center>
                            <select name="tanggal1">
                                @for ($i = 1; $i <= 31; $i++)
                                    <option value="{{$i}}" {{($dataCrekomendasi['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <select name="bulan1">
                                <option value="Januari" {{($dataCrekomendasi['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                <option value="Februari" {{($dataCrekomendasi['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                <option value="Maret" {{($dataCrekomendasi['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                <option value="April" {{($dataCrekomendasi['bulan1']=="April")?'selected':''}}>April</option>
                                <option value="Mei" {{($dataCrekomendasi['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                <option value="Juni" {{($dataCrekomendasi['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                <option value="Juli" {{($dataCrekomendasi['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                <option value="Agustus" {{($dataCrekomendasi['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                <option value="September" {{($dataCrekomendasi['bulan1']=="September")?'selected':''}}>September</option>
                                <option value="Oktober" {{($dataCrekomendasi['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                <option value="November" {{($dataCrekomendasi['bulan1']=="November")?'selected':''}}>November</option>
                                <option value="Desember" {{($dataCrekomendasi['bulan1']=="Desember")?'selected':''}}>Desember</option>
                            </select>
                            <select name="tahun1">
                                @for ($i = 2016; $i <= 2020; $i++)
                                    <option value="{{$i}}" {{($dataCrekomendasi['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>  
                            </center> 
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;"><center>Kepada</center></td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">Yth. Gubernur Provinsi DKI Jakarta melalui Tim Anggaran Pemerintah Daerah</td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">di Jakarta</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    <p style="padding: 30px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan pemohon Hibah/Bantuan Sosial/Bantuan Keuangan yang di ajukan kepada Gubernur sesuai surat Nomor <input type="text" name="nomor2" value="{{$dataCrekomendasi['nomor2']}}"> tanggal 
    <select name="tanggal2">
        @for ($i = 1; $i <= 31; $i++)
            <option value="{{$i}}" {{($dataCrekomendasi['tanggal2']==$i)?'selected':''}}>{{$i}}</option>
        @endfor
    </select>
    <select name="bulan2">
        <option value="Januari" {{($dataCrekomendasi['bulan2']=="Januari")?'selected':''}}>Januari</option>
        <option value="Februari" {{($dataCrekomendasi['bulan2']=="Februari")?'selected':''}}>Februari</option>
        <option value="Maret" {{($dataCrekomendasi['bulan2']=="Maret")?'selected':''}}>Maret</option>
        <option value="April" {{($dataCrekomendasi['bulan2']=="April")?'selected':''}}>April</option>
        <option value="Mei" {{($dataCrekomendasi['bulan2']=="Mei")?'selected':''}}>Mei</option>
        <option value="Juni" {{($dataCrekomendasi['bulan2']=="Juni")?'selected':''}}>Juni</option>
        <option value="Juli" {{($dataCrekomendasi['bulan2']=="Juli")?'selected':''}}>Juli</option>
        <option value="Agustus" {{($dataCrekomendasi['bulan2']=="Agustus")?'selected':''}}>Agustus</option>
        <option value="September" {{($dataCrekomendasi['bulan2']=="September")?'selected':''}}>September</option>
        <option value="Oktober" {{($dataCrekomendasi['bulan2']=="Oktober")?'selected':''}}>Oktober</option>
        <option value="November" {{($dataCrekomendasi['bulan2']=="November")?'selected':''}}>November</option>
        <option value="Desember" {{($dataCrekomendasi['bulan2']=="Desember")?'selected':''}}>Desember</option>
    </select>
    <select name="tahun2">
        @for ($i = 2016; $i <= 2020; $i++)
            <option value="{{$i}}" {{($dataCrekomendasi['tahun2']==$i)?'selected':''}}>{{$i}}</option>
        @endfor
    </select>
    hal <input type="text" name="hal" value="{{$dataCrekomendasi['hal']}}"> dan setelah dilakukan penelitian administrasi dan peninjauan lapangan, dengan ini direkomendasikan kepada : 
    <table width="100%">
        <tr>
            <td width="190" style="padding-left: 30px;">Nama</td>
            <td>:</td>
            <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
        </tr>
        <tr>
            <td width="190" style="padding-left: 30px;">Alamat</td>
            <td>:</td>
            <td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
        </tr>
        <tr>
            <td width="190" style="padding-left: 30px;">Sebesar</td>
            <td>:</td>
            <td>Rp. {{ number_format($recProposalByID->nominal_rekomendasi,2,',','.') }}</td>
        </tr>
    </table>
    <br>
    <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk dapat diproses lebih lanjut sesuai ketentuan peraturan perundang-undangan.</p>
    <table width="100%">
        <tr>
            <td width="30%">
                <center>
                    {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                    <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                </center>
            </td>
            <td width="30%"></td>
            <td><center>
                <h4>KEPALA SKPD/UKPD</h4>
                <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
                <br><br>
                <h4><input type="text" name="nama_kepala" value="{{$dataCrekomendasi['nama_kepala']}}"></h4>
                <h4>NIP <input type="text" name="nip" value="{{$dataCrekomendasi['nip']}}"></h4>
                </center>
            </td>
        </tr>
    </table>
    <p>
        Tembusan : 
        <ol>
            <li>Sekretaris Daerah Provinsi DKI Jakarta</li>
            <li>Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta</li>
            <li>Kepala Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta</li>
        </ol>
    </p>
    <hr>
    {{-- @if($dataCrekomendasi['is_generate']==0) --}}
      @if($recProposalByID->noper=='10')
     <button type="submit" class="btn green" id="btn-simpan-rekomendasi">Simpan</button>  
        @endif
    {{-- @endif --}}
</form>

<form class="btn-pdf-rekomendasi" action="{{ route('skpd.cetak.to.pdf.rekomendasi') }}" method="POST" style="float: right;">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="fase" value="{{ $fase }}">
    <input type="hidden" name="tab" value="{{ $tab }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    @if($dataCrekomendasi['id'] != "")
    <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
    @endif
</form>
</div>