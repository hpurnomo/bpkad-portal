@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Cetak</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{dd($recProposalByID)}} --}}
        <a href="{{route('skpd.cetak.show',['no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tab'=>$tab])}}" class="btn btn-default" style="float: right;">Kembali</a><br><br>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-print" aria-hidden="true"></i> Ubah Rekomendasi Pencairan </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
						<form action="{{ route('skpd.cetak.updateRekomendasiPencairan') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $id }}">
                            <input type="hidden" name="no_prop" value="{{ $no_prop }}">
						    <input type="hidden" name="uuid_prop" value="{{ $uuid_prop }}">
						    <input type="hidden" name="fase" value="{{ $fase }}">
						    <input type="hidden" name="tab" value="{{ $tab }}">
                            <center>
                                <h4>SURAT REKOMENDASI PENCAIRAN</h4>
                                <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
                            </center>
                            <br><br>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="50%">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>Judul</td>
                                                <td>:</td>
                                                <td><input type="text" name="judul" value="{{$dataCrekomendasiPencairan['judul']}}" required="" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor</td>
                                                <td>:</td>
                                                <td><input type="text" name="nomor" value="{{$dataCrekomendasiPencairan['nomor']}}" required="" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td>Sifat</td>
                                                <td>:</td>
                                                <td><input type="text" name="sifat" value="{{$dataCrekomendasiPencairan['sifat']}}" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td>Lampiran</td>
                                                <td>:</td>
                                                <td><input type="text" name="lampiran" value="{{$dataCrekomendasiPencairan['lampiran']}}" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top">Hal</td>
                                                <td style="vertical-align: top;">:</td>
                                                <td>Rekomendasi Pencairan<br> Hibah/Bantuan Sosial/Bantuan Keuangan )*</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <center>
                                                    <select name="tanggal1">
                                                        @for ($i = 1; $i <= 31; $i++)
                                                            <option value="{{$i}}" {{($dataCrekomendasiPencairan['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                    <select name="bulan1">
                                                        <option value="Januari" {{($dataCrekomendasiPencairan['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                                        <option value="Februari" {{($dataCrekomendasiPencairan['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                                        <option value="Maret" {{($dataCrekomendasiPencairan['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                                        <option value="April" {{($dataCrekomendasiPencairan['bulan1']=="April")?'selected':''}}>April</option>
                                                        <option value="Mei" {{($dataCrekomendasiPencairan['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                                        <option value="Juni" {{($dataCrekomendasiPencairan['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                                        <option value="Juli" {{($dataCrekomendasiPencairan['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                                        <option value="Agustus" {{($dataCrekomendasiPencairan['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                                        <option value="September" {{($dataCrekomendasiPencairan['bulan1']=="September")?'selected':''}}>September</option>
                                                        <option value="Oktober" {{($dataCrekomendasiPencairan['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                                        <option value="November" {{($dataCrekomendasiPencairan['bulan1']=="November")?'selected':''}}>November</option>
                                                        <option value="Desember" {{($dataCrekomendasiPencairan['bulan1']=="Desember")?'selected':''}}>Desember</option>
                                                    </select>
                                                    <select name="tahun1">
                                                        @for ($i = 2016; $i <= 2020; $i++)
                                                            <option value="{{$i}}" {{($dataCrekomendasiPencairan['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                                        @endfor
                                                    </select>  
                                                    </center> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 15px;"><center>Kepada</center></td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 15px;">Yth. Kepala Badan Pengelola Keuangan Daerah selaku Pejabat Pengelola Keuangan Daerah (PPKD)</td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 15px;">di <br>&nbsp;&nbsp;&nbsp;&nbsp;Jakarta</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            
                            <p style="padding: 30px 30px 0 30px;text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan  pencairan hibah/bantuan sosial/bantuan keuangan dari <b><i>{{ucwords($recProposalByID->nm_lembaga)}}</i></b> yang diajukan kepada Gubernur melalui Kepala BPKD selaku Pejabat Pengelola Keuangan Daerah (PPKD) sesuai surat Nomor <input type="text" name="nomor2" value="{{$dataCrekomendasiPencairan['nomor2']}}"> tanggal 
                            <select name="tanggal2">
                                @for ($i = 1; $i <= 31; $i++)
                                    <option value="{{$i}}" {{($dataCrekomendasiPencairan['tanggal2']==$i)?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <select name="bulan2">
                                <option value="Januari" {{($dataCrekomendasiPencairan['bulan2']=="Januari")?'selected':''}}>Januari</option>
                                <option value="Februari" {{($dataCrekomendasiPencairan['bulan2']=="Februari")?'selected':''}}>Februari</option>
                                <option value="Maret" {{($dataCrekomendasiPencairan['bulan2']=="Maret")?'selected':''}}>Maret</option>
                                <option value="April" {{($dataCrekomendasiPencairan['bulan2']=="April")?'selected':''}}>April</option>
                                <option value="Mei" {{($dataCrekomendasiPencairan['bulan2']=="Mei")?'selected':''}}>Mei</option>
                                <option value="Juni" {{($dataCrekomendasiPencairan['bulan2']=="Juni")?'selected':''}}>Juni</option>
                                <option value="Juli" {{($dataCrekomendasiPencairan['bulan2']=="Juli")?'selected':''}}>Juli</option>
                                <option value="Agustus" {{($dataCrekomendasiPencairan['bulan2']=="Agustus")?'selected':''}}>Agustus</option>
                                <option value="September" {{($dataCrekomendasiPencairan['bulan2']=="September")?'selected':''}}>September</option>
                                <option value="Oktober" {{($dataCrekomendasiPencairan['bulan2']=="Oktober")?'selected':''}}>Oktober</option>
                                <option value="November" {{($dataCrekomendasiPencairan['bulan2']=="November")?'selected':''}}>November</option>
                                <option value="Desember" {{($dataCrekomendasiPencairan['bulan2']=="Desember")?'selected':''}}>Desember</option>
                            </select>
                            <select name="tahun2">
                                @for ($i = 2016; $i <= 2020; $i++)
                                    <option value="{{$i}}" {{($dataCrekomendasiPencairan['tahun2']==$i)?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            hal <input type="text" name="hal" value="{{$dataCrekomendasiPencairan['hal']}}"> dan setelah dilakukan penelitian kesesuaian antara penerima, alamat penerima dan besaran dalam Rencana Anggaran Biaya Penggunaan hibah/bantuan sosial/bantuan keuangan sesuai daftar nama penerima, alamat penerima dan besaran hibah/bantuan sosial/bantuan keuangan dalam lampiran Keputusan Gubernur Nomor <input type="text" name="nomor_gubernur" value="{{$dataCrekomendasiPencairan['nomor_gubernur']}}"> tentang <input type="text" name="tentang" value="{{$dataCrekomendasiPencairan['tentang']}}"> dengan rincian sebagai berikut :
                            <table width="100%">
                                <tr>
                                    <td width="190" style="padding-left: 30px;">Nama Penerima</td>
                                    <td>:</td>
                                    <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
                                </tr>
                                <tr>
                                    <td width="190" style="padding-left: 30px;">Alamat</td>
                                    <td>:</td>
                                    <td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
                                </tr>
                                <tr>
                                    <td width="190" style="padding-left: 30px;">Sebesar</td>
                                    <td>:</td>
                                    <td>Rp. <input name="nominal_pencairan" type="text" class="input-numeral" value="{{ $dataCrekomendasiPencairan['nominal_pencairan'] }}" style="width: 50%" required="" max="{{ $recProposalByID->nominal_skgub }}"></td>
                                </tr>
                                <tr>
                                    <td width="190" style="padding-left: 30px;"></td>
                                    <td></td>
                                    <td><i style="color: #BD0711;">Rp. {{ number_format($recProposalByID->nominal_skgub,2,',','.') }} (Catatan : Nilai rekomendasi pencairan tidak boleh lebih besar dari nilai SK)</i></td>
                                </tr>
                            </table>
                            {{-- {{ dd($recProposalByID) }} --}}
                            <br>
                            <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa sesuai surat permohonan dari penerima hibah/bantuan sosial/bantuan keuangan direkomendasikan untuk diproses lebih lanjut.</p>
                            <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian surat rekomendasi ini dibuat dengan sebenarnya sesuai ketentuan peraturan perundang-undangan.</p>
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <center>
                                            {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                                            <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                                        </center>
                                    </td>
                                    <td width="30%"></td>
                                    <td><center>
                                        <h4>KEPALA <span style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}} <br>PROVINSI DKI JAKARTA</span></h4>
                                        <br><br><br>
                                        <h4><input type="text" name="nama_kepala" value="{{$dataCrekomendasiPencairan['nama_kepala']}}"></h4>
                                        <h4>NIP <input type="text" name="nip" value="{{ $dataCrekomendasiPencairan['nip'] }}"></h4>
                                        </center>
                                    </td>
                                </tr>
                            </table>
                            <br><br><br>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Tembusan : 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ol>
                                            <li>Sekretaris Daerah Provinsi DKI Jakarta</li>
                                            <li>Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta</li>        
                                        </ol>
                                    </td>
                                </tr>
                                <tr>
                                    <td>)* coret yang tidak perlu</td>
                                </tr>
                            </table>
                            <hr>
                            <div class="row">
	                            <label class="control-label col-md-3">Upload File Tanda Tangan Basah Pencairan Bertahap
	                            <span class="required"> * </span>
	                            </label>
	                            <div class="col-md-9">
	                                <div class="clearfix margin-top-10">
	                                    <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF/JPG/JPEG/PNG dan Max 10MB </small></span>
	                                </div>
	                                <input type="file" name="file_name" class="btn default form-control">
	                               	<a href="{{ url('/') }}/upload_ttd_basah_pencarian/{{ $dataCrekomendasiPencairan['file_name'] }}" target="_blank">{{ $dataCrekomendasiPencairan['file_name'] }}</a>

	                            </div>
                            </div>
                            <hr>
                            <div class="row">
	                            <label class="control-label col-md-3">Status Pencairan
	                            <span class="required"> * </span>
	                            </label>
	                            <div class="col-md-9">
	                                <select name="status_pencairan" class="form-control">
	                                	<option value="Sudah Dicairkan" {{ ($dataCrekomendasiPencairan['status_pencairan']=='Sudah Dicairkan')?'selected':'' }}>Sudah Dicairkan</option>
	                                	<option value="Belum Dicairkan" {{ ($dataCrekomendasiPencairan['status_pencairan']=='Belum Dicairkan')?'selected':'' }}>Belum Dicairkan</option>
	                                	<option value="Batal Dicairkan" {{ ($dataCrekomendasiPencairan['status_pencairan']=='Batal Dicairkan')?'selected':'' }}>Batal Dicairkan</option>
	                                </select>
	                            </div>
                            </div>
                            <hr>
                            <button type="submit" class="btn green" id="btn-simpan-rekomendasi-pencairan">Simpan</button>
                            <a href="{{route('skpd.cetak.show',['no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tab'=>$tab])}}" class="btn btn-default" style="float: right;">Kembali</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
   
            $('input[name="file_name"]').fileuploader({
                limit: 1,
                extensions: ['jpg', 'jpeg', 'png', 'pdf'],
                fileMaxSize: 10
            });
            
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop