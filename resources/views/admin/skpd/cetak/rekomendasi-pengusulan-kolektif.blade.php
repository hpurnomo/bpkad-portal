<div class="tab-pane fade {{($tab=='rekomendasi')?'active':''}} in" id="tab_6_3">
   
   
    <center>
        <h2>SURAT REKOMENDASI PENGUSULAN KOLEKTIF</h2> 
    </center>
    <br><br>
    
    <select id="jenisBantuan">
        <option value="">Pilih Jenis Bantuan</option>
        <option value="Hibah">Hibah</option>
        <option value="Bantuan Sosial">Bantuan Sosial</option>
    </select>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td><input type="text" id="nomor" value=""  style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td>Sifat</td>
                        <td>:</td>
                        <td><input type="text" id="sifat" value="" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td>:</td>
                        <td><input type="text" id="lampiran" value="" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">Hal</td>
                        <td style="vertical-align: top;">:</td>
                        <td>Rekomendasi Pengusulan <div style="display: inline;" class="jenisBantuan"></div>  Dalam Bentuk Uang pada {{ $periode->keterangan }}</td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <center>
                            <select id="tanggal">
                                @for ($i = 1; $i <= 31; $i++)
                                    <option value="{{$i}}"  >{{$i}}</option>
                                @endfor
                            </select>
                            <select id="bulan">
                                <option value="Januari" >Januari</option>
                                <option value="Februari"  }>Februari</option>
                                <option value="Maret"  >Maret</option>
                                <option value="April" >April</option>
                                <option value="Mei"  >Mei</option>
                                <option value="Juni" >Juni</option>
                                <option value="Juli"  >Juli</option>
                                <option value="Agustus" >Agustus</option>
                                <option value="September" >September</option>
                                <option value="Oktober" >Oktober</option>
                                <option value="November" >November</option>
                                <option value="Desember">Desember</option>
                            </select>
                            <select id="tahun">
                                @for ($i = 2016; $i <= 2020; $i++)
                                    <option value="{{$i}}"  >{{$i}}</option>
                                @endfor
                            </select>  
                            </center> 
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;"><center>Kepada</center></td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">Yth. Gubernur Provinsi DKI Jakarta melalui Tim Anggaran Pemerintah Daerah</td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">di Jakarta</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    <div style="padding: 100px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Sehubungan dengan permohonan pemohon <div class="jenisBantuan" style="display: inline;"></div>  yang diajukan kepada Gubernur setelah dilakukan penelitian administrasi dan peninjauan lapangan, dengan ini direkomendasikan kepada penerima <div class="jenisBantuan" style="display: inline;"></div>  sebagaimana rincian terlampir. </div> 
    
   <div style="padding: 100px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Untuk dapat diproses lebih lanjut sesuai ketentuan peraturan perundang-undangan.</div>
    
    
<div style="padding: 100px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat rekomendasi ini dibuat dengan sebenar-benarnya dan saya bertanggung jawab penuh atas keabsahan rekomendasi ini.</div>

      <table style=" margin-left: 65%;"> 
            
            <tr>
                <td><center>
                <input type="text"  id="plt" value="" style="display: inline;width: 50px;"> KEPALA {{ $skpd->nm_skpd }}   
                
                <br><br>
                <h4><input type="text" id="nama_kepala" value="{{ $skpd->kontak_person }}" placeholder="Nama" ></h4>
                <h4>NIP <input type="text" id="nip" value="{{ $skpd->nip_ketua }}" ></h4>
                <!-- <input type="nama_skpd" name="{{ $skpd->nm_skpd }}"> -->
                </center> 
            </td>
        </tr>
        </table>

    <table style="width: 50%">
        <tr>
            <td>Tembusan : </td>
        </tr>
        <tr>
            <td> 1. Sekretaris Daerah Provinsi DKI Jakarta </td>
        </tr>
        <tr>
            <td> 2. Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta </td>
        </tr>
        <tr>
            <td> 3. Kepala Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta  </td>
        </tr>
    </table>
    <br>
    <legend></legend><br>
    <h4>Pilih Lembaga </h4>
     <table class="table table-striped table-bordered table-hover table-common">
        <thead>
            <tr>
                <th> </th> 
                <th> Nomor </th> 
                <th> Nama Lembaga </th> 
                <th> Judul Proposal </th> 
                <th> Kode Lembaga </th> 
                <th> Tipe Lembaga </th> 
                <th> Nama SKPD </th> 
            </tr>
        </thead> 
    </table>
    <button id="btn_pilih" value="Simpan" class="btn btn-large btn-primary" type="submit"> SIMPAN & DOWNLOAD REKOMENDASI </button> 

     <!-- Modal ===============================--> 
      <div class="modal fade text-left" id="confirm-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header ">
          <h3 class="modal-title" id="myModalLabel35"> Konfirmasi </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          </div>
          <form>
          <div class="modal-body">
             <input type="hidden" id="tipe">
             <input type="hidden" id="table_id">
             <h5 class="mt-1" id="confirm-message"></h5>
          </div>
          <div class="modal-footer">
              <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal">
                  <i class="ft-x"></i> Batal
              </a>
              <a href="javascript:void(0)" class="btn  btn-primary" id="btn_save">
                  <i class="ft-trash"></i> MENGERTI & LANJUTKAN CETAK REKOMENDASI
              </a>
          </div>
          </form>
        </div>
        </div>
      </div> 
      <!-- End of modal --> 

 @section('javascript')  
     @parent 

       <script type="text/javascript">
         

            $(document).ready(function(){

               
              

                var table = $('.table-common').DataTable();

                
                $("#jenisBantuan").change(function(){
                    var jenisBantuan = $(this).val();
                    $(".jenisBantuan").text(jenisBantuan);
                      // table.destroy(); 
                      table = $('.table-common').DataTable({
                          serverSide: true,
                          processing: true, 
                          destroy:true, 
                          remove:true, 
                          lengthMenu: [[15, 30, 50, 100, 150, 200, -1], [15, 30, 50,100,150,200, "All"]], 
                          ajax: {
                              headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                              },
                              // url:"{{ URL::to('skpd/lembaga/get-data') }}", 
                              url:"http://ehibahbansosdki.jakarta.go.id/skpd/lembaga/get-data", 
                              data:{noper:"{{ $noper }}",jenisBantuan:jenisBantuan},
                              type: "POST"
                          },
                          columns: [
                            {!! $column !!}
                          ], 
                          columnDefs: [
                            {!! $columnConf !!}
                          ],     
                          buttons: [
                              'selectAll', 'selectNone'
                          ],
                          dom: 'lBfrtip', 
                          language: {
                              "buttons": {
                                  "selectAll" : "Select All",
                                  "selectNone": "Deselect All"
                              }
                          },
                          select: {
                              'style':    'multi',
                              'selector': 'td:first-child'
                          } 
                    });

                    $("#btn_pilih").click(function(e){
                       e.preventDefault(); 
                       var nomor        = $("#nomor").val();
                       var sifat        = $("#sifat").val();
                       var lampiran     = $("#lampiran").val();
                       var jenisBantuan = $("#jenisBantuan").val();
                       var tahun        = $("#tahun").val();
                       var bulan        = $("#bulan").val();
                       var tanggal      = $("#tanggal").val();
                       var nama_kepala  = $("#nama_kepala").val();
                       var nip          = $("#nip").val();
                       var jabatan      = $("#jabatan").val();
                       var plt      = $("#plt").val();
                       var checkedTable = table.rows('.selected').data().length;

                        if(nomor == "" || sifat =="" || lampiran =="" || jenisBantuan =="" || tahun == "" || bulan == "" ||tanggal == "" || nama_kepala == "" ||  nip == ""  ||  jabatan == "" ){
                           $("#confirm-message").html("Mohon lengkapi data form input"); 
                           $("#btn_save").hide();
                           $("#confirm-popup").modal('show');
                        }
                        else if(checkedTable > 0){ 
                          $("#confirm-message").html("<p>PERHATIAN : Anda akan menyimpan dan mendownload daftar lembaga yang akan anda berikan rekomendasi secara kolektif sesuai pergub 142 Tahun 2018. <b style='color:red;'>Lembaga yang sudah dipilih akan terkunci tidak akan dapat dipilih kembali (rekomendasi ulang)</b>. Pastikan anda sudah mengkoreksi RAB lembaga tersebut dengan detail dan mengecek nominal rekomendasinya dengan seksama<p>");
                          $("#btn_save").show();
                          $("#confirm-popup").modal('show');
                        }
                        else{
                           $("#confirm-message").html("Mohon pilih lembaga yang akan diinput"); 
                           $("#btn_save").hide();
                           $("#confirm-popup").modal('show');
                        } 

                    });


                $("#btn_save").unbind('click').bind('click',function(e){ 
                    e.preventDefault();
                    e.stopPropagation();

                    $("#confirm-popup").modal('hide');
                    $("#confirm-message").html("Proses menyimpan ... "); 
                    $(".modal-footer").hide();
                    $("#confirm-popup").modal('show');

                    var checkedTable = table.rows('.selected').data().length;
                    if(checkedTable > 0){ 
                      $("#confirm-message").html("Apakah Anda yakin degan lembaga yang dipilih?");
                      $("#confirm-popup").modal('show');
                    }
                    else{
                       $("#confirm-message").html("Mohon pilih lembaga yang akan diinput"); 
                       $("#confirm-popup").modal('show');
                    } 

                    var checkedTable    = table.rows('.selected').data(); 
                    var table_row = getRowCheckedTable(checkedTable);
 
                    $.ajax({
                      async: true,
                      type: "POST",
                      // url: "{{ route('skpd.proposal.set.rekomendasi.kolektif') }}",
                      // url: "{{ URL::to('skpd/proposal/set/rekomendasi/kolektif') }}",
                      url: "http://ehibahbansosdki.jakarta.go.id/skpd/proposal/set/rekomendasi/kolektif",
                      dataType: "json",
                      data: {

                             _token: "{{ csrf_token() }}",
                             lembaga:table_row , 
                             nomor:$("#nomor").val(),
                             sifat:$("#sifat").val(),
                             lampiran:$("#lampiran").val(),
                             jenisBantuan:$("#jenisBantuan").val(),
                             tahun:$("#tahun").val(),
                             bulan:$("#bulan").val(),
                             tanggal:$("#tanggal").val(),
                             nama_kepala:$("#nama_kepala").val(),
                             nip:$("#nip").val(),
                             jabatan:$("#jabatan").val(),
                             plt:$("#plt").val(),
                             noper:"{{ $noper }}"
                         
                         },
                      success : function(res){
                        if(res.status == true){
                          $("#confirm-popup").modal('hide'); 
                          Command: toastr["success"]("Sukeses", "Rekomendasi Kolektif sudah disimpan") 
                          toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          }
  
                          // setTimeout(function(){ window.location = "{{ url('skpd/proposal/surat/rekomendasi/kolektif/') }}/{{ $noper }}"; }, 2000);
// 
                          setTimeout(function(){ window.location = "http://ehibahbansosdki.jakarta.go.id/skpd/proposal/surat/rekomendasi/kolektif/{{ $noper }}"; }, 2000);

                           // setTimeout(function(){ 
                           //  window.location = "{{ url('skpd/proposal/surat/rekomendasi/kolektif/cetak') }}/"+ res.lastId; }, 2000);
                        }
                        else{
                          $("#confirm-popup").modal('hide');
                            Command: toastr["error"]("Error", "Rekomendasi Kolektif gagal disimpan") 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
   
                            // setTimeout(function(){ window.location = "{{ route('skpd.proposal.surat.rekomendasi.kolektif.create',['noper'=>$noper]) }}"; }, 2000);
                        }
                      },
                      error : function(err, ajaxOptions, thrownError){
                           $("#confirm-popup").modal('hide');
                            Command: toastr["error"]("Error", "Rekomendasi Kolektif gagal disimpan") 
                            toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            }
   
                            // setTimeout(function(){ window.location = "{{ route('skpd.proposal.surat.rekomendasi.kolektif.create',['noper'=>$noper]) }}"; }, 2000);
                      }
                    });  
                
              });

               
                });


            function getRowCheckedTable(data){
              var res = []; 
                $.each(data, function(idx, value){
                    res[idx] = {
                      table_id : value.nomor
                    };
                }); 
              return res; 
            } 

            }); 
        </script>
@stop
</div>