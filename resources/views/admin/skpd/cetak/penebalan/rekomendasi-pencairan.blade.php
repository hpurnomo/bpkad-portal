@if ($tahap=='apbd')
<div class="tab-pane fade {{($tab=='pencairan')?'active':''}} in" id="tab_6_4">
    <h4>
        <b>Nominal (SK Gub/APBD) Yang Disepakati Saat Ini Sebesar : Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</b>
        <br><br>
        <b><b>Sisa Dana Saat Ini Sebesar : Rp. {{ number_format($recProposalByID->nominal_apbd-$totalNominalPencairan,2,',','.') }}</b></b>
    </h4>
    <hr>
    <div class="btn-group">
        <a href="{{ route('skpd.cetak.penebalan.tambahRekomendasiPencairan',['no_prop'=>base64_encode($recProposalByID->no_prop),'uuid'=>$recProposalByID->uuid,'fase'=>$fase,'tahap'=>$tahap,'tab'=>'pencairan']) }}" class="btn sbold green"> Buat Rekomendasi Pencairan Baru
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <br><br>
    {{-- {{ dd($recCetakRekomendasiPencairanByNoProp) }} --}}
    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="example">
        <thead>
            <tr>
                <th> No. </th>
                <th> Judul </th>
                <th> Nominal Pencairan (Rp.) </th>
                <th> Bukti </th>
                <th> Status </th>
                <th> Action </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($recCetakRekomendasiPencairanByNoProp as $key=>$value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td><a href="{{ route('skpd.cetak.penebalan.editRekomendasiPencairan',['no_prop'=>base64_encode($recProposalByID->no_prop),'uuid'=>$recProposalByID->uuid,'fase'=>$fase,'tahap'=>$tahap,'tab'=>'pencairan','id'=>$value->id]) }}">{{ $value->judul }}</a></td>
                    <td>{{ number_format($value->nominal_pencairan,2,',','.') }}</td>
                    <td><a href="{{ url('/') }}/upload_ttd_basah_pencarian/{{ $value->file_name }}" target="_blank">Lihat Bukti</a></td>
                    <td>{{ $value->status_pencairan }}</td>
                    <td>
                        <form class="btn-pdf-rekomendasi-pencairan" action="{{ route('skpd.cetak.to.pdf.rekomendasi.pencairan') }}" method="POST" style="float: right;">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="id" value="{{ $value->id }}">
                            <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                            <input type="hidden" name="fase" value="{{ $fase }}">
                            <input type="hidden" name="tab" value="{{ $tab }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif