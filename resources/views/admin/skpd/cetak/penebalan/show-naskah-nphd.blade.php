<div class="tab-pane fade {{($tab=='nphd')?'active':''}} in" id="tab_6_5">            
    <div class="row">
        <div class="col-md-12">
            @if($dataCNPHD['id'])
            <a href="{{ route('skpd.cetak.penebalan.naskah.perjanjian.nphd',['uuid'=>$uuid,'fase'=>$fase,'tahap'=>$tahap]) }}" class="btn red confirmLink"><i class="fa fa-download" aria-hidden="true"></i> Unduh Naskah Perjanjian NPHD</a>
            <a href="{{ route('skpd.cetak.penebalan.naskah.perjanjian.nphd.bukafituredit',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>$tahap,'tab'=>'nphd']) }}" class="btn green" style="float: right;"><i class="fa fa-unlock" aria-hidden="true"></i> Buka Laporan Ini Agar Dapat Di Ubah Oleh Lembaga</a>
            @endif
        </div>
    </div>
    <hr>
    <form action="" method="post">
        <center>
            <h4>NASKAH PERJANJIAN HIBAH DAERAH DALAM BENTUK UANG <br><br> PERJANJIAN <br><br> 
                ANTARA <br><br>
                PEMERINTAH PROVINSI DAERAH KHUSUS <br><br> 
                IBUKOTA JAKARTA <br><br>
                DAN <br><br>
                {{ strtoupper($recProposalByID->nm_lembaga) }} <br><br> 
                TENTANG <br><br>
                PEMBERIAN HIBAH DALAM BENTUK UANG
            </h4>
        </center>
        <p style="padding: 30px 0 0 0;">
        Pada hari ini ...................................
        <select name="hari1" style="display: none;">
            <option value="Senin" {{($dataCNPHD['hari1']=='Senin')?'selected':''}}>Senin</option>
            <option value="Selasa" {{($dataCNPHD['hari1']=='Selasa')?'selected':''}}>Selasa</option>
            <option value="Rabu" {{($dataCNPHD['hari1']=='Rabu')?'selected':''}}>Rabu</option>
            <option value="Kamis" {{($dataCNPHD['hari1']=='Kamis')?'selected':''}}>Kamis</option>
            <option value="Jumat" {{($dataCNPHD['hari1']=='Jumat')?'selected':''}}>Jumat</option>
        </select>
        tanggal .........
        <select name="tanggal1" style="display: none;">
            @for ($i = 1; $i <= 31; $i++)
                <option value="{{$i}}" {{($dataCNPHD['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
            @endfor
        </select>
        bulan ............................................
        <select name="bulan1" style="display: none;">
            <option value="Januari" {{($dataCNPHD['bulan1']=="Januari")?'selected':''}}>Januari</option>
            <option value="Februari" {{($dataCNPHD['bulan1']=="Februari")?'selected':''}}>Februari</option>
            <option value="Maret" {{($dataCNPHD['bulan1']=="Maret")?'selected':''}}>Maret</option>
            <option value="April" {{($dataCNPHD['bulan1']=="April")?'selected':''}}>April</option>
            <option value="Mei" {{($dataCNPHD['bulan1']=="Mei")?'selected':''}}>Mei</option>
            <option value="Juni" {{($dataCNPHD['bulan1']=="Juni")?'selected':''}}>Juni</option>
            <option value="Juli" {{($dataCNPHD['bulan1']=="Juli")?'selected':''}}>Juli</option>
            <option value="Agustus" {{($dataCNPHD['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
            <option value="September" {{($dataCNPHD['bulan1']=="September")?'selected':''}}>September</option>
            <option value="Oktober" {{($dataCNPHD['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
            <option value="November" {{($dataCNPHD['bulan1']=="November")?'selected':''}}>November</option>
            <option value="Desember" {{($dataCNPHD['bulan1']=="Desember")?'selected':''}}>Desember</option>
        </select>
        tahun .................
        <select name="tahun1" style="display: none;">
            @for ($i = 2016; $i <= 2020; $i++)
                <option value="{{$i}}" {{($dataCNPHD['tahun1']==$i)?'selected':''}}>{{$i}}</option>
            @endfor
        </select> 
        yang bertanda tangan di bawah ini: 
        
        <table width="100%">
            <tr>
                <td>I.</td>
                <td style="padding: 0 0 0 10px;">Nama : {{$dataCNPHD['nama1']}}</td>
            </tr>
            <tr>
                <td></td>
                <td style="padding: 0 0 0 10px;">Dalam Jabatan sebagai {{$dataCNPHD['ketPLT']}} Kepala {{ $recProposalByID->nm_skpd }} Pemberi Rekomendasi dengan alamat {{ $recProposalByID->alamatSKPD }};<br>
                        oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah Provinsi DKI Jakarta, untuk selanjutnya disebut PIHAK PERTAMA.</td>
            </tr>
            <tr>
                <td>II.</td>
                <td style="padding: 0 0 0 10px;">Nama : {{$dataCNPHD['nama2']}}</td>
            </tr>
            <tr>
                <td></td>
                <td style="padding: 0 0 0 10px;">Jabatan {{$dataCNPHD['jabatan1']}} berkantor di. {{$dataCNPHD['alamat']}} dalam hal ini menjalani jabatannya sesuai {{$dataCNPHD['deskripsi_jabatan']}}, 
		                oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah/Pemerintah Daerah/Kelompok Masyarakat/Organisasi Kemasyarakatan untuk selanjutnya disebut PIHAK KEDUA.</td>
            </tr>
        </table>

        <p style="padding: 10px 0 0 10px;">Bahwa masing-masing pihak bertindak dalam jabatannya sebagaimana tersebut di atas, secara bersama-sama disebut PARA PIHAK dengan terlebih dahulu memperhatikan ketentuan sebagai berikut :</p>
        <table width="100%">
            <tr>
        		<td style="vertical-align: top;">1.</td>
        		<td style="padding: 0 0 0 10px;">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Undang-Undang Nomor 9 Tahun 2015;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">2.</td>
        		<td style="padding: 0 0 0 10px;">Undang-Undang Nomor 29 Tahun 2007 tentang Pemerintahan Provinsi Daerah Khusus Ibukota Jakarta sebagai Ibukota Negara Kesatuan Republik Indonesia;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">3.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan Pemerintah Nomor 58 Tahun 2005 tentang Pengelolaan Keuangan Daerah;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">4.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan Menteri Dalam Negeri Nomor 13 Tahun 2006 tentang Pedoman Pengelolaan Keuangan Daerah sebagaimana telah diubah dengan Peraturan Menteri Dalam Negeri Nomor 21 Tahun 2011;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">5.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan Daerah Nomor 5 Tahun 2007 tentang Pokok-pokok Pengelolaan Keuangan Daerah;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">6.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan  Daerah Nomor 5 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Provinsi Daerah Khusus Ibukota Jakarta;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">7.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan {{$dataCNPHD['peraturan4']}} Nomor {{$dataCNPHD['nomor4']}} Tahun {{ $dataCNPHD['tahun4'] }} tentang {{$dataCNPHD['tentang4']}}</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">8.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan Gubernur Nomor 142 Tahun 2013 tentang Sistem dan Prosedur Pengelolaan Keuangan. Daerah sebagaimana telah di-ubah dengan Peraturan Gubernur Nomor 161 Tahun 2014;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">9.</td>
        		<td style="padding: 0 0 0 10px;">Peraturan Gubernur Nomor 142 tahun 2018 tentang Pedoman Pemberian Hibah dan Bantuan Sosial yang Bersumber dari Anggaran Pendapatan dan Belanja Daerah sebagaimana telah beberapa kali diubah terakhir dengan Peraturan Gubernur Nomor 20 tahun 2020;</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">10.</td>
        		<td style="padding: 0 0 0 10px;">Keputusan Gubernur Nomor {{$dataCNPHD['nomor6']}} Tahun
        {{ $dataCNPHD['tahun6'] }} tentang {{ $dataCNPHD['tentang6'] }};</td>
        	</tr>
        </table>
        
        <p style="padding: 30px 0 0 0;">Bahwa berdasarkan hal tersebut dan sesuai dengan rekomendasi {{$dataCNPHD['namaSKPD']}} Nomor {{$dataCNPHD['nomor7']}} tanggal {{ $dataCNPHD['tanggal7'] }} bulan {{ $dataCNPHD['bulan7'] }} tahun {{ $dataCNPHD['tahun7'] }},
                PARA PIHAK sepakat untuk melakukan Perjanjian Hibah dalam bentuk uang, dengan syarat dan ketentuan sebagai berikut :</p>
        
        <center>
        	Pasal 1
            <br><br>
            JUMLAH DAN TUJUAN HIBAH  
        </center>
        <br>
        <table width="100%">
            <tr>
                <td style="vertical-align: top;">(1)</td>
                <td style="padding: 0 0 0 10px;">PIHAK PERTAMA memberikan Hibah berupa uang melalui transfer dana kepada PIHAK KEDUA sebagaimana PIHAK KEDUA menerima dari PIHAK PERTAMA senilai Rp. {{$dataCNPHD['senilai_angka']}} ( {{$dataCNPHD['senilai_teks']}} rupiah ).</td>
            </tr>
            <tr>
                <td style="vertical-align: top;">(2)</td>
                <td style="padding: 0 0 0 10px;">Hibah sebagaimana dimaksud pada ayat (1) dipergunakan sesuai dengan Rencana Penggunaan Hibah/Proposal yang merupakan bagian yang tidak terpisahkan dari naskah perjanjian hibah daerah ini.</td>
            </tr>
            <tr>
                <td style="vertical-align: top;">(3)</td>
                <td style="padding: 0 0 0 10px;">Penggunaan belanja hibah sebagaimana dimaksud pada ayat (2) bertujuan untuk</td>
            </tr>
            <tr>
                <td style="vertical-align: top;"></td>
                <td>{!! $dataCNPHD['kegiatan2'] !!}</td>
            </tr>
        </table>
        <br>
        <center>
        	Pasal 2
            <br><br>
            PENGGUNAAN
        </center>
        <br>
        <table width="100%">
        	<tr>
        		<td style="vertical-align: top;">(1)</td>
        		<td style="padding: 0 0 0 10px;">PIHAK KEDUA menggunakan belanja hibah berupa uang sebagaimana dimaksud dalam Pasal 1 ayat (1) sesuai dengan Rencana Penggunaan Rencana Penggunaan Hibah / Proposal</td>
        	</tr>
        	<tr>
        		<td style="vertical-align: top;">(2)</td>
        		<td style="padding: 0 0 0 10px;">Belanja Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk :</td>
        	</tr>
        </table>
        <br>
        <table width="90%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding: 0 0 0 10px;">
                    @if($recProposalByID->noper >= 9 )
                    {!!  base64_decode($dataCNPHD['kegiatan1']) !!}
                    @else
                    {!!  $dataCNPHD['kegiatan1'] !!}
                    @endif 
                    <table border="1" cellpadding="1" cellspacing="1" width="100%"> 
                        <tbody> 
                            <tr>
                                <td style="text-align: right;padding: 10px;font-style: italic;"> {!! $dataCNPHD['totalTerbilangKegiatan1'] !!}</td> 
                            </tr> 
                        </tbody>
                    </table>

                </td>
            </tr>
        </table>
         <center>
            Pasal 3
            <br><br>
            HAK DAN KEWAJIBAN PIHAK KEDUA
        </center> 
        <br>
        <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td>PIHAK KEDUA menerima dana Hibah dari PIHAK PERTAMA untuk digunakan sesuai dengan penggunaan sebagaimana dimaksud dalam Pasal 2.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td>Dalam hal PIHAK KEDUA telah menerima Hibah 1 (satu) tahun sebelumnya, pencairan dan penyaluran dana Hibah sebagaimana dimaksud pada ayat (1) akan diperhitungkan sisa dana penggunaan Hibah yang tertuang dalam Laporan Penggunaan Hibah tahun sebelumnya yaitu sebesar Rp. {{ $dataCNPHD['total1'] }} </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td>PIHAK KEDUA wajib menandatangani Pakta Integritas yang menyatakan bahwa Hibah yang diterimakan digunakan sesuai dengan Naskah Perjanjian Hibah daerah ini. </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(4)</td>
                        <td> PIHAK KEDUA selaku penerima Hibah dan objek pemeriksaan, wajib menyimpan bukti-bukti pengeluaran yang lengkap dan sah sesuai peraturan perundang-undangan. </td> 
                    </tr>

                </table>
        <br>
         <center>
                    Pasal 4
                    <br><br>
                    HAK DAN KEWAJIBAN PIHAK PERTAMA
                </center>
                <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td> PIHAK PERTAMA wajib memberikan dana Hibah kepada PIHAK KEDUA melalui transfer ke rekening Bank PIHAK KEDUA apabila seluruh persyaratan dan kelengkapan berkas pengajuan dana Hibah telah dipenuhi oleh PIHAK KEDUA. </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td> Dalam hal PIHAK KEDUA telah menerima Hibah 1 (satu) tahun sebelumnya, pencairan dan penyaluran dana Hibah sebagaimana dimaksud pada ayat (1) dilakukan setelah diperhitungkan sisa dana penggunaan Hibah yang tertuang dalam laporan penggunaan Hibah tahun sebelumnya yaitu sebesar Rp. {{ $dataCNPHD['total2'] }}</td>
                    </tr>

                     <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td>  PIHAK PERTAMA Menunda pencairan belanja Hibah apabila PIHAK KEDUA tidak/ belum memenuhi persyaratan dan kelengkapan berkas pengajuan dana sesuai dengan peraturan perundang-undangan. </td>
                    </tr>
                </table>
        <br>
        <br>
                <center>
                    Pasal 5
                    <br><br>
                    TATA CARA PELAPORAN 
                </center>
                <br>
                  <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td>PIHAK KEDUA membuat dan menyampaikan laporan penggunaan Hibah kepada Gubernur melalui Kepala SKPD/UKPD Pemberi Rekomendasi dengan tembusan Kepala BPKD selaku PPKD paling lambat 1 (satu) bulan berikutnya setelah pelaksanaan kegiatan selesai atau tanggal 10 Maret Tahun Anggaran berikutnya. 
                    </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td>) Laporan penggunaan Hibah sebagaimana dimaksud dilengkapi dengan surat pernyataan tanggung jawab bermeterai cukup dari PIHAK KEDUA yang menyatakan bahwa Hibah yang telah diterima telah dipergunakan sesuai dengan NPHD. </td>
                    </tr>
                </table>
                <br>

                 <center>
                    Pasal 
                    <br><br>
                    AUDIT
                </center>
                <br>
                  <table width="100%">
                    <tr> 
                        <td>Dalam kondisi tertentu penerima Hibah dan Bantuan Sosial dapat dilakukan audit oleh aparat pengawas fungsional. 
                    </td> 
                </table>
                <br>
                 <p>Demikian Perjanjian Hibah ini dibuat dan ditandatangani di Jakarta pada hari dan tanggal tersebut di atas dalam rangkap 2 (dua), masing-masing bermeterai cukup dan mempunyai kekuatan hukum yang sama, 1 (satu) eksemplar untuk PIHAK PERTAMA dan 1 (satu) eksemplar untuk PIHAK KEDUA. </p>
                <br>
        <table width="100%">
            <tr>
                <td width="30%">
                    <center>
                    <h4>PIHAK KEDUA<br>
                    {{ $dataCNPHD['jabatan2'] }}</h4>,
                    <br><br><br>
                    <h4> {{ $dataCNPHD['nama_pihak_kedua'] }} </h4>
                    <h4>NIP/NRP* {{ $dataCNPHD['nip_pihak_kedua'] }} </h4>
                    <br>
                    <em>*) bagi penerima hibah instansi Pemerintah</em>
                    </center>
                </td>
                <td width="30%"></td>
                <td><center>
                    <h4>PIHAK PERTAMA <br> {{ $dataCNPHD['ketPLT'] }} KEPALA {{ $recProposalByID->nm_skpd }}</h4>
                    <br><br><br>
                    <h4> {{ $dataCNPHD['nama_kepala'] }} </h4>
                    <h4>NIP {{ $dataCNPHD['nip_kepala'] }}</h4>
                    </center>
                </td>
            </tr>
        </table>
    </form>
        
</div>