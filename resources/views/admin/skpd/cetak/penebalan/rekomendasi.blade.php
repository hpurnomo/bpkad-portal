<div class="tab-pane fade {{($tab=='rekomendasi')?'active':''}} in" id="tab_6_3">
   <form action="{{ route('skpd.cetak.penebalan.updateRekomendasi') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="uuid" value="{{$uuid}}">
    <input type="hidden" name="fase" value="{{$fase}}">
    <input type="hidden" name="tahap" value="{{$tahap}}">
    <input type="hidden" name="tab" value="rekomendasi">

      <center>
        <h2>SURAT REKOMENDASI PENGUSULAN KOLEKTIF</h2> 
    </center>
    <br><br>
    
    <select name="jenisBantuan" id="jenisBantuan">
        <option value="">Pilih Jenis Bantuan</option>
        <option value="Hibah">Hibah</option>
        <option value="Bantuan Sosial">Bantuan Sosial</option>
    </select>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td><input type="text" name="nomor" value="{{$dataCrekomendasi['nomor']}}"  style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td>Sifat</td>
                        <td>:</td>
                        <td><input type="text" name="sifat" value="{{$dataCrekomendasi['sifat']}}" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td>:</td>
                        <td><input type="text" name="lampiran" value="{{$dataCrekomendasi['lampiran']}}" style="width: 80%"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">Hal</td>
                        <td style="vertical-align: top;">:</td>
                        <td>Rekomendasi Pengusulan <div style="display: inline;" class="jenisBantuan"></div>  Dalam Bentuk Uang pada {{ $periode->keterangan }}</td>
                    </tr>
                </table>
            </td>
           <td width="50%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <center>
                            <select name="tanggal1">
                                @for ($i = 1; $i <= 31; $i++)
                                    <option value="{{$i}}" {{($dataCrekomendasi['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <select name="bulan1">
                                <option value="Januari" {{($dataCrekomendasi['bulan1']=="Januari")?'selected':''}}>Januari</option>
                                <option value="Februari" {{($dataCrekomendasi['bulan1']=="Februari")?'selected':''}}>Februari</option>
                                <option value="Maret" {{($dataCrekomendasi['bulan1']=="Maret")?'selected':''}}>Maret</option>
                                <option value="April" {{($dataCrekomendasi['bulan1']=="April")?'selected':''}}>April</option>
                                <option value="Mei" {{($dataCrekomendasi['bulan1']=="Mei")?'selected':''}}>Mei</option>
                                <option value="Juni" {{($dataCrekomendasi['bulan1']=="Juni")?'selected':''}}>Juni</option>
                                <option value="Juli" {{($dataCrekomendasi['bulan1']=="Juli")?'selected':''}}>Juli</option>
                                <option value="Agustus" {{($dataCrekomendasi['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
                                <option value="September" {{($dataCrekomendasi['bulan1']=="September")?'selected':''}}>September</option>
                                <option value="Oktober" {{($dataCrekomendasi['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
                                <option value="November" {{($dataCrekomendasi['bulan1']=="November")?'selected':''}}>November</option>
                                <option value="Desember" {{($dataCrekomendasi['bulan1']=="Desember")?'selected':''}}>Desember</option>
                            </select>
                            <select name="tahun1">
                                @for ($i = 2016; $i <= 2020; $i++)
                                    <option value="{{$i}}" {{($dataCrekomendasi['tahun1']==$i)?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>  
                            </center> 
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;"><center>Kepada</center></td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">Yth. Gubernur Provinsi DKI Jakarta melalui Tim Anggaran Pemerintah Daerah</td>
                    </tr>
                    <tr>
                        <td style="padding: 15px;">di Jakarta</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    <div style="padding: 100px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Sehubungan dengan permohonan pemohon <div class="jenisBantuan" style="display: inline;"></div>  yang diajukan kepada Gubernur setelah dilakukan penelitian administrasi dan peninjauan lapangan, dengan ini direkomendasikan kepada penerima <div class="jenisBantuan" style="display: inline;"></div>  sebagaimana rincian terlampir. </div> 
    
   <div style="padding: 100px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Untuk dapat diproses lebih lanjut sesuai ketentuan peraturan perundang-undangan.</div>
    
    
<div style="padding: 100px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat rekomendasi ini dibuat dengan sebenar-benarnya dan saya bertanggung jawab penuh atas keabsahan rekomendasi ini.</div>

      <table style=" margin-left: 65%;"> 
            
            <tr>
                <td><center>
                <input type="text"  name="plt" value="{{ $dataCrekomendasi['plt'] }}" style="display: inline;width: 50px;"> KEPALA {{ $skpd->nm_skpd }}   
                
                <br><br>
                <h4><input type="text" name="nama_kepala" value="{{ $skpd->kontak_person }}" placeholder="Nama" ></h4>
                <h4>NIP <input type="text" name="nip" value="{{ $skpd->nip_ketua }}" ></h4>
                <!-- <input type="nama_skpd" name="{{ $skpd->nm_skpd }}"> -->
                </center> 
            </td>
        </tr>
        </table>

    <table style="width: 50%">
        <tr>
            <td>Tembusan : </td>
        </tr>
        <tr>
            <td> 1. Sekretaris Daerah Provinsi DKI Jakarta </td>
        </tr>
        <tr>
            <td> 2. Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta </td>
        </tr>
        <tr>
            <td> 3. Kepala Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta  </td>
        </tr>
    </table>
    <br>
    <legend></legend><br>

    <button type="submit" class="btn green" id="btn-simpan-rekomendasi">Simpan</button>
    {{-- @endif --}}
</form>

<form class="btn-pdf-rekomendasi" action="{{ route('skpd.cetak.penebalan.to.pdf.rekomendasi') }}" method="POST" style="float: right;">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="uuid" value="{{$uuid}}">
    <input type="hidden" name="fase" value="{{$fase}}">
    <input type="hidden" name="tahap" value="{{$tahap}}">
    <input type="hidden" name="tab" value="{{ $tab }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    @if($dataCrekomendasi['id'] != "")
    <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
    @endif
</form>
</div>

 @section('javascript')  
     @parent 
<script type="text/javascript">
    $(document).ready(function(){
         $("#jenisBantuan").change(function(){
        var jenisBantuan = $(this).val();
        $(".jenisBantuan").text(jenisBantuan);
    });

    });
</script>
@stop