@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Cetak {{ strtoupper($tahap) }}</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <a href="{{route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab])}}" class="btn btn-default" style="float: right;">Kembali</a><br><br>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-print" aria-hidden="true"></i> Ubah Rekomendasi Pencairan </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                </div>
            </div>
            {{-- {{ dd($dataCrekomendasiPencairan['judul']) }} --}}
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
						<form action="{{ route('skpd.cetak.updateRekomendasiPencairan') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $id }}">
                            <input type="hidden" name="no_prop" value="{{ $no_prop }}">
						    <input type="hidden" name="uuid" value="{{ $uuid }}">
                            <input type="hidden" name="tahap" value="{{ $tahap }}">
                            <input type="hidden" name="fase" value="{{ $fase }}">
                            <input type="hidden" name="tab" value="{{ $tab }}">
                            <center>
                                <h4>SURAT REKOMENDASI PENCAIRAN</h4>
                                <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
                            </center>
                            <br><br>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="50%">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>Judul <span class="text-danger">*</span></td>
                                                <td>:</td>
                                                <td><input type="text" name="judul" value="{{ $dataCrekomendasiPencairan['judul'] }}" required="" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor <span class="text-danger">*</span></td>
                                                <td>:</td>
                                                <td><input type="text" name="nomor" value="{{ $dataCrekomendasiPencairan['nomor'] }}" required="" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td>Sifat <span class="text-danger">*</span></td>
                                                <td>:</td>
                                                <td><input type="text" name="sifat" value="{{ $dataCrekomendasiPencairan['sifat'] }}" required="" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td>Lampiran <span class="text-danger">*</span></td>
                                                <td>:</td>
                                                <td><input type="text" name="lampiran" value="{{ $dataCrekomendasiPencairan['lampiran'] }}" required="" style="width: 80%"></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top">Hal</td>
                                                <td style="vertical-align: top;">:</td>
                                                <td>Surat Rekomendasi Pencairan<br> 
                                                <select name="text1">
                                                    <option value="Bantuan Sosial" {{ ($dataCrekomendasiPencairan['text1'] == 'Bantuan Sosial')?'selected':'' }}>Bantuan Sosial</option>
                                                    <option value="Hibah" {{ ($dataCrekomendasiPencairan['text1'] == 'Hibah')?'selected':'' }}>Hibah</option>
                                                </select> *)
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <center>
                                                    <select name="tanggal1">
                                                        @for ($i = 1; $i <= 31; $i++)
                                                            <option value="{{$i}}" {{ ($dataCrekomendasiPencairan['tanggal1'] == $i)?'selected':'' }}>{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                    <select name="bulan1">
                                                        <option value="Januari" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Januari')?'selected':'' }}>Januari</option>
                                                        <option value="Februari" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Februari')?'selected':'' }}>Februari</option>
                                                        <option value="Maret" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Maret')?'selected':'' }}>Maret</option>
                                                        <option value="April" {{ ($dataCrekomendasiPencairan['bulan1'] == 'April')?'selected':'' }}>April</option>
                                                        <option value="Mei" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Mei')?'selected':'' }}>Mei</option>
                                                        <option value="Juni" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Juni')?'selected':'' }}>Juni</option>
                                                        <option value="Juli" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Juli')?'selected':'' }}>Juli</option>
                                                        <option value="Agustus" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Agustus')?'selected':'' }}>Agustus</option>
                                                        <option value="September" {{ ($dataCrekomendasiPencairan['bulan1'] == 'September')?'selected':'' }}>September</option>
                                                        <option value="Oktober" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Oktober')?'selected':'' }}>Oktober</option>
                                                        <option value="November" {{ ($dataCrekomendasiPencairan['bulan1'] == 'November')?'selected':'' }}>November</option>
                                                        <option value="Desember" {{ ($dataCrekomendasiPencairan['bulan1'] == 'Desember')?'selected':'' }}>Desember</option>
                                                    </select>
                                                    <select name="tahun1">
                                                        @for ($i = 2016; $i <= 2020; $i++)
                                                            <option value="{{$i}}" {{ ($dataCrekomendasiPencairan['tahun1'] == $i)?'selected':'' }}>{{$i}}</option>
                                                        @endfor
                                                    </select>  
                                                    </center> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 15px;"><center>Kepada</center></td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 15px;">Yth. Kepala BPKD Provinsi DKI Jakarta selaku PPKD</td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 15px;">di <br>&nbsp;&nbsp;&nbsp;&nbsp;Jakarta</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            
                            <p style="padding: 30px 30px 0 30px;text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan  pencairan 
                            <select name="text2">
                                <option value="bantuan sosial" {{ ($dataCrekomendasiPencairan['text2'] == 'bantuan sosial')?'selected':'' }}>bantuan sosial</option>
                                <option value="hibah" {{ ($dataCrekomendasiPencairan['text2'] == 'hibah')?'selected':'' }}>hibah</option>
                            </select>*) dari {{ ucwords($recProposalByID->nm_lembaga) }} yang diajukan sesuai surat Nomor <input type="text" name="nomor2" value="{{ $dataCrekomendasiPencairan['nomor2'] }}" required=""> tanggal 
                            <select name="tanggal2">
                                @for ($i = 1; $i <= 31; $i++)
                                    <option value="{{$i}}" {{ ($dataCrekomendasiPencairan['tanggal2'] == $i)?'selected':'' }}>{{$i}}</option>
                                @endfor
                            </select>
                            <select name="bulan2">
                                <option value="Januari" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Januari')?'selected':'' }}>Januari</option>
                                <option value="Februari" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Februari')?'selected':'' }}>Februari</option>
                                <option value="Maret" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Maret')?'selected':'' }}>Maret</option>
                                <option value="April" {{ ($dataCrekomendasiPencairan['bulan2'] == 'April')?'selected':'' }}>April</option>
                                <option value="Mei" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Mei')?'selected':'' }}>Mei</option>
                                <option value="Juni" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Juni')?'selected':'' }}>Juni</option>
                                <option value="Juli" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Juli')?'selected':'' }}>Juli</option>
                                <option value="Agustus" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Agustus')?'selected':'' }}>Agustus</option>
                                <option value="September" {{ ($dataCrekomendasiPencairan['bulan2'] == 'September')?'selected':'' }}>September</option>
                                <option value="Oktober" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Oktober')?'selected':'' }}>Oktober</option>
                                <option value="November" {{ ($dataCrekomendasiPencairan['bulan2'] == 'November')?'selected':'' }}>November</option>
                                <option value="Desember" {{ ($dataCrekomendasiPencairan['bulan2'] == 'Desember')?'selected':'' }}>Desember</option>
                            </select>
                            <select name="tahun2">
                                @for ($i = 2016; $i <= 2020; $i++)
                                    <option value="{{$i}}" {{ ($dataCrekomendasiPencairan['tahun2'] == $i)?'selected':'' }}>{{$i}}</option>
                                @endfor
                            </select>
                            hal <input type="text" name="hal" value="{{ $dataCrekomendasiPencairan['hal'] }}" required=""> dan setelah dilakukan penelitian kesesuaian dan keabsahan dokumen terkait persyaratan, nama penerima, alamat penerima dan besaran dalam Rencana Anggaran Biaya Penggunaan
                            <select name="text3">
                                <option value="bantuan sosial" {{ ($dataCrekomendasiPencairan['text3'] == 'bantuan sosial')?'selected':'' }}>bantuan sosial</option>
                                <option value="hibah" {{ ($dataCrekomendasiPencairan['text3'] == 'hibah')?'selected':'' }}>hibah</option>
                            </select>*) sesuai daftar nama penerima, alamat penerima dan besaran 
                             <select name="text9">
                                <option value="bantuan sosial" {{ ($dataCrekomendasiPencairan['text9'] == 'bantuan sosial')?'selected':'' }}>bantuan sosial</option>
                                <option value="hibah" {{ ($dataCrekomendasiPencairan['text9'] == 'hibah')?'selected':'' }}>hibah</option>
                            </select>
                             dalam lampiran Keputusan Gubernur Nomor <input type="text" name="nomor_gubernur" value="{{ $dataCrekomendasiPencairan['nomor_gubernur'] }}" required=""> Tahun 
                            <select name="tahun3">
                                @for ($i = 2016; $i <= 2020; $i++)
                                    <option value="{{$i}}" {{ ($dataCrekomendasiPencairan['tahun3'] == $i)?'selected':'' }}>{{$i}}</option>
                                @endfor
                            </select> tentang <input type="text" name="tentang" value="{{ $dataCrekomendasiPencairan['tentang'] }}" required=""> dengan rincian sebagai berikut :
                            <table width="100%">
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Nama Penerima</td>
                                    <td> : </td>
                                    <td>&nbsp;{{ucwords($recProposalByID->nm_lembaga)}}</td>
                                </tr>
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Alamat</td>
                                    <td> : </td>
                                    <td>&nbsp;{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
                                </tr>
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Besaran 
                                    <select name="text4">
                                        <option value="Bantuan Sosial" {{ ($dataCrekomendasiPencairan['text4'] == 'Bantuan Sosial')?'selected':'' }}>Bantuan Sosial</option>
                                        <option value="Hibah" {{ ($dataCrekomendasiPencairan['text4'] == 'Hibah')?'selected':'' }}>Hibah</option>
                                    </select> *)
                                    </td>
                                    <td> : </td>
                                    <td>&nbsp;Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="padding-left: 30px;">(dalam hal nama penerima lebih dari 1 (satu), maka menggunakan format sebagaimana terlampir)</td>
                                </tr>
                            </table>
                            <br>
                            <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa sesuai surat permohonan dari penerima 
                            <select name="text5">
                                <option value="bantuan sosial" {{ ($dataCrekomendasiPencairan['text5'] == 'bantuan sosial')?'selected':'' }}>bantuan sosial</option>
                                <option value="hibah" {{ ($dataCrekomendasiPencairan['text5'] == 'hibah')?'selected':'' }}>hibah</option>
                            </select>*) direkomendasikan untuk diproses pencairan anggaran 
                            <select name="text6">
                                <option value="bantuan sosial" {{ ($dataCrekomendasiPencairan['text6'] == 'bantuan sosial')?'selected':'' }}>bantuan sosial</option>
                                <option value="hibah" {{ ($dataCrekomendasiPencairan['text6'] == 'hibah')?'selected':'' }}>hibah</option>
                            </select>*) atas nama tersebut di atas dengan rincian : </p>
                            <table width="100%">
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Besaran 
                                    <select name="text7">
                                        <option value="Bantuan Sosial" {{ ($dataCrekomendasiPencairan['text7'] == 'Bantuan Sosial')?'selected':'' }}>Bantuan Sosial</option>
                                        <option value="Hibah" {{ ($dataCrekomendasiPencairan['text7'] == 'Hibah')?'selected':'' }}>Hibah</option>
                                    </select> *)
                                    </td>
                                    <td> : </td>
                                    <td>&nbsp;Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</td>
                                </tr>
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Akumulasi Pencairan Sebelumnya</td>
                                    <td> : </td>
                                    <td>&nbsp;Rp. {{ number_format($akumulasiPencairan-$dataCrekomendasiPencairan['nominal_pencairan'],2,',','.') }} (**)</td>
                                </tr>
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Pencairan saat ini</td>
                                    <td> : </td>
                                    <td style="border-bottom: 1px solid black;">&nbsp;Rp. <input name="nominal_pencairan" type="text" value="{{ $dataCrekomendasiPencairan['nominal_pencairan'] }}" class="input-numeral" style="width: 50%" required="" max="{{ $sisaDana }}"> <i class="text-danger">Tidak dapat lebih besar dari sisa dana</i></td>
                                </tr>
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Sisa 
                                    <select name="text8">
                                        <option value="Bantuan Sosial" {{ ($dataCrekomendasiPencairan['text8'] == 'Bantuan Sosial')?'selected':'' }}>Bantuan Sosial</option>
                                        <option value="Hibah" {{ ($dataCrekomendasiPencairan['text8'] == 'Hibah')?'selected':'' }}>Hibah</option>
                                    </select> *)
                                    </td>
                                    <td> : </td>
                                    <td>&nbsp;Rp. {{ number_format($sisaDana,2,',','.') }}</td>
                                </tr>
                                <tr>
                                    <td width="280" style="padding-left: 30px;">Terbilang</td>
                                    <td> : </td>
                                    <td><input type="text" name="terbilang" value="{{ $dataCrekomendasiPencairan['terbilang'] }}" style="width: 70%"  required=""></td>
                                </tr>
                            </table>
                            
                            <p style="padding: 0 30px">Untuk ditransfer Ke Nomor Rekening : <input type="text" name="no_rekening" value="{{ $dataCrekomendasiPencairan['no_rekening'] }}" required=""> a.n. <input type="text" name="atas_nama" value="{{ $dataCrekomendasiPencairan['atas_nama'] }}" required=""></p>

                            <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian surat rekomendasi ini dibuat dengan sebenar-benarnya dan saya bertanggung jawab penuh atas keabsahan rekomendasi tersebut.</p>
                            
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <center>
                                            {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                                            <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                                        </center>
                                    </td>
                                    <td width="30%"></td>
                                    <td><center>
                                        <h4>KEPALA <span style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</span></h4>
                                        <br><br><br>
                                        <h4><input type="text" name="plt" value="{{ $dataCrekomendasiPencairan['plt'] }}">  <input type="text" name="nama_kepala" value="{{ $dataCrekomendasiPencairan['nama_kepala'] }}" readonly=""></h4>
                                        <h4>NIP <input type="text" name="nip" value="{{ $dataCrekomendasiPencairan['nip'] }}" readonly=""></h4>
                                        </center>
                                    </td>
                                </tr>
                            </table>
                            <br><br><br>
                            <table width="100%">
                                <tr>
                                    <td>*) pilih salah satu</td>
                                </tr>
                                <tr>
                                    <td>**) diisi dalam hal pencairan dilakukan secara bertahap</td>
                                </tr>
                            </table>
                            <hr>
                            <div class="row">
	                            <label class="control-label col-md-3">Upload File Tanda Tangan Basah Pencairan Bertahap
	                            <span class="required"> * </span>
	                            </label>
	                            <div class="col-md-9">
	                                <div class="clearfix margin-top-10">
	                                    <span class="label" style="color: red;font-size: 15px;font-weight: 400;    background-color: #f6f6fa;"> <small>Info : File berformat PDF/JPG/JPEG/PNG dan Max 10MB </small></span>
	                                </div>
	                                <input type="file" name="file_name" class="btn default form-control">
	                               	<a href="{{ url('/') }}/upload_ttd_basah_pencarian/{{ $dataCrekomendasiPencairan['file_name'] }}" target="_blank">{{ $dataCrekomendasiPencairan['file_name'] }}</a>

	                            </div>
                            </div>
                            <hr>
                            <div class="row">
	                            <label class="control-label col-md-3">Status Pencairan
	                            <span class="required"> * </span>
	                            </label>
	                            <div class="col-md-9">
	                                <select name="status_pencairan" class="form-control">
	                                	<option value="Sudah Dicairkan" {{ ($dataCrekomendasiPencairan['status_pencairan']=='Sudah Dicairkan')?'selected':'' }}>Sudah Dicairkan</option>
	                                	<option value="Belum Dicairkan" {{ ($dataCrekomendasiPencairan['status_pencairan']=='Belum Dicairkan')?'selected':'' }}>Belum Dicairkan</option>
	                                	<option value="Batal Dicairkan" {{ ($dataCrekomendasiPencairan['status_pencairan']=='Batal Dicairkan')?'selected':'' }}>Batal Dicairkan</option>
	                                </select>
	                            </div>
                            </div>
                            <hr>
                            <button type="submit" class="btn green" id="btn-simpan-rekomendasi-pencairan">Simpan Perubahan</button>
                            <a href="{{route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab])}}" class="btn btn-default" style="float: right;">Kembali</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
   
            $('input[name="file_name"]').fileuploader({
                limit: 1,
                extensions: ['jpg', 'jpeg', 'png', 'pdf'],
                fileMaxSize: 10
            });
            
        });
    </script>
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop