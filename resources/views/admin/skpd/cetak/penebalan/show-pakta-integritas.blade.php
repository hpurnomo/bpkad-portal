<div class="tab-pane fade {{($tab=='nphd')?'active':''}} in" id="tab_6_6">            
    <div class="row">
        <div class="col-md-12">
            @if($dataCNPHD['id'])
            <a href="{{ route('skpd.cetak.penebalan.pakta.integritas',['uuid'=>$uuid,'fase'=>$fase,'tahap'=>$tahap]) }}" class="btn red confirmLink"><i class="fa fa-download" aria-hidden="true"></i> Unduh Pakta Integritas</a>
            {{-- <a href="{{ route('skpd.cetak.penebalan.naskah.perjanjian.nphd.bukafituredit',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>$tahap,'tab'=>'nphd']) }}" class="btn green" style="float: right;"><i class="fa fa-unlock" aria-hidden="true"></i> Buka Laporan Ini Agar Dapat Di Ubah Oleh Lembaga</a> --}}
            @endif
        </div>
    </div>
    <hr>
    <form action="" method="post">
        <center>
		    <h4> PAKTA INTEGRITAS </h4>
		</center>
		<p style="padding: 30px 30px 0 50px;">
		Sesuai Peraturan Menteri Dalam Negeri Nomor {{ $dataCPI['nomor1'] }} Tahun {{ $dataCPI['tahun1'] }} dan Peraturan Gubernur Nomor {{ $dataCPI['nomor2'] }} Tahun {{ $dataCPI['tahun2'] }}, dengan ini kami yang bertanda tangan di bawah ini :
		<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
			<tr>
				<td width="150">Nama</td>
				<td> : </td>
				<td style="padding: 3px;text-align: justify;">{{ $dataCPI['nama_pemohon'] }}</td>
			</tr>
			<tr>
				<td width="150">No. KTP/NIP/NRP *)</td>
				<td> : </td>
				<td style="padding: 3px;text-align: justify;">{{ $dataCPI['no_pemohon'] }}</td>
			</tr>
			<tr>
				<td width="150">Jabatan</td>
				<td> : </td>
				<td style="padding: 3px;text-align: justify;">{{ $dataCPI['jabatan_pemohon'] }}</td>
			</tr>
			<tr>
				<td width="150">Nama Lembaga **)</td>
				<td> : </td>
				<td style="padding: 3px;text-align: justify;">{{ $dataCPI['nama_lembaga'] }}
				</td>
			</tr>
			<tr>
				<td width="150">Alamat Lembaga</td>
				<td> : </td>
				<td style="padding: 3px;text-align: justify;">{{ $dataCPI['alamat_lembaga'] }}</td>
			</tr>
		</table>
		<p style="padding: 0 30px 0 50px;text-align: justify;">Sebagai penerima {{ $dataCPI['text1'] }} dalam bentuk uang dari Pemerintah Provinsi DKI Jakarta Tahun Anggaran {{ $dataCPI['tahun_anggaran1'] }} sebesar Rp.{{ $dataCPI['nominal'] }} ({{ $dataCPI['terbilang'] }} rupiah) yang akan digunakan untuk kegiatan sebagai berikut :</p>
		{!! $dataCPI['kegiatan1'] !!}
		<p style="padding: 0 30px 0 50px;text-align: justify;">Dengan ini menyatakan bahwa : </p>
		<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
		    <tr>
		        <td style="vertical-align: top;">1.</td>
		        <td style="padding: 0 50px 0 10px;text-align: justify;">Akan melaporkan dan mempertanggungjawabkan penggunaan dana {{ $dataCPI['text2'] }} yang diterima dari Pemerintah Provinsi DKI Jakarta kepada Gubernur Provinsi DKI Jakarta melalui BPKD selaku PPKD dengan tembusan {{ $dataCPI['nm_skpd'] }} paling lambat tanggal 10 bulan Maret tahun anggaran berikutnya;</td>
		    </tr>
		    <tr>
		        <td style="vertical-align: top;">2.</td>
		        <td style="padding: 0 50px 0 10px;text-align: justify;">Akan melaksanakan kegiatan sesuai dengan proposal yang diusulkan {{ $dataCPI['text3'] }}, serta bertanggung jawab secara formal dan material atas penggunaan dana {{ $dataCPI['text4'] }} yang diterima.</td>
		    </tr>
		    <tr>
		        <td style="vertical-align: top;">3.</td>
		        <td style="padding: 0 50px 0 10px;text-align: justify;">Tidak akan mengalihkan anggaran belanja {{ $dataCPI['text5'] }} kepada pihak lain; dan</td>
		    </tr>
		    <tr>
		        <td style="vertical-align: top;">4.</td>
		        <td style="padding: 0 50px 0 10px;text-align: justify;">Bersedia dituntut sesuai dengan hukum yang berlaku di Republik Indonesia apabila di kemudian hari terdapat penyimpangan/penyalahgunaan dana {{ $dataCPI['text6'] }} yang diterima </td>
		    </tr>
		</table>
		<p style="padding: 0 30px 0 50px;text-align: justify;">Demikian surat pernyataan tanggung jawab mutlak ini dibuat di atas meterai secukupnya untuk dapat dipergunakan sebagaimana mestinya.</p>
		<table width="100%">
		    <tr>
		        <td width="30%">
		        	&nbsp;
		        </td>
		        <td width="30%"></td>
		        <td>
		        	<center>
		        		<p>Jakarta,{{ $dataCPI['text7'] }} </p>
		        		<p>{{ $dataCPI['jabatan_pemberi'] }}</p>
		        		<br>
		        		<p>(materai 6000)</p>
		        		<br>
		        		<p>{{ $dataCPI['nama_pemberi'] }}</p>
		        		<p>NIP/NRP {{ $dataCPI['no_pemberi'] }} *)</p>
		            </center>
		        </td>
		    </tr>
		</table>
		<p style="padding: 30px 30px 0 50px;text-align: justify;">
			<small>
				*) bagi penerima hibah instansi pemerintah <br>
				**) bagi penerima bantuan sosial selain individu/keluarga <br><br>
				Note : coret bagian yang tidak perlu
			</small>
		</p>
    </form>
        
</div>