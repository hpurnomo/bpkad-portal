@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Cetak {{ strtoupper($tahap) }}</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{dd($recProposalByID)}} --}}
        <a href="{{route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab])}}" class="btn btn-default" style="float: right;">Kembali</a><br><br>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-print" aria-hidden="true"></i> Tambah Rekomendasi Pencairan </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
						<form action="{{ route('skpd.cetak.penebalan.storeRekomendasiPencairan') }}" method="post">
						    {{ csrf_field() }}
						    <input type="hidden" name="no_prop" value="{{ $no_prop }}">
						    <input type="hidden" name="uuid" value="{{ $uuid }}">
						    <input type="hidden" name="tahap" value="{{ $tahap }}">
						    <input type="hidden" name="fase" value="{{ $fase }}">
						    <input type="hidden" name="tab" value="{{ $tab }}">
						    <center>
						        <h4>SURAT REKOMENDASI PENCAIRAN</h4>
						        <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4>
						    </center>
						    <br><br>
						    <table width="100%" border="0" cellpadding="0" cellspacing="0">
						        <tr>
						            <td width="50%">
						                <table width="100%" border="0" cellpadding="0" cellspacing="0">
						                    <tr>
						                        <td>Judul <span class="text-danger">*</span></td>
						                        <td>:</td>
						                        <td><input type="text" name="judul" value="" required="" style="width: 80%"></td>
						                    </tr>
						                    <tr>
						                        <td>Nomor <span class="text-danger">*</span></td>
						                        <td>:</td>
						                        <td><input type="text" name="nomor" value="" required="" style="width: 80%"></td>
						                    </tr>
						                    <tr>
						                        <td>Sifat <span class="text-danger">*</span></td>
						                        <td>:</td>
						                        <td><input type="text" name="sifat" value="" required="" style="width: 80%"></td>
						                    </tr>
						                    <tr>
						                        <td>Lampiran <span class="text-danger">*</span></td>
						                        <td>:</td>
						                        <td><input type="text" name="lampiran" value="" required="" style="width: 80%"></td>
						                    </tr>
						                    <tr>
						                        <td style="vertical-align: top">Hal</td>
						                        <td style="vertical-align: top;">:</td>
						                        <td>Surat Rekomendasi Pencairan<br> 
						                        <select name="text1">
								                    <option value="Bantuan Sosial">Bantuan Sosial</option>
								                    <option value="Hibah">Hibah</option>
								                </select> *)
            									</td>
						                    </tr>
						                </table>
						            </td>
						            <td width="50%">
						                <table width="100%" border="0" cellpadding="0" cellspacing="0">
						                    <tr>
						                        <td>
						                            <center>
						                            <select name="tanggal1">
						                                @for ($i = 1; $i <= 31; $i++)
						                                    <option value="{{$i}}">{{$i}}</option>
						                                @endfor
						                            </select>
						                            <select name="bulan1">
						                                <option value="Januari">Januari</option>
						                                <option value="Februari">Februari</option>
						                                <option value="Maret">Maret</option>
						                                <option value="April">April</option>
						                                <option value="Mei">Mei</option>
						                                <option value="Juni">Juni</option>
						                                <option value="Juli">Juli</option>
						                                <option value="Agustus">Agustus</option>
						                                <option value="September">September</option>
						                                <option value="Oktober">Oktober</option>
						                                <option value="November">November</option>
						                                <option value="Desember">Desember</option>
						                            </select>
						                            <select name="tahun1">
						                                @for ($i = 2016; $i <= 2020; $i++)
						                                    <option value="{{$i}}">{{$i}}</option>
						                                @endfor
						                            </select>  
						                            </center> 
						                        </td>
						                    </tr>
						                    <tr>
						                        <td style="padding: 15px;"><center>Kepada</center></td>
						                    </tr>
						                    <tr>
						                        <td style="padding: 15px;">Yth. Kepala BPKD Provinsi DKI Jakarta selaku PPKD</td>
						                    </tr>
						                    <tr>
						                        <td style="padding: 15px;">di <br>&nbsp;&nbsp;&nbsp;&nbsp;Jakarta</td>
						                    </tr>
						                </table>
						            </td>
						        </tr>
						    </table>
						    
						    <p style="padding: 30px 30px 0 30px;text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan  pencairan 
						    <select name="text2">
			                    <option value="bantuan sosial">bantuan sosial</option>
			                    <option value="hibah">hibah</option>
			                </select>*) dari {{ ucwords($recProposalByID->nm_lembaga) }} yang diajukan sesuai surat Nomor <input type="text" name="nomor2" value="" required=""> tanggal 
						    <select name="tanggal2">
						        @for ($i = 1; $i <= 31; $i++)
						            <option value="{{$i}}">{{$i}}</option>
						        @endfor
						    </select>
						    <select name="bulan2">
						        <option value="Januari">Januari</option>
                                <option value="Februari">Februari</option>
                                <option value="Maret">Maret</option>
                                <option value="April">April</option>
                                <option value="Mei">Mei</option>
                                <option value="Juni">Juni</option>
                                <option value="Juli">Juli</option>
                                <option value="Agustus">Agustus</option>
                                <option value="September">September</option>
                                <option value="Oktober">Oktober</option>
                                <option value="November">November</option>
                                <option value="Desember">Desember</option>
						    </select>
						    <select name="tahun2">
						        @for ($i = 2016; $i <= 2020; $i++)
						            <option value="{{$i}}">{{$i}}</option>
						        @endfor
						    </select>
						    hal <input type="text" name="hal" value="" required=""> dan setelah dilakukan penelitian kesesuaian dan keabsahan dokumen terkait persyaratan, nama penerima, alamat penerima dan besaran dalam Rencana Anggaran Biaya Penggunaan
						    <select name="text3">
			                    <option value="bantuan sosial">bantuan sosial</option>
			                    <option value="hibah">hibah</option>
			                </select>*) sesuai daftar nama penerima, alamat penerima dan besaran 
			                <select name="text9">
                                <option value="bantuan sosial"  >bantuan sosial</option>
                                <option value="hibah" >hibah</option>
                            </select> dalam lampiran Keputusan Gubernur Nomor <input type="text" name="nomor_gubernur" value="" required=""> Tahun 
			                <select name="tahun3">
						        @for ($i = 2016; $i <= 2020; $i++)
						            <option value="{{$i}}">{{$i}}</option>
						        @endfor
						    </select> tentang <input type="text" name="tentang" value="" required=""> dengan rincian sebagai berikut :
						    <table width="100%">
						        <tr>
						            <td width="280" style="padding-left: 30px;">Nama Penerima</td>
						            <td> : </td>
						            <td>&nbsp;{{ucwords($recProposalByID->nm_lembaga)}}</td>
						        </tr>
						        <tr>
						            <td width="280" style="padding-left: 30px;">Alamat</td>
						            <td> : </td>
						            <td>&nbsp;{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
						        </tr>
						        <tr>
						            <td width="280" style="padding-left: 30px;">Besaran 
						            <select name="text4">
					                    <option value="Bantuan Sosial">Bantuan Sosial</option>
					                    <option value="Hibah">Hibah</option>
					                </select> *)
					            	</td>
						            <td> : </td>
						            <td>&nbsp;Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</td>
						        </tr>
						        <tr>
						            <td colspan="3" style="padding-left: 30px;">(dalam hal nama penerima lebih dari 1 (satu), maka menggunakan format sebagaimana terlampir)</td>
						        </tr>
						    </table>
						    {{-- {{ dd($recProposalByID) }} --}}
						    <br>
						    <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa sesuai surat permohonan dari penerima 
					    	<select name="text5">
			                    <option value="bantuan sosial">bantuan sosial</option>
			                    <option value="hibah">hibah</option>
			                </select>*) direkomendasikan untuk diproses pencairan anggaran 
			            	<select name="text6">
			                    <option value="bantuan sosial">bantuan sosial</option>
			                    <option value="hibah">hibah</option>
			                </select>*) atas nama tersebut di atas dengan rincian : </p>
			                <table width="100%">
						        <tr>
						            <td width="280" style="padding-left: 30px;">Besaran 
						            <select name="text7">
					                    <option value="Bantuan Sosial">Bantuan Sosial</option>
					                    <option value="Hibah">Hibah</option>
					                </select> *)
					            	</td>
						            <td> : </td>
						            <td>&nbsp;Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</td>
						        </tr>
						        <tr>
						            <td width="280" style="padding-left: 30px;">Akumulasi Pencairan Sebelumnya</td>
						            <td> : </td>
						            <td>&nbsp;Rp. {{ number_format($akumulasiPencairan,2,',','.') }} (**)</td>
						        </tr>
						        <tr>
						            <td width="280" style="padding-left: 30px;">Pencairan saat ini</td>
						            <td> : </td>
						            <td style="border-bottom: 1px solid black;">&nbsp;Rp. <input name="nominal_pencairan" type="text" class="input-numeral" style="width: 50%" required="" max="{{ $sisaDana }}"> <i class="text-danger">Tidak dapat lebih besar dari sisa dana</i></td>
						        </tr>
						        <tr>
						            <td width="280" style="padding-left: 30px;">Sisa 
						            <select name="text8">
					                    <option value="Bantuan Sosial">Bantuan Sosial</option>
					                    <option value="Hibah">Hibah</option>
					                </select> *)
					            	</td>
						            <td> : </td>
						            <td>&nbsp;Rp. {{ number_format($sisaDana,2,',','.') }}</td>
						        </tr>
						        <tr>
						            <td width="280" style="padding-left: 30px;">Terbilang</td>
						            <td> : </td>
						            <td><input type="text" name="terbilang" style="width: 70%"  required=""></td>
						        </tr>
						    </table>
						    
						    <p style="padding: 0 30px">Untuk ditransfer Ke Nomor Rekening : <input type="text" name="no_rekening" required=""> a.n. <input type="text" name="atas_nama" required=""></p>

						    <p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian surat rekomendasi ini dibuat dengan sebenar-benarnya dan saya bertanggung jawab penuh atas keabsahan rekomendasi tersebut.</p>
						    
						    <table width="100%">
						        <tr>
						            <td width="30%">
						                <center>
						                    {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
						                    <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
						                </center>
						            </td>
						            <td width="30%"></td>
						            <td><center>
						                <h4>KEPALA <span style="text-transform: uppercase;"> {{$recProposalByID->nm_skpd}}</span></h4>
						                <br><br><br>
						                <h4> <input type="text" name="plt" value="{{ $recSkpdByNomor->plt }}"> <input type="text" name="nama_kepala" value="{{ $recSkpdByNomor->kontak_person }}" readonly=""></h4>
						                <h4>NIP <input type="text" name="nip" value="{{ $recSkpdByNomor->nip_ketua }}" readonly=""></h4>
						                </center>
						            </td>
						        </tr>
						    </table>
						    <br><br><br>
						    <table width="100%">
						        {{-- <tr>
						            <td>
						                Tembusan : 
						            </td>
						        </tr>
						        <tr>
						            <td>
						                <ol>
						                    <li>Sekretaris Daerah Provinsi DKI Jakarta</li>
						                    <li>Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta</li>        
						                </ol>
						            </td>
						        </tr> --}}
						        <tr>
						            <td>*) pilih salah satu</td>
						        </tr>
						        <tr>
						            <td>**) diisi dalam hal pencairan dilakukan secara bertahap</td>
						        </tr>
						    </table>
						    <hr>
						    {{-- @if($dataCrekomendasiPencairan['is_generate']==0) --}}
						    <button type="submit" class="btn green" id="btn-simpan-rekomendasi-pencairan">Simpan</button>
						    {{-- @endif --}}
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // numeral
            var cleaveNumeral = new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop