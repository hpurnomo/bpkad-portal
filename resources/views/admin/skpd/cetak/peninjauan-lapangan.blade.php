<div class="tab-pane fade {{($tab=='peninjauan-lapangan')?'active':''}} in" id="tab_6_2">
    <form action="{{ route('skpd.cetak.updatePL') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="fase" value="{{$fase}}">
    <input type="hidden" name="tab" value="peninjauan-lapangan">
    <center>
        <h4>BERITA ACARA PENINJAUAN LAPANGAN</h4>
        <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_lembaga}}</h4>
        <h4>TAHUN ANGGARAN {{$recProposalByID->tahun_anggaran}}</h4>
        <h4>NOMOR <input type="text" name="nomor" value="{{$recProposalByID->no_survei_lap}}" readonly=""></h4>
    </center>
    <br><br>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada hari ini 
    <select name="hari">
        <option value="Senin" {{($dataCpl['hari']=='Senin')?'selected':''}}>Senin</option>
        <option value="Selasa" {{($dataCpl['hari']=='Selasa')?'selected':''}}>Selasa</option>
        <option value="Rabu" {{($dataCpl['hari']=='Rabu')?'selected':''}}>Rabu</option>
        <option value="Kamis" {{($dataCpl['hari']=='Kamis')?'selected':''}}>Kamis</option>
        <option value="Jumat" {{($dataCpl['hari']=='Jumat')?'selected':''}}>Jumat</option>
    </select>
    tanggal
    <select name="tanggal1">
        @for ($i = 1; $i <= 31; $i++)
            <option value="{{$i}}" {{($dataCpl['tanggal1']==$i)?'selected':''}}>{{$i}}</option>
        @endfor
    </select>
    bulan
    <select name="bulan1">
        <option value="Januari" {{($dataCpl['bulan1']=="Januari")?'selected':''}}>Januari</option>
        <option value="Februari" {{($dataCpl['bulan1']=="Februari")?'selected':''}}>Februari</option>
        <option value="Maret" {{($dataCpl['bulan1']=="Maret")?'selected':''}}>Maret</option>
        <option value="April" {{($dataCpl['bulan1']=="April")?'selected':''}}>April</option>
        <option value="Mei" {{($dataCpl['bulan1']=="Mei")?'selected':''}}>Mei</option>
        <option value="Juni" {{($dataCpl['bulan1']=="Juni")?'selected':''}}>Juni</option>
        <option value="Juli" {{($dataCpl['bulan1']=="Juli")?'selected':''}}>Juli</option>
        <option value="Agustus" {{($dataCpl['bulan1']=="Agustus")?'selected':''}}>Agustus</option>
        <option value="September" {{($dataCpl['bulan1']=="September")?'selected':''}}>September</option>
        <option value="Oktober" {{($dataCpl['bulan1']=="Oktober")?'selected':''}}>Oktober</option>
        <option value="November" {{($dataCpl['bulan1']=="November")?'selected':''}}>November</option>
        <option value="Desember" {{($dataCpl['bulan1']=="Desember")?'selected':''}}>Desember</option>
    </select>
    tahun
    <select name="tahun1">
        @for ($i = 2016; $i <= 2020; $i++)
            <option value="{{$i}}" {{($dataCpl['tahun1']==$i)?'selected':''}}>{{$i}}</option>
        @endfor
    </select>
    yang bertanda tangan di bawah ini :</p>
    <table border="1" width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th style="padding: 10px;"><center>No</center></th>
          <th style="padding: 10px;"><center>Nama</center></th>
          <th style="padding: 10px;"><center>Jabatan Dalam Tim</center></th>  
          <th style="padding: 10px;"><center>Tanda Tangan</center></th>
          <th style="padding: 10px;"><center></center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($dataDcpl as $key=>$value)
        <tr>
          <td style="padding: 10px;"><center>{{ $value->no_urut }}</center></td>
          <td style="padding: 10px;">{{ $value->nama }}</td>
          <td style="padding: 10px;">{{ $value->jabatan }}</td>  
          <td style="padding: 10px;"></td>
          <td style="padding: 10px;"><center><a href="{{ route('skpd.cetak.delete.detail.peserta.peninjauan.lapangan',['id'=>$value->id,'no_prop'=>$recProposalByID->no_prop,'fase'=>$fase,'tab'=>$tab]) }}" title="hapus"><i class="fa fa-trash"></i></a></center></td>
        </tr>
        @endforeach
      </tbody>
      <tfoot id="items-peninjauan">
        
      </tfoot>
    </table>
    <a class="btn btn-primary" href="javascript:;" id="add-peserta-peninjauan">Add Field</a>
    <br><br>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan Surat Tugas Kepala {{$recProposalByID->nm_skpd}}, Nomor <input type="text" name="nomor2" value="{{$dataCpl['nomor2']}}"> 
    tanggal
    <select name="tanggal2">
        @for ($i = 1; $i <= 31; $i++)
            <option value="{{$i}}" {{($dataCpl['tanggal2']==$i)?'selected':''}}>{{$i}}</option>
        @endfor
    </select>
    bulan
    <select name="bulan2">
        <option value="Januari" {{($dataCpl['bulan2']=="Januari")?'selected':''}}>Januari</option>
        <option value="Februari" {{($dataCpl['bulan2']=="Februari")?'selected':''}}>Februari</option>
        <option value="Maret" {{($dataCpl['bulan2']=="Maret")?'selected':''}}>Maret</option>
        <option value="April" {{($dataCpl['bulan2']=="April")?'selected':''}}>April</option>
        <option value="Mei" {{($dataCpl['bulan2']=="Mei")?'selected':''}}>Mei</option>
        <option value="Juni" {{($dataCpl['bulan2']=="Juni")?'selected':''}}>Juni</option>
        <option value="Juli" {{($dataCpl['bulan2']=="Juli")?'selected':''}}>Juli</option>
        <option value="Agustus" {{($dataCpl['bulan2']=="Agustus")?'selected':''}}>Agustus</option>
        <option value="September" {{($dataCpl['bulan2']=="September")?'selected':''}}>September</option>
        <option value="Oktober" {{($dataCpl['bulan2']=="Oktober")?'selected':''}}>Oktober</option>
        <option value="November" {{($dataCpl['bulan2']=="November")?'selected':''}}>November</option>
        <option value="Desember" {{($dataCpl['bulan2']=="Desember")?'selected':''}}>Desember</option>
    </select>
    tahun
    <select name="tahun2">
        @for ($i = 2016; $i <= 2020; $i++)
            <option value="{{$i}}" {{($dataCpl['tahun2']==$i)?'selected':''}}>{{$i}}</option>
        @endfor
    </select> telah peninjauan lapangan terhadap:</p>
    <br>
    <table width="100%">
        <tr>
            <td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama(Lembaga)</td>
            <td>:</td>
            <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
        </tr>
        <tr>
            <td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat(Lembaga)</td>
            <td>:</td>
            <td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
        </tr>
    </table>
    <br>
    <p>Dengan hasil sebagai berikut :</p>
     <table  border="1" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="padding: 10px;"><center>No</center></td>
                <td style="padding: 10px;"><center>Data Administrasi</center></td>
                <td style="padding: 10px;"><center>Ada</center></td>
                <td style="padding: 10px;"><center>Tidak Ada</center></td>
                <td style="padding: 10px;"><center>Keterangan</center></td>
            </tr> 
            <tr>
                <td><b><center>A</center></b></td>
                <td colspan="4" style="padding-left: 10px;"><b> Kelengkapan proposal usulan hibah/bansos:</b></td> 
            </tr>

            <tr>
                <td><center>1</center></td>
                <td style="padding-left: 10px;">Identitas dan alamat pengusul</td>
                <td><center>@if($prop_adminisitrasi['cek_identitas_alamat']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_identitas_alamat']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_identitas_alamat'] !!}</td>
            </tr>

             <tr>
                <td><center>2</center></td>
                <td style="padding-left: 10px;">Latar Belakang</td>
                <td><center>@if($prop_adminisitrasi['cek_latar_belakang']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_latar_belakang']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_latar_belakang'] !!}</td>
            </tr>

            <tr>
                <td><center>3</center></td>
                <td style="padding-left: 10px;">Maksud dan Tujuan</td>
                <td><center>@if($prop_adminisitrasi['cek_maksud_tujuan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_maksud_tujuan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_maksud_tujuan'] !!}</td>
            </tr>

            <tr>
                <td><center>4</center></td>
                <td style="padding-left: 10px;">Rincian rencana kegiatan (jadwal pelaksanaan kegiatan)</td>
                <td><center>@if($prop_adminisitrasi['cek_jadwal_pelaksanaan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_jadwal_pelaksanaan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_jadwal_pelaksanaan'] !!}</td>
            </tr>

             <tr>
                <td><center>5</center></td>
                <td style="padding-left: 10px;">Rincian rencana penggunaan hibah/bansos  (rincian anggaran biaya)</td>
                <td><center>@if($prop_adminisitrasi['cek_rencana_rab']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_rencana_rab']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_rencana_rab'] !!}</td>
            </tr>

           <!--  <tr>
                <td><center>2</center></td>
                <td style="padding-left: 10px;">Alamat</td>
                <td><center>@if($dataKal['cek_alamat_lembaga']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_alamat_lembaga']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_alamat_lembaga'] !!}</td>
            </tr>
            <tr>
                <td><center>3</center></td>
                <td style="padding-left: 10px;">Aktivitas</td>
                <td><center>@if($dataKap['cek_aktivitas']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKap['cek_aktivitas']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_aktivitas'] !!}</td>
            </tr>
            <tr>
                <td><center>4</center></td>
                <td style="padding-left: 10px;">Kepengurusan</td>
                <td><center>@if($dataKap['cek_kepengurusan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKap['cek_kepengurusan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_kepengurusan'] !!}</td>
            </tr>
            <tr>
                <td><center>5</center></td>
                <td style="padding-left: 10px;">Rencana Anggaran Biaya</td>
                <td><center>@if($dataKap['cek_rab']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKap['cek_rab']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_rab'] !!}</td>
            </tr>
           
            <tr>
                <td><center>7</center></td>
                <td style="padding-left: 10px;">Waktu pelaksanaan</td>
                <td><center>@if($dataKap['cek_waktu_pelaksanaan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKap['cek_waktu_pelaksanaan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_waktu_pelaksanaan'] !!}</td>
            </tr> -->

            <tr>
                <td><b><center>B</center></b></td>
                <td colspan="4" style="padding-left: 10px;"><b> Dokumen Administrasi:</b></td> 
            </tr>  

            <tr>
                <td><center>1</center></td>
                <td style="padding-left: 10px;"> Fotokopi Kartu Tanda penduduk (KTP) Ketua/ pimpinan badan, lembaga atau organisasi kemasyarakatan *)</td>
                <td><center>@if($dataKal['cek_ktp_ketua']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_ktp_ketua']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_ktp_ketua'] !!}</td>
            </tr>

             <tr>
                <td><center>2</center></td>
                <td style="padding-left: 10px;"> Fotokopi Akta Notaris Pendirian badan hukum yang telah mendapatkan pengesahan dari Kementrian yang membidangi hukum atau keputusan Gubernur tentang </td>
                <td><center>@if($dataKal['cek_akta']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_akta']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_akta'] !!}</td>
            </tr>

             <tr>
                <td><center>3</center></td>
                <td style="padding-left: 10px;">Fotokopi Nomor Pokok Wajib Pajak (NPWP) *) </td>
                <td><center>@if($dataKal['cek_npwp']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_npwp']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_npwp'] !!}</td>
            </tr>

             <tr>
                <td><center>4</center></td>
                <td style="padding-left: 10px;">    Fotokopi Surat keterangan domisili organisasi kemasyarakatan dari kelurahan setempat atau sebutan lainnya *)</td>
                <td><center>@if($dataKal['cek_skd']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_skd']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_skd'] !!}</td>
            </tr>

            <tr>
                <td><center>5</center></td>
                <td style="padding-left: 10px;">Fotokopi izin operasional /tanda daftar lembaga dari instasi yang berwenang *)</td>
                <td><center>@if($dataKal['cek_izin_operasional']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_izin_operasional']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_izin_operasional'] !!}</td>
            </tr>

              <tr>
                <td><center>6</center></td>
                <td style="padding-left: 10px;">Fotokopi sertifikat tanah/bukti kepemilikan tanah atau bukti perjanjian sewa bangunan/gedung dengan jangka waktu sewa minimal 3 (tiga) tahun atau dokumen lain yang dipersamakan *)</td>
                <td><center>@if($dataKal['cek_sertifikat']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_sertifikat']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_sertifikat'] !!}</td>
            </tr> 

            <tr>
                <td><center>7</center></td>
                <td style="padding-left: 10px;">Surat pernyataan tangungjawab bermaterai cukup *)</td>
                <td><center>@if($dataKal['cek_pernyataan_materai']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_pernyataan_materai']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['ket_pernyataan_materai'] !!}</td>
            </tr>

             <tr>
                <td><center>8</center></td>
                <td style="padding-left: 10px;">Salinan rekening bank yang masih aktif atas nama badan, lembaga atau organisasi kemasyarakatan</td>
                <td><center>@if($dataKal['cek_rekening_lembaga']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_rekening_lembaga']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_rekening_lembaga'] !!}</td>
            </tr>

             <tr>
                <td><center>9</center></td>
                <td style="padding-left: 10px;">Fotokopi SK kepengurusan atau dokumen yang dipersamakan *)</td>
                <td><center>@if($dataKal['cek_sk_kepengurusan']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_sk_kepengurusan']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['ket_sk_kepengurusan'] !!}</td>
            </tr>
            <tr>
                <td><center>10</center></td>
                <td style="padding-left: 10px;">Bantuan yang pernah diterima tahun sebelumnya apabila ada (tanda terima laporan pertanggungjawaban) *)</td>
                <td><center>@if($dataKal['cek_bantuan_tahun_sebelum']==1)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td><center>@if($dataKal['cek_bantuan_tahun_sebelum']==2)<i class="fa fa-check-square-o" aria-hidden="true"></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_bantuan_tahun_sebelum'] !!}</td>
            </tr> 
          
            
        </table>
    <br>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian laporan ini dibuat dengan sebenarnya dan agar dapat dipergunakan sebagaimana mestinya.</p>
    <p>Catatan:<br>*disesuikan dengan status/kedudukan kelembagaan Pemohon.</p>
    <table width="100%">
        <tr>
            <td width="30%">
                <center>
                <h4>PENGUSUL HIBAH</h4>
                <br><br><br>
                <h4><input type="text" name="nama_pengusul" value="{{$dataCpl['nama_pengusul']}}"></h4>
                <h4>&nbsp;</h4>
                </center>
            </td>
            <td width="30%">
                <center>
                    {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                    <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                </center>
            </td>
            <td><center>
                <h4>KETUA TIM EVALUASI</h4>
                <br><br><br>
                <h4><input type="text" name="nama_ketua" value="{{$dataCpl['nama_ketua']}}"></h4>
                <h4>NIP <input type="text" name="nip" value="{{$dataCpl['nip']}}"></h4>
                </center>
            </td>
        </tr>
    </table>
    <hr>
    {{-- @if($dataCpl['is_generate']==0) --}}
    <button type="submit" class="btn green" id="btn-simpan-lapangan">Simpan</button>
    {{-- @endif --}}
</form>

<form class="btn-pdf-lapangan" action="{{ route('skpd.cetak.to.pdf.peninjauan.lapangan') }}" method="POST" style="float: right;">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
    <input type="hidden" name="fase" value="{{ $fase }}">
    <input type="hidden" name="tab" value="{{ $tab }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    @if($dataCpl['id'] != "")
    <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
    @endif
</form>
</div>