@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Verifikasi Berkas Pencairan</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{dd($recVerifikasiBerkasPencairanByNoProp['id'])}} --}}
        
        <a href="{{route('skpd.proposal.show',['id'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase])}}" class="btn btn-default" style="float: right;">Kembali</a><br><br>
        
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-check" aria-hidden="true"></i> Pengaturan Verifikasi Berkas Pencairan </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form action="{{ route('skpd.proposal.verifikasi.berkas.pencairan.store') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                            <input type="hidden" name="fase" value="{{ $fase }}">
                            <center>
                                <h4><b>CEK LIST KELENGKAPAN BERKAS PERMOHONAN</b></h4>
                                <h4><b>PENCAIRAN {{ $recProposalByID->kelompok }}</b></h4>
                            </center>
                            <hr>
                            <center>
                            	<table width="80%">
                            		<tr>
                            			<td width="211" style="padding-top: 10px;">Tahun Anggaran</td>
                            			<td width="13" style="padding-top: 10px;">:</td>
                            			<td style="padding-top: 10px;"><input type="text" class="form-control" name="tahun_anggaran" readonly="" value="{{ $recProposalByID->tahun_anggaran }}"></td>
                            		</tr>
                            		<tr>
                            			<td style="padding-top: 10px;">No SK Gub Penetapan</td>
                            			<td style="padding-top: 10px;">:</td>
                            			<td style="padding-top: 10px;"><input type="text" class="form-control" name="no_sk_gub_penetapan" value="{{ $recVerifikasiBerkasPencairanByNoProp['no_sk_gub_penetapan'] }}"></td>
                            		</tr>
                            		<tr>
                            			<td style="padding-top: 10px;">Kelompok Belanja PPKD</td>
                            			<td style="padding-top: 10px;">:</td>
                            			<td style="padding-top: 10px;"><input type="text" class="form-control" name="kelompok_belanja" readonly="" value="{{ $recProposalByID->kelompok }}"></td>
                            		</tr>
                            		<tr>
                            			<td style="padding-top: 10px;">No Urut Pemohon SK Gub</td>
                            			<td style="padding-top: 10px;">:</td>
                            			<td style="padding-top: 10px;"><input type="text" class="form-control" name="no_urut_pemohon_sk" value="{{ $recVerifikasiBerkasPencairanByNoProp['no_urut_pemohon_sk'] }}"></td>
                            		</tr>
                            		<tr>
                            			<td style="padding-top: 10px;">Nama Pemohon</td>
                            			<td style="padding-top: 10px;">:</td>
                            			<td style="padding-top: 10px;"><input type="text" class="form-control" name="nama_pemohon" readonly="" value="{{ strtoupper($recProposalByID->nm_lembaga) }}"></td>
                            		</tr>
                            		<tr>
                            			<td style="padding-top: 10px;">SKPD/UKPD</td>
                            			<td style="padding-top: 10px;">:</td>
                            			<td style="padding-top: 10px;"><input type="text" class="form-control" name="nm_skpd" readonly="" value="{{ strtoupper($recProposalByID->nm_skpd) }}"></td>
                            		</tr>
                            	</table>
                            </center>

                            <center style="margin-top: 50px;">
                            	<table width="100%" cellpadding="0" cellspacing="0" border="2">
                            		<thead style="background: #F1F1F1;">
                            			<tr>
                            				<th rowspan="2"><center>NO</center></th>
                            				<th rowspan="2"><center>KELENGKAPAN DOKUMEN YANG DIPERLUKAN</center></th>
                            				<th colspan="2"><center>DOKUMEN</center></th>
                            				<th rowspan="2"><center>KETERANGAN</center></th>
                            			</tr>
                            			<tr>
                            				<th><center>Ada</center></th>
                            				<th><center>Tidak</center></th>
                            			</tr>
                            		</thead>
                            		<tbody>
                            			<!-- <tr>
                            				<td style="padding: 10px;"><center>1</center></td>
                            				<td style="padding: 10px;">SURAT PERMOHONAN PENCAIRAN DARI LEMBAGA (ASLI)<br><b style="color: red;">(DITUJUKAN KE GUBERNUR MELALUI SKPD/UKPD PEMBERI REKOMENDASI)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_pengantar1" name="cek_surat_pengantar" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_pengantar']==1)?'checked':'' }}>
	                                                <label for="cek_surat_pengantar1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_pengantar2" name="cek_surat_pengantar" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_pengantar']==0)?'checked':'' }}>
	                                                <label for="cek_surat_pengantar2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_surat_pengantar" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_surat_pengantar'] }}</textarea></center>
                            				</td>
                            			</tr> -->
                                        
                                        <!-- here -->
                            			<tr>
                            				<td style="padding: 10px;"><center>1</center></td>
                            				<td style="padding: 10px;">SURAT PERMOHONAN PENCAIRAN DARI LEMBAGA (ASLI)<br><b style="color: red;"> (DITUJUKAN KE GUBERNUR MELALUI SKPD/UKPD PEMBERI REKOMENDASI)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_permohonan1" name="cek_surat_permohonan" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_permohonan']==1)?'checked':'' }}>
	                                                <label for="cek_surat_permohonan1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_permohonan2" name="cek_surat_permohonan" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_permohonan']==0)?'checked':'' }}>
	                                                <label for="cek_surat_permohonan2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_surat_permohonan" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_surat_permohonan'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>2</center></td>
                            				<td style="padding: 10px;">RAB TERPISAH DARI PROPOSAL SESUAI NOMINAL DI SK  (copy)<br><b style="color: red;"> (DI TTD KETUA DAN BENDAHARA)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_rab1" name="cek_rab" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_rab']==1)?'checked':'' }}>
	                                                <label for="cek_rab1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_rab2" name="cek_rab" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_rab']==0)?'checked':'' }}>
	                                                <label for="cek_rab2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_rab" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_rab'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>3</center></td>
                            				<td style="padding: 10px;">KWITANSI<br><b style="color: red;"> (BERMETERAI  DI TTD KETUA DAN BENDAHARA)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_kwitansi1" name="cek_kwitansi" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_kwitansi']==1)?'checked':'' }} >
	                                                <label for="cek_kwitansi1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_kwitansi2" name="cek_kwitansi" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_kwitansi']==0)?'checked':'' }}>
	                                                <label for="cek_kwitansi2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_kwitansi" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_kwitansi'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>4</center></td>
                            				<td style="padding: 10px;">FOTOCOPY REKENING KORAN BANK YANG MASIH AKTIF<br><b style="color: red;"> (ATAS NAMA LEMBAGA PENERIMA SESUAI SK GUB PENERIMA) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_rekening1" name="cek_rekening" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_rekening']==1)?'checked':'' }} >
	                                                <label for="cek_rekening1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_rekening2" name="cek_rekening" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_rekening']==0)?'checked':'' }}>
	                                                <label for="cek_rekening2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_rekening" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_rekening'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>5</center></td>
                            				<td style="padding: 10px;">FOTOCOPY SK YANG ADA NAMA LEMBAGA PENERIMA<br><b style="color: red;"> (SK GUB PENETAPAN PENERIMA BANTUAN)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_sk_lembaga1" name="cek_sk_lembaga" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_sk_lembaga']==1)?'checked':'' }} >
	                                                <label for="cek_sk_lembaga1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_sk_lembaga2" name="cek_sk_lembaga" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_sk_lembaga']==0)?'checked':'' }}>
	                                                <label for="cek_sk_lembaga2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_sk_lembaga" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_sk_lembaga'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			

                                        <tr>
                                            <td style="padding: 10px;"><center>6</center></td>
                                            <td style="padding: 10px;">FOTOCOPY KTP PENGURUS<br><b style="color: red;"> (MINIMAL FOTOCOPY KTP KETUA DAN BENDAHARA) </b></td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_ktp_pengurus1" name="cek_ktp_pengurus" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_ktp_pengurus']==1)?'checked':'' }} >
                                                    <label for="cek_ktp_pengurus1">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_ktp_pengurus2" name="cek_ktp_pengurus" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_ktp_pengurus']==0)?'checked':'' }}>
                                                    <label for="cek_ktp_pengurus2">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_ktp_pengurus" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_ktp_pengurus'] }}</textarea></center>
                                            </td>
                                        </tr>

                                         <tr>
                                            <td style="padding: 10px;"><center>7</center></td>
                                            <td style="padding: 10px;">NASKAH PERJANJIAN HIBAH DAERAH <br><b style="color: red;"> (BERMETERAI SERTA DI TTD KETUA LEMBAGA DAN KEPALA SKPD/UKPD PEMBERI REKOMENDASI) </b></td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_perjanjian_hibah_daerah" name="cek_perjanjian_hibah_daerah" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_perjanjian_hibah_daerah']==1)?'checked':'' }} >
                                                    <label for="cek_perjanjian_hibah_daerah">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_perjanjian_hibah_daerah2" name="cek_perjanjian_hibah_daerah" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_perjanjian_hibah_daerah']==0)?'checked':'' }}>
                                                    <label for="cek_perjanjian_hibah_daerah">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_perjanjian_hibah_daerah" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_perjanjian_hibah_daerah'] }}</textarea></center>
                                            </td>
                                        </tr>

                                         <tr>
                                            <td style="padding: 10px;"><center>8</center></td>
                                            <td style="padding: 10px;">PAKTA INTEGRITAS BERMATERAI CUKUP<br><b style="color: red;"> (DI TTD KETUA) </b></td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_pakta_integritas" name="cek_pakta_integritas" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_pakta_integritas']==1)?'checked':'' }} >
                                                    <label for="cek_pakta_integritas">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_pakta_integritas2" name="cek_pakta_integritas" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_pakta_integritas']==0)?'checked':'' }}>
                                                    <label for="cek_pakta_integritas">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_pakta_integritas" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_pakta_integritas'] }}</textarea></center>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="padding: 10px;"><center>9</center></td>
                                            <td style="padding: 10px;">PROPOSAL DEFINITIF<br><b style="color: red;"> (DI TTD KETUA) </b></td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_proposal_definitif1" name="cek_proposal_definitif" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_proposal_definitif']==1)?'checked':'' }} >
                                                    <label for="cek_proposal_definitif1">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_proposal_definitif2" name="cek_proposal_definitif" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_proposal_definitif']==0)?'checked':'' }}>
                                                    <label for="cek_proposal_definitif2">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_proposal_definitif" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_proposal_definitif'] }}</textarea></center>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="padding: 10px;"><center>10</center></td>
                                            <td style="padding: 10px;">LAPORAN PERTANGGUNGJAWABAN HIBAH/BANTUAN SOSIALYANG DITERIMA TAHUN SEBELUMNYA </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_tgjwb_hibah_sebelumnya1" name="cek_tgjwb_hibah_sebelumnya" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tgjwb_hibah_sebelumnya']==1)?'checked':'' }} >
                                                    <label for="cek_tgjwb_hibah_sebelumnya1">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_tgjwb_hibah_sebelumnya2" name="cek_tgjwb_hibah_sebelumnya" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tgjwb_hibah_sebelumnya']==0)?'checked':'' }}>
                                                    <label for="cek_tgjwb_hibah_sebelumnya2">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_tgjwb_hibah_sebelumnya" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_tgjwb_hibah_sebelumnya'] }}</textarea></center>
                                            </td>
                                        </tr>

                                          <tr>
                                            <td style="padding: 10px;"><center>11</center></td>
                                            <td style="padding: 10px;">DOKUMEN ADMINISTRASI LAINNYA SESUAI KETENTUAN PERATURAN PERUNDANG-UNDANGAN</td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_dok_lain" name="cek_dok_lain" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_dok_lain']==1)?'checked':'' }} >
                                                    <label for="cek_dok_lain">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_dok_lain2" name="cek_dok_lain" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_dok_lain']==0)?'checked':'' }}>
                                                    <label for="cek_dok_lain2">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_dok_lain" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_dok_lain'] }}</textarea></center>
                                            </td>
                                        </tr>

                                        <!-- end here -->
                                     <!-- 
                                        <tr>
                                            <td style="padding: 10px;"><center>7</center></td>
                                            <td style="padding: 10px;">REKOMENDASI<br><b style="color: red;"> (DITUJUKAN KE GUBERNUR MELALUI TAPD) </b></td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_rekomendasi1" name="cek_rekomendasi" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_rekomendasi']==1)?'checked':'' }} >
                                                    <label for="cek_rekomendasi1">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <div class="md-radio">
                                                    <input type="radio" id="cek_rekomendasi2" name="cek_rekomendasi" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_rekomendasi']==0)?'checked':'' }}>
                                                    <label for="cek_rekomendasi2">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> 
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="padding: 10px;">
                                                <center><textarea name="ket_rekomendasi" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_rekomendasi'] }}</textarea></center>
                                            </td>
                                        </tr>

                            			<tr>
                            				<td style="padding: 10px;"><center>8</center></td>
                            				<td style="padding: 10px;">BERITA ACARA KELENGKAPAN ADMINISTRASI<br><b style="color: red;"> (DI TTD KETUA TIM EVALUASI) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bak_administrasi1" name="cek_bak_administrasi" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bak_administrasi']==1)?'checked':'' }} >
	                                                <label for="cek_bak_administrasi1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bak_administrasi2" name="cek_bak_administrasi" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bak_administrasi']==0)?'checked':'' }}>
	                                                <label for="cek_bak_administrasi2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_bak_administrasi" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_bak_administrasi'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>9</center></td>
                            				<td style="padding: 10px;">BERITA ACARA PENINJAUAN LAPANGAN<br><b style="color: red;"> (DI TTD KETUA TIM EVALUASI DAN PENGUSUL HIBAH) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bap_lapangan1" name="cek_bap_lapangan" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bap_lapangan']==1)?'checked':'' }} >
	                                                <label for="cek_bap_lapangan1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bap_lapangan2" name="cek_bap_lapangan" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bap_lapangan']==0)?'checked':'' }}>
	                                                <label for="cek_bap_lapangan2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_bap_lapangan" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_bap_lapangan'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>10</center></td>
                            				<td style="padding: 10px;">FOTOCOPY SURAT TUGAS TIM<br><b style="color: red;"> (DI TTD KEPALA SKPD PEMBERI REKOMENDASI) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_tugas1" name="cek_surat_tugas" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_tugas']==1)?'checked':'' }} >
	                                                <label for="cek_surat_tugas1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_tugas2" name="cek_surat_tugas" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_tugas']==0)?'checked':'' }}>
	                                                <label for="cek_surat_tugas2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_surat_tugas" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_surat_tugas'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>11</center></td>
                            				<td style="padding: 10px;">SURAT PERNYATAAN TANGGUNGJAWAB MUTLAK DAN FAKTA INTEGRITAS<br><b style="color: red;"> (DI TTD OLEH KETUA LEMBAGA DAN BERMATERAI) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_pernyataan_tanggungjawab1" name="cek_surat_pernyataan_tanggungjawab" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_pernyataan_tanggungjawab']==1)?'checked':'' }} >
	                                                <label for="cek_surat_pernyataan_tanggungjawab1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_surat_pernyataan_tanggungjawab2" name="cek_surat_pernyataan_tanggungjawab" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_pernyataan_tanggungjawab']==0)?'checked':'' }}>
	                                                <label for="cek_surat_pernyataan_tanggungjawab2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_surat_pernyataan_tanggungjawab" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_surat_pernyataan_tanggungjawab'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			
                            			<tr>
                            				<td style="padding: 10px;"><center>13</center></td>
                            				<td style="padding: 10px;">FOTOCOPY SURAT KETERANGAN DOMISILI<br><b style="color: red;"> (ALAMAT SESUAI DENGAN KOP SURAT/AKTA PENDIRIAN) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_skd1" name="cek_skd" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_skd']==1)?'checked':'' }} >
	                                                <label for="cek_skd1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_skd2" name="cek_skd" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_skd']==0)?'checked':'' }}>
	                                                <label for="cek_skd2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_skd" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_skd'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>14</center></td>
                            				<td style="padding: 10px;">FOTOCOPY NOMOR POKOK WAJIB PAJAK (NPWP)<br><b style="color: red;"> (ATAS NAMA LEMBAGA PENERIMA SESUAI SK GUB PENERIMA) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_npwp1" name="cek_npwp" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_npwp']==1)?'checked':'' }} >
	                                                <label for="cek_npwp1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_npwp2" name="cek_npwp" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_npwp']==0)?'checked':'' }}>
	                                                <label for="cek_npwp2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_npwp" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_npwp'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			
                            			@if ( $recProposalByID->kelompok=="BANTUAN HIBAH" )
                            			<tr>
                            				<td style="padding: 10px;"><center>16</center></td>
                            				<td style="padding: 10px;">NASKAH PERJANJIAN HIBAH DAERAH (NPHD) (ASLI)<br><b style="color: red;"> (DUA RANGKAP, BERMATERAI 1 DI BENDAHARA UMUM, 1 DI KETUA LEMBAGA) </b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_nphd1" name="cek_nphd" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_nphd']==1)?'checked':'' }} >
	                                                <label for="cek_nphd1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_nphd2" name="cek_nphd" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_nphd']==0)?'checked':'' }}>
	                                                <label for="cek_nphd2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_nphd" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_nphd'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>17</center></td>
                            				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA LAPORAN PERTANGGUNG JAWABAN <b style="color: red;">BANTUAN HIBAH</b> YANG DITERIMA SEBELUMNYA</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_laporan1" name="cek_tanda_terima_laporan" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==1)?'checked':'' }} >
	                                                <label for="cek_tanda_terima_laporan1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_laporan2" name="cek_tanda_terima_laporan" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==0)?'checked':'' }}>
	                                                <label for="cek_tanda_terima_laporan2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_tanda_terima_laporan" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_laporan'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>18</center></td>
                            				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA AUDIT <b style="color: red;">BANTUAN HIBAH</b> YANG DITERIMA SEBELUMNYA <br><b style="color: red;">(JIKA DANA YANG DITERIMA DIATAS 200 JUTA)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_audit1" name="cek_tanda_terima_audit" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==1)?'checked':'' }} >
	                                                <label for="cek_tanda_terima_audit1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_audit2" name="cek_tanda_terima_audit" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==0)?'checked':'' }}>
	                                                <label for="cek_tanda_terima_audit2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_tanda_terima_audit" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_audit'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>19</center></td>
                            				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA LAPORAN MONEV <b style="color: red;">BANTUAN HIBAH</b> YANG DITERIMA SEBELUMNYA</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_monev1" name="cek_tanda_terima_monev" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_monev']==1)?'checked':'' }} >
	                                                <label for="cek_tanda_terima_monev1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_monev2" name="cek_tanda_terima_monev" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_monev']==0)?'checked':'' }}>
	                                                <label for="cek_tanda_terima_monev2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_tanda_terima_monev" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_monev'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>20</center></td>
                            				<td style="padding: 10px;">BUKTI EMAIL VERIFIKASI INPUT KE SISTEM</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bukti_email1" name="cek_bukti_email" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==1)?'checked':'' }} >
	                                                <label for="cek_bukti_email1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bukti_email2" name="cek_bukti_email" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==0)?'checked':'' }}>
	                                                <label for="cek_bukti_email2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_bukti_email" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_bukti_email'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			@else
                            			<tr>
                            				<td style="padding: 10px;"><center>16</center></td>
                            				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA LAPORAN PERTANGGUNG JAWABAN <b style="color: red;">BANTUAN SOSIAL</b> YANG DITERIMA SEBELUMNYA</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_laporan1" name="cek_tanda_terima_laporan" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==1)?'checked':'' }} >
	                                                <label for="cek_tanda_terima_laporan1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_laporan2" name="cek_tanda_terima_laporan" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==0)?'checked':'' }}>
	                                                <label for="cek_tanda_terima_laporan2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_tanda_terima_laporan" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_laporan'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>17</center></td>
                            				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA AUDIT <b style="color: red;">BANTUAN SOSIAL</b> YANG DITERIMA SEBELUMNYA <br><b style="color: red;">(JIKA DANA YANG DITERIMA DIATAS 200 JUTA)</b></td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_audit1" name="cek_tanda_terima_audit" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==1)?'checked':'' }} >
	                                                <label for="cek_tanda_terima_audit1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_tanda_terima_audit2" name="cek_tanda_terima_audit" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==0)?'checked':'' }}>
	                                                <label for="cek_tanda_terima_audit2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_tanda_terima_audit" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_audit'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			<tr>
                            				<td style="padding: 10px;"><center>18</center></td>
                            				<td style="padding: 10px;">BUKTI EMAIL VERIFIKASI INPUT KE SISTEM</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bukti_email1" name="cek_bukti_email" value="1" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==1)?'checked':'' }} >
	                                                <label for="cek_bukti_email1">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<div class="md-radio">
	                                                <input type="radio" id="cek_bukti_email2" name="cek_bukti_email" value="0" class="md-radiobtn" {{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==0)?'checked':'' }}>
	                                                <label for="cek_bukti_email2">
	                                                    <span class="inc"></span>
	                                                    <span class="check"></span>
	                                                    <span class="box"></span> 
	                                                </label>
	                                            </div>
                            				</td>
                            				<td style="padding: 10px;">
                            					<center><textarea name="ket_bukti_email" rows="3">{{ $recVerifikasiBerkasPencairanByNoProp['ket_bukti_email'] }}</textarea></center>
                            				</td>
                            			</tr>
                            			@endif -->

                            			
                            		</tbody>
                            	</table>
                            </center>
                            <br>
                            <br>
                        	<table width="100%">
                                <tr>
                                    <td width="30%">
                                    	<center>
                                            <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" />
                                        </center>
                                    </td>
                                    <td width="30%"></td>
                                    <td><center>
                                        <h4><input type="text" name="plt" value="{{ $recVerifikasiBerkasPencairanByNoProp['plt'] }}" style="width: 50px;"> {{ strtoupper($recProposalByID->nm_skpd) }}</h4>
                                        <br><br><br>
                                        <h4><input type="text" name="nama_ketua" value="{{ $recVerifikasiBerkasPencairanByNoProp['nama_ketua'] }}"></h4>
                                        <h4>NIP <input type="text" name="nip" value="{{ $recVerifikasiBerkasPencairanByNoProp['nip'] }}"></h4>
                                        </center>
                                    </td>
                                </tr>
                            </table>
                            <hr>
                            @if($recVerifikasiBerkasPencairanByNoProp['is_generate'] == NULL || $recVerifikasiBerkasPencairanByNoProp['is_generate'] == 0)
                            <button type="submit" class="btn green" id="btn-simpan-administrasi">Simpan</button>
                            @endif
                        </form>

                        <form class="btn-unduh-pdf" action="{{ route('skpd.proposal.verifikasi.berkas.pencairan.generateToPDF') }}" method="POST" style="float: right;">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                            <input type="hidden" name="fase" value="{{ $fase }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            @if($recVerifikasiBerkasPencairanByNoProp['id'] != "")
                            <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Unduh PDF</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif

    <script type="text/javascript">
    	$(function(){
    		$(".btn-unduh-pdf").on("submit", function(){
	            return confirm("Apakah Anda yakin ? Jika 'OK' Anda juga menyetujui data cetak KELENGKAPAN BERKAS PERMOHONAN PENCAIRAN BANTUAN HIBAH tidak dapat di update kembali. ");
	        });
    	});
    </script>
@stop