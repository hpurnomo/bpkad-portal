@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.user.management.manage') }}">User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <ol>
                        <li>Data dibawah ini merupakan data User yang telah ter-filter berdasarkan skpd masing-masing. Untuk mem-Verifikasi User, klik <em>"Nama User"</em> pada kolom Nama.</li>
                    </ol>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('skpd.user.management.create') }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat User Lembaga Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column display responsive nowrap" id="sample_3">
                            <thead>
                                <tr>
                                    <th> No. </th>
                                    <th> No Registrasi </th>
                                    <th> Nama </th>
                                    <th> Nama Lembaga </th>
                                    <th> Username </th>
                                    <th> E-Mail </th>
                                    <th> NRK </th>
                                    <th> Verified </th>
                                    <th> Active </th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recUserBySkpdID as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    <td> {{ $value->no_registrasi }} </td>
                                    <td> 
                                        @if($value->group_id==12)
                                        <a href="{{ route('skpd.user.management.show',['userID'=>base64_encode($value->user_id)]) }}" class="btn btn-circle blue btn-outline sbold">{{ $value->real_name }} </a>
                                        @else
                                        {{ $value->real_name }} 
                                        @endif
                                    </td>
                                    <td> {{ ucwords(strtolower($value->nm_lembaga)) }} </td>
                                    <td> {{ $value->username }} </td>
                                    <td> {{ $value->email }} </td>
                                    <td> {{ $value->nrk }} </td>
                                    <td>
                                    	{!! ($value->is_verify==1)?'<span class="label label-sm label-success"> Yes </span>':'<span class="label label-sm label-danger"> No </span>' !!}
                                    </td>
                                    <td>
                                        {!! ($value->is_active==1)?'<span class="label label-sm label-success"> Yes </span>':'<span class="label label-sm label-danger"> No </span>' !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop