@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.user.management.manage') }}">User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Detil User Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <div class="portlet-body form">
            <form action="{{ route('skpd.user.management.change.status') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
            {{ csrf_field() }}
                <input type="hidden" name="userID" value="{{ $recUserID }}">
                <input type="hidden" name="no_lembaga" value="{{ $recUserLembagaByNoLembaga->no_lembaga }}">
                <div class="form-group form-md-radios">
                    <label class="col-md-3 control-label" for="form_control_1">Ubah Status User: </label>
                    <div class="col-md-9">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_8" name="is_verify" value="1" class="md-radiobtn" {{ ($recUserByID->is_verify==1)?'checked':'' }}>
                                <label for="checkbox1_8">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Diverifikasi </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_9" name="is_verify" value="0" class="md-radiobtn" {{ ($recUserByID->is_verify==0)?'checked':'' }}>
                                <label for="checkbox1_9">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Belum Diverifikasi </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <hr>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption" style="width: 95%;">
                    <i class="icon-eye"></i>Detil User 
                    <a href="{{ route('skpd.user.management.edit',['id'=>base64_encode($recUserByID->user_id)])}}" class="btn btn-circle btn-success" style="box-shadow: 2px 2px 5px #88898A !important;float:right;background: #0B8E31;"><i class="icon-pencil"></i> Perbaiki Detil User Lembaga</a>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        <!-- <h3 class="form-section">Detail Lembaga</h3> -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Username:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->username }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>No Registrasi:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->no_registrasi }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>E-Mail:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->email }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>No KTP:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->ktp }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Nama Lengkap:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->real_name }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Foto:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        <img src="{{ url('/') }}/foto_user/avatar/{{ $recUserByID->avatar }}" width="50">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>No Telp:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->telp }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Alamat:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">{{ $recUserByID->address }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                </form>
                <!-- END FORM-->
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align:right;">
                    {{--
                    <form class="delete" action="{{ route('skpd.user.management.delete.user') }}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="userID" value="{{$recUserByID->user_id}}">
                        <button type="submit" class="btn red-mint" value="Delete"><i class="icon-trash"></i> Hapus User Ini</button>
                    </form>
                    --}}

                    @if($recUserByID->is_active==1)
                    <a href="{{ route('skpd.user.management.nonaktif.user',base64_encode($recUserByID->user_id)) }}" onClick='confirm("Anda yakin untuk nonaktifkan user ini ?")' class="btn red-mint" ><i class="icon-trash"></i> Nonaktikan User Ini</a>
                    @elseif($recUserByID->is_active==0)
                     <a href="{{ route('skpd.user.management.nonaktif.user',base64_encode($recUserByID->user_id)) }}" onClick='confirm("Anda yakin untuk mengaktifkan user ini ?")' class="btn default" ><i class="icon-trash"></i> Aktifkan User Ini</a>

                    @endif
                </div>
            </div>
        </div>

        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-eye"></i>Info Lembaga </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    @if($recUserByID->fotoLembaga==NULL)
                                    <img src="{{url('/')}}/assets/images/default-logo-lembaga.jpg" class="img-responsive" width="140">
                                    @else
                                    <img src="{{url('/')}}/foto_lembaga/{{ $recUserByID->fotoLembaga }}" class="img-responsive" width="140" style="border: 1px solid #c3c3c3;border-radius: 10px !important;padding: 10px;">
                                    @endif
                                </div>
                                <div class="col-md-10">
                                    <h3 style="height: 140px;padding-top: 33px;font-weight: bolder;">{{ $recUserByID->nm_lembaga }}</h3>
                                </div>
                            </div>
                        </div>
                        <h3 class="form-section">Data Utama</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Alamat:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->alamatLembaga }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>NPWP:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->npwpLembaga }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>RT/RW:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->rtNama }}/{{$recUserByID->rwNama}} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Scan NPWP:</b></label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            @if(!empty($recUserLembagaByNoLembaga->npwp))
                                            <a href="{{ url('/') }}/user_lembaga/{{ $recUserLembagaByNoLembaga->npwp }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->npwp,20) }} <i class="fa fa-external-link"></i></a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Kelurahan:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->nmkel }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>EMail:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->email }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Kecamatan:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->nmkec }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>No Telp:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->no_telepon }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>Kota:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->nmkota }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><b>No Fax:</b></label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{ $recUserByID->no_fax }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <h3 class="form-section">Data Khusus</h3>
                        
                        <h4><u>Akta Kumham</u></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nomor:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_akta }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Tanggal:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->tgl_akta }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{!! ($recUserLembagaByNoLembaga->cek_akta==1)?'<i class="fa fa-check" style="color:green;"></i>':'' !!} <b>Scan Dokumen:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserLembagaByNoLembaga->akta))
                                        <a href="{{ url('/') }}/user_lembaga/{{ $recUserLembagaByNoLembaga->akta }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->akta,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h4><u>Sertifikat</u></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nomor:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_sertifikat }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Tanggal:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->tgl_sertifikat }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{!! ($recUserLembagaByNoLembaga->cek_sertifikat==1)?'<i class="fa fa-check" style="color:green;"></i>':'' !!} <b>Scan Dokumen:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserLembagaByNoLembaga->sertifikat))
                                        <a href="{{ url('/') }}/user_lembaga/{{ $recUserLembagaByNoLembaga->sertifikat }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->sertifikat,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h4><u>Surat Domisili</u></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nomor:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_surat_domisili }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Tanggal:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->tgl_surat_domisili }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{!! ($recUserLembagaByNoLembaga->cek_skd==1)?'<i class="fa fa-check" style="color:green;"></i>':'' !!} <b>Scan Dokumen:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserLembagaByNoLembaga->skd))
                                        <a href="{{ url('/') }}/user_lembaga/{{ $recUserLembagaByNoLembaga->skd }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->skd,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h4><u>Izin Operasional</u></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nomor:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_izin_operasional }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Tanggal:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->tgl_izin_operasional }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{!! ($recUserLembagaByNoLembaga->cek_izin_operasional==1)?'<i class="fa fa-check" style="color:green;"></i>':'' !!} <b>Scan Dokumen:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserLembagaByNoLembaga->izin_operasional))
                                        <a href="{{ url('/') }}/user_lembaga/{{ $recUserLembagaByNoLembaga->izin_operasional }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->izin_operasional,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h3 class="form-section">Rekening Lembaga</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nama Bank:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->nama_bank }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Atas Nama:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->pemilik_rek }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>No Rekening:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_rek }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Scan Rekening:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserLembagaByNoLembaga->rekening_lembaga))
                                        <a href="{{ url('/') }}/berkas_rekening/{{ $recUserLembagaByNoLembaga->rekening_lembaga }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->rekening_lembaga,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h3 class="form-section">Kontak Pengurus</h3>
                        <h4><u>Ketua</u></h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nama:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->kontak_person }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>NIK:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->nik_person }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Scan KTP:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserLembagaByNoLembaga->ktp_ketua))
                                        <a href="{{ url('/') }}/user_lembaga/{{ $recUserLembagaByNoLembaga->ktp_ketua }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserLembagaByNoLembaga->ktp_ketua,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Alamat:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->alamat_person }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>No Telp/HP:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_hp_person }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>EMail:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->email_person }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Foto:</b></label>
                                    <div class="col-md-8">
                                        @if(!empty($recUserByID->foto_person))
                                        <a href="{{ url('/') }}/foto_ketua_lembaga/{{ $recUserByID->foto_person }}" class="btn btn-circle blue btn-outline" target="_blank">{{ str_limit($recUserByID->foto_person,20) }} <i class="fa fa-external-link"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <h4><u>Bendahara</u></h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nama:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->kontak_person_bendahara }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>EMail:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->email_person_bendahara }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Alamat:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->alamat_person_bendahara }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>No Telp/HP:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_hp_person_bendahara }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <h4><u>Sekretaris</u></h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Nama:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->kontak_person_sekretaris }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>EMail:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->email_person_sekretaris }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Alamat:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->alamat_person_sekretaris }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>No Telp/HP:</b></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> {{ $recUserByID->no_hp_person_sekretaris }} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            // autosubmit
            $('#checkbox1_8,#checkbox1_9').click(function(){
            	$('form#form_sample_1').submit();
            });
        });
        $(function(){
            $(".delete").on("submit", function(){
                return confirm("Anda yakin menghapus user ini?");
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop