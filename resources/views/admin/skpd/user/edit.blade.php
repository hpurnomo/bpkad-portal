@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.user.management.manage') }}">User Management</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.user.management.show',['id'=>base64_encode($recUserByID->user_id)])}}">Info User Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                	<span>Perbaiki User Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Perbaiki User Lembaga</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    	@include('admin.part.alert')
                        <form action="{{ route('skpd.user.management.update') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                {!! csrf_field() !!}
                                <input name="user_id" type="hidden" value="{{ $recUserByID->user_id }}" readonly>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Lengkap
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="real_name" type="text" data-required="1" class="form-control" value="{{ $recUserByID->real_name }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Username
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="username" type="text" id="username" class="form-control" value="{{ $recUserByID->username }}"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input name="email" type="text" class="form-control" value="{{ $recUserByID->email }}"> </div>
                                </div>
                                <div class="form-group">
                                     <label class="control-label col-md-3">Password
                                    </label>
                                    <div class="col-md-4">
                                        <input name="password" type="password" class="form-control"> </div> 
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Ubah</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });


            $( "input#username" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^a-z0-9-]/gi, '_').replace(/-+/g, '_').replace(/^-|-$/g, '').toLowerCase();
              });
            });

            // autosubmit
            $('#checkbox1_8,#checkbox1_9').click(function(){
                $('form#form_sample_1').submit();
            });
        });
        $(function(){
            $(".delete").on("submit", function(){
                return confirm("Anda yakin menghapus user ini?");
            });
        });
        $(function(){
            $('#no_lembaga').on('change',function(){
                console.log($(this).val());
                $.ajax({
                    type: 'get',
                    url: '{{ route('ajax.alamat.lembaga') }}',
                    data: 'nomor='+$(this).val(),
                    success: function(html){
                        // document.getElementById('alamat').innerHTML = html;
                        $('#alamat').val(html);
                    }
                });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif
@stop