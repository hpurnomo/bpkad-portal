@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <ol>
                        <li>Data dibawah ini merupakan data Lembaga yang telah ter-filter berdasarkan skpd masing-masing. Klik <em>"Nama Lembaga"</em> untuk pengaturan.</li>
                        <li>Tipe Lembaga dibagi menjadi 2 kategori :
                            <ul>
                                <li>
                                    Tempat Ibadah, Majelis Taklim, dan Taman/ Tempat Pendidikan Al-Quran.
                                </li>
                                <li>Lembaga Lainnya.</li>
                            </ul>
                        </li>
                        <li>
                            SKPD/UKPD diwajibkan melakukan verifikasi penelitian administrasi dan peninjauan lapangan dengan cermat dan akurat. Ketentuan ini tertuang dalam Pergub No.142 Tahun 2018 Pasal 9 ayat 1-11. 
                        </li>
                        <li>
                            Pergub No.142 Tahun 2018 Pasal 9 ayat 4 : Dalam melakukan penelitian administrasi dan peninjauan lapangan terhadap usulan Hibah sebagaimana dimaksud pada ayat (2), tim evaluasi dapat melakukan koordinasi dan/atau mengikutsertakan unsur SKPD/UKPD terkait lainnya.
                        </li>
                    </ol>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group">
                                        <a href="{{ route('skpd.lembaga.create') }}" class="btn btn-circle sbold green" style="box-shadow: 2px 2px 5px #88898A !important;"> Buat Lembaga Baru
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        <a href="{{ route('skpd.lembaga.rekapitulasi.checklist') }}" class="btn btn-circle sbold blue" style="box-shadow: 2px 2px 5px #88898A !important; margin-left: 10px;"> Lihat Rekapitulasi Checklist Lembaga
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('skpd.lembaga.download.rekapitulasi.checklist') }}" class="btn btn-circle sbold red" style="box-shadow: 2px 2px 5px #88898A !important; margin-left: 10px;"> Download Rekapitulasi Checklist Lembaga
                                            <i class="fa fa-download"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- {{dd($recByKdSkpd)}} --}}
                        <table class="table table-striped table-bordered table-hover table-checkable order-column display responsive nowrap" id="sample_3">
                            <thead>
                                <tr>
                                    <th> No </th>
                                    <th> Nama Lembaga </th>
                                    <th> Kode Lembaga </th>
                                    <th> Tipe Lembaga </th>
                                    <th> Nama SKPD </th>
                                    <th> NPWP </th>
                                    <th> Alamat </th>
                                    <th> Tgl Input </th>
                                    <th> Tgl Verified </th>
                                    <th> Verified </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($recByKdSkpd as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    <td><a href="{{ route('skpd.lembaga.show',['noLembaga'=>$value->nomor]) }}"> {{ ucwords(strtolower($value->nm_lembaga)) }} </a></td>
                                    <td> {{ $value->kd_skpd }}.{{ $value->nomor }} </td>
                                    <td> 
                                        {{ str_limit($value->tipeLembaga, $limit = 12, $end = '...') }}
                                    </td>
                                    <td> {{ strtoupper($value->nm_skpd) }} </td>
                                    <td> {{ $value->npwp }} </td>
                                    <td> {{ ucwords(strtolower($value->alamat)) }} </td>
                                    <td> {{ date('d-M-Y',strtotime($value->crd)) }} </td>
                                    <td> {{ ($value->crd==NULL)?'-':date('d-M-Y',strtotime($value->crd)) }} </td>
                                    <td>
                                        {!! ($value->is_verify==1)?'<span class="label label-sm label-success"> Yes </span>':'<span class="label label-sm label-danger"> No </span>' !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
@stop