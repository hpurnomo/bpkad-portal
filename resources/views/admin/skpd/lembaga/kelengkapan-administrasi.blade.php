<div class="tab-pane fade" id="tab_1_1">
    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
    <form action="{{ route('skpd.lembaga.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
    <div class="form-group">
        <div class="form-body">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="{{ $recUserLembagaByNoLembaga->id }}">
            <input type="hidden" name="no_lembaga" value="{{ $recUserLembagaByNoLembaga->no_lembaga }}">
            <input type="hidden" name="email" value="{{ $recLembagaByNomor->email }}">
            <input type="hidden" name="nm_lembaga" value="{{ $recLembagaByNomor->nm_lembaga }}">
            <input type="hidden" name="nm_skpd" value="{{ $recLembagaByNomor->nm_skpd }}">
            <div class="form-group form-md-radios">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="50%"><center>Data Administrasi</center></th>
                                <th><center>Ada</center></th>
                                <th><center>Tidak Ada</center></th>
                                <th><center>Keterangan</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b>A</b></td>
                                <td colspan="4"><b>Kelengkapan proposal usulan hibah/bansos:</b></td> 
                            </tr>  
                             <tr>
                                <td>1</td>
                                <td>Fotokopi Kartu Tanda penduduk (KTP) Ketua/ pimpinan badan, lembaga atau organisasi kemasyarakatan *)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_ktp_ketua1" name="cek_ktp_ketua" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_ktp_ketua==1)?'checked':'' }}>
                                        <label for="cek_ktp_ketua1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_ktp_ketua2" name="cek_ktp_ketua" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_ktp_ketua==2)?'checked':'' }}>
                                        <label for="cek_ktp_ketua2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_ktp_ketua" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_ktp_ketua }}</textarea></td>
                            </tr>

                              <tr>
                                <td>2</td>
                                <td>Fotokopi Akta Notaris Pendirian badan hukum yang telah mendapatkan pengesahan dari Kementrian yang membidangi hukum atau keputusan Gubernur tentang </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_akta1" name="cek_akta" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_akta==1)?'checked':'' }}>
                                        <label for="cek_akta1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_akta2" name="cek_akta" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_akta==2)?'checked':'' }}>
                                        <label for="cek_akta2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_akta" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_akta }}</textarea></td>
                            </tr>

                            
                             <tr>
                                <td>3</td>
                                <td>Fotokopi Nomor Pokok Wajib Pajak (NPWP) *) </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_npwp1" name="cek_npwp" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_npwp==1)?'checked':'' }}>
                                        <label for="cek_npwp1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_npwp2" name="cek_npwp" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_npwp==2)?'checked':'' }}>
                                        <label for="cek_npwp2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_npwp" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_npwp }}</textarea></td>
                            </tr>
                              <tr>
                                <td>4</td>
                                <td>Fotokopi Surat keterangan domisili organisasi kemasyarakatan dari kelurahan setempat atau sebutan lainnya *)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_skd1" name="cek_skd" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_skd==1)?'checked':'' }}>
                                        <label for="cek_skd1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_skd2" name="cek_skd" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_skd==2)?'checked':'' }}>
                                        <label for="cek_skd2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_skd" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_skd }}</textarea></td>
                            </tr>
                              <tr>
                                <td>5</td>
                                <td>Fotokopi izin operasional /tanda daftar lembaga dari instasi yang berwenang *)</span></td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_izin_operasional1" name="cek_izin_operasional" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_izin_operasional==1)?'checked':'' }}>
                                        <label for="cek_izin_operasional1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_izin_operasional2" name="cek_izin_operasional" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_izin_operasional==2)?'checked':'' }}>
                                        <label for="cek_izin_operasional2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_izin_operasional" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_izin_operasional }}</textarea></td>
                            </tr>


                            <tr>
                                <td>6</td>
                                <td>Fotokopi sertifikat tanah/bukti kepemilikan tanah atau bukti perjanjian sewa bangunan/gedung dengan jangka waktu sewa minimal 3 (tiga) tahun atau dokumen lain yang dipersamakan *)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_sertifikat1" name="cek_sertifikat" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_sertifikat==1)?'checked':'' }}>
                                        <label for="cek_sertifikat1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_sertifikat2" name="cek_sertifikat" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_sertifikat==2)?'checked':'' }}>
                                        <label for="cek_sertifikat2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_sertifikat" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_sertifikat }}</textarea></td>
                            </tr>

                           <tr>
                                <td>7</td>
                                <td>Surat pernyataan tangungjawab bermaterai cukup *)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_pernyataan_materai1" name="cek_pernyataan_materai" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_pernyataan_materai==1)?'checked':'' }}>
                                        <label for="cek_pernyataan_materai1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_pernyataan_materai2" name="cek_pernyataan_materai" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_pernyataan_materai==2)?'checked':'' }}>
                                        <label for="cek_pernyataan_materai2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="ket_pernyataan_materai" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->ket_pernyataan_materai }}</textarea></td>
                            </tr>   
                          
                            <tr>
                                <td>8</td>
                                <td>Salinan rekening bank yang masih aktif atas nama badan, lembaga atau organisasi kemasyarakatan</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_rekening_lembaga1" name="cek_rekening_lembaga" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_rekening_lembaga==1)?'checked':'' }}>
                                        <label for="cek_rekening_lembaga1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_rekening_lembaga2" name="cek_rekening_lembaga" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_rekening_lembaga==2)?'checked':'' }}>
                                        <label for="cek_rekening_lembaga2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_rekening_lembaga" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_rekening_lembaga }}</textarea></td>
                            </tr>
                            
                            <tr>
                                <td>9</td>
                                <td>Fotokopi SK kepengurusan atau dokumen yang dipersamakan *)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_sk_kepengurusan1" name="cek_sk_kepengurusan" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_sk_kepengurusan==1)?'checked':'' }}>
                                        <label for="cek_sk_kepengurusan1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_sk_kepengurusan2" name="cek_sk_kepengurusan" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_sk_kepengurusan==2)?'checked':'' }}>
                                        <label for="cek_sk_kepengurusan2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="ket_sk_kepengurusan" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->ket_sk_kepengurusan }}</textarea></td>
                            </tr>

                            <tr>
                                <td>10</td>
                                <td>Bantuan yanng pernah diterima tahun sebelumnya apabila ada (tanda terima laporan pertanggungjawaban) *)</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_bantuan_tahun_sebelum1" name="cek_bantuan_tahun_sebelum" value="1" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_bantuan_tahun_sebelum==1)?'checked':'' }}>
                                        <label for="cek_bantuan_tahun_sebelum1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_bantuan_tahun_sebelum2" name="cek_bantuan_tahun_sebelum" value="2" class="md-radiobtn" {{ ($recUserLembagaByNoLembaga->cek_bantuan_tahun_sebelum==2)?'checked':'' }}>
                                        <label for="cek_bantuan_tahun_sebelum2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_bantuan_tahun_sebelum" class="form-control" rows="5">{{ $recUserLembagaByNoLembaga->keterangan_cek_bantuan_tahun_sebelum }}</textarea></td>
                            </tr> 

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <center>
                <input type="checkbox" name="is_approve" {{ ($recUserLembagaByNoLembaga->is_approve==1)?'checked':'' }}> <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Lembaga Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
            </center>
        </div>
        <br>
        <br>
        <br>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12" style="padding-left: 33px;">
                    <button type="submit" class="btn green-meadow">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>