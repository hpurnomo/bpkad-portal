@extends('admin/layout/template')
@section('content')
<style type="text/css">
    .blink {
      animation: blink-animation 5s steps(5, start) infinite;
      -webkit-animation: blink-animation 5s steps(5, start) infinite;
    }
    @keyframes  blink-animation {
      to {
        visibility: hidden;
      }
    }
    @-webkit-keyframes blink-animation {
      to {
        visibility: hidden;
      }
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Detil Lembaga</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{ dd($recLembagaByNomor) }} --}}
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <p>
                        <b class="text-danger blink">Segera Lakukan Verifikasi Lembaga Untuk Memastikan Lembaga Tersebut Layak Mengajukan Proposal dan Juga Untuk Kebutuhan Berita Acara Kelengkapan Administrasi Maupun Peninjauan Lapangan</b>
                        <br><br>Notifikasi Hasil Checklist Kelengkapan Administrasi / Peninjauan Lapangan, Akan Langsung Terkirim Ke-EMail Lembaga Sehingga Lembaga Dapat Merespon Dengan Segera.
                    </p>
                </div>
            </div>
        </div>

        {{-- {{dd($recLembagaByNomor)}} --}}
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_0" data-toggle="tab">1. Detil Lembaga </a>
                            </li>
                            <li {{-- class="{{ ($recUserLembagaByNoLembaga->is_approve==0)?'active':'' }}" --}}>
                                <a href="#tab_1_1" data-toggle="tab">2. Kelengkapan Administrasi </a>
                            </li>
                            <li {{-- class="{{ ($recUserLembagaByNoLembaga->is_approve==1)?'active':'' }}" --}}>
                                <a href="#tab_1_2" data-toggle="tab">3. Peninjauan Lapangan </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            {{-- Detil Lembaga --}}
                            @include('admin.skpd.lembaga.detil-lembaga')
                            
                            {{-- Kelengkapan Administrasi --}}
                            @include('admin.skpd.lembaga.kelengkapan-administrasi')
                            
                            {{-- Peninjauan Lapangan --}}
                            @include('admin.skpd.lembaga.peninjauan-lapangan')
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
    <!-- END page-content-wrapper -->

@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>

    @endif

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = {{ ($recLembagaByNomor->latitude==NULL)?'0':$recLembagaByNomor->latitude }}; 
            var longValue = {{ ($recLembagaByNomor->longitude==NULL)?'0':$recLembagaByNomor->longitude }}; 
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng({{ ($recLembagaByNomor->latitude==NULL)?'0':$recLembagaByNomor->latitude }},{{ ($recLembagaByNomor->longitude==NULL)?'0':$recLembagaByNomor->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
@stop