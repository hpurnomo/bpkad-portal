<div class="tab-pane fade" id="tab_1_2">
    @if($recUserLembagaByNoLembaga->is_approve==0)
        <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
    @else
    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
    <form action="{{ route('skpd.lembaga.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
    <div class="form-group">
        <div class="form-body">
            {!! csrf_field() !!}
            <input type="hidden" name="no_lembaga" value="{{ $recUserLembagaByNoLembaga->no_lembaga }}">
            <input type="hidden" name="email" value="{{ $recLembagaByNomor->email }}">
            <input type="hidden" name="nm_lembaga" value="{{ $recLembagaByNomor->nm_lembaga }}">
            <input type="hidden" name="nm_skpd" value="{{ $recLembagaByNomor->nm_skpd }}">
            <div class="form-group form-md-radios">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="50%"><center>Data Lapangan</center></th>
                                <th><center>Sesuai</center></th>
                                <th><center>Tidak Sesuai</center></th>
                                <th><center>Keterangan</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Nama dan Identitas</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_ktp_ketua21" name="cek_ktp_ketua" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_ktp_ketua==1)?'checked':'' }}>
                                        <label for="cek_ktp_ketua21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_ktp_ketua22" name="cek_ktp_ketua" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_ktp_ketua==2)?'checked':'' }}>
                                        <label for="cek_ktp_ketua22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_ktp_ketua" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_ktp_ketua }}</textarea></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Alamat</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_alamat_lembaga21" name="cek_alamat_lembaga" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_alamat_lembaga==1)?'checked':'' }}>
                                        <label for="cek_alamat_lembaga21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_alamat_lembaga22" name="cek_alamat_lembaga" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_alamat_lembaga==2)?'checked':'' }}>
                                        <label for="cek_alamat_lembaga22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_alamat_lembaga" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_alamat_lembaga }}</textarea></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Saldo akhir tahun lalu beserta rekening Bank</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_rekening_lembaga21" name="cek_rekening_lembaga" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_rekening_lembaga==1)?'checked':'' }}>
                                        <label for="cek_rekening_lembaga21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_rekening_lembaga22" name="cek_rekening_lembaga" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_rekening_lembaga==2)?'checked':'' }}>
                                        <label for="cek_rekening_lembaga22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_rekening_lembaga" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_rekening_lembaga }}</textarea></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila ada</td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_bantuan_tahun_sebelum21" name="cek_bantuan_tahun_sebelum" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_bantuan_tahun_sebelum==1)?'checked':'' }}>
                                        <label for="cek_bantuan_tahun_sebelum21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_bantuan_tahun_sebelum22" name="cek_bantuan_tahun_sebelum" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_bantuan_tahun_sebelum==2)?'checked':'' }}>
                                        <label for="cek_bantuan_tahun_sebelum22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_bantuan_tahun_sebelum" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_bantuan_tahun_sebelum }}</textarea></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Nomor Pokok Wajib Pajak <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_npwp21" name="cek_npwp" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_npwp==1)?'checked':'' }}>
                                        <label for="cek_npwp21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_npwp22" name="cek_npwp" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_npwp==2)?'checked':'' }}>
                                        <label for="cek_npwp22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_npwp" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_npwp }}</textarea></td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_akta21" name="cek_akta" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_akta==1)?'checked':'' }}>
                                        <label for="cek_akta21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_akta22" name="cek_akta" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_akta==2)?'checked':'' }}>
                                        <label for="cek_akta22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_akta" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_akta }}</textarea></td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Surat Keterangan Domisili dari Kelurahan setempat <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_skd21" name="cek_skd" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_skd==1)?'checked':'' }}>
                                        <label for="cek_skd21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_skd22" name="cek_skd" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_skd==2)?'checked':'' }}>
                                        <label for="cek_skd22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_skd" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_skd }}</textarea></td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_sertifikat21" name="cek_sertifikat" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_sertifikat==1)?'checked':'' }}>
                                        <label for="cek_sertifikat21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_sertifikat22" name="cek_sertifikat" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_sertifikat==2)?'checked':'' }}>
                                        <label for="cek_sertifikat22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_sertifikat" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_sertifikat }}</textarea></td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang <span style="color:red;">(aslinya)*</span></td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_izin_operasional21" name="cek_izin_operasional" value="1" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_izin_operasional==1)?'checked':'' }}>
                                        <label for="cek_izin_operasional21">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="md-radio">
                                        <input type="radio" id="cek_izin_operasional22" name="cek_izin_operasional" value="2" class="md-radiobtn" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->cek_izin_operasional==2)?'checked':'' }}>
                                        <label for="cek_izin_operasional22">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> 
                                        </label>
                                    </div>
                                </td>
                                <td><textarea name="keterangan_cek_izin_operasional" class="form-control" rows="5">{{ (empty($recUserLembagaLapanganByNoLembaga))?'':$recUserLembagaLapanganByNoLembaga->keterangan_cek_izin_operasional }}</textarea></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <center>
                <input type="checkbox" name="is_approve" {{ (empty($recUserLembagaLapanganByNoLembaga))?'':($recUserLembagaLapanganByNoLembaga->is_approve==1)?'checked':'' }}> <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Lembaga Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Berita Acara Peninjauan Lapangan.</span>
            </center>
        </div>
        <br><br><br>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12" style="padding-left: 33px;">
                    <button type="submit" class="btn green-meadow">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                </div>
            </div>
        </div>
    </div>
    </form>
    @endif
</div>