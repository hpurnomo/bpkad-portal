@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.lembaga.manage') }}">Lembaga</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Rekapitulasi</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
            	<div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <ol>
						<li>C1 : Nama dan Identitas	</li>
						<li>C2 : Alamat	</li>
						<li>C3 : Saldo akhir tahun lalu beserta rekening Bank </li>
						<li>C4 : Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila ada	</li>
						<li>C5 : Nomor Pokok Wajib Pajak </li>
						<li>C6 : Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga </li>
						<li>C7 : Surat Keterangan Domisili dari Kelurahan setempat </li>
						<li>C8 : Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan </li>
						<li>C9 : Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang </li>
					</ol>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <a href="{{ route('skpd.lembaga.manage') }}" class="btn grey-salsa btn-outline"> Kembali</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_3">
                            <thead>
								<tr>
									<th rowspan="2">No</th>
									<th rowspan="2">Lembaga</th>
									<th colspan="9">Data Administrasi</th>
									<th colspan="9">Data Lapangan</th>
								</tr>
								<tr>
									<th>C1</th>
									<th>C2</th>
									<th>C3</th>
									<th>C4</th>
									<th>C5</th>
									<th>C6</th>
									<th>C7</th>
									<th>C8</th>
									<th>C9</th>
									<th>C1</th>
									<th>C2</th>
									<th>C3</th>
									<th>C4</th>
									<th>C5</th>
									<th>C6</th>
									<th>C7</th>
									<th>C8</th>
									<th>C9</th>
								</tr>
							</thead>
                            <tbody>
								@foreach ($dataKelengkapanAdministrasi as $key=>$value)
									<tr>
										<td>{{ $key+1 }}</td>
										<td><a href="{{ route('skpd.lembaga.show',['noLembaga'=>$value->nomor]) }}"> {{ ucwords(strtolower($value->nm_lembaga)) }} </a></td>
										<td><center>{!! ($value->cek_ktp_ketua==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_alamat_lembaga==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_rekening_lembaga==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_bantuan_tahun_sebelum==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_npwp==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_akta==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_skd==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_sertifikat==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_izin_operasional==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>

										<td><center>{!! ($value->cek_ktp_ketua_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_alamat_lembaga_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_rekening_lembaga_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_bantuan_tahun_sebelum_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_npwp_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_akta_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_skd_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_sertifikat_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
										<td><center>{!! ($value->cek_izin_operasional_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
									</tr>
								@endforeach
							</tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
@stop