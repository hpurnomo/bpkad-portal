<div class="tab-pane fade" id="tab_10_4">
@if (empty($recProposalDefinitifAPBDByUUID))
    <h5 style="color: red;font-weight: bolder;">*Belum ada proposal yang dikirim oleh Lembaga.</h5>
@else
    <div class="portlet-body form">
        <a href="{{ route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>'apbd','tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
        <a href="{{ route('skpd.proposal.verifikasi.berkas.pencairan',['fase'=>$fase,'id'=>$no_prop]) }}" class="btn btn-circle red btn-outline sbold uppercase" style="margin-top: 10px;">Verifikasi berkas pencairan</a>
         <!-- <a href="{{ route('skpd.proposal.verifikasi.berkas.pencairan',['fase'=>$fase,'id'=>$no_prop]) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;">Upload Dokumen Pencairan</a> -->
          <a href="{{ route('lembaga.upload.dokumen.list',['noper'=>$recProposalByID->noper,'no_prop'=>$recProposalByID->no_prop,'uuid_proposal_pengajuan'=>$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan]) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;">Upload scan dokumen</a>
        <hr>
        <form action="{{ route('skpd.proposal.updateAPBD') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
        {{ csrf_field() }}
            <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan }}">
            <input type="hidden" name="jdl_prop" value="{{$recProposalDefinitifAPBDByUUID->jdl_prop}}">
            <input type="hidden" name="proposalID" value="{{ $proposalID }}">
            <input type="hidden" name="fase" value="{{$fase}}">
            <input type="hidden" name="nm_lembaga" value="{{$recProposalDefinitifAPBDByUUID->nm_lembaga}}">
            <input type="hidden" name="no_lembaga" value="{{$recProposalDefinitifAPBDByUUID->no_lembaga}}">
            <input type="hidden" name="email_lembaga" value="{{$recProposalDefinitifAPBDByUUID->email_lembaga}}">
            <div class="form-group form-md-radios">
                <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
                <div class="col-md-9">
                    <div class="md-radio-inline">
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_84" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==0)?'checked':'' }} {{ ( !empty($recProposalDefinitifAPBDByUUID->nominal_rekomendasi) && $recProposalDefinitifAPBDByUUID->status_prop==1 )?'disabled':'' }}>
                            <label for="checkbox1_84">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Menunggu </label>
                        </div>
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_114" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==3)?'checked':'' }} {{ ( !empty($recProposalDefinitifAPBDByUUID->nominal_rekomendasi) && $recProposalDefinitifAPBDByUUID->status_prop==1 )?'disabled':'' }}>
                            <label for="checkbox1_114">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Dikoreksi </label>
                        </div>
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_94" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==1)?'checked':'' }}>
                            <label for="checkbox1_94">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Sudah Direkomendasi </label>
                        </div>
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_104" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalDefinitifAPBDByUUID->status_prop==2)?'checked':'' }} {{ ( !empty($recProposalDefinitifAPBDByUUID->nominal_rekomendasi) && $recProposalDefinitifAPBDByUUID->status_prop==1 )?'disabled':'' }}>
                            <label for="checkbox1_104">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Di Tolak </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            {{-- for status diterima --}}
            <div id="nominal-rekomendasi4" @if($recProposalDefinitifAPBDByUUID->status_prop==0) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==2) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==3) style="display:none;" @endif>
                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Kategori
                    </label>
                    <div class="col-md-7">
                        <select class="form-control select2me" name="kategoriProposalID">
                            <option value="">Pilih Kategori</option>
                            @foreach($recAllKategoriProposal as $key=>$value)
                                <option value="{{ $value->id }}" {{ ($recProposalByID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Program
                    </label>
                    <div class="col-md-7">
                        <select class="form-control select2me" name="no_prg">
                            <option value="">Pilih Program</option>
                            @foreach($recAllProgram as $key=>$value)
                                <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Rekening
                    </label>
                    <div class="col-md-7">
                        <select class="form-control select2me" name="no_rek">
                            <option value="">Pilih Rekening</option>
                            @foreach($recAllRekening as $key=>$value)
                                <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                    <div class="col-md-6">
                        <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral-apbd" value="{{ str_replace(".", ",", $recProposalDefinitifAPBDByUUID->nominal_rekomendasi) }}">
                        @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                        <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                    <div class="col-md-4">
                        <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_rekomendasi }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalDefinitifAPBDByUUID->tgl_rekomendasi }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                    <div class="col-md-4">
                        <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_survei_lap }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalDefinitifAPBDByUUID->tgl_survei_lap }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                    <div class="col-md-4">
                        <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_kelengkapan_administrasi }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalDefinitifAPBDByUUID->tgl_kelengkapan_administrasi }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                    <div class="col-md-4">
                        <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalDefinitifAPBDByUUID->no_surat_skpd }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalDefinitifAPBDByUUID->tgl_surat_skpd }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                <hr>
                <!--  here bro -->
 
                @if($periodeDetail->definitif=='Y' && $periodeDetail->status ==1)
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-5">
                        <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    </div>
                </div>
                @endif 

                <br>
                <!-- -->
                @endif
            </div>
            

            {{-- for status ditolak --}}
            <div id="alasan4" @if($recProposalDefinitifAPBDByUUID->status_prop==0) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==1) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==3) style="display:none;" @endif>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                    <div class="col-md-5">
                        <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalDefinitifAPBDByUUID->alasan_ditolak }}"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-5">
                        <button type="submit" class="btn green">Simpan</button>
                    </div>
                </div>
            </div>

            <div id="alasan-dikembalikan4" @if($recProposalDefinitifAPBDByUUID->status_prop==0) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==1) style="display:none;" @elseif($recProposalDefinitifAPBDByUUID->status_prop==2) style="display:none;" @endif>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                    <div class="col-md-5">
                        <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalDefinitifAPBDByUUID->alasan_dikembalikan}}"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-5">
                        <button type="submit" class="btn green">Kirim</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-eye"></i>Detil Proposal Definitif</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_01234" data-toggle="tab">1. Data Proposal </a>
                                </li>
                                <li>
                                    <a href="#tab_1_11234" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                </li>
                            </ul>
                        <div class="tab-content">
                            
                            {{-- Data Proposal --}}
                            <div class="tab-pane fade active in" id="tab_1_01234">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" role="form">
                                    <div class="form-body" style="padding: 0;margin: 0;">
                                        <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalDefinitifAPBDByUUID->tgl_prop)) }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ $recProposalDefinitifAPBDByUUID->no_proposal }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ $recProposalDefinitifAPBDByUUID->jdl_prop }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <h3 class="form-section">Isi Proposal</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->latar_belakang !!} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->maksud_tujuan !!} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {!! $recProposalDefinitifAPBDByUUID->keterangan_prop !!} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <h3 class="form-section">Nominal Permohonan</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($recProposalDefinitifAPBDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="form-section">Data Pendukung</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                    <div class="col-md-9">
                                                        @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                        @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                        <p class="form-control-static"> 
                                                            <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                            {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                            </a>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                    <div class="col-md-9">
                                                        @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                        @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                        <p class="form-control-static"> 
                                                            <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                            {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                            </a>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                    <div class="col-md-9">
                                                        @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                        @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                        <p class="form-control-static"> 
                                                            <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                            {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                            </a>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                    <div class="col-md-9">
                                                        @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                        @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                        <p class="form-control-static"> 
                                                            <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                            {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                            </a>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                    <div class="col-md-9">
                                                        @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                        @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                        <p class="form-control-static"> 
                                                            <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                            {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                            </a>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                    <div class="col-md-9">
                                                        @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                        @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalDefinitifAPBDByUUID->uuid_proposal_pengajuan)->where('tahap','apbd')->get() as $key=>$value)
                                                        <p class="form-control-static"> 
                                                            <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                            {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                            </a>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                    <div class="col-md-9">
                                                        <div class="portlet-body">
                                                            <div class="input-group">
                                                                <input type="hidden" class="form-control" id="pac-input-apbd" placeholder="Cari Alamat">
                                                                {{-- <span class="input-group-btn">
                                                                    <button class="btn blue" id="gmap_geocoding_btn">
                                                                        <i class="fa fa-search"></i>
                                                                </span> --}}
                                                            </div>
                                                            <div id="gmap_basic_apbd" class="gmaps"> </div>
                                                            <b>Latitude : </b>
                                                            <input type="text" class="form-control" name="latitude" id="latValAPBD" value="{{$recProposalDefinitifAPBDByUUID->latitude}}" >
                                                            <b>Longitude : </b>
                                                            <input type="text" class="form-control" name="longitude" id="longValAPBD" value="{{$recProposalDefinitifAPBDByUUID->longitude}}" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                            
                            {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                            <div class="tab-pane fade" id="tab_1_11234">
                                <div class="col-md-12">
                                    <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                    @if($recProposalDefinitifAPBDByUUID->nominal_pengajuan != $rencana_anggaran_apbd)
                                    <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                    @else
                                    <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nominal Definitif (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($recProposalDefinitifAPBDByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nilai Input RAB Definitif (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_apbd,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <br><br><br>
                                    <!--/row-->
                                    
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                        <thead>
                                            <tr>
                                                <th> No. </th>
                                                <th> Rencana Kegiatan/Rincian </th>
                                                <th> Vol/Satuan (jumlah) </th>
                                                <th> Harga Satuan (Rp) </th>
                                                <th> Anggaran (Rp) </th>
                                                <th> Anggaran Terpakai (Rp) </th>
                                                <th> Sisa Anggaran (Rp) </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($recRencanaByUUIDAPBD)==0)
                                                <tr>
                                                    <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                </tr>
                                            @else
                                                @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                                @foreach ($recRencanaByUUIDAPBD as $key=>$value)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td><b>{{$value->rencana}}</b></td>
                                                    <td></td>
                                                    <td></td>
                                                     <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                        <td><b> {{ number_format($value->anggaran_digunakan,2,",",".")}} </b></td>
                                                        <td><b> </b></td>
                                                </tr>
                                                {!! $metrics->outputByRencanaID($value->id,$fase,$noper,$uuid,'apbd','rencana') !!}
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_2_01234" data-toggle="tab">1. Kelengkapan Administrasi </a>
                        </li>
                        <li>
                            <a href="#tab_2_11234" data-toggle="tab">2. Peninjauan Lapangan </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        {{-- Kelengkapan Administrasi --}}
                        <div class="tab-pane fade active in" id="tab_2_01234">
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                            <form action="{{ route('skpd.proposal.penebalan.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-group">
                                <div class="form-body">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                    <input type="hidden" name="fase" value="{{ $fase }}">
                                    <input type="hidden" name="uuid" value="{{ $uuid }}">
                                    <input type="hidden" name="tahap" value="apbd">
                                    <div class="form-group form-md-radios">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th width="50%"><center>Data Administrasi</center></th>
                                                        <th><center>Ada</center></th>
                                                        <th><center>Tidak Ada</center></th>
                                                        <th><center>Keterangan</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Aktivitas</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas1111234" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas1111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas2111234" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_aktivitas==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas2111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Kepengurusan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan1111234" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan1111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan2111234" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan2111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Rencana Anggaran Biaya (RAB)</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab1111234" name="cek_rab" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab1111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab2111234" name="cek_rab" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_rab==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab2111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab))
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Waktu Pelaksanaan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan1111234" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan1111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan2111234" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan2111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                @if(!empty($statusAdministrasiLembaga) && !empty($statusLapanganLembaga))
                                    @if($statusLapanganLembaga->is_approve == null || $statusLapanganLembaga->is_approve == 0)
                                        <b class="text-danger blink">Lembaga "{{$recProposalByID->nm_lembaga}}" belum lolos tahap "CHECKLIST KESESUAIAN DATA ANTARA DOKUMEN TERTULIS DAN SOFTCOPY". <div class="blink"> Mohon periksa kembali Lembaga tersebut.</div>
                                        </b>
                                        <a href="{{ route('skpd.lembaga.show',['noLembaga'=>$recProposalByID->no_lembaga]) }}">Periksa Lembaga</a>
                                    @else
                                    <center>
                                        <input type="checkbox" name="is_approve"
                                            @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve==1)
                                            checked
                                            @endif
                                        > 
                                        <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                    </center>
                                    @endif
                                @else
                                    <b class="text-danger">Lembaga "{{$recProposalByID->nm_lembaga}}" belum lolos tahap "CHECKLIST KESESUAIAN DATA ANTARA DOKUMEN TERTULIS DAN SOFTCOPY".<div class="blink"> Mohon periksa kembali Lembaga tersebut.</div>
                                    </b>
                                    <a href="{{ route('skpd.lembaga.show',['noLembaga'=>$recProposalByID->no_lembaga]) }}">Periksa Lembaga</a>
                                @endif
                                </div>
                                <br>
                                <br>
                                <br>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-11">
                                            <button type="submit" class="btn green-meadow">Kirim</button>
                                            <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        
                        {{-- Kelengkapan Lapangan --}}
                        <div class="tab-pane fade" id="tab_2_11234">
                            @if(isset($recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanAPBDByUUIDAndTahap->is_approve==1)
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                            <form action="{{ route('skpd.proposal.penebalan.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-group">
                                <div class="form-body">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                    <input type="hidden" name="fase" value="{{ $fase }}">
                                    <input type="hidden" name="uuid" value="{{ $uuid }}">
                                    <input type="hidden" name="tahap" value="apbd">
                                    <div class="form-group form-md-radios">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th width="50%"><center>Data Lapangan</center></th>
                                                        <th><center>Sesuai</center></th>
                                                        <th><center>Tidak Sesuai</center></th>
                                                        <th><center>Keterangan</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Aktivitas</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas11111234" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas11111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas22111234" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_aktivitas==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas22111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas))
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Kepengurusan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan11111234" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan11111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan22111234" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_kepengurusan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan22111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Rencana Anggaran Biaya (RAB)</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab11111234" name="cek_rab" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab11111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab22111234" name="cek_rab" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_rab==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab22111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab))
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Waktu Pelaksanaan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan11111234" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan11111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan22111234" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan22111234">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanAPBDByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <input type="checkbox" name="is_approve"
                                            @if(isset($recProposalLapanganPenebalanAPBDByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanAPBDByUUIDAndTahap->is_approve==1)
                                            checked
                                            @endif
                                        > 
                                        <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                    </center>
                                </div>
                                <br>
                                <br>
                                <br>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-11">
                                            <button type="submit" class="btn green-meadow">Kirim</button>
                                            <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                            @else
                            <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
</div>                            