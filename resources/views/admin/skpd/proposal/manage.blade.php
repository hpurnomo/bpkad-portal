@extends('admin/layout/template')
@section('content')
<style type="text/css">
.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-100{
    width:100px;
}
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar</span>
                </li>
            </ul>
        </div>
        {{-- {{ dd($recDetailPeriodeTahapanByNoper[1]->status) }} --}}
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
        	<!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="note note-info">
                    <h4 class="block">Info!</h4>
                    <ol>
                        <li>Data dibawah ini merupakan proposal yang telah terkirim dari Lembaga terkait.</li>
                        <li>Klik pada "Judul Proposal" untuk pengaturan.</li>
                    </ol>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Daftar </span>
                        </div>
  

                        <form class="btn-pdf" action="{{ route('skpd.proposal.download.daftar.penerima.hibah') }}" method="POST" style="float: right;">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="fase" value="{{ $fase }}">
                            <input type="hidden" name="noper" value="{{ $noper }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            
                            <a href="{{ route('skpd.proposal.surat.rekomendasi.kolektif',['noper'=>$noper]) }}" class="btn green" > Rekomendasi Kolektif </a>
                            <button type="submit" class="btn red"><i class="fa fa-download" aria-hidden="true"></i> Download Daftar Rekomendasi Penerima Hibah</button>
                        
                        </form>
                    </div>
                    <div class="portlet-body">
                        
                        <table class="table table-striped table-bordered table-hover table-checkable order-column display responsive nowrap" id="example">
                            <thead>
                                <tr>
                                    <th> No. </th>
                                    {{-- <th> Action </th> --}}
                                    <th> Judul Proposal </th>
                                    <th> Status </th>
                                    <th> Lembaga </th>
                                    <th> Nominal Permohonan (Rp) </th>
                                    <th> Nominal Rekomendasi (Rp) </th>
                                    <th> Nominal TAPD (Rp) </th>
                                    <th> Nominal Banggar (Rp) </th>
                                    <th> Nominal DDN (Rp) </th>
                                    <th> Nominal APBD (Rp) </th>
                                    <th> Tgl Submit </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($recProposalBySkpdID as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    {{-- <td> --}}
                                        {{-- @if($recDetailPeriodeTahapanByNoper[1]->status==1) --}}
                                        {{-- <a href="{{ route('skpd.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle dark btn-outline sbold uppercase">Pengaturan</a><br> --}}
                                        {{-- @else
                                        <a href="{{ route('skpd.proposal.only.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}" class="btn btn-circle dark btn-outline sbold uppercase">Lihat</a><br>
                                        @endif --}}
                                        {{-- <a href="{{ route('skpd.cetak.show',['no_prop'=>base64_encode($value->no_prop),'fase'=>$fase,'tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak</a><br>
                                        <a href="{{ route('skpd.proposal.verifikasi.berkas.pencairan',['fase'=>$fase,'id'=>base64_encode($value->no_prop)]) }}" class="btn btn-circle red btn-outline sbold uppercase" style="margin-top: 10px;">Verifikasi berkas pencairan</a> --}}
                                        {{-- <a href="{{ route('skpd.proposal.verifikasi.berkas.pencairan',['fase'=>$fase,'id'=>base64_encode($value->no_prop)]) }}" class="btn btn-circle red btn-outline sbold uppercase" style="margin-top: 10px;">Verifikasi berkas pencairan</a> --}}
                                    {{-- </td> --}}
                                    <td><a href="{{ route('skpd.proposal.show',['id'=>base64_encode($value->no_prop),'fase'=>$fase]) }}">{{ $value->jdl_prop }}</a></td>
                                    <td>
                                        @if($value->status_prop==0)
                                            Belum Direkomendasi
                                        @elseif($value->status_prop==1)
                                            Sudah Direkomendasi
                                        @elseif($value->status_prop==2)
                                            Di Tolak
                                            <p class="bg-red bg-font-red" style="padding:5px;">Alasan : {{ $value->alasan_ditolak }} </p>
                                        @endif
                                    </td>
                                    <td> {{ ucwords(strtolower($value->nm_lembaga)) }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_pengajuan,'2',',','.') }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_rekomendasi,'2',',','.') }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_tapd,'2',',','.') }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_banggar,'2',',','.') }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_ddn,'2',',','.') }} </td>
                                    <td style="text-align: right;"> {{ number_format($value->nominal_apbd,'2',',','.') }} </td>
                                    <td> @if($value->tgl_prop != NULL){{ date('d M Y',strtotime($value->tgl_prop)) }} @endif</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                responsive: true,
                columnDefs: [
                    {
                        render: function (data, type, full, meta) {
                            return "<div class='text-wrap width-100'>" + data + "</div>";
                        },
                        targets: [1,2,3]
                    }
                 ]
            } );
        } );
    </script>
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            // autosubmit
            $('#checkbox1_8,#checkbox1_9,#checkbox1_10').click(function(){
                $('form#form_sample_1').submit();
            });

            $(".btn-pdf").on("submit", function(){
                return confirm("Apakah Anda yakin ?");
            });
        });
    </script>
    @if( !empty(Session::get('status')) )
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif

@stop