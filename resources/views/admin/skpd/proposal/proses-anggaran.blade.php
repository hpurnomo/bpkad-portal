<div class="tab-pane fade" id="tab_10_5">
    <div class="row">
        <div class="col-md-12">

            <h4><b>Proses Anggaran</b></h4>
            <hr> 
            <!-- BEGIN FORM-->
             @if($periodeDetail->rek_anggaran=='Y' && $periodeDetail->status ==1)
            <form action="{{ route('skpd.proposal.updateProsesAnggaran') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
            @else
            <form  class="form-horizontal" > 
            @endif
            {{ csrf_field() }}

                <input type="hidden" name="noper" value="{{$noper}}" id="noper"> 
                <input type="hidden" name="uuid" value="{{$recProposalByID->uuid}}" id="uuid"> 
                <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}" id="no_prop">
                <input type="hidden" name="proposalID" value="{{ $proposalID }}">
                <input type="hidden" name="fase" value="{{ $fase }}">
            	<input type="hidden" name="jdl_prop" value="{{$recProposalByID->jdl_prop}}" id="jdl_prop">
            	<input type="hidden" name="no_lembaga" value="{{$recProposalByID->no_lembaga}}" id="no_lembaga">
                <input type="hidden" name="email_lembaga" value="{{$recProposalByID->email_lembaga}}" id="email_lembaga">
	            <input type="hidden" name="email" value="{{$recProposalByID->emailKoordinator}}" id="email">
	            <input type="hidden" name="real_name" value="{{$recProposalByID->real_name}}" id="real_name">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal Permohonan (Rp): </label>
                    <div class="col-md-4">                                
                        <input name="nominal_pengajuan" type="text" data-required="1" class="form-control input-numeral2" value="{{ str_replace(".", ",", $recProposalByID->nominal_pengajuan) }}" disabled="">

                        <span class="help-block" style="color: green;"><i>Diinput oleh Lembaga.</i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-4">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral3" value="{{ str_replace(".", ",", $recProposalByID->nominal_rekomendasi) }}" disabled="">
                        <span class="help-block" style="color: green;"><i>Diinput oleh SKPD.</i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-4">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal TAPD (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_tapd" type="text" class="form-control input-numeral4" value="{{ str_replace(".", ",", $recProposalByID->nominal_tapd) }}" disabled="">

                        @if($recDetailPeriodeTahapanByNoper[2]->status==1)
                        <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pembahasan TAPD telah berakhir.</i></span>
                        @endif
                        
                        <input type="checkbox" name="is_penebalan_nominal_tapd" class="checkProposal"   data-id="TAPD" {{ ($recProposalByID->is_penebalan_nominal_tapd==1)?'checked':'' }} {{ ($recDetailPeriodeTahapanByNoper[2]->status==0)?'disabled':'' }}> Setuju Proposal Penebalan 
                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-4">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal BANGGAR (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_banggar" type="text" class="form-control input-numeral5 "   value="{{ str_replace(".", ",", $recProposalByID->nominal_banggar) }}" disabled="">

                        @if($recDetailPeriodeTahapanByNoper[3]->status==1)
                        <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pembahasan Banggar telah berakhir.</i></span>
                        @endif
                        
                        <input type="checkbox" name="is_penebalan_nominal_banggar" class="checkProposal" data-id="BANGGAR"  {{ ($recProposalByID->is_penebalan_nominal_banggar==1)?'checked':'' }} {{ ($recDetailPeriodeTahapanByNoper[3]->status==0)?'disabled':'' }}> Setuju Proposal Penebalan
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-4">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal DDN (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_ddn" type="text" class="form-control input-numeral6 "  value="{{ str_replace(".", ",", $recProposalByID->nominal_ddn) }}" disabled="">

                        @if($recDetailPeriodeTahapanByNoper[4]->status==1)
                        <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pembahasan DDN telah berakhir.</i></span>
                        @endif

                        <input type="checkbox" name="is_penebalan_nominal_ddn" class="checkProposal" data-id="DDN" {{ ($recProposalByID->is_penebalan_nominal_ddn==1)?'checked':'' }} {{ ($recDetailPeriodeTahapanByNoper[4]->status==0)?'disabled':'' }}> Setuju Proposal Penebalan
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-4">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal APBD (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_apbd" type="text" class="form-control input-numeral7 "  value="{{ str_replace(".", ",", $recProposalByID->nominal_apbd) }}" disabled="">
                        
                        @if($recDetailPeriodeTahapanByNoper[5]->status==1)
                        <span class="help-block" style="color: green;"><i>Diinput oleh PPKD.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pembahasan APBD telah berakhir.</i></span>
                        @endif

                        <input type="checkbox" name="is_penebalan_nominal_apbd" class="checkProposal" data-id="Definitif" {{ ($recProposalByID->is_penebalan_nominal_apbd==1)?'checked':'' }} {{ ($recDetailPeriodeTahapanByNoper[5]->status==0)?'disabled':'' }}> Setuju Proposal Definitif
                    </div>
                </div>
                {{-- <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal Pencairan (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_pencairan" type="text" data-required="1" class="form-control input-numeral8" value="{{ str_replace(".", ",", $recProposalByID->nominal_pencairan) }}">
                    </div>
                </div> --}}
                {{-- <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Nominal LPJ (Rp): </label>
                    <div class="col-md-4">
                        <input name="nominal_lpj" type="text" data-required="1" class="form-control input-numeral9" value="{{ str_replace(".", ",", $recProposalByID->nominal_lpj) }}">
                    </div>
                </div> --}}
                <hr>
                 @if($periodeDetail->rek_anggaran=='Y' && $periodeDetail->status ==1)
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    </div>  
                @endif
                {{--herebro
                  @if($recProposalByID->noper>=10)
                     <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    </div>
                    @endif
                --}}

               
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>  


     <!-- Modal ===============================--> 
      <div class="modal fade text-left" id="confirm-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header ">
          <h3 class="modal-title" id="myModalLabel35"> Konfirmasi </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          </div>
          <form>
          <div class="modal-body">
             <input type="hidden" id="tipe">
             <input type="hidden" id="table_id">
             <h5 class="mt-1" id="confirm-message"></h5>
          </div>
          <div class="modal-footer">
              <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal">
                  <i class="ft-x"></i> Batal
              </a>
              <a href="javascript:void(0)" class="btn  btn-primary" id="btn_save">
                  <i class="ft-trash"></i> KIRIM
              </a>
          </div>
          </form>
        </div>
        </div>
      </div> 
      <!-- End of modal --> 