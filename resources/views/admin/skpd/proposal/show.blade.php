@extends('admin/layout/template')
@section('content')
<style type="text/css">
    .blink {
      animation: blink-animation 2s steps(5, start) infinite;
      -webkit-animation: blink-animation 2s steps(5, start) infinite;
    }
    @keyframes    blink-animation {
      to {
        visibility: hidden;
      }
    }
    @-webkit-keyframes blink-animation {
      to {
        visibility: hidden;
      }
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('skpd.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper])}}">Daftar</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Pengaturan Proposal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        {{-- {{ dd($recProposalByID) }} --}}
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        <a href="{{route('skpd.proposal.manage',['fase'=>$fase,'noper'=>$recProposalByID->noper])}}" class="btn btn-default" style="float: right;margin-bottom: 10px;">Kembali</a>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_10_0" data-toggle="tab">Proposal Pengajuan </a>
                            </li>

                            {{-- Proposal Penebalan Tahap TAPD --}}
                            @if ($recProposalByID->is_penebalan_nominal_tapd==1)
                            <li {{-- class="{{ ($recUserLembagaByNoLembaga->is_approve==0)?'active':'' }}" --}}>
                                <a href="#tab_10_1" data-toggle="tab">Penebalan TAPD </a>
                            </li>
                            @endif
                            
                            {{-- Proposal Penebalan Tahap BANGGAR --}}
                            @if ($recProposalByID->is_penebalan_nominal_banggar==1)
                            <li>
                                <a href="#tab_10_2" data-toggle="tab">Penebalan BANGGAR </a>
                            </li>
                            @endif

                            {{-- Proposal Penebalan Tahap DDN --}}
                            @if ($recProposalByID->is_penebalan_nominal_ddn==1)
                            <li>
                                <a href="#tab_10_3" data-toggle="tab">Penebalan DDN </a>
                            </li>
                            @endif

                            {{-- Proposal Definitif Tahap APBD --}} 
                            @if ($recProposalByID->is_penebalan_nominal_apbd==1)
                            <li>
                                <a href="#tab_10_4" data-toggle="tab">Proposal Definitif </a>
                            </li>
                            @endif

                            <li>
                                <a href="#tab_10_5" data-toggle="tab">Proses Anggaran </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            
                            {{-- Proposal Pengajuan --}}
                            @include('admin.skpd.proposal.proposal-pengajuan')

                            {{-- Proposal Penebalan Tahap TAPD --}}
                            @if ($recProposalByID->is_penebalan_nominal_tapd==1)
                            @include('admin.skpd.proposal.proposal-tapd')
                            @endif
                            
                            {{-- Proposal Penebalan Tahap BANGGAR --}}
                            @if ($recProposalByID->is_penebalan_nominal_banggar==1)
                            @include('admin.skpd.proposal.proposal-banggar')
                            @endif
                            
                            {{-- Proposal Penebalan Tahap DDN --}}
                            @if ($recProposalByID->is_penebalan_nominal_ddn==1)
                            @include('admin.skpd.proposal.proposal-ddn')
                            @endif
                            
                            {{-- Proposal Definitif Tahap APBD --}}
                            @if ($recProposalByID->is_penebalan_nominal_apbd==1)
                            @include('admin.skpd.proposal.proposal-definitif')
                            @endif
                            
                            {{-- Proses Anggaran --}}
                            @include('admin.skpd.proposal.proses-anggaran')

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->

</div>
@stop
@section('javascript')
    @parent
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    }); 
            });
        </script>
    @endif

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>

    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = '{{ $recProposalByID->latitude }}';
            var longValue = '{{ $recProposalByID->longitude }}';
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>

    @if (!empty($recProposalPenebalanTAPDByUUID))
    {{-- tapd --}}
    <script type="text/javascript">
        var MapsGoogleTAPD = function () {
            
            var latValue = {{ ($recProposalPenebalanTAPDByUUID->latitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanTAPDByUUID->longitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalPenebalanTAPDByUUID->latitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->latitude }},{{ ($recProposalPenebalanTAPDByUUID->longitude==NULL)?'0':$recProposalPenebalanTAPDByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_tapd'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-tapd'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValTAPD').val(data.lat());
                $('#longValTAPD').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleTAPD.init();
    });
    </script>
    @endif

    @if ($recProposalPenebalanBANGGARByUUID)
    {{-- banggar --}}
    <script type="text/javascript">
        var MapsGoogleBANGGAR = function () {
            
            var latValue = {{ ($recProposalPenebalanBANGGARByUUID->latitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanBANGGARByUUID->longitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalPenebalanBANGGARByUUID->latitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->latitude }},{{ ($recProposalPenebalanBANGGARByUUID->longitude==NULL)?'0':$recProposalPenebalanBANGGARByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_banggar'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-banggar'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValBANGGAR').val(data.lat());
                $('#longValBANGGAR').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleBANGGAR.init();
    });
    </script>
    @endif

    @if ($recProposalPenebalanDDNByUUID)
    {{-- ddn --}}
    <script type="text/javascript">
        var MapsGoogleDDN = function () {
            
            var latValue = {{ ($recProposalPenebalanDDNByUUID->latitude==NULL)?'0':$recProposalPenebalanDDNByUUID->latitude }};
            var longValue = {{ ($recProposalPenebalanDDNByUUID->longitude==NULL)?'0':$recProposalPenebalanDDNByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalPenebalanDDNByUUID->latitude==NULL)?'0':$recProposalPenebalanDDNByUUID->latitude }},{{ ($recProposalPenebalanDDNByUUID->longitude==NULL)?'0':$recProposalPenebalanDDNByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_ddn'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-ddn'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValDDN').val(data.lat());
                $('#longValDDN').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleDDN.init();
    });
    </script>
    @endif

    @if ($recProposalDefinitifAPBDByUUID)
    {{-- apbd --}}
    <script type="text/javascript">
        var MapsGoogleAPBD = function () {
            
            var latValue = {{ ($recProposalDefinitifAPBDByUUID->latitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->latitude }};
            var longValue = {{ ($recProposalDefinitifAPBDByUUID->longitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->longitude }};
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng({{ ($recProposalDefinitifAPBDByUUID->latitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->latitude }},{{ ($recProposalDefinitifAPBDByUUID->longitude==NULL)?'0':$recProposalDefinitifAPBDByUUID->longitude }})
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic_apbd'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input-apbd'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latValAPBD').val(data.lat());
                $('#longValAPBD').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogleAPBD.init();
    });
    </script>
    @endif

    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            // autosubmit
            // $('#checkbox1_8,#checkbox1_9,#checkbox1_10').click(function(){
            //     $('form#form_sample_1').submit();
            // });

            // PENGAJUAN
            // belum direkomendasikan
            $('#checkbox1_8').click(function(){
                $('#nominal-rekomendasi,#alasan,#alasan-dikembalikan').css('display','none');
            });
            // sudah direkomendasikan
            $('#checkbox1_9').click(function(){
                $('#nominal-rekomendasi').css('display','block');
                $('#alasan').css('display','none');
                $('#alasan-dikembalikan').css('display','none');
            });
            // ditolak disertai alasan
            $('#checkbox1_10').click(function(){
                $('#nominal-rekomendasi').css('display','none');
                $('#alasan').css('display','block');
                $('#alasan-dikembalikan').css('display','none');
            });
            // dikembalikan kelembaga untuk diperbaiki
            $('#checkbox1_11').click(function(){
                $('#nominal-rekomendasi').css('display','none');
                $('#alasan').css('display','none');
                $('#alasan-dikembalikan').css('display','block');
            });

            // TAPD
            // belum direkomendasikan
            $('#checkbox1_81').click(function(){
                $('#nominal-rekomendasi1,#alasan1,#alasan-dikembalikan1').css('display','none');
            });
            // sudah direkomendasikan
            $('#checkbox1_91').click(function(){
                $('#nominal-rekomendasi1').css('display','block');
                $('#alasan1').css('display','none');
                $('#alasan-dikembalikan1').css('display','none');
            });
            // ditolak disertai alasan
            $('#checkbox1_101').click(function(){
                $('#nominal-rekomendasi1').css('display','none');
                $('#alasan1').css('display','block');
                $('#alasan-dikembalikan1').css('display','none');
            });
            // dikembalikan kelembaga untuk diperbaiki
            $('#checkbox1_111').click(function(){
                $('#nominal-rekomendasi1').css('display','none');
                $('#alasan1').css('display','none');
                $('#alasan-dikembalikan1').css('display','block');
            });

            // BANGGAR
            // belum direkomendasikan
            $('#checkbox1_82').click(function(){
                $('#nominal-rekomendasi2,#alasan2,#alasan-dikembalikan2').css('display','none');
            });
            // sudah direkomendasikan
            $('#checkbox1_92').click(function(){
                $('#nominal-rekomendasi2').css('display','block');
                $('#alasan2').css('display','none');
                $('#alasan-dikembalikan2').css('display','none');
            });
            // ditolak disertai alasan
            $('#checkbox1_102').click(function(){
                $('#nominal-rekomendasi2').css('display','none');
                $('#alasan2').css('display','block');
                $('#alasan-dikembalikan2').css('display','none');
            });
            // dikembalikan kelembaga untuk diperbaiki
            $('#checkbox1_112').click(function(){
                $('#nominal-rekomendasi2').css('display','none');
                $('#alasan2').css('display','none');
                $('#alasan-dikembalikan2').css('display','block');
            });

            // DDN
            // belum direkomendasikan
            $('#checkbox1_83').click(function(){
                $('#nominal-rekomendasi3,#alasan3,#alasan-dikembalikan3').css('display','none');
            });
            // sudah direkomendasikan
            $('#checkbox1_93').click(function(){
                $('#nominal-rekomendasi3').css('display','block');
                $('#alasan3').css('display','none');
                $('#alasan-dikembalikan3').css('display','none');
            });
            // ditolak disertai alasan
            $('#checkbox1_103').click(function(){
                $('#nominal-rekomendasi3').css('display','none');
                $('#alasan3').css('display','block');
                $('#alasan-dikembalikan3').css('display','none');
            });
            // dikembalikan kelembaga untuk diperbaiki
            $('#checkbox1_113').click(function(){
                $('#nominal-rekomendasi3').css('display','none');
                $('#alasan3').css('display','none');
                $('#alasan-dikembalikan3').css('display','block');
            });

            // APBD
            // belum direkomendasikan
            $('#checkbox1_84').click(function(){
                $('#nominal-rekomendasi4,#alasan4,#alasan-dikembalikan4').css('display','none');
            });
            // sudah direkomendasikan
            $('#checkbox1_94').click(function(){
                $('#nominal-rekomendasi4').css('display','block');
                $('#alasan4').css('display','none');
                $('#alasan-dikembalikan4').css('display','none');
            });
            // ditolak disertai alasan
            $('#checkbox1_104').click(function(){
                $('#nominal-rekomendasi4').css('display','none');
                $('#alasan4').css('display','block');
                $('#alasan-dikembalikan4').css('display','none');
            });
            // dikembalikan kelembaga untuk diperbaiki
            $('#checkbox1_114').click(function(){
                $('#nominal-rekomendasi4').css('display','none');
                $('#alasan4').css('display','none');
                $('#alasan-dikembalikan4').css('display','block');
            });
        });
    </script>

    <script type="text/javascript">
        $(function(){
            // numeral
            new Cleave('.input-numeral', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            // proses anggaran
            new Cleave('.input-numeral2', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral3', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral4', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral5', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral6', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            new Cleave('.input-numeral7', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });

            // tapd
            @if ($recProposalByID->is_penebalan_nominal_tapd==1)
            new Cleave('.input-numeral-tapd', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif

            // banggar
            @if ($recProposalByID->is_penebalan_nominal_banggar==1)
            new Cleave('.input-numeral-banggar', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif

            // ddn
            @if ($recProposalByID->is_penebalan_nominal_ddn==1)
            new Cleave('.input-numeral-ddn', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif

            // apbd
            @if ($recProposalByID->is_penebalan_nominal_apbd==1)
            new Cleave('.input-numeral-apbd', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralIntegerScale: 15,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
            @endif
        });
    </script>
   
@stop