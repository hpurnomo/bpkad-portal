<div class="tab-pane fade" id="tab_10_2">
                                @if (empty($recProposalPenebalanBANGGARByUUID))
                                    <h5 style="color: red;font-weight: bolder;">*Belum ada proposal yang dikirim oleh Lembaga.</h5>
                                @else
<div class="portlet-body form">
    <a href="{{ route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>'banggar','tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
    <hr>
    <form action="{{ route('skpd.proposal.updateBANGGAR') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
    {{ csrf_field() }}
        <input type="hidden" name="uuid_proposal_pengajuan" value="{{ $recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan }}">
        <input type="hidden" name="jdl_prop" value="{{$recProposalPenebalanBANGGARByUUID->jdl_prop}}">
        <input type="hidden" name="proposalID" value="{{ $proposalID }}">
        <input type="hidden" name="fase" value="{{$fase}}">
        <input type="hidden" name="nm_lembaga" value="{{$recProposalPenebalanBANGGARByUUID->nm_lembaga}}">
        <input type="hidden" name="no_lembaga" value="{{$recProposalPenebalanBANGGARByUUID->no_lembaga}}">
        <input type="hidden" name="email_lembaga" value="{{$recProposalPenebalanBANGGARByUUID->email_lembaga}}">
        <div class="form-group form-md-radios">
            <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
            <div class="col-md-9">
                <div class="md-radio-inline">
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_82" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==0)?'checked':'' }} {{ ( !empty($recProposalPenebalanBANGGARByUUID->nominal_rekomendasi) && $recProposalPenebalanBANGGARByUUID->status_prop==1 )?'disabled':'' }}>
                        <label for="checkbox1_82">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Menunggu </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_112" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==3)?'checked':'' }} {{ ( !empty($recProposalPenebalanBANGGARByUUID->nominal_rekomendasi) && $recProposalPenebalanBANGGARByUUID->status_prop==1 )?'disabled':'' }}>
                        <label for="checkbox1_112">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Dikoreksi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_92" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==1)?'checked':'' }}>
                        <label for="checkbox1_92">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Sudah Direkomendasi </label>
                    </div>
                    <div class="md-radio">
                        <input type="radio" id="checkbox1_102" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalPenebalanBANGGARByUUID->status_prop==2)?'checked':'' }} {{ ( !empty($recProposalPenebalanBANGGARByUUID->nominal_rekomendasi) && $recProposalPenebalanBANGGARByUUID->status_prop==1 )?'disabled':'' }}>
                        <label for="checkbox1_102">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> Di Tolak </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">
            </label>
            <div class="col-md-7">
                <hr>
            </div>
        </div>
        {{-- for status diterima --}}
        <div id="nominal-rekomendasi2" @if($recProposalPenebalanBANGGARByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==2) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Kategori
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="kategoriProposalID">
                        <option value="">Pilih Kategori</option>
                        @foreach($recAllKategoriProposal as $key=>$value)
                            <option value="{{ $value->id }}" {{ ($recProposalByID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Program
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_prg">
                        <option value="">Pilih Program</option>
                        @foreach($recAllProgram as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Pilih Rekening
                </label>
                <div class="col-md-7">
                    <select class="form-control select2me" name="no_rek">
                        <option value="">Pilih Rekening</option>
                        @foreach($recAllRekening as $key=>$value)
                            <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                <div class="col-md-6">
                    <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral-banggar" value="{{ str_replace(".", ",", $recProposalPenebalanBANGGARByUUID->nominal_rekomendasi) }}">
                    @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                    <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                    @else
                    <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                <div class="col-md-4">
                    <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_rekomendasi }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_rekomendasi }}">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                <div class="col-md-4">
                    <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_survei_lap }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_survei_lap }}">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_kelengkapan_administrasi }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_kelengkapan_administrasi }}">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                <div class="col-md-4">
                    <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalPenebalanBANGGARByUUID->no_surat_skpd }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                <div class="col-md-4">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalPenebalanBANGGARByUUID->tgl_surat_skpd }}">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Pilih tanggal </span>
                </div>
            </div>
            @if($recDetailPeriodeTahapanByNoper[1]->status==1)
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1"></label>
                <div class="col-md-5">
                    <button type="submit" class="btn green">Simpan</button>
                </div>
            </div>
            @endif
        </div>
        

        {{-- for status ditolak --}}
        <div id="alasan2" @if($recProposalPenebalanBANGGARByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==3) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                <div class="col-md-5">
                    <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalPenebalanBANGGARByUUID->alasan_ditolak }}"> 
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1"></label>
                <div class="col-md-5">
                    <button type="submit" class="btn green">Simpan</button>
                </div>
            </div>
        </div>

        <div id="alasan-dikembalikan2" @if($recProposalPenebalanBANGGARByUUID->status_prop==0) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==1) style="display:none;" @elseif($recProposalPenebalanBANGGARByUUID->status_prop==2) style="display:none;" @endif>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                <div class="col-md-5">
                    <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalPenebalanBANGGARByUUID->alasan_dikembalikan}}"> 
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="form_control_1"></label>
                <div class="col-md-5">
                    <button type="submit" class="btn green">Kirim</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-eye"></i>Detil Proposal BANGGAR</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_012" data-toggle="tab">1. Data Proposal </a>
                            </li>
                            <li>
                                <a href="#tab_1_112" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                            </li>
                        </ul>
                    <div class="tab-content">
                        
                        {{-- Data Proposal --}}
                        <div class="tab-pane fade active in" id="tab_1_012">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body" style="padding: 0;margin: 0;">
                                    <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalPenebalanBANGGARByUUID->tgl_prop)) }} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanBANGGARByUUID->no_proposal }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{ $recProposalPenebalanBANGGARByUUID->jdl_prop }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Isi Proposal</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->latar_belakang !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->maksud_tujuan !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {!! $recProposalPenebalanBANGGARByUUID->keterangan_prop !!} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section">Nominal Permohonan</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                <div class="col-md-8">
                                                    <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanBANGGARByUUID->nominal_pengajuan,2,',','.') }} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="form-section">Data Pendukung</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasPernyataanTanggungJawabPenebalan','BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan')
                                                    @foreach($berkasPernyataanTanggungJawabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasKepengurusanPenebalan','BPKAD\Http\Entities\BerkasKepengurusanPenebalan')
                                                    @foreach($berkasKepengurusanPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                <div class="col-md-9">
                                                    @inject('fotoProposalPenebalan','BPKAD\Http\Entities\FotoProposalPenebalan')
                                                    @foreach($fotoProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasProposalPenebalan','BPKAD\Http\Entities\BerkasProposalPenebalan')
                                                    @foreach($berkasProposalPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRabPenebalan','BPKAD\Http\Entities\BerkasRabPenebalan')
                                                    @foreach($berkasRabPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                <div class="col-md-9">
                                                    @inject('berkasRekeningPenebalan','BPKAD\Http\Entities\BerkasRekeningPenebalan')
                                                    @foreach($berkasRekeningPenebalan->where('uuid_proposal_pengajuan',$recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan)->where('tahap','banggar')->get() as $key=>$value)
                                                    <p class="form-control-static"> 
                                                        <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                        {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                <div class="col-md-9">
                                                    <div class="portlet-body">
                                                        <div class="input-group">
                                                            <input type="hidden" class="form-control" id="pac-input-banggar" placeholder="Cari Alamat">
                                                            {{-- <span class="input-group-btn">
                                                                <button class="btn blue" id="gmap_geocoding_btn">
                                                                    <i class="fa fa-search"></i>
                                                            </span> --}}
                                                        </div>
                                                        <div id="gmap_basic_banggar" class="gmaps"> </div>
                                                        <b>Latitude : </b>
                                                        <input type="text" class="form-control" name="latitude" id="latValBANGGAR" value="{{$recProposalPenebalanBANGGARByUUID->latitude}}" >
                                                        <b>Longitude : </b>
                                                        <input type="text" class="form-control" name="longitude" id="longValBANGGAR" value="{{$recProposalPenebalanBANGGARByUUID->longitude}}" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                        
                        {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                        <div class="tab-pane fade" id="tab_1_112">
                            <div class="col-md-12">
                                <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                @if($recProposalPenebalanBANGGARByUUID->nominal_pengajuan != $rencana_anggaran_banggar)
                                <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                @else
                                <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($recProposalPenebalanBANGGARByUUID->nominal_pengajuan,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Rp. {{ number_format($rencana_anggaran_banggar,2,',','.') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <br><br><br>
                                <!--/row-->
                                            <?php 
                                                $tahap='banggar';
                                                $status='';
                                            ?>
                                             <form  action="{{ route('member.proposal.updateAll.rincian.penebalan') }}" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
                                            <input type="hidden" name="no_prop" value="{{ $recProposalPenebalanBANGGARByUUID->no_prop }}" /> 
                                            <input type="hidden" name="uuid" value="{{ $recProposalPenebalanBANGGARByUUID->uuid_proposal_pengajuan }}" /> 

                                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                                <thead>
                                                      <tr>
                                                        <th> No. </th>
                                                        <th> Rencana Kegiatan/Rincian </th>
                                                        <th> Vol/Satuan (jumlah) </th>
                                                        <th> Harga Satuan (Rp) </th>
                                                        <th> Total Pengajuan (Rp) </th>
                                                        <th> Total Rekomendasi (Rp) </th> 
                                                        <!-- <th> Total Banggar (Rp) </th>  -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($recRencanaByUUIDBANGGAR)==0)
                                                        <tr>
                                                            <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                        </tr>
                                                    @else
                                                        @inject('metrics', 'BPKAD\Services\RincianPenebalanService')
                                                        @foreach ($recRencanaByUUIDBANGGAR as $key=>$value)
                                                           <td>{{$key+1}}</td>
                                                            <td> {{$value->rencana}} </b></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><b>{{ number_format($value->rencana_anggaran_pengajuan,2,",",".")}}</b></td>
                                                            <td><b>{{ number_format($value->rencana_anggaran_rekomendasi,2,",",".")}}</b></td> 
                                                            <!-- <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td> -->
                                                        </tr>
                                                        {!! $metrics->outputByRencanaIDRekomendasi($value->id,$fase,$noper,$uuid,$tahap,$status) !!}
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
 
                                             @if($recProposalPenebalanBANGGARByUUID->noper>= 11 )
                                             <button style="margin: 10px" type="submit" class="btn green-meadow">Simpan RAB  </button>  
                                             @endif

                                        </form>

                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_012" data-toggle="tab">1. Kelengkapan Administrasi </a>
                    </li>
                    <li>
                        <a href="#tab_2_112" data-toggle="tab">2. Peninjauan Lapangan </a>
                    </li>
                </ul>

                <div class="tab-content">
                    {{-- Kelengkapan Administrasi --}}
                    <div class="tab-pane fade active in" id="tab_2_012">
                        <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="banggar">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Administrasi</center></th>
                                                    <th><center>Ada</center></th>
                                                    <th><center>Tidak Ada</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_aktivitas11112" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_aktivitas21112" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_kepengurusan11112" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_kepengurusan21112" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_rab11112" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_rab21112" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_waktu_pelaksanaan11112" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan11112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_waktu_pelaksanaan21112" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan21112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                            @if(!empty($statusAdministrasiLembaga) && !empty($statusLapanganLembaga))
                                @if($statusLapanganLembaga->is_approve == null || $statusLapanganLembaga->is_approve == 0)
                                    <b class="text-danger blink">Lembaga "{{$recProposalByID->nm_lembaga}}" belum lolos tahap "CHECKLIST KESESUAIAN DATA ANTARA DOKUMEN TERTULIS DAN SOFTCOPY". <div class="blink"> Mohon periksa kembali Lembaga tersebut.</div>
                                    </b>
                                    <a href="{{ route('skpd.lembaga.show',['noLembaga'=>$recProposalByID->no_lembaga]) }}">Periksa Lembaga</a>
                                @else
                                <center>
                                    <input type="checkbox" name="is_approve"
                                        @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                                @endif
                            @else
                                <b class="text-danger">Lembaga "{{$recProposalByID->nm_lembaga}}" belum lolos tahap "CHECKLIST KESESUAIAN DATA ANTARA DOKUMEN TERTULIS DAN SOFTCOPY".<div class="blink"> Mohon periksa kembali Lembaga tersebut.</div>
                                </b>
                                <a href="{{ route('skpd.lembaga.show',['noLembaga'=>$recProposalByID->no_lembaga]) }}">Periksa Lembaga</a>
                            @endif
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-11">
                                        <button type="submit" class="btn green-meadow">Kirim</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    
                    {{-- Kelengkapan Lapangan --}}
                    <div class="tab-pane fade" id="tab_2_112">
                        @if(isset($recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                        <form action="{{ route('skpd.proposal.penebalan.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                        <div class="form-group">
                            <div class="form-body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                <input type="hidden" name="fase" value="{{ $fase }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="tahap" value="banggar">
                                <div class="form-group form-md-radios">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="50%"><center>Data Lapangan</center></th>
                                                    <th><center>Sesuai</center></th>
                                                    <th><center>Tidak Sesuai</center></th>
                                                    <th><center>Keterangan</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Aktivitas</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_aktivitas111112" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_aktivitas221112" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_aktivitas==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_aktivitas221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas))
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_aktivitas }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Kepengurusan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_kepengurusan111112" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_kepengurusan221112" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_kepengurusan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_kepengurusan221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan))
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_kepengurusan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Rencana Anggaran Biaya (RAB)</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_rab111112" name="cek_rab" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_rab221112" name="cek_rab" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_rab==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_rab221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab))
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_rab }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Waktu Pelaksanaan</td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_waktu_pelaksanaan111112" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==1)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan111112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="md-radio">
                                                            <input type="radio" id="cek_waktu_pelaksanaan221112" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                            @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->cek_waktu_pelaksanaan==2)
                                                            checked
                                                            @endif
                                                            >
                                                            <label for="cek_waktu_pelaksanaan221112">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> 
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan))
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                    @else
                                                    <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                    @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <input type="checkbox" name="is_approve"
                                        @if(isset($recProposalLapanganPenebalanBANGGARByUUIDAndTahap->is_approve) && $recProposalLapanganPenebalanBANGGARByUUIDAndTahap->is_approve==1)
                                        checked
                                        @endif
                                    > 
                                    <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                </center>
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-11">
                                        <button type="submit" class="btn green-meadow">Kirim</button>
                                        <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        @else
                        <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                                @endif
                            </div>
