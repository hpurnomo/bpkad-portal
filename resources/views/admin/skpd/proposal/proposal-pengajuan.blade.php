<div class="tab-pane fade active in" id="tab_10_0">
    <div class="portlet-body form">
        <a href="{{ route('skpd.cetak.show',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase,'tab'=>'kelengkapan-administrasi']) }}" class="btn btn-circle green btn-outline sbold uppercase" style="margin-top: 10px;"><i class="fa fa-print" aria-hidden="true"></i> Cetak Laporan/Berita Acara</a>
        <hr>
        <form action="{{ route('skpd.proposal.update') }}" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
        {{ csrf_field() }}
            <input type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
            <input type="hidden" name="jdl_prop" value="{{$recProposalByID->jdl_prop}}">
            <input type="hidden" name="proposalID" value="{{ $proposalID }}">
            <input type="hidden" name="fase" value="{{$fase}}">
            <input type="hidden" name="nm_lembaga" value="{{$recProposalByID->nm_lembaga}}">
            <input type="hidden" name="no_lembaga" value="{{$recProposalByID->no_lembaga}}">
            <input type="hidden" name="email_lembaga" value="{{$recProposalByID->email_lembaga}}">
            <input type="hidden" name="email" value="{{$recProposalByID->emailKoordinator}}">
            <input type="hidden" name="real_name" value="{{$recProposalByID->real_name}}">
            <div class="form-group form-md-radios">
                <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
                <div class="col-md-9">
                    <div class="md-radio-inline">
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_8" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalByID->status_prop==0)?'checked':'' }} {{ ( !empty($recProposalByID->nominal_rekomendasi) && $recProposalByID->status_prop==1 )?'disabled':'' }}>
                            <label for="checkbox1_8">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Menunggu </label>
                        </div>
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_11" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalByID->status_prop==3)?'checked':'' }} {{ ( !empty($recProposalByID->nominal_rekomendasi) && $recProposalByID->status_prop==1 )?'disabled':'' }}>
                            <label for="checkbox1_11">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Dikoreksi </label>
                        </div>
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_9" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalByID->status_prop==1)?'checked':'' }}>
                            <label for="checkbox1_9">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Sudah Direkomendasi </label>
                        </div>
                        <div class="md-radio">
                            <input type="radio" id="checkbox1_10" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalByID->status_prop==2)?'checked':'' }} {{ ( !empty($recProposalByID->nominal_rekomendasi) && $recProposalByID->status_prop==1 )?'disabled':'' }}>
                            <label for="checkbox1_10">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> Di Tolak </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">
                </label>
                <div class="col-md-7">
                    <hr>
                </div>
            </div>
            {{-- for status diterima --}}
            <div id="nominal-rekomendasi" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==2) style="display:none;" @elseif($recProposalByID->status_prop==3) style="display:none;" @endif>
                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Kategori
                    </label>
                    <div class="col-md-7">
                        <select class="form-control select2me" name="kategoriProposalID">
                            <option value="">Pilih Kategori</option>
                            @foreach($recAllKategoriProposal as $key=>$value)
                                <option value="{{ $value->id }}" {{ ($recProposalByID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Program
                    </label>
                    <div class="col-md-7">
                        <select class="form-control select2me" name="no_prg">
                            <option value="">Pilih Program</option>
                            @foreach($recAllProgram as $key=>$value)
                                <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->kd_program }} {{ $value->nm_program }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Rekening
                    </label>
                    <div class="col-md-7">
                        <select class="form-control select2me" name="no_rek">
                            <option value="">Pilih Rekening</option>
                            @foreach($recAllRekening as $key=>$value)
                                <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->kd_rekening }} {{ $value->nm_rekening }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    @if($recProposalByID->noper >= 10)
                    <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                    <div class="col-md-6">
                        <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral" value="{{ str_replace(".", ",", $recProposalByID->nominal_rekomendasi) }}" readonly="true">
                        @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                        <span class="help-block" style="color: red;"><i>Nilai Rekomendasi akan otomatis dijumlahkan dari nominal RAB hasil koreksi SKPD Koordinator.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                        @endif
                    </div>
                    @else
                      <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi (Rp): </label>
                        <div class="col-md-6">
                        <input name="nominal_rekomendasi" type="text" data-required="1" class="form-control input-numeral" value="{{ str_replace(".", ",", $recProposalByID->nominal_rekomendasi) }}">
                        @if($recDetailPeriodeTahapanByNoper[1]->status==1)
                        <span class="help-block" style="color: red;"><i>Jika Anda sudah memasukan nilai rekomendasi status proposal tidak dapat di ubah. Hapus nilai rekomendasi untuk tetap dapat mengubah status proposal.</i></span>
                        @else
                        <span class="help-block" style="color: red;"><i>Pengajuan nilai rekomendasi saat ini telah ditutup.</i></span>
                        @endif
                    </div> 
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                    <div class="col-md-4">
                        <input name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_rekomendasi }}">
                    </div>
                    <span class="help-block" style="color: red;"><i>otomatis terkirim ke cetak berita acara.</i></span>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalByID->tgl_rekomendasi }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                    <div class="col-md-4">
                        <input name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_survei_lap }}">
                    </div>
                    <span class="help-block" style="color: red;"><i>otomatis terkirim ke cetak berita acara.</i></span>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalByID->tgl_survei_lap }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Kelengkapan Administrasi: </label>
                    <div class="col-md-4">
                        <input name="no_kelengkapan_administrasi" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_kelengkapan_administrasi }}">
                    </div>
                    <span class="help-block" style="color: red;"><i>otomatis terkirim ke cetak berita acara.</i></span>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Kelengkapan Administrasi: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_kelengkapan_administrasi" value="{{ $recProposalByID->tgl_kelengkapan_administrasi }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                    </label>
                    <div class="col-md-7">
                        <hr>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                    <div class="col-md-4">
                        <input name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_surat_skpd }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                    <div class="col-md-4">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalByID->tgl_surat_skpd }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                        <span class="help-block"> Pilih tanggal </span>
                    </div>
                </div> 
                <!-- herebro  -->
                @if($periodeDetail->pengajuan_prop=='Y' && $periodeDetail->status ==1)
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1"></label>
                        <div class="col-md-5">
                            <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                        </div>
                    </div>  
                @endif

                 {{-- 
                   
                    @if($recProposalByID->noper == 10)
                      <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"></label>
                                <div class="col-md-5">
                                    <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                                </div>
                            </div>  
                    @endif 
                   
                 
                        @if($recProposalByID->status_prop == 0) 
                        <hr>
                        <h4 style="color: red;text-align: center;">Mohon maaf fase telah ditutup</h4> 
                        @elseif($recProposalByID->status_prop !=1 && $statusRekKolektif == 0)
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"></label>
                                <div class="col-md-5">
                                    <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                                </div>
                            </div>  
                        @endif 
                   
                        @if($recProposalByID->status_prop !=1 && $statusRekKolektif == 0)
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"></label>
                                <div class="col-md-5">
                                    <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                                </div>
                            </div>  
                        @endif -->
                        --}}
                   
                     
            </div>
            

            {{-- for status ditolak --}}
            <div id="alasan" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==1) style="display:none;" @elseif($recProposalByID->status_prop==3) style="display:none;" @endif>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                    <div class="col-md-5">
                        <input name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalByID->alasan_ditolak }}"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-5">
                        <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    </div>
                </div>
            </div>

            <div id="alasan-dikembalikan" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==1) style="display:none;" @elseif($recProposalByID->status_prop==2) style="display:none;" @endif>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                    <div class="col-md-5">
                        <input name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value="{{$recProposalByID->alasan_dikembalikan}}"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="form_control_1"></label>
                    <div class="col-md-5">
                        <button type="submit" class="btn green">Simpan & Kirim Notifikasi ke-Email Lembaga</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-eye"></i>Detil Proposal Usulan</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_0" data-toggle="tab">1. Data Proposal </a>
                                </li>
                                <li>
                                    <a href="#tab_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                
                                {{-- Data Proposal --}}
                                <div class="tab-pane fade active in" id="tab_1_0">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body" style="padding: 0;margin: 0;">
                                            <h3 class="form-section">Detil Lembaga</h3>
                                            @inject('metrics', 'BPKAD\Services\LembagaService')
                                            {{-- {{ dd($metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkota) }} --}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Nama Lembaga :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nm_lembaga }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Alamat Lembaga :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->alamat }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>RT/RW :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->RT }}/{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->RW }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Kelurahan :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkel }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Kecamatan :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkec }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-6"><b>Kota :</b></label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkota }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalByID->tgl_prop)) }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalByID->no_proposal }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {{ $recProposalByID->jdl_prop }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Isi Proposal</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalByID->latar_belakang !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                             <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalByID->maksud_tujuan !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> {!! $recProposalByID->keterangan_prop !!} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Nominal Permohonan</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="form-section">Data Pendukung</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawab')
                                                            @foreach($berkasPernyataanTanggungJawab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusan')
                                                            @foreach($berkasKepengurusan->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('fotoProposal','BPKAD\Http\Entities\FotoProposal')
                                                            @foreach($fotoProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposal')
                                                            @foreach($berkasProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRab','BPKAD\Http\Entities\BerkasRab')
                                                            @foreach($berkasRab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                        <div class="col-md-9">
                                                            @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekening')
                                                            @foreach($berkasRekening->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                            <p class="form-control-static"> 
                                                                <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                        <div class="col-md-9">
                                                            <div class="portlet-body">
                                                                <div class="input-group">
                                                                    <input type="hidden" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                                    {{-- <span class="input-group-btn">
                                                                        <button class="btn blue" id="gmap_geocoding_btn">
                                                                            <i class="fa fa-search"></i>
                                                                    </span> --}}
                                                                </div>
                                                                <div id="gmap_basic" class="gmaps"> </div>
                                                                <b>Latitude : </b>
                                                                <input type="text" class="form-control" name="latitude" id="latVal" value="{{$recProposalByID->latitude}}"  >
                                                                <b>Longitude : </b>
                                                                <input type="text" class="form-control" name="longitude" id="longVal" value="{{$recProposalByID->longitude}}"  >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                
                                {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                <div class="tab-pane fade" id="tab_1_1">
                                    <div class="col-md-12">
                                        <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                        @if($recProposalByID->nominal_pengajuan != $rencana_anggaran)
                                        <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                        @else
                                        <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static">Rp. {{ number_format($rencana_anggaran,2,',','.') }} </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"><b>Nilai Rekomendasi RAB (Rp):</b></label>
                                                    <div class="col-md-8">
                                                        @if($recProposalByID->nominal_rekomendasi != "")
                                                        <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_rekomendasi,2,',','.') }} </p>
                                                        @endif 
                                                    </div>
                                                </div>
                                            </div> 
                                            <!--/span-->
                                        </div>
                                        <br><br><br>
                                        <!--/row-->
                                        {{-- <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{ route('member.proposal.create.rencana',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase]) }}" class="btn sbold green"> Buat Rencana Kegiatan
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        
                                        <form  action="{{ route('member.proposal.updateAll.rincian') }}" method="POST">  
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
                                        <input type="hidden" name="no_prop" value="{{ $no_prop }}" /> 
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                            <thead>
                                                <tr>
                                                    <th> Nos. </th>
                                                    <th> Rencana Kegiatan/Rincian </th>
                                                    <th> Vol/Satuan (jumlah) </th>
                                                    <th> Harga Satuan Pengajuan (Rp) </th>
                                                    <th> Anggaran Pengajuan (Rp) </th>
                                                    {{-- <th> Harga Satuan Rekomendasi (Rp) </th> --}} 
                                                    @if($recProposalByID->noper >= 10 )
                                                    <th> Anggaran Rekomendasi  (Rp) </th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($recRencanaByNoProp)==0)
                                                    <tr>
                                                        <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                    </tr>
                                                @else
                                                    @inject('metrics', 'BPKAD\Services\RincianService')
                                                    @foreach ($recRencanaByNoProp as $key=>$value)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td><b>{{$value->rencana}}</b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td> 
                                                        @if($recProposalByID->noper >= 10 )
                                                        <td><b>{{ number_format($value->rencana_anggaran_skpd,2,",",".")}}</b></td>
                                                        @endif 

                                                    </tr>
                                                    {!! $metrics->outputByRencanaID($value->id,$value->no_prop,$fase) !!}
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        
                                        <!--  herebro --> 
                                         @if(Auth::user()->group_id==10)
                                            
                                            @if($periodeDetail->rek_anggaran=='Y' && $periodeDetail->status ==1) 
                                                <button style="margin: 10px" type="submit" class="btn green-meadow">Simpan Rekomendasi Anggaran</button>  
                                            @endif

                                        @endif 
                                       <!--    -->
                                         <br>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_2_0" data-toggle="tab">1. Kelengkapan Administrasi </a>
                        </li>
                        <li>
                            <a href="#tab_2_1" data-toggle="tab">2. Peninjauan Lapangan </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        {{-- Kelengkapan Administrasi --}}
                        <div class="tab-pane fade active in" id="tab_2_0">
                            <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018). Hasil dari checklist kesesuaian data ini akan otomatis terlampir di cetak berita acara kelengkapan administrasi :</span></span>
                            <form action="{{ route('skpd.proposal.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-group">
                                <div class="form-body">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                    <input type="hidden" name="fase" value="{{ $fase }}">
                                    <div class="form-group form-md-radios">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th width="50%"><center>Data Administrasi</center></th>
                                                        <th><center>Ada</center></th>
                                                        <th><center>Tidak Ada</center></th>
                                                        <th><center>Keterangan</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                     <tr>
                                                        <td><b>B</b></td>
                                                        <td colspan="4"><b>Kelengkapan proposal usulan hibah/bansos:</b></td> 
                                                    </tr>  

                                                     <tr>
                                                        <td>1</td>
                                                        <td>Identitas dan alamat pengusul</td>
                                                        <td> 
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_identitas_alamat1" name="cek_identitas_alamat" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_identitas_alamat) && $recProposalAdministrasiByNoProp->cek_identitas_alamat==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_identitas_alamat1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td> 
                                                             <div class="md-radio">
                                                                <input type="radio" id="cek_identitas_alamat2" name="cek_identitas_alamat" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_identitas_alamat) && $recProposalAdministrasiByNoProp->cek_identitas_alamat==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_identitas_alamat2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->ket_identitas_alamat))
                                                            <textarea name="ket_identitas_alamat" class="form-control" rows="2">{{ $recProposalAdministrasiByNoProp->ket_identitas_alamat }}</textarea>
                                                            @else
                                                            <textarea name="ket_identitas_alamat" class="form-control" rows="2"></textarea>
                                                            @endif
                                                            </td>
                                                        </td>
                                                    </tr> 

                                                     <tr>
                                                        <td>2</td>
                                                        <td>Latar Belakang</td>
                                                       <td> 
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_latar_belakang1" name="cek_latar_belakang" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_latar_belakang) && $recProposalAdministrasiByNoProp->cek_latar_belakang==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_latar_belakang1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td> 
                                                             <div class="md-radio">
                                                                <input type="radio" id="cek_latar_belakang2" name="cek_latar_belakang" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_latar_belakang) && $recProposalAdministrasiByNoProp->cek_latar_belakang==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_latar_belakang2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->ket_latar_belakang))
                                                            <textarea name="ket_latar_belakang" class="form-control" rows="2">{{ $recProposalAdministrasiByNoProp->ket_latar_belakang }}</textarea>
                                                            @else
                                                            <textarea name="ket_latar_belakang" class="form-control" rows="2"></textarea>
                                                            @endif
                                                            </td>
                                                        </td>
                                                    </tr> 

                                                    <tr>
                                                        <td>3</td>
                                                        <td>Maksud dan tujuan</td>
                                                          <td> 
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_maksud_tujuan1" name="cek_maksud_tujuan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_maksud_tujuan) && $recProposalAdministrasiByNoProp->cek_maksud_tujuan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_maksud_tujuan1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td> 
                                                             <div class="md-radio">
                                                                <input type="radio" id="cek_maksud_tujuan2" name="cek_maksud_tujuan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_maksud_tujuan) && $recProposalAdministrasiByNoProp->cek_maksud_tujuan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_maksud_tujuan2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->ket_maksud_tujuan))
                                                            <textarea name="ket_maksud_tujuan" class="form-control" rows="2">{{ $recProposalAdministrasiByNoProp->ket_maksud_tujuan }}</textarea>
                                                            @else
                                                            <textarea name="ket_maksud_tujuan" class="form-control" rows="2"></textarea>
                                                            @endif
                                                            </td>
                                                        </td>
                                                    </tr> 

                                                    <tr>
                                                        <td>4</td>
                                                        <td>Rincian rencana kegiatan (Jadwal pelaksanaan kegiatan)</td>
                                                        <td> 
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_jadwal_pelaksanaan1" name="cek_jadwal_pelaksanaan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_jadwal_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_jadwal_pelaksanaan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_jadwal_pelaksanaan1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td> 
                                                             <div class="md-radio">
                                                                <input type="radio" id="cek_jadwal_pelaksanaan2" name="cek_jadwal_pelaksanaan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_jadwal_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_jadwal_pelaksanaan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_jadwal_pelaksanaan2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->ket_jadwal_pelaksanaan))
                                                            <textarea name="ket_jadwal_pelaksanaan" class="form-control" rows="2">{{ $recProposalAdministrasiByNoProp->ket_jadwal_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea name="ket_jadwal_pelaksanaan" class="form-control" rows="2"></textarea>
                                                            @endif
                                                            </td>
                                                        </td>
                                                    </tr> 

                                                     <tr>
                                                        <td>5</td>
                                                        <td>Rincian rencana penggunaan hibah/bansos (Rincian Anggaran Biaya)</td>
                                                       <td> 
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rencana_rab1" name="cek_rencana_rab" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_rencana_rab) && $recProposalAdministrasiByNoProp->cek_rencana_rab==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rencana_rab1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td> 
                                                             <div class="md-radio">
                                                                <input type="radio" id="cek_rencana_rab2" name="cek_rencana_rab" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_rencana_rab) && $recProposalAdministrasiByNoProp->cek_rencana_rab==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rencana_rab2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->ket_rencana_rab))
                                                            <textarea name="ket_rencana_rab" class="form-control" rows="2">{{ $recProposalAdministrasiByNoProp->ket_rencana_rab }}</textarea>
                                                            @else
                                                            <textarea name="ket_rencana_rab" class="form-control" rows="2"></textarea>
                                                            @endif
                                                            </td>
                                                        </td>
                                                    </tr> 
                                                    {{--

                                                    <tr>
                                                        <td>1</td>
                                                        <td>Aktivitas</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas1" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_aktivitas) && $recProposalAdministrasiByNoProp->cek_aktivitas==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas2" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_aktivitas) && $recProposalAdministrasiByNoProp->cek_aktivitas==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_aktivitas))
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalAdministrasiByNoProp->keterangan_cek_aktivitas }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Kepengurusan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan1" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_kepengurusan) && $recProposalAdministrasiByNoProp->cek_kepengurusan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan2" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_kepengurusan) && $recProposalAdministrasiByNoProp->cek_kepengurusan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_kepengurusan))
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalAdministrasiByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Rencana Anggaran Biaya (RAB)</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab1" name="cek_rab" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_rab) && $recProposalAdministrasiByNoProp->cek_rab==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab2" name="cek_rab" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_rab) && $recProposalAdministrasiByNoProp->cek_rab==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_rab))
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalAdministrasiByNoProp->keterangan_cek_rab }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Waktu Pelaksanaan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan1" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan2" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_waktu_pelaksanaan))
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalAdministrasiByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>

                                                    --}}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                @if(!empty($statusAdministrasiLembaga) && !empty($statusLapanganLembaga))
                                    @if($statusLapanganLembaga->is_approve == null || $statusLapanganLembaga->is_approve == 0)
                                        <b class="text-danger blink">Lembaga "{{$recProposalByID->nm_lembaga}}" belum lolos tahap "CHECKLIST KESESUAIAN DATA ANTARA DOKUMEN TERTULIS DAN SOFTCOPY". <div class="blink"> Mohon periksa kembali Lembaga tersebut.</div>
                                        </b>
                                        <a href="{{ route('skpd.lembaga.show',['noLembaga'=>$recProposalByID->no_lembaga]) }}">Periksa Lembaga</a>
                                    @else
                                    <center>
                                        <input type="checkbox" name="is_approve"
                                            @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
                                            checked
                                            @endif
                                        > 
                                        <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                    </center>
                                    @endif
                                @else
                                    <b class="text-danger">Lembaga "{{$recProposalByID->nm_lembaga}}" belum lolos tahap "CHECKLIST KESESUAIAN DATA ANTARA DOKUMEN TERTULIS DAN SOFTCOPY".<div class="blink"> Mohon periksa kembali Lembaga tersebut.</div>
                                    </b>
                                    <a href="{{ route('skpd.lembaga.show',['noLembaga'=>$recProposalByID->no_lembaga]) }}">Periksa Lembaga</a>
                                @endif
                                </div>
                                <br>
                                <br>
                                <br>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-11">
                                            <button type="submit" class="btn green-meadow">Simpan {{-- & Kirim Notifikasi ke-Email Lembaga --}}</button>
                                            <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        
                        {{-- Peninjauan Lapangan --}}
                        <div class="tab-pane fade" id="tab_2_1">
                            @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018). Hasil dari checklist kesesuaian data ini akan otomatis terlampir di cetak berita acara peninjauan lapangan :</span></span>
                            <form action="{{ route('skpd.proposal.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                            <div class="form-group">
                                <div class="form-body">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                    <input type="hidden" name="fase" value="{{ $fase }}">
                                    <div class="form-group form-md-radios">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th width="50%"><center>Data Lapangan</center></th>
                                                        <th><center>Sesuai</center></th>
                                                        <th><center>Tidak Sesuai</center></th>
                                                        <th><center>Keterangan</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Aktivitas</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas11" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas11">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_aktivitas22" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_aktivitas22">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganByNoProp->keterangan_cek_aktivitas))
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5">{{ $recProposalLapanganByNoProp->keterangan_cek_aktivitas }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_aktivitas" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Kepengurusan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan11" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan11">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_kepengurusan22" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_kepengurusan22">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganByNoProp->keterangan_cek_kepengurusan))
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5">{{ $recProposalLapanganByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_kepengurusan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Rencana Anggaran Biaya (RAB)</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab11" name="cek_rab" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab11">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_rab22" name="cek_rab" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_rab22">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganByNoProp->keterangan_cek_rab))
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5">{{ $recProposalLapanganByNoProp->keterangan_cek_rab }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_rab" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Waktu Pelaksanaan</td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan11" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==1)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan11">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="md-radio">
                                                                <input type="radio" id="cek_waktu_pelaksanaan22" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==2)
                                                                checked
                                                                @endif
                                                                >
                                                                <label for="cek_waktu_pelaksanaan22">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> 
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        @if(isset($recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan))
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5">{{ $recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                        @else
                                                        <textarea name="keterangan_cek_waktu_pelaksanaan" class="form-control" rows="5"></textarea>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <input type="checkbox" name="is_approve"
                                            @if(isset($recProposalLapanganByNoProp->is_approve) && $recProposalLapanganByNoProp->is_approve==1)
                                            checked
                                            @endif
                                        > 
                                        <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                    </center>
                                </div>
                                <br>
                                <br>
                                <br>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-11">
                                            <button type="submit" class="btn green-meadow">Simpan {{-- & Kirim Notifikasi ke-Email Lembaga --}}</button>
                                            <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                            @else
                            <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
     function isInputNumber(evt){
                
        var ch = String.fromCharCode(evt.which);
        
        if(!(/[0-9]/.test(ch))){
            evt.preventDefault();
        }
        
    }
</script>