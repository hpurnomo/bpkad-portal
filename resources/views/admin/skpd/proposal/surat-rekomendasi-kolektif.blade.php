@extends('admin/layout/template')
@section('content')
<style type="text/css">
.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-100{
    width:100px;
}
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.index') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span> Surat Rekomendasi Kolektif </span>
                </li>
            </ul>
        </div> 
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> 
            <!-- Managed Datatables
            <small>managed datatable samples</small> -->
        </h3>
        <div class="row">
            <div class="col-md-12"> 
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Daftar </span>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                    <?php
                                        $date       = strtotime(gmdate("Y-m-d H:i:s", time()+60*60*7)); 
                                        $max_date   = strtotime("2019-06-15 23:59:00"); 
                                    ?>  

                                     @if($periodeDetail->rekomendasiKolektif=='Y' && $periodeDetail->status ==1)
                                        <a href="{{ route('skpd.proposal.surat.rekomendasi.kolektif.create',['noper'=>$noper]) }}" class="btn green " > <i class="fa fa-plus"></i> Buat Rekomendasi Kolektif Baru </a>  
                                    @endif
                                    
                               
                            </div>
                        </div>
                       
  
                       
                    </div>
                    <div class="portlet-body">
                        
                         <table class="table table-striped table-bordered table-hover table-checkable order-column display  nowrap" id="sample_3">
                            <thead>
                                <tr>
                                    <th> No. </th> 
                                    <th> Nomor Rekomendasi </th>
                                    <th> Tgl Rekomendasi </th>
                                    <th> Lembaga yang dilampirkan </th> 
                                    <th> Download </th> 
                                    <th> Action  </th> 
                                </tr>
                            </thead> 
                             <tbody>
                                @foreach($rekomendasiKolektif as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    <td> {{ $value->nomor }} </td>
                                    <td> {{ $value->tanggal }} {{ $value->bulan }} {{ $value->tahun }}</td> 
                                    <td> 
                                        <?php 
                                        $detail = DB::table("cetak_rekomendasi_kolektif_detail")->where("id_kol_head",$value->uuid)->get();
                                        
                                        $lembaga = '';
                                        $i =1;
                                        foreach ($detail as $row) { 
                                            $dataDetail = DB::table("mst_lembaga")->select("nm_lembaga")->where("nomor",$row->id_lembaga)->first();
                                            $lembaga .= $i. ". ".$dataDetail->nm_lembaga."<br>";
                                            $i++;
                                        }
                                          
                                        ?>
                                        {!! $lembaga !!}
                                    </td>
                                    <!-- <td></td> -->
                                    
                                        <td>
                                            <a href="{{ route('skpd.proposal.surat.rekomendasi.kolektif.cetak',['id'=>base64_encode($value->id)]) }}"> <i class="fa fa-download"></i> Download </a>
                                        </td>
                                    <!-- <td></td> -->
                                    
                                          <td>
                                            <a href="{{ route('skpd.proposal.surat.rekomendasi.kolektif.cancel',['id'=>base64_encode($value->id)]) }}" onclick="return confirm('Anda yakin untu menghapus rekomendasi kolektif ini?');"> <i class="fa fa-trash"></i> Batalkan Rekomendasi </a>
                                        </td> 
                                     
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {


        });
    </script>
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            // autosubmit
            $('#checkbox1_8,#checkbox1_9,#checkbox1_10').click(function(){
                $('form#form_sample_1').submit();
            });

            $(".btn-pdf").on("submit", function(){
                return confirm("Apakah Anda yakin ?");
            });
        });
    </script>
    @if( !empty(Session::get('status')) )
        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif

@stop