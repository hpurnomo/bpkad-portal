@extends('admin/layout/template')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:;"><i class="icon-layers"></i>Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{ route('skpd.proposal.manage') }}">Proposal</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Pengaturan Proposal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title">
        	<!-- Dashboard -->
            <!-- <small>dashboard & statistics</small> -->
        </h3>
        {{-- {{ dd($recProposalByID) }} --}}
        <div class="portlet-body form">
            <form action="" class="form-horizontal" id="form_sample_1" novalidate="novalidate" method="POST">
            {{ csrf_field() }}
                <input disabled="" type="hidden" name="no_prop" value="{{ $recProposalByID->no_prop }}">
                <input disabled="" type="hidden" name="proposalID" value="{{ $proposalID }}">
                <div class="form-group form-md-radios">
                    <label class="col-md-3 control-label" for="form_control_1">Ubah Status Proposal: </label>
                    <div class="col-md-9">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_8" name="status_prop" value="0" class="md-radiobtn" {{ ($recProposalByID->status_prop==0)?'checked':'' }} disabled="">
                                <label for="checkbox1_8">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Menunggu </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_11" name="status_prop" value="3" class="md-radiobtn" {{ ($recProposalByID->status_prop==3)?'checked':'' }} disabled="">
                                <label for="checkbox1_11">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Dikoreksi </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_9" name="status_prop" value="1" class="md-radiobtn" {{ ($recProposalByID->status_prop==1)?'checked':'' }} disabled="">
                                <label for="checkbox1_9">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Sudah Direkomendasi </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_10" name="status_prop" value="2" class="md-radiobtn" {{ ($recProposalByID->status_prop==2)?'checked':'' }} disabled="">
                                <label for="checkbox1_10">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Di Tolak </label>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- for status diterima --}}
                <div id="nominal-rekomendasi" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==2) style="display:none;" @elseif($recProposalByID->status_prop==3) style="display:none;" @endif>
                    <div class="form-group">
                        <label class="control-label col-md-3">Pilih Kategori
                        </label>
                        <div class="col-md-7">
                            <select disabled="" class="form-control select2me" name="kategoriProposalID">
                                <option value="">Pilih Kategori</option>
                                @foreach($recAllKategoriProposal as $key=>$value)
                                    <option value="{{ $value->id }}" {{ ($recProposalByID->kategoriProposalID==$value->id)?'selected':'' }}>{{ ucfirst($value->nama) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Pilih Program
                        </label>
                        <div class="col-md-7">
                            <select disabled="" class="form-control select2me" name="no_prg">
                                <option value="">Pilih Program</option>
                                @foreach($recAllProgram as $key=>$value)
                                    <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_prg==$value->nomor)?'selected':'' }}>{{ $value->nm_program }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Pilih Rekening
                        </label>
                        <div class="col-md-7">
                            <select disabled="" class="form-control select2me" name="no_rek">
                                <option value="">Pilih Rekening</option>
                                @foreach($recAllRekening as $key=>$value)
                                    <option value="{{ $value->nomor }}" {{ ($recProposalByID->no_rek==$value->nomor)?'selected':'' }}>{{ $value->nm_rekening }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Nominal Rekomendasi: </label>
                        <div class="col-md-4">
                            <input disabled="" name="nominal_rekomendasi" type="text" data-required="1" class="form-control only-numeric" value="{{ $recProposalByID->nominal_rekomendasi }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">No. Survei Lapangan: </label>
                        <div class="col-md-4">
                            <input disabled="" name="no_survei_lap" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_survei_lap }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Tgl. Survei Lapangan: </label>
                        <div class="col-md-4">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                <input disabled="" type="text" class="form-control" readonly name="tgl_survei_lap" value="{{ $recProposalByID->tgl_survei_lap }}">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                            <span class="help-block"> Pilih tanggal </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">No. Rekomendasi: </label>
                        <div class="col-md-4">
                            <input disabled="" name="no_rekomendasi" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_rekomendasi }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Tgl. Rekomendasi: </label>
                        <div class="col-md-4">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                <input disabled="" type="text" class="form-control" readonly name="tgl_rekomendasi" value="{{ $recProposalByID->tgl_rekomendasi }}">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                            <span class="help-block"> Pilih tanggal </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">No. Surat SKPD: </label>
                        <div class="col-md-4">
                            <input disabled="" name="no_surat_skpd" type="text" data-required="1" class="form-control" value="{{ $recProposalByID->no_surat_skpd }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Tgl. Surat SKPD: </label>
                        <div class="col-md-4">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                <input disabled="" type="text" class="form-control" readonly name="tgl_surat_skpd" value="{{ $recProposalByID->tgl_surat_skpd }}">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                            <span class="help-block"> Pilih tanggal </span>
                        </div>
                    </div>
                </div>
                

                {{-- for status ditolak --}}
                <div id="alasan" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==1) style="display:none;" @elseif($recProposalByID->status_prop==3) style="display:none;" @endif>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Alasan Ditolak: </label>
                        <div class="col-md-5">
                            <input disabled="" name="alasan_ditolak" type="text" data-required="1" class="form-control button-submit" value="{{ $recProposalByID->alasan_ditolak }}"> 
                        </div>
                    </div>
                </div>

                <div id="alasan-dikembalikan" @if($recProposalByID->status_prop==0) style="display:none;" @elseif($recProposalByID->status_prop==1) style="display:none;" @elseif($recProposalByID->status_prop==2) style="display:none;" @endif>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Alasan Dikembalikan: </label>
                        <div class="col-md-5">
                            <input disabled="" name="alasan_dikembalikan" type="text" data-required="1" class="form-control button-submit" value=""> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <br>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-eye"></i>Detil Proposal</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <a href="{{route('skpd.proposal.manage',['fase'=>$fase])}}" class="btn btn-default" style="float: right;">Kembali</a>
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_0" data-toggle="tab">1. Data Proposal </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_1" data-toggle="tab">2. Isi Rencana Penggunaan Bantuan & RAB </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    
                                    {{-- Data Proposal --}}
                                    <div class="tab-pane fade active in" id="tab_1_0">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" role="form">
                                            <div class="form-body" style="padding: 0;margin: 0;">
                                                <h3 class="form-section">Detil Lembaga</h3>
                                                @inject('metrics', 'BPKAD\Services\LembagaService')
                                                {{-- {{ dd($metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkota) }} --}}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-6"><b>Nama Lembaga :</b></label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nm_lembaga }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-6"><b>Alamat Lembaga :</b></label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->alamat }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-6"><b>RT/RW :</b></label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->RT }}/{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->RW }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-6"><b>Kelurahan :</b></label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkel }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-6"><b>Kecamatan :</b></label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static"> {{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkec }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-6"><b>Kota :</b></label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static">{{ $metrics->getLembagaByNomor($recProposalByID->no_lembaga)->nmkota }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <h3 class="form-section" style="margin-top: 0;">Data Utama</h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Tanggal Proposal:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> {{ date('d M Y',strtotime($recProposalByID->tgl_prop)) }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>No Proposal:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> {{ $recProposalByID->no_proposal }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Judul Proposal:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> {{ $recProposalByID->jdl_prop }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <h3 class="form-section">Isi Proposal</h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Latar Belakang:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> {{ $recProposalByID->latar_belakang }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Maksud Tujuan:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> {{ $recProposalByID->maksud_tujuan }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Keterangan:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> {{ $recProposalByID->keterangan_prop }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <h3 class="form-section">Nominal Permohonan</h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3 class="form-section">Data Pendukung</h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Surat Pernyataan Tanggung Jawab Bermeterai:</b></label>
                                                            <div class="col-md-9">
                                                                @inject('berkasPernyataanTanggungJawab','BPKAD\Http\Entities\BerkasPernyataanTanggungJawab')
                                                                @foreach($berkasPernyataanTanggungJawab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                <p class="form-control-static"> 
                                                                    <a href="{{ url('/') }}/berkas_pernyataan_tanggungjawab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                    </a>
                                                                </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Struktur Pengurus:</b></label>
                                                            <div class="col-md-9">
                                                                @inject('berkasKepengurusan','BPKAD\Http\Entities\BerkasKepengurusan')
                                                                @foreach($berkasKepengurusan->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                <p class="form-control-static"> 
                                                                    <a href="{{ url('/') }}/berkas_kepengurusan/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                    </a>
                                                                </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Foto Kondisi Saat Ini:</b></label>
                                                            <div class="col-md-9">
                                                                @inject('fotoProposal','BPKAD\Http\Entities\FotoProposal')
                                                                @foreach($fotoProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                <p class="form-control-static"> 
                                                                    <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                    </a>
                                                                </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Berkas Proposal:</b></label>
                                                            <div class="col-md-9">
                                                                @inject('berkasProposal','BPKAD\Http\Entities\BerkasProposal')
                                                                @foreach($berkasProposal->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                <p class="form-control-static"> 
                                                                    <a href="{{ url('/') }}/foto_proposal/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                    </a>
                                                                </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Berkas Rencana Anggaran Biaya (RAB):</b></label>
                                                            <div class="col-md-9">
                                                                @inject('berkasRab','BPKAD\Http\Entities\BerkasRab')
                                                                @foreach($berkasRab->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                <p class="form-control-static"> 
                                                                    <a href="{{ url('/') }}/berkas_rab/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                    </a>
                                                                </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Saldo Akhir Rekening Bank:</b></label>
                                                            <div class="col-md-9">
                                                                @inject('berkasRekening','BPKAD\Http\Entities\BerkasRekening')
                                                                @foreach($berkasRekening->where('no_prop',$recProposalByID->no_prop)->get() as $key=>$value)
                                                                <p class="form-control-static"> 
                                                                    <a href="{{ url('/') }}/berkas_rekening/{{ $value->file_name }}" class="btn btn-circle blue btn-outline" target="_blank">
                                                                    {{ str_limit($value->file_name,25) }} <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                    </a>
                                                                </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Titik Peta:</b></label>
                                                            <div class="col-md-9">
                                                                <div class="portlet-body">
                                                                    <div class="input-group">
                                                                        <input type="hidden" class="form-control" id="pac-input" placeholder="Cari Alamat">
                                                                        {{-- <span class="input-group-btn">
                                                                            <button class="btn blue" id="gmap_geocoding_btn">
                                                                                <i class="fa fa-search"></i>
                                                                        </span> --}}
                                                                    </div>
                                                                    <div id="gmap_basic" class="gmaps"> </div>
                                                                    <b>Latitude : </b>
                                                                    <input type="text" class="form-control" name="latitude" id="latVal" value="{{$recProposalByID->latitude}}"  >
                                                                    <b>Longitude : </b>
                                                                    <input type="text" class="form-control" name="longitude" id="longVal" value="{{$recProposalByID->longitude}}"  >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->

                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Status Proposal:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static">
                                                                    @if($recProposalByID->status_prop==0)
                                                                        <span class="label label-warning">Belum Direkomendasi</span>
                                                                    @elseif($recProposalByID->status_prop==1)
                                                                        <span class="label label-success">Sudah Direkomendasi</span>
                                                                    @elseif($recProposalByID->status_prop==3)
                                                                        <span class="label label-warning">Di Koreksi</span>
                                                                    @else
                                                                        <span class="label label-danger">Di Tolak</span>
                                                                        <p><u><b>Alasan ditolak karena, {{ $recProposalByID->alasan_ditolak }}</b></u></p>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><b>Status Terkirim:</b></label>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static"> 
                                                                    {!! ($recProposalByID->status_kirim==1)?'Terkirim <i class="icon-paper-plane"></i>':'Belum Terkirim' !!}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <p>
                                                                    <em><b><u>Jika Anda sudah mengecek dan tetap tidak menemukan email dari kami silahkan klik tombol kirim ulang bukti dibawah ini.</u></b></em>
                                                                </p>
                                                                <p>
                                                                    <a href="{{ route('member.proposal.resendEmail',['id'=>base64_encode($recProposalByID->no_prop),'email'=>Auth::user()->email,'real_name'=>Auth::user()->real_name,'tahun_anggaran'=>$recProposalByID->tahun_anggaran])}}" class="btn btn-circle green"> KIRIM ULANG BUKTI KIRIM PROPOSAL
                                                                    <i class="icon-paper-plane"></i></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                    
                                    {{-- Isi Rencana Penggunaan Bantuan & RAB --}}
                                    <div class="tab-pane fade" id="tab_1_1">
                                        <div class="col-md-12">
                                            <h3 class="form-section">Rencana Penggunaan Anggaran (Rp)</h3>
                                            @if($recProposalByID->nominal_pengajuan != $rencana_anggaran)
                                            <p><i style="display: block;padding-top: 10px;color: red;">*Nilai permohonan bantuan belum sesuai dengan nilai input RAB.</i></p>
                                            @else
                                            <p><i style="display: block;padding-top: 10px;color: green;">*Nilai permohonan bantuan sudah sesuai dengan nilai input RAB.</i></p>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4"><b>Nominal Permohonan Bantuan (Rp):</b></label>
                                                        <div class="col-md-8">
                                                            <p class="form-control-static">Rp. {{ number_format($recProposalByID->nominal_pengajuan,2,',','.') }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4"><b>Nilai Input RAB (Rp):</b></label>
                                                        <div class="col-md-8">
                                                            <p class="form-control-static">Rp. {{ number_format($rencana_anggaran,2,',','.') }} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <br><br><br>
                                            <!--/row-->
                                            {{-- <div class="table-toolbar">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="btn-group">
                                                            <a href="{{ route('member.proposal.create.rencana',['no_prop'=>base64_encode($recProposalByID->no_prop),'fase'=>$fase]) }}" class="btn sbold green"> Buat Rencana Kegiatan
                                                                <i class="fa fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                                <thead>
                                                    <tr>
                                                        <th> No. </th>
                                                        <th> Rencana Kegiatan/Rincian </th>
                                                        <th> Vol/Satuan (jumlah) </th>
                                                        <th> Harga Satuan (Rp) </th>
                                                        <th> Anggaran (Rp) </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($recRencanaByNoProp)==0)
                                                        <tr>
                                                            <td colspan="5"><i><center>data belum tersedia</center></i></td>
                                                        </tr>
                                                    @else
                                                        @inject('metrics', 'BPKAD\Services\RincianService')
                                                        @foreach ($recRencanaByNoProp as $key=>$value)
                                                        <tr>
                                                            <td>{{$key+1}}</td>
                                                            <td><b>{{$value->rencana}}</b></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><b>{{ number_format($value->rencana_anggaran,2,",",".")}}</b></td>
                                                        </tr>
                                                        {!! $metrics->outputByRencanaID($value->id,$value->no_prop,$fase) !!}
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_2_0" data-toggle="tab">1. Kelengkapan Administrasi </a>
                            </li>
                            <li>
                                <a href="#tab_2_1" data-toggle="tab">2. Peninjauan Lapangan </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            {{-- Kelengkapan Administrasi --}}
                            <div class="tab-pane fade active in" id="tab_2_0">
                                <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="{{ route('skpd.proposal.checklist.data.administrasi') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Administrasi</center></th>
                                                            <th><center>Ada</center></th>
                                                            <th><center>Tidak Ada</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_aktivitas1" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_aktivitas) && $recProposalAdministrasiByNoProp->cek_aktivitas==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_aktivitas2" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_aktivitas) && $recProposalAdministrasiByNoProp->cek_aktivitas==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_aktivitas))
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalAdministrasiByNoProp->keterangan_cek_aktivitas }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Kepengurusan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_kepengurusan1" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_kepengurusan) && $recProposalAdministrasiByNoProp->cek_kepengurusan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_kepengurusan2" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_kepengurusan) && $recProposalAdministrasiByNoProp->cek_kepengurusan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_kepengurusan))
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalAdministrasiByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_rab1" name="cek_rab" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_rab) && $recProposalAdministrasiByNoProp->cek_rab==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_rab2" name="cek_rab" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_rab) && $recProposalAdministrasiByNoProp->cek_rab==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_rab))
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalAdministrasiByNoProp->keterangan_cek_rab }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Waktu Pelaksanaan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_waktu_pelaksanaan1" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan1">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_waktu_pelaksanaan2" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan) && $recProposalAdministrasiByNoProp->cek_waktu_pelaksanaan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan2">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalAdministrasiByNoProp->keterangan_cek_waktu_pelaksanaan))
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalAdministrasiByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input disabled="" type="checkbox" name="is_approve"
                                                @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
                                                checked
                                                @endif
                                            > 
                                            <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <hr>
                                    {{-- <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-1 col-md-11">
                                                <button type="submit" class="btn green-meadow">Kirim</button>
                                                <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                </form>
                            </div>
                            
                            {{-- Kelengkapan Lapangan --}}
                            <div class="tab-pane fade" id="tab_2_1">
                                @if(isset($recProposalAdministrasiByNoProp->is_approve) && $recProposalAdministrasiByNoProp->is_approve==1)
                                    <span class="caption-subject font-purple-soft bold uppercase"><span style="line-height: 30px;font-weight: bolder;">Checklist kesesuaian data antara dokumen tertulis dan softcopy<br>(Berdasarkan pergub 142 tahun 2018):</span></span>
                                <form action="{{ route('skpd.proposal.checklist.data.lapangan') }}" class="form-horizontal" novalidate="novalidate" method="POST">
                                <div class="form-group">
                                    <div class="form-body">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="no_prop" value="{{ $proposalID }}">
                                        <div class="form-group form-md-radios">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th width="50%"><center>Data Lapangan</center></th>
                                                            <th><center>Sesuai</center></th>
                                                            <th><center>Tidak Sesuai</center></th>
                                                            <th><center>Keterangan</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_aktivitas11" name="cek_aktivitas" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_aktivitas22" name="cek_aktivitas" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganByNoProp->cek_aktivitas) && $recProposalLapanganByNoProp->cek_aktivitas==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_aktivitas22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_aktivitas))
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_aktivitas }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_aktivitas" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Kepengurusan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_kepengurusan11" name="cek_kepengurusan" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_kepengurusan22" name="cek_kepengurusan" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganByNoProp->cek_kepengurusan) && $recProposalLapanganByNoProp->cek_kepengurusan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_kepengurusan22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_kepengurusan))
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_kepengurusan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_kepengurusan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Rencana Anggaran Biaya (RAB)</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_rab11" name="cek_rab" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_rab22" name="cek_rab" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganByNoProp->cek_rab) && $recProposalLapanganByNoProp->cek_rab==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_rab22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_rab))
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_rab }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_rab" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Waktu Pelaksanaan</td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_waktu_pelaksanaan11" name="cek_waktu_pelaksanaan" value="1" class="md-radiobtn" 
                                                                    @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==1)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan11">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="md-radio">
                                                                    <input disabled="" type="radio" id="cek_waktu_pelaksanaan22" name="cek_waktu_pelaksanaan" value="2" class="md-radiobtn"
                                                                    @if(isset($recProposalLapanganByNoProp->cek_waktu_pelaksanaan) && $recProposalLapanganByNoProp->cek_waktu_pelaksanaan==2)
                                                                    checked
                                                                    @endif
                                                                    >
                                                                    <label for="cek_waktu_pelaksanaan22">
                                                                        <span class="inc"></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> 
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            @if(isset($recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan))
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control">{{ $recProposalLapanganByNoProp->keterangan_cek_waktu_pelaksanaan }}</textarea>
                                                            @else
                                                            <textarea disabled="" name="keterangan_cek_waktu_pelaksanaan" class="form-control"></textarea>
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <input disabled="" type="checkbox" name="is_approve"
                                                @if(isset($recProposalLapanganByNoProp->is_approve) && $recProposalLapanganByNoProp->is_approve==1)
                                                checked
                                                @endif
                                            > 
                                            <span style="color:red;font-weight: bolder;">Dengan ini Saya Menyatakan Proposal Ini Telah Melewati Tahap-Tahap Checklist Kesesuaian Data Antara Dokumen Tertulis & Softcopy (Berdasarkan pergub 142 tahun 2018) Untuk Memenuhi Kelengkapan Administrasi.</span>
                                        </center>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-1 col-md-11">
                                                <button type="submit" class="btn green-meadow">Kirim</button>
                                                <button type="reset" class="btn grey-salsa btn-outline">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                @else
                                <h5 style="color: red;font-weight: bolder;">*Anda Belum mem-Verifikasi Kelengkapan Administrasi</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@stop
@section('javascript')
    @parent
    <script type="text/javascript">
        $(function(){
            // only numeric
            $( "input.only-numeric" ).on( "blur", function() {
              $( this ).val(function( i, val ) {
                return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
              });
            });

            // autosubmit
            // $('#checkbox1_8,#checkbox1_9,#checkbox1_10').click(function(){
            //     $('form#form_sample_1').submit();
            // });

            
            // belum direkomendasikan
            $('#checkbox1_8').click(function(){
                $('#nominal-rekomendasi,#alasan,#alasan-dikembalikan').css('display','none');
            });
            // sudah direkomendasikan
            $('#checkbox1_9').click(function(){
                $('#nominal-rekomendasi').css('display','block');
                $('#alasan').css('display','none');
                $('#alasan-dikembalikan').css('display','none');
            });
            // ditolak disertai alasan
            $('#checkbox1_10').click(function(){
                $('#nominal-rekomendasi').css('display','none');
                $('#alasan').css('display','block');
                $('#alasan-dikembalikan').css('display','none');
            });
            // dikembalikan kelembaga untuk diperbaiki
            $('#checkbox1_11').click(function(){
                $('#nominal-rekomendasi').css('display','none');
                $('#alasan').css('display','none');
                $('#alasan-dikembalikan').css('display','block');
            });
        });
    </script>
    @if( !empty(Session::get('status')) )

        <script type="text/javascript">
            $(function(){
                    $(window).load(function(){
                        Command: toastr["{{ Session::get('status') }}"]("{{ Session::get('msg') }}", "{{ Session::get('status') }}")

                        toastr.options = {
                          "closeButton": false,
                          "debug": false,
                          "newestOnTop": false,
                          "progressBar": false,
                          "positionClass": "toast-top-right",
                          "preventDuplicates": false,
                          "onclick": null,
                          "showDuration": "300",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }
                    });
            });
        </script>
    @endif
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&libraries=places"></script>
    <script type="text/javascript">
        var MapsGoogle = function () {
            
            var latValue = '{{ $recProposalByID->latitude }}';
            var longValue = '{{ $recProposalByID->longitude }}';
            var markers = [];
            var mapBasic = function () {

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(-6.1680111,106.8732487)
                };
                var map = new google.maps.Map(document.getElementById('gmap_basic'),mapOptions);

                if(latValue != ''){
                    var myLatlng = new google.maps.LatLng(latValue,longValue);
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                    }); 
                    markers.push(marker);
                }
                
                var input = (document.getElementById('pac-input'));
                var searchBox = new google.maps.places.SearchBox((input));

                google.maps.event.addListener(searchBox, 'places_changed', function() {

                    var places = searchBox.getPlaces();
                    // For each place, get the icon, place name, and location.
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0, place; place = places[i]; i++) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            title: place.name,
                            position: place.geometry.location
                        });
                        markers.push(marker);

                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    var bounds = map.getBounds();
                    searchBox.setBounds(bounds);
                });

                google.maps.event.addListener(map, 'click', function(e) {
                    var latLng = e.latLng;
                    // console.log(latLng.lat())
                    setAllMap(null);
                    get_koordinate(latLng)           
                    placeMarker(e.latLng, map);
                });
            }

            // Tambahi Koordinat Ke TextBox
            function get_koordinate(data) {
                console.log(data);
                $('#latVal').val(data.lat());
                $('#longVal').val(data.lng()); 
            }

            function placeMarker(position, map) {
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                console.log(marker);
                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            return {
                //main function to initiate map samples
                init: function () {
                    mapBasic();
                }
            };

    }();

    jQuery(document).ready(function() {
        MapsGoogle.init();
    });
    </script>
@stop