<!-- Header
		============================================= -->
		<header id="header" class="page-section full-header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="{{URL::to('/')}}" class="standard-logo"><img src="/assets/images/logo.png" alt="ehibahbansos Logo"></a>
						<a href="{{URL::to('/')}}" class="retina-logo"><img src="/assets/images/logo@2x.png" alt="ehibahbansos Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul>
							<li class="{{Request::url()==route('home') ? 'current' : ''}}">
								<a href="{{route('home')}}">
									<div>Beranda</div>
								</a>
							</li>
							<li class="{{Request::segment(1)=='pencarian' ? 'current' : ''}}">
								<a href="{{route('search.engine')}}">
									<div>Pencarian</div>
								</a>
							</li>
							{{-- <li class="{{Request::url()==route('pengaduan.form') ? 'current' : ''}}">
								<a href="{{route('pengaduan.form')}}">
									<div>Laporan</div>
								</a>
							</li> --}}
							<li class="{{Request::url()==route('login.form') ? 'current' : ''}}">
								<a href="{{route('login.form')}}">
									<div>Login</div>
								</a>
							</li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>
		
		</header><!-- #header end -->