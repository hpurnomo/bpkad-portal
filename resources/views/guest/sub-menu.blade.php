		<div class="row">
			<div class="col-md-12" style="background-color: #FF7503;">
				<marquee direction="" onmouseover="this.stop();" onmouseout="this.start();" scrollamount="10">
				@foreach ($recRunningTextByID as $element)
					<a href="{{ (!empty($element->link))? $element->link:'javascript:;' }}" style="color: white;font-size: 15px;letter-spacing: 0.5px;" target="_blank">
					&#10687; {{ $element->brief }}</a>
				@endforeach
				</marquee>
			</div>
		</div>
		<div class="row clearfix common-height">

			{{-- <div id="bg-fake" class="col-md-6 center col-padding">
				<div><center>loading image... <br><img src="{{ url('/') }}/assets/images/preloader.gif"></center></div>
			</div> --}}

			{{-- <div id="bg-real" class="col-md-6 center col-padding" style="background-image: url('assets/images/slider/swiper/small2.jpg'); background-size: cover; display: none;">
				<div>&nbsp;</div>
			</div> --}}
			<div class="col-md-6 col-padding" style="background: #fbfbfb;overflow-y:scroll;">
				<div class="heading-block">
					<h3>Info & Unduhan Terbaru</h3>
				</div>

				<div id="post-list-footer">
					@foreach ($recInfoTerkiniByID as $element)
					<div class="spost clearfix">
						<div class="entry-c">
							<div class="entry-title">
								<h4><a href="{{url('/')}}/info_terkini/{{ $element->file_name }}" target="_blank">{{ $element->title }} : {{ $element->brief }}</a></h4>
							</div>
							<ul class="entry-meta">
								<li>{{ date('d M Y',strtotime($element->created_at)) }}</li>
							</ul>
						</div>
					</div>
					@endforeach
				</div>
			</div>

			<div class="col-md-6 center col-padding" style="background-color: rgb(241, 241, 241);">
				<div>
					<div class="heading-block nobottomborder">
						<span class="before-heading color"></span>
						<h3>Video Tutorial Pengajuan Hibah Bansos</h3>
					</div>

					<div class="center bottommargin">
						<a href="{{url('/')}}/assets/images/ehibahbansos_2019.mp4" data-lightbox="iframe" style="position: relative;" frameborder="0" allowfullscreen>
							<img src="assets/images/thumbnail video.png" alt="Video">
							<span class="i-overlay nobg"><img src="assets/images/icons/video-play.png" alt="Play"></span>
						</a>
					</div>
					<p class="lead nobottommargin"><h3>KEADILAN SOSIAL BAGI WARGA JAKARTA</h3>"Melalui Sistem Hibah Bansos yang transparan dan akuntabel mari kita ciptakan keadilan sosial bagi warga Jakarta"</p>
				</div>
			</div>

		</div>

		<!-- Page Sub Menu
		============================================= -->
		<div id="page-menu">

			<div id="page-menu-wrap">

				<div class="container clearfix">

					<div class="menu-title">Jelajahi <span>eHibah&Bansos</span></div>

					<nav class="one-page-menu">
						<ul>
							<li><a href="#" data-href="#header"><div>Beranda</div></a></li>
							{{-- <li><a href="#" data-href="#section-about"><div>Profil</div></a></li> --}}
							<li><a href="#" data-href="#section-work"><div>Prosedur Tahapan</div></a></li>
							{{-- <li><a href="#" data-href="#section-team"><div>Tim</div></a></li> --}}
							<li><a href="#" data-href="#section-faq"><div>Tanya Jawab</div></a></li>
							<li><a href="#" data-href="#section-testimonials" class="no-offset"><div>Testimonials</div></a></li>
							<li><a href="#" data-href="#section-contact"><div>Kontak Kami</div></a></li>
							<li><a href="#" data-href="#footer"><div>Panduan</div></a></li>
							<li><a href="http://bpkd.jakarta.go.id/" target="_blank"><div>Berita</div></a></li>
						</ul>
					</nav>

					<div id="page-submenu-trigger"><i class="icon-reorder"></i></div>

				</div>

			</div>

		</div><!-- #page-menu end -->
