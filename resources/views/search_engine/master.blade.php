@extends('layout-portal.master')

@section('title', 'Pencarian')

@section('header')
	@parent
	@include($roles.'.header')
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

@endsection
	
@section('content')
	<!-- Content
		============================================= -->
		<section id="slider" class="full-screen force-full-screen with-header swiper_wrapper page-section clearfix" style="height: 100%">
			<div class="swiper-container swiper-parent" style="background:none; height: 100%; ">
				<div class="col-xs-12 col-md-7 col-lg-7" style="overflow: scroll;height: 100%;">
					{{-- form filter --}}
					<div class="row form-filter">
						{!! Form::open(array('route' => 'filter.search.engine','id'=>'filter-pencarian','method'=>'GET')) !!}
						
						<div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Kotamadya</label>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select name="nokota" class="transparent-grey">
										<option value="">:: Kotamadya ::</option>
										@foreach($data['kota'] as $row)
										<option value="{{$row->nokota}}#{{$row->kdkota}}">{{$row->nmkota}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>	

						<div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Lokasi</label>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select name="kelurahan" class="transparent-grey">
										<option value="">:: Kelurahan ::</option>
										@foreach($data['kelurahan'] as $row)
										<option value="{{$row->nokel}}">{{$row->nmkel}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>	

						{{-- <div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Nama Lembaga</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<input type="text" name="nm_lembaga" class="form-control transparent-grey">
								</div>
							</div>
						</div> --}}

						{!! Form::close() !!}
					</div>
					{{-- result filter --}}
					<div class="row result-filter" style="padding: 47px;">
						<table id="myTable" class="table table-hover">
							<thead>
								<tr>
								  <th>Nama Lembaga</th>
								  <th>Lokasi Lembaga</th>
								  <th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['lembaga'] as $row)
								<tr>
								  <td>{!! str_limit($row->nm_lembaga, $limit = 50, $end = '...') !!}</td>
								  <td>{!! str_limit($row->alamat, $limit = 50, $end = ' ...') !!}</td>
								  <td><a href="{{route('detail.lembaga',['tahun'=>\Carbon\Carbon::now('Asia/Jakarta')->format('Y'),'id'=>$row->nomor])}}" class="button button-3d button-small button-rounded button-green" target="_blank">Lihat</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-12 col-md-5 col-lg-5" style="height: 100%;padding:0;">
					<div id="map" class="gmap" style="height: 100%"></div>
				</div>
			</div>

		</section>

@stop

@section('footer')
	@parent
@endsection

@section('javascript')
	@parent
		<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script type="text/javascript">
			$(function(){
				// $('#header').addClass('sticky-header');
				var url = $('form#filter-pencarian').attr('action');

				$('form#filter-pencarian').change(function(){
					$(this).submit();
				});
			});
			$(function() {
			    $( "#slider-range" ).slider({
			      range: true,
			      min: 0,
			      max: 1000000,
			      values: [ 100000, 300000 ],
			      slide: function( event, ui ) {
			        $( "#amount" ).val( "Rp." + ui.values[ 0 ] + " - Rp." + ui.values[ 1 ] );
			      }
			    });
			    $( "#amount" ).val( "Rp." + $( "#slider-range" ).slider( "values", 0 ) +
			      " - Rp." + $( "#slider-range" ).slider( "values", 1 ) );
			});
		</script>
		<script type="text/javascript">
			$(function(){
				$('#myTable').DataTable({
					 "searching": true
				});
			})
		</script>

		<!-- Google Maps API KEY
		============================================= -->
<<<<<<< HEAD
	    {{-- <script src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_KEY') }}&callback=initMap" defer></script> --}}
	    <script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap" async defer></script>

        <script type="text/javascript">
=======
	    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-M010SJwgUKsqrREjPtIJpiWOZp256Qc&callback=initMap" defer></script>
        <script type="text/javascript">
        	
>>>>>>> dev
			function initMap() {

			    var locations = [
			         // ['Title A', -6.21462,106.84513],
			         @foreach ($data['map'] as $element)
			         ['{{ $element->nm_lembaga }}', {{ $element->latitude }}, {{ $element->longitude }}],
			         @endforeach
			    ];
			    var map = new google.maps.Map(document.getElementById('map'), {
			         zoom: 12,
			         center: new google.maps.LatLng(-6.21462,106.84513),
			         mapTypeId: google.maps.MapTypeId.ROADMAP
			    });

			    var infowindow = new google.maps.InfoWindow;

			    var marker, i;

			    for (i = 0; i < locations.length; i++) {  
			        marker = new google.maps.Marker({
			             position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			             map: map
			        });

			        google.maps.event.addListener(marker, 'click', (function(marker, i) {
			             return function() {
			                 infowindow.setContent(locations[i][0]);
			                 infowindow.open(map, marker);
			             }
			        })(marker, i));
			    }
			}

		</script>

		<!-- Jquery Select2
		============================================= -->
	    <script type="text/javascript">
			 $('select').select2();
		</script>
@stop