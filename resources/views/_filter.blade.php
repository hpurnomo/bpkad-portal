<div class="row form-filter">
						{!! Form::open(array('route' => 'filter.search.engine','id'=>'filter-pencarian','method'=>'GET')) !!}
						{{-- <div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Tahun Anggaran</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select name="periode" class="transparent-grey">
										<option value="">:: Periode Tahun ::</option>
										@foreach($data['periode'] as $row)
										<option value="{{$row->noper}}">{{$row->tahun}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div> --}}	

						<div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Lokasi</label>
							</div>
							{{-- <div class="col-lg-3 col-md-3 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select name="kota" class="transparent-grey">
										<option value="">:: Kota ::</option>
										@foreach($data['kota'] as $row)
										<option value="{{$row->nokota}}">{{$row->nmkota}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-lg-3 col-md-3 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select name="kecamatan" class="transparent-grey">
										<option value="">:: Kecamatan ::</option>
										@foreach($data['kecamatan'] as $row)
										<option value="{{$row->nokec}}">{{$row->nmkec}}</option>
										@endforeach
									</select>
								</div>
							</div> --}}

							<div class="col-lg-3 col-md-3 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select name="kelurahan" class="transparent-grey">
										<option value="">:: Kelurahan ::</option>
										@foreach($data['kelurahan'] as $row)
										<option value="{{$row->nokel}}">{{$row->nmkel}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>	

						{{-- <div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Jenis</label>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select class="transparent-grey">
										@for($i=1;$i<10;$i++)
										<option value="Jenis {{$i}}">Jenis Hibah</option>
										@endfor
									</select>
								</div>
							</div>

							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Kategori</label>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<select class="transparent-grey">
										@for($i=1;$i<10;$i++)
										<option value="Kategori {{$i}}">Kategori Hibah</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>	 --}}

						<div class="filter-section">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Nama Lembaga</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<input type="text" name="nm_lembaga" class="form-control transparent-grey">
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>

						<div class="filter-section" style="display: none;">
							<div class="col-lg-3 col-md-3 col-sm-12 padding-top-5">
								<label class="text-color-red">Judul Proposal</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-12 margin-bottom-15">
								<div class="row row-condensed">
									<input type="text" name="jdl_prop" class="form-control transparent-grey">
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>	

						<div class="filter-section" style="display: none;">
							<div class="col-lg-3 col-md-12 padding-top-5">
								<label class="text-color-red">Anggaran</label>
							</div>
							<div class="col-lg-9 col-md-12 margin-bottom-15">
								<div class="row row-condensed">
									{{-- <p>
									  <label for="amount">Range Anggaran:</label>
									  <input type="text" id="amount" class="text-color-red" readonly style="border:0;font-weight:bold;">
									</p>
									<div id="slider-range"></div> --}}
									<select name="anggaran" class="transparent-grey">
										<option value="0-1000000000000">:: Pilih Anggaran ::</option>
										<option value="0-100000000"> < 100jt </option>
										<option value="100000000-500000000"> 100jt s/d 500jt </option>
										<option value="500000000-1000000000"> 500jt s/d 1M</option>
										<option value="1000000000-1000000000000"> > 1M</option>
									</select>
									<small>Catatan: untuk memilih filter anggaran terlebih dahulu tentukan tahun anggaran.</small>
								</div>
							</div>
							<div class="separator">&nbsp;</div>
						</div>	
						{!! Form::close() !!}
					</div>