<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_full">

						<div class="col_one_third">

							<div class="widget clearfix">

								<img src="/assets/images/footer-widget-logo.png" alt="" class="footer-logo">


								<div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>Unit Pengelola Data Informasi dan Belanja PPKD Badan Pengelola Keuangan Daerah</strong><br>
										Gedung Balaikota DKI Jakarta Blog G Lantai 14,<br>
										Jl. Medan Merdeka Selatan No. 8-9 Jakarta Pusat.
									</address>

									<abbr title="Phone Number"><strong>Phone :</strong></abbr> 021 382 2539<br>
									<abbr title="Phone Number"><strong>Fax :</strong></abbr> 021 382 2439<br><br>
									<!-- <abbr title="Email Address"><strong>Email:</strong></abbr> bpkad@jakarta.go.id-->
								</div>

							</div>

						</div>

						<div class="col_two_third col_last">

							<div class="widget widget_links clearfix">

								<h4>Buku Panduan</h4>

								<ul style="text-transform:uppercase;">
									<li><a href="{{ URL::to('info_terkini/LEMBAGA - Buku Manual Penggunaan Hibah Bansos.pdf') }}" target="_blank">PANDUAN APLIKASI HIBAHBANSOS V.2 (LEMBAGA)</a></li>
									<li><a  href="{{ URL::to('info_terkini/SKPD - Buku Manual Penggunaan Hibah Bansos.pdf') }}" target="_blank">PANDUAN APLIKASI HIBAHBANSOS V.2 (SKPD KOORDINATOR)</a></li>
									<li><a href="{{ URL::to('info_terkini/PPKD - Buku Manual Penggunaan Hibah Bansos.pdf') }}"  target="_blank">PANDUAN APLIKASI HIBAHBANSOS V.2 (PPKD KOORDINATOR)</a></li>
								</ul>

							</div>

						</div>

						{{-- <div class="col_one_third col_last">

							<div class="widget clearfix">
								<h4>Twitter</h4>
								<a class="twitter-timeline" href="https://twitter.com/eHibahBansosDKI" data-widget-id="673919311480680448">Tweets by @eHibahBansosDKI</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>

						</div> --}}

					</div>

					{{-- <div class="col_one_third col_last">

						<div class="widget subscribe-widget clearfix">
							<h5><strong>Subscribe</strong> to Our Newsletter to get Important News.</h5>
							<div id="widget-subscribe-form-result" data-notify-type="success" data-notify-msg=""></div>
							<form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
								<div class="input-group divcenter">
									<span class="input-group-addon"><i class="icon-email2"></i></span>
									<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
									<span class="input-group-btn">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</span>
								</div>
							</form>
							<script type="text/javascript">
								$("#widget-subscribe-form").validate({
									submitHandler: function(form) {
										$(form).find('.input-group-addon').find('.icon-email2').removeClass('icon-email2').addClass('icon-line-loader icon-spin');
										$(form).ajaxSubmit({
											target: '#widget-subscribe-form-result',
											success: function() {
												$(form).find('.input-group-addon').find('.icon-line-loader').removeClass('icon-line-loader icon-spin').addClass('icon-email2');
												$('#widget-subscribe-form').find('.form-control').val('');
												$('#widget-subscribe-form-result').attr('data-notify-msg', $('#widget-subscribe-form-result').html()).html('');
												SEMICOLON.widget.notifications($('#widget-subscribe-form-result'));
											}
										});
									}
								});
							</script>
					</div> --}}

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; {{ date('Y') }} All Rights Reserved by BPKD.<br>
						<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
						Contact <a href="http://klakklik.id">developer</a>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							{{-- <a href="#" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a> --}}

							{{-- <a href="#" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a> --}}

							{{-- <a href="#" class="social-icon si-small si-borderless si-gplus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a> --}}

						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> hibahbansosdki@gmail.com {{-- <span class="middot">&middot;</span> <i class="icon-headphones"></i> 0812 6000 0304 <span class="middot">&middot;</span> --}} 
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end