@if (count($errors) > 0)
    <div class="style-msg2 errormsg">
		<div class="msgtitle">Opss!! You Must Fix the Following Errors :</div>
		<div class="sb-msg">
			<ul>
	            @foreach ($errors->all() as $error)
	                <li><b>{{ $error }}</b></li>
	            @endforeach
	        </ul>
		</div>
	</div>
@endif
{!! Session("errMsg") !!}

