<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	{!! Html::style('assets/css/bootstrap.css') !!}
	{!! Html::style('assets/style.css') !!}
	{!! Html::style('assets/custom-style.css') !!}
	{!! Html::style('assets/css/dark.css') !!}
	{!! Html::style('assets/css/font-icons.css') !!}
	{!! Html::style('assets/css/animate.css') !!}
	{!! Html::style('assets/css/magnific-popup.css') !!}
	{!! Html::style('assets/css/vmap.css') !!}
	{!! Html::style('assets/css/responsive.css') !!}
	{!! Html::style('assets/css/jquery.dataTables.min.css') !!}
	{!! Html::style('assets/bower_components/sweetalert/dist/sweetalert.css') !!}
	{!! Html::style('assets/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.css') !!}
	{!! Html::style('assets/css/select2.min.css') !!}

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	{!! Html::style('assets/css/colors.css') !!}

	{{-- favicon --}}
	<link rel="shortcut icon" href="{{URL::to('/')}}/assets/icon/favicon.ico" type="image/x-icon">
	<link rel="icon" href="{{URL::to('/')}}/assets/icon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="57x57" href="{{URL::to('/')}}/assets/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="{{URL::to('/')}}/assets/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="{{URL::to('/')}}/assets/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="{{URL::to('/')}}/assets/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="{{URL::to('/')}}/assets/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="{{URL::to('/')}}/assets/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="{{URL::to('/')}}/assets/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('/')}}/assets/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{URL::to('/')}}/assets/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('/')}}/assets/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="{{URL::to('/')}}/assets/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('/')}}/assets/icon/favicon-16x16.png">
	<link rel="manifest" href="{{URL::to('/')}}/assets/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{URL::to('/')}}/assets/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- External JavaScripts
	============================================= -->
	{!! Html::script('assets/js/jquery.js') !!}
	{!! Html::script('assets/js/plugins.js') !!}
	{!! Html::script('assets/js/jquery.formatCurrency-1.4.0.js') !!}
	{!! Html::script('assets/js/jquery.formatCurrency.all.js') !!}
	{!! Html::script('assets/js/jquery.dataTables.min.js') !!}
	{!! Html::script('assets/js/pwstrength.js') !!}
	{!! Html::script('assets/bower_components/sweetalert/dist/sweetalert.min.js') !!}
	{!! Html::script('assets/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') !!}
	{!! Html::script('assets/bower_components/jasny-bootstrap/js/fileinput.js') !!}
	{!! Html::script('assets/bower_components/jasny-bootstrap/js/inputmask.js') !!}
	{!! Html::script('assets/js/select2.min.js') !!}

	<!-- Document Title
	============================================= -->
	<title> eHibahBansos :: @yield('title')</title>
	
	

</head>

<body class="stretched no-transition">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		
		@section('header')
			{{-- Header  --}}
		@show

		@section('submenu')
			{{-- Sub Menu  --}}
		@show
		
		{{-- content --}}
		@yield('content')
		
		@section('footer')
			{{-- footer  --}}
		@show

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	@section('javascript')
		<!-- Footer Scripts
		============================================= -->
		{!! Html::script('assets/js/functions.js') !!}
	@show
	<script id="dsq-count-scr" src="//ehibahbansosdki.disqus.com/count.js" async></script>
</body>
</html>