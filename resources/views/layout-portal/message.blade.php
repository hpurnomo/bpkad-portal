@section('javascript')
	@parent
		<script type="text/javascript">
		$(function(){
			swal("Success!", "Laporan Anda Telah Berhasil Terkirim, dan segera kami tindak lanjuti. Terima Kasih !", "success");

			$('#register-form,.fancy-title').css('display','none');
		});
		</script>
@endsection