<table border="0" width="700" style="border: 2px solid #111111;padding: 15px;">
	<thead>
		<tr>
			<td colspan="3"> <center><img src="{{ $message->embed(url('/assets/images/logo-dki.png')) }}" width="111"></center> </td>
		</tr>
		<tr>
			<td colspan="3"><center>SISTEM EHIBAHBANSOS PROVINSI DKI JAKARTA</center></td>
		</tr>
		<tr>
			<td colspan="3"><hr></td>
		</tr>
		<tr>
			<td colspan="3">Dengan kami menyatakan bahwa : </td>
		</tr>
		<tr>
			<td width="200"><b>JUDUL PROPOSAL</b></td>
			<td width="1">:</td>
			<td>{{ $data['jdl_prop'] }}</td>
		</tr>
		<tr>
			<td width="200"><b>ALASAN DIKEMBALIKAN</b></td>
			<td width="1">:</td>
			<td>{{ $data['alasan_dikembalikan'] }}</td>
		</tr>
		<tr>
			<td colspan="3">Segera perbaiki Proposal Anda kemudian dikirim kembali melalui Sistem Aplikasi EHibahBansos. Terima Kasih. </td>
		</tr>
		<tr>
			<td colspan="3">
				<center>
					<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>date('Y'),'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
				</center>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<center>
					<br><br>
					<a href="{{ url('/') }}/member/proposal/index" style='background: red;padding: 15px;color: white;font-size: 11px;text-decoration: none;text-transform: uppercase;letter-spacing: 1px;border-radius:20px;margin:40px;'>Klik Disini Untuk Melihat Proposal</a>
				</center>
			</td>
		</tr>
	</thead>
</table>
<br><br><br><br><br><br>