@extends('emails.template')

@section('content')
  
Yth {{ $data['nama_lembaga'] }} <br><br><br>

 Kami informasikan saat ini proses anggaran hibah bansos lembaga anda pada {{  $data['periode']}} sudah memasuki tahapan APBD Final, silahkan untuk menindaklajuti dengan membuat Proposal Definitif sesuai dengan nominal yang ditetapkan dengan cara login ke aplikasi ehibahbansosdki.jakarta.go.id . Terimakasih 
 		
@endsection
