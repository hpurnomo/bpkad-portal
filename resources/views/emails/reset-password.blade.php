@extends('emails.template')

@section('content')
	<center>
		<h2>Forgot Password?</h2>
	</center>

	<p style="font-size: 15px;color: #777777;">
		<center style="margin-bottom: 25px;">
			Someone (probably you) requested a reset of password for your account. To reset your password, please click the button below.
		</center>
	</p> 

	<center style="margin-bottom: 25px;">
		<a href="{{ config('mail')['url_mail'] }}/password/reset/{{ $data['token'] }}" style='background: #4164D8;padding: 15px;color: white;font-size: 16px;text-decoration: none;text-transform: uppercase;letter-spacing: 3px;border-radius:5px;margin:40px;'>
			Reset Password
		</a>
	</center>

	<small style="color:#ababab;">
		<center>
			if someone else made this request, you may ignore this message and continue using your old password.
		</center>
	</small> 
@endsection
