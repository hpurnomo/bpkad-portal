@extends('emails.template')

@section('content')

<center>
	<h2>DENGAN INI KAMI MENYATAKAN BAHWA :</h2>
</center>

<table border="0" width="700" style="padding: 15px;">
	<thead>
		<tr>
			<td width="250"><b>JUDUL PROPOSAL</b></td>
			<td width="1">:</td>
			<td>{{ $data['jdl_prop'] }}</td>
		</tr>
		<tr>
			<td width="250"><b>NOMOR REKOMENDASI</b></td>
			<td width="1">:</td>
			<td>{{ $data['no_rekomendasi'] }}</td>
		</tr>
		<tr>
			<td width="250"><b>TANGGAL REKOMENDASI</b></td>
			<td width="1">:</td>
			<td>{{ $data['tgl_rekomendasi'] }}</td>
		</tr>
		<tr>
			<td colspan="3"><h3>Telah Kami Rekomendasikan. Terima Kasih.</h3></td>
		</tr>
	</thead>
</table>
<center>
	<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>date('Y'),'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
	<br><br>
	<a href="{{ url('/') }}/member/proposal/index" style='background: red;padding: 15px;color: white;font-size: 11px;text-decoration: none;text-transform: uppercase;letter-spacing: 1px;border-radius:20px;margin:40px;'>Klik Disini Untuk Melihat Proposal</a>
</center>
@endsection
