@extends('emails.template')

@section('content')

<center>
	<h2>{{ $data['nm_skpd'] }}</h2>
</center>

<p style="font-size: 15px;color: #777777;">
	<center style="margin-bottom: 25px;">
		DENGAN INI MEMBERITAHUKAN KEPADA LEMBAGA <b style="text-transform: uppercase;">{{ $data['nm_lembaga'] }}</b> UNTUK SEGERA MELAKUKAN PERBAIKAN DATA LEMBAGA DENGAN MERUJUK PADA PENINJAUAN LAPANGAN DI BAWAH INI :
	</center>
</p> 

<table border="0" width="700" style="padding: 15px;">
	<thead>
		<tr>
			<td colspan="3">
				<table border="1" cellspacing="1" cellpadding="1" style="width: 900px;">
					<tr>
						<td style="font-size: 16px;width: 375px;text-align: center;font-weight: bolder;">Data Lapangan</td>
						<td style="font-size: 16px;width: 150px;text-align: center;font-weight: bolder;">Sesuai/Tidak Sesuai</td>
						<td style="font-size: 16px;width: 375px;text-align: center;font-weight: bolder;">Keterangan</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Nama dan Identitas</td>
						<td style="font-size: 16px;">
							@if($data['cek_ktp_ketua']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_ktp_ketua'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Alamat</td>
						<td style="font-size: 16px;">
							@if($data['cek_alamat_lembaga']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_alamat_lembaga'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Saldo akhir tahun lalu beserta rekening Bank</td>
						<td style="font-size: 16px;">
							@if($data['cek_rekening_lembaga']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_rekening_lembaga'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila Sesuai</td>
						<td style="font-size: 16px;">
							@if($data['cek_bantuan_tahun_sebelum']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_bantuan_tahun_sebelum'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Nomor Pokok Wajib Pajak (aslinya)*</td>
						<td style="font-size: 16px;">
							@if($data['cek_npwp']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_npwp'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Akte Notaris Pendirian BSesuain Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga (aslinya)*</td>
						<td style="font-size: 16px;">
							@if($data['cek_akta']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_akta'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Surat Keterangan Domisili dari Kelurahan setempat (aslinya)*</td>
						<td style="font-size: 16px;">
							@if($data['cek_skd']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_skd'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan (aslinya)*</td>
						<td style="font-size: 16px;">
							@if($data['cek_sertifikat']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_sertifikat'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang (aslinya)*</td>
						<td style="font-size: 16px;">
							@if($data['cek_izin_operasional']==1)
							Sesuai
							@else
							Tidak Sesuai
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_izin_operasional'] }}</td>
					</tr>
					<tr>
						<td colspan="3">
							<center>
								<br>
								<br>
								<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>date('Y'),'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
								<br>
								<br>
							</center>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</thead>
</table>
@endsection
