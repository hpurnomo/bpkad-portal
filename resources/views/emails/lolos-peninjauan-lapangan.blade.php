@extends('emails.template')

@section('content')

<center>
	<h2>{{ $data['nm_skpd'] }}</h2>
</center>

<p style="font-size: 15px;color: #777777;">
	<center style="margin-bottom: 25px;">
		DENGAN INI MEMBERITAHUKAN KEPADA LEMBAGA <b style="text-transform: uppercase;">{{ $data['nm_lembaga'] }}</b> TELAH LOLOS TAHAP PENINJAUAN LAPANGAN
	</center>
</p> 

<center style="margin-bottom: 25px;">
	<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>date('Y'),'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
</center>

<small style="color:#ababab;">
	<center>
		email ini sudah menjadi bukti yang sah. Untuk menyatakan lembaga tersebut diatas telah lolos tahap verifikasi.
	</center>
</small>

@endsection
