@extends('emails.template')

@section('content')

<center>
	<h2>
		TANDA BUKTI PENGAJUAN PROPOSAL <br>
		SISTEM EHIBAHBANSOS PROVINSI DKI JAKARTA <br>
		TAHUN ANGGARAN {{ $data['tahun_anggaran'] }}
	</h2>
</center>

<table border="0" width="700" style="padding: 15px;">
	<thead>
		<tr>
			<td width="150"><b>JUDUL PROPOSAL</b></td>
			<td width="1">:</td>
			<td>{{ $data['jdl_prop'] }}</td>
		</tr>
		<tr>
			<td width="150"><b>NILAI PENGAJUAN</b></td>
			<td width="1">:</td>
			<td>Rp. {{ $data['nominal_pengajuan'] }}</td>
		</tr>
	</thead>
</table>

<center>
	<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$data['tahun_anggaran'],'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
	<br><br><br>
	<a href="{{ route('detail.lembaga',['tahun'=>$data['tahun_anggaran'],'idlembaga'=>$data['no_lembaga']]) }}" style='background: red;padding: 15px;color: white;font-size: 11px;text-decoration: none;text-transform: uppercase;letter-spacing: 1px;border-radius:20px;margin:40px;'>Klik Disini Untuk Melihat Proposal</a>

</center>
@endsection