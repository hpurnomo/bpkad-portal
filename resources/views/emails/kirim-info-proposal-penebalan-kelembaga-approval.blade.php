@extends('emails.template')

@section('content')
 

Yth {{ $data['real_name'] }} <br><br><br>

@if($data['tahapan'] == 'Definitif')
	
	Kami informasikan bahwa berdasarkan input proses anggaran di sistem ehibahbansos DKI Jakarta,  {{ $data['keterangan'] }} di tahap FINAL APBD, proposal anda termasuk salah satu yang disetujui sebagai calon penerima ehibahbansos DKI jakarta. <br><br>
	Silahkan untuk segera melakukan perbaikan subatansi dan nominal proposal menyesuaikan dengan nilai definitif yang sudah ditetapkan dengan cara login ke ehibahbansosdki.jakarta.go.id. <br>Terimakasih

@else


Kami informasikan bahwa berdasarkan input proses anggaran di sistem ehibahbansos DKI Jakarta,  {{ $data['keterangan'] }}   di tahap  {{ $data['tahapan'] }}  Proposal anda mendapatkan koreksi nilai anggaran. Silahkan untuk segera melakukan perbaikan ( proses penebalan proposal ) dengan login ke ehibahbansosdki.jakarta.go.id. <br>Terimakasih


@endif				
					
@endsection
