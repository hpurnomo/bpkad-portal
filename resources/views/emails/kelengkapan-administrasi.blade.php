@extends('emails.template')

@section('content')

<center>
	<h2>{{ $data['nm_skpd'] }}</h2>
</center>

<p style="font-size: 15px;color: #777777;">
	<center style="margin-bottom: 25px;">
		DENGAN INI MEMBERITAHUKAN KEPADA LEMBAGA <b style="text-transform: uppercase;">{{ $data['nm_lembaga'] }}</b> UNTUK SEGERA MELAKUKAN PERBAIKAN DATA LEMBAGA DENGAN MERUJUK PADA HASIL PENELITIAN KELENGKAPAN ADMINSITRASI DI BAWAH INI :
	</center>
</p> 

<table border="0" width="700" style="padding: 15px;">
	<thead>
		<tr>
			<td colspan="3">
				<table border="1" cellspacing="1" cellpadding="1" style="width: 900px;">
					<tr>
						<td style="font-size: 16px;width: 375px;text-align: center;font-weight: bolder;">Data Administrasi</td>
						<td style="font-size: 16px;width: 150px;text-align: center;font-weight: bolder;">Ada/Tidak Ada</td>
						<td style="font-size: 16px;width: 375px;text-align: center;font-weight: bolder;">Keterangan</td>
					</tr>

					<tr>
						<td style="font-size: 16px;">Fotokopi Kartu Tanda penduduk (KTP) Ketua/ pimpinan badan, lembaga tau organisasi kemasyarakatan *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_ktp_ketua']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_ktp_ketua'] }}</td>
					</tr>
 					
 					<tr>
						<td style="font-size: 16px;">Fotokopi Akta Notaris Pendirian badan hukum yang telah mendapatkan pengesahan dari Kementrian yang membidangi hukum atau keputusan Gubernur tentang </td>
						<td style="font-size: 16px;">
							@if($data['cek_akta']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_akta'] }}</td>
					</tr>

					<tr>
						<td style="font-size: 16px;"> Fotokopi Nomor Pokok Wajib Pajak (NPWP) *) </td>
						<td style="font-size: 16px;">
							@if($data['cek_npwp']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_npwp'] }}</td>
					</tr>
					<tr>
						<td style="font-size: 16px;"> 	Fotokopi Surat keterangan domisili organisasi kemasyarakatan dari kelurahan setempat atau sebutan lainnya *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_skd']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_skd'] }}</td>
					</tr>
					<tr>
						<td style="font-size: 16px;">Fotokopi izin operasional /tanda daftar lembaga dari instasi yang berwenang *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_izin_operasional']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_izin_operasional'] }}</td>
					</tr>
					<tr>
						<td style="font-size: 16px;">Fotokopi sertifikat tanah/bukti kepemilikan tanah atau bukti perjanjian sewa bangunan/gedung dengan jangka waktu sewa minimal 3 (tiga) tahun atau dokumen lain yang dipersamakan *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_sertifikat']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_sertifikat'] }}</td>
					</tr>
					<tr>
						<td style="font-size: 16px;">Surat pernyataan tangungjawab bermaterai cukup *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_pernyataan_materai']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['ket_pernyataan_materai'] }}</td>
					</tr>
					<tr>
						<td style="font-size: 16px;">Salinan rekening bank yang masih aktif atas nama badan, lembaga atau organisasi kemasyarakatan</td>
						<td style="font-size: 16px;">
							@if($data['cek_rekening_lembaga']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_rekening_lembaga'] }}</td>
					</tr>
					<tr>
						<td style="font-size: 16px;">Fotokopi SK kepengurusan atau dokumen yang dipersamakan *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_sk_kepengurusan']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['ket_sk_kepengurusan'] }}</td>
					</tr> 

					<tr>
						<td style="font-size: 16px;">Bantuan yanng pernah diterima tahun sebelumnya apabila ada (tanda terima laporan pertanggungjawaban) *)</td>
						<td style="font-size: 16px;">
							@if($data['cek_bantuan_tahun_sebelum']==1)
							Ada
							@else
							Tidak Ada
							@endif
						</td>
						<td style="font-size: 16px;">{{ $data['keterangan_cek_bantuan_tahun_sebelum'] }}</td>
					</tr> 
					<tr>
						<td colspan="3">
							<center>
								<br>
								<br>
								<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>date('Y'),'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
								<br>
								<br>
							</center>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</thead>
</table>
@endsection
