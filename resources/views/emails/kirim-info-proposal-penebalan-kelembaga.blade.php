@extends('emails.template')

@section('content')

<center>
	<h2>DENGAN INI KAMI MENYATAKAN BAHWA :</h2>
</center>

<table border="0" width="700" style="padding: 15px;">
	<thead>
		<tr>
			<td width="250"><b>JUDUL PROPOSAL</b></td>
			<td width="1">:</td>
			<td>{{ $data['jdl_prop'] }}</td>
		</tr>
		<tr>
			<td colspan="3">
				<h3>
					Di wajibkan untuk menginput Proposal 
					{{ $data['is_penebalan_nominal_tapd'] }} {{ $data['is_penebalan_nominal_banggar'] }} {{ $data['is_penebalan_nominal_ddn'] }} {{ $data['is_penebalan_nominal_apbd'] }}.
				</h3>
			</td>
		</tr>
	</thead>
</table>
<center>
	<img src="{{ $message->embed($data['base64'].','.DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>date('Y'),'idlembaga'=>$data['no_lembaga']]), 'QRCODE'), 'barcode') }}">
</center>
@endsection
