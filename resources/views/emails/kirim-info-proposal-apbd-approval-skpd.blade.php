@extends('emails.template')

@section('content')
  
Yth {{ $data['nm_skpd'] }} <br><br><br>

kami informasikan bahwa saat ini proses anggaran hibah/bansos lembaga dibawah koordinator anda pada {{  $data['periode']}} sudah memasuki tahapan APBD Final. Silahkan untuk mengecek ke aplikasi ehibahbansosdki.jakarta.go.id pada Tab Proses anggaran yang ada di detil proposal untuk  melakukan persetujuan penerusan info tindak lanjut proposal definitif ke lembaga bersangkutan. Terimakasih 
 		
@endsection
