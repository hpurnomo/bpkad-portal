<div class="fancy-title title-dotted-border title-left margin-top-15">
							<h4>HISTORY PROPOSAL</h4>
						</div>
						<ul class="nav nav-pills boot-tabs clearfix">
							@foreach($data['historyTahun'] as $row)
								<li class="{{($row->tahun==Request::segment(2))?'active':''}}"><a href="{{route('detail.lembaga',['tahun'=>$row->tahun,'id'=>$data['lembaga']['0']->nomor])}}">{{$row->tahun}}</a></li>
							@endforeach
						</ul>
						@if(empty($data['historyProposal']))
							<h5 class="text-color-red">Proposal Untuk Tahun {{Request::segment(2)}} Tidak Tersedia. Silahkan Pilih Tahun Lainnya.</h5>
						@else
							<div class="col_half separator-right">
							{{-- Judul Proposal --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Judul Proposal</strong>
								</div>
								<div class="col-md-9">
									: {{$data['historyProposal'][0]->jdl_prop}}
								</div>
							</div>
							{{-- Tgl Pengajuan Proposal --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Tgl Pengajuan Proposal</strong>
								</div>
								<div class="col-md-9">
									: {{date('d-M-Y',strtotime($data['historyProposal'][0]->tgl_prop))}}
								</div>
							</div>
							{{-- Latar Belakang --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Latar Belakang</strong>
								</div>
								<div class="col-md-9">
									: -
								</div>
							</div>
							{{-- Maksud & Tujuan --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Maksud & Tujuan</strong>
								</div>
								<div class="col-md-9">
									: - 
								</div>
							</div>
							{{-- Rencana Kegiatan --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Rencana Kegiatan</strong>
								</div>
								<div class="col-md-9">
									: -
								</div>
							</div>
						</div>
						<div class="col_half col_last">
							{{-- Survei Lapangan --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Survei Lapangan</strong>
								</div>
								<div class="col-md-9">
									:  <b>No.</b> <div class="text-color-red">{{$data['historyProposal'][0]->no_survei_lap}}</div> <b>Tgl. {{date('d-M-Y',strtotime($data['historyProposal'][0]->tgl_survei_lap))}}</b>
								</div>
							</div>
							{{-- Rekomendasi --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Rekomendasi</strong>
								</div>
								<div class="col-md-9">
									:  <b>No.</b> <div class="text-color-red">{{$data['historyProposal'][0]->no_rekomendasi}}</div> <b>Tgl. {{date('d-M-Y',strtotime($data['historyProposal'][0]->tgl_rekomendasi))}}</b>
								</div>
							</div>
							{{-- Surat SKPD --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Surat SKPD</strong>
								</div>
								<div class="col-md-9">
									:  <b>No.</b> <div class="text-color-red">{{$data['historyProposal'][0]->no_surat_skpd}}</div> <b>Tgl. {{date('d-M-Y',strtotime($data['historyProposal'][0]->tgl_surat_skpd))}}</b>
								</div>
							</div>
							{{-- Tahapan Saat Ini --}}
							<div class="row margin-top-15">
								<div class="col-md-3">
									<strong>Tahapan Saat Ini</strong>
								</div>
								<div class="col-md-9">
									:  {{$data['historyProposal'][0]->status_input}}
								</div>
							</div>
						</div>
						@endif
						
					</div>

					<div class="col_full margin-top-35">
						<div class="fancy-title title-dotted-border title-center">
							<h4><span>Gallery</span></h4>
						</div>
						<div id="related-portfolio" class="owl-carousel portfolio-carousel">
	                        <div class="oc-item">
	                            <div class="iportfolio">
	                                <div class="portfolio-image">
	                                    <a href="#">
	                                    	@if(!empty($data['fotoProposal1'][0]->file_name))
	                                        <img src="{{URL::to('/')}}/foto_proposal/{{$data['fotoProposal1'][0]->file_name}}" style="height:280px;">
	                                        @else
	                                        <img src="/assets/images/portfolio/4/4.jpg" alt="Mac Sunglasses">
	                                        @endif
	                                    </a>
	                                    <div class="portfolio-overlay" data-lightbox="gallery">
	                                        <a href="/assets/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
	                                        <a href="/assets/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
	                                    </div>
	                                </div>
	                                <div class="portfolio-desc">
	                                    <h3><a href="#">Sebelum</a></h3>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="oc-item">
	                            <div class="iportfolio">
	                                <div class="portfolio-image">
	                                    <a href="#">
	                                        @if(!empty($data['fotoProposal2'][0]->file_name))
	                                        <img src="{{URL::to('/')}}/foto_proposal/{{$data['fotoProposal2'][0]->file_name}}" style="height:280px;">
	                                        @else
	                                        <img src="/assets/images/portfolio/4/4.jpg" alt="Mac Sunglasses">
	                                        @endif
	                                    </a>
	                                    <div class="portfolio-overlay" data-lightbox="gallery">
	                                        <a href="/assets/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
	                                        <a href="/assets/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
	                                    </div>
	                                </div>
	                                <div class="portfolio-desc">
	                                    <h3><a href="#">Sedang</a></h3>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="oc-item">
	                            <div class="iportfolio">
	                                <div class="portfolio-image">
	                                    <a href="#">
	                                        @if(!empty($data['fotoProposal3'][0]->file_name))
	                                        <img src="{{URL::to('/')}}/foto_proposal/{{$data['fotoProposal3'][0]->file_name}}" style="height:280px;">
	                                        @else
	                                        <img src="/assets/images/portfolio/4/4.jpg" alt="Mac Sunglasses">
	                                        @endif
	                                    </a>
	                                    <div class="portfolio-overlay" data-lightbox="gallery">
	                                        <a href="/assets/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
	                                        <a href="/assets/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
	                                    </div>
	                                </div>
	                                <div class="portfolio-desc">
	                                    <h3><a href="#">Setelah</a></h3>
	                                </div>
	                            </div>
	                        </div>
	                    </div><!-- .portfolio-carousel end -->
	                </div>
                    <script type="text/javascript">

                        jQuery(document).ready(function($) {

                            var relatedPortfolio = $("#related-portfolio");

                            relatedPortfolio.owlCarousel({
                                margin: 30,
                                nav: false,
                                dots: false,
                                navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
                                autoplay: true,
                                autoplayHoverPause: true,
                                responsive:{
                                    0:{ items:1 },
                                    480:{ items:2 },
                                    768:{ items:3 },
                                    992: { items:3 }
                                }
                            });

                        });

                    </script>