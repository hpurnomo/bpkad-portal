@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include('guest.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('login.form')}}">Login</a></li>
					<li class="active">Forgot Password</li>
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_full">
						<div class="style-msg2 successmsg">
							<div class="msgtitle">Reset Password Akun Anda Berhasil!</div>
							<div class="sb-msg">
								<h4>Kami telah mengirimkan Ke-Email Anda, Link untuk mereset password. Terima Kasih</h4>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection