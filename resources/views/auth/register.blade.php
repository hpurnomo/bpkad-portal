@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('login.form')}}">Login</a></li>
					<li class="active">Register</li>
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_one_fourth">&nbsp;</div>
					<div class="col_three_fourth col_last nomargin">
						<div id="processTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
							<ul class="process-steps bottommargin clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
								<li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" tabindex="0" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="true">
									<a href="{{route('signup.form')}}" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">1</a>
									<h5>Pilih Lembaga</h5>
								</li>
								<li class="ui-state-default ui-corner-top" tabindex="-1" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">2</a>
									<h5>Masukan Detail Profil</h5>
								</li>
								<li class="ui-state-default ui-corner-top" tabindex="-1" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">3</a>
									<h5>Selesai</h5>
								</li>
							</ul>
						</div>
					</div>
					<div class="col_full">
						<div class="style-msg2" style="background-color: #EEE;color: #C72C2C;">
                            <div class="msgtitle"><i class="icon-pencil2"></i>Tahapan Pendaftaran Member Baru :</div>
                            <div class="sb-msg">
                                <ol style="line-height: 25px;font-size: 15px;">
                                    <li>Pilih Lembaga pada list Lembaga dibawah ini. </li>
                                    <li>Jika Lembaga Anda tidak ditemukan. Anda Dapat membuat Lembaga Baru. Klik 
                                    <a href="assets/images/under-construction.jpeg" data-lightbox="image"> disini </a> untuk membuat Lembaga.
									</li>
                                    <li>Setelah selesai melakukan pendaftaran Kami akan mengirimkan instruksi selanjutnya via email.</li>
                                    <li>Isi field yang tersedia dengan data-data yang benar.</li>
                                </ol>
                            </div>
                        </div>
						<div class="promo promo-border promo-center bottommargin">
							<h3>LIST LEMBAGA</h3>
							<div class="divider divider-short divider-rounded divider-center"><i class="icon-pencil"></i></div>
							<table class="table table-bordered" id="Lembaga">
						        <thead>
						            <tr>
						                <th>Nomor</th>
						                <th>Kode SPKD</th>
						                <th>Nama SKPD (koordinator)</th>
						                <th>Kode Lembaga</th>
						                <th><strong style="color:red;">Nama Lembaga</strong></th>
						                <th>Alamat</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						    </table>
					    </div>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection

@section('javascript')
	@parent
   	<script>
	$(function() {
	    $('#Lembaga').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{!! route('datatables.lembaga') !!}',
	        columns: [
	            { data: 'nomor', name: 'a.nomor'},
	            { data: 'kd_skpd', name: 'a.kd_skpd' },
	            { data: 'nm_skpd', name: 'b.nm_skpd' },
	            { data: 'kd_lembaga', name: 'a.kd_lembaga' },
	            { data: 'nama_lembaga', name: 'a.nm_lembaga' },
	            { data: 'alamat', name: 'a.alamat'},
	            { data: 'action', name: 'action'}
	        ]
	    });
	});
	</script>
@endsection