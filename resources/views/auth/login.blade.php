@extends('layout-portal.master')

@section('title', 'Login')

@section('header')
	@parent
	@include('guest.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{ url('/') }}">Beranda</a></li>
					<li class="active">Login</li>
					
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">

					<div class="col_two_fifth">
						<div class="panel panel-default divcenter noradius noborder" style="max-width: 400px;border: 1px solid #cccccc !important;">
							<div class="panel-body" style="padding: 40px;">
								{{-- alert --}}
								@include('layout-portal/alert')
								<form id="login-form" name="login-form" class="nobottommargin" method="POST" action="{{route('login.form')}}">
									{!! csrf_field() !!}
									<h3>Login Pengguna</h3>
									<div class="col_full">
										<label for="login-form-username">Username/NRK/E-Mail:</label>
										<input type="text" id="login-form-username" name="username" value="{{ old('username') }}"class="form-control not-dark" required>
									</div>

									<div class="col_full">
										<label for="login-form-password">Password:</label>
										<input type="password" id="login-form-password" name="password" value="" class="form-control not-dark" required>
									</div>

									<div class="col_full nobottommargin">
										<button class="button button-3d button-green nomargin" id="login-form-submit" name="login-form-submit" value="login">Masuk</button>
										<a href="{{ route('password.by.email') }}" class="fright" style="padding-top: 10px;">Lupa Password?</a>
									</div>
								</form>
								<a href="{{ route('token.skpd.form') }}" class="fleft button button-3d button-mini button-rounded button-dirtygreen" style="margin-left:0;">Daftar Sebagai Verifikator SKPD <i class="icon-circle-arrow-right"></i></a>
							</div>
							{{-- <a href="{{ url('/page-admin') }}" target="_blank" class="fleft button button-mini button-border button-rounded" style="margin-left:0;">Masuk Sebagai PPKD <i class="icon-circle-arrow-right"></i></a> --}}
						</div>
					</div>

					<div class="col_three_fifth col_last">
						{{-- <h3>Anda Belum Memiliki Akun Pengguna? <a href="{{route('kategori.penerima')}}" class="button button-3d button-red nomargin">Daftar Sekarang .. !</a></h3> --}}
						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Pengumuman</div>
							<div class="acc_content clearfix" style="display: block;">
								@foreach ($recPengumuman as $element)
									<p>
										<b>{{ $element->title }} : </b><br>
										{{ $element->brief }}. {{-- <a href="{{ url('/pengumuman') }}/{{ $element->file_name }}" class="btn btn-circle blue btn-outline" download="">Unduh Dokumen Terkait</a> --}}
									</p>
								@endforeach
							</div>

							<div class="acctitle acctitlec"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Bagaimana cara mendapatkan Member Account?</div>
							<div class="acc_content clearfix" style="display: none;">
							<p>Silahkan untuk datang langsung ke <b>SKPD</b> koordinator dengan membawa kelengkapan data Lembaga dan berkas proposal. Selanjutnya Anda akan didaftarkan di <i><b>Sistem eHibahbansos</b></i> dan dapat melengkapi data secara mandiri serta mengajukan proposal secara <b>online</b> </p>
							</div>

							
						</div>
					</div>

				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection