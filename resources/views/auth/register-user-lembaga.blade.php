@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('login.form')}}">Login</a></li>
					<li class="active">Register</li>
					
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_one_fourth">&nbsp;</div>
					<div class="col_three_fourth col_last nomargin">
						<div id="processTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
							<ul class="process-steps bottommargin clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
								<li class="ui-state-default ui-corner-top" tabindex="0" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="true">
									<a href="{{route('signup.form')}}" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">1</a>
									<h5>Pilih Lembaga</h5>
								</li>
								<li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" tabindex="-1" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">2</a>
									<h5>Masukan Detail Profil</h5>
								</li>
								<li class="ui-state-default ui-corner-top" tabindex="-1" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">3</a>
									<h5>Selesai</h5>
								</li>
							</ul>
						</div>
					</div>
					<div class="col_full center-register">
						{{-- alert --}}
						@include('layout-portal/alert')
						<form id="register-form" name="register-form" class="nobottommargin" action="{{route('signup.lembaga.user')}}" method="post" enctype="multipart/form-data">
							{!! Form::token() !!}
							<input type="hidden" name="no_skpd" value="{{ $data['skpd'][0]->nomor }}">
							<input type="hidden" name="no_lembaga" value="{{ $data['lembaga'][0]->nomor }}">
							<input type="hidden" name="group_id" value="12">
							<input type="hidden" name="is_active" value="0">
							<div class="col_full">
								<label for="register-form-name">Kode SKPD<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="kd_skpd" value="{{ $data['skpd'][0]->kd_skpd }}" class="required form-control input-block-level" readonly>
							</div>

							<div class="col_full">
								<label for="register-form-email">Nama SKPD<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="nm_skpd" value="{{ $data['skpd'][0]->nm_skpd }}" class="required form-control input-block-level" readonly>
							</div>

							<div class="col_full">
								<label for="register-form-phone">Kode Lembaga<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="kd_lembaga" value="{{ $data['lembaga'][0]->kd_lembaga }}" class="required form-control input-block-level only-numeric" readonly>
							</div>

							<div class="col_full">
								<label for="register-form-phone">Nama Lembaga<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="nm_lembaga" value="{{ $data['lembaga'][0]->nm_lembaga }}" class="required form-control input-block-level" readonly>
							</div>
							<div class="divider divider-short divider-rounded divider-center"><i class="icon-pencil"></i></div>
							<div class="col_full">
								<label for="register-form-phone">Nama Operator Lembaga<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="real_name" value="{{ old('real_name') }}" class="required form-control input-block-level" placeholder="Masukan nama lengkap" required>
							</div>

							<div class="col_full">
								<label for="register-form-phone">No KTP<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="ktp" value="{{ old('ktp') }}" class="required form-control input-block-level only-numeric" placeholder="contoh: 317105030xxxxxxx" required>
							</div>

							<div class="col_full">
								<label for="register-form-phone">No Telp Operator Lembaga<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="telp" value="{{ old('telp') }}" class="required form-control input-block-level only-numeric" placeholder="contoh: 0811987654xx" required>
							</div>

							<div class="col_full">
								<label for="register-form-phone">Email Resmi Lembaga<strong class="text-color-red">*</strong>:</label>
								<input type="email" name="email" value="{{ old('email') }}" class="required form-control input-block-level" placeholder="Masukan email dengan benar" required>
							</div>

							<div class="col_full">
								<label for="register-form-phone">Upload KTP(Photo/Scan)<strong class="text-color-red">*</strong>:</label><br/>
								<div class="fileinput fileinput-new" data-provides="fileinput">
								  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
									<div>
										<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="photo"></span>
										<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
									</div>
								</div>
								<em style="display: block;color: red;"> *File yang diizinkan : jpg/png maksimal 1MB</em>
							</div>

							<div class="col_full" id="pwd-container">
								<label for="register-form-phone">Password<strong class="text-color-red">*</strong>:</label>
								<input type="password" name="password" value="{{ old('password') }}" class="required form-control input-block-level" required>
								<div style="margin: 10px 0;">
				                    <div class="pwstrength_viewport_progress"></div>
				                </div>
							</div>

							<div class="col_full">
								{!! app('captcha')->display(); !!}
							</div>

							<div class="clear"></div>

							<div class="col_full nobottommargin">
								<button class="button button-3d button-blue nomargin" id="register-form-submit" name="register-form-submit" value="register">DAFTAR</button>
							</div>

						</form>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection
@section('javascript')
	@parent
	<script type="text/javascript">
		$(function(){
			// upload file
			$('.fileinput').fileinput();

			// only numeric
			$( "input.only-numeric" ).on( "blur", function() {
			  $( this ).val(function( i, val ) {
			    return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
			  });
			});
		});

		jQuery(document).ready(function () {
		    "use strict";
		    var options = {};
		    options.ui = {
		        container: "#pwd-container",
		        showVerdictsInsideProgressBar: true,
		        viewports: {
		            progress: ".pwstrength_viewport_progress"
		        }
		    };
		    options.common = {
		        debug: true,
		        onLoad: function () {
		            $('#messages').text('Start typing password');
		        }
		    };
		    $(':password').pwstrength(options);
		});
	</script>
@endsection