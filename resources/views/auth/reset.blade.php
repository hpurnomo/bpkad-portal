@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include('guest.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('login.form')}}">Login</a></li>
					<li class="active">Reset Password</li>
					
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_one_fourth">&nbsp;</div>
					<div class="col_full center-register">
						{{-- alert --}}
						@include('layout-portal/alert')
						<form id="register-form" name="register-form" class="nobottommargin" action="{{route('reset.password')}}" method="post">
							{!! Form::token() !!}
							<input type="hidden" name="remember_token" value="{{ $token }}">
							<div class="col_full">
								<label for="register-form-phone">Password<strong class="text-color-red">*</strong>:</label>
								<input type="password" name="password" value="{{ old('password') }}" class="required form-control input-block-level" required>
								<div style="margin: 10px 0;">
				                    <div class="pwstrength_viewport_progress"></div>
				                </div>
							</div>

							<div class="clear"></div>

							<div class="col_full nobottommargin">
								<button class="button button-3d button-blue nomargin" id="register-form-submit" name="send-password-reset" value="password-reset"> Password Reset</button>
							</div>

						</form>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection
@section('javascript')
	@parent
	<script type="text/javascript">
		$(function(){
			// upload file
			$('.fileinput').fileinput();

			// only numeric
			$( "input.only-numeric" ).on( "blur", function() {
			  $( this ).val(function( i, val ) {
			    return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
			  });
			});
		});

		jQuery(document).ready(function () {
		    "use strict";
		    var options = {};
		    options.ui = {
		        container: "#pwd-container",
		        showVerdictsInsideProgressBar: true,
		        viewports: {
		            progress: ".pwstrength_viewport_progress"
		        }
		    };
		    options.common = {
		        debug: true,
		        onLoad: function () {
		            $('#messages').text('Start typing password');
		        }
		    };
		    $(':password').pwstrength(options);
		});
	</script>
@endsection