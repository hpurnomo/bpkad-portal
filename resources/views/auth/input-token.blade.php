@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('login.form')}}">Login</a></li>
					<li class="active">Register</li>
					
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_one_fourth">&nbsp;</div>
					<div class="col_three_fourth col_last nomargin">
						<div id="processTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
							<ul class="process-steps bottommargin clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
								<li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" tabindex="0" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="true">
									<a href="{{route('token.skpd.form')}}" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">1</a>
									<h5>Masukan Token</h5>
								</li>
								<li class="ui-state-default ui-corner-top" tabindex="-1" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">2</a>
									<h5>Masukan Detail Profil</h5>
								</li>
								<li class="ui-state-default ui-corner-top" tabindex="-1" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">3</a>
									<h5>Selesai</h5>
								</li>
							</ul>
						</div>
					</div>
					<div class="col_full center-register">
						{{-- alert --}}
						@include('layout-portal/alert')
						<form id="register-form" name="register-form" class="nobottommargin" action="{{route('verifikasi.token')}}" method="post">
							{!! Form::token() !!}
							<div class="col_full">
								<label for="register-form-phone">Input Token<strong class="text-color-red">*</strong>:</label>
								<input type="text" name="token" value="{{ old('token') }}" class="required form-control input-block-level" placeholder="contoh: 12345" required>
							</div>

							<div class="clear"></div>

							<div class="col_full nobottommargin">
								<button class="button button-3d button-blue nomargin" id="register-form-submit" name="register-form-submit" value="register">Verifikasi Token</button>
							</div>

						</form>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection
@section('javascript')
	@parent
	<script type="text/javascript">
		$(function(){
			// upload file
			$('.fileinput').fileinput();

			// only numeric
			$( "input.only-numeric" ).on( "blur", function() {
			  $( this ).val(function( i, val ) {
			    return val.replace(/[^0-9\s]/gi, '').replace(/[_\s]/g, '');
			  });
			});
		});

		jQuery(document).ready(function () {
		    "use strict";
		    var options = {};
		    options.ui = {
		        container: "#pwd-container",
		        showVerdictsInsideProgressBar: true,
		        viewports: {
		            progress: ".pwstrength_viewport_progress"
		        }
		    };
		    options.common = {
		        debug: true,
		        onLoad: function () {
		            $('#messages').text('Start typing password');
		        }
		    };
		    $(':password').pwstrength(options);
		});
	</script>
@endsection