@extends('layout-portal.master')

@section('title', 'Register')

@section('header')
	@parent
	@include($roles.'.header')
@endsection

@section('submenu')
	@parent
@endsection

@section('content')
	<!-- Content
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="{{route('login.form')}}">Login</a></li>
					<li class="active">Register</li>
					
				</ol>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_one_fourth">&nbsp;</div>
					<div class="col_three_fourth col_last nomargin">
						<div id="processTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
							<ul class="process-steps bottommargin clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
								<li class="ui-state-default ui-corner-top" tabindex="0" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="true">
									<a href="{{route('signup.form')}}" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">1</a>
									<h5>Pilih Lembaga</h5>
								</li>
								<li class="ui-state-default ui-corner-top" tabindex="-1" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">2</a>
									<h5>Masukan Detail Profil</h5>
								</li>
								<li class="ui-state-default ui-corner-top  ui-tabs-active ui-state-active" tabindex="-1" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false">
									<a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">3</a>
									<h5>Selesai</h5>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="col_full">
						<div class="style-msg2 successmsg">
							<div class="msgtitle">Registrasi Anda Berhasil!</div>
							<div class="sb-msg">
								<h4 class="text-color-red">No Registrasi: {{$data['noReg']}}</h4>
								<h4>Kami telah mengirimkan Kode Aktifiasi Akun Ke-Email Anda. Segera cek email Anda dan lakukan Aktifasi Akun. Terima kasih</h4>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section><!-- #content end -->
@stop
@section('footer')
	@parent
	@include('layout-portal.footer')
@endsection