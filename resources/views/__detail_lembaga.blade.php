<div class="col_full margin-top-35">
						<div class="fancy-title title-dotted-border title-center">
							<h4><span>Gallery</span></h4>
						</div>
						<div id="related-portfolio" class="owl-carousel portfolio-carousel">
	                        <div class="oc-item">
	                            <div class="iportfolio">
	                                <div class="portfolio-image">
	                                    <a href="#">
	                                    	@if(!empty($data['fotoProposal1'][0]->file_name))
	                                        <img src="{{URL::to('/')}}/foto_proposal/{{$data['fotoProposal1'][0]->file_name}}" style="height:280px;">
	                                        @else
	                                        <img src="/assets/images/portfolio/4/4.jpg" alt="Mac Sunglasses">
	                                        @endif
	                                    </a>
	                                    <div class="portfolio-overlay" data-lightbox="gallery">
	                                        <a href="/assets/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
	                                        <a href="/assets/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
	                                    </div>
	                                </div>
	                                <div class="portfolio-desc">
	                                    <h3><a href="#">Sebelum</a></h3>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="oc-item">
	                            <div class="iportfolio">
	                                <div class="portfolio-image">
	                                    <a href="#">
	                                        @if(!empty($data['fotoProposal2'][0]->file_name))
	                                        <img src="{{URL::to('/')}}/foto_proposal/{{$data['fotoProposal2'][0]->file_name}}" style="height:280px;">
	                                        @else
	                                        <img src="/assets/images/portfolio/4/4.jpg" alt="Mac Sunglasses">
	                                        @endif
	                                    </a>
	                                    <div class="portfolio-overlay" data-lightbox="gallery">
	                                        <a href="/assets/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
	                                        <a href="/assets/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
	                                    </div>
	                                </div>
	                                <div class="portfolio-desc">
	                                    <h3><a href="#">Sedang</a></h3>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="oc-item">
	                            <div class="iportfolio">
	                                <div class="portfolio-image">
	                                    <a href="#">
	                                        @if(!empty($data['fotoProposal3'][0]->file_name))
	                                        <img src="{{URL::to('/')}}/foto_proposal/{{$data['fotoProposal3'][0]->file_name}}" style="height:280px;">
	                                        @else
	                                        <img src="/assets/images/portfolio/4/4.jpg" alt="Mac Sunglasses">
	                                        @endif
	                                    </a>
	                                    <div class="portfolio-overlay" data-lightbox="gallery">
	                                        <a href="/assets/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
	                                        <a href="/assets/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
	                                    </div>
	                                </div>
	                                <div class="portfolio-desc">
	                                    <h3><a href="#">Setelah</a></h3>
	                                </div>
	                            </div>
	                        </div>
	                    </div><!-- .portfolio-carousel end -->
	                </div>
                    <script type="text/javascript">

                        jQuery(document).ready(function($) {

                            var relatedPortfolio = $("#related-portfolio");

                            relatedPortfolio.owlCarousel({
                                margin: 30,
                                nav: false,
                                dots: false,
                                navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
                                autoplay: true,
                                autoplayHoverPause: true,
                                responsive:{
                                    0:{ items:1 },
                                    480:{ items:2 },
                                    768:{ items:3 },
                                    992: { items:3 }
                                }
                            });

                        });

                    </script>