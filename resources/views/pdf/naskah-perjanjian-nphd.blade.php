<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
	<style type="text/css">
	   table { page-break-inside:auto; }
	   tr    { page-break-inside:avoid; page-break-after:auto; }
       footer {page-break-after: always;}
	</style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;margin-top: 20px;margin-bottom: 20px;top:30px;">
<center style="margin-top: 10px;">
    <h4>NASKAH PERJANJIAN HIBAH DAERAH DALAM BENTUK UANG<br><br> PERJANJIAN <br><br> 
		ANTARA <br><br>
		PEMERINTAH PROVINSI DAERAH KHUSUS <br><br> 
		IBUKOTA JAKARTA <br><br>
		DAN <br><br>
		{{ strtoupper($recProposalByID->nm_lembaga) }} <br><br> 
		TENTANG <br><br>
		PEMBERIAN HIBAH DALAM BENTUK UANG
	</h4>
</center>
<p style="padding: 30px 30px 0 50px;">
Pada hari ini <b>...................................{{-- {{$dataCNPHD['hari1']}} --}}</b> tanggal <b>..........{{-- {{$dataCNPHD['tanggal1']}} --}}</b> bulan <b>...................................{{-- {{$dataCNPHD['bulan1']}} --}}</b> tahun <b>.................{{-- {{$dataCNPHD['tahun1']}} --}}</b>
yang bertanda tangan di bawah ini: 
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
	<tr>
		<td>I.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Nama : <b>{{$dataCNPHD['nama1']}}</b></td>
	</tr>
	<tr>
		<td></td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">
        Dalam Jabatan sebagai {{$dataCNPHD['ketPLT']}} Kepala {{ $recProposalByID->nm_skpd }} Pemberi Rekomendasi  dengan alamat {{ $recProposalByID->alamatSKPD }};<br>
        oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah Provinsi DKI Jakarta, untuk selanjutnya disebut PIHAK PERTAMA.</td>
	</tr>
</table>
<br><br>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
	<tr>
		<td>II.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Nama : <b>{{$dataCNPHD['nama2']}}</b></td>
	</tr>
	<tr>
		<td></td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Jabatan <b>{{$dataCNPHD['jabatan1']}}</b> berkantor di. <b>{{$dataCNPHD['alamat']}}</b> dalam hal ini menjalani jabatannya sesuai {{$dataCNPHD['deskripsi_jabatan']}}, 
        oleh karenanya sah berwenang bertindak untuk dan atas nama Pemerintah/Pemerintah Daerah/Kelompok Masyarakat/Organisasi Kemasyarakatan untuk selanjutnya disebut PIHAK KEDUA.</td>
	</tr>
</table>
<p style="padding: 0 50px 0 50px;text-align: justify;">Bahwa masing-masing pihak bertindak dalam jabatannya sebagaimana tersebut di atas, secara bersama-sama disebut PARA PIHAK dengan terlebih dahulu memperhatikan ketentuan sebagai berikut :</p>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
	<tr>
		<td style="vertical-align: top;">1.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Undang-Undang Nomor 9 Tahun 2015;</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">2.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Undang-Undang Nomor 29 Tahun 2007 tentang Pemerintahan Provinsi Daerah Khusus Ibukota Jakarta sebagai Ibukota Negara Kesatuan Republik Indonesia;</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">3.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Peraturan Pemerintah Nomor 12 Tahun 2019 tentang Pengelolaan Keuangan Daerah;</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">4.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;"> Peraturan Menteri Dalam Negeri Nomor 13 Tahun 2006 tentang Pedoman Pengelolaan Keuangan Daerah sebagaimana telah beberapa kali diubah terakhir dengan Peraturan Menteri Dalam Negeri Nomor 21 Tahun 2011;
    </td>
	</tr>
	<tr>
		<td style="vertical-align: top;">5.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Peraturan Daerah Nomor 5 Tahun 2007 tentang Pokok-pokok Pengelolaan Keuangan Daerah;</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">6.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Peraturan Daerah Nomor 5 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Provinsi Daerah Khusus Ibukota Jakarta sebagaimana telah diubah dengan Peraturan Daerah Nomor 2 Tahun 2019;</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">7.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Peraturan {{$dataCNPHD['peraturan4']}} Nomor {{$dataCNPHD['nomor4']}} Tahun {{$dataCNPHD['tahun4']}}
        tentang {{$dataCNPHD['tentang4']}};</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">8.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Peraturan Gubernur Nomor 142 Tahun 2013 tentang Sistem dan Prosedur Pengelolaan Keuangan. Daerah sebagaimana telah di-ubah dengan Peraturan Gubernur Nomor 161 Tahun 2014;</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">9.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Peraturan Gubernur Nomor 142 tahun 2018 tentang Pedoman Pemberian Hibah dan Bantuan Sosial yang Bersumber dari Anggaran Pendapatan dan Belanja Daerah sebagaimana telah beberapa kali diubah terakhir dengan Peraturan Gubernur Nomor 20 tahun 2020;</td>
	</tr>
	<tr> 
		<td style="vertical-align: top;">10.</td>
		<td style="padding: 0 50px 0 10px;text-align: justify;">Keputusan Gubernur Nomor {{$dataCNPHD['nomor6']}} Tahun
		{{ $dataCNPHD['tahun6'] }} tentang {{ $dataCNPHD['tentang6'] }};</td>
	</tr>
</table>

<p style="padding: 0 50px 0 50px;text-align: justify;">Bahwa berdasarkan hal tersebut dan sesuai dengan rekomendasi {{$dataCNPHD['namaSKPD']}} Nomor <b>{{$dataCNPHD['nomor7']}}</b> tanggal 
{{-- harusnya tanggal --}}
<b>{{$dataCNPHD['tanggal7']}} {{$dataCNPHD['bulan7']}} {{$dataCNPHD['tahun7']}}</b>,
PARA PIHAK sepakat untuk melakukan Perjanjian Hibah dalam bentuk uang, dengan syarat dan ketentuan sebagai berikut :</p>
<center>
    Pasal 1
    <br><br>
    JUMLAH DAN TUJUAN HIBAH  
</center>
<br>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
    <tr>
        <td style="vertical-align: top;">(1)</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">PIHAK PERTAMA memberikan Hibah berupa uang melalui transfer dana kepada PIHAK KEDUA sebagaimana PIHAK KEDUA menerima dari PIHAK PERTAMA senilai Rp.{{$dataCNPHD['senilai_angka']}} ( {{ $dataCNPHD['senilai_teks'] }} rupiah ).</td>
    </tr>
    <tr>
        <td style="vertical-align: top;">(2)</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">Hibah sebagaimana dimaksud pada ayat (1) dipergunakan sesuai dengan Rencana Penggunaan Hibah/Proposal yang merupakan bagian yang tidak terpisahkan dari naskah perjanjian hibah daerah ini.</td>
    </tr>
    <tr>
        <td style="vertical-align: top;">(3)</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">Penggunaan belanja hibah sebagaimana dimaksud pada ayat (2) bertujuan untuk</td>
    </tr>
    <tr>
        <td style="vertical-align: top;"></td>
        <td>{!! $dataCNPHD['kegiatan2'] !!}</td>
    </tr>
</table>

    <br>
     <center>
        Pasal 2
        <br><br>
        PENGGUNAAN
    </center>
    <br>
    <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
        <tr>
            <td style="vertical-align: top;">(1)</td>
            <td style="padding: 0 50px 0 10px;text-align: justify;">PIHAK KEDUA menggunakan belanja Hibah berupa uang sebagaimana dimaksud dalam Pasal 2 ayat (1) sesuai dengan Rencana Penggunaan Rencana Penggunaan Hibah/Proposal.</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">(2)</td>
            <td style="padding: 0 50px 0 10px;text-align: justify;">Belanja Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk :</td>
        </tr>
    </table>
    <br>

@if($recProposalByID->noper >= 9 )
<?php

function remove_accent($str)
{
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', "'", 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
} 
$string = base64_decode($dataCNPHD['kegiatan1']);
$string= iconv("UTF-8","ISO-8859-1//IGNORE",$string);
echo remove_accent(iconv("ISO-8859-1","UTF-8",$string));

?> 
@else
{!!  $dataCNPHD['kegiatan1'] !!}
@endif

 <table border="1" cellpadding="1" cellspacing="1" width="100%"> 
    <tbody> 
        <tr>
            <td style="text-align: right;padding: 10px;font-style: italic;">   
                <i>{{ $dataCNPHD['totalTerbilangKegiatan1'] }}</i>  
            </td> 
        </tr> 
    </tbody>
</table>
<br>
<center>
                    Pasal 3
                    <br><br>
                    HAK DAN KEWAJIBAN PIHAK KEDUA
                </center>
                <br>

                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;">PIHAK KEDUA menerima dana Hibah dari PIHAK PERTAMA untuk digunakan sesuai dengan penggunaan sebagaimana dimaksud dalam Pasal 2.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;">Dalam hal PIHAK KEDUA telah menerima Hibah 1 (satu) tahun sebelumnya, pencairan dan penyaluran dana Hibah sebagaimana dimaksud pada ayat (1) akan diperhitungkan sisa dana penggunaan Hibah yang tertuang dalam Laporan Penggunaan Hibah tahun sebelumnya yaitu sebesar Rp. {{ $dataCNPHD['total1'] }} </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;">PIHAK KEDUA wajib menandatangani Pakta Integritas yang menyatakan bahwa Hibah yang diterimakan digunakan sesuai dengan Naskah Perjanjian Hibah daerah ini. </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(4)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;"> PIHAK KEDUA selaku penerima Hibah dan objek pemeriksaan, wajib menyimpan bukti-bukti pengeluaran yang lengkap dan sah sesuai peraturan perundang-undangan. </td> 
                    </tr>

                </table>
                <br>
                <center>
                    Pasal 4
                    <br><br>
                    HAK DAN KEWAJIBAN PIHAK PERTAMA
                </center>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;"> PIHAK PERTAMA wajib memberikan dana Hibah kepada PIHAK KEDUA melalui transfer ke rekening Bank PIHAK KEDUA apabila seluruh persyaratan dan kelengkapan berkas pengajuan dana Hibah telah dipenuhi oleh PIHAK KEDUA. </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;"> Dalam hal PIHAK KEDUA telah menerima Hibah 1 (satu) tahun sebelumnya, pencairan dan penyaluran dana Hibah sebagaimana dimaksud pada ayat (1) dilakukan setelah diperhitungkan sisa dana penggunaan Hibah yang tertuang dalam laporan penggunaan Hibah tahun sebelumnya yaitu sebesar Rp. {{ $dataCNPHD['total2'] }}</td>
                    </tr>

                     <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;">  PIHAK PERTAMA Menunda pencairan belanja Hibah apabila PIHAK KEDUA tidak/ belum memenuhi persyaratan dan kelengkapan berkas pengajuan dana sesuai dengan peraturan perundang-undangan. </td>
                    </tr>
                </table>
                <br>
                <center>
                    Pasal 5
                    <br><br>
                    TATA CARA PELAPORAN 
                </center>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
                    <tr>
                        <td style="vertical-align: top;">(1)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;">PIHAK KEDUA membuat dan menyampaikan laporan penggunaan Hibah kepada Gubernur melalui Kepala SKPD/UKPD Pemberi Rekomendasi dengan tembusan Kepala BPKD selaku PPKD paling lambat 1 (satu) bulan berikutnya setelah pelaksanaan kegiatan selesai atau tanggal 10 Maret Tahun Anggaran berikutnya. 
                    </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(2)</td>
                        <td style="padding: 0 50px 0 10px;text-align: justify;">) Laporan penggunaan Hibah sebagaimana dimaksud dilengkapi dengan surat pernyataan tanggung jawab bermeterai cukup dari PIHAK KEDUA yang menyatakan bahwa Hibah yang telah diterima telah dipergunakan sesuai dengan NPHD. </td>
                    </tr>
                </table>
                <br>

                 <center>
                    Pasal 
                    <br><br>
                    AUDIT
                </center>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
                    <tr> 
                        <td style="padding: 0 50px 0 10px;text-align: justify;">Dalam kondisi tertentu penerima Hibah dan Bantuan Sosial dapat dilakukan audit oleh aparat pengawas fungsional. 
                    </td> 
                </table>
                <br> 
                 <p style="padding: 0 50px 0 50px;text-align: justify;">Demikian Perjanjian Hibah ini dibuat dan ditandatangani di Jakarta pada hari dan tanggal tersebut di atas dalam rangkap 2 (dua), masing-masing bermeterai cukup dan mempunyai kekuatan hukum yang sama, 1 (satu) eksemplar untuk PIHAK PERTAMA dan 1 (satu) eksemplar untuk PIHAK KEDUA. </p>
                <br>

                <table width="100%">
                    <tr>
                        <td width="30%">
                            <center>
                            <h4 style="margin: 0;">PIHAK KEDUA <br>{{$dataCNPHD['jabatan2']}}<br> &nbsp;,</h4>
                            <br><br><br><br><br><br><br><br>
                            <h4 style="margin: 0;">{{$dataCNPHD['nama_pihak_kedua']}}</h4>
                            <h4 style="margin: 0;">NIP/NRP* {{$dataCNPHD['nip_pihak_kedua']}}</h4>
                            <br>
                            <small>*) bagi penerima hibah instansi Pemerintah</small>
                            </center>
                        </td>
                        <td width="25%">
                        	<center>
                                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
                            </center>
                        </td>
                        <td width="35%">
                        	<center>
                            <h4 style="margin: 0;">PIHAK PERTAMA <br>{{$dataCNPHD['ketPLT']}} KEPALA {{ $recProposalByID->nm_skpd }},</h4>
                            <br><br><br><br><br><br><br><br>
                            <h4 style="margin: 0;">{{$dataCNPHD['nama_kepala']}}</h4>
                            <h4 style="margin: 0;">NIP {{$dataCNPHD['nip_kepala']}}</h4>
                            <br>
                            <em><br><br></em>
                            </center>
                        </td>
                    </tr>
                </table>

    </body>
</html>