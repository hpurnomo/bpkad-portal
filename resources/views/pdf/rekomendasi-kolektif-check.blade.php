@extends('layout-portal.master')

@section('title', 'Beranda')

@section('header')
    @parent
    @include('guest.header')
@endsection
 

@section('content')

<!-- Content
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Beranda</a></li>
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li class="active">Cek Dokumen</li>
                </ol>
            </div>

        </section>

        <section id="content">

            <div class="content-wrap">
                <div class="container clearfix">

                <iframe src="{{ URL::to('qrcode/pdf') }}/{{ $uuid }}" width="100%" height="500px"></iframe>
                    

                </div>
            </div>

        </section><!-- #content end -->
@stop
@section('footer')
    @parent
    @include('layout-portal.footer')
@endsection

 