<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
	<style type="text/css">
	   table { page-break-inside:auto }
	   tr    { page-break-inside:avoid; page-break-after:auto }
	</style>
</head>
<body>
<center>
    <br>
    <br> 
    <h4 style="margin: 0 0 10px 0;">HASIL PENELITIAN KELENGKAPAN ADMINISTRASI</h4>
    <h4 style="text-transform: uppercase;margin: 0 0 10px 0;">{{$recProposalByID->nm_lembaga}}</h4>
	<h4 style="margin: 0 0 10px 0;">TAHUN ANGGARAN {{$recProposalByID->tahun_anggaran}}</h4>
	<h4 style="margin: 0 0 10px 0;">NOMOR {{$dataCka['nomor']}}</h4>
</center>
<br><br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada hari ini <b>{{$dataCka['hari']}}</b> tanggal <b>{{$dataCka['tanggal1']}}</b> bulan <b>{{$dataCka['bulan1']}}</b> tahun <b>{{$dataCka['tahun1']}}</b> yang bertanda tangan di bawah ini :</p>
<br>
<table border="1" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td style="padding: 10px;"><center>No</center></td>
		<td style="padding: 10px;"><center>Nama</center></td>
		<td style="padding: 10px;"><center>Jabatan Dalam Tim</center></td>
		<td style="padding: 10px;"><center>Tanda Tangan</center></td>
	</tr>
	@foreach ($dataDcka as $key=>$value)
	<tr>
		<td width="10"><center>{{ $value->no_urut }}</center></td>
		<td style="padding: 13px;">{{ $value->nama }}</td>
		<td style="padding: 13px;">{{ $value->jabatan }}</td>
		<td></td>
	</tr>
	@endforeach
</table>
<br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan Surat Tugas Kepala <b>{{$recProposalByID->nm_skpd}}</b> Nomor <b>{{$dataCka['nomor2']}}</b>, tanggal <b>{{$dataCka['tanggal2']}}</b> bulan <b>{{$dataCka['bulan2']}}</b> tahun <b>{{$dataCka['tahun2']}}</b> telah melakukan penelitian administrasi terhadap:</p>
<br>
<table width="100%">
	<tr>
		<td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama(Lembaga)</td>
		<td>:</td>
		<td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
	</tr>
	<tr>
		<td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat(Lembaga)</td>
		<td>:</td>
		<td>{{ucwords($recProposalByID->alamatLembaga)}} Kota {{ucwords($recProposalByID->nmkota)}},Kecamatan {{ucwords($recProposalByID->nmkec)}},Kelurahan {{ucwords($recProposalByID->nmkel)}},RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}</td>
	</tr>
</table>
<br>
<p>Dengan hasil sebagai berikut :</p>
<table  border="1" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td style="padding: 10px;"><center>No</center></td>
		<td style="padding: 10px;"><center>Data Administrasi</center></td>
		<td style="padding: 10px;"><center>Ada</center></td>
		<td style="padding: 10px;"><center>Tidak Ada</center></td>
		<td style="padding: 10px;"><center>Keterangan</center></td>
	</tr>
	<tr>
		<td><center>1</center></td>
		<td style="padding: 10px;">&nbsp;Nama dan Identitas</td>
		<td><center>@if($dataKal['cek_ktp_ketua']==1)<input type="checkbox" checked="">@endif</center></td>
		<td><center>@if($dataKal['cek_ktp_ketua']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_ktp_ketua'] !!}</td>
	</tr>
	<tr>
		<td><center>2</center></td>
		<td style="padding: 10px;">&nbsp;Alamat</td>
		<td><center>@if($dataKal['cek_alamat_lembaga']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_alamat_lembaga']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_alamat_lembaga'] !!}</td>
	</tr>
	<tr>
		<td><center>3</center></td>
		<td style="padding: 10px;">&nbsp;Aktivitas</td>
		<td><center>@if($dataKap['cek_aktivitas']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKap['cek_aktivitas']==2)<input type="checkbox" checked=""></i>@endif</center></td>
        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_aktivitas'] !!}</td>
	</tr>
	<tr>
		<td><center>4</center></td>
		<td style="padding: 10px;">&nbsp;Kepengurusan</td>
		<td><center>@if($dataKap['cek_kepengurusan']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKap['cek_kepengurusan']==2)<input type="checkbox" checked=""></i>@endif</center></td>
        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_kepengurusan'] !!}</td>
	</tr>
	<tr>
		<td><center>5</center></td>
		<td style="padding: 10px;">&nbsp;Rencana Anggaran Biaya</td>
		<td><center>@if($dataKap['cek_rab']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKap['cek_rab']==2)<input type="checkbox" checked=""></i>@endif</center></td>
        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_rab'] !!}</td>
	</tr>
	<tr>
		<td><center>6</center></td>
		<td style="padding: 10px;">&nbsp;Saldo akhir tahun lalu beserta rekening Bank</td>
		<td><center>@if($dataKal['cek_rekening_lembaga']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_rekening_lembaga']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_rekening_lembaga'] !!}</td>
	</tr>
	<tr>
		<td><center>7</center></td>
		<td style="padding: 10px;">&nbsp;Waktu pelaksanaan</td>
		<td><center>@if($dataKap['cek_waktu_pelaksanaan']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKap['cek_waktu_pelaksanaan']==2)<input type="checkbox" checked=""></i>@endif</center></td>
        <td style="padding-left: 10px;">{!! $dataKap['keterangan_cek_waktu_pelaksanaan'] !!}</td>
	</tr>
	<tr>
		<td><center>8</center></td>
		<td style="padding: 10px;">&nbsp;Bantuan yang pernah diterima 1(satu) tahun sebelumnya apabila ada</td>
		<td><center>@if($dataKal['cek_bantuan_tahun_sebelum']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_bantuan_tahun_sebelum']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_bantuan_tahun_sebelum'] !!}</td>
	</tr>
	<tr>
		<td><center>9</center></td>
		<td style="padding: 10px;">&nbsp;Nomor Pokok Wajib Pajak (aslinya)*</td>
		<td><center>@if($dataKal['cek_npwp']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_npwp']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_npwp'] !!}</td>
	</tr>
	<tr>
		<td><center>10</center></td>
		<td style="padding: 10px;">&nbsp;Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau pembentukan Organisasi/Lembaga (aslinya)*</td>
		<td><center>@if($dataKal['cek_akta']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_akta']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_akta'] !!}</td>
	</tr>
	<tr>
		<td><center>11</center></td>
		<td style="padding: 10px;">&nbsp;Surat Keterangan Domisili dari Kelurahan setempat (aslinya)*</td>
		<td><center>@if($dataKal['cek_skd']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_skd']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_skd'] !!}</td>
	</tr>
	<tr>
		<td><center>12</center></td>
		<td style="padding: 10px;">&nbsp;Sertifikat Tanah/Bukti Kepemilikan Tanah atau perjanjian kontrak atau sewa gedung/bangunan (aslinya)*</td>
		<td><center>@if($dataKal['cek_sertifikat']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_sertifikat']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_sertifikat'] !!}</td>
	</tr>
	<tr>
		<td><center>13</center></td>
		<td style="padding: 10px;">&nbsp;Izin Operasioanl/tanda daftar bagi Lembaga/Yayasan dari instansi yang berwenang (aslinya)*</td>
		<td><center>@if($dataKal['cek_izin_operasional']==1)<input type="checkbox" checked=""></i>@endif</center></td>
        <td><center>@if($dataKal['cek_izin_operasional']==2)<input type="checkbox" checked=""></i>@endif</center></td>
		<td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_izin_operasional'] !!}</td>
	</tr>
</table>
<br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian laporan ini dibuat dengan sebenarnya dan agar dapat dipergunakan sebagaimana mestinya.</p>
<p>Catatan:<br>*disesuikan dengan status/kedudukan kelembagaan Pemohon.</p>
<table width="100%">
	<tr>
		<td width="30%">
			<center>
                {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
            </center>
		</td>
		<td width="30%"></td>
		<td><center>
			<h4>KETUA TIM EVALUASI</h4>
        	<br><br><br>
        	<h4>{{ucwords($dataCka['nama_ketua'])}}</h4>
        	<h4>NIP {{ucwords($dataCka['nip'])}}</h4>
        	</center>
		</td>
	</tr>
</table>
</body>
</html>