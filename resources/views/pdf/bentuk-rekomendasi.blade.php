<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <style type="text/css">
       table { page-break-inside:auto }
       tr    { page-break-inside:avoid; page-break-after:auto }
    </style>
</head>
<body>
<center>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    {{-- <h4>(BENTUK REKOMENDASI)</h4> --}}
    {{-- <h4 style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</h4> --}}
</center>
<br><br>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="60%">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><b>Nomor</b></td>
                    <td>:</td>
                    <td>{{$dataCrekomendasi['nomor']}}</td>
                </tr>
                <tr>
                    <td><b>Sifat</b></td>
                    <td>:</td>
                    <td>{{$dataCrekomendasi['sifat']}}</td>
                </tr>
                <tr>
                    <td><b>Lampiran</b></td>
                    <td>:</td>
                    <td>{{$dataCrekomendasi['lampiran']}}</td>
                </tr>
                <tr>
                    <td style="vertical-align: top"><b>Hal</b></td>
                    <td style="vertical-align: top;">:</td>
                    <td>Rekomendasi Pengusulan<br> Permohonan Hibah/Bantuan<br>
                    Sosial/Bantuan Keuangan</td>
                </tr>
            </table>
        </td>
        <td width="40%">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <center>
                        <b>{{ $dataCrekomendasi['tanggal1'] }} {{ $dataCrekomendasi['bulan1'] }} {{ $dataCrekomendasi['tahun1'] }}</b>
                        </center> 
                    </td>
                </tr>
                <tr>
                    <td style="padding: 15px;"><center><b>Kepada</b></center></td>
                </tr>
                <tr>
                    <td style="padding: 15px;">Yth. Gubernur Provinsi DKI Jakarta melalui Tim Anggaran Pemerintah Daerah</td>
                </tr>
                <tr>
                    <td style="padding: 15px;">di Jakarta</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<p style="padding: 30px 30px 0 30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan pemohon Hibah/Bantuan Sosial/Bantuan Keuangan yang di ajukan kepada Gubernur sesuai surat Nomor <b>{{$dataCrekomendasi['nomor2']}}</b> tanggal <b>{{ $dataCrekomendasi['tanggal2'] }} {{ $dataCrekomendasi['bulan2'] }} {{ $dataCrekomendasi['tahun2'] }}</b> hal <b>{{$dataCrekomendasi['hal']}}</b> dan setelah dilakukan penelitian administrasi dan peninjauan lapangan, dengan ini direkomendasikan kepada : 
<table width="100%">
    <tr>
        <td width="190" style="padding-left: 30px;">Nama</td>
        <td>:</td>
        <td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
    </tr>
    <tr>
        <td width="190" style="padding-left: 30px;">Alamat</td>
        <td>:</td>
        <td>{{ucwords($recProposalByID->alamatLembaga)}}, RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}}, Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}.</td>
    </tr>
    <tr>
        <td width="190" style="padding-left: 30px;">Sebesar</td>
        <td>:</td> 
        <td>Rp. {{ number_format($recProposalByID->nominal_rekomendasi,2,',','.') }}</td> 
    </tr>
</table>
<br>
<p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk dapat diproses lebih lanjut sesuai ketentuan peraturan perundang-undangan.</p>
<table width="100%">
    <tr>
        <td width="30%">
            <center>
                {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
            </center>
        </td>
        <td width="30%"></td>
        <td><center>
            <h4 style="text-transform: uppercase;margin: 0;padding: 0;">KEPALA SKPD/UKPD</h4>
            <h4 style="text-transform: uppercase;margin: 0;padding: 0;">{{$recProposalByID->nm_skpd}}</h4>
            <br><br><br>
            <h4>{{$dataCrekomendasi['nama_kepala']}}</h4>
            <h4>NIP {{$dataCrekomendasi['nip']}}</h4>
            </center>
        </td>
    </tr>
</table>
<p>
    Tembusan : 
    <ol>
        <li>Sekretaris Daerah Provinsi DKI Jakarta</li>
        <li>Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta</li>
        <li>Kepala Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta</li>
    </ol>
</p>
</body>
</html>