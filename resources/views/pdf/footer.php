@extends('layout-portal.master')

@section('title', 'Beranda')

@section('header')
    @parent
    @include('guest.header')
@endsection
 

@section('content')

<!-- Content
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Beranda</a></li>
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li class="active">Cek Dokumen</li>
                </ol>
            </div>

        </section>

        <section id="content">

            <div class="content-wrap">
                <div class="container clearfix">

                <iframe src="{{ URL::to('qrcode/pdf') }}/{{ $uuid }}" width="100%" height="500px"></iframe>
                   <!--  <div class="fancy-title title-dotted-border title-center">
                        <h3>Kategori Penerima Yang Tersedia</h3>
                    </div> -->
                     
                    <!-- <div class="pre-scrollable"  >    

                    <table style="width:1024px;" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td width="40%">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
                                    <tr>
                                        <td>Nomor </td>
                                        <td> : </td>
                                        <td>&nbsp;&nbsp; {{ $dataCetakkomendasiPencairan['nomor'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sifat </td>
                                        <td> : </td>
                                        <td> &nbsp;&nbsp; {{ $dataCetakkomendasiPencairan['sifat'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Lampiran </td>
                                        <td> : </td>
                                        <td>&nbsp;&nbsp; {{ $dataCetakkomendasiPencairan['lampiran'] }}</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top">Hal </td>
                                        <td style="vertical-align: top;"> : </td>
                                        <td width="200px;"> <div style="padding-left: 10px"> Rekomendasi Pengusulan  {{ $dataCetakkomendasiPencairan['jenisBantuan'] }} Dalam Bentuk Uang pada {{ $periode->keterangan }} 
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="20%">
                                
                            </td>
                            <td width="40%">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <center>{{ $dataCetakkomendasiPencairan['tanggal'] }} {{ $dataCetakkomendasiPencairan['bulan'] }} {{ $dataCetakkomendasiPencairan['tahun'] }}
                                            </center> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 10px;"><center>Kepada</center></td>
                                    </tr>
                                   <tr>
                                        <td style="padding: 10px;" width="70%"> <div style="padding-left: 100px">Yth. Gubernur Provinsi DKI Jakarta melalui Tim Anggaran Pemerintah Daerah <br><br> di <br><br> Jakarta </div> </td>
                                    </tr> 
                                </table>
                            </td>
                        </tr>
                    </table>

                     <div style="padding: 30px 30px 0 150px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Sehubungan dengan permohonan pemohon {{ $dataCetakkomendasiPencairan['jenisBantuan'] }}   yang diajukan kepada Gubernur setelah dilakukan penelitian administrasi dan peninjauan lapangan, dengan ini direkomendasikan kepada penerima {{ $dataCetakkomendasiPencairan['jenisBantuan'] }} sebagaimana rincian terlampir. </div> 
                        
                       <div style="padding: 30px 30px 0 150px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Untuk dapat diproses lebih lanjut sesuai ketentuan peraturan perundang-undangan.</div>
                        
                        
                    <div style="padding: 30px 30px 0 150px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat rekomendasi ini dibuat dengan sebenar-benarnya dan saya bertanggung jawab penuh atas keabsahan rekomendasi ini.</div>
                     

                     <table style=" margin-left: 70%;">  
                                <tr>
                                    <td><center>
                                   <h4 style="text-transform: uppercase;">KEPALA {{ $skpd->nm_skpd }}</h4>
                                    
                                    <br><br>
                                    <h4>{{ $dataCetakkomendasiPencairan['nama_kepala'] }} 
                                    <br>NIP {{ $dataCetakkomendasiPencairan['nip'] }}</h4>
                                    </center> 
                                </td>
                            </tr>
                            </table>

                        <table style="width:70%">
                            <tr>
                                <td>Tembusan : </td>
                            </tr>
                            <tr>
                                <td> 1. Sekretaris Daerah Provinsi DKI Jakarta </td>
                            </tr>
                            <tr>
                                <td> 2. Kepala Badan Perencanaan Pembangunan Daerah Provinsi DKI Jakarta </td>
                            </tr>
                            <tr>
                                <td> 3. Kepala Badan Pengelola Keuangan Daerah Provinsi DKI Jakarta  </td>
                            </tr>
                        </table> 
                        <br><br>
                        {!! $qrcode; !!}

                        <div class="pagebreak">

                            <center> 
                                <h2 style="text-transform: uppercase;">&nbsp; Daftar Rekomendasi</h2>
                            </center>
                            <br><br>

                            <table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-bordered"> 
                              <tr>
                                <th rowspan="3" style="text-align: center;">No</th>
                                <th rowspan="3" style="text-align: center;width:100px;">Nama</th>
                                <th rowspan="3" style="text-align: center;width:100px;">Alamat</th>
                                <th colspan="3" style="text-align: center;width:600px;">Tujuan & Rencana Kegiatan </th>
                                <th rowspan="3" style="text-align: center;">Keterangan</th>
                              </tr>
                              <tr>
                                <td rowspan="2" style="text-align: center;width:400px;">Kegiatan</td>
                                <td colspan="2" style="text-align: center;width:200px;">Besaran</td>
                              </tr>
                              <tr>
                                <td style="text-align: center;width:100px;">Usulan</td>
                                <td style="text-align: center;width:100px;">Rekomendasi</td>
                              </tr>


                              <?php
                                $detail = DB::table("cetak_rekomendasi_kolektif_detail")->where("id_kol_head",$dataCetakkomendasiPencairan->uuid)->get(); 
                                $i=1;
                                foreach ($detail as $row) {  
                                    $dataDetail = DB::table('mst_lembaga as a')
                                                            ->select('a.nomor','a.nm_lembaga','a.kd_lembaga','a.alamat',
                                                                'd.jdl_prop','d.no_prop','d.maksud_tujuan','w.nmkota','x.nmkec','y.nmkel')
                                                                  ->leftJoin('mst_skpd as b','a.kd_skpd','=','b.kd_skpd')
                                                                  ->join('trx_proposal as d','a.nomor','=','d.no_lembaga') 
                                                                  ->leftJoin('mst_kota as w','a.nokota','=','w.nokota')
                                                                  ->leftJoin('mst_kecamatan as x','a.nokec','=','x.nokec')
                                                                  ->leftJoin('mst_kelurahan as y','a.nokel','=','y.nokel')
                                                                  ->where('d.noper',$row->noper)
                                                                  ->where('a.nomor',$row->id_lembaga)
                                                                  ->first(); 

                                      echo '<tr>
                                        <td>'.$i.'</td>
                                        <td>'.$dataDetail->nm_lembaga.'</td>
                                        <td>'.$dataDetail->alamat.'<br> Kelurahan. '.$dataDetail->nmkel.' <br>Kecamatan. '.$dataDetail->nmkec.'<br>Kota/Kabupaten. '.$dataDetail->nmkota.' </td>
                                        <td colspan="3" style="width:600px;">
                                        <table width="100%">  
                                            <tr>
                                                <td style="width:400px;border-bottom:solid 1px gray;">Tujuan :  '.$dataDetail->maksud_tujuan.' </td> 
                                                <td style="width:100px;text-align:right;border-bottom:solid 1px gray;"> </td>
                                                <td style="width:100px;text-align:right;border-bottom:solid 1px gray;"> </td>
                                            <tr>';
                                        $rencanaKegiatan = DB::table("trx_rencana")->where("no_prop",$dataDetail->no_prop)->get(); 
                                        $totalAnggaran=0;
                                        $totalAnggaranSKPD=0;
                                        $no=1;
                                        foreach ($rencanaKegiatan as $key => $value) {
                                           echo ' 
                                            <tr>
                                                <td  style="width:400px;border-bottom:solid 1px gray;">'.$no.'. '.$value->rencana.'</td> 
                                                <td style="width:100px;text-align:right;border-bottom:solid 1px gray;">'.number_format($value->rencana_anggaran,2).'</td>
                                                <td style="width:100px;text-align:right;border-bottom:solid 1px gray;">'.number_format($value->rencana_anggaran_skpd,2).'</td>
                                            <tr>';
                                            $totalAnggaran      = $totalAnggaran + $value->rencana_anggaran;
                                            $totalAnggaranSKPD  = $totalAnggaranSKPD + $value->rencana_anggaran_skpd;
                                            $no++;
                                        }
                                       
                                        echo '
                                            <tr>
                                                <td style="width:400px;text-align:right"><b>Total</b></td>
                                                <td style="width:100px;text-align:right"><b>Rp. '.number_format($totalAnggaran,2).'</b></td>
                                                <td style="width:100px;text-align:right"><b>Rp. '.number_format($totalAnggaranSKPD,2).'</b></td>
                                            </tr>
                                        ';

                                        echo '</table>
                                        </td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td colspan="7"> &nbsp; </td>
                                      </tr>
                                      '; 

                                    $i++;

                                }
                                

                              ?>
                            </table>

                     

                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="30%">
                                          {!! $qrcode; !!}
                                    </td> 
                                    <td width="70%">
                                         <table style=" margin-left: 70%;">  
                                            <tr>
                                                <td><center>
                                                <h4 style="text-transform: uppercase;">KEPALA {{ $skpd->nm_skpd }}</h4>
                                                
                                                <br><br>
                                                <h4>{{ $dataCetakkomendasiPencairan['nama_kepala'] }}<br>
                                                NIP {{ $dataCetakkomendasiPencairan['nip'] }}</h4>
                                                </center> 
                                            </td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            
                            </div>

                        </div>   -->

                </div>
            </div>

        </section><!-- #content end -->
@stop
@section('footer')
    @parent
    @include('layout-portal.footer')
@endsection

 