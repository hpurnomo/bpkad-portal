<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
	   table { page-break-inside:auto }
	   tr    { page-break-inside:avoid; page-break-after:auto }
	</style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;">
<center>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</center>
<center>
    <h4> PAKTA INTEGRITAS </h4>
</center>
<p style="padding: 30px 30px 0 50px;">
Sesuai Peraturan Menteri Dalam Negeri Nomor {{ $dataCPI['nomor1'] }} Tahun {{ $dataCPI['tahun1'] }} dan Peraturan Gubernur Nomor {{ $dataCPI['nomor2'] }} Tahun {{ $dataCPI['tahun2'] }}, dengan ini kami yang bertanda tangan di bawah ini :
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
	<tr>
		<td width="150">Nama</td>
		<td> : </td>
		<td style="padding: 3px;text-align: justify;">{{ $dataCPI['nama_pemohon'] }}</td>
	</tr>
	<tr>
		<td width="150">No. KTP/NIP/NRP *)</td>
		<td> : </td>
		<td style="padding: 3px;text-align: justify;">{{ $dataCPI['no_pemohon'] }}</td>
	</tr>
	<tr>
		<td width="150">Jabatan</td>
		<td> : </td>
		<td style="padding: 3px;text-align: justify;">{{ $dataCPI['jabatan_pemohon'] }}</td>
	</tr>
	<tr>
		<td width="150">Nama Lembaga **)</td>
		<td> : </td>
		<td style="padding: 3px;text-align: justify;">{{ $dataCPI['nama_lembaga'] }}
		</td>
	</tr>
	<tr>
		<td width="150">Alamat Lembaga</td>
		<td> : </td>
		<td style="padding: 3px;text-align: justify;">{{ $dataCPI['alamat_lembaga'] }}</td>
	</tr>
</table>

<p style="padding: 0 30px 0 50px;text-align: justify;">Sebagai penerima {{ $dataCPI['text1'] }} dalam bentuk uang dari Pemerintah Provinsi DKI Jakarta Tahun Anggaran {{ $dataCPI['tahun_anggaran1'] }} sebesar Rp.{{ $dataCPI['nominal'] }} ({{ $dataCPI['terbilang'] }} rupiah) yang akan digunakan untuk kegiatan sebagai berikut :</p>

<div style="padding: 0 30px 0 50px;text-align: justify;">
    @if($recProposalByID->noper >= 9 )
    {!!  base64_decode($dataCPI['kegiatan1']) !!}
    @else
    {!!  $dataCPI['kegiatan1'] !!}
    @endif
     
    <table border="1" cellpadding="1" cellspacing="1" width="100%"> 
        <tbody> 
            <tr>
                <td style="text-align: right;padding: 10px;font-style: italic;">   
                    <i>{{ $dataCPI['totalTerbilangKegiatan1'] }}</i>  
                </td> 
            </tr> 
        </tbody>
    </table> 
</div>

<p style="padding: 0 30px 0 50px;text-align: justify;">Dengan ini menyatakan bahwa : </p>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
    <tr>
        <td style="vertical-align: top;">1.</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">Akan melaporkan dan mempertanggungjawabkan penggunaan dana {{ $dataCPI['text2'] }} yang diterima dari Pemerintah Provinsi DKI Jakarta kepada Gubernur Provinsi DKI Jakarta melalui BPKD selaku PPKD dengan tembusan {{ $dataCPI['nm_skpd'] }} paling lambat tanggal 10 bulan Maret tahun anggaran berikutnya;</td>
    </tr>
    <tr>
        <td style="vertical-align: top;">2.</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">Akan melaksanakan kegiatan sesuai dengan proposal yang diusulkan {{ $dataCPI['text3'] }}, serta bertanggung jawab secara formal dan material atas penggunaan dana {{ $dataCPI['text4'] }} yang diterima.</td>
    </tr>
    <tr>
        <td style="vertical-align: top;">3.</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">Tidak akan mengalihkan anggaran belanja {{ $dataCPI['text5'] }} kepada pihak lain; dan</td>
    </tr>
    <tr>
        <td style="vertical-align: top;">4.</td>
        <td style="padding: 0 50px 0 10px;text-align: justify;">Bersedia dituntut sesuai dengan hukum yang berlaku di Republik Indonesia apabila di kemudian hari terdapat penyimpangan/penyalahgunaan dana {{ $dataCPI['text6'] }} yang diterima </td>
    </tr>
</table>
<p style="padding: 0 30px 0 50px;text-align: justify;">Demikian surat pernyataan tanggung jawab mutlak ini dibuat di atas meterai secukupnya untuk dapat dipergunakan sebagaimana mestinya.</p>
<table width="100%">
    <tr>
        <td width="30%">
        	<center>
                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
            </center>
        </td>
        <td width="30%"></td>
        <td>
        	<center>
        		<p>Jakarta,{{ $dataCPI['text7'] }} </p>
        		<p>{{ $dataCPI['jabatan_pemberi'] }}</p>
        		<br>
        		<p>(materai 6000)</p>
        		<br><br><br>
        		<p>{{ $dataCPI['nama_pemberi'] }}</p>
        		<p>NIP/NRP {{ $dataCPI['no_pemberi'] }} *)</p>
            </center>
        </td>
    </tr>
</table>
<p style="padding: 30px 30px 0 50px;text-align: justify;">
	<small>
		*) bagi penerima hibah instansi pemerintah <br>
		**) bagi penerima bantuan sosial selain individu/keluarga <br><br>
		Note : coret bagian yang tidak perlu
	</small>
</p>
</body>
</html>