<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <style type="text/css">
       table { page-break-inside:auto; }
       tr    { page-break-inside:avoid; page-break-after:auto; }
    </style>
</head>
<body>
<center>
    {{-- <br>
    <br> --}}
    <br>
    <br>
    <br>
    <br>
</center>
<center>
    <h4>&nbsp;</h4>
    <h4 style="text-transform: uppercase;">&nbsp;</h4>
</center>
<br><br>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                {{-- <tr>
                    <td>Judul</td>
                    <td>:</td>
                    <td>{{ $dataCrekomendasiPencairan['judul'] }}</td>
                </tr> --}}
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td>{{ $dataCrekomendasiPencairan['nomor'] }}</td>
                </tr>
                <tr>
                    <td>Sifat</td>
                    <td>:</td>
                    <td>{{ $dataCrekomendasiPencairan['sifat'] }}</td>
                </tr>
                <tr>
                    <td>Lampiran</td>
                    <td>:</td>
                    <td>{{ $dataCrekomendasiPencairan['lampiran'] }}</td>
                </tr>
                <tr>
                    <td style="vertical-align: top">Hal</td>
                    <td style="vertical-align: top;">:</td>
                    <td>Surat Rekomendasi Pencairan<br> 
                    {{ $dataCrekomendasiPencairan['text1'] }}
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <center>{{ $dataCrekomendasiPencairan['tanggal1'] }} {{ $dataCrekomendasiPencairan['bulan1'] }} {{ $dataCrekomendasiPencairan['tahun1'] }}
                        </center> 
                    </td>
                </tr>
                <tr>
                    <td style="padding: 15px;"><center>Kepada</center></td>
                </tr>
                <tr>
                    <td style="padding: 15px;">Yth. Kepala BPKD Provinsi DKI Jakarta selaku PPKD</td>
                </tr>
                <tr>
                    <td style="padding: 15px;">di <br>&nbsp;&nbsp;&nbsp;&nbsp;Jakarta</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<p style="padding: 30px 30px 0 30px;text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan permohonan  pencairan {{ $dataCrekomendasiPencairan['text2'] }} dari {{ ucwords($recProposalByID->nm_lembaga) }} yang diajukan sesuai surat Nomor {{ $dataCrekomendasiPencairan['nomor2'] }} tanggal {{ $dataCrekomendasiPencairan['tanggal2'] }} {{ $dataCrekomendasiPencairan['bulan2'] }} {{ $dataCrekomendasiPencairan['tahun2'] }} hal {{ $dataCrekomendasiPencairan['hal'] }} dan setelah dilakukan penelitian kesesuaian dan keabsahan dokumen terkait persyaratan, nama penerima, alamat penerima dan besaran dalam Rencana Anggaran Biaya Penggunaan {{ $dataCrekomendasiPencairan['text3'] }} sesuai daftar nama penerima, alamat penerima dan besaran {{ $dataCrekomendasiPencairan['text9'] }} dalam lampiran Keputusan Gubernur Nomor {{ $dataCrekomendasiPencairan['nomor_gubernur'] }} Tahun {{ $dataCrekomendasiPencairan['tahun3'] }} tentang {{ $dataCrekomendasiPencairan['tentang'] }}dengan rincian sebagai berikut :
<table width="100%">
    <tr>
        <td width="280" style="padding-left: 30px;">Nama Penerima</td>
        <td> : </td>
        <td>&nbsp;{{ucwords($recProposalByID->nm_lembaga)}}</td>
    </tr>
    <tr>
        <td width="280" style="padding-left: 30px;">Alamat</td>
        <td> : </td>
        <td>&nbsp;{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
    </tr>
    <tr>
        <td width="280" style="padding-left: 30px;">Besaran {{ $dataCrekomendasiPencairan['text4'] }}</td>
        <td> : </td>
        <td>&nbsp;Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</td>
    </tr>
    <tr>
        <td colspan="3" style="padding-left: 30px;">(dalam hal nama penerima lebih dari 1 (satu), maka menggunakan format sebagaimana terlampir)</td>
    </tr>
</table>
<br>
<p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa sesuai surat permohonan dari penerima {{ $dataCrekomendasiPencairan['text5'] }} direkomendasikan untuk diproses pencairan anggaran {{ $dataCrekomendasiPencairan['text6'] }} atas nama tersebut di atas dengan rincian : </p>
<table width="100%">
    <tr>
        <td width="280" style="padding-left: 30px;">Besaran {{ $dataCrekomendasiPencairan['text7'] }}</td>
        <td> : </td>
        <td>&nbsp;Rp. {{ number_format($recProposalByID->nominal_apbd,2,',','.') }}</td>
    </tr>
    <tr>
        <td width="280" style="padding-left: 30px;">Akumulasi Pencairan Sebelumnya</td>
        <td> : </td>
        <td>&nbsp;Rp. {{ number_format($akumulasiPencairan-$dataCrekomendasiPencairan['nominal_pencairan'],2,',','.') }} (**)</td>
    </tr>
    <tr>
        <td width="280" style="padding-left: 30px;">Pencairan saat ini</td>
        <td> : </td>
        <td style="border-bottom: 1px solid black;">&nbsp;Rp. {{ number_format($dataCrekomendasiPencairan['nominal_pencairan'],2,',','.') }} </td>
    </tr>
    <tr>
        <td width="280" style="padding-left: 30px;">Sisa {{ $dataCrekomendasiPencairan['text8'] }}</td>
        <td> : </td>
        <td>&nbsp;Rp. {{ number_format($sisaDana,2,',','.') }}</td>
    </tr>
    <tr>
        <td width="280" style="padding-left: 30px;">Terbilang</td>
        <td> : </td>
        <td>{{ $dataCrekomendasiPencairan['terbilang'] }}</td>
    </tr>
</table>

<p style="padding: 0 30px">Untuk ditransfer Ke Nomor Rekening : {{ $dataCrekomendasiPencairan['no_rekening'] }} a.n. {{ $dataCrekomendasiPencairan['atas_nama'] }}</p>

<p style="padding: 0 30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian surat rekomendasi ini dibuat dengan sebenar-benarnya dan saya bertanggung jawab penuh atas keabsahan rekomendasi tersebut.</p>

<table width="100%">
    <tr>
        <td width="30%">
            <center>
                {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
            </center>
        </td>
        <td width="30%"></td>
        <td><center>
            <h4>{{ $dataCrekomendasiPencairan['plt'] }} KEPALA <span style="text-transform: uppercase;">{{$recProposalByID->nm_skpd}}</span></h4>
            <br><br><br>
            <h4> {{ $dataCrekomendasiPencairan['nama_kepala'] }}</h4>
            <h4>NIP {{ $dataCrekomendasiPencairan['nip'] }}</h4>
            </center>
        </td>
    </tr>
</table>
</body>
</html>