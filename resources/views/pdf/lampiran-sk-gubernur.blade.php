<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <style type="text/css">
       table { page-break-inside:auto }
       tr    { page-break-inside:avoid; page-break-after:auto }
    </style>
</head>
<body>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding: 10px;" width="10"><center>No.</center></td>
        <td><center>SKPD/UKPD Pemberi Rekomendasi</center></td>
        <td><center>Kode Rekening</center></td>
        <td><center>Nama Rekening</center></td>
        <td><center>Nama Penerima</center></td>
        <td><center>Alamat</center></td>
        <td><center>Judul Proposal</center></td>
        <td><center>Penetapan</center></td>
    </tr>
    @foreach ($query as $key=>$value)
    <tr>
        <td style="padding: 10px;"><center>{{$key+1}}</center></td>
        <td style="padding: 10px;">{{ucwords($value->SKPDUKPDPemberiRekomendasi)}}</td>
        <td style="padding: 10px;">{{ucwords($value->KodeRekening)}}</td>
        <td style="padding: 10px;">{{ucwords($value->NamaRekening)}}</td>
        <td style="padding: 10px;">{{ucwords($value->NamaPenerima)}}</td>
        <td style="padding: 10px;">{{ucwords($value->Alamat)}}</td>
        <td style="padding: 10px;">{{ucwords($value->JudulProposal)}}</td>
        <td style="padding: 10px;">Rp. {{number_format($value->Penetapan,2,',','.')}}</td>
    </tr>
    @endforeach
</table>
</body>
</html>