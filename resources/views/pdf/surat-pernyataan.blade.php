<!DOCTYPE html>
<html>
<head>
	<title></title> 
    <style type="text/css">
       table { page-break-inside:auto; }
       tr    { page-break-inside:avoid; page-break-after:auto; }

       .pagebreak { page-break-before: always; }

    </style>
</head>
<body>  

<br><br><br><br><br> 

<center> 
    <h4 style="text-transform: uppercase;">&nbsp;(Surat pernyataan tanggung jawab pengusulan)<br>
        Kop Surat (Pengusul Hibah/Bantuan Sosial)<br>
        <br>
        Surat Pernyataan Tanggung Jawab
    </h4>
</center>
<p style="padding: 30px 30px 0 50px;">
Yang bertandatangan di bawah ini:</p>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 50px;">
    <tr>
        <td width="150">Nama</td>
        <td> : </td>
        <td style="padding: 3px;text-align: justify;"> {{ $user->kontak_person }} </td>
    </tr>
    <tr>
        <td width="150">No. KTP</td>
        <td> : </td>
        <td style="padding: 3px;text-align: justify;">{{ $user->nik_person }} </td>
    </tr>
    <tr>
        <td width="150">Alamat</td>
        <td> : </td>
        <td style="padding: 3px;text-align: justify;"> {{ $user->alamat_person }} </td>
    </tr>
    <tr>
        <td width="150">Jabatan</td>
        <td> : </td>
        <td style="padding: 3px;text-align: justify;"> Ketua
        </td>
    </tr>
    <tr>
        <td width="150">Nama Lembaga</td>
        <td> : </td>
        <td style="padding: 3px;text-align: justify;">{{  $user->nm_lembaga }} </td>
    </tr>
     <tr>
        <td width="150">Alamat Lembaga</td>
        <td> : </td>
        <td style="padding: 3px;text-align: justify;"> {{  $user->alamatLembaga }} </td>
    </tr>
</table>

<p style="padding: 30px 30px 0 50px;">
Saya selaku Ketua/Pimpinan/Kepala {{  $user->nm_lembaga }} dengan ini menyatakan bahwa:
</p>
 
<div style="padding: 30px 30px 0 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <ul style="list-style: decimal">
        <li>
             Bertanggung jawab sepenuhnya terhadap kebenaran data yang diajukan di dalam proposal bantuan untuk {{ $periode->keterangan }} dan apabila di kemudian hari ternyata ditemukan data yang tidak benar, maka saya siap bertanggung jawab dan menanggung segala konsekuensi hukum yang timbul.

        </li>
        <li>
            Akan menggunakan bantuan sesuai dengan proposal dan bertanggung jawab atas penggunaanya secara formal dan materil apabila mendapatkan bantuan dari Pemerintah Provinsi DKI Jakarta.
        </li>
    </ul>  
</div> 
     
    
    
<div style="padding: 30px 30px 0 150px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat pernyataan ini saya buat dengan sebenar benarnya tanpa ada unsur paksaan untuk dapat dipergunakan sebagaimana mestinya.</div>
 

<table style=" margin-left: 60%;">  
    <tr>
        <td><center>
            <br>
           <p style="font-size: 18px;">  Jakarta ........................... </p>
       
        <h4 style="text-transform: uppercase;">
            {{  $user->nm_lembaga }} <br> 
            Ketua
        
        
        <br><br> <br><br> 
        <br>  {{ $user->kontak_person }}</h4>
        </center> 
    </td>
</tr>
</table>

   

</body>
</html>