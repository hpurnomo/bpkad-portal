<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
   table { page-break-inside:auto }
   tr    { page-break-inside:avoid; page-break-after:auto }
</style>
<body>
	<center>
	    <h4 style="margin: 0;">CEK LIST KELENGKAPAN BERKAS PERMOHONAN</h4>
	    <h4 style="margin: 0;">PENCAIRAN BANTUAN {{ $recVerifikasiBerkasPencairanByNoProp['kelompok_belanja'] }}</h4>
	</center>
	<br> 
	<center>
	       <table width="80%">
	              <tr>
	                     <td width="211" style="padding-top: 10px;">Tahun Anggaran</td>
	                     <td width="13" style="padding-top: 10px;">:</td>
	                     <td style="padding-top: 10px;">{{ $recVerifikasiBerkasPencairanByNoProp['tahun_anggaran'] }}</td>
	              </tr>
	              <tr>
	                     <td style="padding-top: 10px;">No SK Gub Penetapan</td>
	                     <td style="padding-top: 10px;">:</td>
	                     <td style="padding-top: 10px;">{{ $recVerifikasiBerkasPencairanByNoProp['no_sk_gub_penetapan'] }}</td>
	              </tr>
	              <tr>
	                     <td style="padding-top: 10px;">Kelompok Belanja PPKD</td>
	                     <td style="padding-top: 10px;">:</td>
	                     <td style="padding-top: 10px;">{{ $recVerifikasiBerkasPencairanByNoProp['kelompok_belanja'] }}</td>
	              </tr>
	              <tr>
	                     <td style="padding-top: 10px;">No Urut Pemohon SK Gub</td>
	                     <td style="padding-top: 10px;">:</td>
	                     <td style="padding-top: 10px;">{{ $recVerifikasiBerkasPencairanByNoProp['no_urut_pemohon_sk'] }}</td>
	              </tr>
	              <tr>
	                     <td style="padding-top: 10px;">Nama Pemohon</td>
	                     <td style="padding-top: 10px;">:</td>
	                     <td style="padding-top: 10px;">{{ strtoupper($recVerifikasiBerkasPencairanByNoProp['nama_pemohon']) }}</td>
	              </tr>
	              <tr>
	                     <td style="padding-top: 10px;">SKPD/UKPD</td>
	                     <td style="padding-top: 10px;">:</td>
	                     <td style="padding-top: 10px;">{{ strtoupper($recVerifikasiBerkasPencairanByNoProp['nm_skpd']) }}</td>
	              </tr>
	       </table>
	</center>

	<center style="margin-top: 50px;">
    	<table width="100%" cellpadding="0" cellspacing="0" border="2">
    		<thead style="background: #F1F1F1;">
    			<tr>
    				<th rowspan="2"><center>NO</center></th>
    				<th rowspan="2"><center>KELENGKAPAN DOKUMEN YANG DIPERLUKAN</center></th>
    				<th colspan="2"><center>DOKUMEN</center></th>
    				<th rowspan="2"><center>KETERANGAN</center></th>
    			</tr>
    			<tr>
    				<th><center>Ada</center></th>
    				<th><center>Tidak</center></th>
    			</tr>
    		</thead>
    		<tbody>
    			<!-- <tr>
    				<td style="padding: 10px;"><center>1</center></td>
    				<td style="padding: 10px;">SURAT PENGANTAR PENCAIRAN DARI PEMBERI REKOMENDASI (ASLI)<br><b style="color: red;">(DITUJUKAN KE BPKD SELAKU PPKD)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_pengantar']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_pengantar']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_surat_pengantar'] }}
    				</td>
    			</tr> -->
    			<tr>
    				<td style="padding: 10px;"><center>1</center></td>
    				<td style="padding: 10px;">SURAT PERMOHONAN PENCAIRAN DARI LEMBAGA (ASLI)<br><b style="color: red;"> (DITUJUKAN KE GUBERNUR MELALUI SKPD/UKPD PEMBERI REKOMENDASI)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_permohonan']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_surat_permohonan']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_surat_permohonan'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>2</center></td>
    				<td style="padding: 10px;">RAB TERPISAH DARI PROPOSAL SESUAI NOMINAL DI SK  (copy)<br><b style="color: red;"> (DI TTD KETUA DAN BENDAHARA)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_rab']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_rab']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_rab'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>3</center></td>
    				<td style="padding: 10px;">KWITANSI<br><b style="color: red;"> (BERMETERAI  DI TTD KETUA DAN BENDAHARA)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_kwitansi']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_kwitansi']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_kwitansi'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>4</center></td>
    				<td style="padding: 10px;">FOTOCOPY REKENING KORAN BANK YANG MASIH AKTIF<br><b style="color: red;"> (ATAS NAMA LEMBAGA PENERIMA SESUAI SK GUB PENERIMA)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_rekening']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_rekening']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_rekening'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>5</center></td>
    				<td style="padding: 10px;">FOTOCOPY SK YANG ADA NAMA LEMBAGA PENERIMA<br><b style="color: red;"> (SK GUB PENETAPAN PENERIMA BANTUAN)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_sk_lembaga']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_sk_lembaga']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_sk_lembaga'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>6</center></td>
    				<td style="padding: 10px;">FOTOCOPY KTP PENGURUS<br><b style="color: red;">(MINIMAL FOTOCOPY KTP KETUA DAN BENDAHARA) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_ktp_pengurus']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_ktp_pengurus']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_ktp_pengurus'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>7</center></td>
    				<td style="padding: 10px;">NASKAH PERJANJIAN HIBAH DAERAH <br><b style="color: red;"> (BERMETERAI SERTA DI TTD KETUA LEMBAGA DAN KEPALA SKPD/UKPD PEMBERI REKOMENDASI) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_perjanjian_hibah_daerah']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_perjanjian_hibah_daerah']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_perjanjian_hibah_daerah'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>8</center></td>
    				<td style="padding: 10px;">PAKTA INTEGRITAS<br><b style="color: red;"> (DI TTD KETUA) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_pakta_integritas']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_pakta_integritas']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_pakta_integritas'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>9</center></td>
    				<td style="padding: 10px;">PROPOSAL DEFINITIF<br><b style="color: red;">(DI TTD KETUA)  </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_proposal_definitif']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_proposal_definitif']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_proposal_definitif'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>10</center></td>
    				<td style="padding: 10px;">LAPORAN PERTANGGUNGJAWABAN HIBAH/BANTUAN SOSIALYANG DITERIMA TAHUN SEBELUMNYA</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tgjwb_hibah_sebelumnya']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tgjwb_hibah_sebelumnya']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_tgjwb_hibah_sebelumnya'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>11</center></td>
    				<td style="padding: 10px;">DOKUMEN ADMINISTRASI LAINNYA SESUAI KETENTUAN PERATURAN PERUNDANG-UNDANGAN</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_dok_lain']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_dok_lain']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_dok_lain'] }}
    				</td>
    			</tr>
                <!-- 
    			<tr>
    				<td style="padding: 10px;"><center>13</center></td>
    				<td style="padding: 10px;">FOTOCOPY SURAT KETERANGAN DOMISILI<br><b style="color: red;"> (ALAMAT SESUAI DENGAN KOP SURAT/AKTA PENDIRIAN) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_skd']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_skd']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_skd'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>14</center></td>
    				<td style="padding: 10px;">FOTOCOPY NOMOR POKOK WAJIB PAJAK (NPWP)<br><b style="color: red;"> (ATAS NAMA LEMBAGA PENERIMA SESUAI SK GUB PENERIMA) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_npwp']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_npwp']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_npwp'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>15</center></td>
    				<td style="padding: 10px;">PROPOSAL DEFINITIF<br><b style="color: red;"> (DI TTD KETUA) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_proposal_definitif']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_proposal_definitif']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_proposal_definitif'] }}
    				</td>
    			</tr> -->


    			<!-- @if ( $recVerifikasiBerkasPencairanByNoProp['kelompok_belanja']=="BANTUAN HIBAH" )
    			<tr>
    				<td style="padding: 10px;"><center>16</center></td>
    				<td style="padding: 10px;">NASKAH PERJANJIAN HIBAH DAERAH (NPHD) (ASLI)<br><b style="color: red;"> (DUA RANGKAP, BERMATERAI 1 DI BENDAHARA UMUM, 1 DI KETUA LEMBAGA) </b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_nphd']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_nphd']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_nphd'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>17</center></td>
    				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA LAPORAN PERTANGGUNG JAWABAN <b style="color: red;">BANTUAN HIBAH</b> YANG DITERIMA SEBELUMNYA</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_laporan'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>18</center></td>
    				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA AUDIT <b style="color: red;">BANTUAN HIBAH</b> YANG DITERIMA SEBELUMNYA <b style="color: red;">(JIKA DANA YANG DITERIMA DIATAS 200 JUTA)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_audit'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>19</center></td>
    				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA LAPORAN MONEV <b style="color: red;">BANTUAN HIBAH</b> YANG DITERIMA SEBELUMNYA</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_monev']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_monev']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_monev'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>20</center></td>
    				<td style="padding: 10px;">BUKTI EMAIL VERIFIKASI INPUT KE SISTEM</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_bukti_email'] }}
    				</td>
    			</tr>
    			@else
    			<tr>
    				<td style="padding: 10px;"><center>16</center></td>
    				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA LAPORAN PERTANGGUNG JAWABAN <b style="color: red;">BANTUAN SOSIAL</b> YANG DITERIMA SEBELUMNYA</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_laporan']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_laporan'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>17</center></td>
    				<td style="padding: 10px;">FOTOCOPY TANDA TERIMA AUDIT <b style="color: red;">BANTUAN SOSIAL</b> YANG DITERIMA SEBELUMNYA <br><b style="color: red;">(JIKA DANA YANG DITERIMA DIATAS 200 JUTA)</b></td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_tanda_terima_audit']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_tanda_terima_audit'] }}
    				</td>
    			</tr>
    			<tr>
    				<td style="padding: 10px;"><center>18</center></td>
    				<td style="padding: 10px;">BUKTI EMAIL VERIFIKASI INPUT KE SISTEM</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==1)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					<center>{{ ($recVerifikasiBerkasPencairanByNoProp['cek_bukti_email']==0)?'&#10004;':'' }}</center>
    				</td>
    				<td style="padding: 10px;">
    					{{ $recVerifikasiBerkasPencairanByNoProp['ket_bukti_email'] }}
    				</td>
    			</tr>
    			@endif -->
    			
    		</tbody>
    	</table>
    </center>
	
	<br>
	<br>
	<table width="100%">
	    <tr>
	        <td width="30%">
	              <center>
	                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" />
	            </center>
	        </td>
	        <td width="30%"></td>
	        <td><center>
	            <h4>{{ strtoupper($recVerifikasiBerkasPencairanByNoProp['plt'])  }} {{ strtoupper($recVerifikasiBerkasPencairanByNoProp['nm_skpd']) }}</h4>
	            <br><br><br>
	            <h4 style="margin: 0;">{{ $recVerifikasiBerkasPencairanByNoProp['nama_ketua'] }}</h4>
	            <h4 style="margin: 0;">NIP {{ $recVerifikasiBerkasPencairanByNoProp['nip'] }}</h4>
	            </center>
	        </td>
	    </tr>
	</table>
</body>
</html>