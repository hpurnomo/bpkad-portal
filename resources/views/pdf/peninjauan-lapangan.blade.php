<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
	<style type="text/css">
	   table { page-break-inside:auto }
	   tr    { page-break-inside:avoid; page-break-after:auto }
	</style>
</head>
<body>
<center> 
    <br>
    <br>
    <h4 style="margin: 0 0 10px 0;">BERITA ACARA PENINJAUAN LAPANGAN</h4>
    <h4 style="text-transform: uppercase;margin: 0 0 10px 0;">{{$recProposalByID->nm_lembaga}}</h4>
	<h4 style="margin: 0 0 10px 0;">TAHUN ANGGARAN {{$recProposalByID->tahun_anggaran}}</h4>
	<h4 style="margin: 0 0 10px 0;">NOMOR {{$dataCpl['nomor']}}</h4>
</center>
<br><br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada hari ini <b>{{$dataCpl['hari']}}</b> tanggal <b>{{$dataCpl['tanggal1']}}</b> bulan <b>{{$dataCpl['bulan1']}}</b> tahun <b>{{$dataCpl['tahun1']}}</b> yang bertanda tangan di bawah ini :</p>
<br>
<table border="1" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td style="padding: 10px;"><center>No</center></td>
		<td style="padding: 10px;"><center>Nama</center></td>
		<td style="padding: 10px;"><center>Jabatan Dalam Tim</center></td>
		<td style="padding: 10px;"><center>Tanda Tangan</center></td>
	</tr>
	@foreach ($dataDcpl as $key=>$value)
	<tr>
		<td width="10"><center>{{ $value->no_urut }}</center></td>
		<td style="padding: 13px;">{{ $value->nama }}</td>
		<td style="padding: 13px;">{{ $value->jabatan }}</td>
		<td></td>
	</tr>
	@endforeach
</table>
<br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan Surat Tugas Kepala <b>{{$recProposalByID->nm_skpd}}</b> Nomor <b>{{$dataCpl['nomor2']}}</b>, tanggal <b>{{$dataCpl['tanggal2']}}</b> bulan <b>{{$dataCpl['bulan2']}}</b> tahun <b>{{$dataCpl['tahun2']}}</b> telah peninjauan lapangan terhadap:</p>
<br>
<table width="100%">
	<tr>
		<td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama(Lembaga)</td>
		<td>:</td>
		<td>{{ucwords($recProposalByID->nm_lembaga)}}</td>
	</tr>
	<tr>
		<td width="190">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alamat(Lembaga)</td>
		<td>:</td>
		{{-- <td>{{ucwords($recProposalByID->alamatLembaga)}} Kota {{ucwords($recProposalByID->nmkota)}},Kecamatan {{ucwords($recProposalByID->nmkec)}},Kelurahan {{ucwords($recProposalByID->nmkel)}},RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}</td> --}}
		<td>{{ucwords($recProposalByID->alamatLembaga)}} RT{{ucwords($recProposalByID->namaRT)}}/RW{{ucwords($recProposalByID->namaRW)}}, Kelurahan {{ucwords($recProposalByID->nmkel)}},Kecamatan {{ucwords($recProposalByID->nmkec)}}, {{ucwords($recProposalByID->nmkota)}}</td>
	</tr>
</table>
<br>
<p>Dengan hasil sebagai berikut :</p>
<table  border="1" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="padding: 10px;"><center>No</center></td>
                <td style="padding: 10px;"><center>Data Administrasi</center></td>
                <td style="padding: 10px;"><center>Ada</center></td>
                <td style="padding: 10px;"><center>Tidak Ada</center></td>
                <td style="padding: 10px;"><center>Keterangan</center></td>
            </tr> 
            <tr>
                <td><b><center>A</center></b></td>
                <td colspan="4" style="padding-left: 10px;"><b> Kelengkapan proposal usulan hibah/bansos:</b></td> 
            </tr>

            <tr>
                <td><center>1</center></td>
                <td style="padding-left: 10px;">Identitas dan alamat pengusul</td>
                <td><center>@if($prop_adminisitrasi['cek_identitas_alamat']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_identitas_alamat']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_identitas_alamat'] !!}</td>
            </tr>

             <tr>
                <td><center>2</center></td>
                <td style="padding-left: 10px;">Latar Belakang</td>
                <td><center>@if($prop_adminisitrasi['cek_latar_belakang']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_latar_belakang']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_latar_belakang'] !!}</td>
            </tr>

            <tr>
                <td><center>3</center></td>
                <td style="padding-left: 10px;">Maksud dan Tujuan</td>
                <td><center>@if($prop_adminisitrasi['cek_maksud_tujuan']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_maksud_tujuan']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_maksud_tujuan'] !!}</td>
            </tr>

            <tr>
                <td><center>4</center></td>
                <td style="padding-left: 10px;">Rincian rencana kegiatan (jadwal pelaksanaan kegiatan)</td>
                <td><center>@if($prop_adminisitrasi['cek_jadwal_pelaksanaan']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_jadwal_pelaksanaan']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_jadwal_pelaksanaan'] !!}</td>
            </tr>

             <tr>
                <td><center>5</center></td>
                <td style="padding-left: 10px;">Rincian rencana penggunaan hibah/bansos  (rincian anggaran biaya)</td>
                <td><center>@if($prop_adminisitrasi['cek_rencana_rab']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($prop_adminisitrasi['cek_rencana_rab']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $prop_adminisitrasi['ket_rencana_rab'] !!}</td>
            </tr>
              <tr>
                <td><b><center>B</center></b></td>
                <td colspan="4" style="padding-left: 10px;"><b> Dokumen Administrasi:</b></td> 
            </tr>  

            <tr>
                <td><center>1</center></td>
                <td style="padding-left: 10px;"> Fotokopi Kartu Tanda penduduk (KTP) Ketua/ pimpinan badan, lembaga atau organisasi kemasyarakatan *)</td>
                <td><center>@if($dataKal['cek_ktp_ketua']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($dataKal['cek_ktp_ketua']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_ktp_ketua'] !!}</td>
            </tr>

             <tr>
                <td><center>2</center></td>
                <td style="padding-left: 10px;"> Fotokopi Akta Notaris Pendirian badan hukum yang telah mendapatkan pengesahan dari Kementrian yang membidangi hukum atau keputusan Gubernur tentang </td>
                <td><center>@if($dataKal['cek_akta']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($dataKal['cek_akta']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_akta'] !!}</td>
            </tr>

             <tr>
                <td><center>3</center></td>
                <td style="padding-left: 10px;">Fotokopi Nomor Pokok Wajib Pajak (NPWP) *) </td>
                <td><center>@if($dataKal['cek_npwp']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($dataKal['cek_npwp']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_npwp'] !!}</td>
            </tr>

             <tr>
                <td><center>4</center></td>
                <td style="padding-left: 10px;">    Fotokopi Surat keterangan domisili organisasi kemasyarakatan dari kelurahan setempat atau sebutan lainnya *)</td>
                <td><center>@if($dataKal['cek_skd']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($dataKal['cek_skd']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_skd'] !!}</td>
            </tr>

            <tr>
                <td><center>5</center></td>
                <td style="padding-left: 10px;">Fotokopi izin operasional /tanda daftar lembaga dari instasi yang berwenang *)</td>
                <td><center>@if($dataKal['cek_izin_operasional']==1)<input type="checkbox" checked=""></i>@endif</center></td>
                <td><center>@if($dataKal['cek_izin_operasional']==2)<input type="checkbox" checked=""></i>@endif</center></td>
                <td style="padding-left: 10px;">{!! $dataKal['keterangan_cek_izin_operasional'] !!}</td>
            </tr>
        </table>
<br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian laporan ini dibuat dengan sebenarnya dan agar dapat dipergunakan sebagaimana mestinya.</p>
<p>Catatan:<br>*disesuikan dengan status/kedudukan kelembagaan Pemohon.</p>
<table width="100%">
	<tr>
		<td width="30%">
			<center>
			<h4>PENGUSUL HIBAH</h4>
        	<br><br><br>
        	<h4>{{ucwords($dataCpl['nama_pengusul'])}}</h4>
        	<h4>&nbsp;</h4>
        	</center>
		</td>
		<td width="30%">
			<center>
                {{-- <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(url('/').$_SERVER['REQUEST_URI'], 'QRCODE')}}" alt="barcode" /> --}}
                <img src="data:image/png;base64,{{ DNS2D::getBarcodePNG(route('detail.lembaga',['tahun'=>$recProposalByID->tahun_anggaran,'idlembaga'=>$recProposalByID->no_lembaga]), 'QRCODE')}}" alt="barcode" />
            </center>
		</td>
		<td><center>
			<h4>KETUA TIM EVALUASI</h4>
        	<br><br><br>
        	<h4>{{ucwords($dataCpl['nama_ketua'])}}</h4>
        	<h4>NIP {{ucwords($dataCpl['nip'])}}</h4>
        	</center>
		</td>
	</tr>
</table>
</body>
</html>