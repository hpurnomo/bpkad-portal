<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
	<style type="text/css">
	   table { page-break-inside:auto }
	   tr    { page-break-inside:avoid; page-break-after:auto }
	</style>
</head>
<body>
	{{-- {{ dd($dataKelengkapanAdministrasi) }} --}}
	<center>
	    <h4>Rekapitulasi Checklist Lembaga</h4>
	</center>
	<p>
		Keterangan :
		<ol>
			<li>C1 : Nama dan Identitas	</li>
			<li>C2 : Alamat	</li>
			<li>C3 : Saldo akhir tahun lalu beserta rekening Bank </li>
			<li>C4 : Bantuan yang pernah diterima 1 (satu) tahun sebelumnya apabila ada	</li>
			<li>C5 : Nomor Pokok Wajib Pajak </li>
			<li>C6 : Akte Notaris Pendirian Badan Hukum dan telah mendapatkan pengesahan dari Kementerian Hukum dan HAM dan/atau Pembentukan Organisasi/Lembaga </li>
			<li>C7 : Surat Keterangan Domisili dari Kelurahan setempat </li>
			<li>C8 : Sertifikat tanah/bukti kepemilikan tanah atau perjanjian kontrak dan sewa gedung/bangunan </li>
			<li>C9 : Izin operasional/tanda daftar bagi lembaga/yayasan dari instansi yang berwenang </li>
		</ol>
	</p>
	<table width="100%" border="1">
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Lembaga</th>
				<th colspan="9">Data Administrasi</th>
				<th colspan="9">Data Lapangan</th>
			</tr>
			<tr>
				<th>C1</th>
				<th>C2</th>
				<th>C3</th>
				<th>C4</th>
				<th>C5</th>
				<th>C6</th>
				<th>C7</th>
				<th>C8</th>
				<th>C9</th>
				<th>C1</th>
				<th>C2</th>
				<th>C3</th>
				<th>C4</th>
				<th>C5</th>
				<th>C6</th>
				<th>C7</th>
				<th>C8</th>
				<th>C9</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($dataKelengkapanAdministrasi as $key=>$value)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $value->nm_lembaga }}</td>
					<td><center>{!! ($value->cek_ktp_ketua==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_alamat_lembaga==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_rekening_lembaga==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_bantuan_tahun_sebelum==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_npwp==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_akta==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_skd==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_sertifikat==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_izin_operasional==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>

					<td><center>{!! ($value->cek_ktp_ketua_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_alamat_lembaga_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_rekening_lembaga_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_bantuan_tahun_sebelum_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_npwp_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_akta_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_skd_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_sertifikat_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
					<td><center>{!! ($value->cek_izin_operasional_lapangan==1)?'<b style="color:green;">&#x2713;</b>':'<b style="color:red;">X</b>' !!}</center></td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>