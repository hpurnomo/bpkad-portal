<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <style type="text/css">
       table { page-break-inside:auto }
       tr    { page-break-inside:avoid; page-break-after:auto }
    </style>
</head>
<body>
<center>
    <h4>DAFTAR NAMA PENERIMA, ALAMAT DAN BESARAN ALOKASI HIBAH DITERIMA</h4>
</center>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding: 10px;" width="50"><center>No.</center></td>
        <td width="200"><center>Nama Penerima</center></td>
        <td width="200"><center>Alamat Penerima</center></td>
        <td><center>Jumlah</center></td>
    </tr>
    @foreach ($recProposalBySkpdID as $key=>$value)
    <tr>
        <td style="padding: 10px;"><center>{{$key+1}}</center></td>
        <td style="padding: 10px;">{{ucwords($value->nm_lembaga)}}</td>
        <td style="padding: 10px;">{{ucwords($value->alamatLembaga)}}</td>
        <td style="padding: 10px;">Rp. {{number_format($value->nominal_rekomendasi,2,',','.')}}</td>
    </tr>
    @endforeach
</table>
</body>
</html>