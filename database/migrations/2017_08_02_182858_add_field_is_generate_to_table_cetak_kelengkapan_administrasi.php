<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIsGenerateToTableCetakKelengkapanAdministrasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cetak_kelengkapan_administrasi', function (Blueprint $table) {
            $table->integer('is_generate')->default(0)->after('nip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cetak_kelengkapan_administrasi', function (Blueprint $table) {
            //
        });
    }
}
