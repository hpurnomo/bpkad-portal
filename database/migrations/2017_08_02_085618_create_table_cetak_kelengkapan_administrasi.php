<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCetakKelengkapanAdministrasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cetak_kelengkapan_administrasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_prop');
            $table->string('nomor')->nullable();
            $table->string('hari')->nullable();
            $table->string('tanggal1')->nullable();
            $table->string('bulan1')->nullable();
            $table->string('tahun1')->nullable();
            $table->string('nama1')->nullable();
            $table->string('jabatan1')->nullable();
            $table->string('nama2')->nullable();
            $table->string('jabatan2')->nullable();
            $table->string('nama3')->nullable();
            $table->string('jabatan3')->nullable();
            $table->string('nama4')->nullable();
            $table->string('jabatan4')->nullable();
            $table->string('nama5')->nullable();
            $table->string('jabatan5')->nullable();
            $table->string('tanggal2')->nullable();
            $table->string('bulan2')->nullable();
            $table->string('tahun2')->nullable();
            $table->string('nama_ketua')->nullable();
            $table->string('nip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cetak_kelengkapan_administrasi');
    }
}
