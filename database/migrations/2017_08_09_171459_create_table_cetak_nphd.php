<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCetakNphd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cetak_nphd', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_prop');

            $table->string('hari1')->nullable();
            $table->string('tanggal1')->nullable();
            $table->string('bulan1')->nullable();
            $table->string('tahun1')->nullable();
            
            $table->string('nama1')->nullable();
            $table->string('jabatan1')->nullable();
            $table->string('alamat')->nullable();

            $table->string('pasal')->nullable();
            $table->string('nomor1')->nullable();
            $table->string('hadapan')->nullable();
            $table->string('notaris')->nullable();

            $table->string('nomor2')->nullable();
            $table->string('tahun2')->nullable();
            $table->string('tanggal2')->nullable();

            $table->string('nomor3')->nullable();
            $table->string('tahun3')->nullable();

            $table->string('nomor4')->nullable();
            $table->string('tahun4')->nullable();
            $table->string('tahun_anggaran1')->nullable();

            $table->string('nomor5')->nullable();
            $table->string('tahun5')->nullable();

            $table->string('nomor6')->nullable();
            $table->string('tahun6')->nullable();
            $table->string('tahun_anggaran2')->nullable();

            $table->string('nomor7')->nullable();
            $table->string('tahun7')->nullable();

            $table->string('senilai_angka')->nullable();
            $table->string('senilai_teks')->nullable();
            
            $table->string('hari2')->nullable();
            $table->string('tanggal3')->nullable();

            $table->string('kegiatan1')->nullable();
            $table->string('kegiatan2')->nullable();
            $table->string('kegiatan3')->nullable();
            
            $table->string('jabatan2')->nullable();
            $table->string('nama_pihak_kedua')->nullable();
            $table->string('nip_pihak_kedua')->nullable();
            
            $table->string('nama_kepala')->nullable();
            $table->string('nip_kepala')->nullable();
            $table->integer('is_generate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cetak_nphd');
    }
}
