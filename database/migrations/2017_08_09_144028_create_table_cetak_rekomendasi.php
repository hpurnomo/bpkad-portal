<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCetakRekomendasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cetak_rekomendasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_prop');
            $table->string('nomor')->nullable();
            $table->string('sifat')->nullable();
            $table->string('lampiran')->nullable();
            $table->string('tanggal1')->nullable();
            $table->string('bulan1')->nullable();
            $table->string('tahun1')->nullable();
            $table->string('nomor2')->nullable();
            $table->string('tanggal2')->nullable();
            $table->string('bulan2')->nullable();
            $table->string('tahun2')->nullable();
            $table->string('hal')->nullable();
            $table->string('nama_kepala')->nullable();
            $table->string('nip')->nullable();
            $table->integer('is_generate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cetak_rekomendasi');
    }
}
