<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldNomor2ToTableCetakKelengkapanAdministrasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cetak_kelengkapan_administrasi', function (Blueprint $table) {
            $table->string('nomor2')->after('jabatan5')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cetak_kelengkapan_administrasi', function (Blueprint $table) {
            //
        });
    }
}
