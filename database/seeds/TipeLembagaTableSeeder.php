<?php

use Illuminate\Database\Seeder;

class TipeLembagaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mst_tipe_lembaga')->insert(['nama' => 'keagamaan',
                                                    'nama' => 'non keagamaan']);
    }
}
