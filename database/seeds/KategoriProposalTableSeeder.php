<?php

use Illuminate\Database\Seeder;

class KategoriProposalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mst_kategori_proposal')->insert(['nama' => 'hibah',
        											'nama' => 'bansos']);
    }
}
