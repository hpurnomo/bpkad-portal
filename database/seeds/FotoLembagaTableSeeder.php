<?php

use Illuminate\Database\Seeder;

class FotoLembagaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foto_lembaga')->insert([
                                        'no_lembaga' => '179',
                                        'file_name' => 'yayasan_layak.jpg',
                                        'no_lembaga' => '1',
                                        'file_name' => 'Asosiasi_DPRD_Provinsi_se_Indonesia_dan_Forum_Komunikasi_Sekretaris_DPRD_Provinsi_se_Indonesia.jpg',
                                        'no_lembaga' => '207',
                                        'file_name' => 'Badan_Amil_Zakat_Infaq_dan_Shadaqah_Jakarta.jpg',
                                        'no_lembaga' => '2',
                                        'file_name' => 'DRD_Provinsi_DKI_Jakarta.jpg',
                                        'no_lembaga' => '211',
                                        'file_name' => 'PTIQ_Jakarta.jpg',
                                        'no_lembaga' => '208',
                                        'file_name' => 'LPTQ_Jakarta.jpg',
                                        'no_lembaga' => '1382',
                                        'file_name' => 'Masjid_Baiturrahmah.jpg',
                                        'no_lembaga' => '216',
                                        'file_name' => 'NU_Jakarta.jpg',
                                        'no_lembaga' => '221',
                                        'file_name' => 'Pengurus_Wilayah_Nahdlatul_Ulama_Provinsi_DKI_Jakarta.jpg'
                                    ]);
    }
}
