<?php

namespace BPKAD\Services;

use BPKAD\Http\Controllers\Controller;
use DB;
use BPKAD\Http\Entities\Trx_rincian_penebalan;
use Auth;

class RincianPenebalanService extends Controller
{
    public function outputByRencanaID($id,$fase,$noper,$uuid,$tahap,$status=null){
    	$result = DB::table('trx_rincian_penebalan as a')->select('a.*')
    											->where('a.rencanaID',$id)
    											->get();

        $output = "";
        foreach ($result as $key => $value) {
            $output .= "
            <tr>
                <td></td>
                <td><a href=\"".route('member.proposal.penebalan.edit.rincian',['id'=>$value->id,'fase'=>$fase,'noper'=>$noper,'uuid'=>$uuid,'tahap'=>$tahap,'status'=>$status])."\" style=\"padding-left:20px;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td>
                <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                <td>".number_format($value->harga_satuan,2,",",".")."</td>
                <td>".number_format($value->rincian_anggaran,2,",",".")."</td>";
                
                if(Auth::user()->group_id == 12){ 
                    $output .="
                    <td><input type=\"text\" class=\"mask_currency\" name=\"rincian_anggaran_digunakan,".$value->id."\" value=\"".$value->rincian_anggaran_digunakan."\" onkeypress=\"isInputNumber(event)\" style=\"width: 110px;\"></td>";
                }else{
                    $output .="
                    <td>".number_format($value->rincian_anggaran_digunakan,2,",",".")."</td>";
                } 
                
                 $output .="
                <td>".number_format($value->rincian_anggaran-$value->rincian_anggaran_digunakan,2,",",".")."</td>
            </tr>
            "; 
        }

    	return $output;
    }

    public function outputByRencanaIDRekomendasi($id,$fase,$noper,$uuid,$tahap,$status=null){
        $result = DB::table('trx_rincian_penebalan as a')->select('a.*')
                                                ->where('a.rencanaID',$id)
                                                ->get();

        $output = "";
        foreach ($result as $key => $value) {
            $output .= "
            <tr>
                <td></td>
                <td><a href=\"".route('member.proposal.penebalan.edit.rincian',['id'=>$value->id,'fase'=>$fase,'noper'=>$noper,'uuid'=>$uuid,'tahap'=>$tahap,'status'=>$status])."\" style=\"padding-left:20px;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td>
                <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                <td>".number_format($value->harga_satuan,2,",",".")."</td>
                <td>".number_format($value->rincian_anggaran,2,",",".")."</td>";
                
                if(Auth::user()->group_id == 10){ 
                    $output .="
                    <td><input type=\"text\" class=\"mask_currency\" name=\"anggaran_rekomendasi,".$value->id."\" value=\"".$value->anggaran_rekomendasi."\" onkeypress=\"isInputNumber(event)\" style=\"width: 110px;\"></td>";
                }else{
                    $output .="
                    <td>".number_format($value->anggaran_rekomendasi,2,",",".")."</td>";
                } 
                
                 $output .=" 
            </tr>
            "; 
        }

        return $output; 
    }

     public function outputByRencanaIDEdit($id,$fase,$noper,$uuid,$tahap,$status=null){
        $result = DB::table('trx_rincian_penebalan as a')->select('a.*')
                                                ->where('a.rencanaID',$id)
                                                ->get();

        $output = "";
        foreach ($result as $key => $value) {
            $output .= "
            <tr>
                <td></td>
                <td><a href=\"".route('member.proposal.penebalan.edit.rincian',['id'=>$value->id,'fase'=>$fase,'noper'=>$noper,'uuid'=>$uuid,'tahap'=>$tahap,'status'=>$status])."\" style=\"padding-left:20px;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td>
                <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                <!-- <td>".number_format($value->harga_satuan,2,",",".")."</td> -->
                <td>".number_format($value->anggaran_pengajuan,2,",",".")."</td> 
                <td>".number_format($value->anggaran_rekomendasi,2,",",".")."</td>  
                <td>".number_format($value->rincian_anggaran,2,",",".")."</td> 
                 
            </tr>
            "; 
        }

        return $output;
    }


    public function outputByRencanaIDWithoutEdit($id,$fase,$noper,$uuid,$tahap,$status=null){
        $result = DB::table('trx_rincian_penebalan as a')->select('a.*')
                                                ->where('a.rencanaID',$id)
                                                ->get();

        $output = "";
        foreach ($result as $key => $value) {
            $output .= "
            <tr>
                <td></td>
                <td><a href=\"javascript:;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td>
                <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                <td>".number_format($value->harga_satuan,2,",",".")."</td>
                <td>".number_format($value->rincian_anggaran,2,",",".")."</td>";
                
                if(Auth::user()->group_id == 12){ 
                    $output .="
                    <td><input type=\"text\" class=\"mask_currency\" name=\"rincian_anggaran_digunakan,".$value->id."\" value=\"".$value->rincian_anggaran_digunakan."\" onkeypress=\"isInputNumber(event)\" style=\"width: 110px;\"></td>";
                }else{
                    $output .="
                    <td>".number_format($value->rincian_anggaran_digunakan,2,",",".")."</td>";
                } 

                $output .="
                <td>".number_format($value->rincian_anggaran-$value->rincian_anggaran_digunakan,2,",",".")."</td>
            </tr>
            "; 
        }

        return $output;
    }

    public function outputByRencanaIDForNPHD($id){
        $result = DB::table('trx_rincian_penebalan as a')->select('a.*')
                                                ->where('a.rencanaID',$id)
                                                ->where('a.rincian_anggaran','>',0)
                                                ->get();

        $output = "";
        foreach ($result as $key => $value) {
            $output .= "
            <tr>
                <td></td>
                <td style=\"padding:15px;\">".$value->rincian."</td>
                <td style=\"text-align:right;padding: 10px;\">".number_format($value->rincian_anggaran,2,",",".")."</td>
            </tr>
            "; 
        }

        return $output;
    }

    public function convert($number)
    {
        $number = str_replace('.', '', $number);
        if ( ! is_numeric($number)) throw new Exception("Please input number.");
        $base    = array('nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $numeric = array('1000000000000000', '1000000000000', '1000000000000', 1000000000, 1000000, 1000, 100, 10, 1);
        $unit    = array('kuadriliun', 'triliun', 'biliun', 'milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
        $str     = null;
        $i = 0;
        if ($number == 0) {
            $str = 'nol';
        } else {
            while ($number != 0) {
                $count = (int)($number / $numeric[$i]);
                if ($count >= 10) {
                    $str .= static::convert($count) . ' ' . $unit[$i] . ' ';
                } elseif ($count > 0 && $count < 10) {
                    $str .= $base[$count] . ' ' . $unit[$i] . ' ';
                }
                $number -= $numeric[$i] * $count;
                $i++;
            }
            $str = preg_replace('/satu puluh (\w+)/i', '\1 belas', $str);
            $str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
            $str = preg_replace('/\s{2,}/', ' ', trim($str));
        }
        return $str;
    }
}
