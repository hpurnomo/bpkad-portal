<?php

namespace BPKAD\Services;

use BPKAD\Http\Controllers\Controller;
use DB;
use BPKAD\Http\Entities\Lembaga;

class LembagaService extends Controller
{
    public function getCountByKdSKPD($kdskpd){
        $count = Lembaga::where('kd_skpd',$kdskpd)->count();

        return $count;
    }

    public function getLembagaByNomor($nomor){
    	$result = DB::table('mst_lembaga as a')->select('a.*','b.nama as RT','c.nama as RW','d.nmkel','e.nmkec','f.nmkota')
    											->leftJoin('mst_rt as b','a.rtID','=','b.id')
    											->leftJoin('mst_rw as c','a.rwID','=','c.id')
    											->leftJoin('mst_kelurahan as d','a.nokel','=','d.nokel')
    											->leftJoin('mst_kecamatan as e','a.nokec','=','e.nokec')
    											->leftJoin('mst_kota as f','a.nokota','=','f.nokota')
    											->where('a.nomor',$nomor)
    											->first();

    	return $result;
    }

}
