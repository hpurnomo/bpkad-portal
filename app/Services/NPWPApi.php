<?php 
namespace BPKAD\Services;

class NPWPApi extends ApiHttpBasic
{

  protected $url;
  protected $key;

  public function __construct($url='',$key='npwp')
  {
    $this->url = $url;
    $this->key = $key; 
    parent::__construct('http://soadki.jakarta.go.id/rest/gov/dki/djp/api/expose/ws/npwp',"updiprod","updiprod");
  }

  public function getData($nik)
  {
    $params = 'npwp';
    $args = ['npwp'=>$nik];
    return parent::connect('',$args);
  }
 

}
