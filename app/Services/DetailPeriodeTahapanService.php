<?php

namespace BPKAD\Services;

use BPKAD\Http\Controllers\Controller;
use DB;

class DetailPeriodeTahapanService extends Controller
{
    public function getByPeriodeID($periodeID){
    	$strQuery = DB::table('detail_periode_tahapan AS a')->select('a.*','b.id AS tahapID','b.name')
    														->join('mst_tahapan AS b','a.tahapanID','=','b.id')
    														->where('a.periodeID',$periodeID)->get();

    	$output = "";
    	foreach ($strQuery as $key => $value) {
    		$output .= "<tr>
    						<td></td>
    						<td></td>
    						<td>Tahap ".$value->tahapID."</td>
    						<td><a href=\"".route('ppkd.periode.tahapan.edit',['id'=>$value->id])."\">".$value->name."</a></td>
    						<td>".$this->status($value->status)."</td>
    						<td>".date('d M-Y',strtotime($value->mulai))."</td>
    						<td>".date('d M-Y',strtotime($value->selesai))."</td>
    						<td>".$value->keterangan."</td>
    					</tr>";
    	}

    	return $output;
    }

    private function status($status){
    	if($status==1){
    		return 'Aktif';
    	}
    	else{
    		return 'Histori';
    	}
    }

}
