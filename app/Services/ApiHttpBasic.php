<?php 

namespace BPKAD\Services;

class ApiHttpBasic
{
	protected $uri;

	protected $username;

	protected $password;

  public function __construct($uri, $username, $password)
  {
    $this->uri = $uri;
    $this->username = $username;
    $this->password = $password;
  }

  public function connect($param='',$arg='', $method='GET')
  {
    $uri_param = is_array($param) ? implode('/',$param):'/'.$param;
    $uri_arg = is_array($arg) ? http_build_query($arg):$arg;
    $api_uri = $param ? $this->uri.$uri_param.'?'.$uri_arg : $this->uri.'?'.$uri_arg;

    // \Log::debug($api_uri);

    // CURL call
    $ch = curl_init($api_uri);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_USERPWD, $this->username.":".$this->password);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //Tell cURL that it should only spend 10 seconds
    //trying to connect to the URL in question.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, env('CURL_CONNECT_TIMEOUT',10));
    //A given cURL operation should only take
    //30 seconds max.
    curl_setopt($ch, CURLOPT_TIMEOUT, env('CURL_TIMEOUT',30));
    // curl_setopt($ch, CURLOPT_HTTPHEADER, [
    //    'Content-Type: application/json'
    // ]);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
    //   'identification' => $username,
    //   'password'       => $password
    // ]));
    // Output dengan header=true hanya untuk meta document xml/json
    // curl_setopt($ch, CURLOPT_HEADER, true);         
    // Mendapatkan tanggapan
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);  
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, FALSE);
    curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
    // Menggunakan metode HTTP POST 
    if($method=='POST'){
      curl_setopt($ch, CURLOPT_POST, TRUE);       
      // Sisipkan parameter    
      curl_setopt($ch, CURLOPT_POSTFIELDS,$uri_param);  
    }
    $result = curl_exec($ch);
    $response = json_decode($result);
    $errno = curl_errno($ch);
    $errmsg = curl_error($ch); 

    if ($errno!==0){                            
      $response=array('status'=>'error','err_code'=>$errno,'message'=>$errmsg,'data'=>$response);
      return $response;
    }

    curl_close($ch);

    $response=array('status'=>'success','err_code'=>$errno,'message'=>'Success','data'=>$response);
    return $response;
  }
}
