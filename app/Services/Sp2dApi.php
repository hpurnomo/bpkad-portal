<?php 
namespace BPKAD\Services;

class Sp2dApi extends ApiHttpBasic
{

  protected $url;
  protected $key;
  protected $key2;

  public function __construct($url='',$key='tahun',$key2='no_kegiatan')
  {
    $this->url  = $url;
    $this->key  = $key; 
    $this->key2 = $key2; 
    parent::__construct('http://soadki.jakarta.go.id/rest/gov/dki/sipkd/hibah/ws/hibah',"updiprod","updiprod");
  }

  public function getData($tahun,$kegiatan)
  {
    $params = '';
    $args = ['tahun'=>$tahun,'kegiatan'=>$kegiatan];
    return parent::connect('',$args);
  }
 
}
