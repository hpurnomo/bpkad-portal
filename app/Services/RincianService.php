<?php

namespace BPKAD\Services;

use BPKAD\Http\Controllers\Controller;
use DB;
use BPKAD\Http\Entities\Trx_rincian; 
use Auth;

class RincianService extends Controller
{
    public function outputByRencanaID($id,$no_prop,$fase){
    	$result = DB::table('trx_rincian as a')->select('a.*')
    											->where('a.rencanaID',$id)
    											->get(); 
                // <td><a href=\"".route('member.proposal.edit.rincian',['id'=>$value->id,'no_prop'=>$no_prop,'fase'=>$fase])."\" style=\"padding-left:20px;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td> 

        $output = ""; 
        $check = DB::table("trx_proposal")->where("no_prop",$no_prop)->first();
 
        if($check->noper >= 10 ){

              foreach ($result as $key => $value) {
                    $output .= "
                    <tr>
                        <td></td>";
                        if(Auth::user()->group_id==12){
                            
                            $output .="
                                <td><a href=\"".route('member.proposal.edit.rincian',['id'=>$value->id,'no_prop'=>$no_prop,'fase'=>$fase])."\" style=\"padding-left:20px;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td>"; 

                        }else{ 
                            $output .="<td>".$value->rincian."</td>"; 
                        }
                        
                        $output .="
                        <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                        <td>".number_format($value->harga_satuan,2,",",".")."</td>
                        <td>".number_format($value->rincian_anggaran,2,",",".")."</td>
                        <td>".number_format($value->rincian_anggaran_skpd,2,",",".")."</td>";

 
                    if(Auth::user()->group_id==10){
                    $output .="<td>  
                           <input type=\"text\"  class=\"mask_currency\" name=\"rincian_anggaran_skpd,".$value->id."\" value=\"".round($value->rincian_anggaran_skpd,0)."\" onkeypress=\"isInputNumber(event)\"> 
                        </td> ";
                    }

                    $output .="    
                    </tr>
                    ";  
                }
        }else{
                foreach ($result as $key => $value) {
                $output .= "
                <tr>
                    <td></td>
                    <td>".$value->rincian."</td>
                    <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                    <td>".number_format($value->harga_satuan,2,",",".")."</td>
                    <td>".number_format($value->rincian_anggaran,2,",",".")."</td>  
                </tr>
                ";  
            }
        }
      

    	return $output;
    }

    public function outputByRencanaIDWithoutEdit($id,$no_prop,$fase){
        $result = DB::table('trx_rincian as a')->select('a.*')
                                                ->where('a.rencanaID',$id)
                                                ->get();

        $output = "";
        foreach ($result as $key => $value) {
            $output .= "
            <tr>
                <td></td>
                <td><a href=\"javascript:;\" style=\"padding-left:20px;\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i> ".$value->rincian."</a></td>
                <td>".str_replace(".", ",", $value->volume1)." ".$value->satuan1." x ".str_replace(".", ",", $value->volume2)." ".$value->satuan2."</td>
                <td>".number_format($value->harga_satuan,2,",",".")."</td>
                <td>".number_format($value->rincian_anggaran,2,",",".")."</td>
            </tr>
            "; 
        }

        return $output;
    }

}
