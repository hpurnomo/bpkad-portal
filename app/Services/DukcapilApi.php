<?php 
namespace BPKAD\Services;

class DukcapilApi extends ApiHttpBasic
{

  protected $url;
  protected $key;

  public function __construct($url='',$key='nik')
  {
    $this->url = $url;
    $this->key = $key;
    // http://soadev.jakarta.go.id/nikpktdev?app=PKTdev&pget=PKTdev&pusr=PKTdev&nik=3171030107780007
    parent::__construct('http://soadki.jakarta.go.id/rest/gov/dki/simpeg/ws/getPegawaiSIPKD',"updiprod","updiprod");
  }

  public function getData($nik)
  {
    $params = 'nrk';
    $args = ['nrk'=>$nik];
    return parent::connect('',$args);
  }

  public function getAnggota($nik)
  {
    $args = ['app'=>'PKTdev','pget'=>'PKTdev','pusr'=>'PKTdev','nik'=>$nik];
    $result = parent::connect('',$args);
    $out = $result['data'];
    $result = ['status'=>0, 'data'=>'', 'message'=>'NIK Tidak Valid'];
    if ($result['status'] == 'success')
    {
      $data = $out->DATA->ANGGOTA;
      $result = ['status'=>$out->status, 'data'=>$data, 'message'=> $out->status ? 'NIK Valid' : 'NIK Tidak Valid'];
    }

    return $result;
  }

}
