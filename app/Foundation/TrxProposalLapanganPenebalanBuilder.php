<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Trx_proposal_lapangan_penebalan;

trait TrxProposalLapanganPenebalanBuilder{

	static function getArrayByUUIDAndtahap($uuid,$tahap){
		$plp = $query = Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		
		if($plp){
			$dataPlp = ['cek_aktivitas' => $plp->cek_aktivitas,
		    'keterangan_cek_aktivitas' => $plp->keterangan_cek_aktivitas,
		    'cek_kepengurusan' => $plp->cek_kepengurusan,
		    'keterangan_cek_kepengurusan' => $plp->keterangan_cek_kepengurusan,
		    'cek_rab' => $plp->cek_rab,
		    'keterangan_cek_rab' => $plp->keterangan_cek_rab,
		    'cek_waktu_pelaksanaan' => $plp->cek_waktu_pelaksanaan,
		    'keterangan_cek_waktu_pelaksanaan' => $plp->keterangan_cek_waktu_pelaksanaan];
			
		}
		else{
			$dataPlp = ['cek_aktivitas' => '',
		    'keterangan_cek_aktivitas' => '',
		    'cek_kepengurusan' => '',
		    'keterangan_cek_kepengurusan' => '',
		    'cek_rab' => '',
		    'keterangan_cek_rab' => '',
		    'cek_waktu_pelaksanaan' => '',
		    'keterangan_cek_waktu_pelaksanaan' => ''];
		}

		return $dataPlp;
	}

}