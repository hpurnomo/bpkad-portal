<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Cetak_rekomendasi_pencairan;
use Carbon\Carbon;

trait CetakRekomendasiPencairanBuilder{

	static function getArrayByNoProp($no_prop){
		$crekomendasiPencairan = Cetak_rekomendasi_pencairan::where('no_prop',$no_prop)->first();
		
		if($crekomendasiPencairan){
			$dataCrekomendasiPencairan = ['id' => $crekomendasiPencairan->id,
					'nomor' => $crekomendasiPencairan->nomor,
					'sifat' => $crekomendasiPencairan->sifat,
					'lampiran' => $crekomendasiPencairan->lampiran,
					'tanggal1' => $crekomendasiPencairan->tanggal1,
					'bulan1' => $crekomendasiPencairan->bulan1,
					'tahun1' => $crekomendasiPencairan->tahun1,
					'nomor2' => $crekomendasiPencairan->nomor2,
					'tanggal2' => $crekomendasiPencairan->tanggal2,
					'bulan2' => $crekomendasiPencairan->bulan2,
					'tahun2' => $crekomendasiPencairan->tahun2,
					'hal' => $crekomendasiPencairan->hal,
					'nomor_gubernur' => $crekomendasiPencairan->nomor_gubernur,
					'tentang' => $crekomendasiPencairan->tentang,
					'nama_kepala' => $crekomendasiPencairan->nama_kepala,
					'nip' => $crekomendasiPencairan->nip,
					'uuid_prop' => $crekomendasiPencairan->uuid_prop,
					'judul' => $crekomendasiPencairan->judul,
					'status_pencairan' => $crekomendasiPencairan->status_pencairan,
					'nominal_pencairan' => $crekomendasiPencairan->nominal_pencairan,
					'file_name' => $crekomendasiPencairan->file_name,
					'text1' => $crekomendasiPencairan->text1,
					'text2' => $crekomendasiPencairan->text2,
					'text3' => $crekomendasiPencairan->text3,
					'tahun3' => $crekomendasiPencairan->tahun3,
					'text4' => $crekomendasiPencairan->text4,
					'text5' => $crekomendasiPencairan->text5,
					'text6' => $crekomendasiPencairan->text6,
					'text7' => $crekomendasiPencairan->text7,
					'text8' => $crekomendasiPencairan->text8,
					'text9' => $crekomendasiPencairan->text9,
					'terbilang' => $crekomendasiPencairan->terbilang,
					'no_rekening' => $crekomendasiPencairan->no_rekening,
					'atas_nama' => $crekomendasiPencairan->atas_nama,
					'plt' => $crekomendasiPencairan->plt,
					'is_generate' => $crekomendasiPencairan->is_generate];	
		}
		else{
			$dataCrekomendasiPencairan = ['id' => '',
					'nomor' => '',
					'sifat' => '',
					'lampiran' => '',
					'tanggal1' => '',
					'bulan1' => '',
					'tahun1' => '',
					'nomor2' => '',
					'tanggal2' => '',
					'bulan2' => '',
					'tahun2' => '',
					'hal' => '',
					'nomor_gubernur' => '',
					'tentang' => '',
					'nama_kepala' => '',
					'nip' => '',
					'uuid_prop' => '',
					'judul' => '',
					'status_pencairan' => '',
					'nominal_pencairan' => '',
					'file_name' => '',
					'text1' => '',
					'text2' => '',
					'text3' => '',
					'tahun3' => '',
					'text4' => '',
					'text5' => '',
					'text6' => '',
					'text7' => '',
					'text8' => '',
					'text9' => '',
					'text3' => '',
					'terbilang' => '',
					'no_rekening' => '',
					'atas_nama' => '',
					'plt' => '',
					'is_generate' => ''];	
		}

		return $dataCrekomendasiPencairan;
	}


	static function getArrayByID($id){
		$crekomendasiPencairan = Cetak_rekomendasi_pencairan::where('id',$id)->first();
		
		if($crekomendasiPencairan){
			$dataCrekomendasiPencairan = ['id' => $crekomendasiPencairan->id,
					'nomor' => $crekomendasiPencairan->nomor,
					'sifat' => $crekomendasiPencairan->sifat,
					'lampiran' => $crekomendasiPencairan->lampiran,
					'tanggal1' => $crekomendasiPencairan->tanggal1,
					'bulan1' => $crekomendasiPencairan->bulan1,
					'tahun1' => $crekomendasiPencairan->tahun1,
					'nomor2' => $crekomendasiPencairan->nomor2,
					'tanggal2' => $crekomendasiPencairan->tanggal2,
					'bulan2' => $crekomendasiPencairan->bulan2,
					'tahun2' => $crekomendasiPencairan->tahun2,
					'hal' => $crekomendasiPencairan->hal,
					'nomor_gubernur' => $crekomendasiPencairan->nomor_gubernur,
					'tentang' => $crekomendasiPencairan->tentang,
					'nama_kepala' => $crekomendasiPencairan->nama_kepala,
					'nip' => $crekomendasiPencairan->nip,
					'uuid_prop' => $crekomendasiPencairan->uuid_prop,
					'judul' => $crekomendasiPencairan->judul,
					'status_pencairan' => $crekomendasiPencairan->status_pencairan,
					'nominal_pencairan' => $crekomendasiPencairan->nominal_pencairan,
					'file_name' => $crekomendasiPencairan->file_name,
					'text1' => $crekomendasiPencairan->text1,
					'text2' => $crekomendasiPencairan->text2,
					'text3' => $crekomendasiPencairan->text3,
					'tahun3' => $crekomendasiPencairan->tahun3,
					'text4' => $crekomendasiPencairan->text4,
					'text5' => $crekomendasiPencairan->text5,
					'text6' => $crekomendasiPencairan->text6,
					'text7' => $crekomendasiPencairan->text7,
					'text8' => $crekomendasiPencairan->text8,
					'text9' => $crekomendasiPencairan->text9,
					'text3' => $crekomendasiPencairan->text3,
					'terbilang' => $crekomendasiPencairan->terbilang,
					'no_rekening' => $crekomendasiPencairan->no_rekening,
					'atas_nama' => $crekomendasiPencairan->atas_nama,
					'plt' => $crekomendasiPencairan->plt,
					'is_generate' => $crekomendasiPencairan->is_generate
				];	
		}
		else{
			$dataCrekomendasiPencairan = ['id' => '',
					'nomor' => '',
					'sifat' => '',
					'lampiran' => '',
					'tanggal1' => '',
					'bulan1' => '',
					'tahun1' => '',
					'nomor2' => '',
					'tanggal2' => '',
					'bulan2' => '',
					'tahun2' => '',
					'hal' => '',
					'nomor_gubernur' => '',
					'tentang' => '',
					'nama_kepala' => '',
					'nip' => '',
					'uuid_prop' => '',
					'judul' => '',
					'status_pencairan' => '',
					'nominal_pencairan' => '',
					'file_name' => '',
					'text1' => '',
					'text2' => '',
					'text3' => '',
					'tahun3' => '',
					'text4' => '',
					'text5' => '',
					'text6' => '',
					'text7' => '',
					'text8' => '',
					'text9' => '',
					'text3' => '',
					'terbilang' => '',
					'no_rekening' => '',
					'atas_nama' => '',
					'plt' => '',
					'is_generate' => ''];	
		}

		return $dataCrekomendasiPencairan;
	}

	static function update($request)
	{
		// FORMAT RUPIAH
		$nominal_pencairan = str_replace(".", "", $request['nominal_pencairan']);

		$update = Cetak_rekomendasi_pencairan::find($request['id']);
		$update->no_prop = $request['no_prop'];
		$update->uuid_prop = $request['uuid'];
		$update->judul = $request['judul'];
		$update->nomor = $request['nomor'];
		$update->sifat = $request['sifat'];
		$update->lampiran = $request['lampiran'];
		$update->text1 = $request['text1'];
		$update->tanggal1 = $request['tanggal1'];
		$update->bulan1 = $request['bulan1'];
		$update->tahun1 = $request['tahun1'];
		$update->text2 = $request['text2'];
		$update->nomor2 = $request['nomor2'];
		$update->tanggal2 = $request['tanggal2'];
		$update->bulan2 = $request['bulan2'];
		$update->tahun2 = $request['tahun2'];
		$update->hal = $request['hal'];
		$update->text3 = $request['text3'];
		$update->nomor_gubernur = $request['nomor_gubernur'];
		$update->tahun3 = $request['tahun3'];
		$update->tentang = $request['tentang'];
		$update->text4 = $request['text4'];
		$update->text5 = $request['text5'];
		$update->text6 = $request['text6'];
		$update->text7 = $request['text7'];
		$update->text8 = $request['text8'];
		$update->text9 = $request['text9'];
		$update->text3 = $request['text3'];
		$update->terbilang = $request['terbilang'];
		$update->no_rekening = $request['no_rekening'];
		$update->atas_nama = $request['atas_nama'];
		$update->nama_kepala = $request['nama_kepala'];
		$update->nip = $request['nip'];
		$update->plt = $request['plt'];
		$update->nominal_pencairan = doubleval(str_replace(",",".",$nominal_pencairan));
		$update->save();

		//upload file to surat pernyataan tanggung jawab
		if ( isset($request['file_name']) ) {

			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());

			$destinationPath = 'upload_ttd_basah_pencarian/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$request['file_name']->move($destinationPath, $fileName);
		}

		$update->status_pencairan = $request['status_pencairan'];
		$update->save();

		// $updateNominalPencairan = Proposal::where('no_prop',$request->no_prop)->first();
		// $updateNominalPencairan->nominal_pencairan = $request->nominal_pencairan;
		// $updateNominalPencairan->save();

		// Proposal::where('no_prop',$request['no_prop'])
  //         				->update(['nominal_pencairan' => $request['nominal_pencairan']
  //         						 ]);

		return true;
	}

}