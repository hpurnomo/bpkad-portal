<?php
namespace BPKAD\Foundation;

use DB;

trait ProposalPenebalanBANGGARBuilder{

	static function getByUUID($uuid){
		$query = DB::table('trx_proposal_penebalan_banggar as a')
		->select('a.*',
		'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.email as email_lembaga','b.is_verify',
		'c.tahun as tahun_anggaran','d.nm_skpd','e.kelompok',
		'f.nmkota','g.nmkec','h.nmkel','i.nama as namaRW','j.nama as namaRT')
		->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
		->leftJoin('mst_periode as c','a.noper','=','c.noper')
		->leftJoin('mst_skpd as d','a.no_skpd','=','d.nomor')
		->leftJoin('mst_rekening as e','a.no_rek','=','e.nomor')
		->leftJoin('mst_kota as f','b.nokota','=','f.nokota')
		->leftJoin('mst_kecamatan as g','b.nokec','=','g.nokec')
		->leftJoin('mst_kelurahan as h','b.nokel','=','h.nokel')
		->leftJoin('mst_rw as i','b.rwID','=','i.id')
		->leftJoin('mst_rt as j','b.rtID','=','j.id')

		->where('a.uuid_proposal_pengajuan',$uuid)->first();
		return $query;
	}

	static function getByUUIDandTerkirim($uuid){
		$query = DB::table('trx_proposal_penebalan_banggar as a')
		->select('a.*',
		'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.email as email_lembaga','b.is_verify',
		'c.tahun as tahun_anggaran','d.nm_skpd','e.kelompok',
		'f.nmkota','g.nmkec','h.nmkel','i.nama as namaRW','j.nama as namaRT')
		->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
		->leftJoin('mst_periode as c','a.noper','=','c.noper')
		->leftJoin('mst_skpd as d','a.no_skpd','=','d.nomor')
		->leftJoin('mst_rekening as e','a.no_rek','=','e.nomor')
		->leftJoin('mst_kota as f','b.nokota','=','f.nokota')
		->leftJoin('mst_kecamatan as g','b.nokec','=','g.nokec')
		->leftJoin('mst_kelurahan as h','b.nokel','=','h.nokel')
		->leftJoin('mst_rw as i','b.rwID','=','i.id')
		->leftJoin('mst_rt as j','b.rtID','=','j.id')

		->where('a.uuid_proposal_pengajuan',$uuid)
		->where('a.status_kirim',1)
		->first();
		return $query;
	}
}