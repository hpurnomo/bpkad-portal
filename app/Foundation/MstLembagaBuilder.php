<?php
namespace BPKAD\Foundation;

use DB;

trait MstLembagaBuilder{

	static function getAll(){
		$query = DB::table('mst_lembaga as a')->select('a.*','b.nm_skpd')
											  ->leftJoin('mst_skpd as b','a.kd_skpd','=','b.kd_skpd')
											  ->get();
		return $query;
	}

	static function getByKdSkpd($kd_skpd){
		$query = DB::table('mst_lembaga as a')->select('a.*','b.nm_skpd','b.IsAktif','c.nama AS tipeLembaga')
											  ->leftJoin('mst_skpd as b','a.kd_skpd','=','b.kd_skpd')
											  ->join('mst_tipe_lembaga as c','a.tipeLembagaID','=','c.id')
											  ->where('a.kd_skpd',$kd_skpd)
											  ->orderBy('a.nomor','desc')
											  ->get();
		return $query;
	}

	static function getByNomor($nomor){
		$query = DB::table('mst_lembaga as a')->select('a.*','b.nm_skpd','c.nama as namaTipeLembaga','d.nama AS noRT','e.nama AS noRW','f.nmkel','g.ktp_ketua','g.akta','g.npwp AS scanNPWP','g.skd','g.izin_operasional','g.sertifikat','g.rekening_lembaga','h.file_name AS fotoLembaga','i.nmkota','j.nmkec')
											  ->leftJoin('mst_skpd as b','a.kd_skpd','=','b.kd_skpd')
											  ->leftJoin('mst_tipe_lembaga as c','a.tipeLembagaID','=','c.id')
											  ->leftJoin('mst_rt AS d','a.rtID','=','d.id')
											  ->leftJoin('mst_rw AS e','a.rwID','=','e.id')
											  ->leftJoin('mst_kelurahan AS f','a.nokel','=','f.nokel')
											  ->leftJoin('user_lembaga AS g','a.nomor','=','g.no_lembaga')
											  ->leftJoin('foto_lembaga as h', 'a.nomor', '=', 'h.no_lembaga')
											  ->leftJoin('mst_kota as i', 'a.nokota', '=', 'i.nokota')
											  ->leftJoin('mst_kecamatan as j', 'a.nokec', '=', 'j.nokec')
											  ->where('a.nomor',$nomor)
											  ->first();
		return $query;
	}

}