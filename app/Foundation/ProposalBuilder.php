<?php
namespace BPKAD\Foundation;

use DB;

trait ProposalBuilder{

	static function getByID($id){
		$query = DB::table('trx_proposal as a')
		->select('a.*',
		'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.email as email_lembaga','b.is_verify',
		'c.tahun as tahun_anggaran','d.nm_skpd','d.alamat as alamatSKPD','d.kontak_person as ketuaSKPD','d.nip_ketua as nipKetuaSKPD','e.kelompok',
		'f.nmkota','g.nmkec','h.nmkel','i.nama as namaRW','j.nama as namaRT','k.email as emailKoordinator','k.real_name','d.email as email_skpd')
		->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
		->leftJoin('mst_periode as c','a.noper','=','c.noper')
		->leftJoin('mst_skpd as d','a.no_skpd','=','d.nomor')
		->leftJoin('mst_rekening as e','a.no_rek','=','e.nomor')
		->leftJoin('mst_kota as f','b.nokota','=','f.nokota')
		->leftJoin('mst_kecamatan as g','b.nokec','=','g.nokec')
		->leftJoin('mst_kelurahan as h','b.nokel','=','h.nokel')
		->leftJoin('mst_rw as i','b.rwID','=','i.id')
		->leftJoin('mst_rt as j','b.rtID','=','j.id')
		->join('users as k','a.crb','=','k.user_id')
		->where('a.no_prop',$id)->first();
		return $query;
	}

	static function getBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.noper',4)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getTotalProposalDisetujuiBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',1)
										->where('a.noper',4)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalDitolakBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',2)
										->where('a.noper',4)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalDikoreksiBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',3)
										->where('a.noper',4)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalMenungguBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',0)
										->where('a.noper',4)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}


	static function getProposalPerubahanBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.noper',5)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getTotalProposalPerubahanDisetujuiBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',1)
										->where('a.noper',5)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalPerubahanDitolakBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',2)
										->where('a.noper',5)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalPerubahanDikoreksiBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',3)
										->where('a.noper',5)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getProposalPengusulanBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.noper',6)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getTotalProposalPengusulanDisetujuiBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',1)
										->where('a.noper',6)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalPengusulanDitolakBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',2)
										->where('a.noper',6)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	static function getTotalProposalPengusulanDikoreksiBySkpdID($skpdID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.status_prop',3)
										->where('a.noper',6)
										->where('a.status_kirim',1)
										->get();
		return $query;
	}

	// static function getByNoLembaga($noLembaga){
	// 	$query = DB::table('trx_proposal as a')->select('a.*',
	// 									'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
	// 									->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
	// 									->where('a.no_lembaga',$noLembaga)
	// 									->where('a.noper',4)
	// 									->get();
	// 	return $query;
	// }

	static function getTotalProposalDisetujuiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',1)
										->where('a.noper',4)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalDisetujuiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',1)
										->where('a.noper',4)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalDitolakByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',2)
										->where('a.noper',4)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalDitolakByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',2)
										->where('a.noper',4)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalDikoreksiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',3)
										->where('a.noper',4)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalDikoreksiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',3)
										->where('a.noper',4)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalPerubahanDisetujuiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',1)
										->where('a.noper',5)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalPerubahanDisetujuiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',1)
										->where('a.noper',5)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalPerubahanDitolakByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',2)
										->where('a.noper',5)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalPerubahanDitolakByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',2)
										->where('a.noper',5)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalPerubahanDikoreksiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',3)
										->where('a.noper',5)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalPerubahanDikoreksiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',3)
										->where('a.noper',5)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalPengusulanDisetujuiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',1)
										->where('a.noper',6)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalPengusulanDisetujuiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',1)
										->where('a.noper',6)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalPengusulanDitolakByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',2)
										->where('a.noper',6)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalPengusulanDitolakByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',2)
										->where('a.noper',6)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	static function getTotalProposalPengusulanDikoreksiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',3)
										->where('a.noper',6)
										->get();
		return $query;
	}

	static function getTotalRupiahProposalPengusulanDikoreksiByNoLembaga($noLembaga){
		$query = DB::table('trx_proposal as a')->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.status_prop',3)
										->where('a.noper',6)
										->sum('a.nominal_rekomendasi');
		return $query;
	}

	// static function getByNoLembagaNoper($noLembaga,$noper){
	// 	$query = DB::table('trx_proposal as a')->select('a.*',
	// 									'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
	// 									->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
	// 									->where('a.no_lembaga',$noLembaga)
	// 									->where('a.noper',$noper)->get();
	// 	return $query;
	// }

	static function getByNoLembagaFasePengusulan($noLembaga,$noper){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->where('a.noper',$noper)->get();
		return $query;
	}

	static function getTotalProposalDisetujuiByUserID($userID){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.crb',$userID)
										->where('a.status_prop',1)
										->get();
		return $query;
	}

	// static function getByUserID($userID){
	// 	$query = DB::table('trx_proposal as a')->select('a.*',
	// 									'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
	// 									->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
	// 									->where('a.crb',$userID)->get();
	// 	return $query;
	// }
	

	static function getTotalProposal(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										// ->where('a.noper',4)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDisetujui(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',1)
										// ->where('a.noper',4)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalMenunggu(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',0)
										// ->where('a.noper',4)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDitolak(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',2)
										// ->where('a.noper',4)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDikoreksi(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',3)
										// ->where('a.noper',4)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDisetujuiByFaseNoper($noper){
		$query = DB::table('trx_proposal as a')
		->select('a.*',
		'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
			'c.nm_skpd')
		->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
		->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
		->where('a.status_prop',1)
		->where('a.noper',$noper)
		->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDisetujuiFasePenetapanByNoper($noper){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',1)
										->where('a.noper',$noper)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDisetujuiFasePerubahan(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',1)
										->where('a.noper',5)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getProposalDisetujuiFasePengusulan(){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',1)
										->where('a.noper',6)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	static function getBySkpdIDProposalSKPDByNoper($skpdID,$noper){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_skpd',$skpdID)
										->where('a.noper',$noper)
										->where('a.status_kirim',1)->get();
		return $query;
	}

	// static function getBySkpdIDProposalPenetapan($skpdID){
	// 	$query = DB::table('trx_proposal as a')->select('a.*',
	// 									'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
	// 									->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
	// 									->where('a.no_skpd',$skpdID)
	// 									->where('a.noper',4)
	// 									->where('a.status_kirim',1)->get();
	// 	return $query;
	// }

	// static function getBySkpdIDProposalPerubahan($skpdID){
	// 	$query = DB::table('trx_proposal as a')->select('a.*',
	// 									'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
	// 									->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
	// 									->where('a.no_skpd',$skpdID)
	// 									->where('a.noper',5)
	// 									->where('a.status_kirim',1)->get();
	// 	return $query;
	// }

	// static function getBySkpdIDProposalPengusulan($skpdID){
	// 	$query = DB::table('trx_proposal as a')->select('a.*',
	// 									'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
	// 									->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
	// 									->where('a.no_skpd',$skpdID)
	// 									->where('a.noper',6)
	// 									->where('a.status_kirim',1)->get();
	// 	return $query;
	// }

	static function getByNoLembagaDilengkapiFasePenetapanAPBD2017($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->whereIn('a.status_prop',[0,1])
										->Where('a.noper',4)->get();
		return $query;
	}

	static function getByNoLembagaDilengkapiFasePenetapanAPBD2018($noLembaga){
		$query = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->where('a.no_lembaga',$noLembaga)
										->whereIn('a.status_prop',[0,1])
										->Where('a.noper',6)->get();
		return $query;
	}

}