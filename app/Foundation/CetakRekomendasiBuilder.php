<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Cetak_rekomendasi;

trait CetakRekomendasiBuilder{

	static function getArrayByNoProp($no_prop){
		$crekomendasi = Cetak_rekomendasi::where('no_prop',$no_prop)->first();
		
		if($crekomendasi){
			$dataCrekomendasi = ['id' => $crekomendasi->id,
					'nomor' => $crekomendasi->nomor,
					'sifat' => $crekomendasi->sifat,
					'lampiran' => $crekomendasi->lampiran,
					'tanggal1' => $crekomendasi->tanggal1,
					'bulan1' => $crekomendasi->bulan1,
					'tahun1' => $crekomendasi->tahun1,
					'nomor2' => $crekomendasi->nomor2,
					'tanggal2' => $crekomendasi->tanggal2,
					'bulan2' => $crekomendasi->bulan2,
					'tahun2' => $crekomendasi->tahun2,
					'hal' => $crekomendasi->hal,
					'nama_kepala' => $crekomendasi->nama_kepala,
					'nip' => $crekomendasi->nip,
					'is_generate' => $crekomendasi->is_generate];	
		}
		else{
			$dataCrekomendasi = ['id' => '',
					'nomor' => '',
					'sifat' => '',
					'lampiran' => '',
					'tanggal1' => '',
					'bulan1' => '',
					'tahun1' => '',
					'nomor2' => '',
					'tanggal2' => '',
					'bulan2' => '',
					'tahun2' => '',
					'hal' => '',
					'nama_kepala' => '',
					'nip' => '',
					'is_generate' => ''];
		}

		return $dataCrekomendasi;
	}

}