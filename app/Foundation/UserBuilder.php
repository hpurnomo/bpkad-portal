<?php
namespace BPKAD\Foundation;

use DB;

trait UserBuilder{

	static function getById($id){

		$queryFirst = DB::table('users as a')->select('a.*')   
			->where('a.user_id',$id)->first();

		if($queryFirst->group_id==13){ 
			$query = DB::table('users as a')->select('a.*')  
			->join('user_group as b', 'a.group_id', '=', 'b.group_id')
			->where('a.user_id',$id)->first();
		}else{ 
			$query = DB::table('users as a')->select('a.*','b.group_name',
										'c.nomor','c.nm_lembaga','c.alamat as alamatLembaga','c.email as emailLembaga','c.npwp as npwpLembaga','c.no_telepon','c.no_fax','c.no_akta','c.tgl_akta','c.no_surat_domisili','c.tgl_surat_domisili','c.no_sertifikat','c.tgl_sertifikat','c.no_izin_operasional','c.tgl_izin_operasional','c.nama_bank','c.no_rek','c.pemilik_rek','c.kontak_person','c.nik_person','c.email_person','c.alamat_person','c.no_hp_person','c.foto_person','c.kontak_person_sekretaris','c.email_person_sekretaris','c.alamat_person_sekretaris','c.no_hp_person_sekretaris','c.kontak_person_bendahara','c.email_person_bendahara','c.alamat_person_bendahara','c.no_hp_person_bendahara','c.latitude','c.longitude',
										'd.nm_skpd','d.kd_skpd',
										'e.kdkel','e.nmkel','e.kdpos',
										'f.nama as rtNama',
										'g.nama as rwNama',
										'h.nmkec',
										'i.nmkota',
										'j.file_name AS fotoLembaga')
										->join('user_group as b', 'a.group_id', '=', 'b.group_id')
										->leftJoin('mst_lembaga as c', 'a.no_lembaga', '=', 'c.nomor')
										->join('mst_skpd as d', 'a.no_skpd', '=', 'd.nomor')
										->leftJoin('mst_kelurahan as e', 'c.nokel', '=', 'e.nokel')
										->leftJoin('mst_rt as f', 'c.rtID', '=', 'f.id')
										->leftJoin('mst_rw as g', 'c.rwID', '=', 'g.id')
										->leftJoin('mst_kecamatan as h', 'c.nokec', '=', 'h.nokec')
										->leftJoin('mst_kota as i', 'c.nokota', '=', 'i.nokota')
										->leftJoin('foto_lembaga as j', 'a.no_lembaga', '=', 'j.no_lembaga')
										->where('a.user_id',$id)->first();
		}

		return $query;
	}

	static function getBySkpdID($skpdID){
		$query = DB::table('users as a')->select('a.*','b.group_name',
										'c.nomor','c.nm_lembaga','c.alamat as alamatLembaga','c.npwp as npwpLembaga','c.no_akta','c.tgl_akta','c.no_surat_domisili','c.tgl_surat_domisili','c.no_sertifikat','c.tgl_sertifikat','c.no_izin_operasional','c.tgl_izin_operasional',
										'd.nm_skpd',
										'e.nokec','e.kdkel','e.nmkel','e.kdpos')
										->join('user_group as b', 'a.group_id', '=', 'b.group_id')
										->leftJoin('mst_lembaga as c', 'a.no_lembaga', '=', 'c.nomor')
										->join('mst_skpd as d', 'a.no_skpd', '=', 'd.nomor')
										->leftJoin('mst_kelurahan as e', 'c.nokel', '=', 'e.nokel')
										->orderBy('a.group_id','desc') 
										->where('a.no_skpd',$skpdID)->get();
		return $query;
	}

	static function getByGroupID($groupID){
		if($groupID==10){
			$query = DB::table('users as a')->select('a.*',
			'b.nm_skpd')
			->join('mst_skpd as b', 'a.no_skpd', '=', 'b.nomor')
			->orderBy('a.no_skpd','desc')
			->where('a.group_id',$groupID)->get();	
		}elseif($groupID==13){
			$query = DB::table('users as a')->select('a.*')  
			->where('a.group_id',$groupID)->get();
		}
										
		return $query;
	}


}