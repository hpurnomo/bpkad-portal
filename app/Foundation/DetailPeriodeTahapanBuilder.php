<?php
namespace BPKAD\Foundation;

use DB;

trait DetailPeriodeTahapanBuilder{

	static function getDetailPeriodeTahapanByID($id){
		$query = DB::table('detail_periode_tahapan AS a')->select('a.*','b.keterangan AS namaPeriode','b.tahun AS tahunAnggaran','c.name')
														->join('mst_periode AS b','a.periodeID','=','b.noper')
														->join('mst_tahapan AS c','a.tahapanID','=','c.id')
														->where('a.id',$id)->first();
		return $query;
	}

}