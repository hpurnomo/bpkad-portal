<?php
namespace BPKAD\Foundation;

use DB;

trait SkpdBuilder{

	static function getById($id){
		$query = DB::table('mst_skpd as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify')
										->leftJoin('mst_lembaga as b', 'a.kd_skpd', '=', 'b.kd_skpd')
										->where('a.nomor',$id)->get();
		return $query;
	}

	static function getSkpdCountLembaga(){

		$query = DB::table('mst_skpd AS a')->select('a.nomor','a.kd_skpd','a.nm_skpd',DB::raw('count(b.kd_skpd) as totalLembaga'))
											->leftJoin('mst_lembaga AS b','a.kd_skpd','=','b.kd_skpd')
											->groupBy('a.kd_skpd')->get();
		return $query;
	}

}