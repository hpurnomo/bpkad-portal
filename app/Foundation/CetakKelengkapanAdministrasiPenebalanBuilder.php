<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Cetak_kelengkapan_administrasi_penebalan;

trait CetakKelengkapanAdministrasiPenebalanBuilder{

	static function getArrayByUUIDAndtahap($uuid,$tahap){
		$cka = Cetak_kelengkapan_administrasi_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		
		if($cka){
			$dataCka = ['id' => $cka->id,
					'nomor' => $cka->nomor,
					'hari' => $cka->hari,
					'tanggal1' => $cka->tanggal1,
					'bulan1' => $cka->bulan1,
					'tahun1' => $cka->tahun1,
					'nama1' => $cka->nama1,
					'jabatan1' => $cka->jabatan1,
					'nama2' => $cka->nama2,
					'jabatan2' => $cka->jabatan2,
					'nama3' => $cka->nama3,
					'jabatan3' => $cka->jabatan3,
					'nama4' => $cka->nama4,
					'jabatan4' => $cka->jabatan4,
					'nama5' => $cka->nama5,
					'jabatan5' => $cka->jabatan5,
					'nomor2' => $cka->nomor2,
					'tanggal2' => $cka->tanggal2,
					'bulan2' => $cka->bulan2,
					'tahun2' => $cka->tahun2,
					'nama_ketua' => $cka->nama_ketua,
					'nip' => $cka->nip,
					'is_generate' => $cka->is_generate];	
		}
		else{
			$dataCka = ['id' => '',
					'nomor' => '',
					'hari' => '',
					'tanggal1' => '',
					'bulan1' => '',
					'tahun1' => '',
					'nama1' => '',
					'jabatan1' => '',
					'nama2' => '',
					'jabatan2' => '',
					'nama3' => '',
					'jabatan3' => '',
					'nama4' => '',
					'jabatan4' => '',
					'nama5' => '',
					'jabatan5' => '',
					'nomor2' => '',
					'tanggal2' => '',
					'bulan2' => '',
					'tahun2' => '',
					'nama_ketua' => '',
					'nip' => '',
					'is_generate' => ''];
		}

		return $dataCka;
	}

}