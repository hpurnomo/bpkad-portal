<?php
namespace BPKAD\Foundation;

use DB;

trait BerkasProposalBuilder{

	static function getByNoProp($no_prop){
		$query = DB::table('berkas_proposal as a')->select('a.*')
										->where('a.no_prop',$no_prop)->first();
		return $query;
	}

}