<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Cetak_peninjauan_lapangan_penebalan;

trait CetakPeninjauanLapanganPenebalanBuilder{

	static function getArrayByUUIDAndtahap($uuid,$tahap){
		$cpl = Cetak_peninjauan_lapangan_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		
		if($cpl){
			$dataCpl = ['id' => $cpl->id,
					'nomor' => $cpl->nomor,
					'hari' => $cpl->hari,
					'tanggal1' => $cpl->tanggal1,
					'bulan1' => $cpl->bulan1,
					'tahun1' => $cpl->tahun1,
					'nama1' => $cpl->nama1,
					'jabatan1' => $cpl->jabatan1,
					'nama2' => $cpl->nama2,
					'jabatan2' => $cpl->jabatan2,
					'nama3' => $cpl->nama3,
					'jabatan3' => $cpl->jabatan3,
					'nama4' => $cpl->nama4,
					'jabatan4' => $cpl->jabatan4,
					'nama5' => $cpl->nama5,
					'jabatan5' => $cpl->jabatan5,
					'nomor2' => $cpl->nomor2,
					'tanggal2' => $cpl->tanggal2,
					'bulan2' => $cpl->bulan2,
					'tahun2' => $cpl->tahun2,
					'nama_pengusul' => $cpl->nama_pengusul,
					'nama_ketua' => $cpl->nama_ketua,
					'nip' => $cpl->nip,
					'is_generate' => $cpl->is_generate];	
		}
		else{
			$dataCpl = ['id' => '',
					'nomor' => '',
					'hari' => '',
					'tanggal1' => '',
					'bulan1' => '',
					'tahun1' => '',
					'nama1' => '',
					'jabatan1' => '',
					'nama2' => '',
					'jabatan2' => '',
					'nama3' => '',
					'jabatan3' => '',
					'nama4' => '',
					'jabatan4' => '',
					'nama5' => '',
					'jabatan5' => '',
					'nomor2' => '',
					'tanggal2' => '',
					'bulan2' => '',
					'tahun2' => '',
					'nama_pengusul' => '',
					'nama_ketua' => '',
					'nip' => '',
					'is_generate' => ''];
		}

		return $dataCpl;
	}

}