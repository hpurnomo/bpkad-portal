<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\VerifikasiBerkasPencairan;

trait VerifikasiBerkasPencairanBuilder{

	static function getArrayByNoProp($no_prop){
		$cekBerkas = VerifikasiBerkasPencairan::where('no_prop',$no_prop)->first();
		
		if($cekBerkas){
			$dataCekBerkas = [
				'id' => $cekBerkas->id,
				'no_prop' => $cekBerkas->no_prop,
				'tahun_anggaran' => $cekBerkas->tahun_anggaran,
				'no_sk_gub_penetapan' => $cekBerkas->no_sk_gub_penetapan,
				'kelompok_belanja' => $cekBerkas->kelompok_belanja,
				'no_urut_pemohon_sk' => $cekBerkas->no_urut_pemohon_sk,
				'nama_pemohon' => $cekBerkas->nama_pemohon,
				'nm_skpd' => $cekBerkas->nm_skpd,
				'cek_surat_pengantar' => $cekBerkas->cek_surat_pengantar,
				'ket_surat_pengantar' => $cekBerkas->ket_surat_pengantar,
				'cek_surat_permohonan' => $cekBerkas->cek_surat_permohonan,
				'ket_surat_permohonan' => $cekBerkas->ket_surat_permohonan,
				'cek_rab' => $cekBerkas->cek_rab,
				'ket_rab' => $cekBerkas->ket_rab,
				'cek_kwitansi' => $cekBerkas->cek_kwitansi,
				'ket_kwitansi' => $cekBerkas->ket_kwitansi,
				'cek_rekening' => $cekBerkas->cek_rekening,
				'ket_rekening' => $cekBerkas->ket_rekening,
				'cek_sk_lembaga' => $cekBerkas->cek_sk_lembaga,
				'ket_sk_lembaga' => $cekBerkas->ket_sk_lembaga,
				'cek_rekomendasi' => $cekBerkas->cek_rekomendasi,
				'ket_rekomendasi' => $cekBerkas->ket_rekomendasi,
				'cek_bak_administrasi' => $cekBerkas->cek_bak_administrasi,
				'ket_bak_administrasi' => $cekBerkas->ket_bak_administrasi,
				'cek_bap_lapangan' => $cekBerkas->cek_bap_lapangan,
				'ket_bap_lapangan' => $cekBerkas->ket_bap_lapangan,
				'cek_surat_tugas' => $cekBerkas->cek_surat_tugas,
				'ket_surat_tugas' => $cekBerkas->ket_surat_tugas,
				'cek_surat_pernyataan_tanggungjawab' => $cekBerkas->cek_surat_pernyataan_tanggungjawab,
				'ket_surat_pernyataan_tanggungjawab' => $cekBerkas->ket_surat_pernyataan_tanggungjawab,
				'cek_ktp_pengurus' => $cekBerkas->cek_ktp_pengurus,
				'ket_ktp_pengurus' => $cekBerkas->ket_ktp_pengurus,
				'cek_skd' => $cekBerkas->cek_skd,
				'ket_skd' => $cekBerkas->ket_skd,
				'cek_npwp' => $cekBerkas->cek_npwp,
				'ket_npwp' => $cekBerkas->ket_npwp,
				'cek_proposal_definitif' => $cekBerkas->cek_proposal_definitif,
				'ket_proposal_definitif' => $cekBerkas->ket_proposal_definitif,
				'cek_nphd' => $cekBerkas->cek_nphd,
				'ket_nphd' => $cekBerkas->ket_nphd,
				'cek_tanda_terima_laporan' => $cekBerkas->cek_tanda_terima_laporan,
				'ket_tanda_terima_laporan' => $cekBerkas->ket_tanda_terima_laporan,
				'cek_tanda_terima_audit' => $cekBerkas->cek_tanda_terima_audit,
				'ket_tanda_terima_audit' => $cekBerkas->ket_tanda_terima_audit,
				'cek_tanda_terima_monev' => $cekBerkas->cek_tanda_terima_monev,
				'ket_tanda_terima_monev' => $cekBerkas->ket_tanda_terima_monev,
				'cek_bukti_email' => $cekBerkas->cek_bukti_email,
				'ket_bukti_email' => $cekBerkas->ket_bukti_email,

				'cek_pakta_integritas' => $cekBerkas->cek_pakta_integritas,
				'ket_pakta_integritas' => $cekBerkas->ket_pakta_integritas, 
				'cek_perjanjian_hibah_daerah'=>$cekBerkas->cek_perjanjian_hibah_daerah,
				'ket_perjanjian_hibah_daerah'=>$cekBerkas->ket_perjanjian_hibah_daerah,
				'cek_tgjwb_hibah_sebelumnya'=>$cekBerkas->cek_tgjwb_hibah_sebelumnya,
				'ket_tgjwb_hibah_sebelumnya'=>$cekBerkas->ket_tgjwb_hibah_sebelumnya,
				'cek_dok_lain'=>$cekBerkas->cek_dok_lain,
				'ket_dok_lain'=>$cekBerkas->ket_dok_lain, 

				'nama_ketua' => $cekBerkas->nama_ketua,
				'nip' => $cekBerkas->nip,
				'plt' => $cekBerkas->plt,
				'is_generate' => $cekBerkas->is_generate
			];	
		}
		else{
			$dataCekBerkas = [
				'id' => '',
				'no_prop' => '',
				'tahun_anggaran' => '',
				'no_sk_gub_penetapan' => '',
				'kelompok_belanja' => '',
				'no_urut_pemohon_sk' => '',
				'nama_pemohon' => '',
				'nm_skpd' => '',
				'cek_surat_pengantar' => '',
				'ket_surat_pengantar' => '',
				'cek_surat_permohonan' => '',
				'ket_surat_permohonan' => '',
				'cek_rab' => '',
				'ket_rab' => '',
				'cek_kwitansi' => '',
				'ket_kwitansi' => '',
				'cek_rekening' => '',
				'ket_rekening' => '',
				'cek_sk_lembaga' => '',
				'ket_sk_lembaga' => '',
				'cek_rekomendasi' => '',
				'ket_rekomendasi' => '',
				'cek_bak_administrasi' => '',
				'ket_bak_administrasi' => '',
				'cek_bap_lapangan' => '',
				'ket_bap_lapangan' => '',
				'cek_surat_tugas' => '',
				'ket_surat_tugas' => '',
				'cek_surat_pernyataan_tanggungjawab' => '',
				'ket_surat_pernyataan_tanggungjawab' => '',
				'cek_ktp_pengurus' => '',
				'ket_ktp_pengurus' => '',
				'cek_skd' => '',
				'ket_skd' => '',
				'cek_npwp' => '',
				'ket_npwp' => '',
				'cek_proposal_definitif' => '',
				'ket_proposal_definitif' => '',
				'cek_nphd' => '',
				'ket_nphd' => '',
				'cek_tanda_terima_laporan' => '',
				'ket_tanda_terima_laporan' => '',
				'cek_tanda_terima_audit' => '',
				'ket_tanda_terima_audit' => '',
				'cek_tanda_terima_monev' => '',
				'ket_tanda_terima_monev' => '',
				'cek_bukti_email' => '',
				'ket_bukti_email' => '',
				'nama_ketua' => '',
				'cek_pakta_integritas' => '',
				'ket_pakta_integritas' => '',
				'cek_perjanjian_hibah_daerah'=>'',
				'ket_perjanjian_hibah_daerah'=>'',
				'cek_tgjwb_hibah_sebelumnya'=>'',
				'ket_tgjwb_hibah_sebelumnya'=>'',
				'cek_dok_lain'=>'',
				'ket_dok_lain'=>'',

				'nip' => '',
				'plt' => '',
				'is_generate' => ''
			];
		}

		return $dataCekBerkas;
	}

}