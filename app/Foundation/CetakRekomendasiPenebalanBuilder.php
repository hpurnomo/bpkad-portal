<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Cetak_rekomendasi_penebalan;

trait CetakRekomendasiPenebalanBuilder{

	static function getArrayByUUIDAndtahap($uuid,$tahap){
		$crekomendasi = Cetak_rekomendasi_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		
		if($crekomendasi){
			$dataCrekomendasi = ['id' => $crekomendasi->id,
					'nomor' => $crekomendasi->nomor,
					'sifat' => $crekomendasi->sifat,
					'lampiran' => $crekomendasi->lampiran,
					'tanggal1' => $crekomendasi->tanggal1,
					'bulan1' => $crekomendasi->bulan1,
					'tahun1' => $crekomendasi->tahun1,
					'nomor2' => $crekomendasi->nomor2,
					'tanggal2' => $crekomendasi->tanggal2,
					'bulan2' => $crekomendasi->bulan2,
					'tahun2' => $crekomendasi->tahun2,
					'hal' => $crekomendasi->hal,
					'nama_kepala' => $crekomendasi->nama_kepala,
					'plt' => $crekomendasi->plt,
					'jenisBantuan' => $crekomendasi->jenisBantuan,
					'nip' => $crekomendasi->nip,
					'is_generate' => $crekomendasi->is_generate];	
		}
		else{
			$dataCrekomendasi = ['id' => '',
					'nomor' => '',
					'sifat' => '',
					'lampiran' => '',
					'tanggal1' => '',
					'bulan1' => '',
					'tahun1' => '',
					'nomor2' => '',
					'tanggal2' => '',
					'bulan2' => '',
					'tahun2' => '',
					'hal' => '',
					'nama_kepala' => '',
					'plt' => '',
					'jenisBantuan' => '',
					'nip' => '',
					'is_generate' => ''];
		}

		return $dataCrekomendasi;
	}

}