<?php
namespace BPKAD\Foundation;

use DB;

trait MstTokenBuilder{

	static function getAll(){
		$query = DB::table('mst_token as a')->select('a.*','b.nm_skpd')
											->leftJoin('mst_skpd as b','a.skpdID','=','b.nomor')->get();
										
		return $query;
	}

	static function getByID($id){
		$query = DB::table('mst_token as a')->select('a.*','b.nm_skpd')
											->leftJoin('mst_skpd as b','a.skpdID','=','b.nomor')
											->where('a.id',$id)->first();
										
		return $query;	
	}

}