<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\UserLembagaLapangan;

trait UserLembagaLapanganBuilder{

	static function getByNoLembaga($no_lembaga){
		$query = UserLembagaLapangan::where('no_lembaga',$no_lembaga)
								->first();
		return $query;
	}

	static function getArrayByNoLembaga($no_lembaga){
		$pll = $query = UserLembagaLapangan::where('no_lembaga',$no_lembaga)
								->first();
		
		if($pll){
			$dataPll = ['cek_ktp_ketua' => $pll->cek_ktp_ketua,
		    'keterangan_cek_ktp_ketua' => $pll->keterangan_cek_ktp_ketua,
		    'cek_alamat_lembaga' => $pll->cek_alamat_lembaga,
		    'keterangan_cek_alamat_lembaga' => $pll->keterangan_cek_alamat_lembaga,
		    'cek_akta' => $pll->cek_akta,
		    'keterangan_cek_akta' => $pll->keterangan_cek_akta,
		    'cek_npwp' => $pll->cek_npwp,
		    'keterangan_cek_npwp' => $pll->keterangan_cek_npwp,
		    'cek_skd' => $pll->cek_skd,
		    'keterangan_cek_skd' => $pll->keterangan_cek_skd,
		    'cek_izin_operasional' => $pll->cek_izin_operasional,
		    'keterangan_cek_izin_operasional' => $pll->keterangan_cek_izin_operasional,
		    'cek_sertifikat' => $pll->cek_sertifikat,
		    'keterangan_cek_sertifikat' => $pll->keterangan_cek_sertifikat,
		    'cek_rekening_lembaga' => $pll->cek_rekening_lembaga,
		    'keterangan_cek_rekening_lembaga' => $pll->keterangan_cek_rekening_lembaga,
		    'cek_bantuan_tahun_sebelum' => $pll->cek_bantuan_tahun_sebelum,
		    'keterangan_cek_bantuan_tahun_sebelum' => $pll->keterangan_cek_bantuan_tahun_sebelum];
		}
		else{
			$dataPll = ['cek_ktp_ketua' => '',
		    'keterangan_cek_ktp_ketua' => '',
		    'cek_alamat_lembaga' => '',
		    'keterangan_cek_alamat_lembaga' => '',
		    'cek_akta' => '',
		    'keterangan_cek_akta' => '',
		    'cek_npwp' => '',
		    'keterangan_cek_npwp' => '',
		    'cek_skd' => '',
		    'keterangan_cek_skd' => '',
		    'cek_izin_operasional' => '',
		    'keterangan_cek_izin_operasional' => '',
		    'cek_sertifikat' => '',
		    'keterangan_cek_sertifikat' => '',
		    'cek_rekening_lembaga' => '',
		    'keterangan_cek_rekening_lembaga' => '',
		    'cek_bantuan_tahun_sebelum' => '',
		    'keterangan_cek_bantuan_tahun_sebelum' => ''];
		}

		return $dataPll;
	}

}