<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\UserLembaga;

trait UserLembagaBuilder{

	static function getByUserIDNoLembaga($userID,$no_lembaga){
		$query = UserLembaga::where('userID',$userID)
								->where('no_lembaga',$no_lembaga)
								->first();
		return $query;
	}

	static function getByUserID($userID){
		$query = UserLembaga::where('userID',$userID)
								->first();
		return $query;
	}

	static function getByNoLembaga($no_lembaga){
		$query = UserLembaga::where('no_lembaga',$no_lembaga)
								->first();
		return $query;
	}

	static function getArrayByNoLembaga($no_lembaga){
		$kal = $query = UserLembaga::where('no_lembaga',$no_lembaga)
								->first();
		
		if($kal){
			$dataKal = ['cek_ktp_ketua' => $kal->cek_ktp_ketua,
		    'keterangan_cek_ktp_ketua' => $kal->keterangan_cek_ktp_ketua,
		    'cek_alamat_lembaga' => $kal->cek_alamat_lembaga,
		    'keterangan_cek_alamat_lembaga' => $kal->keterangan_cek_alamat_lembaga,
		    'cek_akta' => $kal->cek_akta,
		    'keterangan_cek_akta' => $kal->keterangan_cek_akta,
		    'cek_npwp' => $kal->cek_npwp,
		    'keterangan_cek_npwp' => $kal->keterangan_cek_npwp,
		    'cek_skd' => $kal->cek_skd,
		    'keterangan_cek_skd' => $kal->keterangan_cek_skd,
		    'cek_izin_operasional' => $kal->cek_izin_operasional,
		    'keterangan_cek_izin_operasional' => $kal->keterangan_cek_izin_operasional,
		    'cek_sertifikat' => $kal->cek_sertifikat,
		    'keterangan_cek_sertifikat' => $kal->keterangan_cek_sertifikat,
		    'cek_rekening_lembaga' => $kal->cek_rekening_lembaga,
		    'keterangan_cek_rekening_lembaga' => $kal->keterangan_cek_rekening_lembaga,
		    'cek_bantuan_tahun_sebelum' => $kal->cek_bantuan_tahun_sebelum,
		    'keterangan_cek_bantuan_tahun_sebelum' => $kal->keterangan_cek_bantuan_tahun_sebelum,
		    'cek_sk_kepengurusan' => $kal->cek_sk_kepengurusan,
		    'ket_sk_kepengurusan' => $kal->ket_sk_kepengurusan,
		    'cek_pernyataan_materai' => $kal->cek_pernyataan_materai,
		    'ket_pernyataan_materai' => $kal->ket_pernyataan_materai
			];
		}
		else{
			$dataKal = ['cek_ktp_ketua' => '',
		    'keterangan_cek_ktp_ketua' => '',
		    'cek_alamat_lembaga' => '',
		    'keterangan_cek_alamat_lembaga' => '',
		    'cek_akta' => '',
		    'keterangan_cek_akta' => '',
		    'cek_npwp' => '',
		    'keterangan_cek_npwp' => '',
		    'cek_skd' => '',
		    'keterangan_cek_skd' => '',
		    'cek_izin_operasional' => '',
		    'keterangan_cek_izin_operasional' => '',
		    'cek_sertifikat' => '',
		    'keterangan_cek_sertifikat' => '',
		    'cek_rekening_lembaga' => '',
		    'keterangan_cek_rekening_lembaga' => '',
		    'cek_bantuan_tahun_sebelum' => '',
		    'keterangan_cek_bantuan_tahun_sebelum' => '', 
		    'cek_sk_kepengurusan' => '',
		    'ket_sk_kepengurusan' => '', 
		    'cek_pernyataan_materai' => '',
		    'ket_pernyataan_materai' => '',

		];
		}

		return $dataKal;
	}

	static function getByKdSkpd($kd_skpd){
		$query = DB::table('mst_lembaga AS a')->select('a.nm_lembaga','a.nomor','b.*',
				'c.cek_ktp_ketua AS cek_ktp_ketua_lapangan','c.cek_alamat_lembaga AS cek_alamat_lembaga_lapangan','c.cek_akta AS cek_akta_lapangan','c.cek_npwp AS cek_npwp_lapangan','c.cek_skd AS cek_skd_lapangan','c.cek_izin_operasional AS cek_izin_operasional_lapangan','c.cek_sertifikat AS cek_sertifikat_lapangan','c.cek_rekening_lembaga AS cek_rekening_lembaga_lapangan','c.cek_bantuan_tahun_sebelum AS cek_bantuan_tahun_sebelum_lapangan')
					->leftJoin('user_lembaga AS b','a.nomor','=','b.no_lembaga')
					->leftJoin('user_lembaga_lapangan AS c','a.nomor','=','c.no_lembaga')
					->where('a.kd_skpd','=',$kd_skpd)
					->get();

		return $query;

	}

}