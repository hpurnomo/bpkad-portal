<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Trx_proposal_administrasi;

trait TrxProposalAdministrasiBuilder{

	static function getArrayByNoProp($no_prop){
		$kap = $query = Trx_proposal_administrasi::where('no_prop',$no_prop)
								->first();
		
		if($kap){
			$dataKap = ['cek_aktivitas' => $kap->cek_aktivitas,
		    'keterangan_cek_aktivitas' => $kap->keterangan_cek_aktivitas,
		    'cek_kepengurusan' => $kap->cek_kepengurusan,
		    'keterangan_cek_kepengurusan' => $kap->keterangan_cek_kepengurusan,
		    'cek_rab' => $kap->cek_rab,
		    'keterangan_cek_rab' => $kap->keterangan_cek_rab,
		    'cek_waktu_pelaksanaan' => $kap->cek_waktu_pelaksanaan,
		    'keterangan_cek_waktu_pelaksanaan' => $kap->keterangan_cek_waktu_pelaksanaan];
			
		}
		else{
			$dataKap = ['cek_aktivitas' => '',
		    'keterangan_cek_aktivitas' => '',
		    'cek_kepengurusan' => '',
		    'keterangan_cek_kepengurusan' => '',
		    'cek_rab' => '',
		    'keterangan_cek_rab' => '',
		    'cek_waktu_pelaksanaan' => '',
		    'keterangan_cek_waktu_pelaksanaan' => ''];
		}

		return $dataKap;
	}

}