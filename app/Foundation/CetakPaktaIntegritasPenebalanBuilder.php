<?php
namespace BPKAD\Foundation;

use DB;
use BPKAD\Http\Entities\Cetak_pakta_integritas_penebalan;

trait CetakPaktaIntegritasPenebalanBuilder{

	static function getArrayByUUIDAndTahap($uuid,$tahap){
		$cPI = Cetak_pakta_integritas_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		
		if($cPI){
			$dataCPI = ['id' => $cPI->id,
						'uuid_proposal_pengajuan' => $cPI->uuid_proposal_pengajuan,
						'tahap' => $cPI->tahap,
						'nomor1' => $cPI->nomor1,
						'tahun1' => $cPI->tahun1,
						'nomor2' => $cPI->nomor2,
						'tahun2' => $cPI->tahun2,
						'nama_pemohon' => $cPI->nama_pemohon,
						'no_pemohon' => $cPI->no_pemohon,
						'jabatan_pemohon' => $cPI->jabatan_pemohon,
						'nama_lembaga' => $cPI->nama_lembaga,
						'alamat_lembaga' => $cPI->alamat_lembaga,
						'text1' => $cPI->text1,
						'tahun_anggaran1' => $cPI->tahun_anggaran1,
						'nominal' => $cPI->nominal,
						'terbilang' => $cPI->terbilang,
						'kegiatan1' => $cPI->kegiatan1,
						'totalTerbilangKegiatan1' => $cPI->totalTerbilangKegiatan1,
						'text2' => $cPI->text2,
						'nm_skpd' => $cPI->nm_skpd,
						'text3' => $cPI->text3,
						'text4' => $cPI->text4,
						'text5' => $cPI->text5,
						'text6' => $cPI->text6,
						'jabatan_pemberi' => $cPI->jabatan_pemberi,
						'nama_pemberi' => $cPI->nama_pemberi,
						'no_pemberi' => $cPI->no_pemberi,
						'text7' => $cPI->text7,
						'is_generate' => $cPI->is_generate];	
		}
		else{
			$dataCPI = ['id' => '',
						'uuid_proposal_pengajuan' => '',
						'tahap' => '',
						'nomor1' => '',
						'tahun1' => '',
						'nomor2' => '',
						'tahun2' => '',
						'nama_pemohon' => '',
						'no_pemohon' => '',
						'jabatan_pemohon' => '',
						'nama_lembaga' => '',
						'alamat_lembaga' => '',
						'text1' => '',
						'tahun_anggaran1' => '',
						'nominal' => '',
						'terbilang' => '',
						'kegiatan1' => '',
						'totalTerbilangKegiatan1' => '',
						'text2' => '',
						'nm_skpd' => '',
						'text3' => '',
						'text4' => '',
						'text5' => '',
						'text6' => '',
						'jabatan_pemberi' => '',
						'nama_pemberi' => '',
						'no_pemberi' => '',
						'text7' => '',
						'is_generate' => ''];
		}

		return $dataCPI;
	}

}