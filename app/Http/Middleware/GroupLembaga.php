<?php

namespace BPKAD\Http\Middleware;

use Closure;
use Auth;

class GroupLembaga
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if(Auth::user()->group_id==12){
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
