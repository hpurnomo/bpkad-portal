<?php

namespace BPKAD\Http\Middleware;

use Closure;
use Auth;

class GroupSkpd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->group_id==10){
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
