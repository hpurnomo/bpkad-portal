<?php

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

/*
|--------------------------------------------------------------------------
| Portal Routes
|--------------------------------------------------------------------------
*/
// demo
Route::get('/latihan',function(){
    return view('latihan');
});

Route::get('/footer-nphd',function(){
    return view('pdf.square');
});

Route::get('/test-query',function(){
    $query = DB::select("ALTER TABLE trx_proposal ADD no_kelengkapan_administrasi VARCHAR(255) NULL AFTER tgl_survei_lap, ADD tgl_kelengkapan_administrasi DATE NULL AFTER no_kelengkapan_administrasi");

    return $query;
});

Route::get('test-email',function(){

    $data = [];

    Mail::send('emails.welcome', $data, function ($m) {
            $m->from('hello@app.com', 'Your Application');

            $m->to('hary.purnomo87@gmail.com', 'hary purnomo')->subject('Your Reminder!');
        });

    return 'success';
});

//go to app PPKD
Route::get('admin',['as'=>'home.admin','uses'=>'Beranda\HomeController@admin']);
Route::get('page-admin',function(){
    return view('beranda.admin');
});

/*
| Module Auth
*/
Route::get('login',['as'=>'login.form','uses'=>'Auth\AuthController@getLogin']);
Route::post('login',['as'=>'login.form','uses'=>'Auth\AuthController@postLogin']);
Route::get('logout', 'Auth\AuthController@getLogout');
Route::resource('forgot-password','Auth\ForgotPassword');

/*
| Module Registrasi User Lembaga
*/
Route::get('signup',['as'=>'signup.form','uses'=>'Auth\RegisterController@index']);
Route::get('signup-user-lembaga/{idLembaga}',['as'=>'signup.lembaga.user','uses'=>'Auth\RegisterController@userLembaga']);
Route::post('signup-user-lembaga/{idLembaga}',['as'=>'signup.lembaga.user','uses'=>'Auth\RegisterController@regUser']);
Route::get('registration-status/{noReg}',['as'=>'registration.status','uses'=>'Auth\RegisterController@regStatus']);
Route::get('aktifasi-akun/{ver_token}',['as'=>'aktifasi.akun','uses'=>'Auth\RegisterController@aktifasiAkun']);

/*
| Module Registrasi User Individu
*/
Route::get('signup-user-individu',['as'=>'signup.individu.form','uses'=>'Auth\RegisterController@index']);
// Route::get('signup-user-lembaga/{idLembaga}',['as'=>'signup.lembaga.user','uses'=>'Auth\RegisterController@userLembaga']);
// Route::post('signup-user-lembaga/{idLembaga}',['as'=>'signup.lembaga.user','uses'=>'Auth\RegisterController@regUser']);
// Route::get('registration-status/{noReg}',['as'=>'registration.status','uses'=>'Auth\RegisterController@regStatus']);
// Route::get('aktifasi-akun/{ver_token}',['as'=>'aktifasi.akun','uses'=>'Auth\RegisterController@aktifasiAkun']);

/*
| Module Verify User
*/
Route::get('input-token',['as'=>'token.skpd.form','uses'=>'Auth\RegisterController@inputToken']);
Route::post('verifikasi-token',['as'=>'verifikasi.token','uses'=>'Auth\RegisterController@verifikasiToken']);
Route::get('signup-skpd/{skpdID}',['as'=>'signup.skpd.form','uses'=>'Auth\RegisterController@userSKPD']);
Route::post('reg-user-skpd', ['as'=>'reg.user.skpd','uses'=>'Auth\RegisterController@regUserSKPD']);

/*
| Module Password Reset
*/
Route::get('password/email', ['as'=>'password.by.email','uses'=>'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as'=>'password.by.email','uses'=>'Auth\PasswordController@postEmail']);
Route::get('password/nrk', ['as'=>'password.by.nrk','uses'=>'Auth\PasswordController@getNrk']);
Route::post('password/nrk', ['as'=>'password.by.nrk','uses'=>'Auth\PasswordController@postNrk']);
Route::get('password/status',['as'=>'password.reset.status','uses'=>'Auth\PasswordController@getStatus']);

// Password reset 
Route::get('password/reset/{token}', ['as'=>'reset.password','uses'=>'Auth\PasswordController@getReset']);
Route::post('password/reset', ['as'=>'reset.password','uses'=>'Auth\PasswordController@postReset']);

/*
| Module Portal
*/
//Beranda
Route::get('/',['as'=>'home','uses'=>'Beranda\HomeController@index']);
Route::post('kirim-pesan',['as'=>'kirim.pesan','uses'=>'Beranda\HomeController@kirimPesan']);
Route::get('penerima-tahun/{tahun}',['as'=>'penerima.tahun','uses'=>'Beranda\HomeController@showPenerima']);

//Pencarian
Route::get('pencarian',['as'=>'search.engine','uses'=>'Search_engine\SearchEngineController@index']);
Route::get('pencarian/filter/{request?}',['as'=>'filter.search.engine','uses'=>'Search_engine\SearchEngineController@filter']);
// Route::get('pencarian/filter-pencarian/bypage/{page?}',function(){
// 	return session('key');
// });
// Route::get('filter-pencarian/page/{page?}',['as'=>'filter.search.engine','uses'=>'Search_engine\SearchEngineController@filterByPage']);

//lembaga
Route::get('detail-lembaga/{tahun}/{idlembaga}',['as'=>'detail.lembaga','uses'=>'Lembaga\LembagaController@show']);

//pengaduan
Route::get('laporan',['as'=>'pengaduan.form','uses'=>'Pengaduan\PengaduanController@index']);
Route::post('laporan',['as'=>'pengaduan.post','uses'=>'Pengaduan\PengaduanController@store']);

//Login Kategori Penerima
Route::get('kategori-penerima',['as'=>'kategori.penerima','uses'=>'Beranda\HomeController@kategoriPenerima']);

//Datatables
Route::get('datatables-lembaga',['as'=>'datatables.lembaga','uses'=>'Auth\DatatablesController@getDataTables']);
Route::get('transaksi-proposal/{tahun}',['as'=>'datatables.trx.proposal','uses'=>'Beranda\DatatablesController@getTrxProposal']);

/*
|--------------------------------------------------------------------------
| Administrator Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'auth.admin'], function () {
	
	/**
	| Module Dashboard
	**/
    Route::get('/home',['as'=>'dashboard.member','uses'=>'Admin\Dashboard\DashboardController@manage']);
    Route::get('dashboard',['as'=>'dashboard.member','uses'=>'Admin\Dashboard\DashboardController@manage']);

    /**
    | Module Profile User
    **/
    Route::get('profile-user',['as'=>'profile.user', 'uses'=>'Admin\Profile\ProfileController@manage']);
    //user lembaga
    Route::post('profile-user/lembaga/update',['as'=>'profile.user.lembaga.update', 'uses'=>'Admin\Profile\ProfileUpdateController@updateUserLembaga']);
    Route::post('profile-user/lembaga/update/avatar',['as'=>'profile.user.lembaga.update.avatar', 'uses'=>'Admin\Profile\ProfileUpdateController@updateUserLembagaAvatar']);
    Route::post('profile-user/lembaga/reset/password',['as'=>'profile.user.lembaga.reset.password', 'uses'=>'Admin\Profile\ProfileUpdateController@updateUserLembagaResetPass']); 
    Route::post('profile-user/lembaga/reset/email',['as'=>'profile.user.lembaga.reset.email', 'uses'=>'Admin\Profile\ProfileUpdateController@updateUserLembagaResetEmail']); 

    /**
    * * * * * * * * * * *
    * MODULE GROUP PPKD 
    * * * * * * * * * * *
    **/

    /*
    | Module Users Management for PPKD
    */
    Route::get('ppkd/user-management',['as'=>'ppkd.user.management.manage','uses'=>'Admin\Ppkd\User\UserController@manage']);
    Route::get('ppkd/user-management/create',['as'=>'ppkd.user.management.create','uses'=>'Admin\Ppkd\User\UserController@create']);
    Route::post('ppkd/user-management/store',['as'=>'ppkd.user.management.store','uses'=>'Admin\Ppkd\User\UserController@store']);
    Route::get('ppkd/user-management/edit/{userID}',['as'=>'ppkd.user.management.edit','uses'=>'Admin\Ppkd\User\UserController@edit']);
    Route::post('ppkd/user-management/update',['as'=>'ppkd.user.management.update','uses'=>'Admin\Ppkd\User\UserController@update']);

    /*
    
    | Module Periode Tahapan for PPKD
    */
    Route::get('ppkd/periode-tahapan',['as'=>'ppkd.periode.tahapan.manage','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeTahapanController@manage']);
    Route::get('ppkd/periode-tahapan/edit/{id}',['as'=>'ppkd.periode.tahapan.edit','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeTahapanController@edit']);
    Route::post('ppkd/periode-tahapan/edit/update',['as'=>'ppkd.periode.tahapan.update','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeTahapanController@update']);

        Route::get('ppkd/periode-tahapan/create',['as'=>'ppkd.periode.create','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeController@create']);
        Route::post('ppkd/periode-tahapan/edit/periode/store',['as'=>'ppkd.periode.store','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeController@store']);

        Route::get('ppkd/periode-tahapan/edit/periode/{noper}',['as'=>'ppkd.periode.edit','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeController@edit']);
        Route::post('ppkd/periode-tahapan/edit/periode/update',['as'=>'ppkd.periode.update','uses'=>'Admin\Ppkd\PeriodeTahapan\PeriodeController@update']);

    /*
    | Module Manage SKPD Koordinator For PPKD
    */
    Route::get('ppkd/skpd-koordinator',['as'=>'ppkd.skpd.koordinator.manage','uses'=>'Admin\Ppkd\Koordinator\SkpdKoordinatorController@manage']);
    Route::get('ppkd/skpd-koordinator/show/lembaga/{kdskpd}',['as'=>'ppkd.skpd.koordinator.show','uses'=>'Admin\Ppkd\Koordinator\SkpdKoordinatorController@show']);
    Route::get('ppkd/skpd-koordinator/edit/lembaga/{nomor}',['as'=>'ppkd.skpd.koordinator.edit','uses'=>'Admin\Ppkd\Koordinator\SkpdKoordinatorController@edit']);
    Route::post('ppkd/skpd-koordinator/edit/lembaga/update',['as'=>'ppkd.skpd.koordinator.update','uses'=>'Admin\Ppkd\Koordinator\SkpdKoordinatorController@update']);

    /*
    | Module Manage Token For PPKD
    */
    Route::get('ppkd/pengaturan-token',['as'=>'ppkd.pengaturan.token.manage','uses'=>'Admin\Ppkd\Token\TokenController@manage']);
    Route::get('ppkd/pengaturan-token/create',['as'=>'ppkd.pengaturan.token.create','uses'=>'Admin\Ppkd\Token\TokenController@create']);
    Route::post('ppkd/pengaturan-token/store',['as'=>'ppkd.pengaturan.token.store','uses'=>'Admin\Ppkd\Token\TokenController@store']);
    Route::get('ppkd/pengaturan-token/edit/{nomor}',['as'=>'ppkd.pengaturan.token.edit','uses'=>'Admin\Ppkd\Token\TokenController@edit']);
    Route::delete('ppkd/pengaturan-token/delete',['as'=>'ppkd.pengaturan.token.delete','uses'=>'Admin\Ppkd\Token\TokenController@delete']);
    
    /*
    | Module Monitoring SP2D
    */
    Route::get('ppkd/monitoring',['as'=>'ppkd.monitoring.manage','uses'=>'Admin\Ppkd\Monitoring\MonitoringController@manage']);

    /*
    | Module Lembaga for PPKD
    */
    Route::get('ppkd/lembaga',['as'=>'ppkd.lembaga.manage','uses'=>'Admin\Ppkd\Lembaga\LembagaController@manage']);
    Route::get('ppkd/lembaga/create',['as'=>'ppkd.lembaga.create','uses'=>'Admin\Ppkd\Lembaga\LembagaController@create']);
    Route::post('ppkd/lembaga/store',['as'=>'ppkd.lembaga.store','uses'=>'Admin\Ppkd\Lembaga\LembagaController@store']);
    Route::get('ppkd/lembaga/show/{id}',['as'=>'ppkd.lembaga.show','uses'=>'Admin\Ppkd\Lembaga\LembagaController@show']);
    // Route::get('ppkd/lembaga/edit/{id}',['as'=>'ppkd.lembaga.edit','uses'=>'Admin\Ppkd\Lembaga\LembagaController@edit']);
    Route::post('ppkd/lembaga/update',['as'=>'ppkd.lembaga.update','uses'=>'Admin\Ppkd\Lembaga\LembagaController@update']);

    /*
    | Module Proposal for PPKD
    */
    Route::get('ppkd/proposal/index',['as'=>'ppkd.proposal.index','uses'=>'Admin\Ppkd\Proposal\ProposalController@index']);
    Route::get('ppkd/proposal/{fase}/{noper}',['as'=>'ppkd.proposal.manage','uses'=>'Admin\Ppkd\Proposal\ProposalController@manage']);
    Route::get('ppkd/proposal/edit/fase/{fase}/{id}',['as'=>'ppkd.proposal.edit','uses'=>'Admin\Ppkd\Proposal\ProposalController@edit']);
    Route::post('ppkd/proposal/update',['as'=>'ppkd.proposal.update','uses'=>'Admin\Ppkd\Proposal\ProposalController@update']);
    Route::post('ppkd/proposal/updateProsesAnggaran',['as'=>'ppkd.proposal.updateProsesAnggaran','uses'=>'Admin\Ppkd\Proposal\ProposalController@updateProsesAnggaran']);
    // export to budgeting
    Route::get('ppkd/proposal/{fase}/export-to-budgeting',['as'=>'ppkd.proposal.export.to.budgeting','uses'=>'Admin\Ppkd\Proposal\ProposalController@exportToBudgeting']);
    // Cetak Laporan
    Route::get('ppkd/cetak/show/{no_prop}/{fase}/{tab}',['as'=>'ppkd.cetak.show','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@show']);
        // kelengkapan administrasi
        Route::post('ppkd/cetak/update-kelengkapan-administrasi-lembaga',['as'=>'ppkd.cetak.updateKal','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@updateKal']);
        Route::post('ppkd/cetak/to-pdf-kelengkapan-administrasi',['as'=>'ppkd.cetak.to.pdf.kelengkapan.administrasi','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@generateKelAdministrasiToPDF']);
        // peninjauan lapangan
        Route::post('ppkd/cetak/update-peninjauan-lapangan',['as'=>'ppkd.cetak.updatePL','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@updatePL']);
        Route::post('ppkd/cetak/to-pdf-peninjauan-lapangan',['as'=>'ppkd.cetak.to.pdf.peninjauan.lapangan','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@generatePeninjauanLapanganToPDF']);
        // bentuk rekomendasi
        Route::post('ppkd/cetak/update-bentuk-rekomendasi',['as'=>'ppkd.cetak.updateRekomendasi','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@updateRekomendasi']);
        Route::post('ppkd/cetak/to-pdf-bentuk-rekomendasi',['as'=>'ppkd.cetak.to.pdf.rekomendasi','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@generateRekomendasiToPDF']);
        // cetak naskah perjanjian NPHD
        Route::get('ppkd/cetak/naskah-perjanjian-nphd/generateNPHDToPDF/{id}',['as'=>'ppkd.cetak.naskah.perjanjian.nphd.generateNPHDToPDF','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@generateNPHDToPDF']);
        Route::get('ppkd/cetak/naskah-perjanjian-nphd/buka-fitur-edit/{id}/{fase}/{tab}',['as'=>'ppkd.cetak.naskah.perjanjian.nphd.bukafituredit','uses'=>'Admin\Ppkd\Proposal\Cetak\CetakController@bukaFiturEdit']);
    // cetak naskah perjanjian NPHD
    Route::get('ppkd/proposal/naskah-perjanjian-nphd/show/{no_prop}/{fase}',['as'=>'ppkd.proposal.naskah.perjanjian.nphd.show','uses'=>'Admin\Ppkd\Proposal\ProposalController@showNaskahNPHD']);
    Route::post('ppkd/proposal/naskah-perjanjian-nphd/update',['as'=>'ppkd.proposal.cetak.naskah.perjanjian.nphd.update','uses'=>'Admin\Ppkd\Proposal\ProposalController@updateNaskahNPHD']);
    //Checklist verifikasi berkas pencairan oleh skpd/ukpd
    Route::get('ppkd/proposal/verifikasi-berkas-pencairan/{fase}/{no_prop}',['as'=>'ppkd.proposal.verifikasi.berkas.pencairan','uses'=>'Admin\Ppkd\Proposal\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@show']);
    Route::post('ppkd/proposal/verifikasi-berkas-pencairan/store',['as'=>'ppkd.proposal.verifikasi.berkas.pencairan.store','uses'=>'Admin\Ppkd\Proposal\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@store']);
    Route::post('ppkd/proposal/verifikasi-berkas-pencairan/generateToPDF',['as'=>'ppkd.proposal.verifikasi.berkas.pencairan.generateToPDF','uses'=>'Admin\Ppkd\Proposal\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@generateToPDF']);
    Route::post('ppkd/proposal/verifikasi-berkas-pencairan/uncheck',['as'=>'ppkd.proposal.verifikasi.berkas.pencairan.uncheck','uses'=>'Admin\Ppkd\Proposal\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@uncheck']);

    /*
    | Module Aktifkan Proposal Definitif for PPKD
    */
    Route::put('ppkd/migrate/proposal-to-proposal-definitif/{noper}',['as'=>'ppkd.migrate.proposal.to.definitif','uses'=>'Admin\Ppkd\Proposal\ProposalToDefinitifController@migrate']);

    Route::get('ppkd/migrate/update-uuid',['as'=>'ppkd.migrate.update.uuid','uses'=>'Admin\Ppkd\Proposal\ProposalToDefinitifController@updateUUID']);

    /*
    | Module Download Rekap for PPKD
    */
    Route::get('ppkd/download-rekap/index',['as'=>'ppkd.download.rekap.index','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@index']);
    Route::get('ppkd/download-rekap/{fase}/{noper}',['as'=>'ppkd.download.rekap.manage','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@manage']);
    
    Route::get('ppkd/download-rekap/excel/lampiran-sk-gubernur/{noper}',['as'=>'ppkd.download.rekap.lampiran.sk.gubernur','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@lampiranSKGubernur']);
    
    Route::get('ppkd/download-rekap/excel/laporan-realisasi-anggaran-hibahbansos/{noper}',['as'=>'ppkd.download.rekap.laporan.realisasi.anggaran.hibahbansos','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@laporanRealisasiAnggaranHibahBansos']);

    Route::get('ppkd/download-rekap/excel/laporan-realisasi-perskpd/{noper}',['as'=>'ppkd.download.rekap.laporan.realisasi.perskpd','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@laporanRealisasiPerSKPD']);

    Route::get('ppkd/download-rekap/excel/laporan-lembaga-yang-tidak-merealisasikan-hibahbansos/{noper}',['as'=>'ppkd.download.rekap.laporan.lembaga.not.realisasi','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@laporanLembagaNotRealisasi']);

    Route::get('ppkd/download-rekap/excel/laporan-lembaga-lpj/{noper}',['as'=>'ppkd.download.rekap.laporan.lembaga.lpj','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@laporanLembagaLPJ']);

    Route::get('ppkd/download-rekap/excel/rekap-jumlah-lembaga/{noper}',['as'=>'ppkd.download.rekap.jumlah.lembaga','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@rekapJumlahLembaga']);

    Route::get('ppkd/download-rekap/excel/rekap-perkode-rekening/{noper}',['as'=>'ppkd.download.rekap.perkode.rekening','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@rekapPerKodeRekening']);

    Route::get('ppkd/download-rekap/excel/rekap-lembaga-usulan-tolak/{noper}',['as'=>'ppkd.download.rekap.lembaga.usulan.tolak','uses'=>'Admin\Ppkd\DownloadRekap\DownloadRekapController@rekapLembagaUsulanTolak']);

    /*
    | Module Pengaturan Running Text
    */
    Route::get('ppkd/running-text',['as'=>'ppkd.running.text.manage','uses'=>'Admin\Ppkd\RunningText\RunningTextController@manage']);
    Route::get('ppkd/running-text/create',['as'=>'ppkd.running.text.create','uses'=>'Admin\Ppkd\RunningText\RunningTextController@create']);
    Route::post('ppkd/running-text/store',['as'=>'ppkd.running.text.store','uses'=>'Admin\Ppkd\RunningText\RunningTextController@store']);
    Route::get('ppkd/running-text/edit/{id}',['as'=>'ppkd.running.text.edit','uses'=>'Admin\Ppkd\RunningText\RunningTextController@edit']);
    Route::post('ppkd/running-text/update',['as'=>'ppkd.running.text.update','uses'=>'Admin\Ppkd\RunningText\RunningTextController@update']);

    Route::delete('ppkd/running-text/delete',['as'=>'ppkd.running.text.delete','uses'=>'Admin\Ppkd\RunningText\RunningTextController@delete']);

    /*
    | Module Pengaturan Info Terkini
    */
    Route::get('ppkd/info-terkini',['as'=>'ppkd.info.terkini.manage','uses'=>'Admin\Ppkd\InfoTerkini\InfoTerkiniController@manage']);
    Route::get('ppkd/info-terkini/create',['as'=>'ppkd.info.terkini.create','uses'=>'Admin\Ppkd\InfoTerkini\InfoTerkiniController@create']);
    Route::post('ppkd/info-terkini/store',['as'=>'ppkd.info.terkini.store','uses'=>'Admin\Ppkd\InfoTerkini\InfoTerkiniController@store']);
    Route::get('ppkd/info-terkini/edit/{id}',['as'=>'ppkd.info.terkini.edit','uses'=>'Admin\Ppkd\InfoTerkini\InfoTerkiniController@edit']);
    Route::post('ppkd/info-terkini/update',['as'=>'ppkd.info.terkini.update','uses'=>'Admin\Ppkd\InfoTerkini\InfoTerkiniController@update']);

    Route::delete('ppkd/info-terkini/delete',['as'=>'ppkd.info.terkini.delete','uses'=>'Admin\Ppkd\InfoTerkini\InfoTerkiniController@delete']);

    /*
    | Module Pengaturan Pengumuman
    */
    Route::get('ppkd/pengumuman',['as'=>'ppkd.pengumuman.manage','uses'=>'Admin\Ppkd\Pengumuman\PengumumanController@manage']);
    Route::get('ppkd/pengumuman/create',['as'=>'ppkd.pengumuman.create','uses'=>'Admin\Ppkd\Pengumuman\PengumumanController@create']);
    Route::post('ppkd/pengumuman/store',['as'=>'ppkd.pengumuman.store','uses'=>'Admin\Ppkd\Pengumuman\PengumumanController@store']);
    Route::get('ppkd/pengumuman/edit/{id}',['as'=>'ppkd.pengumuman.edit','uses'=>'Admin\Ppkd\Pengumuman\PengumumanController@edit']);
    Route::post('ppkd/pengumuman/update',['as'=>'ppkd.pengumuman.update','uses'=>'Admin\Ppkd\Pengumuman\PengumumanController@update']);

    Route::delete('ppkd/pengumuman/delete',['as'=>'ppkd.pengumuman.delete','uses'=>'Admin\Ppkd\Pengumuman\PengumumanController@delete']);

    /*
    | Module Pengaturan Testimonial
    */
    Route::get('ppkd/testimonial',['as'=>'ppkd.testimonial.manage','uses'=>'Admin\Ppkd\Testimonial\TestimonialController@manage']);
    Route::get('ppkd/testimonial/create',['as'=>'ppkd.testimonial.create','uses'=>'Admin\Ppkd\Testimonial\TestimonialController@create']);
    Route::post('ppkd/testimonial/store',['as'=>'ppkd.testimonial.store','uses'=>'Admin\Ppkd\Testimonial\TestimonialController@store']);
    Route::get('ppkd/testimonial/edit/{id}',['as'=>'ppkd.testimonial.edit','uses'=>'Admin\Ppkd\Testimonial\TestimonialController@edit']);
    Route::post('ppkd/testimonial/update',['as'=>'ppkd.testimonial.update','uses'=>'Admin\Ppkd\Testimonial\TestimonialController@update']);

    Route::delete('ppkd/testimonial/delete',['as'=>'ppkd.testimonial.delete','uses'=>'Admin\Ppkd\Testimonial\TestimonialController@delete']);

    /*
    | Module Pengaturan FaQ
    */
    Route::get('ppkd/faq',['as'=>'ppkd.faq.manage','uses'=>'Admin\Ppkd\FAQ\FAQController@manage']);
    Route::get('ppkd/faq/create',['as'=>'ppkd.faq.create','uses'=>'Admin\Ppkd\FAQ\FAQController@create']);
    Route::post('ppkd/faq/store',['as'=>'ppkd.faq.store','uses'=>'Admin\Ppkd\FAQ\FAQController@store']);
    Route::get('ppkd/faq/edit/{id}',['as'=>'ppkd.faq.edit','uses'=>'Admin\Ppkd\FAQ\FAQController@edit']);
    Route::post('ppkd/faq/update',['as'=>'ppkd.faq.update','uses'=>'Admin\Ppkd\FAQ\FAQController@update']);

    // Cetak Laporan Penebalan
    Route::get('ppkd/cetak-penebalan/show/{uuid}/{no_prop}/{fase}/{tahap}/{tab}',['as'=>'ppkd.cetak.penebalan.show','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@show']);
    // kelengkapan administrasi
        Route::post('ppkd/cetak-penebalan/update-kelengkapan-administrasi-lembaga-penebalan',['as'=>'ppkd.cetak.penebalan.updateKal','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@updateKal']);
        Route::post('ppkd/cetak-penebalan/to-pdf-kelengkapan-administrasi-penebalan',['as'=>'ppkd.cetak.penebalan.to.pdf.kelengkapan.administrasi','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@generateKelAdministrasiToPDF']);
    // peninjauan lapangan
        Route::post('ppkd/cetak-penebalan/update-peninjauan-lapangan-penebalan',['as'=>'ppkd.cetak.penebalan.updatePL','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@updatePL']);
        Route::post('ppkd/cetak-penebalan/to-pdf-peninjauan-lapangan-penebalan',['as'=>'ppkd.cetak.penebalan.to.pdf.peninjauan.lapangan','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@generatePeninjauanLapanganToPDF']);
    // bentuk rekomendasi
        Route::post('ppkd/cetak-penebalan/update-bentuk-rekomendasi-penebalan',['as'=>'ppkd.cetak.penebalan.updateRekomendasi','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@updateRekomendasi']);
        Route::post('ppkd/cetak-penebalan/to-pdf-bentuk-rekomendasi-penebalan',['as'=>'ppkd.cetak.penebalan.to.pdf.rekomendasi','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@generateRekomendasiToPDF']);

    // bentuk rekomendasi pencairan
    // Route::get('skpd/cetak-penebalan/tambah-rekomendasi-pencairan-penebalan/{no_prop}/{uuid}/{fase}/{tahap}/{tab}',['as'=>'skpd.cetak.penebalan.tambahRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@tambahRekomendasiPencairan']);
    // Route::post('skpd/cetak-penebalan/store-bentuk-rekomendasi-pencairan-penebalan',['as'=>'skpd.cetak.penebalan.storeRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@storeRekomendasiPencairan']);
    // Route::get('skpd/cetak-penebalan/edit-rekomendasi-pencairan/{no_prop}/{uuid}/{fase}/{tahap}/{tab}/{id}',['as'=>'skpd.cetak.penebalan.editRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@editRekomendasiPencairan']);
    // Route::post('skpd/cetak/update-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.updateRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@updateRekomendasiPencairan']);
    // Route::post('skpd/cetak/to-pdf-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.to.pdf.rekomendasi.pencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generateRekomendasiPencairanToPDF']);
    
    // nphd
    Route::get('ppkd/cetak-penebalan/naskah-perjanjian-nphd-penebalan/{uuid}/{fase}/{tahap}',['as'=>'ppkd.cetak.penebalan.naskah.perjanjian.nphd','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@generateNPHDToPDF']);

    Route::get('ppkd/cetak-penebalan/naskah-perjanjian-nphd/buka-fitur-edit/{uuid}/{no_prop}/{fase}/{tahap}/{tab}',['as'=>'ppkd.cetak.penebalan.naskah.perjanjian.nphd.bukafituredit','uses'=>'Admin\Ppkd\Proposal\Cetak\Penebalan\CetakController@bukaFiturEdit']);
    

    /**
    * * * * * * * * * * *
    * MODULE GROUP SKPD 
    * * * * * * * * * * *
    **/

    /*
    | Module Users Management for SKPD
    */
    Route::get('skpd/user-management',['as'=>'skpd.user.management.manage','uses'=>'Admin\Skpd\User\UserController@manage']);
    Route::get('skpd/user-management/show/{userID}',['as'=>'skpd.user.management.show','uses'=>'Admin\Skpd\User\UserController@show']);
    Route::get('skpd/user-management/create',['as'=>'skpd.user.management.create','uses'=>'Admin\Skpd\User\UserController@create']);
    Route::post('skpd/user-management/store',['as'=>'skpd.user.management.store','uses'=>'Admin\Skpd\User\UserController@store']);
    Route::get('skpd/user-management/edit/{userID}',['as'=>'skpd.user.management.edit','uses'=>'Admin\Skpd\User\UserController@edit']);
    Route::post('skpd/user-management/update',['as'=>'skpd.user.management.update','uses'=>'Admin\Skpd\User\UserController@update']);

    Route::post('skpd/user-management/change-status',['as'=>'skpd.user.management.change.status','uses'=>'Admin\Skpd\User\UserController@changeStatus']);

    Route::delete('skpd/user-management/delete/user',['as'=>'skpd.user.management.delete.user','uses'=>'Admin\Skpd\User\UserController@delete']);

    Route::get('ajax/alamat-lembaga/{nomor}',['as'=>'ajax.alamat.lembaga',function($nomor){
        $strQuery = BPKAD\Http\Entities\Lembaga::where('nomor',$_GET['nomor'])->first();

        return $strQuery->alamat;
    }]);

    /*
    | Module Profil SKPD
    */
    Route::get('skpd/profil',['as'=>'skpd.profil','uses'=>'Admin\Skpd\User\UserController@profilSKPD']);
    Route::post('skpd/profil/update',['as'=>'skpd.profil.update','uses'=>'Admin\Skpd\User\UserController@updateProfilSKPD']);


    /*
    | Module Lembaga for SKPD
    */
    Route::get('skpd/lembaga',['as'=>'skpd.lembaga.manage','uses'=>'Admin\Skpd\Lembaga\LembagaController@manage']);
    Route::get('skpd/lembaga/show/{noLembaga}',['as'=>'skpd.lembaga.show','uses'=>'Admin\Skpd\Lembaga\LembagaController@show']);
    Route::get('skpd/lembaga/create',['as'=>'skpd.lembaga.create','uses'=>'Admin\Skpd\Lembaga\LembagaController@create']);
    Route::post('skpd/lembaga/store',['as'=>'skpd.lembaga.store','uses'=>'Admin\Skpd\Lembaga\LembagaController@store']);
    Route::get('skpd/lembaga/edit/{id}',['as'=>'skpd.lembaga.edit','uses'=>'Admin\Skpd\Lembaga\LembagaController@edit']);
    Route::post('skpd/lembaga/update',['as'=>'skpd.lembaga.update','uses'=>'Admin\Skpd\Lembaga\LembagaController@update']);

    //Checklist kesesuaian data antara dokumen tertulis dan soft copy:
        Route::post('skpd/lembaga/checklist-data-administrasi',['as'=>'skpd.lembaga.checklist.data.administrasi','uses'=>'Admin\Skpd\Lembaga\LembagaController@checklistDataAdministrasi']);
        Route::post('skpd/lembaga/checklist-data-lapangan',['as'=>'skpd.lembaga.checklist.data.lapangan','uses'=>'Admin\Skpd\Lembaga\LembagaController@checklistDataLapangan']);

        Route::get('skpd/lembaga/rekapitulasi-checklist',['as'=>'skpd.lembaga.rekapitulasi.checklist','uses'=>'Admin\Skpd\Lembaga\LembagaController@rekapitulasiChecklist']);

        Route::get('skpd/lembaga/download-rekapitulasi-checklist',['as'=>'skpd.lembaga.download.rekapitulasi.checklist','uses'=>'Admin\Skpd\Lembaga\LembagaController@downloadRekapitulasiChecklist']);


    /*
    | Module Proposal for SKPD
    */
    Route::get('skpd/proposal/index',['as'=>'skpd.proposal.index','uses'=>'Admin\Skpd\Proposal\ProposalController@index']);

    Route::get('skpd/proposal/{fase}/{noper}',['as'=>'skpd.proposal.manage','uses'=>'Admin\Skpd\Proposal\ProposalController@manage']);
    Route::get('skpd/proposal/show/{id}/{fase}',['as'=>'skpd.proposal.show','uses'=>'Admin\Skpd\Proposal\ProposalController@show']);
    // Route::get('skpd/proposal/only-show/{id}/{fase}',['as'=>'skpd.proposal.only.show','uses'=>'Admin\Skpd\Proposal\ProposalController@onlyShow']);
    Route::post('skpd/proposal/update',['as'=>'skpd.proposal.update','uses'=>'Admin\Skpd\Proposal\ProposalController@update']);
    Route::post('skpd/proposal/updateTAPD',['as'=>'skpd.proposal.updateTAPD','uses'=>'Admin\Skpd\Proposal\ProposalController@updateTAPD']);
    Route::post('skpd/proposal/updateBANGGAR',['as'=>'skpd.proposal.updateBANGGAR','uses'=>'Admin\Skpd\Proposal\ProposalController@updateBANGGAR']);
    Route::post('skpd/proposal/updateDDN',['as'=>'skpd.proposal.updateDDN','uses'=>'Admin\Skpd\Proposal\ProposalController@updateDDN']);
    Route::post('skpd/proposal/updateAPBD',['as'=>'skpd.proposal.updateAPBD','uses'=>'Admin\Skpd\Proposal\ProposalController@updateAPBD']);

    Route::post('skpd/proposal/updateProsesAnggaran',['as'=>'skpd.proposal.updateProsesAnggaran','uses'=>'Admin\Skpd\Proposal\ProposalController@updateProsesAnggaran']);

        // download daftar penerima hibah
        Route::post('skpd/proposal/download-daftar-penerima-hibah',['as'=>'skpd.proposal.download.daftar.penerima.hibah','uses'=>'Admin\Skpd\Proposal\ProposalController@downloadPenerimaHibah']);
    
    //Checklist kesesuaian data antara dokumen tertulis dan soft copy:
        Route::post('skpd/proposal/checklist-data-administrasi',['as'=>'skpd.proposal.checklist.data.administrasi','uses'=>'Admin\Skpd\Proposal\ProposalController@checklistDataAdministrasi']);
        Route::post('skpd/proposal/checklist-data-lapangan',['as'=>'skpd.proposal.checklist.data.lapangan','uses'=>'Admin\Skpd\Proposal\ProposalController@checklistDataLapangan']);

    // Cetak Laporan
    Route::get('skpd/cetak/show/{no_prop}/{fase}/{tab}',['as'=>'skpd.cetak.show','uses'=>'Admin\Skpd\Cetak\CetakController@show']);
        // kelengkapan administrasi
        Route::post('skpd/cetak/update-kelengkapan-administrasi-lembaga',['as'=>'skpd.cetak.updateKal','uses'=>'Admin\Skpd\Cetak\CetakController@updateKal']);
        Route::post('skpd/cetak/to-pdf-kelengkapan-administrasi',['as'=>'skpd.cetak.to.pdf.kelengkapan.administrasi','uses'=>'Admin\Skpd\Cetak\CetakController@generateKelAdministrasiToPDF']);
        Route::get('skpd/cetak/delete-detail-peserta-kelengkapan-administrasi/{id}/{no_prop}/{fase}/{tab}',['as'=>'skpd.cetak.delete.detail.peserta.kelengkapan.administrasi','uses'=>'Admin\Skpd\Cetak\CetakController@deleteDcka']);
        // peninjauan lapangan
        Route::post('skpd/cetak/update-peninjauan-lapangan',['as'=>'skpd.cetak.updatePL','uses'=>'Admin\Skpd\Cetak\CetakController@updatePL']);
        Route::post('skpd/cetak/to-pdf-peninjauan-lapangan',['as'=>'skpd.cetak.to.pdf.peninjauan.lapangan','uses'=>'Admin\Skpd\Cetak\CetakController@generatePeninjauanLapanganToPDF']);
        Route::get('skpd/cetak/delete-detail-peserta-peninjauan-lapangan/{id}/{no_prop}/{fase}/{tab}',['as'=>'skpd.cetak.delete.detail.peserta.peninjauan.lapangan','uses'=>'Admin\Skpd\Cetak\CetakController@deleteDcpl']);
        // bentuk rekomendasi
        Route::post('skpd/cetak/update-bentuk-rekomendasi',['as'=>'skpd.cetak.updateRekomendasi','uses'=>'Admin\Skpd\Cetak\CetakController@updateRekomendasi']);
        Route::post('skpd/cetak/to-pdf-bentuk-rekomendasi',['as'=>'skpd.cetak.to.pdf.rekomendasi','uses'=>'Admin\Skpd\Cetak\CetakController@generateRekomendasiToPDF']);
        // bentuk rekomendasi pencairan
        // Route::get('skpd/cetak/tambah-rekomendasi-pencairan/{no_prop}/{uuid_prop}/{fase}/{tab}',['as'=>'skpd.cetak.tambahRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\CetakController@tambahRekomendasiPencairan']);
        // Route::post('skpd/cetak/store-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.storeRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\CetakController@storeRekomendasiPencairan']);
        // Route::get('skpd/cetak/edit-rekomendasi-pencairan/{no_prop}/{uuid_prop}/{fase}/{tab}/{id}',['as'=>'skpd.cetak.editRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\CetakController@editRekomendasiPencairan']);
        // Route::post('skpd/cetak/update-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.updateRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\CetakController@updateRekomendasiPencairan']);
        // Route::post('skpd/cetak/to-pdf-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.to.pdf.rekomendasi.pencairan','uses'=>'Admin\Skpd\Cetak\CetakController@generateRekomendasiPencairanToPDF']);

        // NPHD
        Route::get('skpd/cetak/naskah-perjanjian-nphd/{id}',['as'=>'skpd.cetak.naskah.perjanjian.nphd','uses'=>'Admin\Skpd\Cetak\CetakController@generateNPHDToPDF']);

    //Checklist verifikasi berkas pencairan oleh skpd/ukpd
    Route::get('skpd/proposal/verifikasi-berkas-pencairan/{fase}/{no_prop}',['as'=>'skpd.proposal.verifikasi.berkas.pencairan','uses'=>'Admin\Skpd\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@show']);
        Route::post('skpd/proposal/verifikasi-berkas-pencairan/store',['as'=>'skpd.proposal.verifikasi.berkas.pencairan.store','uses'=>'Admin\Skpd\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@store']);

        Route::post('skpd/proposal/verifikasi-berkas-pencairan/generateToPDF',['as'=>'skpd.proposal.verifikasi.berkas.pencairan.generateToPDF','uses'=>'Admin\Skpd\VerifikasiBerkasPencairan\VerifikasiBerkasPencairanController@generateToPDF']);


    /*
    | Module Administrasi for SKPD
    */
    //Tim Peneliti
    Route::get('skpd/tim-peneliti',['as'=>'skpd.tim.peneliti.manage','uses'=>'Admin\Skpd\TimPeneliti\TimPenelitiController@manage']);
    Route::get('skpd/tim-peneliti/show/{id}',['as'=>'skpd.tim.peneliti.show','uses'=>'Admin\Skpd\TimPeneliti\TimPenelitiController@show']);
    Route::post('skpd/tim-peneliti/store',['as'=>'skpd.tim.peneliti.store','uses'=>'Admin\Skpd\TimPeneliti\TimPenelitiController@store']);

    //Checklist kesesuaian data antara dokumen tertulis dan soft copy Penebalan
    Route::post('skpd/proposal-penebalan/checklist-data-administrasi-penebalan',['as'=>'skpd.proposal.penebalan.checklist.data.administrasi','uses'=>'Admin\Skpd\Proposal\ProposalController@checklistDataAdministrasiPenebalan']);
    Route::post('skpd/proposal-penebalan/checklist-data-lapangan-penebalan',['as'=>'skpd.proposal.penebalan.checklist.data.lapangan','uses'=>'Admin\Skpd\Proposal\ProposalController@checklistDataLapanganPenebalan']);

    // Cetak Laporan Penebalan
    Route::get('skpd/cetak-penebalan/show/{uuid}/{no_prop}/{fase}/{tahap}/{tab}',['as'=>'skpd.cetak.penebalan.show','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@show']);
    // kelengkapan administrasi
        Route::post('skpd/cetak-penebalan/update-kelengkapan-administrasi-lembaga-penebalan',['as'=>'skpd.cetak.penebalan.updateKal','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@updateKal']);
        Route::post('skpd/cetak-penebalan/to-pdf-kelengkapan-administrasi-penebalan',['as'=>'skpd.cetak.penebalan.to.pdf.kelengkapan.administrasi','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generateKelAdministrasiToPDF']);
        Route::get('skpd/cetak-penebalan/delete-detail-peserta-kelengkapan-administrasi-penebalan/{id}/{uuid}/{no_prop}/{fase}/{tahap}/{tab}',['as'=>'skpd.cetak.penebalan.delete.detail.peserta.kelengkapan.administrasi','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@deleteDcka']);
    // peninjauan lapangan
        Route::post('skpd/cetak-penebalan/update-peninjauan-lapangan-penebalan',['as'=>'skpd.cetak.penebalan.updatePL','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@updatePL']);
        Route::post('skpd/cetak-penebalan/to-pdf-peninjauan-lapangan-penebalan',['as'=>'skpd.cetak.penebalan.to.pdf.peninjauan.lapangan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generatePeninjauanLapanganToPDF']);
        Route::get('skpd/cetak-penebalan/delete-detail-peserta-peninjauan-lapangan-penebalan/{id}/{uuid}/{no_prop}/{fase}/{tahap}/{tab}',['as'=>'skpd.cetak.penebalan.delete.detail.peserta.peninjauan.lapangan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@deleteDcpl']);
    // bentuk rekomendasi
        Route::post('skpd/cetak-penebalan/update-bentuk-rekomendasi-penebalan',['as'=>'skpd.cetak.penebalan.updateRekomendasi','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@updateRekomendasi']);
        Route::post('skpd/cetak-penebalan/to-pdf-bentuk-rekomendasi-penebalan',['as'=>'skpd.cetak.penebalan.to.pdf.rekomendasi','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generateRekomendasiToPDF']);
    // NPHD
        Route::get('skpd/cetak-penebalan/naskah-perjanjian-nphd-penebalan/{uuid}/{fase}/{tahap}',['as'=>'skpd.cetak.penebalan.naskah.perjanjian.nphd','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generateNPHDToPDF']);
        Route::get('skpd/cetak-penebalan/naskah-perjanjian-nphd/buka-fitur-edit/{uuid}/{no_prop}/{fase}/{tahap}/{tab}',['as'=>'skpd.cetak.penebalan.naskah.perjanjian.nphd.bukafituredit','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@bukaFiturEdit']);
    // Pakta Integritas
        Route::get('skpd/cetak-penebalan/pakta-integritas/{uuid}/{fase}/{tahap}',['as'=>'skpd.cetak.penebalan.pakta.integritas','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generatePaktaIntegritasToPDF']);

    // bentuk rekomendasi pencairan
    Route::get('skpd/cetak-penebalan/tambah-rekomendasi-pencairan-penebalan/{no_prop}/{uuid}/{fase}/{tahap}/{tab}',['as'=>'skpd.cetak.penebalan.tambahRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@tambahRekomendasiPencairan']);
    Route::post('skpd/cetak-penebalan/store-bentuk-rekomendasi-pencairan-penebalan',['as'=>'skpd.cetak.penebalan.storeRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@storeRekomendasiPencairan']);
    Route::get('skpd/cetak-penebalan/edit-rekomendasi-pencairan/{no_prop}/{uuid}/{fase}/{tahap}/{tab}/{id}',['as'=>'skpd.cetak.penebalan.editRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@editRekomendasiPencairan']);
    Route::post('skpd/cetak/update-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.updateRekomendasiPencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@updateRekomendasiPencairan']);
    Route::post('skpd/cetak/to-pdf-bentuk-rekomendasi-pencairan',['as'=>'skpd.cetak.to.pdf.rekomendasi.pencairan','uses'=>'Admin\Skpd\Cetak\Penebalan\CetakController@generateRekomendasiPencairanToPDF']);
    
    /**
    * * * * * * * * * * *
    * MODULE GROUP Member 
    * * * * * * * * * * *
    **/

    /*
    | Module Lembaga for member
    */
    Route::get('member/lembaga/manage',['as'=>'member.lembaga.manage', 'uses'=>'Admin\Member\Lembaga\LembagaController@manage']);
    Route::get('member/lembaga/edit/{nomor}',['as'=>'member.lembaga.edit', 'uses'=>'Admin\Member\Lembaga\LembagaController@edit']);
    Route::post('member/lembaga/update',['as'=>'member.lembaga.update', 'uses'=>'Admin\Member\Lembaga\LembagaController@update']);

    Route::get('member/lembaga/upload-foto-lembaga',['as'=>'member.lembaga.foto.lembaga', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadFotoLembaga']); 
    Route::get('member/lembaga/upload-ktp-ketua',['as'=>'member.lembaga.ktp.ketua', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadKtp']); 
    Route::get('member/lembaga/upload-foto-ketua',['as'=>'member.lembaga.foto.ketua', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadFotoKetua']);
    Route::get('member/lembaga/upload-akta',['as'=>'member.lembaga.akta', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadAkta']);
    Route::get('member/lembaga/upload-npwp',['as'=>'member.lembaga.npwp', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadNpwp']);
    Route::get('member/lembaga/upload-skd',['as'=>'member.lembaga.skd', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadSkd']);
    Route::get('member/lembaga/upload-izin-operasional',['as'=>'member.lembaga.izin.operasional', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadIzinOperasional']);
    Route::get('member/lembaga/upload-sertifikat',['as'=>'member.lembaga.sertifikat', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadSertifikat']);
    Route::get('member/lembaga/upload-rekening',['as'=>'member.lembaga.rekening', 'uses'=>'Admin\Member\Lembaga\LembagaController@uploadRekening']);

    Route::get('member/lembaga/lokasi/edit/{nomor}',['as'=>'member.lembaga.lokasi.edit', 'uses'=>'Admin\Member\Lembaga\LembagaController@editLokasi']);
    Route::post('member/lembaga/lokasi/update',['as'=>'member.lembaga.lokasi.update', 'uses'=>'Admin\Member\Lembaga\LembagaController@updateLokasi']);

    Route::post('member/lembaga/store',['as'=>'member.lembaga.store', 'uses'=>'Admin\Member\Lembaga\LembagaController@store']);

    /*
    | Module Proposal for member
    */
    Route::get('member/proposal/index',['as'=>'member.proposal.index', 'uses'=>'Admin\Member\Proposal\ProposalController@index']);
   
    Route::get('member/proposal/manage/{fase}/{noper}',['as'=>'member.proposal.manage', 'uses'=>'Admin\Member\Proposal\ProposalController@manage']);
    Route::get('member/proposal/show/{id}/{fase}',['as'=>'member.proposal.show', 'uses'=>'Admin\Member\Proposal\ProposalController@show']);
    Route::get('member/proposal/create/{fase}/{noper}',['as'=>'member.proposal.create', 'uses'=>'Admin\Member\Proposal\ProposalController@create']);
    Route::post('member/proposal/store',['as'=>'member.proposal.store', 'uses'=>'Admin\Member\Proposal\ProposalController@store']);
    Route::get('member/proposal/edit/{id}/{fase}/{status?}',['as'=>'member.proposal.edit', 'uses'=>'Admin\Member\Proposal\ProposalController@edit']);
    Route::post('member/proposal/update',['as'=>'member.proposal.update', 'uses'=>'Admin\Member\Proposal\ProposalController@update']);
    
    Route::post('member/proposal/setujui-kirim',['as'=>'member.proposal.setujui.kirim', 'uses'=>'Admin\Member\Proposal\ProposalController@setujuiKirim']);

    Route::delete('member/proposal/delete',['as'=>'member.proposal.delete', 'uses'=>'Admin\Member\Proposal\ProposalController@delete']);
    Route::get('member/proposal/resend/{no_prop}/{email}/{real_name}/{email_lembaga}/{no_lembaga}/{tahun_anggaran}/{fase}',['as'=>'member.proposal.resendEmail', 'uses'=>'Admin\Member\Proposal\ProposalController@resendEmail']);

        //route rencana
        Route::get('member/proposal/create-rencana/{no_prop}/{fase}',['as'=>'member.proposal.create.rencana', 'uses'=>'Admin\Member\Proposal\ProposalController@createRencana']);
        Route::post('member/proposal/create-rencana/store',['as'=>'member.proposal.store.rencana', 'uses'=>'Admin\Member\Proposal\ProposalController@storeRencana']);
        Route::get('member/proposal/edit-rencana/{id}/{fase}',['as'=>'member.proposal.edit.rencana', 'uses'=>'Admin\Member\Proposal\ProposalController@editRencana']);
        Route::post('member/proposal/edit-rencana/update',['as'=>'member.proposal.update.rencana', 'uses'=>'Admin\Member\Proposal\ProposalController@updateRencana']);
        Route::delete('member/proposal/hapus-rencana/delete',['as'=>'member.proposal.delete.rencana', 'uses'=>'Admin\Member\Proposal\ProposalController@deleteRencana']);
            
            //route rincian
            Route::get('member/proposal/create-rincian/{id}/{no_prop}/{fase}',['as'=>'member.proposal.create.rincian', 'uses'=>'Admin\Member\Proposal\ProposalController@createRincian']);
            Route::post('member/proposal/create-rincian/store',['as'=>'member.proposal.store.rincian', 'uses'=>'Admin\Member\Proposal\ProposalController@storeRincian']);
            Route::get('member/proposal/edit-rincian/{id}/{no_prop}/{fase}',['as'=>'member.proposal.edit.rincian', 'uses'=>'Admin\Member\Proposal\ProposalController@editRincian']);
            Route::post('member/proposal/edit-rincian/update',['as'=>'member.proposal.update.rincian', 'uses'=>'Admin\Member\Proposal\ProposalController@updateRincian']);
            Route::delete('member/proposal/hapus-rincian/delete',['as'=>'member.proposal.delete.rincian', 'uses'=>'Admin\Member\Proposal\ProposalController@deleteRincian']);

        // route sementara
        Route::get('member/proposal/perbaikan/{id}',['as'=>'member.proposal.perbaikan', 'uses'=>'Admin\Member\Proposal\ProposalController@perbaikan']);
        Route::post('member/proposal/perbaikan/update',['as'=>'member.proposal.perbaikan.update', 'uses'=>'Admin\Member\Proposal\ProposalController@updatePerbaikan']);

    // Route cetak NPHD
    Route::get('member/proposal/naskah-perjanjian-nphd/show/{no_prop}/{fase}',['as'=>'member.proposal.naskah.perjanjian.nphd.show','uses'=>'Admin\Member\Proposal\ProposalController@showNaskahNPHD']);
    Route::post('member/proposal/naskah-perjanjian-nphd/update',['as'=>'member.proposal.naskah.perjanjian.nphd.update','uses'=>'Admin\Member\Proposal\ProposalController@updateNaskahNPHD']);
    
    Route::get('member/proposal/cetak-perjanjian-nphd/{id}',['as'=>'member.proposal.cetak.perjanjian.nphd', 'uses'=>'Admin\Member\Proposal\ProposalController@generateNPHDToPDF']);

    /*
    | Module Proposal Penebalan/Definitif for member
    */
    Route::get('member/proposal-penebalan/create/{fase}/{noper}/{uuid}/{tahap}',['as'=>'member.proposal.penebalan.create', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@create']);
    Route::post('member/proposal-penebalan/store',['as'=>'member.proposal.penebalan.store', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@store']);
    Route::get('member/proposal-penebalan/edit/{fase}/{noper}/{uuid}/{tahap}/{status?}',['as'=>'member.proposal.penebalan.edit', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@edit']);
    Route::post('member/proposal-penebalan/update',['as'=>'member.proposal.penebalan.update', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@update']);

    Route::post('member/proposal-penebalan/setujui-kirim',['as'=>'member.proposal.penebalan.setujui.kirim', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@setujuiKirim']);

    Route::get('member/proposal-penebalan/resend/{no_prop}/{email}/{real_name}/{email_lembaga}/{no_lembaga}/{tahun_anggaran}/{fase}/{tahap}',['as'=>'member.proposal.penebalan.resendEmail', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@resendEmail']);

        //route rencana
        Route::get('member/proposal-penebalan/create-rencana/{fase}/{noper}/{uuid}/{tahap}/{status?}',['as'=>'member.proposal.penebalan.create.rencana', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@createRencana']);
        Route::post('member/proposal-penebalan/create-rencana/store',['as'=>'member.proposal.penebalan.store.rencana', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@storeRencana']);
        Route::get('member/proposal-penebalan/edit-rencana/{id}/{fase}/{noper}/{uuid}/{tahap}/{status?}',['as'=>'member.proposal.penebalan.edit.rencana', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@editRencana']);
        Route::post('member/proposal-penebalan/edit-rencana/update',['as'=>'member.proposal.penebalan.update.rencana', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@updateRencana']);
        Route::delete('member/proposal-penebalan/hapus-rencana/delete',['as'=>'member.proposal.penebalan.delete.rencana', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@deleteRencana']);
            
            //route rincian
            Route::get('member/proposal-penebalan/create-rincian/{id}/{fase}/{noper}/{uuid}/{tahap}/{status?}',['as'=>'member.proposal.penebalan.create.rincian', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@createRincian']);
            Route::post('member/proposal-penebalan/create-rincian/store',['as'=>'member.proposal.penebalan.store.rincian', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@storeRincian']);
            Route::get('member/proposal-penebalan/edit-rincian/{id}/{fase}/{noper}/{uuid}/{tahap}/{status?}',['as'=>'member.proposal.penebalan.edit.rincian', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@editRincian']);
            Route::post('member/proposal-penebalan/update-rincian',['as'=>'member.proposal.penebalan.update.rincian', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@updateRincian']);
            Route::delete('member/proposal-penebalan/delete-rincian',['as'=>'member.proposal.penebalan.delete.rincian', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@deleteRincian']);

    // Route cetak NPHD
    Route::get('member/proposal-penebalan/naskah-perjanjian-nphd/show/{uuid}/{fase}/{tahap}',['as'=>'member.proposal.penebalan.naskah.perjanjian.nphd.show','uses'=>'Admin\Member\Proposal\ProposalPenebalanController@showNaskahNPHD']);
    Route::post('member/proposal-penebalan/naskah-perjanjian-nphd/update',['as'=>'member.proposal.penebalan.naskah.perjanjian.nphd.update','uses'=>'Admin\Member\Proposal\ProposalPenebalanController@updateNaskahNPHD']);
    Route::get('member/proposal-penebalan/naskah-perjanjian-nphd/{uuid}/{fase}/{tahap}',['as'=>'member.proposal.penebalan.cetak.perjanjian.nphd', 'uses'=>'Admin\Member\Proposal\ProposalPenebalanController@generateNPHDToPDF']);

    // Route cetak Pakta Integritas
    Route::get('member/proposal-penebalan/pakta-integritas/show/{uuid}/{fase}/{tahap}',['as'=>'member.proposal.penebalan.pakta.integritas.show','uses'=>'Admin\Member\Proposal\ProposalPenebalanController@showPaktaIntegritas']);
    Route::post('member/proposal-penebalan/pakta-integritas/update',['as'=>'member.proposal.penebalan.pakta.integritas.update','uses'=>'Admin\Member\Proposal\ProposalPenebalanController@updatePaktaIntegritas']);
    Route::get('member/proposal-penebalan/pakta-integritas/{uuid}/{fase}/{tahap}',['as'=>'member.proposal.penebalan.cetak.pakta.integritas','uses'=>'Admin\Member\Proposal\ProposalPenebalanController@generatePaktaIntegritasToPDF']);
});