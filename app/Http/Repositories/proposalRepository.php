<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\FotoProposal;
use BPKAD\Http\Entities\BerkasProposal;
use BPKAD\Http\Entities\BerkasRab;
use BPKAD\Http\Entities\BerkasRekening;
use BPKAD\Http\Entities\BerkasKepengurusan;
use BPKAD\Http\Entities\BerkasPernyataanTanggungJawab;
use Carbon\Carbon;
use Mail;
use Uuid;
use Config;

class proposalRepository 
{

	public function store($request){		
		// Format rupiah
		$nominal_pengajuan = str_replace(".", "", $request['nominal_pengajuan']);

		// check jumlah proposal terkirim
		$checkJumlahProposalTerkirim = Proposal::where('no_lembaga',$request['no_lembaga'])->where('status_kirim',1)->where('noper',$request['noper'])->count();

		$store = new Proposal;

		$store->noper = $request['noper']; 
		$store->no_skpd = $request['no_skpd'];
		$store->no_lembaga = $request['no_lembaga'];
		$store->tgl_prop = $request['tgl_prop'];
		$store->no_proposal = $request['no_proposal'];
		$store->crb = $request['crb'];
		$store->crd = $request['crd'];
		$store->jdl_prop = $request['jdl_prop'];
		$store->latar_belakang = $request['latar_belakang'];
		$store->maksud_tujuan = $request['maksud_tujuan'];
		$store->keterangan_prop = $request['keterangan_prop'];
		$store->nominal_pengajuan = doubleval(str_replace(",",".",$nominal_pengajuan));
		$store->uuid = Uuid::generate(4);
		$store->status_kirim = 0;
		
		$store->latitude = $request['latitude'];
		$store->longitude = $request['longitude'];
		$store->save();

		//upload file to surat pernyataan tanggung jawab
		if ( $request->hasFile('surat_pernyataan_tanggung_jawab') ) {

			$storePernyataanTanggungJawab = new BerkasPernyataanTanggungJawab;

			$storePernyataanTanggungJawab->no_prop = $store->id;
			$storePernyataanTanggungJawab->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$storePernyataanTanggungJawab->file_size = $request['surat_pernyataan_tanggung_jawab']->getClientSize();
			$storePernyataanTanggungJawab->mime_type = $request['surat_pernyataan_tanggung_jawab']->getClientMimeType();
			$storePernyataanTanggungJawab->file_extension = $request['surat_pernyataan_tanggung_jawab']->getExtension();
			$storePernyataanTanggungJawab->save();

			$destinationPath = 'berkas_pernyataan_tanggungjawab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$request['surat_pernyataan_tanggung_jawab']->move($destinationPath, $fileName);
		}

		//upload file to photo lembaga
		if ( $request->hasFile('photo_lembaga') ) {

			$storeFotoProposal = new FotoProposal;

			$storeFotoProposal->no_prop = $store->id;
			$storeFotoProposal->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$storeFotoProposal->file_size = $request['photo_lembaga']->getClientSize();
			$storeFotoProposal->mime_type = $request['photo_lembaga']->getClientMimeType();
			$storeFotoProposal->file_extension = $request['photo_lembaga']->getExtension();
			$storeFotoProposal->tahapan = 1; //photo sebelum
			$storeFotoProposal->save();

			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$request['photo_lembaga']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_proposal') ) {

			$storeBerkasProposal = new BerkasProposal;
			
			$storeBerkasProposal->no_prop = $store->id;
			$storeBerkasProposal->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$storeBerkasProposal->file_size = $request['berkas_proposal']->getClientSize();
			$storeBerkasProposal->mime_type = $request['berkas_proposal']->getClientMimeType();
			$storeBerkasProposal->file_extension = $request['berkas_proposal']->getExtension();
			$storeBerkasProposal->save();
			
			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$request['berkas_proposal']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_rab') ) {

			$storeBerkasRab = new BerkasRab;
			
			$storeBerkasRab->no_prop = $store->id;
			$storeBerkasRab->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$storeBerkasRab->file_size = $request['berkas_rab']->getClientSize();
			$storeBerkasRab->mime_type = $request['berkas_rab']->getClientMimeType();
			$storeBerkasRab->file_extension = $request['berkas_rab']->getExtension();
			$storeBerkasRab->save();
			
			$destinationPath = 'berkas_rab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$request['berkas_rab']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_kepengurusan') ) {

			$storeBerkasKepengurusan = new BerkasKepengurusan;
			
			$storeBerkasKepengurusan->no_prop = $store->id;
			$storeBerkasKepengurusan->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$storeBerkasKepengurusan->file_size = $request['berkas_kepengurusan']->getClientSize();
			$storeBerkasKepengurusan->mime_type = $request['berkas_kepengurusan']->getClientMimeType();
			$storeBerkasKepengurusan->file_extension = $request['berkas_kepengurusan']->getExtension();
			$storeBerkasKepengurusan->save();
			
			$destinationPath = 'berkas_kepengurusan/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$request['berkas_kepengurusan']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_rekening') ) {

			$storeBerkasRekening = new BerkasRekening;
			
			$storeBerkasRekening->no_prop = $store->id;
			$storeBerkasRekening->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$storeBerkasRekening->file_size = $request['berkas_rekening']->getClientSize();
			$storeBerkasRekening->mime_type = $request['berkas_rekening']->getClientMimeType();
			$storeBerkasRekening->file_extension = $request['berkas_rekening']->getExtension();
			$storeBerkasRekening->save();
			
			$destinationPath = 'berkas_rekening/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$request['berkas_rekening']->move($destinationPath, $fileName);
		}

		return $store->id;
	}

	public function update($request){

		// Format rupiah
		$nominal_pengajuan = str_replace(".", "", $request['nominal_pengajuan']);

		Proposal::where('no_prop',$request['no_prop'])
					->update([
							'tgl_prop'=>$request['tgl_prop'],
							'no_proposal'=>$request['no_proposal'],
							'jdl_prop'=>$request['jdl_prop'],
							'latar_belakang'=>$request['latar_belakang'],
							'maksud_tujuan'=>$request['maksud_tujuan'],
							'nominal_pengajuan'=>doubleval(str_replace(",",".",$nominal_pengajuan)),
							'keterangan_prop'=>$request['keterangan_prop'],
							'status_prop'=> 0, //set jadi menunggu 
							'latitude'=>$request['latitude'],
							'longitude'=>$request['longitude'],
							'upb' => $request['upb'],
							'upd' => $request['upd']
							]);

		//upload file to surat pernyataan tanggung jawab
		if ( $request->hasFile('surat_pernyataan_tanggung_jawab') ) {

			$update = BerkasPernyataanTanggungJawab::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$update->file_size = $request['surat_pernyataan_tanggung_jawab']->getClientSize();
			$update->mime_type = $request['surat_pernyataan_tanggung_jawab']->getClientMimeType();
			$update->file_extension = $request['surat_pernyataan_tanggung_jawab']->getExtension();
			$update->save();

			$destinationPath = 'berkas_pernyataan_tanggungjawab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$request['surat_pernyataan_tanggung_jawab']->move($destinationPath, $fileName);
		}

		//upload file to foto proposal
		if ( $request->hasFile('photo_lembaga') ) {

			$update = FotoProposal::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$update->file_size = $request['photo_lembaga']->getClientSize();
			$update->mime_type = $request['photo_lembaga']->getClientMimeType();
			$update->file_extension = $request['photo_lembaga']->getExtension();
			$update->save();

			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$request['photo_lembaga']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_proposal') ) {

			$update = BerkasProposal::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$update->file_size = $request['berkas_proposal']->getClientSize();
			$update->mime_type = $request['berkas_proposal']->getClientMimeType();
			$update->file_extension = $request['berkas_proposal']->getExtension();
			$update->save();

			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$request['berkas_proposal']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_rab') ) {

			$update = BerkasRab::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$update->file_size = $request['berkas_rab']->getClientSize();
			$update->mime_type = $request['berkas_rab']->getClientMimeType();
			$update->file_extension = $request['berkas_rab']->getExtension();
			$update->save();

			$destinationPath = 'berkas_rab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$request['berkas_rab']->move($destinationPath, $fileName);
		}

		//upload file to berkas kepengurusan
		if ( $request->hasFile('berkas_kepengurusan') ) {

			$update = BerkasKepengurusan::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$update->file_size = $request['berkas_kepengurusan']->getClientSize();
			$update->mime_type = $request['berkas_kepengurusan']->getClientMimeType();
			$update->file_extension = $request['berkas_kepengurusan']->getExtension();
			$update->save();

			$destinationPath = 'berkas_kepengurusan/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$request['berkas_kepengurusan']->move($destinationPath, $fileName);
		}

		//upload file to berkas rekening
		if ( $request->hasFile('berkas_rekening') ) {

			$update = BerkasRekening::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$update->file_size = $request['berkas_rekening']->getClientSize();
			$update->mime_type = $request['berkas_rekening']->getClientMimeType();
			$update->file_extension = $request['berkas_rekening']->getExtension();
			$update->save();

			$destinationPath = 'berkas_rekening/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$request['berkas_rekening']->move($destinationPath, $fileName);
		}

		return true;
	}

	public function setujuiKirim($request){

		// check jumlah proposal terkirim
		$checkJumlahProposalTerkirim = Proposal::where('no_lembaga',$request['no_lembaga'])->where('status_kirim',1)->where('noper',$request['noper'])->count();

		if ($checkJumlahProposalTerkirim  == 0 ) {
			$status_kirim = ($request['status_kirim']=='on')? 1 : 0;
		}else{
			$status_kirim = 0;
		}

		Proposal::where('no_prop',$request['no_prop'])
				->update(['status_kirim' => $status_kirim ]);


		if ($checkJumlahProposalTerkirim  == 0) {
			
			if ($request['status_kirim']=='on') {

				$data = ['jdl_prop' => $request['jdl_prop'],
						 'nominal_pengajuan' => $request['nominal_pengajuan'],
						 'email' => $request['email'],
						 'email_lembaga' => $request['email_lembaga'],
						 // 'email_lembaga' => 'hary.purnomo87@gmail.com',
						 'no_lembaga' => $request['no_lembaga'],
						 'real_name' => $request['real_name'],
						 'tahun_anggaran' => $request['tahun_anggaran'],
						 'base64'=>'data:image/png;base64'
						];

			    Mail::send('emails.kirim-proposal', ['data' => $data], function ($m) use ($data) {
			            $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

			            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Bukti Kirim Proposal');
			        });
			}

		}

		return true;
	}

	//for skpd
	public function update_status($request){

		// convert string to double
		$nominal_rekomendasi = str_replace(".", "", $request['nominal_rekomendasi']);

		// status dikoreksi
		if ($request['status_prop'] == 3){
			Proposal::where('no_prop',$request['no_prop'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_dikembalikan' => $request['alasan_dikembalikan'],
					  'status_kirim' => 0
					 ]);

          	$data = ['no_prop'=> base64_encode($request['no_prop']),
          			 'jdl_prop' => $request['jdl_prop'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 'email' => $request['email'],
          			 'real_name' => $request['real_name'],
          			 'nm_lembaga' => $request['nm_lembaga'],
          			 'no_lembaga' => $request['no_lembaga'],
          			 'alasan_dikembalikan' => $request['alasan_dikembalikan'],
          			 'base64'=>'data:image/png;base64'
					];

		    Mail::send('emails.kirim-alasan-dikoreksi', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Proposal Dikoreksi');
		        });

          	return true;
		}
		// status direkomendasi
		elseif($request['status_prop'] == 1){
			Proposal::where('no_prop',$request['no_prop'])
			->update(['status_prop' => $request['status_prop'],
					  'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
					  'no_survei_lap' => $request['no_survei_lap'],
					  'tgl_survei_lap' => ($request['tgl_survei_lap'] != null ? $request['tgl_survei_lap'] : date('Y-m-d')),
					  'no_kelengkapan_administrasi' => $request['no_kelengkapan_administrasi'],
					  'tgl_kelengkapan_administrasi' => ($request['tgl_kelengkapan_administrasi'] != null ? $request['tgl_kelengkapan_administrasi'] : date('Y-m-d')),
					  'no_rekomendasi' => $request['no_rekomendasi'],
					  'tgl_rekomendasi' => ($request['tgl_rekomendasi'] != null ? $request['tgl_rekomendasi'] : date('Y-m-d')),
					  'no_surat_skpd' => $request['no_surat_skpd'],
					  'tgl_surat_skpd' =>  ($request['tgl_surat_skpd'] != null ? $request['tgl_surat_skpd'] : date('Y-m-d')),
					  'no_prg' => $request['no_prg'],
				  'no_rek' => $request['no_rek'],
				  'kategoriProposalID' => $request['kategoriProposalID'],
				  'status_kirim' => 1
					 ]);

          	$data = ['no_prop'=> base64_encode($request['no_prop']),
          			 'jdl_prop' => $request['jdl_prop'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 // 'email_lembaga' => 'hary.purnomo87@gmail.com',
          			 'email' => $request['email'],
          			 'real_name' => $request['real_name'],
          			 'nm_lembaga' => $request['nm_lembaga'],
          			 'no_lembaga' => $request['no_lembaga'],
          			 'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
          			 'no_rekomendasi' => $request['no_rekomendasi'],
          			 'tgl_rekomendasi' => $request['tgl_rekomendasi'],
          			 'base64'=>'data:image/png;base64'		
          			];

          	if($data['email'] !="" && $data['email_lembaga'] !="" ){
          		 Mail::send('emails.kirim-alasan-direkomendasi', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Proposal Direkomendasi');
		        });
          	}
		   

		   	return true;
		}
		// status ditolak & menunggu
		else{
			Proposal::where('no_prop',$request['no_prop'])
          				->update(['status_prop' => $request['status_prop'],
          						  'alasan_ditolak' => $request['alasan_ditolak']
          						 ]);
		
			$data = ['no_prop'=> base64_encode($request['no_prop']),
          			 'jdl_prop' => $request['jdl_prop'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 'email' => $request['email'],
          			 'real_name' => $request['real_name'],
          			 'nm_lembaga' => $request['nm_lembaga'],
          			 'no_lembaga' => $request['no_lembaga'],
          			 'alasan_ditolak' => $request['alasan_ditolak'],
          			 'base64'=>'data:image/png;base64'
					];
			if($data['email'] !="" && $data['email_lembaga'] !="" ){
		    Mail::send('emails.kirim-alasan-ditolak', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Proposal Ditolak');
		        });
		   }

			return true;
		}
	}

	public function updatePerbaikan($request){

		Proposal::where('no_prop',$request['no_prop'])
					->update([
							'tgl_prop'=>$request['tgl_prop'],
							'no_proposal'=>$request['no_proposal'],
							'latitude'=>$request['latitude'],
							'longitude'=>$request['longitude'],
							'upb' => $request['upb'],
							'upd' => $request['upd']
							]);

		//upload file to surat pernyataan tanggung jawab
		if ( $request->hasFile('surat_pernyataan_tanggung_jawab') ) {

			$update = BerkasPernyataanTanggungJawab::firstOrNew(array('no_prop' => $request['no_prop']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$update->file_size = $request['surat_pernyataan_tanggung_jawab']->getClientSize();
			$update->mime_type = $request['surat_pernyataan_tanggung_jawab']->getClientMimeType();
			$update->file_extension = $request['surat_pernyataan_tanggung_jawab']->getExtension();
			$update->save();

			$destinationPath = 'berkas_pernyataan_tanggungjawab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$request['surat_pernyataan_tanggung_jawab']->move($destinationPath, $fileName);
		}

		return true;
	}

	public function penerimaTahunIni(){
		//set @var tahun
		$tahun = \Carbon\Carbon::now('Asia/Jakarta')->format('Y'); 
		//set @var query
		$query = DB::table('mst_periode AS mp')->leftJoin('trx_proposal AS tp','mp.noper','=','tp.noper')
											  ->where('mp.tahun','=',$tahun)
											  ->select('tp.*')->count();
		return $query;
	}

	public function penerimaTahunDepan(){
		//set @var tahun
		$tahun = \Carbon\Carbon::now('Asia/Jakarta')->format('Y')+1; 
		//set @var query
		$query = DB::table('mst_periode AS mp')->leftJoin('trx_proposal AS tp','mp.noper','=','tp.noper')
											  ->where('mp.tahun','=',$tahun)
											  ->select('tp.*')->count();
		return $query;
	}

	public function penerimaDuaTahunDepan(){
		//set @var tahun
		$tahun = \Carbon\Carbon::now('Asia/Jakarta')->format('Y')+2; 
		//set @var query
		$query = DB::table('mst_periode AS mp')->leftJoin('trx_proposal AS tp','mp.noper','=','tp.noper')
											  ->where('mp.tahun','=',$tahun)
											  ->select('tp.*')->count();
		return $query;
	}

	public function penerima($tahun){
		//set @var query
		$query = DB::table('mst_periode AS mp')->leftJoin('trx_proposal AS tp','mp.noper','=','tp.noper')
											  ->where('mp.tahun','=',$tahun)
											  ->select('tp.*')->get();
		return $query;
	}
}
?>