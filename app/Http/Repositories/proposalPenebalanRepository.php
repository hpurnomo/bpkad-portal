<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\ProposalPenebalanTAPD;
use BPKAD\Http\Entities\ProposalPenebalanBANGGAR;
use BPKAD\Http\Entities\ProposalPenebalanDDN;
use BPKAD\Http\Entities\ProposalDefinitifAPBD;
use BPKAD\Http\Entities\FotoProposalPenebalan;
use BPKAD\Http\Entities\BerkasProposalPenebalan;
use BPKAD\Http\Entities\BerkasRabPenebalan;
use BPKAD\Http\Entities\BerkasRekeningPenebalan;
use BPKAD\Http\Entities\BerkasKepengurusanPenebalan;
use BPKAD\Http\Entities\BerkasPernyataanTanggungJawabPenebalan;
use Carbon\Carbon;
use Mail;
use Uuid;
use Config;

class proposalPenebalanRepository 
{

	public function store($request){

		// dd($request->all());	

		// Format rupiah
		$nominal_pengajuan = str_replace(".", "", $request['nominal_pengajuan']);

		if ($request->tahap=='tapd') {
			$store = new ProposalPenebalanTAPD;
		}
		elseif ($request->tahap=='banggar') {
			$store = new ProposalPenebalanBANGGAR;
		}
		elseif ($request->tahap=='ddn') {
			$store = new ProposalPenebalanDDN;
		}
		elseif ($request->tahap=='apbd') {
			$store = new ProposalDefinitifAPBD;
		}

		$store->noper = $request['noper']; 
		$store->no_skpd = $request['no_skpd'];
		$store->no_lembaga = $request['no_lembaga'];
		$store->tgl_prop = $request['tgl_prop'];
		$store->no_proposal = $request['no_proposal'];
		$store->crb = $request['crb'];
		$store->crd = $request['crd'];
		$store->jdl_prop = $request['jdl_prop'];
		$store->latar_belakang = $request['latar_belakang'];
		$store->maksud_tujuan = $request['maksud_tujuan'];
		$store->keterangan_prop = $request['keterangan_prop'];
		$store->nominal_pengajuan = doubleval(str_replace(",",".",$nominal_pengajuan));
		$store->uuid_proposal_pengajuan = $request['uuid'];
		$store->status_kirim = 0;
		$store->latitude = $request['latitude'];
		$store->longitude = $request['longitude'];
		$store->save();

		//upload file to surat pernyataan tanggung jawab
		if ( $request->hasFile('surat_pernyataan_tanggung_jawab') ) {

			$storePernyataanTanggungJawab = new BerkasPernyataanTanggungJawabPenebalan;

			$storePernyataanTanggungJawab->uuid_proposal_pengajuan = $request['uuid'];
			$storePernyataanTanggungJawab->tahap = $request['tahap'];
			$storePernyataanTanggungJawab->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$storePernyataanTanggungJawab->file_size = $request['surat_pernyataan_tanggung_jawab']->getClientSize();
			$storePernyataanTanggungJawab->mime_type = $request['surat_pernyataan_tanggung_jawab']->getClientMimeType();
			$storePernyataanTanggungJawab->file_extension = $request['surat_pernyataan_tanggung_jawab']->getExtension();
			$storePernyataanTanggungJawab->save();

			$destinationPath = 'berkas_pernyataan_tanggungjawab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$request['surat_pernyataan_tanggung_jawab']->move($destinationPath, $fileName);
		}

		//upload file to photo lembaga
		if ( $request->hasFile('photo_lembaga') ) {

			$storeFotoProposal = new FotoProposalPenebalan;

			$storeFotoProposal->uuid_proposal_pengajuan = $request['uuid'];
			$storeFotoProposal->tahap = $request['tahap'];
			$storeFotoProposal->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$storeFotoProposal->file_size = $request['photo_lembaga']->getClientSize();
			$storeFotoProposal->mime_type = $request['photo_lembaga']->getClientMimeType();
			$storeFotoProposal->file_extension = $request['photo_lembaga']->getExtension();
			$storeFotoProposal->tahapan = 1; //photo sebelum
			$storeFotoProposal->save();

			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$request['photo_lembaga']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_proposal') ) {

			$storeBerkasProposal = new BerkasProposalPenebalan;
			
			$storeBerkasProposal->uuid_proposal_pengajuan = $request['uuid'];
			$storeBerkasProposal->tahap = $request['tahap'];
			$storeBerkasProposal->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$storeBerkasProposal->file_size = $request['berkas_proposal']->getClientSize();
			$storeBerkasProposal->mime_type = $request['berkas_proposal']->getClientMimeType();
			$storeBerkasProposal->file_extension = $request['berkas_proposal']->getExtension();
			$storeBerkasProposal->save();
			
			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$request['berkas_proposal']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_rab') ) {

			$storeBerkasRab = new BerkasRabPenebalan;
			
			$storeBerkasRab->uuid_proposal_pengajuan = $request['uuid'];
			$storeBerkasRab->tahap = $request['tahap'];
			$storeBerkasRab->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$storeBerkasRab->file_size = $request['berkas_rab']->getClientSize();
			$storeBerkasRab->mime_type = $request['berkas_rab']->getClientMimeType();
			$storeBerkasRab->file_extension = $request['berkas_rab']->getExtension();
			$storeBerkasRab->save();
			
			$destinationPath = 'berkas_rab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$request['berkas_rab']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_kepengurusan') ) {

			$storeBerkasKepengurusan = new BerkasKepengurusanPenebalan;
			
			$storeBerkasKepengurusan->uuid_proposal_pengajuan = $request['uuid'];
			$storeBerkasKepengurusan->tahap = $request['tahap'];
			$storeBerkasKepengurusan->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$storeBerkasKepengurusan->file_size = $request['berkas_kepengurusan']->getClientSize();
			$storeBerkasKepengurusan->mime_type = $request['berkas_kepengurusan']->getClientMimeType();
			$storeBerkasKepengurusan->file_extension = $request['berkas_kepengurusan']->getExtension();
			$storeBerkasKepengurusan->save();
			
			$destinationPath = 'berkas_kepengurusan/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$request['berkas_kepengurusan']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_rekening') ) {

			$storeBerkasRekening = new BerkasRekeningPenebalan;
			
			$storeBerkasRekening->uuid_proposal_pengajuan = $request['uuid'];
			$storeBerkasRekening->tahap = $request['tahap'];
			$storeBerkasRekening->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$storeBerkasRekening->file_size = $request['berkas_rekening']->getClientSize();
			$storeBerkasRekening->mime_type = $request['berkas_rekening']->getClientMimeType();
			$storeBerkasRekening->file_extension = $request['berkas_rekening']->getExtension();
			$storeBerkasRekening->save();
			
			$destinationPath = 'berkas_rekening/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$request['berkas_rekening']->move($destinationPath, $fileName);
		}
		
		return $store->id;
	}

	public function update($request){
		// Format rupiah
		$nominal_pengajuan = str_replace(".", "", $request['nominal_pengajuan']);

		if ($request->tahap=='tapd') {
			ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request['uuid'])
					->update([
							'tgl_prop'=>$request['tgl_prop'],
							'no_proposal'=>$request['no_proposal'],
							'jdl_prop'=>$request['jdl_prop'],
							'latar_belakang'=>$request['latar_belakang'],
							'maksud_tujuan'=>$request['maksud_tujuan'],
							'nominal_pengajuan'=>doubleval(str_replace(",",".",$nominal_pengajuan)),
							'keterangan_prop'=>$request['keterangan_prop'],
							'status_prop'=> 0, //set jadi menunggu 
							'latitude'=>$request['latitude'],
							'longitude'=>$request['longitude'],
							'upb' => $request['upb'],
							'upd' => $request['upd']
							]);
		}
		elseif ($request->tahap=='banggar') {
			ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request['uuid'])
					->update([
							'tgl_prop'=>$request['tgl_prop'],
							'no_proposal'=>$request['no_proposal'],
							'jdl_prop'=>$request['jdl_prop'],
							'latar_belakang'=>$request['latar_belakang'],
							'maksud_tujuan'=>$request['maksud_tujuan'],
							'nominal_pengajuan'=>doubleval(str_replace(",",".",$nominal_pengajuan)),
							'keterangan_prop'=>$request['keterangan_prop'],
							'status_prop'=> 0, //set jadi menunggu 
							'latitude'=>$request['latitude'],
							'longitude'=>$request['longitude'],
							'upb' => $request['upb'],
							'upd' => $request['upd']
							]);
		}
		elseif ($request->tahap=='ddn') {
			ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request['uuid'])
					->update([
							'tgl_prop'=>$request['tgl_prop'],
							'no_proposal'=>$request['no_proposal'],
							'jdl_prop'=>$request['jdl_prop'],
							'latar_belakang'=>$request['latar_belakang'],
							'maksud_tujuan'=>$request['maksud_tujuan'],
							'nominal_pengajuan'=>doubleval(str_replace(",",".",$nominal_pengajuan)),
							'keterangan_prop'=>$request['keterangan_prop'],
							'status_prop'=> 0, //set jadi menunggu 
							'latitude'=>$request['latitude'],
							'longitude'=>$request['longitude'],
							'upb' => $request['upb'],
							'upd' => $request['upd']
							]);
		}
		elseif ($request->tahap=='apbd') {
			ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request['uuid'])
					->update([
							'tgl_prop'=>$request['tgl_prop'],
							'no_proposal'=>$request['no_proposal'],
							'jdl_prop'=>$request['jdl_prop'],
							'latar_belakang'=>$request['latar_belakang'],
							'maksud_tujuan'=>$request['maksud_tujuan'],
							'nominal_pengajuan'=>doubleval(str_replace(",",".",$nominal_pengajuan)),
							'keterangan_prop'=>$request['keterangan_prop'],
							'status_prop'=> 0, //set jadi menunggu 
							'latitude'=>$request['latitude'],
							'longitude'=>$request['longitude'],
							'upb' => $request['upb'],
							'upd' => $request['upd']
							]);
		}

		//upload file to surat pernyataan tanggung jawab
		if ( $request->hasFile('surat_pernyataan_tanggung_jawab') ) {

			$update = BerkasPernyataanTanggungJawabPenebalan::firstOrNew(array('uuid_proposal_pengajuan' => $request['uuid'],'tahap'=>$request['tahap']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$update->file_size = $request['surat_pernyataan_tanggung_jawab']->getClientSize();
			$update->mime_type = $request['surat_pernyataan_tanggung_jawab']->getClientMimeType();
			$update->file_extension = $request['surat_pernyataan_tanggung_jawab']->getExtension();
			$update->save();

			$destinationPath = 'berkas_pernyataan_tanggungjawab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['surat_pernyataan_tanggung_jawab']->getClientOriginalName());
			$request['surat_pernyataan_tanggung_jawab']->move($destinationPath, $fileName);
		}

		//upload file to foto proposal
		if ( $request->hasFile('photo_lembaga') ) {

			$update = FotoProposalPenebalan::firstOrNew(array('uuid_proposal_pengajuan' => $request['uuid'],'tahap'=>$request['tahap']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$update->file_size = $request['photo_lembaga']->getClientSize();
			$update->mime_type = $request['photo_lembaga']->getClientMimeType();
			$update->file_extension = $request['photo_lembaga']->getExtension();
			$update->save();

			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['photo_lembaga']->getClientOriginalName());
			$request['photo_lembaga']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_proposal') ) {

			$update = BerkasProposalPenebalan::firstOrNew(array('uuid_proposal_pengajuan' => $request['uuid'],'tahap'=>$request['tahap']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$update->file_size = $request['berkas_proposal']->getClientSize();
			$update->mime_type = $request['berkas_proposal']->getClientMimeType();
			$update->file_extension = $request['berkas_proposal']->getExtension();
			$update->save();

			$destinationPath = 'foto_proposal/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_proposal']->getClientOriginalName());
			$request['berkas_proposal']->move($destinationPath, $fileName);
		}

		//upload file to berkas proposal
		if ( $request->hasFile('berkas_rab') ) {

			$update = BerkasRabPenebalan::firstOrNew(array('uuid_proposal_pengajuan' => $request['uuid'],'tahap'=>$request['tahap']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$update->file_size = $request['berkas_rab']->getClientSize();
			$update->mime_type = $request['berkas_rab']->getClientMimeType();
			$update->file_extension = $request['berkas_rab']->getExtension();
			$update->save();

			$destinationPath = 'berkas_rab/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rab']->getClientOriginalName());
			$request['berkas_rab']->move($destinationPath, $fileName);
		}

		//upload file to berkas kepengurusan
		if ( $request->hasFile('berkas_kepengurusan') ) {

			$update = BerkasKepengurusanPenebalan::firstOrNew(array('uuid_proposal_pengajuan' => $request['uuid'],'tahap'=>$request['tahap']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$update->file_size = $request['berkas_kepengurusan']->getClientSize();
			$update->mime_type = $request['berkas_kepengurusan']->getClientMimeType();
			$update->file_extension = $request['berkas_kepengurusan']->getExtension();
			$update->save();

			$destinationPath = 'berkas_kepengurusan/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_kepengurusan']->getClientOriginalName());
			$request['berkas_kepengurusan']->move($destinationPath, $fileName);
		}

		//upload file to berkas rekening
		if ( $request->hasFile('berkas_rekening') ) {

			$update = BerkasRekeningPenebalan::firstOrNew(array('uuid_proposal_pengajuan' => $request['uuid'],'tahap'=>$request['tahap']));
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$update->file_size = $request['berkas_rekening']->getClientSize();
			$update->mime_type = $request['berkas_rekening']->getClientMimeType();
			$update->file_extension = $request['berkas_rekening']->getExtension();
			$update->save();

			$destinationPath = 'berkas_rekening/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['berkas_rekening']->getClientOriginalName());
			$request['berkas_rekening']->move($destinationPath, $fileName);
		}

		return true;

	}

	public function setujuiKirim($request){

		if ($request['tahap']=='tapd') {
			ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request['uuid'])->update(['status_kirim' => ($request['status_kirim']=='on')? 1 : 0 ]);
		}
		elseif ($request['tahap']=='banggar') {
			ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request['uuid'])->update(['status_kirim' => ($request['status_kirim']=='on')? 1 : 0 ]);
		}
		elseif ($request['tahap']=='ddn') {
			ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request['uuid'])->update(['status_kirim' => ($request['status_kirim']=='on')? 1 : 0 ]);
		}
		elseif ($request['tahap']=='apbd') {
			ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request['uuid'])->update(['status_kirim' => ($request['status_kirim']=='on')? 1 : 0 ]);
		}
			
		// if ($request['status_kirim']=='on') {

		// 	$data = ['jdl_prop' => $request['jdl_prop'],
		// 			 'nominal_pengajuan' => $request['nominal_pengajuan'],
		// 			 'email' => $request['email'],
		// 			 'email_lembaga' => $request['email_lembaga'],
		// 			 // 'email_lembaga' => 'hary.purnomo87@gmail.com',
		// 			 'no_lembaga' => $request['no_lembaga'],
		// 			 'real_name' => $request['real_name'],
		// 			 'tahun_anggaran' => $request['tahun_anggaran'],
		// 			 'base64'=>'data:image/png;base64'
		// 			];

		//     Mail::send('emails.kirim-proposal', ['data' => $data], function ($m) use ($data) {
		//             $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		//             $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Bukti Kirim Proposal');
		//         });
		// }

		return true;
	}

	//for skpd
	public function update_status($request){

		// convert string to double
		$nominal_rekomendasi = str_replace(".", "", $request['nominal_rekomendasi']);

		// status dikoreksi
		if ($request['status_prop'] == 3){
			Proposal::where('no_prop',$request['no_prop'])
          				->update(['status_prop' => $request['status_prop'],
          						  'alasan_dikembalikan' => $request['alasan_dikembalikan'],
          						  'status_kirim' => 0
          						 ]);

          	$data = ['no_prop'=> base64_encode($request['no_prop']),
          			 'jdl_prop' => $request['jdl_prop'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 'nm_lembaga' => $request['nm_lembaga'],
          			 'no_lembaga' => $request['no_lembaga'],
          			 'alasan_dikembalikan' => $request['alasan_dikembalikan'],
          			 'base64'=>'data:image/png;base64'
					];

		    Mail::send('emails.kirim-alasan-dikoreksi', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email_lembaga'], $data['nm_lembaga'])->subject('Proposal Dikoreksi');
		        });

          	return true;
		}
		// status direkomendasi
		elseif($request['status_prop'] == 1){
			Proposal::where('no_prop',$request['no_prop'])
          				->update(['status_prop' => $request['status_prop'],
          						  'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
          						  'no_survei_lap' => $request['no_survei_lap'],
          						  'tgl_survei_lap' => $request['tgl_survei_lap'],
          						  'no_kelengkapan_administrasi' => $request['no_kelengkapan_administrasi'],
          						  'tgl_kelengkapan_administrasi' => $request['tgl_kelengkapan_administrasi'],
          						  'no_rekomendasi' => $request['no_rekomendasi'],
          						  'tgl_rekomendasi' => $request['tgl_rekomendasi'],
          						  'no_surat_skpd' => $request['no_surat_skpd'],
          						  'tgl_surat_skpd' => $request['tgl_surat_skpd'],
          						  'no_prg' => $request['no_prg'],
								  'no_rek' => $request['no_rek'],
								  'kategoriProposalID' => $request['kategoriProposalID'],
								  'status_kirim' => 1
          						 ]);

          	$data = ['no_prop'=> base64_encode($request['no_prop']),
          			 'jdl_prop' => $request['jdl_prop'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 'nm_lembaga' => $request['nm_lembaga'],
          			 'no_lembaga' => $request['no_lembaga'],
          			 'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
          			 'no_rekomendasi' => $request['no_rekomendasi'],
          			 'tgl_rekomendasi' => $request['tgl_rekomendasi'],
          			 'base64'=>'data:image/png;base64'		
          			];

		    Mail::send('emails.kirim-alasan-direkomendasi', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email_lembaga'], $data['nm_lembaga'])->subject('Proposal Direkomendasi');
		        });

		   	return true;
		}
		// status ditolak & menunggu
		else{
			Proposal::where('no_prop',$request['no_prop'])
          				->update(['status_prop' => $request['status_prop'],
          						  'alasan_ditolak' => $request['alasan_ditolak']
          						 ]);
		
			$data = ['no_prop'=> base64_encode($request['no_prop']),
          			 'jdl_prop' => $request['jdl_prop'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 'nm_lembaga' => $request['nm_lembaga'],
          			 'no_lembaga' => $request['no_lembaga'],
          			 'alasan_ditolak' => $request['alasan_ditolak'],
          			 'base64'=>'data:image/png;base64'
					];

		    Mail::send('emails.kirim-alasan-ditolak', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email_lembaga'], $data['nm_lembaga'])->subject('Proposal Ditolak');
		        });

			return true;
		}
	}

	

	
}
?>