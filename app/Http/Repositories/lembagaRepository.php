<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\Lembaga;

class lembagaRepository 
{
	public function getAll(){
		return Lembaga::all();
	}

	public function getRecByID($id){
		$query = DB::table('mst_lembaga as a')->select('a.*','b.nm_skpd','c.nama as namaTipeLembaga','d.nama AS noRT','e.nama AS noRW','f.nmkel','g.ktp_ketua','g.akta','g.npwp AS scanNPWP','g.skd','g.izin_operasional','g.sertifikat','g.rekening_lembaga','h.file_name AS fotoLembaga','i.nmkota','j.nmkec')
											  ->leftJoin('mst_skpd as b','a.kd_skpd','=','b.kd_skpd')
											  ->leftJoin('mst_tipe_lembaga as c','a.tipeLembagaID','=','c.id')
											  ->leftJoin('mst_rt AS d','a.rtID','=','d.id')
											  ->leftJoin('mst_rw AS e','a.rwID','=','e.id')
											  ->leftJoin('mst_kelurahan AS f','a.nokel','=','f.nokel')
											  ->leftJoin('user_lembaga AS g','a.nomor','=','g.no_lembaga')
											  ->leftJoin('foto_lembaga as h', 'a.nomor', '=', 'h.no_lembaga')
											  ->leftJoin('mst_kota as i', 'a.nokota', '=', 'i.nokota')
											  ->leftJoin('mst_kecamatan as j', 'a.nokec', '=', 'j.nokec')
											  ->where('a.nomor',$id)
											  ->get();
		return $query;
	}

	public function getAllWithPaginate(){
		$query = DB::table('mst_lembaga AS ml')->select(['ml.*','fl.file_name'])
					->leftJoin('foto_lembaga AS fl','ml.nomor','=','fl.no_lembaga')
					->where('is_verify',1)
					->get();
		// return Lembaga::paginate(12);
		return $query;
	}

	public function getAllMap(){
		
	}

}

?>