<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\User;
use Carbon\Carbon;

class userSkpdRepository 
{
	public function store($request){

      $store = new User;
  	  $store->no_skpd = $request['no_skpd'];
      $store->group_id = $request['group_id'];
      $store->is_active = $request['is_active'];
      $store->nrk = $request['nrk'];
      $store->real_name = $request['real_name'];
      $store->ktp = $request['ktp'];
      $store->telp = $request['telp'];
      $store->email = $request['email'];
      $store->password = bcrypt($request['password']);
      $store->no_registrasi = $request['no_skpd'].'.'.Carbon::now('Asia/Jakarta')->format('dmY').'.'.Carbon::now('Asia/Jakarta')->format('hms');
      $store->photo = Carbon::now('Asia/Jakarta')->format('dmYhms').'_'.str_replace(' ', '_', $request['photo']->getClientOriginalName());
      $store->mimes = $request['photo']->getClientOriginalExtension();
      $store->verification_token = md5(($request['password']).Carbon::now('Asia/Jakarta')->format('dmYhms'));
      $store->is_verify = 1;
      $store->save();

      $destinationPath = 'foto_user/';
      $fileName = Carbon::now('Asia/Jakarta')->format('dmYhms').'_'.str_replace(' ', '_', $request['photo']->getClientOriginalName());
      $request['photo']->move($destinationPath, $fileName);

      // send via api mailgun
      $config = array();
 
      $config['api_key'] = "key-054898c9df75c9e9a6493c6418f0fad0";
 
      $config['api_url'] = "https://api.mailgun.net/v3/sandboxe7f880f7718d4162b6f290f9d7f8c075.mailgun.org/messages";
 
      $message = array();
 
      $message['from'] = "Admin eHibahbansos DKI Jakarta :: hibahbansosdki@gmail.com";
 
      $message['to'] = "".$request['email']."";
       
      $message['subject'] = 'Aktifasi Akun';
 
      $message['html'] = "Dear, <b>".$request['real_name']."</b><br/><br/>Agar Anda dapat login ke Aplikasi eHibahbansos DKI Jakarta Silahkan Aktifasi Akun Anda Segera. <br/><br/><br/><br/><a href='".URL('aktifasi-akun')."/".md5(($request['password']).Carbon::now('Asia/Jakarta')->format('dmYhms'))."' style='background: #0095FF;padding: 15px;color: white;font-size: 16px;text-decoration: none;text-transform: uppercase;letter-spacing: 3px;border-radius:20px;margin:40px;'>Aktifasi Akun</a><br/><br/><br/><br/><br/><br/><p>&nbsp;</p>";
 
      $ch = curl_init();
 
      curl_setopt($ch, CURLOPT_URL, $config['api_url']);
 
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
 
      curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
 
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 
      curl_setopt($ch, CURLOPT_POST, true); 
 
      curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
 
      $result = curl_exec($ch);
 
      curl_close($ch);

      return $store->id.'.'.$request['no_skpd'].'.'.Carbon::now('Asia/Jakarta')->format('dmY').'.'.Carbon::now('Asia/Jakarta')->format('hms');
	}

      public function verification_token($ver_token){
            DB::table('users')
                        ->where('verification_token', $ver_token)
                        ->update(['is_active' => 1]);
      }

}

?>