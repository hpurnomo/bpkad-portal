<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\Kota;

class kotaRepository 
{
	public function getAll(){
		return Kota::all();
	}

	public function getByNoKota($nokota){
		return Kota::where('nokota',$nokota)->get();
	}

}

?>