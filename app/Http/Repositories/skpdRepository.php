<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\Skpd;

class skpdRepository 
{
	public function getAll(){
		return Skpd::all();
	}

	public function getRecByID($id){
		return Skpd::where('nomor',$id)->get();
	}

	public function getAllWithPaginate(){
		return Skpd::paginate(12);
	}

	public function getRecByKDSKPD($id){
		return Skpd::where('kd_skpd',$id)->get();
	}

}

?>