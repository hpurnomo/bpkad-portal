<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/
use Session;

class sessionFlashRepository 
{

	public function errMsg($className, $errmsg){
		return session()->flash("errMsg", "<div class=\"alert ".$className."\">
						  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
						  <i class=\"icon-warning-sign\"></i><strong>Warning!</strong> ".$errmsg."
						</div>"); 
	}
}?>