<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\FotoLembaga;
use Carbon\Carbon;
use Illuminate\Http\Request;

class userLembagaFilesRepository 
{
	public function store($request){

            if($request->hasFile('ktp_ketua')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->ktp_ketua = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['ktp_ketua']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'user_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['ktp_ketua']->getClientOriginalName());
                  $request['ktp_ketua']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('akta')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->akta = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['akta']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'user_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['akta']->getClientOriginalName());
                  $request['akta']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('npwp')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->npwp = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['npwp']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'user_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['npwp']->getClientOriginalName());
                  $request['npwp']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('skd')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->skd = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['skd']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'user_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['skd']->getClientOriginalName());
                  $request['skd']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('izin_operasional')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->izin_operasional = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['izin_operasional']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'user_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['izin_operasional']->getClientOriginalName());
                  $request['izin_operasional']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('sertifikat')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->sertifikat = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['sertifikat']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'user_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['sertifikat']->getClientOriginalName());
                  $request['sertifikat']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('foto_lembaga')){

                  $store = FotoLembaga::firstOrNew(['no_lembaga'=>$request['no_lembaga']]);
                 
                  $store->no_lembaga = $request['no_lembaga'];
                  $store->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['foto_lembaga']->getClientOriginalName());
                  $store->save();

                  //upload file
                  $destinationPath = 'foto_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['foto_lembaga']->getClientOriginalName());
                  $request['foto_lembaga']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('foto_person')){

                  DB::table('mst_lembaga')->where('nomor',$request['no_lembaga'])
                                          ->update(['foto_person' => Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['foto_person']->getClientOriginalName())
                                                ]);

                  //upload file
                  $destinationPath = 'foto_ketua_lembaga/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['foto_person']->getClientOriginalName());
                  $request['foto_person']->move($destinationPath, $fileName);
            }
            else if($request->hasFile('rekening_lembaga')){
                  $update = UserLembaga::where('no_lembaga',$request['no_lembaga'])->first();
                  $update->rekening_lembaga = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['rekening_lembaga']->getClientOriginalName());
                  $update->save();

                  //upload file
                  $destinationPath = 'berkas_rekening/';
                  $fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['rekening_lembaga']->getClientOriginalName());
                  $request['rekening_lembaga']->move($destinationPath, $fileName);
            }

	}

}

?>