<?php 
namespace BPKAD\Http\Repositories;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use DB;
use BPKAD\Http\Entities\Kelurahan;

class kelurahanRepository 
{
	public function getAll(){
		return Kelurahan::all();
	}

	public function getKdKel($kdkel){
		return Kelurahan::where('kdkel','like','%'.$kdkel.'%')->get();
	}

}

?>