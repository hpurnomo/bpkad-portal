<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;
use DB;

class Kabupaten extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_kota';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'nokota','noprov','kdkota','nmkota'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'nokota';

     public static function getData(){ 
        return DB::table("mst_kota as a")
        ->select("a.nmkota","a.kdkota","a.nokota","c.nmprov")
        ->join("mst_provinsi as c","a.noprov","=","c.noprov")
        ->orderBy("a.nmkota","asc");

    }
}
