<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class ProposalPenebalanTAPD extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trx_proposal_penebalan_tapd';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'noper','no_lembaga','no_skpd','no_prg','no_rek','no_giat','jdl_prop','status_kirim','status_prop','alasan_ditolak','alasan_dikembalikan','kd_prop',
    'tgl_prop','no_proposal','latar_belakang','maksud_tujuan','nominal_pengajuan','nominal_rekomendasi','nominal_tapd','is_penebalan_nominal_tapd','nominal_banggar','is_penebalan_nominal_banggar','nominal_ddn','is_penebalan_nominal_ddn','nominal_apbd','is_penebalan_nominal_apbd','nominal_skgub','nominal_pencairan','nominal_lpj','crb','crd','upb',
    'upd','upp','proses_pencairan','keterangan_prop','status_input','no_survei_lap','tgl_survei_lap','no_kelengkapan_administrasi','tgl_kelengkapan_administrasi','no_rekomendasi','tgl_rekomendasi','no_surat_skpd','tgl_surat_skpd','kategoriProposalID','latitude','longitude','cek_aktivitas','cek_kepengurusan','cek_rab','cek_waktu_pelaksanaan','created_by','uuid_proposal_pengajuan','created_at','updated_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'no_prop';
}
