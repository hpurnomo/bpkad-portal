<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class VerifikasiBerkasPencairan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'verifikasi_berkas_pencairan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['no_prop','tahun_anggaran','no_sk_gub_penetapan','kelompok_belanja','no_urut_pemohon_sk','nama_pemohon','nm_skpd','cek_surat_pengantar','ket_surat_pengantar','cek_surat_permohonan','ket_surat_permohonan','cek_rab','ket_rab','cek_kwitansi','ket_kwitansi','cek_rekening','ket_rekening','cek_sk_lembaga','ket_sk_lembaga','cek_rekomendasi','ket_rekomendasi','cek_bak_administrasi','ket_bak_administrasi','cek_bap_lapangan','ket_bap_lapangan','cek_surat_tugas','ket_surat_tugas','cek_surat_pernyataan_tanggungjawab','ket_surat_pernyataan_tanggungjawab','cek_ktp_pengurus','ket_ktp_pengurus','cek_skd','ket_skd','cek_npwp','ket_npwp','cek_proposal_definitif','ket_proposal_definitif','cek_nphd','ket_nphd','cek_tanda_terima_laporan','ket_tanda_terima_laporan','cek_tanda_terima_audit','ket_tanda_terima_audit','cek_tanda_terima_monev','ket_tanda_terima_monev','cek_bukti_email','ket_bukti_email','nama_ketua','nip','cek_pakta_integritas','ket_pakta_integritas','cek_perjanjian_hibah_daerah','ket_perjanjian_hibah_daerah','cek_tgjwb_hibah_sebelumnya','ket_tgjwb_hibah_sebelumnya','cek_dok_lain','ket_dok_lain','plt'];


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
