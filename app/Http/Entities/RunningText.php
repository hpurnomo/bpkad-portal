<?php

namespace BPKAD\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class RunningText extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'running_text';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brief','link','status'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
