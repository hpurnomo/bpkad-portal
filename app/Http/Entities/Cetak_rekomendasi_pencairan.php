<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class Cetak_rekomendasi_pencairan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cetak_rekomendasi_pencairan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['no_prop','nomor','sifat','lampiran','tanggal1','bulan1','tahun1','nomor2','tanggal2','bulan2','tahun2','hal','nomor_gubernur','tentang','nama_kepala','nip','uuid_prop','judul','status_pencairan','nominal_pencairan','file_name','text1','text2','text3','tahun3','text4','text5','text6','text7','text8','text9','terbilang','no_rekening','atas_nama','is_generate','plt'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
