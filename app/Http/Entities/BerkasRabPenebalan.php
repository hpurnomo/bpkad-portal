<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class BerkasRabPenebalan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'berkas_rab_penebalan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid_proposal_pengajuan','tahap','file_name','file_size','mime_type','file_extension'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
