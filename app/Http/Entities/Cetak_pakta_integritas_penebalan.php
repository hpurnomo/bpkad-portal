<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class Cetak_pakta_integritas_penebalan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cetak_pakta_integritas_penebalan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uuid_proposal_pengajuan','tahap','nomor1','tahun1','nomor2','tahun2','nama_pemohon','no_pemohon','jabatan_pemohon','nama_lembaga','alamat_lembaga','text1','tahun_anggaran1','nominal','terbilang','kegiatan1','totalTerbilangKegiatan1','text2','nm_skpd','text3','text4','text5','text6','jabatan_pemberi','nama_pemberi','no_pemberi','text7','is_generate'];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
