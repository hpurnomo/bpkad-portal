<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class UserLembagaLapangan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_lembaga_lapangan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_lembaga','cek_ktp_ketua','keterangan_cek_ktp_ketua',
        'cek_alamat_lembaga','keterangan_cek_alamat_lembaga','cek_akta',
        'keterangan_cek_akta','cek_npwp','keterangan_cek_npwp','cek_skd',
        'keterangan_cek_skd','izin_operasional','cek_izin_operasional',
        'keterangan_cek_izin_operasional','cek_sertifikat',
        'keterangan_cek_sertifikat',
        'cek_rekening_lembaga','keterangan_cek_rekening_lembaga',
        'cek_bantuan_tahun_sebelum','keterangan_cek_bantuan_tahun_sebelum',
        'is_approve','approve_by','created_at','updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
