<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class Cetak_nphd_penebalan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cetak_nphd_penebalan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uuid_proposal_pengajuan','tahap','hari1','tanggal1','bulan1','tahun1','nama1','nama2','ketPLT','jabatan1','alamat','pasal','nomor1','hadapan','notaris','nomor_ham','nomor2','tahun2','tanggal2','nomor3','tahun3','nomor4','tahun4','tahun_anggaran1','nomor5','tahun5','nomor6','tahun6','tahun_anggaran2','nomor7','tahun7','senilai_angka','senilai_teks','hari2','tanggal3','kegiatan1','totalTerbilangKegiatan1','kegiatan2','kegiatan3','jabatan2','nama_pihak_kedua','nip_pihak_kedua','nama_kepala','nip_kepala','is_generate','total1','total2','namaSKPD'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
