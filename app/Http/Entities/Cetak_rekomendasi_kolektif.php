<?php

namespace BPKAD\Http\Entities;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Cetak_rekomendasi_kolektif extends Model
{ 
    protected $table = 'cetak_rekomendasi_kolektif'; 
    protected $fillable = ['nomor','sifat','lampiran','tanggal','bulan','tahun','nama_kepala','nip','lembaga','is_generated','created_at','updated_at','created_by','updated_by','plt'];  
    public $timestamps = true; 
    protected $primarykey = 'id';

    public static function getData($noper){
    	return DB::table("cetak_rekomendasi_kolektif")
    		->where("noper",$noper)
    		->where("created_by",Auth::user()->user_id)->orderBy("nomor","asc");
    }
}
