<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class Lembaga extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_lembaga';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'kd_skpd','kd_lembaga','nm_lembaga','alamat','nokel','rtID','rwID',
    'no_telepon','no_fax','email','kontak_person','nik_person','no_hp_person','kontak_person_sekretaris','email_person_sekretaris','alamat_person_sekretaris','no_hp_person_sekretaris','kontak_person_bendahara','email_person_bendahara','alamat_person_bendahara','no_hp_person_bendahara','latitude','longitude',
    'tgl_registrasi','crb','crd','upb','upd','upp'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'nomor';
}
