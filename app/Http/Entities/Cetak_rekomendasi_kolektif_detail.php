<?php

namespace BPKAD\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Cetak_rekomendasi_kolektif_detail extends Model
{ 
    protected $table 		= 'cetak_rekomendasi_kolektif_detail'; 
    protected $fillable 	= ['id_kol_head','id_lembaga','created_by','updated_by'];  
    public $timestamps 		= true; 
    protected $primarykey 	= 'id';

}
