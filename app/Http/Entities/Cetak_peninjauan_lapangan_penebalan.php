<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class Cetak_peninjauan_lapangan_penebalan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cetak_peninjauan_lapangan_penebalan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uuid_proposal_pengajuan','tahap','nomor','hari','tanggal1','bulan1','tahun1','nama1','jabatan1','nama2','jabatan2','nama3','jabatan3','nama4','jabatan4','nama5','jabatan5','nomor2','tanggal2','bulan2','tahun2','nama_pengusul','nama_ketua','nip','is_generate'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
