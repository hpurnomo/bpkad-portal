<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;
use DB;

class Kelurahan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_kelurahan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'nokec','kdkel','nmkel','kdpos','crb','crd',
    'upb','upd','upp'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'nokel';

     public static function getData(){

        return DB::table("mst_kelurahan as d")
        ->select("d.nokel","d.kdkel","d.nmkel","a.nmkec","b.nmkota","c.nmprov")
        ->join("mst_kecamatan as a","d.nokec","=","a.nokec")
        ->join("mst_kota as b","a.nokota","=","b.nokota")
        ->join("mst_provinsi as c","b.noprov","=","c.noprov")
        ->orderBy("d.nmkel","asc");

    }

}
