<?php

namespace BPKAD\Http\Entities;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Database\Eloquent\Model;

class Trx_proposal_administrasi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trx_proposal_administrasi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['no_prop','cek_aktivitas','keterangan_cek_aktivitas','cek_kepengurusan','keterangan_cek_kepengurusan','cek_rab','keterangan_cek_rab','cek_waktu_pelaksanaan','keterangan_cek_waktu_pelaksanaan','is_approve','approve_by',
        'cek_identitas_alamat','ket_identitas_alamat',
        'cek_latar_belakang','ket_latar_belakang',
        'cek_maksud_tujuan','ket_maksud_tujuan',
        'cek_jadwal_pelaksanaan','ket_jadwal_pelaksanaan',
        'cek_rencana_rab','ket_rencana_rab'  
        ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key table.
     *
     * @var string
     */
    protected $primarykey = 'id';
}
