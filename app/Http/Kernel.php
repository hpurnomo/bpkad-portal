<?php

namespace BPKAD\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \BPKAD\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \BPKAD\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \BPKAD\Http\Middleware\Authenticate::class,
        'auth.admin' => \BPKAD\Http\Middleware\AuthenticateWithAdmin::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \BPKAD\Http\Middleware\RedirectIfAuthenticated::class,
        'GroupSkpd' => \BPKAD\Http\Middleware\GroupSkpd::class,
        'GroupPpkd' => \BPKAD\Http\Middleware\GroupPpkd::class,
        'GroupLembaga' => \BPKAD\Http\Middleware\GroupLembaga::class,
    ];
}
