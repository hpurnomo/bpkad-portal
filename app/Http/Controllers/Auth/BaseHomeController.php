<?php 
namespace BPKAD\Http\Controllers\Auth;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BPKAD\Http\Controllers\Auth\DatatablesController;
use BPKAD\Http\Repositories\lembagaRepository;
use BPKAD\Http\Repositories\skpdRepository;
use BPKAD\Http\Repositories\userRepository;
use BPKAD\Http\Repositories\userSkpdRepository;
use BPKAD\Http\Repositories\tokenRepository;
use BPKAD\Http\Repositories\sessionFlashRepository;

class BaseHomeController extends Controller
{
	/* roles user */
	protected $roles;
	/**
	*@var lembaga
	*/
	protected $lembaga;
	/**
	*@var skpd
	*/
	protected $skpd;
	/**
	*@var user
	*/
	protected $users;
	/**
	*@var token	
	*/
	protected $token;
	/**
	*@var sessionFlashRepository	
	*/
	protected $sessionFlash;
	/**
	*@var userSkpdRepository	
	*/
	protected $userSkpd;

	function __construct(Request $request, lembagaRepository $lembaga, skpdRepository $skpd, userRepository $users,userSkpdRepository $userSkpd,tokenRepository $token,sessionFlashRepository $sessionFlash){
		if ($request->session()->has('roles')) {
		    $this->roles = $request->session()->get('roles');
		}
		else {
			$this->roles = 'guest';
		}
		$this->lembaga = $lembaga;
		$this->skpd = $skpd;
		$this->users = $users;
		$this->token = $token;
		$this->sessionFlash = $sessionFlash;
		$this->userSkpd = $userSkpd;
	}
}

?>