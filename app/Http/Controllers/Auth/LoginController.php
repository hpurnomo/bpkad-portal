<?php 
namespace BPKAD\Http\Controllers\Auth;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

class LoginController extends BaseHomeController
{
	
	public function index(){
		return view('auth.login')->with('roles',$this->roles);
	}

}


?>