<?php

namespace BPKAD\Http\Controllers\Auth;

use BPKAD\User;
use Validator;
use BPKAD\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use BPKAD\Http\Entities\Pengumuman;


class AuthController extends Controller
{
    protected $redirectPath = '/dashboard';
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => 'required|max:255',
    //         'email' => 'required|email|max:255|unique:users',
    //         'password' => 'required|confirmed|min:6',
    //     ]);
    // }


    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login')->with('recPengumuman',Pengumuman::where('status',1)->get());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $is_email = strpos($request['username'],"@");

        if(!empty($is_email)){
            $recUser = User::where('email',$request['username'])->first();
            // If User is active
            if($recUser['is_active'] == 1)
            {
                if (Auth::attempt(['email' => $request['username'], 'password' => $request['password']]))
                {
                    //session
                    $set_session = array('userID' => $recUser['user_id'],
                                         'username' => $recUser['username'],
                                         'email' => $recUser['email'],
                                         'real_name' => $recUser['real_name'],
                                         'group_id' => $recUser['group_id'],
                                         'avatar' => $recUser['avatar'],
                                         'photo' => $recUser['photo'],
                                         'is_verify' => $recUser['is_verify']
                                        );
                    $request->session()->put($set_session);
                    return redirect()->intended($this->redirectPath());
                }
                else
                {
                    return redirect('/login')
                    ->withErrors([
                        $this->loginUsername() => 'Email/Password Anda Salah.',
                    ]);
                }
            }
            else
            {
                return redirect('/login')
                    ->withErrors([
                        $this->loginUsername() => 'Akun Anda Belum Diaktifasi.',
                    ]);
            }
        }
        else
        {
            if (is_numeric($request['username'])==true) 
            {
                $recUser = User::where('nrk',$request['username'])->first();
                // If User is active
                if($recUser['is_active'] == 1)
                {
                    if (Auth::attempt(['nrk' => $request['username'], 'password' => $request['password']]))
                    {
                        //session
                        $set_session = array('userID' => $recUser['user_id'],
                                             'username' => $recUser['username'],
                                             'email' => $recUser['email'],
                                             'real_name' => $recUser['real_name'],
                                             'group_id' => $recUser['group_id'],
                                             'avatar' => $recUser['avatar'],
                                             'photo' => $recUser['photo'],
                                             'is_verify' => $recUser['is_verify']
                                            );
                        $request->session()->put($set_session);
                        return redirect()->intended($this->redirectPath());
                    }
                    else
                    {
                        return redirect('/login')
                        ->withErrors([
                            $this->loginUsername() => 'NRK/Password Anda Salah.',
                        ]);
                    }
                }
                else
                {
                    return redirect('/login')
                        ->withErrors([
                            $this->loginUsername() => 'Akun Anda Belum Diaktifasi.',
                        ]);
                }
            }
            else
            {
                $recUser = User::where('username',$request['username'])->first();
                // If User is active
                if($recUser['is_active'] == 1)
                {
                    if (Auth::attempt(['username' => $request['username'], 'password' => $request['password']]))
                    {
                        //session
                        $set_session = array('userID' => $recUser['user_id'],
                                             'username' => $recUser['username'],
                                             'email' => $recUser['email'],
                                             'real_name' => $recUser['real_name'],
                                             'group_id' => $recUser['group_id'],
                                             'avatar' => $recUser['avatar'],
                                             'photo' => $recUser['photo'],
                                             'is_verify' => $recUser['is_verify']
                                            );
                        $request->session()->put($set_session);
                        return redirect()->intended($this->redirectPath());
                    }
                    else
                    {
                        return redirect('/login')
                        ->withErrors([
                            $this->loginUsername() => 'Username/Password Anda Salah.',
                        ]);
                    }
                }
                else
                {
                    return redirect('/login')
                        ->withErrors([
                            $this->loginUsername() => 'Akun Anda Belum Diaktifasi.',
                        ]);
                }   
            }
        }
        

        // return redirect('/login')
        //     ->withInput($request->only($this->loginUsername(), 'remember'))
        //     ->withErrors([
        //         // $this->loginUsername() => $this->getFailedLoginMessage(),
        //         $this->loginUsername() => 'Email/Password Anda Salah. ',
        //     ]);
    }
}
