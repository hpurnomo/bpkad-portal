<?php 
namespace BPKAD\Http\Controllers\Auth;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Requests\RegUserRequest;
use BPKAD\Http\Requests\TokenRequest;
use BPKAD\Http\Requests\RegUserSkpdRequest;

class RegisterController extends BaseHomeController
{
	public function index(){		
		return view('auth.register')->with('roles',$this->roles);
	}

	public function userLembaga($idLembaga){
		/**
		*@var array
		*/
		$lembaga = $this->lembaga->getRecByID($idLembaga);
		/**
		*@var array
		*/
		$skpd = $this->skpd->getRecByKDSKPD($lembaga[0]->kd_skpd);

		$data = array('lembaga' => $lembaga,
					  'skpd' => $skpd,
					 );
		return view('auth.register-user-lembaga')->with('roles',$this->roles)
												 ->with('data',$data);
	}

	public function regUser(RegUserRequest $request){
		$result = $this->users->store($request->all());
		
		if(isset($result)){
			return redirect(route('registration.status',['noReg'=>$result]));
		}
	}

	public function regStatus($noReg){
		$data = array('noReg' => $noReg);
		return view('auth.register-status')->with('roles',$this->roles)
												 ->with('data',$data);
	}

	public function aktifasiAkun($ver_token){
		$this->users->verification_token($ver_token);
		return view('auth.verifikasi-akun-status')->with('roles',$this->roles); 
	}

	/**
	* Show Form Token User SKPD
	* 
	* @return view auth.input-token
	*/
	public function inputToken(){
		return view('auth.input-token')->with('roles',$this->roles);
	}

	public function verifikasiToken(TokenRequest $request){
		$result = $this->token->check($request['token']);
		if(isset($result)){
			return redirect(route('signup.skpd.form',['skpdID'=>base64_encode($result['skpdID'])]));
		}else{
			$this->sessionFlash->errMsg('alert-warning','Nomor token yang Anda masukan tidak sesuai ! Jika ada kendala? Hubungi Kontak Kami/Hotline eHibahBansos.');
			return redirect(route('token.skpd.form'));
		}
	}

	/**
	* Show Form Reg User SKPD
	* 
	* @param Integer $skpdID
	*/
	public function userSKPD($skpdID){
		$data = array('getSkpdByID' => $this->skpd->getRecByID(base64_decode($skpdID)) );

		return view('auth.register-user-skpd')->with('data',$data)->with('roles',$this->roles);
	}

	/**
	* Store Data Register User SKPD
	* 
	* @param array $request
	*/
	public function regUserSKPD(RegUserSkpdRequest $request){
		$result = $this->userSkpd->store($request->all());
		
		if(isset($result)){
			return redirect(route('registration.status',['noReg'=>$result]));
		} 
	}


	/**
	* Show From Reg User Individu
	*/
}

?>