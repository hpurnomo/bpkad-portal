<?php 
namespace BPKAD\Http\Controllers\Auth;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Datatables;
use DB;

class DatatablesController extends Controller
{
	public function getDataTables(){
			DB::statement('SET @num:=0;');
			$lembaga = DB::table('mst_lembaga AS a')
											->select(['a.nomor','a.kd_skpd','a.kd_lembaga','a.nm_lembaga','a.alamat','b.nm_skpd'])
											->leftJoin('mst_skpd AS b','a.kd_skpd','=','b.kd_skpd');			
			
			return Datatables::of($lembaga)
					// ->addColumn('no_urut',function($lembaga){
					// 		foreach ($lembaga as $key => $value) {
					// 			return $value;
					// 		}
					// })
					->addColumn('nama_lembaga',function($lembaga){
							return '<strong style="color:red;">'.$lembaga->nm_lembaga.'</strong>';
					})
					->addColumn('action',function($lembaga){
							return '<a href="'.route('signup.lembaga.user',['id'=>$lembaga->nomor]).'" class="button button-rounded button-reveal button-mini button-red tright"><i class="icon-angle-right"></i><span>Pilih</span></a>';
					})
					->make(true);
	}
}

?>