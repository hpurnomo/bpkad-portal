<?php

namespace BPKAD\Http\Controllers\Auth;

use Illuminate\Http\Request;
use BPKAD\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use BPKAD\User;
use Mail;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getEmail()
    {
        return view('auth.password-email');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        if($this->checkEmail($request['email']) == true){
            
            $this->setRememberToken($request['email'], $request['_token']);

            $this->sendEmail($request['email'], $request['_token']);
            
            return redirect(route('password.reset.status'));
        }
        else{
            return redirect(route('password.by.email'))->withErrors(['email'=>'Email Anda Tidak Tersedia']);
        }

    }

    /**
     * check email 
     *
     * @param  string  $email
     * @return void
     */
    protected function checkEmail($email)
    {
        $arr = User::where('email',$email)->first();

            if(!empty($arr['email'])){
                return true;
            }
    }

    /**
     * set remember token at table user
     *
     * @return string
     */
    protected function setRememberToken($email, $token)
    {
        User::where('is_active', 1)
                          ->where('email', $email)
                          ->update(['remember_token' => $token]);

        return true;
    }

    /**
     * send email
     *
     * @param string email
     */
    protected function sendEmail($email, $token)
    {
        $data = ['email' => $email,
                 'token' => $token,
                ];

        Mail::send('emails.reset-password', ['data' => $data], function ($m) use ($data) {
                $m->from(config('mail')['from']['address'],config('mail')['from']['name']);

                $m->to($data['email'], '')->subject('Reset Password');
            });

        return true;
    }

    public function postReset(Request $request){
        User::where('is_active', 1)
                          ->where('remember_token', $request['remember_token'])
                          ->update(['password' => bcrypt($request['password'])]);
        
        return redirect(route('login.form'));

    }

    public function getNrk()
    {
        return view('auth.password-nrk');
    }

    /**
     * Display the status.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStatus()
    {
        return view('auth.password-reset-status');
    }

}
