<?php 
namespace BPKAD\Http\Controllers\Lembaga;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

class LembagaController extends BaseHomeController
{
	
	public function index(){
		return view('beranda.master')->with('roles',$this->roles);
	}

	public function show($tahun, $id){
		// return dd($this->fotoProposal2($tahun,$id));
		// exit();
		$data = array('lembaga' => $this->lembaga->getRecByID($id),
					  'historyTahun' => $this->getHistoryTahun($id),
					  'historyProposal' => $this->historyProposal($tahun,$id),
					  'fotoProposal1' => $this->fotoProposal1($tahun,$id),
					  'fotoProposal2' => $this->fotoProposal2($tahun,$id),
					  'fotoProposal3' => $this->fotoProposal3($tahun,$id),
					 );
		return view('lembaga.show')->with('roles',$this->roles)
								   ->with('data',$data);
	}
}


?>