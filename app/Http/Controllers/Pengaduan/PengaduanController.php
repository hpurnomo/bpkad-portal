<?php 
namespace BPKAD\Http\Controllers\Pengaduan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Requests\StorePengaduanRequest;
use BPKAD\Http\Entities\Pengaduan;

class PengaduanController extends BaseHomeController
{
	
	public function index(){
		$data = array('result' => NULL );
		return view('pengaduan.add')->with('roles',$this->roles)->with('data',$data);
	}

	public function store(StorePengaduanRequest $request){
		Pengaduan::create($request->all());

		$data = array('result' => 'success' );
		return view('pengaduan.add')->with('roles',$this->roles)->with('data',$data);
	}

}


?>