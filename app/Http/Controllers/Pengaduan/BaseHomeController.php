<?php 
namespace BPKAD\Http\Controllers\Pengaduan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseHomeController extends Controller
{
	protected $roles;

	function __construct(Request $request){
		
		if ($request->session()->has('roles')) {
		    $this->roles = $request->session()->get('roles');
		}
		else {
			$this->roles = 'guest';
		}
		
	}
}


?>