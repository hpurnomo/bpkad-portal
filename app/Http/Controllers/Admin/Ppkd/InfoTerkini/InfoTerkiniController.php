<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\InfoTerkini;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\InfoTerkini;
use Carbon\Carbon;


class InfoTerkiniController extends BaseHomeController
{

	public function manage(){
		return view('admin.ppkd.info-terkini.manage')->with('recInfoTerkini',InfoTerkini::all());
	}

	public function create(){
		return view('admin.ppkd.info-terkini.create');
	}

	public function store(Request $request){

		$this->validate($request, [
	        'file_name' => 'mimes:jpeg,png,pdf',
	    ]);
		
		$store = new InfoTerkini;
		$store->title = $request->title;
		$store->brief = $request->brief;
		$store->status = $request->status;
		$store->save();


		if ( $request->hasFile('file_name') ) {

			$update = InfoTerkini::find($store->id);
			
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$update->file_size = $request['file_name']->getClientSize();
			$update->mime_type = $request['file_name']->getClientMimeType();
			$update->file_extension = $request['file_name']->getExtension();
			$update->save();
			
			$destinationPath = 'info_terkini/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$request['file_name']->move($destinationPath, $fileName);
		}
		
		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.info.terkini.manage'));
	}

	public function edit($id){
		return view('admin.ppkd.info-terkini.edit')->with('recInfoTerkiniByID',InfoTerkini::find($id));
	}

	public function update(Request $request){

		$this->validate($request, [
	        'file_name' => 'mimes:jpeg,png,pdf',
	    ]);
		
		$update = InfoTerkini::find($request->id);
		$update->title = $request->title;
		$update->brief = $request->brief;
		$update->status = $request->status;
		$update->save();


		if ( $request->hasFile('file_name') ) {

			$update = InfoTerkini::find($request->id);
			
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$update->file_size = $request['file_name']->getClientSize();
			$update->mime_type = $request['file_name']->getClientMimeType();
			$update->file_extension = $request['file_name']->getExtension();
			$update->save();
			
			$destinationPath = 'info_terkini/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$request['file_name']->move($destinationPath, $fileName);
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.info.terkini.manage'));
	}

	public function delete(Request $request){
		$infoTerkini = InfoTerkini::find($request->id);
		$infoTerkini->delete();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('ppkd.info.terkini.manage'));
	}
}