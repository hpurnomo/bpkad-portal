<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Token;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_token;
use BPKAD\Foundation\MstTokenBuilder;
use BPKAD\Http\Entities\Skpd;
use BPKAD\Http\Requests\StoreTokenRequest;

class TokenController extends BaseHomeController
{

	public function manage(){
		return view('admin.ppkd.token.manage')->with('recToken',MstTokenBuilder::getAll());
	}

	public function create(){
		return view('admin.ppkd.token.create')->with('recSkdp',Skpd::orderBy('nm_skpd','asc')->get());
	}

	public function store(StoreTokenRequest $request){

		$store = new Mst_token;
		$store->skpdID = explode('.',$request->token)[3];
		$store->token = $request->token;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');

		return redirect(route('ppkd.pengaturan.token.manage'));
	}

	public function edit($id){
		return view('admin.ppkd.token.edit')->with('recTokenByID',MstTokenBuilder::getByID($id));
	}

	public function delete(Request $request){
		
		Mst_token::destroy($request->id);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been delete successfully. Thank you.');

		return redirect(route('ppkd.pengaturan.token.manage'));
	}

}