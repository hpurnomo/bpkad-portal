<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Token;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BPKAD\Http\Requests\UpdateUserSkpdViaPpkdRequest;
use BPKAD\User;

class BaseHomeController extends Controller
{	
	function __construct(){
		//code
	}
}

?>