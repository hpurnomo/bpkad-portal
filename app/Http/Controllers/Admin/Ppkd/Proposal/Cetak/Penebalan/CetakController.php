<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Proposal\Cetak\Penebalan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Foundation\ProposalBuilder;
use Auth;
use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_program;
use BPKAD\Http\Entities\Mst_rekening;
use BPKAD\Http\Entities\Mst_kategori_proposal;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\UserLembagaLapangan;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;
use BPKAD\Http\Entities\Cetak_kelengkapan_administrasi;
use BPKAD\Http\Entities\Cetak_detail_kelengkapan_administrasi;
use BPKAD\Http\Entities\Cetak_peninjauan_lapangan;
use BPKAD\Http\Entities\Cetak_detail_peninjauan_lapangan;
use BPKAD\Http\Entities\Cetak_rekomendasi;
use BPKAD\Http\Entities\Cetak_rekomendasi_pencairan;
use BPKAD\Foundation\CetakKelengkapanAdministrasiBuilder;
use BPKAD\Foundation\CetakPeninjauanLapanganBuilder;
use BPKAD\Foundation\CetakRekomendasiBuilder;
use BPKAD\Foundation\CetakRekomendasiPencairanBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use BPKAD\Foundation\UserLembagaLapanganBuilder;
use BPKAD\Foundation\TrxProposalAdministrasiBuilder;
use BPKAD\Foundation\TrxProposalLapanganBuilder;
use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Http\Entities\Cetak_nphd;
use Mail;
use PDF;
use Carbon\Carbon;

use BPKAD\Foundation\CetakKelengkapanAdministrasiPenebalanBuilder;
use BPKAD\Foundation\CetakPeninjauanLapanganPenebalanBuilder;
use BPKAD\Foundation\CetakRekomendasiPenebalanBuilder;
use BPKAD\Foundation\TrxProposalAdministrasiPenebalanBuilder;
use BPKAD\Foundation\TrxProposalLapanganPenebalanBuilder;
use BPKAD\Foundation\CetakNPHDPenebalanBuilder;

use BPKAD\Foundation\ProposalPenebalanTAPDBuilder;
use BPKAD\Foundation\ProposalPenebalanBANGGARBuilder;
use BPKAD\Foundation\ProposalPenebalanDDNBuilder;
use BPKAD\Foundation\ProposalDefinitifAPBDBuilder;

use BPKAD\Http\Entities\Cetak_kelengkapan_administrasi_penebalan;
use BPKAD\Http\Entities\Cetak_detail_kelengkapan_administrasi_penebalan;

use BPKAD\Http\Entities\Cetak_peninjauan_lapangan_penebalan;
use BPKAD\Http\Entities\Cetak_detail_peninjauan_lapangan_penebalan;

use BPKAD\Http\Entities\Cetak_rekomendasi_penebalan;
use BPKAD\Http\Entities\Cetak_nphd_penebalan;



class CetakController extends BaseHomeController
{

	public function show($uuid,$no_prop,$fase,$tahap,$tab){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($no_prop)->no_lembaga;	

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array kelengkapan administrasi lembaga
		$dataKal = UserLembagaBuilder::getArrayByNoLembaga($no_lembaga);

		// get array kelengkapan administrasi proposal
		$dataKap = TrxProposalAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);


		// get array cetak peninjuan lapangan
		$dataCpl = CetakPeninjauanLapanganPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array peninjauan lapangan lembaga
		$dataPll = UserLembagaLapanganBuilder::getArrayByNoLembaga($no_lembaga);

		// get array peninjauan lapangan proposal
		$dataPlp = TrxProposalLapanganPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array cetak rekomendasi
		$dataCrekomendasi = CetakRekomendasiPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByNoProp($no_prop);

		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get arrau trx proposal tiap tahapan
		if ($tahap=='tapd') {
			$recProposalPenebalanByUUID = ProposalPenebalanTAPDBuilder::getByUUID($uuid);
		}
		elseif ($tahap=='banggar') {
			$recProposalPenebalanByUUID = ProposalPenebalanBANGGARBuilder::getByUUID($uuid);
		}
		elseif ($tahap=='ddn') {
			$recProposalPenebalanByUUID = ProposalPenebalanDDNBuilder::getByUUID($uuid);
		}
		elseif ($tahap=='apbd') {
			$recProposalPenebalanByUUID = ProposalDefinitifAPBDBuilder::getByUUID($uuid);
		}

		return view('admin.ppkd.proposal.cetak.penebalan.show')
		->with('proposalID',$no_prop)
		->with('recProposalByID',ProposalBuilder::getByID($no_prop))
		->with('uuid',$uuid)
		->with('no_prop',$no_prop)
		->with('fase',$fase)
		->with('tahap',$tahap)
		->with('tab',$tab)
		->with('dataCka',$dataCka)
		->with('dataKal',$dataKal)
		->with('dataKap',$dataKap)
		->with('dataCpl',$dataCpl)
		->with('dataPll',$dataPll)
		->with('dataPlp',$dataPlp)
		->with('dataCrekomendasi',$dataCrekomendasi)
		->with('dataCrekomendasiPencairan',$dataCrekomendasiPencairan)
		->with('dataDcka',Cetak_detail_kelengkapan_administrasi_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->orderBy('no_urut','asc')->get())
		->with('dataDcpl',Cetak_detail_peninjauan_lapangan_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->orderBy('no_urut','asc')->get())
		->with('recCetakRekomendasiPencairanByNoProp',Cetak_rekomendasi_pencairan::where('no_prop',$no_prop)->get())
		->with('dataCNPHD',$dataCNPHD)
		->with('recProposalPenebalanByUUID',$recProposalPenebalanByUUID);
	}

	public function updateKal(Request $request)
	{						
		$update = Cetak_kelengkapan_administrasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 0; 

		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>$request->no_prop,'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function deleteDcka($id,$uuid,$no_prop,$fase,$tahap,$tab){
		$deleteDcka = Cetak_detail_kelengkapan_administrasi_penebalan::find($id);

		$deleteDcka->delete();

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>$request->no_prop,'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function generateKelAdministrasiToPDF(Request $request){

		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		// get array kelengkapan administrasi lembaga
		$dataKal = UserLembagaBuilder::getArrayByNoLembaga($no_lembaga);

		// get array kelengkapan administrasi proposal
		$dataKap = TrxProposalAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCka' => $dataCka,
				'dataKal' => $dataKal,
				'dataKap' => $dataKap,
				'dataDcka' => Cetak_detail_kelengkapan_administrasi_penebalan::where('uuid_proposal_pengajuan',$request->uuid)->where('tahap',$request->tahap)->orderBy('no_urut','asc')->get()
				];

		$update = Cetak_kelengkapan_administrasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 0;
		$update->save();

		// Test View
		// return view('pdf.kelengkapan-administrasi',$data);

		$pdf = PDF::loadView('pdf.kelengkapan-administrasi-penebalan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Kelengkapan-Administrasi.pdf');
	}

	public function updatePL(Request $request)
	{
		$update = Cetak_peninjauan_lapangan_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 0; 
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>$request->no_prop,'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function deleteDcpl($id,$uuid,$no_prop,$fase,$tahap,$tab){
		$deleteDcpl = Cetak_detail_peninjauan_lapangan_penebalan::find($id);

		$deleteDcpl->delete();

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab]));
	}

	public function generatePeninjauanLapanganToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCpl = CetakPeninjauanLapanganPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		// get array peninjauan lapangan lembaga
		$dataPll = UserLembagaLapanganBuilder::getArrayByNoLembaga($no_lembaga);

		// get array peninjauan lapangan proposal
		$dataPlp = TrxProposalLapanganPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCpl' => $dataCpl,
				'dataPll' => $dataPll,
				'dataPlp' => $dataPlp,
				'dataDcpl' => Cetak_detail_peninjauan_lapangan_penebalan::where('uuid_proposal_pengajuan',$request->uuid)->where('tahap',$request->tahap)->orderBy('no_urut','asc')->get()
				];

		$update = Cetak_peninjauan_lapangan_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 0;
		$update->save();

		// Test View
		// return view('pdf.peninjauan-lapangan',$data);

		$pdf = PDF::loadView('pdf.peninjauan-lapangan-penebalan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Peninjauan-Lapangan.pdf');	
	}

	public function updateRekomendasi(Request $request)
	{
		$update = Cetak_rekomendasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 0; 
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>$request->no_prop,'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function generateRekomendasiToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCrekomendasi = CetakRekomendasiPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		// get arrau trx proposal tiap tahapan
		if ($request->tahap=='tapd') {
			$recProposalPenebalanByUUID = ProposalPenebalanTAPDBuilder::getByUUID($request->uuid);
		}
		elseif ($request->tahap=='banggar') {
			$recProposalPenebalanByUUID = ProposalPenebalanBANGGARBuilder::getByUUID($request->uuid);
		}
		elseif ($request->tahap=='ddn') {
			$recProposalPenebalanByUUID = ProposalPenebalanDDNBuilder::getByUUID($request->uuid);
		}
		elseif ($request->tahap=='apbd') {
			$recProposalPenebalanByUUID = ProposalDefinitifAPBDBuilder::getByUUID($request->uuid);
		}

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCrekomendasi' => $dataCrekomendasi,
				'recProposalPenebalanByUUID'=>$recProposalPenebalanByUUID
				];

		$update = Cetak_rekomendasi_penebalan::where('uuid_proposal_pengajuan',$request->uuid)->where('tahap',$request->tahap)->first();
		$update->is_generate = 0;
		$update->save();

		// Test View
		// return view('pdf.bentuk-rekomendasi',$data);

		$pdf = PDF::loadView('pdf.bentuk-rekomendasi',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Bentuk-Rekomendasi.pdf');	
	}

	// TAMBAH REKOMENDASI PENCAIRAN
	public function tambahRekomendasiPencairan($no_prop,$uuid,$fase,$tahap,$tab)
	{		
		return view('admin.ppkd.proposal.cetak.penebalan.tambah-rekomendasi-pencairan')
		->with('no_prop',$no_prop)
		->with('uuid',$uuid)
		->with('fase',$fase)
		->with('tahap',$tahap)
		->with('tab',$tab)
		->with('recProposalByID',ProposalBuilder::getByID($no_prop));
	}

	public function storeRekomendasiPencairan(Request $request)
	{
		// FORMAT RUPIAH
		$nominal_pencairan = str_replace(".", "", $request['nominal_pencairan']);

		$store = new Cetak_rekomendasi_pencairan;
		$store->no_prop = $request->no_prop;
		$store->uuid_prop = $request->uuid;
		$store->judul = $request->judul;
		$store->nomor = $request->nomor;
		$store->sifat = $request->sifat;
		$store->lampiran = $request->lampiran;
		$store->tanggal1 = $request->tanggal1;
		$store->bulan1 = $request->bulan1;
		$store->tahun1 = $request->tahun1;
		$store->nomor2 = $request->nomor2;
		$store->tanggal2 = $request->tanggal2;
		$store->bulan2 = $request->bulan2;
		$store->tahun2 = $request->tahun2;
		$store->hal = $request->hal;
		$store->nomor_gubernur = $request->nomor_gubernur;
		$store->tentang = $request->tentang;
		$store->nama_kepala = $request->nama_kepala;
		$store->nip = $request->nip;
		$store->nominal_pencairan = doubleval(str_replace(",",".",$nominal_pencairan));
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>$request->no_prop,'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function editRekomendasiPencairan($no_prop,$uuid,$fase,$tahap,$tab,$id)
	{
		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByID($id);

		return view('admin.ppkd.proposal.cetak.penebalan.edit-rekomendasi-pencairan')
		->with('no_prop',$no_prop)
		->with('uuid',$uuid)
		->with('fase',$fase)
		->with('tahap',$tahap)
		->with('tab',$tab)
		->with('recProposalByID',ProposalBuilder::getByID($no_prop))
		->with('dataCrekomendasiPencairan',$dataCrekomendasiPencairan)
		->with('id',$id);
	}

	public function updateRekomendasiPencairan(Request $request)
	{
		CetakRekomendasiPencairanBuilder::update($request->all());

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>$request->no_prop,'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function generateRekomendasiPencairanToPDF(Request $request){
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByID($request->id);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCrekomendasiPencairan' => $dataCrekomendasiPencairan
				];

		$update = Cetak_rekomendasi_pencairan::where('id',$request->id)->first();
		$update->is_generate = 0;
		$update->save();
		
		$pdf = PDF::loadView('pdf.bentuk-rekomendasi-pencairan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Bentuk-Rekomendasi-Pencairan.pdf');	
	}

	public function generateNPHDToPDF($uuid,$fase,$tahap){

		// update is_generate cetak nphd
		$update = Cetak_nphd_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		$update->is_generate = 0;
		$update->save();


		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);

		// get array detail proposal by no_prop
		$recProposalByID = ProposalPenebalanTAPDBuilder::getByUUID($uuid);

		$data = ['dataCNPHD' => $dataCNPHD,
				 'recProposalByID' => $recProposalByID
				];

		// return view('pdf.naskah-perjanjian-nphd',$data);
		// exit();

		$pdf = PDF::loadView('pdf.naskah-perjanjian-nphd',$data)
		->setOption('page-width', '210')
		->setOption('page-height', '330')
		->setOption('margin-bottom', '20')
        ->setOption('margin-left', '20')
        ->setOption('margin-right','20')
        ->setOption('margin-top', '20');

		return $pdf->download('Naskah-Perjanjian-NPHD.pdf');
	}

	public function bukaFiturEdit($uuid,$no_prop,$fase,$tahap,$tab){

		$update = Cetak_nphd_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		$update->is_generate = 0;
		$update->save();

		return redirect(route('ppkd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab]));
	}
}