<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Proposal;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_token;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\ProposalDefinitif;
use BPKAD\Foundation\ProposalBuilder;
use BPKAD\Http\Entities\Skpd;
use BPKAD\Http\Requests\StoreTokenRequest;
use BPKAD\Http\Entities\Mst_program;
use BPKAD\Http\Entities\Mst_rekening;
use BPKAD\Http\Entities\Mst_kategori_proposal;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;
use BPKAD\Http\Entities\Cetak_nphd;
use BPKAD\Http\Entities\Periode;
use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Foundation\CetakKelengkapanAdministrasiBuilder;
use Excel;
use PDF;
use DB;

class ProposalToDefinitifController extends BaseHomeController
{

	public function migrate($noper)
	{
		$query = Proposal::where('noper',$noper)->get();

		if($query){
			// foreach ($query as $key => $value) {
			// 	$store = new 
			// }
		}
		

	}

	public function updateUUID()
	{
		DB::select('UPDATE `trx_proposal` SET `uuid`=(uuid()) WHERE 1');

		return 'success';
	}

}