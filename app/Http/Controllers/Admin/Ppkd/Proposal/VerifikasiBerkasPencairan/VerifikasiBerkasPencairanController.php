<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Proposal\VerifikasiBerkasPencairan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Foundation\ProposalBuilder;
use BPKAD\Foundation\VerifikasiBerkasPencairanBuilder;
use Auth;
use Illuminate\Http\Request;
use BPKAD\Http\Entities\VerifikasiBerkasPencairan;
use Mail;
use PDF;

class VerifikasiBerkasPencairanController extends BaseHomeController
{

	public function show($fase,$no_prop){
		return view('admin.ppkd.proposal.verifikasi-berkas-pencairan.show')->with('recProposalByID',ProposalBuilder::getByID($no_prop))
																->with('fase',$fase)
																->with('recVerifikasiBerkasPencairanByNoProp',VerifikasiBerkasPencairanBuilder::getArrayByNoProp($no_prop));
	}

	public function store(Request $request)
	{	

		$store = VerifikasiBerkasPencairan::firstOrCreate(['no_prop'=>$request->no_prop]);

		$store->no_prop = $request->no_prop;
		$store->tahun_anggaran = $request->tahun_anggaran;
		$store->no_sk_gub_penetapan = $request->no_sk_gub_penetapan;
		$store->kelompok_belanja = $request->kelompok_belanja;
		$store->no_urut_pemohon_sk = $request->no_urut_pemohon_sk;
		$store->nama_pemohon = $request->nama_pemohon;
		$store->nm_skpd = $request->nm_skpd;
		$store->cek_surat_pengantar = $request->cek_surat_pengantar;
		$store->ket_surat_pengantar = $request->ket_surat_pengantar;
		$store->cek_surat_permohonan = $request->cek_surat_permohonan;
		$store->ket_surat_permohonan = $request->ket_surat_permohonan;
		$store->cek_rab = $request->cek_rab;
		$store->ket_rab = $request->ket_rab;
		$store->cek_kwitansi = $request->cek_kwitansi;
		$store->ket_kwitansi = $request->ket_kwitansi;
		$store->cek_rekening = $request->cek_rekening;
		$store->ket_rekening = $request->ket_rekening;
		$store->cek_sk_lembaga = $request->cek_sk_lembaga;
		$store->ket_sk_lembaga = $request->ket_sk_lembaga;
		$store->cek_rekomendasi = $request->cek_rekomendasi;
		$store->ket_rekomendasi = $request->ket_rekomendasi;
		$store->cek_bak_administrasi = $request->cek_bak_administrasi;
		$store->ket_bak_administrasi = $request->ket_bak_administrasi;
		$store->cek_bap_lapangan = $request->cek_bap_lapangan;
		$store->ket_bap_lapangan = $request->ket_bap_lapangan;
		$store->cek_surat_tugas = $request->cek_surat_tugas;
		$store->ket_surat_tugas = $request->ket_surat_tugas;
		$store->cek_surat_pernyataan_tanggungjawab = $request->cek_surat_pernyataan_tanggungjawab;
		$store->ket_surat_pernyataan_tanggungjawab = $request->ket_surat_pernyataan_tanggungjawab;
		$store->cek_ktp_pengurus = $request->cek_ktp_pengurus;
		$store->ket_ktp_pengurus = $request->ket_ktp_pengurus;
		$store->cek_skd = $request->cek_skd;
		$store->ket_skd = $request->ket_skd;
		$store->cek_npwp = $request->cek_npwp;
		$store->ket_npwp = $request->ket_npwp;
		$store->cek_proposal_definitif = $request->cek_proposal_definitif;
		$store->ket_proposal_definitif = $request->ket_proposal_definitif;
		$store->cek_nphd = $request->cek_nphd;
		$store->ket_nphd = $request->ket_nphd;
		$store->cek_tanda_terima_laporan = $request->cek_tanda_terima_laporan;
		$store->ket_tanda_terima_laporan = $request->ket_tanda_terima_laporan;
		$store->cek_tanda_terima_audit = $request->cek_tanda_terima_audit;
		$store->ket_tanda_terima_audit = $request->ket_tanda_terima_audit;
		$store->cek_tanda_terima_monev = $request->cek_tanda_terima_monev;
		$store->ket_tanda_terima_monev = $request->ket_tanda_terima_monev;
		$store->cek_bukti_email = $request->cek_bukti_email;
		$store->ket_bukti_email = $request->ket_bukti_email;
		$store->nama_ketua = $request->nama_ketua;
		$store->nip = $request->nip;

		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.proposal.verifikasi.berkas.pencairan',['fase'=>$request->fase,'no_prop'=>$request->no_prop]));
	}

	public function generateToPDF(Request $request){

		$data = ['recVerifikasiBerkasPencairanByNoProp' => VerifikasiBerkasPencairanBuilder::getArrayByNoProp($request->no_prop)
				];

		$update = VerifikasiBerkasPencairan::where('no_prop',$request->no_prop)->first();
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.verifikasi-berkas-pencairan',$data);

		$pdf = PDF::loadView('pdf.verifikasi-berkas-pencairan',$data);

		return $pdf->download('Verifikasi-Berkas-Pencairan.pdf');
	}

	public function uncheck(Request $request){
		$update = VerifikasiBerkasPencairan::where('no_prop',$request->no_prop)->first();
		$update->is_generate = ($request->is_generate=='on')?1:0;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.proposal.verifikasi.berkas.pencairan',['fase'=>$request->fase,'no_prop'=>$request->no_prop]));
	}
}