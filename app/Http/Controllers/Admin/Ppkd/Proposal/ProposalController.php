<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Proposal;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_token;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Foundation\ProposalBuilder;
use BPKAD\Http\Entities\Skpd;
use BPKAD\Http\Requests\StoreTokenRequest;
use BPKAD\Http\Entities\DetailPeriodeTahapan;
use BPKAD\Http\Entities\Mst_program;
use BPKAD\Http\Entities\Mst_rekening;
use BPKAD\Http\Entities\Mst_kategori_proposal;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;
use BPKAD\Http\Entities\Cetak_nphd;
use BPKAD\Http\Entities\Periode;
use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Foundation\CetakKelengkapanAdministrasiBuilder;
use Excel;
use PDF;
use DB;
use Mail;
use Config;

use BPKAD\Http\Entities\ProposalPenebalanTAPD;
use BPKAD\Http\Entities\ProposalPenebalanBANGGAR;
use BPKAD\Http\Entities\ProposalPenebalanDDN;
use BPKAD\Http\Entities\ProposalDefinitifAPBD;

use BPKAD\Foundation\ProposalPenebalanTAPDBuilder;
use BPKAD\Foundation\ProposalPenebalanBANGGARBuilder;
use BPKAD\Foundation\ProposalPenebalanDDNBuilder;
use BPKAD\Foundation\ProposalDefinitifAPBDBuilder;


use BPKAD\Http\Entities\Trx_proposal_administrasi_penebalan;
use BPKAD\Http\Entities\Trx_proposal_lapangan_penebalan;

use BPKAD\Http\Entities\Trx_rencana_penebalan;
use BPKAD\Http\Entities\Trx_rincian_penebalan;

class ProposalController extends BaseHomeController
{

	public function index(){
		return view('admin.ppkd.proposal.index')->with('recPeriode',Periode::where('status',1)->get());
	}

	public function manage($fase,$noper){
		return view('admin.ppkd.proposal.manage')->with('recProposalDisetujui',ProposalBuilder::getProposalDisetujuiByFaseNoper($noper))
			->with('fase',$fase)
			->with('noper',$noper);
	}

	public function create(){
		return view('admin.ppkd.token.create')->with('recSkdp',Skpd::all());
	}

	public function store(StoreTokenRequest $request){

		$store = new Mst_token;
		$store->skpdID = $request->skpdID;
		$store->token = $request->token;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');

		return redirect(route('ppkd.pengaturan.token.manage'));
	}

	public function edit($fase,$id){
		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiBuilder::getArrayByNoProp($id);

		// get noper
		$noper = ProposalBuilder::getByID($id)->noper;

		$recProposalByID = ProposalBuilder::getByID($id);

		return view('admin.ppkd.proposal.edit')
		->with('proposalID',$id)
		->with('recProposalByID',$recProposalByID)
		->with('recAllProgram',Mst_program::orderBy('nm_program','asc')->get())
		->with('recAllRekening',Mst_rekening::orderBy('nm_rekening','asc')->get())
		->with('recAllKategoriProposal',Mst_kategori_proposal::orderBy('nama','asc')->get())
		->with('recProposalAdministrasiByNoProp',Trx_proposal_administrasi::where('no_prop',$id)->first())
		->with('recProposalLapanganByNoProp',Trx_proposal_lapangan::where('no_prop',$id)->first())
		->with('rencana_anggaran',Trx_rencana::where('no_prop',$id)->sum('rencana_anggaran_skpd'))
		->with('recRencanaByNoProp',Trx_rencana::where('no_prop',$id)->get())
		->with('fase',$fase)
		->with('noper',$noper)
		->with('uuid',$recProposalByID->uuid)
		->with('no_prop',$recProposalByID->no_prop)
		->with('dataCka',$dataCka)
		->with('recDetailPeriodeTahapanByNoper',DetailPeriodeTahapan::where('periodeID',$noper)->orderBy('tahapanID','asc')->get())

		// tapd
		->with('recProposalPenebalanTAPDByUUID',ProposalPenebalanTAPDBuilder::getByUUID($recProposalByID->uuid))
		->with('recRencanaByUUIDTAPD',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->get())
		->with('rencana_anggaran_tapd',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanTAPDByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->first())
		->with('recProposalLapanganPenebalanTAPDByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->first())

		// banggar
		->with('recProposalPenebalanBANGGARByUUID',ProposalPenebalanBANGGARBuilder::getByUUID($recProposalByID->uuid))
		->with('recRencanaByUUIDBANGGAR',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->get())
		->with('rencana_anggaran_banggar',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->first())
		->with('recProposalLapanganPenebalanBANGGARByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->first())

		// ddn
		->with('recProposalPenebalanDDNByUUID',ProposalPenebalanDDNBuilder::getByUUID($recProposalByID->uuid))
		->with('recRencanaByUUIDDDN',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->get())
		->with('rencana_anggaran_ddn',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanDDNByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->first())
		->with('recProposalLapanganPenebalanDDNByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->first())

		// APBD
		->with('recProposalDefinitifAPBDByUUID',ProposalDefinitifAPBDBuilder::getByUUID($recProposalByID->uuid))
		->with('recRencanaByUUIDAPBD',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->get())
		->with('rencana_anggaran_apbd',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanAPBDByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->first())
		->with('recProposalLapanganPenebalanAPBDByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->first())
		;
	}

	public function update(Request $request){

		$this->proposalRepository->update_status($request->all());

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');
	
		return redirect(route('ppkd.proposal.edit',['id'=>$request['proposalID']]));
	}

	public function updateProsesAnggaran(Request $request){

		// convert string to double
		$nominal_tapd = str_replace(".", "", $request->nominal_tapd);
		$nominal_banggar = str_replace(".", "", $request->nominal_banggar);
		$nominal_ddn = str_replace(".", "", $request->nominal_ddn);
		$nominal_apbd = str_replace(".", "", $request->nominal_apbd);

  		Proposal::where('no_prop',$request->no_prop)
  		->update(['nominal_tapd' => doubleval(str_replace(",",".",$nominal_tapd)),
				  // 'is_penebalan_nominal_tapd' => ($request->is_penebalan_nominal_tapd=="on")?1:0,
				  'nominal_banggar' => doubleval(str_replace(",",".",$nominal_banggar)),
				  // 'is_penebalan_nominal_banggar' => ($request->is_penebalan_nominal_banggar=="on")?1:0,
				  'nominal_ddn' => doubleval(str_replace(",",".",$nominal_ddn)),
				  // 'is_penebalan_nominal_ddn' => ($request->is_penebalan_nominal_ddn=="on")?1:0,
				  'nominal_apbd' => doubleval(str_replace(",",".",$nominal_apbd)),
				  // 'is_penebalan_nominal_apbd' => ($request->is_penebalan_nominal_apbd=="on")?1:0
				 ]);


		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
	
		return redirect(route('ppkd.proposal.edit',['fase'=>$request->fase,'id'=>$request->no_prop]));
	}

	public function exportExcel($noper){
		$fase='Proposal';
		$result = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',1)
										->where('a.noper',$noper)
										->where('a.status_kirim',1)->get(); 

        $data = json_decode(json_encode($result), true);

            Excel::create($fase.'::Export To Excel', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Export To Excel');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');

	}

	public function exportToBudgeting($fase){
		switch ($fase) {
			case 'penetapan':
				$noper = 4;
				break;
			case 'perubahan':
				$noper = 5;
				break;
			case 'pengusulan':
				$noper = 6;
				break;
		}
	 
		$result = DB::table('trx_proposal as a')->select('a.*',
										'b.nomor as no_lembaga','b.nm_lembaga','b.kd_lembaga','b.alamat as alamatLembaga','b.npwp as npwpLembaga','b.no_akta','b.tgl_akta','b.no_surat_domisili','b.tgl_surat_domisili','b.no_sertifikat','b.tgl_sertifikat','b.no_izin_operasional','b.tgl_izin_operasional','b.is_verify',
											'c.nm_skpd')
										->leftJoin('mst_lembaga as b', 'a.no_lembaga', '=', 'b.nomor')
										->leftJoin('mst_skpd as c', 'a.no_skpd', '=', 'c.nomor')
										->where('a.status_prop',1)
										->where('a.noper',$noper)
										->where('a.status_kirim',1)->get(); 

        $data = json_decode(json_encode($result), true);

            Excel::create($fase.'::Export To Budgeting', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Export To Budgeting');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

	public function showNaskahNPHD($no_prop,$fase){
		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDBuilder::getArrayByNoProp($no_prop);

		return view('admin.ppkd.proposal.show-naskah-nphd')->with('no_prop',$no_prop)
														->with('fase',$fase)
														->with('dataCNPHD',$dataCNPHD)
														->with('recProposalByID',ProposalBuilder::getByID($no_prop));
	}

	public function updateNaskahNPHD(Request $request){

		$update = Cetak_nphd::firstOrCreate(['no_prop'=>$request->no_prop]);
		$update->hari1 = $request->hari1;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nama1 = $request->nama1;
		$update->nama2 = $request->nama2;
		$update->jabatan1 = $request->jabatan1;
		$update->alamat = $request->alamat;
		$update->pasal = $request->pasal;
		$update->nomor1 = $request->nomor1;
		$update->hadapan = $request->hadapan;
		$update->notaris = $request->notaris;
		$update->nomor_ham = $request->nomor_ham;
		$update->nomor2 = $request->nomor2;
		$update->tahun2 = $request->tahun2;
		$update->tanggal2 = $request->tanggal2;
		$update->nomor3 = $request->nomor3;
		$update->tahun3 = $request->tahun3;
		$update->peraturan4 = $request->peraturan4;
		$update->nomor4 = $request->nomor4;
		$update->tahun4 = $request->tahun4;
		$update->tentang4 = $request->tentang4;
		$update->tahun_anggaran1 = $request->tahun_anggaran1;
		$update->nomor5 = $request->nomor5;
		$update->tahun5 = $request->tahun5;
		$update->nomor6 = $request->nomor6;
		$update->tahun6 = $request->tahun6;
		$update->tentang6 = $request->tentang6;
		$update->tahun_anggaran2 = $request->tahun_anggaran2;
		$update->nomor7 = $request->nomor7;
		$update->tanggal7 = $request->tanggal7;
		$update->bulan7 = $request->bulan7;
		$update->tahun7 = $request->tahun7;
		$update->senilai_angka = $request->senilai_angka;
		$update->senilai_teks = $request->senilai_teks;
		$update->hari2 = $request->hari2;
		$update->tanggal3 = $request->tanggal3;
		$update->kegiatan1 = $request->kegiatan1;
		$update->kegiatan2 = $request->kegiatan2;
		$update->kegiatan3 = $request->kegiatan3;
		$update->jabatan2 = $request->jabatan2;
		$update->nama_pihak_kedua = $request->nama_pihak_kedua;
		$update->nip_pihak_kedua = $request->nip_pihak_kedua;
		$update->nama_kepala = $request->nama_kepala;
		$update->nip_kepala = $request->nip_kepala;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.proposal.naskah.perjanjian.nphd.show',['no_prop'=>$request->no_prop,'fase'=>$request->fase]));

	}

	public function notifApproveProposal(Request $request){
 
		$recProposalByID = ProposalBuilder::getByID($request->no_prop);   
	 	 
		$periode= Periode::where("noper",$recProposalByID->noper)->first();  
		// dd($request->all()); 
        	$data = ['jdl_prop' 	=> $recProposalByID->jdl_prop,
        			 'no_lembaga' 	=> $recProposalByID->no_lembaga,
          			 'email_lembaga' => $recProposalByID->email_lembaga,
          			 // 'email_lembaga' => 'muhamad.sidik@rucika.co.id',
          			 // 'email' 	=> $request->email, 
          			 'email_skpd' 	=>  $recProposalByID->email_skpd,
          			 'nama_lembaga' => $recProposalByID->nm_lembaga,   
          			 'real_name' 		=> $recProposalByID->real_name,   
          			 'nm_skpd' 		=> $recProposalByID->nm_skpd,     
          			 'periode' 		=> $periode->keterangan,   
          			 'tahapan' 		=> $request->tahapan 
          			];

          	try{

			    Mail::send('emails.kirim-info-proposal-apbd-approval-lembaga', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name')); 
		            $m->to($data['email_lembaga'], $data['nama_lembaga'])->subject('Info Proposal Persetujuan tahap APBD');
		        });

			    if($data['email_skpd'] != null){
				    Mail::send('emails.kirim-info-proposal-apbd-approval-skpd', ['data' => $data], function ($m) use ($data) {
				        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name')); 
			            $m->to($data['email_skpd'], $data['nm_skpd'])->subject('Info Proposal Persetujuan tahap APBD');
			        });
			    }
		       

		    	return response()->json(['status'=>true,'title'=>'Success','message'=>'Email telah dikirim']);  
			}
			catch(Exception $e){
			    return response()->json(['status'=>true,'title'=>'Success','message'=>$e]);  
			}
 
          
	}

}