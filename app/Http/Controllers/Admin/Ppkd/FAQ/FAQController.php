<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\FAQ;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\FaQ;
use Carbon\Carbon;


class FAQController extends BaseHomeController
{

	public function manage(){
		return view('admin.ppkd.faq.manage')->with('recFaQ',FaQ::orderBy('no_urut','asc')->get());
	}

	public function create(){
		return view('admin.ppkd.faq.create');
	}

	public function store(Request $request){

		$store = new FaQ;
		$store->no_urut = $request->no_urut;
		$store->question = strtoupper($request->question);
		$store->answer = strtoupper($request->answer);
		$store->status = $request->status;
		$store->save();
		
		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.faq.manage'));
	}

	public function edit($id){
		return view('admin.ppkd.faq.edit')->with('recFaQByID',FaQ::find($id));
	}

	public function update(Request $request){

		$update = FaQ::find($request->id);
		$update->no_urut = $request->no_urut;
		$update->question = strtoupper($request->question);
		$update->answer = strtoupper($request->answer);
		$update->status = $request->status;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.faq.manage'));
	}

	public function delete(Request $request){
		$testimonial = Testimonial::find($request->id);
		$testimonial->delete();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('ppkd.testimonial.manage'));
	}
}