<?php

namespace BPKAD\Http\Controllers\Admin\Ppkd\Kabupaten;

use Illuminate\Http\Request;

use BPKAD\Http\Requests;
use BPKAD\Http\Controllers\Controller;
use BPKAD\Http\Entities\Kabupaten;
use BPKAD\Http\Entities\Provinsi;
use Datatables;
use DB;

class KabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function index()
    { 
        $data['column'] = "   
                    {data: 'nokota'},
                    {data: 'kdkota'},
                    {data: 'nmkota'}, 
                    {data: 'nmprov'}, 
                    {data: 'action',orderable: false, searchable: false}
                    "; 

        $data['columnConf'] = "
          {
            'targets': 0,
            'orderable':true, 
          },
          {
            'targets': 1, 
            'className': 'text-left',
          },
           {
            'targets': 2,
            'className': 'text-left',
          },
           {
            'targets': 3,
            'className': 'text-center',
          },
           {
            'targets': 4,
            'className': 'text-center',
          }"; 
        return view('admin.ppkd.kabupaten.index',$data);

    }

    public function getData(){  
        $model = Kabupaten::getData(); 
        DB::statement('SET @num:=0;');
        return Datatables::of($model)
        ->addColumn('action',function($model){
            $action = '<a href="'.route('ppkd.master.kabupaten.edit',$model->nokota).'"> <i class="fa fa-pencil"></i> Edit </a>';
            $action .= '<a href="'.route('ppkd.master.kabupaten.delete',$model->nokota).'" onClick="return  confirm(\'Yakin akan hapus data ini \');"> <i class="fa fa-trash"></i> Delete </a>';

            return $action;
        
        })
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_prov = [];
        $data_prov =Provinsi::all();
        $list_prov = [''=>'-- Pilih Provinsi -- '];
        foreach ($data_prov as $row) 
        {
            $list_prov[$row->noprov] = $row->nmprov;
        }

        $data['list_prov'] = $list_prov;
        return view('admin.ppkd.kabupaten.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Kabupaten; 
        $store->noprov = $request->noprov;
        $store->kdkota = $request->kdkota;
        $store->nmkota = $request->nmkota; 
        $store->save();

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.kabupaten.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['kabupaten'] = Kabupaten::where("nokota",$id)->first();

        $list_prov = [];
        $data_prov =Provinsi::all();
        $list_prov = [''=>'-- Pilih Provinsi -- '];
        foreach ($data_prov as $row) 
        {
            $list_prov[$row->noprov] = $row->nmprov;
        }

        $data['list_prov'] = $list_prov; 
       return view('admin.ppkd.kabupaten.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        Kabupaten::where('nokota',$request->nokota)->update([
            'kdkota'=>$request->kdkota,
            'nmkota'=>$request->nmkota,
            'noprov'=>$request->noprov,
        ]); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.kabupaten.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Kabupaten::where('nokota',$id)->delete(); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been delete successfully.');

        return redirect(route('ppkd.master.kabupaten.index'));
    }

    public  function getList(Request $request)
    {
        $kabupaten = Kabupaten::where("noprov",$request->noprov)->orderBy("nmkota","asc")->get(); 
        $return ='';
        foreach ($kabupaten as $key => $value) {
            $return .='<option value="'.$value->nokota.'">'.$value->nmkota.'</option>';
        }
        echo $return;
    }
}
