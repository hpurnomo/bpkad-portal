<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\PeriodeTahapan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\DetailPeriodeTahapan;
use BPKAD\Foundation\DetailPeriodeTahapanBuilder;
use Auth;
use DB;

class PeriodeTahapanController extends BaseHomeController
{
	use DetailPeriodeTahapanBuilder;

	public function manage(){
		return view('admin.ppkd.periode-tahapan.manage')->with('recPeriode',Periode::orderBy('tahun', 'asc')->get());
	}

	public function edit($id){
		return view('admin.ppkd.periode-tahapan.edit')->with('id',$id)
														->with('recDetailPeriodeTahapanByID',$this->getDetailPeriodeTahapanByID($id));
	}

	public function update(Request $request){
		
		$update = DetailPeriodeTahapan::find($request->id);
		$update->mulai = $request->mulai;
		$update->selesai = $request->selesai;
		$update->keterangan = $request->keterangan;
		$update->status = $request->status;
		$update->save();
		
		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.periode.tahapan.manage'));
	}


}