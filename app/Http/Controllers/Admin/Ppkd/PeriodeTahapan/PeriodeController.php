<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\PeriodeTahapan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\PeriodeLog;
use BPKAD\Http\Entities\Mst_tahapan;
use BPKAD\Http\Entities\DetailPeriodeTahapan;
use Auth;
use DB;

class PeriodeController extends BaseHomeController
{
	public function create(){
		return view('admin.ppkd.periode-tahapan.periode.create');
	}

	public function store(Request $request)
	{
  		$store = new Periode;
		$store->tahun = $request->tahun;
		$store->tahun_proses = $request->tahun_proses;
		$store->jenisPeriodeID = $request->jenisPeriodeID;
		$store->keterangan = $request->keterangan;
		$store->mulai = $request->mulai;
		$store->selesai = $request->selesai;
		$store->status = $request->status;
		$store->bg_color = $request->bg_color;
		$store->proposalDefinitif = 0;
		$store->pengajuan_prop = $request->pengajuan_prop;
		$store->rek_anggaran = $request->rek_anggaran;
		$store->definitif = $request->definitif;
		$store->rekomendasiKolektif = $request->rekomendasiKolektif;
		$store->save();

		$recTahapan = Mst_tahapan::where('status',1)->get();

		foreach ($recTahapan as $key => $value) {
			$update = new DetailPeriodeTahapan;
			$update->periodeID = $store->id;
			$update->tahapanID = $value->id;
			$update->status = 0;
			$update->mulai = $request->mulai;
			$update->selesai = $request->selesai;
			$update->keterangan = "";
			$update->save();
		}

		$store = new PeriodeLog;
		$store->status_periode 	= $request->status;
		$store->definitif 		= $request->definitif;
		$store->pengajuan_prop 	= $request->pengajuan_prop;
		$store->rek_anggaran 	= $request->rek_anggaran; 
		$store->user_id 		= $request->session()->get('userID'); 
		$store->created_at 		= date("Y-m-d H:i:s");
		$store->noper 			= $store->id;
	 
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');

		return redirect(route('ppkd.periode.tahapan.manage'));		
	}

	public function edit($noper){
		
		return view('admin.ppkd.periode-tahapan.periode.edit')->with('noper',$noper)
															->with('recPeriodeByID',Periode::where('noper',$noper)->first());
	}

	public function update(Request $request){ 
		$update = Periode::where('noper',$request->noper)->update([
																'tahun' => $request->tahun,
																'tahun_proses' => $request->tahun_proses,
																'jenisPeriodeID' => $request->jenisPriodeID,
																'keterangan' => $request->keterangan,
																'mulai' => $request->mulai,
																'selesai' => $request->selesai,
																'status' => $request->status,
																'pengajuan_prop' => $request->pengajuan_prop,
																'rek_anggaran' => $request->rek_anggaran,
																'definitif' => $request->definitif, 
																'rekomendasiKolektif' => $request->rekomendasiKolektif, 
																'bg_color' => $request->bg_color
																]);

		$store = new PeriodeLog;
		$store->status_periode 	= $request->status;
		$store->definitif 		= $request->definitif;
		$store->rekomendasiKolektif 		= $request->rekomendasiKolektif;
		$store->pengajuan_prop 	= $request->pengajuan_prop;
		$store->rek_anggaran 	= $request->rek_anggaran; 
		$store->user_id 		= $request->session()->get('userID'); 
		$store->noper 			= $request->noper;
		$store->created_at 		= date("Y-m-d H:i:s");
	 
		$store->save();


		if($request->status == 0){
			$update = DetailPeriodeTahapan::where('periodeID',$request->noper)->update(['status'=>0]); 
		} 

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');

		return redirect(route('ppkd.periode.tahapan.manage'));
	}
}