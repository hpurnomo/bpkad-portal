<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Testimonial;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\Testimonial;
use Carbon\Carbon;


class TestimonialController extends BaseHomeController
{

	public function manage(){
		return view('admin.ppkd.testimonial.manage')->with('recTestimonial',Testimonial::all());
	}

	public function create(){
		return view('admin.ppkd.testimonial.create');
	}

	public function store(Request $request){

		$this->validate($request, [
	        'file_name' => 'mimes:jpeg,png,pdf',
	    ]);
		
		$store = new Testimonial;
		$store->name = $request->name;
		$store->position = $request->position;
		$store->brief = $request->brief;
		$store->status = $request->status;
		$store->save();


		if ( $request->hasFile('file_name') ) {

			$update = Testimonial::find($store->id);
			
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$update->file_size = $request['file_name']->getClientSize();
			$update->mime_type = $request['file_name']->getClientMimeType();
			$update->file_extension = $request['file_name']->getExtension();
			$update->save();
			
			$destinationPath = 'foto_user/avatar/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$request['file_name']->move($destinationPath, $fileName);
		}
		
		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.testimonial.manage'));
	}

	public function edit($id){
		return view('admin.ppkd.testimonial.edit')->with('recTestimonialByID',Testimonial::find($id));
	}

	public function update(Request $request){

		$this->validate($request, [
	        'file_name' => 'mimes:jpeg,png,pdf',
	    ]);
		
		$update = Testimonial::find($request->id);
		$update->name = $request->name;
		$update->position = $request->position;
		$update->brief = $request->brief;
		$update->status = $request->status;
		$update->save();


		if ( $request->hasFile('file_name') ) {

			$update = Testimonial::find($request->id);
			
			$update->file_name = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$update->file_size = $request['file_name']->getClientSize();
			$update->mime_type = $request['file_name']->getClientMimeType();
			$update->file_extension = $request['file_name']->getExtension();
			$update->save();
			
			$destinationPath = 'foto_user/avatar/';
			$fileName = Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request['file_name']->getClientOriginalName());
			$request['file_name']->move($destinationPath, $fileName);
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.testimonial.manage'));
	}

	public function delete(Request $request){
		$testimonial = Testimonial::find($request->id);
		$testimonial->delete();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('ppkd.testimonial.manage'));
	}
}