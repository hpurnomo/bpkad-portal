<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\RunningText;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\RunningText;

class RunningTextController extends BaseHomeController
{

	public function manage(){
		return view('admin.ppkd.running-text.manage')->with('recRunningText',RunningText::all());
	}

	public function create(){
		return view('admin.ppkd.running-text.create');
	}

	public function store(Request $request){
		
		$store = new RunningText;
		$store->brief = $request->brief;
		$store->link = $request->link;
		$store->status = $request->status;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.running.text.manage'));
	}

	public function edit($id){
		return view('admin.ppkd.running-text.edit')->with('recRunningTextByID',RunningText::find($id));
	}

	public function update(Request $request){

		$update = RunningText::find($request->id);
		$update->brief = $request->brief;
		$update->link = $request->link;
		$update->status = $request->status;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.running.text.manage'));
	}

	public function delete(Request $request){
		$runningText = RunningText::find($request->id);
		$runningText->delete();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('ppkd.running.text.manage'));
	}
}