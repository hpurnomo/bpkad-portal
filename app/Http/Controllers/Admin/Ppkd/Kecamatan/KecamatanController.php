<?php

namespace BPKAD\Http\Controllers\Admin\Ppkd\Kecamatan;

use Illuminate\Http\Request;

use BPKAD\Http\Requests;
use BPKAD\Http\Controllers\Controller;
use BPKAD\Http\Entities\Kecamatan;
use BPKAD\Http\Entities\Kabupaten;
use BPKAD\Http\Entities\Provinsi;
use Datatables;
use DB;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function index()
    { 
        $data['column'] = "   
                    {data: 'nokec'}, 
                    {data: 'kdkec'},
                    {data: 'nmkec'}, 
                    {data: 'nmkota'}, 
                    {data: 'nmprov'}, 
                    {data: 'action',orderable: false, searchable: false}
                    "; 

        $data['columnConf'] = "
          {
            'targets': 0,
            'orderable':true, 
          },
          {
            'targets': 1, 
            'className': 'text-left',
          },
           {
            'targets': 2,
            'className': 'text-left',
          },
           {
            'targets': 3,
            'className': 'text-center',
          },
           {
            'targets': 4,
            'className': 'text-center',
          },
           {
            'targets': 5,
            'className': 'text-center',
          }"; 
        return view('admin.ppkd.kecamatan.index',$data);

    }

    public function getData(){  
        $model = Kecamatan::getData();  
        return Datatables::of($model)
        ->addColumn('action',function($model){
            $action = '<a href="'.route('ppkd.master.kecamatan.edit',$model->nokec).'"> <i class="fa fa-pencil"></i> Edit </a>';
            $action .= '<a href="'.route('ppkd.master.kecamatan.delete',$model->nokec).'" onClick="return  confirm(\'Yakin akan hapus data ini \');"> <i class="fa fa-trash"></i> Delete </a>'; 
            return $action; 
        })
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_prov = [];
        $data_prov =Provinsi::all();
        $list_prov = [''=>'-- Pilih Provinsi -- '];
        foreach ($data_prov as $row) 
        {
            $list_prov[$row->noprov] = $row->nmprov;
        }

        $list_kab = [];
        $data_kab =Kabupaten::all();
        $list_kab = [''=>'-- Pilih Kota/Kab -- '];
        foreach ($data_kab as $row) 
        {
            $list_kab[$row->nokota] = $row->nmkota;
        } 

        $data['list_prov'] = $list_prov;
        $data['list_kab'] = $list_kab;
        return view('admin.ppkd.kecamatan.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Kecamatan; 
        $store->nokota  = $request->nokota;
        $store->kdkec   = $request->kdkec;
        $store->nmkec   = $request->nmkec; 
        $store->save();

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.kecamatan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['kecamatan'] = DB::table("mst_kecamatan as a")
        ->select("a.*","c.noprov")
        ->join("mst_kota as b","a.nokota","=","b.nokota")
        ->join("mst_provinsi as c","b.noprov","=","b.noprov")
        ->where("a.nokec",$id)
        ->first();

        $list_prov = [];
        $data_prov =Provinsi::all();
        $list_prov = [''=>'-- Pilih Provinsi -- '];
        foreach ($data_prov as $row) 
        {
            $list_prov[$row->noprov] = $row->nmprov;
        }

        $list_kab = [];
        $data_kab =Kabupaten::all();
        $list_kab = [''=>'-- Pilih Kota/Kabupaten -- '];
        foreach ($data_kab as $row) 
        {
            $list_kab[$row->nokota] = $row->nmkota;
        }

        $data['list_prov'] = $list_prov; 
        $data['list_kab'] = $list_kab; 
       return view('admin.ppkd.kecamatan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        Kecamatan::where('nokec',$request->nokec)->update([
            'kdkec'=>$request->kdkec,
            'nmkec'=>$request->nmkec,
            'nokota'=>$request->nokota,
        ]); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.kecamatan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Kecamatan::where('nokec',$id)->delete(); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been delete successfully.');

        return redirect(route('ppkd.master.kecamatan.index'));
    }

    public  function getList(Request $request)
    {
        $kecamatan = Kecamatan::where("nokota",$request->nokota)->orderBy("nmkec","asc")->get(); 
        $return ='';
        foreach ($kecamatan as $key => $value) {
            $return .='<option value="'.$value->nokec.'">'.$value->nmkec.'</option>';
        }
        echo $return;
    }

}
