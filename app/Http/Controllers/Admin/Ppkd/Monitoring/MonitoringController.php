<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Monitoring;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\SP2d;
use Datatables;
use BPKAD\Services\Sp2dApi;
use DB;
use Auth;
use BPKAD\Http\Entities\Skpd;

class MonitoringController extends BaseHomeController
{ 
	public function manage(){
		$data['column'] = "   
                    {data: 'tahun'}, 
                    {data: 'KODE_SKPD'},
                    {data: 'NAMA_SKPD'},
                    {data: 'KODE_KEGIATAN'},
                    {data: 'NAMA_KEGIATAN'},
                    {data: 'ALAMAT_KEGIATAN'},
                    {data: 'KODE_AKUN'},
                    {data: 'NAMA_AKUN'},
                    {data: 'NILAI_ANGGARAN_MURNI'},
                    {data: 'NILAI_ANGGARAN_PERUBAHAN'},
                    {data: 'TGL_SP2D'},
                    {data: 'NILAI_SP2D'} 
                    "; 

        $data['columnConf'] = "
	      {
	        'targets': 0,
	        'orderable':true, 
	      },
	      {
	        'targets': 1, 
	        'className': 'text-left',
	      },
	       {
	        'targets': 2,
	        'className': 'text-left',
	      },
	       {
	        'targets': 3,
	        'className': 'text-left',
	      },
	       {
	        'targets': 4,
	        'className': 'text-left',
	      },
	       {
	        'targets': 5,
	        'className': 'text-left',
	      },
	       {
	        'targets': 6,
	        'className': 'text-left',
	      },
	       {
	        'targets': 7,
	        'className': 'text-left',
	      },
	       {
	        'targets': 8,
	        'className': 'text-left',
	      },
	       {
	        'targets': 9,
	        'className': 'text-left',
	      },
	       {
	        'targets': 10,
	        'className': 'text-left',
	      } "; 
		return view('admin.ppkd.monitoring.manage',$data);
	}

	public function getData(){ 
		
		if(Auth::user()->group_id==10){
			$skpd = Skpd::where("nomor",Auth::user()->no_skpd)->first();
			$model = SP2d::where("KODE_SKPD",$skpd->kd_skpd)->get();  
		}else{ 
			$model = SP2d::all(); 
		}

        return Datatables::of($model) 
        ->make(true);
    }

    public function cekApi($tahun,$kegiatan){
    	$sp2d = SP2d::where('FLAG',0)->limit(500)->get(); 
    	foreach ($sp2d as $key => $value) {
    		$CHECK        = new Sp2dApi;   
	        $checkSp2d    = $CHECK->getData($value->tahun,$value->no_kegiatan);   
	        $result = $checkSp2d['data']; 
    		$store = Sp2d::find($value->id);
    		$store->KODE_SKPD = $result->results[0]->{'KODE_SKPD'};
    		$store->NAMA_SKPD = $result->results[0]->{'NAMA_SKPD'};
    		$store->KODE_KEGIATAN = $result->results[0]->{'KODE_KEGIATAN'};
    		$store->NAMA_KEGIATAN = $result->results[0]->{'NAMA_KEGIATAN'};
    		$store->ALAMAT_KEGIATAN = $result->results[0]->{'ALAMAT_KEGIATAN'};
    		$store->KODE_AKUN = $result->results[0]->{'KODE_AKUN'};
    		$store->NAMA_AKUN = $result->results[0]->{'NAMA_AKUN'};
    		$store->NILAI_ANGGARAN_MURNI = $result->results[0]->{'NILAI_ANGGARAN_MURNI'};
    		$store->NILAI_ANGGARAN_PERUBAHAN = $result->results[0]->{'NILAI_ANGGARAN_PERUBAHAN'};
    		$store->NO_SP2D = $result->results[0]->{'NO_SP2D'};
    		$store->TGL_SP2D = $result->results[0]->{'TGL_SP2D'};
    		$store->NILAI_SP2D = $result->results[0]->{'NILAI_SP2D'};
    		$store->FLAG =1;
    		$store->save();
    		echo $value->id."-> OKE <br>";
    	}  
        
    }

}