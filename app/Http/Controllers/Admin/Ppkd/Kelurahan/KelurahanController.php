<?php

namespace BPKAD\Http\Controllers\Admin\Ppkd\Kelurahan;

use Illuminate\Http\Request;

use BPKAD\Http\Requests;
use BPKAD\Http\Controllers\Controller;
use BPKAD\Http\Entities\Kelurahan;
use BPKAD\Http\Entities\Kecamatan;
use BPKAD\Http\Entities\Kabupaten;
use BPKAD\Http\Entities\Provinsi;
use Datatables;
use DB;

class KelurahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function index()
    { 
        $data['column'] = "   
                    {data: 'nokel'}, 
                    {data: 'kdkel'},
                    {data: 'nmkel'}, 
                    {data: 'nmkec'}, 
                    {data: 'nmkota'}, 
                    {data: 'nmprov'}, 
                    {data: 'action',orderable: false, searchable: false}
                    "; 

        $data['columnConf'] = "
          {
            'targets': 0,
            'orderable':true, 
          },
          {
            'targets': 1, 
            'className': 'text-left',
          },
           {
            'targets': 2,
            'className': 'text-left',
          },
           {
            'targets': 3,
            'className': 'text-center',
          },
           {
            'targets': 4,
            'className': 'text-center',
          },
           {
            'targets': 5,
            'className': 'text-center',
          },
           {
            'targets': 6,
            'className': 'text-center',
          }"; 
        return view('admin.ppkd.kelurahan.index',$data);

    }

    public function getData(){  
        $model = Kelurahan::getData();  
        return Datatables::of($model)
        ->addColumn('action',function($model){
            $action = '<a href="'.route('ppkd.master.kelurahan.edit',$model->nokel).'"> <i class="fa fa-pencil"></i> Edit </a>';
            $action .= '<a href="'.route('ppkd.master.kelurahan.delete',$model->nokel).'" onClick="return  confirm(\'Yakin akan hapus data ini \');"> <i class="fa fa-trash"></i> Delete </a>'; 
            return $action; 
        })
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_prov = [];
        $data_prov =Provinsi::all();
        $list_prov = [''=>'-- Pilih Provinsi -- '];
        foreach ($data_prov as $row) 
        {
            $list_prov[$row->noprov] = $row->nmprov;
        }
 

        $data['list_prov'] = $list_prov; 
        return view('admin.ppkd.kelurahan.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Kelurahan; 
        $store->nokec  = $request->nokec;
        $store->kdkel   = $request->kdkel;
        $store->nmkel   = $request->nmkel; 
        $store->save();

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.kelurahan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['kelurahan'] = DB::table("mst_kelurahan as a")
        ->select("a.*","d.nokec","b.nokota","c.noprov")
        ->join("mst_kecamatan as d","a.nokec","=","d.nokec")
        ->join("mst_kota as b","d.nokota","=","b.nokota")
        ->join("mst_provinsi as c","b.noprov","=","c.noprov")
        ->where("a.nokel",$id)
        ->first();

        $list_prov = [];
        $data_prov =Provinsi::all();
        $list_prov = [''=>'-- Pilih Provinsi -- '];
        foreach ($data_prov as $row) 
        {
            $list_prov[$row->noprov] = $row->nmprov;
        }

        $list_kab = [];
        $data_kab =Kabupaten::where("noprov",$data['kelurahan']->noprov)->orderBy("nmkota","asc")->get();
        $list_kab = [''=>'-- Pilih Kota/Kabupaten -- '];
        foreach ($data_kab as $row) 
        {
            $list_kab[$row->nokota] = $row->nmkota;
        }

        $list_kec = [];
        $data_kec =Kecamatan::where("nokota",$data['kelurahan']->nokota)->orderBy("nmkec","asc")->get();
        $list_kec = [''=>'-- Pilih Kota/Kecamatan -- '];
        foreach ($data_kec as $row) 
        {
            $list_kec[$row->nokec] = $row->nmkec;
        }

        $data['list_prov'] = $list_prov; 
        $data['list_kab'] = $list_kab; 
        $data['list_kec'] = $list_kec;  
       return view('admin.ppkd.kelurahan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        Kelurahan::where('nokel',$request->nokel)->update([
            'kdkel'=>$request->kdkel,
            'nmkel'=>$request->nmkel,
            'nokec'=>$request->nokec,
        ]); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.kelurahan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Kelurahan::where('nokel',$id)->delete(); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been delete successfully.');

        return redirect(route('ppkd.master.kelurahan.index'));
    }
}
