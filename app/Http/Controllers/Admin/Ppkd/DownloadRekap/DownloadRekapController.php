<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\DownloadRekap;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\Trx_rencana;
use Excel;
use PDF;
use DB;


class DownloadRekapController extends BaseHomeController
{

	public function index(){
		return view('admin.ppkd.download-rekap.index')->with('recPeriode',Periode::where('status',1)->get());
	}

	public function manage($fase,$noper){
		// switch ($fase) {
		// 	case 'penetapan':		
		// 		return view('admin.ppkd.download-rekap.manage')->with('fase',$fase)
		// 													->with('noper',4);
		// 		break;
		// 	case 'perubahan':		
		// 		return view('admin.ppkd.download-rekap.manage')->with('fase',$fase)
		// 													->with('noper',5);
		// 		break;
		// 	case 'pengusulan':
		// 		return view('admin.ppkd.download-rekap.manage')->with('fase',$fase)
		// 													->with('noper',6);
		// 		break;
		// 	default:
		// 		# code...
		// 		break;
		// }
        return view('admin.ppkd.download-rekap.manage')->with('fase',$fase)
                ->with('noper',$noper)
                ->with('recPeriodeByNoper',Periode::where('noper',$noper)->first());
	}

	public function lampiranSKGubernur($noper){
		$query = DB::table('mst_lembaga AS A')->select('C.nm_skpd AS SKPDUKPDPemberiRekomendasi', 'E.kd_program AS KodeProgram', 'E.nm_program AS NamaProgram', 'D.kd_rekening AS KodeRekening','D.nm_rekening AS NamaRekening', 'A.nm_lembaga AS NamaPenerima', 'A.alamat AS Alamat', 'B.jdl_prop AS JudulProposal', 'B.nominal_skgub AS Penetapan')
											->join('trx_proposal AS B','A.nomor','=','B.no_lembaga')
											->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
                                            ->join('mst_rekening AS D','B.no_rek','=','D.nomor')
											->leftJoin('mst_program AS E','B.no_prg','=','E.nomor')
											->where('C.IsAktif',1)
											->where('B.status_kirim',1)
											->where('B.status_prop',1)
											->where('B.noper',$noper)->get();


        $data = json_decode(json_encode($query), true);

            Excel::create('Lampiran SK Gubernur', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Lampiran SK Gubernur');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');

		// $data = ['query'=>$query];

		// return view('pdf.lampiran-sk-gubernur', $data);

		// $pdf = PDF::loadView('pdf.lampiran-sk-gubernur', $data);
		// return $pdf->download('Lampiran-SK-Gubernur.pdf');
	}

	public function laporanRealisasiAnggaranHibahBansos($noper){
		$query = DB::table('mst_lembaga as A')->select('C.kd_skpd as KodeUnit', 'C.nm_skpd as NamaUnit', 'F.kd_urusan as KodeUrusan', 'F.nm_urusan as NamaUrusan', 'E.kd_program as KodeProgram', 'E.nm_program as NamaProgram', 'A.kd_lembaga as KodeLembaga', 'A.nm_lembaga as NamaLembaga', 'A.alamat as AlamatLembaga', 'B.jdl_prop as JudulProposal', 'D.kd_rekening As KodeRekenign', 'D.nm_rekening as NamaRekening', 'B.nominal_pengajuan as NilaiPengajuan', 'B.nominal_rekomendasi as NilaiRekomendasi', 'B.nominal_skgub as NilaiSKGUb', 'B.nominal_pencairan as NilaiPencairan')
											->join('trx_proposal AS B','A.nomor','=','B.no_lembaga')
											->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
											->join('mst_rekening AS D','B.no_rek','=','D.nomor')
											->leftJoin('mst_program AS E','B.no_prg','=','E.nomor')
											->join('mst_urusan AS F','E.kd_urusan','=','F.kd_urusan')
											->where('C.IsAktif',1)
											->where('B.noper',$noper)->get();



		$data = json_decode(json_encode($query), true);

            Excel::create('Laporan Realisasi Anggaran', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Laporan Realisasi Anggaran');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

	public function laporanRealisasiPerSKPD($noper){
		$query = DB::select('SELECT A.kd_skpd, A.nm_skpd,IFNULL(B.TotalJmlProp,0) as TotalProposalDibuat, IFNULL(C.TotalDikirim,0) as TotalDikirim,
IFNULL(C.TotalNominalPengajuan,0) As TotalNominalPengajuan,  IFNULL(D.TotalMenunggu,0) as TotalMenunggu,
IFNULL(E.TotalDikoreksi,0) as TotalDikoreksi, IFNULL(F.TotalDitolak,0) as TotalDitolak , IFNULL(G.TotalDirekomendasi,0) as TotalDirekomendasi, IFNULL(G.TotalNominalRekomendasi,0) as TotalNominalRekomendasi, IFNULL(H.TotalSKGub,0) as TotalSKGub,IFNULL(H.TotalNominalSKGub,0) as TotalNominalSKGub, IFNULL(I.TotalCair,0) as Totalcair,IFNULL(I.TotalNominalCair,0) as TotalNominalPencairan
FROM mst_skpd A
LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) As TotalJmlprop
    	FROM trx_proposal
    	WHERE noper = '.$noper.'
    	GROUP BY no_skpd
	 ) B ON A.nomor = B.no_skpd
LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalDikirim,  SUM(nominal_pengajuan) As TotalNominalPengajuan
        from trx_proposal
        WHERE noper = '.$noper.' AND status_kirim = 1
        GROUP BY no_skpd
    ) C ON A.nomor  = C.no_skpd
LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalMenunggu
        from trx_proposal
        WHERE noper = '.$noper.' AND status_kirim = 1 AND status_prop = 0
        GROUP BY no_skpd
    ) D ON A.nomor  = D.no_skpd
LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalDikoreksi
    	from trx_proposal
    	WHERE noper = '.$noper.' AND status_kirim = 1 AND status_prop = 3
    	GROUP BY no_skpd
    ) E ON A.nomor = E.no_skpd

LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalDitolak
    	from trx_proposal
    	WHERE noper = '.$noper.' AND status_kirim = 1 AND status_prop = 2
    	GROUP BY no_skpd
    ) F ON A.nomor  = F.no_skpd

LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalDirekomendasi, SUM(nominal_rekomendasi) As TotalNominalRekomendasi
    	from trx_proposal
    	WHERE noper = '.$noper.' AND status_kirim = 1 AND status_prop = 1
    	GROUP BY no_skpd
    ) G ON A.nomor  = G.no_skpd
LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalSkGub, SUM(nominal_skGub) As TotalNominalSKGub
    	from trx_proposal
    	WHERE noper = '.$noper.' AND status_kirim = 1 AND status_prop = 1
    	GROUP BY no_skpd
    ) H ON A.nomor  = H.no_skpd
LEFT JOIN (
    SELECT no_skpd, COUNT(no_prop) as TotalCair, SUM(nominal_pencairan) As TotalNominalCair
    	from trx_proposal
    	WHERE noper = '.$noper.' AND status_kirim = 1 AND status_prop = 1
    	GROUP BY no_skpd
    ) I ON A.nomor  = I.no_skpd
WHERE A.IsAktif = 1
ORDER BY A.kd_skpd, A.nm_skpd');

		$data = json_decode(json_encode($query), true);

            Excel::create('Laporan Realisasi SKPD or UKPD', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Laporan Realisasi SKPD or UKPD');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

	public function laporanLembagaNotRealisasi($noper){
		$query = DB::select('SELECT C.kd_skpd as KodeUnit, C.nm_skpd as NamaUnit, E.kd_urusan as KodeUrusan,  
            E.kd_program as KodeProgram, E.nm_program as NamaProgram, A.kd_lembaga as KodeLembaga, A.nm_lembaga as NamaLembaga, A.alamat as AlamatLembaga, B.jdl_prop as JudulProposal, D.kd_rekening As KodeRekenign, D.nm_rekening as NamaRekening, B.nominal_pengajuan as NilaiPengajuan, B.nominal_rekomendasi as NilaiRekomendasi, B.nominal_skgub as NilaiSKGUb, B.nominal_pencairan as NilaiPencairan
FROM mst_lembaga A 
JOIN trx_proposal B oN A.nomor = B.no_lembaga 
JOIN mst_skpd C on B.no_skpd = C.nomor 
JOIN mst_rekening D ON B.no_rek = D.nomor 
LEFT JOIN mst_program E ON B.no_prg = E.nomor 
WHERE B.noper = '.$noper.' and C.IsAktif = 1  and 
B.status_kirim = 1 and B.status_prop = 1 and (B.nominal_pencairan = 0 or B.nominal_pencairan is null) and
(B.nominal_rekomendasi <> 0 OR B.nominal_skgub <> 0 OR B.nominal_rekomendasi is not NULL OR B.nominal_skgub is not NULL )');


		$data = json_decode(json_encode($query), true);

            Excel::create('Laporan Lembaga tidak realisasi', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Laporan Lembaga tidak realisasi');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

	public function laporanLembagaLPJ($noper){
		$query = DB::select('SELECT C.kd_skpd as KodeUnit, C.nm_skpd as NamaUnit, E.kd_urusan as KodeUrusan,  
            E.kd_program as KodeProgram, E.nm_program as NamaProgram, A.kd_lembaga as KodeLembaga, A.nm_lembaga as NamaLembaga, A.alamat as AlamatLembaga, B.jdl_prop as JudulProposal, D.kd_rekening As KodeRekenign, D.nm_rekening as NamaRekening, B.nominal_pengajuan as NilaiPengajuan, B.nominal_rekomendasi as NilaiRekomendasi, B.nominal_skgub as NilaiSKGUb, B.nominal_pencairan as NilaiPencairan, B.nominal_lpj
FROM mst_lembaga A 
JOIN trx_proposal B on A.nomor = B.no_lembaga 
JOIN mst_skpd C on B.no_skpd = C.nomor 
JOIN mst_rekening D on B.no_rek = D.nomor 
LEFT JOIN mst_program E on B.no_prg = E.nomor 
WHERE B.noper = '.$noper.' and C.IsAktif = 1
and (B.nominal_lpj <> 0 OR B.nominal_lpj is not null)');


		$data = json_decode(json_encode($query), true);

            Excel::create('Laporan Lembaga LPJ', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Laporan Lembaga LPJ');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

	public function rekapJumlahLembaga($noper){

        $query = DB::table('mst_lembaga as A')->select('D.kelompok as Kategori', 'I.kd_urusan as KodeUrusan','I.kd_program as KodeProgram','I.nm_program as NamaProgram','D.kd_rekening As KodeRekening',  'D.nm_rekening as NamaRekening', 'A.kd_lembaga as KodeLembaga', 'A.nm_lembaga as NamaLembaga', 'A.alamat as AlamatLembaga','E.nmkel as Kelurahan','F.nmkec as Kecamatan',
            'G.nmkota','H.nmprov as Provinsi',
            'C.kd_skpd as KodeSKPD','C.nm_skpd as NamaSKPD','B.jdl_prop as JudulProposal', 'B.nominal_pengajuan as NilaiPengajuan', 'B.nominal_rekomendasi as NilaiRekomendasi', 'B.nominal_apbd','B.nominal_skgub as NilaiSKGUb', 'B.nominal_pencairan as NilaiPencairan','B.no_lembaga as nomor_lembaga')
                                            ->join('trx_proposal AS B','A.nomor','=','B.no_lembaga')
                                            ->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
                                            ->join('mst_rekening AS D','B.no_rek','=','D.nomor')
                                            ->leftJoin('mst_kelurahan AS E','A.nokel','=','E.nokel')
                                            ->leftJoin('mst_kecamatan AS F','A.nokec','=','F.nokec')
                                            ->leftJoin('mst_kota AS G','A.nokota','=','G.nokota')
                                            ->leftJoin('mst_provinsi AS H','G.noprov','=','H.noprov')
                                            ->leftJoin('mst_program AS I','B.no_prg','=','I.nomor')
                                            // ->join('mst_urusan AS J','I.kd_urusan','=','J.kd_urusan')
                                            ->where('C.IsAktif',1)
                                            ->where('B.status_kirim',1)
                                            ->where('B.status_prop',1) 
                                            ->where('B.noper',$noper)->distinct()->get();

        $data=[];
        foreach ($query as $key => $value) {

          $dataTahunSebelum  = DB::table("trx_proposal")
                    ->where("noper",9)
                    ->where("status_kirim",1)
                    ->where("status_prop",1)
                    ->where("no_lembaga",$value->nomor_lembaga)
                    ->first();

            $bantuanSebelumnya='';
            if($dataTahunSebelum != null){
                $bantuanSebelumnya = $dataTahunSebelum->nominal_apbd;
            } 

            $urusan = DB::table("mst_urusan")->where("kd_urusan",$value->KodeUrusan)->first();
            $textUrusan = '';
            if($urusan != null){
                $textUrusan = $urusan->nm_urusan;
            }
              $data[]= [
                'Kategori'=>$value->Kategori,
                'Kode Urusan'=>$value->KodeUrusan,
                'Nama Urusan'=>$textUrusan,
                'Kode Program'=>$value->KodeProgram,
                'Nama Program'=>$value->NamaProgram,
                'Kode Rekening'=>$value->KodeRekening,
                'Nama Rekening'=>$value->NamaRekening,
                'Kode Lembaga'=>$value->KodeLembaga,
                'Nama Lembaga'=>$value->NamaLembaga,
                'Bantuan Tahun Sebelumnya'=>$bantuanSebelumnya,
                'Alamat Lembaga'=>$value->AlamatLembaga,
                'Kelurahan'=>$value->Kelurahan,
                'Kecamatan'=>$value->Kecamatan,
                'Kota/Kab'=>$value->nmkota,
                'Provinsi'=>$value->Provinsi,
                'Kode SKPD'=>$value->KodeSKPD, 
                'Nama SKPD'=>$value->NamaSKPD,
                'Judul Proposal'=>$value->JudulProposal,
                'Nilai Pengajuan'=>$value->NilaiPengajuan,
                'Nilai Rekomendasi'=>$value->NilaiRekomendasi,
                'Nilai ABPD'=>$value->nominal_apbd,
                'Nilai SKGUb'=>$value->NilaiSKGUb,
                'Nilai Pencairan'=>$value->NilaiPencairan,
                // 'NoLembaga'=>$value->nomor_lembaga,
              ];

        }
  //       exit;
		// $data = json_decode(json_encode($query), true);

            Excel::create('Rekap Jumlah Lembaga', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Rekap Jumlah Lembaga');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

    public  function rekap2020ProposalPengajuan($noper)
    {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        $date = date('Y-m-d-Hi');
        header("content-disposition: attachment;filename=Excel_Rekap_$date.xls"); 

         $query = DB::table('mst_lembaga as A')->select('D.kelompok as Kategori', 'I.kd_urusan as KodeUrusan','I.kd_program as KodeProgram','I.nm_program as NamaProgram','D.kd_rekening As KodeRekening',  'D.nm_rekening as NamaRekening', 'A.kd_lembaga as KodeLembaga', 'A.nm_lembaga as NamaLembaga', 'A.alamat as AlamatLembaga','E.nmkel as Kelurahan','F.nmkec as Kecamatan',
            'G.nmkota','H.nmprov as Provinsi',
            'C.kd_skpd as KodeSKPD','C.nm_skpd as NamaSKPD','B.jdl_prop as JudulProposal', 'B.nominal_pengajuan as NilaiPengajuan', 'B.nominal_rekomendasi as NilaiRekomendasi', 'B.nominal_apbd','B.nominal_skgub as NilaiSKGUb', 'B.nominal_pencairan as NilaiPencairan','B.no_lembaga as nomor_lembaga','B.status_prop','J.keterangan','B.no_prop')
                                            ->join('trx_proposal AS B','A.nomor','=','B.no_lembaga')
                                            ->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
                                            ->join('mst_rekening AS D','B.no_rek','=','D.nomor')
                                            ->leftJoin('mst_kelurahan AS E','A.nokel','=','E.nokel')
                                            ->leftJoin('mst_kecamatan AS F','A.nokec','=','F.nokec')
                                            ->leftJoin('mst_kota AS G','A.nokota','=','G.nokota')
                                            ->leftJoin('mst_provinsi AS H','G.noprov','=','H.noprov')
                                            ->leftJoin('mst_program AS I','B.no_prg','=','I.nomor')
                                            ->leftJoin('mst_periode AS J','B.noper','=','J.noper')
                                            // ->join('mst_urusan AS J','I.kd_urusan','=','J.kd_urusan')
                                            ->where('C.IsAktif',1)
                                            ->where('B.status_kirim',1)
                                            ->where('B.status_prop',1) 
                                            ->where('B.noper',$noper)->distinct()
                                            ->get();
                                            // ->distinct()->get();

        $data=[];
        $html = '<table border="1">
        <tr>
            <td>Periode</td>
            <td>Judul Proposal</td>
            <td>Nama SKPD</td>
            <td>Kode Rekening</td>
            <td>Nama Rekening</td>
            <td>Nama Lembaga</td>
            <td>Alamat Lembaga</td>
            <td>Total Pengajuan Anggaran</td>
            <td>Nominal Rekomendasi</td>
            <td>Status</td> 
            <td>No</td> 
            <td>Rencana Kegiatan/Rincian</td>
            <td>Vol/Satuan (jumlah)</td>
            <td>Harga Satuan (Rp)</td>
            <td>Anggaran Pengajuan(Rp)</td>
            <td>Anggaran Rekomendasi(Rp)</td> 
        </tr>
        ';
        foreach ($query as $key => $value) { 
            $status='';
            if($value->status_prop==0){
                $status = 'Belum Direkomendasi';
            }
            elseif($value->status_prop==1){
                $status = 'Sudah Direkomendasi';
            }
            elseif($value->status_prop==2){ 
                $status = 'Ditolak';
            }

            $rencana = DB::table('trx_rencana as a')
                    ->select('a.*')
                    ->where('a.no_prop',$value->no_prop)
                    ->get();  

            $i          =1; 
            $ips        ='';
            $ipChek     ='';
            $printIps   ='';
            $j=1;
            foreach ($rencana as $key => $row) { 
                     
                    $ips = $value->no_prop; 
                    if($ips != $ipChek){
                        $printIps = $ips;
                    }else{
                        $printIps = "";
                    } 

                    if($ips != $ipChek){
                        $printIps = '
                        <td>'.$value->keterangan.'</td>
                        <td>'.$value->JudulProposal.'</td>
                        <td>'.$value->NamaSKPD.'</td>
                        <td>'.$value->KodeRekening.'</td>
                        <td>'.$value->NamaRekening.'</td>
                        <td>'.$value->NamaLembaga.'</td>
                        <td>'.$value->AlamatLembaga. ', '.$value->Kelurahan.','.$value->Kecamatan.', '.$value->nmkota.', '.$value->Provinsi.'</td> 
                        <td>'.$value->NilaiPengajuan.'</td>
                        <td>'.$value->NilaiRekomendasi.'</td>
                        <td>'.$status.'</td>
                        ';
                    }else{
                        $printIps = '
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td> 
                        <td> </td> 
                        <td> </td>  
                        ';
                    } 

                    $html .='
                    <tr>
                       '.$printIps.'
                        <td>'.$i.'</td> 
                        <td>'.$row->rencana.'</td>
                        <td> </td>
                        <td> </td>
                        <td>'.number_format($row->rencana_anggaran).'</td>
                        <td>'.number_format($row->rencana_anggaran_skpd).'</td>   
                    </tr>';

                        $rincianPengajuan = DB::table('trx_rincian')
                            ->where('rencanaID',$row->id) 
                             ->get();

                        
                        foreach ($rincianPengajuan as $value2) { 
                             $html .='<tr>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td> 
                                <td> </td> 
                                <td> </td> 
                                <td> </td>   
                                <td>'.$value2->rincian.'</td>
                                <td>'.$value2->volume1.' '.$value2->satuan1.' x '.$value2->volume2.' '.$value2->satuan2.'</td>
                                <td>'.$value2->harga_satuan.'  </td>
                                <td>'.number_format($value2->rincian_anggaran).'</td>
                                <td>'.number_format($value2->rincian_anggaran_skpd).'</td>   
                            </tr>';
                        }  

                        $ipChek = $value->no_prop;
                    $i++;
            } 
        }
        $html .=' 
              </table>';
        echo $html;  
    }

    public  function rekap2020ProposalPengajuanNoRAB($noper)
    {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        $date = date('Y-m-d-Hi');
        header("content-disposition: attachment;filename=Excel_Rekap_$date.xls"); 

         $query = DB::table('mst_lembaga as A')->select('D.kelompok as Kategori', 'I.kd_urusan as KodeUrusan','I.kd_program as KodeProgram','I.nm_program as NamaProgram','D.kd_rekening As KodeRekening',  'D.nm_rekening as NamaRekening', 'A.kd_lembaga as KodeLembaga', 'A.nm_lembaga as NamaLembaga', 'A.alamat as AlamatLembaga','E.nmkel as Kelurahan','F.nmkec as Kecamatan',
            'G.nmkota','H.nmprov as Provinsi',
            'C.kd_skpd as KodeSKPD','C.nm_skpd as NamaSKPD','B.jdl_prop as JudulProposal', 'B.nominal_pengajuan as NilaiPengajuan', 'B.nominal_rekomendasi as NilaiRekomendasi', 'B.nominal_apbd','B.nominal_skgub as NilaiSKGUb', 'B.nominal_pencairan as NilaiPencairan','B.no_lembaga as nomor_lembaga','B.status_prop','J.keterangan','B.no_prop')
                                            ->join('trx_proposal AS B','A.nomor','=','B.no_lembaga')
                                            ->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
                                            ->join('mst_rekening AS D','B.no_rek','=','D.nomor')
                                            ->leftJoin('mst_kelurahan AS E','A.nokel','=','E.nokel')
                                            ->leftJoin('mst_kecamatan AS F','A.nokec','=','F.nokec')
                                            ->leftJoin('mst_kota AS G','A.nokota','=','G.nokota')
                                            ->leftJoin('mst_provinsi AS H','G.noprov','=','H.noprov')
                                            ->leftJoin('mst_program AS I','B.no_prg','=','I.nomor')
                                            ->leftJoin('mst_periode AS J','B.noper','=','J.noper')
                                            // ->join('mst_urusan AS J','I.kd_urusan','=','J.kd_urusan')
                                            ->where('C.IsAktif',1)
                                            ->where('B.status_kirim',1)
                                            ->where('B.status_prop',1) 
                                            ->where('B.noper',$noper)->distinct()
                                            ->get();
                                            // ->distinct()->get();

        $data=[];
        $html = '<table border="1">
        <tr>
            <td>Periode</td>
            <td>Judul Proposal</td>
            <td>Nama SKPD</td>
            <td>Kode Rekening</td>
            <td>Nama Rekening</td>
            <td>Nama Lembaga</td>
            <td>Alamat Lembaga</td>
            <td>Total Pengajuan Anggaran</td>
            <td>Nominal Rekomendasi</td>
            <td>Status</td>  
        </tr>
        ';
        foreach ($query as $key => $value) { 
            $status='';
            if($value->status_prop==0){
                $status = 'Belum Direkomendasi';
            }
            elseif($value->status_prop==1){
                $status = 'Sudah Direkomendasi';
            }
            elseif($value->status_prop==2){ 
                $status = 'Ditolak';
            }
 
                     
                        $printIps = '
                        <td>'.$value->keterangan.'</td>
                        <td>'.$value->JudulProposal.'</td>
                        <td>'.$value->NamaSKPD.'</td>
                        <td>'.$value->KodeRekening.'</td>
                        <td>'.$value->NamaRekening.'</td>
                        <td>'.$value->NamaLembaga.'</td>
                        <td>'.$value->AlamatLembaga. ', '.$value->Kelurahan.','.$value->Kecamatan.', '.$value->nmkota.', '.$value->Provinsi.'</td> 
                        <td>'.$value->NilaiPengajuan.'</td>
                        <td>'.$value->NilaiRekomendasi.'</td>
                        <td>'.$status.'</td>
                        ';
                   
                    $html .='
                    <tr>
                       '.$printIps.'
                        
                    </tr>';
  
        }
        $html .=' 
              </table>';
        echo $html;  
    }
    

     public  function rekap2020ProposalPenebalan($noper)
    {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        $date = date('Y-m-d-Hi');
        header("content-disposition: attachment;filename=Proposal_Penebalan_Rekap_$date.xls"); 

         $query = DB::table('mst_lembaga as A')->select('D.kelompok as Kategori', 'I.kd_urusan as KodeUrusan','I.kd_program as KodeProgram','I.nm_program as NamaProgram','D.kd_rekening As KodeRekening',  'D.nm_rekening as NamaRekening', 'A.kd_lembaga as KodeLembaga', 'A.nm_lembaga as NamaLembaga', 'A.alamat as AlamatLembaga','E.nmkel as Kelurahan','F.nmkec as Kecamatan',
            'G.nmkota','H.nmprov as Provinsi',
            'C.kd_skpd as KodeSKPD','C.nm_skpd as NamaSKPD','B.jdl_prop as JudulProposal', 'B.nominal_pengajuan as NilaiPengajuan', 'B.nominal_rekomendasi as NilaiRekomendasi', 'B.nominal_apbd','B.nominal_skgub as NilaiSKGUb', 'B.nominal_pencairan as NilaiPencairan','B.no_lembaga as nomor_lembaga','B.status_prop','J.keterangan','B.no_prop','B.uuid_proposal_pengajuan')
                                            ->join('trx_proposal_definitif_apbd AS B','A.nomor','=','B.no_lembaga')
                                            ->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
                                            ->join('mst_rekening AS D','B.no_rek','=','D.nomor')
                                            ->leftJoin('mst_kelurahan AS E','A.nokel','=','E.nokel')
                                            ->leftJoin('mst_kecamatan AS F','A.nokec','=','F.nokec')
                                            ->leftJoin('mst_kota AS G','A.nokota','=','G.nokota')
                                            ->leftJoin('mst_provinsi AS H','G.noprov','=','H.noprov')
                                            ->leftJoin('mst_program AS I','B.no_prg','=','I.nomor')
                                            ->leftJoin('mst_periode AS J','B.noper','=','J.noper')
                                            // ->join('mst_urusan AS J','I.kd_urusan','=','J.kd_urusan')
                                            ->where('C.IsAktif',1)
                                            ->where('B.status_kirim',1)
                                            ->where('B.status_prop',1) 
                                            ->where('B.noper',$noper)->distinct()
                                            // ->limit(2)
                                            ->get();
                                            // ->distinct()->get();

        $data=[];
        $html = '<table border="1">
        <tr>
            <td>Periode</td>
            <td>Nama SKPD</td>
            <td>Kode Rekening</td>
            <td>Nama Rekening</td>
            <td>Nama Lembaga</td>
            <td>Alamat Lembaga</td>
            <td>Total Pengajuan Anggaran</td>
            <td>Nominal Rekomendasi</td>
            <td>Status</td> 
            <td>No</td> 
            <td>Rencana Kegiatan/Rincian</td>
            <td>Vol/Satuan (jumlah)</td>
            <td>Harga Satuan (Rp)</td>
            <td>Anggaran Pengajuan(Rp)</td>
            <td>Anggaran Rekomendasi(Rp)</td> 
        </tr>
        ';
        foreach ($query as $key => $value) { 
            $status='';
            if($value->status_prop==0){
                $status = 'Belum Direkomendasi';
            }
            elseif($value->status_prop==1){
                $status = 'Sudah Direkomendasi';
            }
            elseif($value->status_prop==2){ 
                $status = 'Ditolak';
            }

            $rencana = DB::table('trx_rencana_penebalan as a')
                    ->select('a.*')
                    ->where('a.uuid_proposal_pengajuan',$value->uuid_proposal_pengajuan)
                    ->get();  

            $i          =1; 
            $ips        ='';
            $ipChek     ='';
            $printIps   ='';
            $j=1;
            foreach ($rencana as $key => $row) { 
                     
                    $ips = $value->no_prop; 
                    if($ips != $ipChek){
                        $printIps = $ips;
                    }else{
                        $printIps = "";
                    } 

                    if($ips != $ipChek){
                        $printIps = '
                        <td>'.$value->keterangan.'</td>
                        <td>'.$value->NamaSKPD.'</td>
                        <td>'.$value->KodeRekening.'</td>
                        <td>'.$value->NamaRekening.'</td>
                        <td>'.$value->NamaLembaga.'</td>
                        <td>'.$value->AlamatLembaga. ', '.$value->Kelurahan.','.$value->Kecamatan.', '.$value->nmkota.', '.$value->Provinsi.'</td> 
                        <td>'.$value->NilaiPengajuan.'</td>
                        <td>'.$value->NilaiRekomendasi.'</td>
                        <td>'.$status.'</td>
                        ';
                    }else{
                        $printIps = '
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td> 
                        <td> </td> 
                        <td> </td>  
                        ';
                    } 

                    $html .='
                    <tr>
                       '.$printIps.'
                        <td>'.$i.'</td> 
                        <td>'.$row->rencana.'</td>
                        <td> </td>
                        <td> </td>
                        <td>'.$row->rencana_anggaran_pengajuan.'</td>
                        <td>'.$row->rencana_anggaran_rekomendasi.'</td>   
                    </tr>';

                        $rincianPengajuan = DB::table('trx_rincian_penebalan')
                            ->where('rencanaID',$row->id) 
                             ->get();

                        
                        foreach ($rincianPengajuan as $value2) { 
                             $html .='<tr>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td> 
                                <td> </td> 
                                <td> </td> 
                                <td> </td>   
                                <td>'.$value2->rincian.'</td>
                                <td>'.$value2->volume1.' '.$value2->satuan1.' x '.$value2->volume2.' '.$value2->satuan2.'</td>
                                <td>'.$value2->harga_satuan.'  </td>
                                <td>'.$value2->anggaran_pengajuan.'</td>   
                                <td>'.$value2->anggaran_rekomendasi.'</td>
                            </tr>';
                        }  

                        $ipChek = $value->no_prop;
                    $i++;
            } 
        }
        $html .=' 
              </table>';
        echo $html;  
    }


    public function rekapNew($noper){ 
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        $date = date('Y-m-d-Hi');
        header("content-disposition: attachment;filename=Excel_Rekap_$date.xls"); 

        $query = DB::table('mst_lembaga as A')->select('D.kelompok as Kategori', 'I.kd_urusan as KodeUrusan','I.kd_program as KodeProgram','I.nm_program as NamaProgram','D.kd_rekening As KodeRekening',  'D.nm_rekening as NamaRekening', 'A.kd_lembaga as KodeLembaga', 'A.nm_lembaga as NamaLembaga', 'A.alamat as AlamatLembaga','E.nmkel as Kelurahan','F.nmkec as Kecamatan',
            'G.nmkota','H.nmprov as Provinsi',
            'C.kd_skpd as KodeSKPD','C.nm_skpd as NamaSKPD','B.jdl_prop as JudulProposal', 'B.nominal_pengajuan as NilaiPengajuan', 'B.nominal_rekomendasi as NilaiRekomendasi', 'B.nominal_apbd','B.nominal_skgub as NilaiSKGUb', 'B.nominal_pencairan as NilaiPencairan','B.no_lembaga as nomor_lembaga','B.status_prop','J.keterangan','B.no_prop')
                                            ->join('trx_proposal AS B','A.nomor','=','B.no_lembaga')
                                            ->join('mst_skpd AS C','B.no_skpd','=','C.nomor')
                                            ->join('mst_rekening AS D','B.no_rek','=','D.nomor')
                                            ->leftJoin('mst_kelurahan AS E','A.nokel','=','E.nokel')
                                            ->leftJoin('mst_kecamatan AS F','A.nokec','=','F.nokec')
                                            ->leftJoin('mst_kota AS G','A.nokota','=','G.nokota')
                                            ->leftJoin('mst_provinsi AS H','G.noprov','=','H.noprov')
                                            ->leftJoin('mst_program AS I','B.no_prg','=','I.nomor')
                                            ->leftJoin('mst_periode AS J','B.noper','=','J.noper')
                                            // ->join('mst_urusan AS J','I.kd_urusan','=','J.kd_urusan')
                                            ->where('C.IsAktif',1)
                                            ->where('B.status_kirim',1)
                                            ->where('B.status_prop',1) 
                                            ->where('B.noper',$noper)->distinct()->get();
                                            // ->distinct()->get();

        $data=[];
        $html = '<table border="1">
        <tr>
            <td>Periode</td>
            <td>Nama SKPD</td>
            <td>Kode Rekening</td>
            <td>Nama Rekening</td>
            <td>Nama Lembaga</td>
            <td>Alamat Lembaga</td>
            <td>Total Pengajuan Anggaran</td>
            <td>Nominal Rekomendasi</td>
            <td>Status</td> 
            <td>Kegiatan</td>
            <td>Nilai Usulan</td>
            <td>Nilai Rekomendasi</td>
        </tr>
        ';
        foreach ($query as $key => $value) { 
            $status='';
            if($value->status_prop==0){
                $status = 'Belum Direkomendasi';
            }
            elseif($value->status_prop==1){
                $status = 'Sudah Direkomendasi';
            }
            elseif($value->status_prop==2){ 
                $status = 'Ditolak';
            }

            $rencana = DB::table('trx_rencana as a')
                    ->select('a.*')
                    ->where('a.no_prop',$value->no_prop)
                    ->get();  

            $i          =1; 
            $ips        ='';
            $ipChek     ='';
            $printIps   ='';

            foreach ($rencana as $key => $row) { 
                    
                    // $anggaran_skpd = DB::select("SELECT SUM(rincian_anggaran_skpd)  as anggaran_skpd
                    //    FROM trx_rincian
                    //    where rencanaID='".$row->id."'
                    //    "); 

                    $ips = $value->no_prop; 
                    if($ips != $ipChek){
                        $printIps = $ips;
                    }else{
                        $printIps = "";
                    } 

                    if($ips != $ipChek){
                        $printIps = '
                        <td>'.$value->keterangan.'</td>
                        <td>'.$value->NamaSKPD.'</td>
                        <td>'.$value->KodeRekening.'</td>
                        <td>'.$value->NamaRekening.'</td>
                        <td>'.$value->NamaLembaga.'</td>
                        <td>'.$value->AlamatLembaga. ', '.$value->Kelurahan.','.$value->Kecamatan.', '.$value->nmkota.', '.$value->Provinsi.'</td> 
                        <td>'.number_format($value->NilaiPengajuan).'</td>
                        <td>'.number_format($value->NilaiRekomendasi).'</td>
                        <td>'.$status.'</td>
                        ';
                    }else{
                        $printIps = '
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td> 
                        <td> </td> 
                        <td> </td> 
                        ';
                    } 

                    $html .='
                    <tr>
                       '.$printIps.'
                        <td>'.$i.'. '.$row->rencana.'</td>
                        <td>'.number_format($row->rencana_anggaran).'</td>  
                        <td>'.number_format($row->rencana_anggaran_skpd).'</td>   
                    </tr>';
                        $ipChek = $value->no_prop;
                    $i++;
            } 
        }
        $html .=' 
              </table>';
        echo $html;  
    }

	public function rekapPerKodeRekening($noper){

        $query = DB::select('
            SELECT A.kd_rekening As KodeRekening, A.kelompok as kelompokRekening, A.nm_rekening as NamaRekening,  IFNULL( B.Nominalpengajuan,0) as NilaiPengajuan, IFNULL(C.NominalRekomendasi,0) as NilaiRekomendasi, IFNULL(D.nominalSKGub,0) as NilaiSKGUb, IFNULL(E.NominalCair,0) as NilaiPencairan
                FROM mst_rekening A 
                LEFT JOIN (
                    SELECT no_rek, SUM(nominal_pengajuan) as NominalPengajuan
                	FROM trx_proposal
                    WHERE noper = '.$noper.' and status_kirim = 1 and status_prop = 1
                    ) B ON A.nomor = B.no_rek
                LEFT JOIN (
                    SELECT no_rek, SUM(nominal_rekomendasi) as NominalRekomendasi
                	FROM trx_proposal
                    WHERE noper = '.$noper.' and status_kirim = 1 and status_prop = 1
                    ) C ON A.nomor = C.no_rek   
                LEFT JOIN (
                    SELECT no_rek, SUM(nominal_skgub) as NominalSKGub
                	FROM trx_proposal
                    WHERE noper = '.$noper.' and status_kirim = 1 and status_prop = 1
                    ) D ON A.nomor = D.no_rek 
                LEFT JOIN (
                    SELECT no_rek, SUM(nominal_pencairan) as NominalCair
                	FROM trx_proposal
                    WHERE noper = '.$noper.' and status_kirim = 1 and status_prop = 1
                    ) E ON A.nomor = E.no_rek 
                ORDER BY A.kd_rekening');

		$data = json_decode(json_encode($query), true);

            Excel::create('Rekap PerKode Rekening', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Rekap PerKode Rekening');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

	public function rekapLembagaUsulanTolak($noper){

		$query = DB::select('SELECT C.kd_skpd as KodeUnit, C.nm_skpd as NamaUnit, E.kd_urusan as KodeUrusan,  
            E.kd_program as KodeProgram, E.nm_program as NamaProgram, A.kd_lembaga as KodeLembaga, A.nm_lembaga as NamaLembaga, A.alamat as AlamatLembaga, B.jdl_prop as JudulProposal, D.kd_rekening As KodeRekenign, D.nm_rekening as NamaRekening, B.nominal_pengajuan as NilaiPengajuan, B.nominal_rekomendasi as NilaiRekomendasi, B.nominal_skgub as NilaiSKGUb
        FROM mst_lembaga A 
        JOIN trx_proposal B oN A.Nomor = B.no_lembaga 
        JOIN mst_skpd C on B.no_skpd = C.nomor 
        JOIN mst_rekening D ON B.no_rek = D.nomor 
        LEFT JOIN mst_program E ON B.no_prg = E.nomor 
        WHERE B.noper = '.$noper.' and C.IsAktif = 1 and  
        (B.nominal_skgub = 0 OR  B.nominal_skgub is NULL )');

        // (B.nominal_rekomendasi = 0 OR B.nominal_skgub = 0 OR B.nominal_rekomendasi is NULL OR B.nominal_skgub is NULL )');

		$data = json_decode(json_encode($query), true);

            Excel::create('Rekap Lembaga Usulan Tolak', function ($excel) use ($data) {
                

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setTitle('Rekap Lembaga Usulan Tolak');
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });

            })->export('xls');
	}

}