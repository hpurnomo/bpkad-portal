<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\User;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Requests\RegUserSkpdViaPpkdRequest;
use BPKAD\Foundation\UserBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use Auth;
use BPKAD\User;
use BPKAD\Http\Entities\Skpd;
use Input;
use BPKAD\Services\DukcapilApi;
use DNS2D;

class UserController extends BaseHomeController
{

	public function manage($group){
		// echo DNS2D::getBarcodeHTML('<a href="http://ehibahbansosdki.jakarta.go.id">', "QRCODE");
		// exit;
		if($group==10){
			$group_name='SKPD';
		}elseif($group==13){
			$group_name='BPK';
		}
		return view('admin.ppkd.user.manage')
								->with('group_id',$group)
								->with('group_name',$group_name)
								->with('recUserByGroupID',UserBuilder::getByGroupID($group));
	} 

	public function create($group){
		if($group==10){
			$group_name='SKPD';
		}elseif($group==13){
			$group_name='BPK';
		}
		return view('admin.ppkd.user.create')
				->with('group_id',$group)
				->with('group_name',$group_name)
				->with('recSkdp',Skpd::orderBy('nm_skpd')->get());
	}

	public function store(RegUserSkpdViaPpkdRequest $request){

		$store = new User;
		$store->group_id = $request->group_id; 
		if($request->group_id==10){
			$store->no_skpd = $request->no_skpd;
			$store->nrk = $request->nrk;
		}
		$store->real_name = $request->real_name; 
		$store->email = $request->email;
		$store->password = bcrypt($request->password);
		$store->is_verify = 1;
		$store->is_active = 1;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.user.management.manage',$request->group_id));
	}

	public function edit($userID,$group){
		if($group==10){
			$group_name='SKPD';
		}elseif($group==13){
			$group_name='BPK';
		}
		return view('admin.ppkd.user.edit')
											->with('group_id',$group)
											->with('group_name',$group_name)
											->with('recSkdp',Skpd::orderBy('nm_skpd')->get())
										   ->with('recUserByID',UserBuilder::getById($userID))
										   ->with('userID',$userID);
	}

	public function update(Request $request){

		if(Input::has('password_reset')){

			$this->updateWithPassword($request->all());

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');

			return redirect(route('ppkd.user.management.manage',$request->group_id));
			
		}else{

			$this->updateWithoutPassword($request->all());

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');

			return redirect(route('ppkd.user.management.manage',$request->group_id));
		
		}
	}

	public function show($userID){
		return view('admin.skpd.user.show')->with('recUserID',$userID)
											->with('recUserByID',UserBuilder::getById($userID))
											->with('recUserLembagaByUserID',UserLembagaBuilder::getByUserID($userID));
	}

	public function changeStatus(Request $request){
		$update = User::where('user_id',$request['userID'])->first();
		$update->is_verify = $request['is_verify'];
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
		
		return redirect(route('skpd.user.management.show',['userID'=>$request['userID']]));
	}

	 public function cekNrk(Request $request){

        $nrk         = $request->nrk; 
        $CHECK       = new DukcapilApi;  
        $checkNik    = $CHECK->getData($nrk);   
        $result 	=  $checkNik['data']; 

        if(!isset($result->results[0]->{'NRK'})){   
          return response()->json(['msg'=>'NRK  tidak ditemukan','status'=>false]);      
        }else{  
        	$nama=$result->results[0]->{'NAMA'};
            return response()->json(['msg'=>'NRK ditemukan','status'=>true,'nama'=>$nama]);  
        }
    	
 
    }




}