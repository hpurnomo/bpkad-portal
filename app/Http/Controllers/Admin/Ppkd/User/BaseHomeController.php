<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\User;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BPKAD\Http\Requests\UpdateUserSkpdViaPpkdRequest;
use BPKAD\User;

class BaseHomeController extends Controller
{	
	function __construct(){

	}

	public function updateWithPassword($request){
			User::where('user_id',$request['user_id'])
			->update([
					'user_id' => $request['user_id'],
					'no_skpd' => $request['no_skpd'],
					'real_name' => $request['real_name'],
					'username' => $request['username'],
					'nrk' => $request['nrk'],
					'email' => $request['email'],
					'password' => bcrypt($request['password_reset'])
				]);
	}

	public function updateWithoutPassword($request){
			User::where('user_id',$request['user_id'])
			->update([
					'user_id' => $request['user_id'],
					'no_skpd' => $request['no_skpd'],
					'real_name' => $request['real_name'],
					'username' => $request['username'],
					'nrk' => $request['nrk'],
					'email' => $request['email']
				]);
	}

}

?>