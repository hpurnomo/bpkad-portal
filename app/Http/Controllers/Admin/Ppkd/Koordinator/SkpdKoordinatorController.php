<?php 
namespace BPKAD\Http\Controllers\Admin\Ppkd\Koordinator;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Foundation\SkpdBuilder;
use BPKAD\Foundation\MstLembagaBuilder;
use Auth;
use BPKAD\Http\Entities\Skpd;
use BPKAD\Http\Requests\StoreLembagaRequest;
use BPKAD\Http\Entities\Kelurahan;
use BPKAD\Http\Entities\Lembaga;
use BPKAD\Http\Entities\mst_tipe_lembaga;

class SkpdKoordinatorController extends BaseHomeController
{

	public function manage(){
		return view('admin.ppkd.koordinator.manage')->with('recSkpdCountLembaga',SkpdBuilder::getSkpdCountLembaga())
													->with('recSkpdIsAktif',Skpd::where('IsAktif',1)->get());
	}

	public function show($kdskpd){

		return view('admin.ppkd.koordinator.show')->with('recSkpdKoordinatorByKdSKPD',MstLembagaBuilder::getByKdSkpd($kdskpd));
	}

	public function create(){
		return view('admin.ppkd.lembaga.create')->with('recSkdp',Skpd::orderBy('nm_skpd','asc')->get())
												->with('recKelurahan',Kelurahan::orderBy('nmkel','asc')->get())
												->with('recTipeLembaga',mst_tipe_lembaga::orderBy('nama','asc')->get());

	}

	public function store(StoreLembagaRequest $request){
		date_default_timezone_set('Asia/Jakarta');//or choose your location

		$store = new Lembaga;
		$store->kd_skpd = $request->kd_skpd;
		$store->tipeLembagaID = $request->tipeLembagaID;
		$store->nm_lembaga = $request->nm_lembaga;
		$store->npwp = $request->npwp;
		$store->alamat = $request->alamat;
		$store->nokel = $request->nokel;
		$store->no_akta = $request->no_akta;
		$store->tgl_akta = $request->tgl_akta;
		$store->no_surat_domisili = $request->no_surat_domisili;
		$store->tgl_surat_domisili = $request->tgl_surat_domisili;
		$store->no_sertifikat = $request->no_sertifikat;
		$store->tgl_sertifikat = $request->tgl_sertifikat;
		$store->no_izin_operasional = $request->no_izin_operasional;
		$store->tgl_izin_operasional = $request->tgl_izin_operasional;
		$store->no_telepon = $request->no_telepon;
		$store->no_fax = $request->no_fax;
		$store->email = $request->email1;
		$store->nama_bank = $request->nama_bank;
		$store->no_rek = $request->no_rek;
		$store->pemilik_rek = $request->pemilik_rek;
		$store->kontak_person = $request->kontak_person;
		$store->email_person = $request->email_person;
		$store->alamat_person = $request->alamat_person;
		$store->no_hp_person = $request->no_hp_person;
		$store->crb = Auth::user()['user_id'];
		$store->crd = date('Y-m-d H:i:s'); 
		$store->is_verify = 0;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');

		return redirect(route('ppkd.lembaga.manage'));

	}

	public function edit($nomor){
		return view('admin.ppkd.koordinator.edit')->with('recLembagaByNomor',MstLembagaBuilder::getByNomor($nomor))
													->with('recSKPDAll',Skpd::where('IsAktif',1)->get());
	}

	public function update(Request $request){

		Lembaga::where('nomor',$request->nomor)->update([
													  "kd_skpd" => $request->kd_skpd
														]);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('ppkd.skpd.koordinator.show',['kdskpd'=>$request->kd_skpd]));
	}


}