<?php

namespace BPKAD\Http\Controllers\Admin\Ppkd\Provinsi;

use Illuminate\Http\Request;

use BPKAD\Http\Requests;
use BPKAD\Http\Controllers\Controller;
use BPKAD\Http\Entities\Provinsi;
use Datatables;

class ProvinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function index()
    { 
        $data['column'] = "   
                    {data: 'noprov'}, 
                    {data: 'kdprov'},
                    {data: 'nmprov'}, 
                    {data: 'action',orderable: false, searchable: false}
                    "; 

        $data['columnConf'] = "
          {
            'targets': 0,
            'orderable':true, 
          },
          {
            'targets': 1, 
            'className': 'text-left',
          },
           {
            'targets': 2,
            'className': 'text-left',
          },
           {
            'targets': 3,
            'className': 'text-center',
          }"; 
        return view('admin.ppkd.provinsi.index',$data);

    }

    public function getData(){  
        $model = Provinsi::all(); 
      
        return Datatables::of($model)
        ->addColumn('action',function($model){
            $action = '<a href="'.route('ppkd.master.provinsi.edit',$model->noprov).'"> <i class="fa fa-pencil"></i> Edit </a>';
            $action .= '<a href="'.route('ppkd.master.provinsi.delete',$model->noprov).'" onClick="return  confirm(\'Yakin akan hapus data ini \');"> <i class="fa fa-trash"></i> Delete </a>'; 
            return $action; 
        })
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ppkd.provinsi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Provinsi; 
        $store->kdprov = $request->kdprov;
        $store->nmprov = $request->nmprov;
        $store->save();

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.provinsi.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['provinsi'] = Provinsi::where("noprov",$id)->first(); 
       return view('admin.ppkd.provinsi.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        Provinsi::where('noprov',$request->noprov)->update([
            'kdprov'=>$request->kdprov,
            'nmprov'=>$request->nmprov
        ]); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been save successfully.');

        return redirect(route('ppkd.master.provinsi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Provinsi::where('noprov',$id)->delete(); 

        $request->session()->flash('status','success');
        $request->session()->flash('msg','Your data has been delete successfully.');

        return redirect(route('ppkd.master.provinsi.index'));
    }
}
