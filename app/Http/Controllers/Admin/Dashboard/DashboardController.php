<?php 
namespace BPKAD\Http\Controllers\Admin\Dashboard;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Foundation\ProposalBuilder;
use BPKAD\Http\Entities\Periode;
use DB;
use Auth;

class DashboardController extends BaseHomeController
{

	public function manage(Request $request){

		switch ($request->session()->get('group_id')) {
			case '12': // For Lembaga
				// dd(Auth::user()->no_lembaga);
				$feedLembaga = DB::table('mst_periode AS a')->select('a.jenisPeriodeID','a.tahun','a.tahun_proses','a.keterangan AS keterangan_periode','b.*')
				->join('trx_proposal AS b','a.noper','=','b.noper')
				->where('a.status',1)
				->where('b.no_lembaga',Auth::user()->no_lembaga)
				->orderBy('b.tgl_prop','desc')
				->get();
 

				$periode = Periode::where('status',1)->get();
			 
				$no_lembaga = Auth::user()->no_lembaga;
				$dashboard='<table  class="table table-bordered table-hovered">
					<th>Periode Tahapan</th>
					<th>Proposal Pengajuan</th>
					<th>Nominal Pengajuan (Rp)</th>
					<th>Proposal Direkomendasi</th>
					<th>Nominal Rekomendasi (Rp)</th>';
			 		$tot_peng_prop=0;
					$tot_peng_nominal=0;
					$tot_rek=0;
					$tot_rek_nominal=0;

				foreach ($periode as $key => $value) { 
					$noper = $value->noper; 
					$total_proposal = DB::select("select count(*) as jml from trx_proposal where noper=$noper and no_lembaga=$no_lembaga  "); 
					$total_proposal_rekomendasi = DB::select("select count(*) as jml from trx_proposal where noper=$noper and no_lembaga=$no_lembaga and status_prop=1 "); 

					$data_nominal = DB::select("select sum(nominal_pengajuan) as pengajuan 
						from 
							trx_proposal 
						where 
							noper=$noper and no_lembaga=$no_lembaga  
						group by noper "); 

					$data_nominal_rek = DB::select("select sum(nominal_rekomendasi) as rekomendasi 
						from 
							trx_proposal 
						where 
							noper=$noper and no_lembaga=$no_lembaga  and status_prop=1
						group by noper ");  
					
					if($total_proposal[0]->jml != null){
						$dashboard .='<tr>
								<td> '.$value->keterangan.' </td>
                                <td style="text-align:center;"> '.$total_proposal[0]->jml.'</td> 
                                <td style="text-align:right;">   '.number_format($data_nominal[0]->pengajuan,2).'</td> ';

                                if($total_proposal_rekomendasi[0]->jml != null){
                                	$dashboard .='<td style="text-align:center;"> '.$total_proposal_rekomendasi[0]->jml.'</td> 
                                <td style="text-align:right;"> '.number_format($data_nominal_rek[0]->rekomendasi,2).'</td></tr>'; 	
                                	$tot_rek=$tot_rek+$total_proposal_rekomendasi[0]->jml;
									$tot_rek_nominal=$tot_rek_nominal+$data_nominal_rek[0]->rekomendasi;
                                }else{
                                	$dashboard .='<td style="text-align:center;"> 0</td> 
                                <td style="text-align:right;"> 0 </td></tr>'; 	 
								 
                                }

							$tot_peng_prop=$tot_peng_prop+$total_proposal[0]->jml;
							$tot_peng_nominal=$tot_peng_nominal+$data_nominal[0]->pengajuan;

							

					} 
					
					
				}
				$dashboard .='<tr>
							<td> <b>Total</b> </td>
                            <td style="text-align:center;font-weight:bold;"> '.$tot_peng_prop.'</td> 
                            <td style="text-align:right;font-weight:bold;"> '.number_format($tot_peng_nominal,2).'</td> 
                            <td style="text-align:center;font-weight:bold;"> '.$tot_rek.'</td> 
                            <td style="text-align:right;font-weight:bold;"> '.number_format($tot_rek_nominal,2).'</td> 
							</tr>';   

				$dashboard .='</table>';

				// dd($feedLembaga);
				return view('admin.dashboard.lembaga')->with('feedLembaga',$feedLembaga)->with('dashboard',$dashboard);
														
				break;
			case '10': // For SKPD
				// dd(Auth::user());
				$feedSKPD = DB::table('mst_periode AS a')->select('a.jenisPeriodeID','a.tahun','a.tahun_proses','a.keterangan AS keterangan_periode','b.*')
				->join('trx_proposal AS b','a.noper','=','b.noper')
				->where('a.status',1)
				->where('b.no_skpd',Auth::user()->no_skpd)
				->where('b.status_kirim',1)
				->orderBy('b.tgl_prop','desc')
				->get();

				$periode = Periode::where('status',1)->get();
				$no_skpd = Auth::user()->no_skpd;
				$dashboard='<table  class="table table-bordered table-hovered">
					<th>Periode Tahapan</th>
					<th>Proposal Pengajuan</th>
					<th>Nominal Pengajuan (Rp)</th>
					<th>Proposal Direkomendasi</th>
					<th>Nominal Rekomendasi (Rp)</th>';
			 		$tot_peng_prop=0;
					$tot_peng_nominal=0;
					$tot_rek=0;
					$tot_rek_nominal=0;
					
				foreach ($periode as $key => $value) { 
					$noper = $value->noper; 
					$total_proposal = DB::select("select count(*) as jml from trx_proposal where noper=$noper and no_skpd=$no_skpd  "); 
					$total_proposal_rekomendasi = DB::select("select count(*) as jml from trx_proposal where noper=$noper and no_skpd=$no_skpd and status_prop=1 "); 

					$data_nominal = DB::select("select sum(nominal_pengajuan) as pengajuan 
						from 
							trx_proposal 
						where 
							noper=$noper and no_skpd=$no_skpd  
						group by noper "); 

					$data_nominal_rek = DB::select("select sum(nominal_rekomendasi) as rekomendasi 
						from 
							trx_proposal 
						where 
							noper=$noper and no_skpd=$no_skpd  and status_prop=1
						group by noper ");  
					
					if($total_proposal[0]->jml != null){
						$dashboard .='<tr>
								<td> '.$value->keterangan.' </td>
                                <td style="text-align:center;"> '.$total_proposal[0]->jml.'</td> 
                                <td style="text-align:right;">   '.number_format($data_nominal[0]->pengajuan,2).'</td> ';

                                if($total_proposal_rekomendasi[0]->jml != null){
                                	$dashboard .='<td style="text-align:center;"> '.$total_proposal_rekomendasi[0]->jml.'</td> 
                                <td style="text-align:right;"> '.number_format($data_nominal_rek[0]->rekomendasi,2).'</td></tr>'; 	
                                	$tot_rek=$tot_rek+$total_proposal_rekomendasi[0]->jml;
									$tot_rek_nominal=$tot_rek_nominal+$data_nominal_rek[0]->rekomendasi;
                                }else{
                                	$dashboard .='<td style="text-align:center;"> 0</td> 
                                <td style="text-align:right;"> 0 </td></tr>'; 	 
								 
                                }

							$tot_peng_prop=$tot_peng_prop+$total_proposal[0]->jml;
							$tot_peng_nominal=$tot_peng_nominal+$data_nominal[0]->pengajuan;

							

					} 
					
					
				}
				$dashboard .='<tr>
							<td> <b>Total</b> </td>
                            <td style="text-align:center;font-weight:bold;"> '.$tot_peng_prop.'</td> 
                            <td style="text-align:right;font-weight:bold;"> '.number_format($tot_peng_nominal,2).'</td> 
                            <td style="text-align:center;font-weight:bold;"> '.$tot_rek.'</td> 
                            <td style="text-align:right;font-weight:bold;"> '.number_format($tot_rek_nominal,2).'</td> 
							</tr>';   

				$dashboard .='</table>';
				 // total proposal, nominal pengajuan, nominal rekomendasi  
				return view('admin.dashboard.skpd')->with('feedSKPD',$feedSKPD)->with('dashboard',$dashboard);
				break;
			case '8': // For PPKD
				// dd(Auth::user());
				$feedPPKD = DB::table('mst_periode AS a')->select('a.jenisPeriodeID','a.tahun','a.tahun_proses','a.keterangan AS keterangan_periode','b.*')
				->join('trx_proposal AS b','a.noper','=','b.noper')
				->where('a.status',1)
				->where('b.status_kirim',1)
				// ->where('b.status_prop',1)
				->orderBy('b.tgl_prop','desc')
				->get();

				$periode = Periode::where('status',1)->get(); 
				$dashboard='<table  class="table table-bordered table-hovered">
					<th>Periode Tahapan</th>
					<th>Proposal Pengajuan</th>
					<th>Nominal Pengajuan (Rp)</th>
					<th>Proposal Direkomendasi</th>
					<th>Nominal Rekomendasi (Rp)</th>';
			 		$tot_peng_prop=0;
					$tot_peng_nominal=0;
					$tot_rek=0;
					$tot_rek_nominal=0;
					
				foreach ($periode as $key => $value) { 
					$noper = $value->noper; 
					$total_proposal = DB::select("select count(*) as jml from trx_proposal where noper=$noper and status_kirim=1   "); 
					$total_proposal_rekomendasi = DB::select("select count(*) as jml from trx_proposal where noper=$noper and status_kirim=1 and status_prop=1 "); 

					$data_nominal = DB::select("select sum(nominal_pengajuan) as pengajuan 
						from 
							trx_proposal 
						where 
							noper=$noper   and status_kirim=1   
						group by noper "); 

					$data_nominal_rek = DB::select("select sum(nominal_rekomendasi) as rekomendasi 
						from 
							trx_proposal 
						where 
							noper=$noper   and status_kirim=1 and status_prop=1
						group by noper ");  
					
					if($total_proposal[0]->jml != null){
						$dashboard .='<tr>
								<td> '.$value->keterangan.' </td>
                                <td style="text-align:center;"> '.$total_proposal[0]->jml.'</td> 
                                <td style="text-align:right;">   '.number_format($data_nominal[0]->pengajuan,2).'</td> ';

                                if($total_proposal_rekomendasi[0]->jml != null){
                                	$dashboard .='<td style="text-align:center;"> '.$total_proposal_rekomendasi[0]->jml.'</td> 
                                <td style="text-align:right;"> '.number_format($data_nominal_rek[0]->rekomendasi,2).'</td></tr>'; 	
                                	$tot_rek=$tot_rek+$total_proposal_rekomendasi[0]->jml;
									$tot_rek_nominal=$tot_rek_nominal+$data_nominal_rek[0]->rekomendasi;
                                }else{
                                	$dashboard .='<td style="text-align:center;"> 0</td> 
                                <td style="text-align:right;"> 0 </td></tr>'; 	 
								 
                                }

							$tot_peng_prop=$tot_peng_prop+$total_proposal[0]->jml;
							$tot_peng_nominal=$tot_peng_nominal+$data_nominal[0]->pengajuan;
 
					}
				} 
 				$dashboard .='<tr>
							<td> <b>Total</b> </td>
                            <td style="text-align:center;font-weight:bold;"> '.$tot_peng_prop.'</td> 
                            <td style="text-align:right;font-weight:bold;"> '.number_format($tot_peng_nominal,2).'</td> 
                            <td style="text-align:center;font-weight:bold;"> '.$tot_rek.'</td> 
                            <td style="text-align:right;font-weight:bold;"> '.number_format($tot_rek_nominal,2).'</td> 
							</tr>';   
				$dashboard .='</table>';

				return view('admin.dashboard.ppkd')->with('feedPPKD',$feedPPKD)->with('dashboard',$dashboard);
				break;
		}
		
		
	}


}