<?php 
namespace BPKAD\Http\Controllers\Admin\Member\Lembaga;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Foundation\UserBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use BPKAD\Foundation\UserLembagaLapanganBuilder;
use BPKAD\Foundation\MstLembagaBuilder;
use BPKAD\Http\Requests\StoreUserLembagaFilesRequest;
use BPKAD\Http\Requests\EditLembagaRequest;
use BPKAD\Http\Entities\Kota;
use BPKAD\Http\Entities\Kecamatan;
use BPKAD\Http\Entities\Kelurahan;
use BPKAD\Http\Entities\Lembaga;
use BPKAD\Http\Entities\Mst_rt;
use BPKAD\Http\Entities\Mst_rw;
use Auth;
use Carbon\Carbon;
use BPKAD\Services\NPWPApi;

class LembagaController extends BaseHomeController
{

	public function manage(Request $request){
		return view('admin.member.lembaga.manage')->with('recLembagaByNomor',MstLembagaBuilder::getByNomor(Auth::user()->no_lembaga))
													->with('recUserLembagaByNoLembaga',UserLembagaBuilder::getByNoLembaga(Auth::user()->no_lembaga))
													->with('recUserLembagaLapanganByNoLembaga',UserLembagaLapanganBuilder::getByNoLembaga(Auth::user()->no_lembaga));
	}

	public function edit($nomor){
		return view('admin.member.lembaga.edit')->with('recLembagaByNomor',MstLembagaBuilder::getByNomor(base64_decode($nomor)))
												->with('recKota',Kota::orderBy('nokota','asc')->get())
												->with('recKecamatan',Kecamatan::orderBy('nokec','asc')->get())
												->with('recKelurahan',Kelurahan::orderBy('nmkel','asc')->get())
												->with('recMstRT',Mst_rt::orderBy('id','asc')->get())
												->with('recMstRW',Mst_rw::orderBy('id','asc')->get());
	}

	public function update(EditLembagaRequest $request){
		$check = Lembaga::where('nomor',$request->nomor)->first();

		if ($check->npwp_check == preg_replace('/\D/', '', $request->npwp)) {
			Lembaga::where('nomor',$request->nomor)->update([
													  "nm_lembaga" => $request->nm_lembaga,
													  "npwp" => $request->npwp,
													  "npwp_check" => preg_replace('/\D/', '', $request->npwp),
													  "alamat" => $request->alamat,
													  "nokota" => $request->nokota,
													  "nokec" => $request->nokec,
													  "nokel" => $request->nokel,
													  "rtID" => $request->rtID,
													  "rwID" => $request->rwID,
													  "no_akta" => $request->no_akta,
													  "tgl_akta" => $request->tgl_akta,
													  "no_surat_domisili" => $request->no_surat_domisili,
													  "tgl_surat_domisili" => $request->tgl_surat_domisili,
													  "no_sertifikat" => $request->no_sertifikat,
													  "tgl_sertifikat" => $request->tgl_sertifikat,
													  "no_izin_operasional" => $request->no_izin_operasional,
													  "tgl_izin_operasional" => $request->tgl_izin_operasional,
													  "no_telepon" => $request->no_telepon,
													  "no_fax" => $request->no_fax,
													  "email" => $request->email1,
													  "nama_bank" => $request->nama_bank,
													  "no_rek" => $request->no_rek,
													  "pemilik_rek" => $request->pemilik_rek,
													  "kontak_person" => $request->kontak_person,
													  "nik_person" => $request->nik_person,
													  "email_person" => $request->email_person,
													  "alamat_person" => $request->alamat_person,
													  "no_hp_person" => $request->no_hp_person,
													  "kontak_person_sekretaris" => $request->kontak_person_sekretaris,
													  "email_person_sekretaris" => $request->email_person_sekretaris,
													  "alamat_person_sekretaris" => $request->alamat_person_sekretaris,
													  "no_hp_person_sekretaris" => $request->no_hp_person_sekretaris,
													  "kontak_person_bendahara" => $request->kontak_person_bendahara,
													  "email_person_bendahara" => $request->email_person_bendahara,
													  "alamat_person_bendahara" => $request->alamat_person_bendahara,
													  "no_hp_person_bendahara" => $request->no_hp_person_bendahara,
													  "upb" => Auth::user()->user_id,
													  "upd" => Carbon::now()
														]);

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Thank you.');

			return redirect(route('member.lembaga.manage'));
		}
		else{
			$checkNpwp = Lembaga::where('npwp_check',preg_replace('/\D/', '', $request->npwp))->count();
			if ($checkNpwp >= 1) {
				$request->session()->flash('status','warning');
				$request->session()->flash('msg','Data lembaga Anda tidak dapat disimpan karna NPWP yang Anda masukan sudah tersedia di-Sistem Kami.Silahkan masukan detail lembaga Anda dengan benar. Terima kasih!');
				return redirect(route('member.lembaga.manage'));
			}
			else{
				Lembaga::where('nomor',$request->nomor)->update([
													  "nm_lembaga" => $request->nm_lembaga,
													  "npwp" => $request->npwp,
													  "npwp_check" => preg_replace('/\D/', '', $request->npwp),
													  "alamat" => $request->alamat,
													  "nokota" => $request->nokota,
													  "nokec" => $request->nokec,
													  "nokel" => $request->nokel,
													  "rtID" => $request->rtID,
													  "rwID" => $request->rwID,
													  "no_akta" => $request->no_akta,
													  "tgl_akta" => $request->tgl_akta,
													  "no_surat_domisili" => $request->no_surat_domisili,
													  "tgl_surat_domisili" => $request->tgl_surat_domisili,
													  "no_sertifikat" => $request->no_sertifikat,
													  "tgl_sertifikat" => $request->tgl_sertifikat,
													  "no_izin_operasional" => $request->no_izin_operasional,
													  "tgl_izin_operasional" => $request->tgl_izin_operasional,
													  "no_telepon" => $request->no_telepon,
													  "no_fax" => $request->no_fax,
													  "email" => $request->email1,
													  "nama_bank" => $request->nama_bank,
													  "no_rek" => $request->no_rek,
													  "pemilik_rek" => $request->pemilik_rek,
													  "kontak_person" => $request->kontak_person,
													  "email_person" => $request->email_person,
													  "alamat_person" => $request->alamat_person,
													  "no_hp_person" => $request->no_hp_person,
													  "kontak_person_sekretaris" => $request->kontak_person_sekretaris,
													  "email_person_sekretaris" => $request->email_person_sekretaris,
													  "alamat_person_sekretaris" => $request->alamat_person_sekretaris,
													  "no_hp_person_sekretaris" => $request->no_hp_person_sekretaris,
													  "kontak_person_bendahara" => $request->kontak_person_bendahara,
													  "email_person_bendahara" => $request->email_person_bendahara,
													  "alamat_person_bendahara" => $request->alamat_person_bendahara,
													  "no_hp_person_bendahara" => $request->no_hp_person_bendahara
														]);

				$request->session()->flash('status','success');
				$request->session()->flash('msg','Your data has been save successfully.');

				return redirect(route('member.lembaga.manage'));
			}
		}
	}

	public function uploadFotoLembaga(){
		return view('admin.member.lembaga.form-upload-foto-lembaga');
	}

	public function uploadKtp(){
		return view('admin.member.lembaga.form-upload-ktp');
	}

	public function uploadFotoKetua(){
		return view('admin.member.lembaga.form-upload-foto-ketua');
	}

	public function uploadAkta(){
		return view('admin.member.lembaga.form-upload-akta');
	}

	public function uploadNpwp(){
		return view('admin.member.lembaga.form-upload-npwp');
	}

	public function uploadSkd(){
		return view('admin.member.lembaga.form-upload-skd');
	}

	public function uploadIzinOperasional(){
		return view('admin.member.lembaga.form-upload-izin-operasional');
	}

	public function uploadSertifikat(){
		return view('admin.member.lembaga.form-upload-sertifikat');
	}

	public function uploadRekening(){
		return view('admin.member.lembaga.form-upload-rekening');
	}

	public function editLokasi($nomor){
		return view('admin.member.lembaga.edit-lokasi')->with('nomor',base64_decode($nomor));
	}

	public function updateLokasi(Request $request)
	{
		Lembaga::where('nomor',$request->nomor)->update([
													  'longitude' => $request->longitude,
													  'latitude' => $request->latitude
													  ]);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.lembaga.manage'));
	}

	public function store(StoreUserLembagaFilesRequest $request){
		$this->userLembagaFilesRepository->store($request);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.lembaga.manage'));
	}

	public function cekNPWP(Request $request){
		$npwp         = $request->npwp; 
        $CHECK       = new NPWPApi;  
        $checkNik    = $CHECK->getData($npwp);   
        $result 	=  $checkNik['data']; 
        // dd($result->{'RespBody'}->NPWP);

        if(!isset($result->{'RespBody'}->NPWP)){   
          return response()->json(['msg'=>'NPWP tidak valid','status'=>false]);      
        }else{   
            return response()->json(['msg'=>'NPWP '.$result->{'RespBody'}->NPWP.' valid','status'=>true]);  
        }
    	
	}

	public function cekNIK(Request $request){
		$npwp         = $request->npwp; 
        $CHECK       = new NIKApi;  
        $checkNik    = $CHECK->getData($npwp);   
        $result 	=  $checkNik['data']; 
        dd($result);

        // if(!isset($result->{'RespBody'}->NPWP)){   
        //   return response()->json(['msg'=>'NPWP tidak valid','status'=>false]);      
        // }else{   
        //     return response()->json(['msg'=>'NPWP '.$result->{'RespBody'}->NPWP.' valid','status'=>true]);  
        // }
    	
	}

}