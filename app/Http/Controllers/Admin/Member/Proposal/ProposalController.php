<?php 
namespace BPKAD\Http\Controllers\Admin\Member\Proposal;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use Auth;
use Mail;
use Carbon\Carbon;
use BPKAD\Http\Requests\StoreProposalRequest;
use BPKAD\Foundation\ProposalBuilder;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\FotoProposal;
use BPKAD\Http\Entities\BerkasProposal;
use BPKAD\Http\Entities\BerkasRab;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\DetailPeriodeTahapan;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;

use BPKAD\Http\Entities\Cetak_nphd;
use BPKAD\Http\Entities\Cetak_nphd_penebalan;
use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Foundation\CetakNPHDPenebalanBuilder;
use BPKAD\Foundation\CetakPaktaIntegritasPenebalanBuilder;

use BPKAD\Http\Entities\Trx_proposal_administrasi_penebalan;
use BPKAD\Http\Entities\Trx_proposal_lapangan_penebalan;

use BPKAD\Http\Entities\Trx_rencana_penebalan;
use BPKAD\Http\Entities\Trx_rincian_penebalan;

use BPKAD\Http\Entities\ProposalPenebalanTAPD;

use BPKAD\Foundation\ProposalPenebalanTAPDBuilder;
use BPKAD\Foundation\ProposalPenebalanBANGGARBuilder;
use BPKAD\Foundation\ProposalPenebalanDDNBuilder;
use BPKAD\Foundation\ProposalDefinitifAPBDBuilder;
use DB;
use BPKAD\Http\Entities\ProposalDefinitifAPBD;
use BPKAD\Http\Entities\DokumenDefinitif;
use BPKAD\Foundation\UserBuilder;

use PDF;
use Config; 

class ProposalController extends BaseHomeController
{

	public function index(){
		return view('admin.member.proposal.index')
			->with('recPeriodeAktif',Periode::where('status',1)->orderBy('noper','asc')->get())
			->with('recPeriodeDefinitifAktif',Periode::where('status',1)->where('proposalDefinitif',1)->orderBy('noper','asc')->get());
	}

	public function manage($fase,$noper){
		//Mendapat No Lembaga
		$getNoLembaga = UserLembaga::where('no_lembaga',Auth::user()->no_lembaga)->first();

		return view('admin.member.proposal.manage-all-fase')
			->with('recProposalByNoLembaga',ProposalBuilder::getByNoLembagaFasePengusulan($getNoLembaga->no_lembaga,$noper))
			->with('totalProposal',count(ProposalBuilder::getByNoLembagaFasePengusulan($getNoLembaga->no_lembaga,$noper)))//hanya dapat membuat 1 proposal dalam setiap fase
			->with('fase',$fase)
			->with('recPeriodeByNoper',Periode::where('noper',$noper)->first())
			->with('recDetailPeriodeTahapanByNoper',DetailPeriodeTahapan::where('periodeID',$noper)->orderBy('tahapanID','asc')->get());
	}

	public function suratPernyataan($id,$fase){
		$getProposal = ProposalBuilder::getByID(base64_decode($id)); 
		$user = UserBuilder::getById(Auth::user()->user_id);
		$data['periode'] = Periode::where('noper',$getProposal->noper)->first();
		// dd($user);
		// dd($getProposal); 
		$data['user'] = $user;
		$pdf= PDF::loadView('pdf.surat-pernyataan',$data)
					->setOption('page-width', '210')
					->setOption('page-height', '330');
		return $pdf->download('Surat Pernyataan.pdf'); 
	}

	public function show($id,$fase){
		//Mendapat UUID Proposal Awal
		$getProposal = ProposalBuilder::getByID(base64_decode($id)); 
		
		// dd(ProposalDefinitifAPBDBuilder::getByUUID($getProposal->uuid));
		// dd($getProposal);
		// dd(ProposalBuilder::getByID(base64_decode($id)));
		return view('admin.member.proposal.show')
		->with('recProposalByID',ProposalBuilder::getByID(base64_decode($id)))
		->with('recPeriodeByNoper',Periode::where('noper',$getProposal->noper)->first())
		->with('recProposalAdministrasiByNoProp',Trx_proposal_administrasi::where('no_prop',base64_decode($id))->first())
		->with('recProposalLapanganByNoProp',Trx_proposal_lapangan::where('no_prop',base64_decode($id))->first())
		->with('rencana_anggaran',Trx_rencana::where('no_prop',base64_decode($id))->sum('rencana_anggaran'))
		->with('rencana_anggaran_skpd',Trx_rencana::where('no_prop',base64_decode($id))->sum('rencana_anggaran_skpd'))
		->with('recRencanaByNoProp',Trx_rencana::where('no_prop',base64_decode($id))->get())
		->with('cekPerjanjianNPHD',CetakNPHDBuilder::getArrayByNoProp(base64_decode($id)))
		->with('fase',$fase)
		->with('uuid',$getProposal->uuid)
		->with('noper',$getProposal->noper)

		// tapd
		->with('recProposalPenebalanTAPDByUUID',ProposalPenebalanTAPDBuilder::getByUUID($getProposal->uuid))
		->with('cekPerjanjianNPHDTAPD',CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($getProposal->uuid,'tapd'))
		->with('recRencanaByUUIDTAPD',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','tapd')->get())
		->with('rencana_anggaran_tapd',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','tapd')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanTAPDByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','tapd')->first())
		->with('recProposalLapanganPenebalanTAPDByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','tapd')->first())


		// banggar
		->with('recProposalPenebalanBANGGARByUUID',ProposalPenebalanBANGGARBuilder::getByUUID($getProposal->uuid))
		->with('cekPerjanjianNPHDBANGGAR',CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($getProposal->uuid,'banggar'))
		->with('recRencanaByUUIDBANGGAR',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','banggar')->get())
		->with('rencana_anggaran_banggar',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','banggar')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','banggar')->first())
		->with('recProposalLapanganPenebalanBANGGARByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','banggar')->first())

		// DDN
		->with('recProposalPenebalanDDNByUUID',ProposalPenebalanDDNBuilder::getByUUID($getProposal->uuid))
		->with('cekPerjanjianNPHDDDN',CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($getProposal->uuid,'ddn'))
		->with('recRencanaByUUIDDDN',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','ddn')->get())
		->with('rencana_anggaran_ddn',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','ddn')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanDDNByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','ddn')->first())
		->with('recProposalLapanganPenebalanDDNByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','ddn')->first())
		
		// APBD
		->with('recProposalDefinitifAPBDByUUID',ProposalDefinitifAPBDBuilder::getByUUID($getProposal->uuid))
		->with('cekPerjanjianNPHDAPBD',CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($getProposal->uuid,'apbd'))
		->with('cekPaktaIntegritas',CetakPaktaIntegritasPenebalanBuilder::getArrayByUUIDAndTahap($getProposal->uuid,'apbd'))
		->with('recRencanaByUUIDAPBD',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','apbd')->get())
		->with('rencana_anggaran_apbd',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','apbd')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanAPBDByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','apbd')->first())
		->with('recProposalLapanganPenebalanAPBDByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$getProposal->uuid)->where('tahap','apbd')->first());
	}

	public function create($fase,$noper){

		return view('admin.member.proposal.create')->with('tgl_prop',date('Y-m-d',strtotime(Carbon::now())))
												   ->with('recPeriodeByNoper',Periode::where('noper',$noper)->first())
												   ->with('fase',$fase);
	}

	public function store(StoreProposalRequest $request){

		// check jumlah proposal terkirim
		$checkJumlahProposalTerkirim = Proposal::where('no_lembaga',$request['no_lembaga'])->where('status_kirim',1)->where('noper',$request['noper'])->count();

		// jika sudah ada proposal yang terkirim tidak dapat mengirim proposal lainnya
		if ($checkJumlahProposalTerkirim  == 0 ) {
			
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');

			$no_prop = $this->proposalRepository->store($request);

			return redirect(route('member.proposal.edit',['id'=>base64_encode($no_prop),'fase'=>$request['fase'],'status'=>'rencana']));
		
		}else{
		
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.Maaf Lembaga Anda hanya dapat mengirim 1 proposal pada masing-masing Fase.');

			return redirect(route('member.proposal.manage',['fase'=>$request['fase'],'noper'=>$request['noper']]));
		
		}

	}

	public function edit($id,$fase,$status=null){
		//get Proposal By ID
		$getProposalByID= ProposalBuilder::getByID(base64_decode($id));
		$getPengajuanProposalStatus = DetailPeriodeTahapan::where('periodeID',$getProposalByID->noper)->where('tahapanID',1)->first();

		return view('admin.member.proposal.edit')->with('recProposalByID',ProposalBuilder::getByID(base64_decode($id)))
			->with('fase',$fase)
			->with('status',$status)
			->with('getPengajuanProposalStatus',$getPengajuanProposalStatus->status)
			->with('recRencanaByNoProp',Trx_rencana::where('no_prop',base64_decode($id))->get())
			->with('recProposalAdministrasiByNoProp',Trx_proposal_administrasi::where('no_prop',base64_decode($id))->first())
			->with('recProposalLapanganByNoProp',Trx_proposal_lapangan::where('no_prop',base64_decode($id))->first())
			->with('rencana_anggaran',Trx_rencana::where('no_prop',base64_decode($id))->sum('rencana_anggaran'));
	}

	public function update(StoreProposalRequest $request){ 
		$this->proposalRepository->update($request);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.edit',[ 'id'=>base64_encode($request['no_prop']),'fase'=>$request['fase'] ]));
	}

	public function setujuiKirim(Request $request){

		// check jumlah proposal terkirim
		$checkJumlahProposalTerkirim = Proposal::where('no_lembaga',$request['no_lembaga'])->where('status_kirim',1)->where('noper',$request['noper'])->count();

		// jika sudah ada proposal yang terkirim tidak dapat mengirim proposal lainnya
		if ($checkJumlahProposalTerkirim  == 0 ) {
			
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Proposal Anda berhasil terkirim. Silahkan Anda cek email Anda/Lembaga. Terima Kasih');

			$no_prop = $this->proposalRepository->setujuiKirim($request);

			return redirect(route('member.proposal.manage',['fase'=>$request['fase'],'noper'=>$request['noper']]));
		
		}else{
		
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.Maaf Lembaga Anda hanya dapat mengirim 1 proposal pada masing-masing Fase.');

			return redirect(route('member.proposal.manage',['fase'=>$request['fase'],'noper'=>$request['noper']]));
		
		}

	}

	public function delete(Request $request){
		Proposal::where('no_prop', $request->no_prop)->delete();
		FotoProposal::where('no_prop', $request->no_prop)->delete();
		BerkasProposal::where('no_prop', $request->no_prop)->delete();
		BerkasRab::where('no_prop', $request->no_prop)->delete();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.manage'));
	}

	public function resendEmail(Request $request,$no_prop,$email,$real_name,$email_lembaga,$no_lembaga,$tahun_anggaran,$fase){

		$strQuery = Proposal::where('no_prop',base64_decode($no_prop))->first();

		$data = ['jdl_prop' => $strQuery->jdl_prop,
				 'nominal_pengajuan' => number_format($strQuery->nominal_pengajuan,2,',','.'),
				 'email' => $email,
				 // 'email_lembaga' => 'hary.purnomo87@gmail.com',
				 'email_lembaga' => $email_lembaga,
				 'tahun_anggaran' => $tahun_anggaran,
				 'no_lembaga' => $no_lembaga,
				 'real_name' => $real_name,
				 'base64'=>'data:image/png;base64'
				];

	    Mail::send('emails.kirim-proposal', ['data' => $data], function ($m) use ($data) {
	            $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

	            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Bukti Kirim Proposal');
	        });

	    $request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.show',['id'=>$no_prop,'fase'=>$fase]));
	}

	// input rencana
	public function createRencana($no_prop,$fase){
		return view('admin.member.proposal.create-rencana')->with('fase',$fase)
															->with('no_prop',$no_prop);
	}

	public function storeRencana(Request $request){

		$store = new Trx_rencana;
		$store->no_prop = base64_decode($request['no_prop']);
		$store->rencana = $request['rencana'];
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.edit',['id'=>$request['no_prop'],'fase'=>$request['fase'],'status'=>'rencana']));
	}

	public function editRencana($id,$fase){
		return view('admin.member.proposal.edit-rencana')->with('fase',$fase)
														->with('recRencanaByID',Trx_rencana::find($id));
	}

	public function updateRencana(Request $request){
		$update = Trx_rencana::find($request['id']);
		$update->rencana = $request['rencana'];
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.edit',['id'=>base64_encode($request['no_prop']),'fase'=>$request['fase'],'status'=>'rencana']));
	}

	public function deleteRencana(Request $request){
		$update = Trx_rencana::find($request['id']);
		$update->delete();

		$deleteAll = Trx_rincian::where('rencanaID',$request['id'])->delete();

		$request->session()->flash('status','warning');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('member.proposal.edit',['id'=>base64_encode($request['no_prop']),'fase'=>$request['fase'],'status'=>'rencana']));
	}

	// input rincian
	public function createRincian($id,$no_prop,$fase){
		return view('admin.member.proposal.create-rincian')->with('fase',$fase)
															->with('no_prop',$no_prop)
															->with('id',$id);
	}

	public function storeRincian(Request $request){

		$volume1 = str_replace(".", "", $request['volume1']);
		$volume2 = str_replace(".", "", $request['volume2']);
		$harga_satuan = str_replace(".", "", $request['harga_satuan']);
		
		if($volume1 > 0 && $volume2 > 0){

			$store = new Trx_rincian;
			$store->rencanaID = $request['rencanaID'];
			$store->rincian = $request['rincian'];
			$store->volume1 = doubleval(str_replace(",",".",$volume1));
			$store->satuan1 = $request['satuan1'];
			$store->volume2 = doubleval(str_replace(",",".",$volume2));
			$store->satuan2 = $request['satuan2'];
			$store->harga_satuan = doubleval(str_replace(",",".",$harga_satuan));
			$store->rincian_anggaran = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));

			$store->harga_satuan_skpd = doubleval(str_replace(",",".",$harga_satuan));
			$store->rincian_anggaran_skpd = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));

			$store->save();

			//total rincian anggaran by rencanaID
			$sum = Trx_rincian::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran');
			
			$update = Trx_rencana::find($request['rencanaID']);
			$update->rencana_anggaran = $sum;
			$update->save();

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been sent successfully.');
		}else{
			$request->session()->flash('status','error');
			$request->session()->flash('msg','Nilai volume tidak boleh 0.');
		}

		return redirect(route('member.proposal.edit',['id'=>$request['no_prop'],'fase'=>$request['fase'],'status'=>'rencana']));
	}

	public function editRincian($id,$no_prop,$fase){ 
		return view('admin.member.proposal.edit-rincian')->with('fase',$fase)
															->with('no_prop',base64_encode($no_prop))
															->with('recTrxRincianByID',Trx_rincian::find($id));
	}

	public function updateRincian(Request $request){

		$volume1 			= str_replace(".", "", $request['volume1']);
		$volume2 			= str_replace(".", "", $request['volume2']);
		$harga_satuan 		= str_replace(".", "", $request['harga_satuan']);
		$harga_satuan_skpd 	= str_replace(".", "", $request['harga_satuan_skpd']);
 
		if($volume1 > 0 && $volume2 > 0){
			$update = Trx_rincian::find($request['id']);
			$update->rincian = $request['rincian'];
			$update->volume1 = doubleval(str_replace(",",".",$volume1));
			$update->satuan1 = $request['satuan1'];
			$update->volume2 = doubleval(str_replace(",",".",$volume2));
			$update->satuan2 = $request['satuan2'];
			$update->harga_satuan 		= doubleval(str_replace(",",".",$harga_satuan));

			if(Auth::user()->group_id==10){ 
				$update->harga_satuan_skpd 	= doubleval(str_replace(",",".",$harga_satuan_skpd));
				$update->rincian_anggaran_skpd = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan_skpd));
			}else{
				$update->harga_satuan_skpd 	= doubleval(str_replace(",",".",$harga_satuan));
				$update->rincian_anggaran_skpd = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));
			} 

			$update->rincian_anggaran 	= ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));

			
			$update->save();

			//total rincian anggaran by rencanaID
			$sum = Trx_rincian::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran');
			
			$update = Trx_rencana::find($request['rencanaID']);
			$update->rencana_anggaran 		= $sum;
			if(Auth::user()->group_id==10){ 
				$sum_skpd = Trx_rincian::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran_skpd');
				$update->rencana_anggaran_skpd 	= $sum_skpd;
			}else{ 
				$update->rencana_anggaran_skpd 	= $sum;
			} 
			$update->save();

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been sent successfully.');
		}else{
			$request->session()->flash('status','error');
			$request->session()->flash('msg','Nilai volume tidak boleh 0.');
		}
		

		if(Auth::user()->group_id==10)
		{ 
			return redirect(route('skpd.proposal.show',['id'=>$request['no_prop'],'fase'=>$request['fase'],'status'=>'rencana'])); 
		}else{
			return redirect(route('member.proposal.edit',['id'=>$request['no_prop'],'fase'=>$request['fase'],'status'=>'rencana'])); 
		}
	}

	public function updateRincianAll(Request $request){ 
	 
		$request_arr = (array) $request->all(); 

		foreach ($request_arr as $key => $value) { 
			$arr = explode(",",$key);  

			if($arr[0] == "rincian_anggaran_skpd"){  
				$value = str_replace("_","",$value); 
				$value = str_replace("__","",$value); 
				$value = str_replace("___","",$value); 
			// 	//update rincian 
				$update = Trx_rincian::find($arr[1]);
				$update->harga_satuan_skpd 		=  doubleval(str_replace(".", "",$value))/($update->volume1*$update->volume2);
				$update->rincian_anggaran_skpd 	=  doubleval(str_replace(".", "",$value));
				$update->save();  
			// 	//update nominal  
			} 
		}

		//update total per rencana
		$rencana = Trx_rencana::where("no_prop",base64_decode($request->no_prop))->get();
		$totalRek = 0;
		foreach ($rencana as $row) {
			$total = DB::select("SELECT 
							SUM(rincian_anggaran_skpd) AS total
						FROM 
							trx_rincian
						WHERE
							rencanaID='".$row->id."'"); 
			//update rencana
			$updateRencana = Trx_rencana::find($row->id);
			$updateRencana->rencana_anggaran_skpd = $total[0]->total;
			$updateRencana->save();

			$totalRek = $totalRek + $total[0]->total;
			// echo $total[0]->total."<br>";
		}
	 	
	 	//update nominal rekomendasi
	 

	 	// $proposal = Proposal::where('no_prop',base64_decode($request->no_prop))->first(); 
	 	// $proposal->nominal_rekomendasi = $totalRek;
	 	// $proposal->save();

	 	$proposal = DB::select("
			UPDATE   trx_proposal SET nominal_rekomendasi='".$totalRek."'
			WHERE no_prop='".base64_decode($request->no_prop)."'");


		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been update successfully.');

		return redirect()->back();
	}

	public function updateAllProposalPengajuan(){ 
	 	
		//update rincian proposal
		$allRincian1 = DB::select("
			UPDATE   trx_rincian SET harga_satuan_skpd=harga_satuan
			WHERE rencanaID IN(
			 SELECT id FROM trx_rencana WHERE no_prop IN(SELECT no_prop FROM trx_proposal WHERE noper IN ('10','11'))
			)");

		$allRincian2 = DB::select("
			UPDATE   trx_rincian SET rincian_anggaran_skpd=rincian_anggaran
			WHERE rencanaID IN(
			 SELECT id FROM trx_rencana WHERE no_prop IN(SELECT no_prop FROM trx_proposal WHERE noper IN ('10','11'))
			)");

 
		//update total per rencana
		$rencana = DB::select("SELECT DISTINCT rencanaID FROM trx_rincian WHERE rencanaID IN(
					 SELECT id FROM trx_rencana WHERE no_prop IN(SELECT no_prop FROM trx_proposal WHERE noper IN ('10','11'))
					) LIMIT 100000");

		foreach ($rencana as $row) { 
			$total = DB::select("SELECT 
							SUM(harga_satuan_skpd) AS total
						FROM 
							trx_rincian
						WHERE
							rencanaID='".$row->rencanaID."'"); 
			 
			//update rencana
			//echo $total[0]->total."<br>"; 
			if($total[0]->total != null || $total[0]->total > 0){ 
				$updateRencana = Trx_rencana::find($row->rencanaID);
				$updateRencana->rencana_anggaran_skpd = $total[0]->total;
				$updateRencana->save();
			}
			// echo $total[0]->total."<br>";
		}

		//update proposal nominal rekomendasi
		$proposalAll = DB::select("SELECT * FROM trx_proposal WHERE noper IN ('10','11') LIMIT 100000");

		foreach ($proposalAll as $rowProp) { 
			$total = DB::select("SELECT DISTINCT no_prop,SUM(rencana_anggaran_skpd) AS total FROM trx_rencana WHERE no_prop='".$rowProp->no_prop."'"); 
			 
			//update rencana 
			if($total[0]->total != null || $total[0]->total > 0){ 
				DB::select("UPDATE trx_proposal SET  nominal_rekomendasi='".$total[0]->total."' WHERE 
					no_prop='".$rowProp->no_prop."'");
			}
			// echo $total[0]->total."<br>";
		}



		echo "oke";
		exit;
	}

	public function deleteRincian(Request $request){
		$update = Trx_rincian::find($request['id']);
		$update->delete();

		//total rincian anggaran by rencanaID
		$sum = Trx_rincian::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran');

		$update = Trx_rencana::find($request['rencanaID']);
		$update->rencana_anggaran = $sum;
		$update->save();

		$request->session()->flash('status','warning');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('member.proposal.edit',['id'=>$request['no_prop'],'fase'=>$request['fase'],'status'=>'rencana']));
	}

	//Perjanjian NPHD
	public function showNaskahNPHD($no_prop,$fase){
		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDBuilder::getArrayByNoProp($no_prop);

		return view('admin.member.proposal.show-naskah-nphd')->with('no_prop',$no_prop)
														->with('fase',$fase)
														->with('dataCNPHD',$dataCNPHD)
														->with('recProposalByID',ProposalBuilder::getByID($no_prop));
	}

	public function updateNaskahNPHD(Request $request){

		$update = Cetak_nphd::firstOrCreate(['no_prop'=>$request->no_prop]);
		$update->hari1 = $request->hari1;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nama1 = $request->nama1;
		$update->nama2 = $request->nama2;
		$update->jabatan1 = $request->jabatan1;
		$update->alamat = $request->alamat;
		$update->deskripsi_jabatan = $request->deskripsi_jabatan;
		$update->pasal = $request->pasal;
		$update->nomor1 = $request->nomor1;
		$update->hadapan = $request->hadapan;
		$update->notaris = $request->notaris;
		$update->nomor_ham = $request->nomor_ham;
		$update->nomor2 = $request->nomor2;
		$update->tahun2 = $request->tahun2;
		$update->tanggal2 = $request->tanggal2;
		$update->nomor3 = $request->nomor3;
		$update->tahun3 = $request->tahun3;
		$update->peraturan4 = $request->peraturan4;
		$update->nomor4 = $request->nomor4;
		$update->tahun4 = $request->tahun4;
		$update->tentang4 = $request->tentang4;
		$update->tahun_anggaran1 = $request->tahun_anggaran1;
		$update->nomor5 = $request->nomor5;
		$update->tahun5 = $request->tahun5;
		$update->nomor6 = $request->nomor6;
		$update->tahun6 = $request->tahun6;
		$update->tentang6 = $request->tentang6;
		$update->tahun_anggaran2 = $request->tahun_anggaran2;
		$update->nomor7 = $request->nomor7;
		$update->tanggal7 = $request->tanggal7;
		$update->bulan7 = $request->bulan7;
		$update->tahun7 = $request->tahun7;
		$update->senilai_angka = $request->senilai_angka;
		$update->senilai_teks = $request->senilai_teks;
		$update->hari2 = $request->hari2;
		$update->tanggal3 = $request->tanggal3;
		$update->kegiatan1 = $request->kegiatan1;
		$update->kegiatan2 = $request->kegiatan2;
		$update->kegiatan3 = $request->kegiatan3;
		$update->jabatan2 = $request->jabatan2;
		$update->nama_pihak_kedua = $request->nama_pihak_kedua;
		$update->nip_pihak_kedua = $request->nip_pihak_kedua;
		$update->nama_kepala = $request->nama_kepala;
		$update->nip_kepala = $request->nip_kepala;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.naskah.perjanjian.nphd.show',['no_prop'=>$request->no_prop,'fase'=>$request->fase]));

	}

	public function generateNPHDToPDF($id){

		// update is_generate cetak nphd
		$update = Cetak_nphd::where('no_prop',$id)->first();
		$update->is_generate = 1;
		$update->save();


		// get array cetak kelengkapan administrasi
		$dataCNPHD = CetakNPHDBuilder::getArrayByNoProp($id);

		// get array detail proposal by no_prop
		$recProposalByID = ProposalBuilder::getByID($dataCNPHD['no_prop']);

		$data = ['dataCNPHD' => $dataCNPHD,
				 'recProposalByID' => $recProposalByID
				];

		// return view('pdf.naskah-perjanjian-nphd',$data);
		// exit();

		$pdf = PDF::loadView('pdf.naskah-perjanjian-nphd',$data)
		->setOption('page-width', '210')
		->setOption('page-height', '330')
		->setOption('margin-bottom', '20')
        ->setOption('margin-left', '20')
        ->setOption('margin-right','20')
        ->setOption('margin-top', '20');

		return $pdf->download('Naskah-Perjanjian-NPHD.pdf');
	}
	// End Perjanjian NPHD

	// untuk sementara
	public function perbaikan($id){
		return view('admin.member.proposal.perbaikan')->with('recProposalByID',ProposalBuilder::getByID(base64_decode($id)));
	} 

	public function updatePerbaikan(Request $request){

		$this->proposalRepository->updatePerbaikan($request);

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('dashboard.member'));
	}

	public function uploadDokumenList($noper,$no_prop,$uuid){ 
		$data['noper'] 		= $noper;
		$data['no_prop'] 	= $no_prop;
		$data['uuid'] 		= $uuid;
		
		if(Auth::user()->group_id==12){ 
			$data['recDokumenByID'] = DokumenDefinitif::where("uuid",$uuid)->where("jenis","!=","srb")->get();
		}else{
			$data['recDokumenByID'] = DokumenDefinitif::where("uuid",$uuid)->where("jenis","srb")->get();
		}
		
		$data['proposal'] 	= ProposalDefinitifAPBD::where("uuid_proposal_pengajuan",$uuid)->first(); 

		return view('admin.member.proposal.dokumen.manage',$data); 
	}

	public function uploadDokumen($noper,$no_prop,$uuid){
		$data['noper'] 		= $noper;
		$data['no_prop'] 	= $no_prop;
		$data['uuid'] 		= $uuid;
		$data['proposal'] 	= ProposalDefinitifAPBD::where("uuid_proposal_pengajuan",$uuid)->first();
		return view('admin.member.proposal.dokumen.create',$data); 
	}

	public function uploadDokumenStore(Request $request){ 
	  	$store = new DokumenDefinitif;
	  	if($request->hasFile('file_dokumen')){
	  			$fileName = $request->jenis_dokumen.'_'.Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request->file_dokumen->getClientOriginalName());  
 				$store->file 			= $fileName;  
              $destinationPath = 'laporan/'; 
              $request->file_dokumen->move($destinationPath, $fileName);
       	}

        $store->uuid 	= $request->uuid; 
        $store->name 	= $request->name; 
        $store->jenis 	= $request->jenis_dokumen; 
        $store->save();
 
       	$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		if(Auth::user()->group_id==12){ 
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');
			return redirect($request->prev);
		}else{
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');
			return redirect($request->prev);
		}
	}

	public function uploadDokumenEdit($id)
	{ 	$id = base64_decode($id);
		$data['dokumen'] 	= DokumenDefinitif::find($id);
		return view('admin.member.proposal.dokumen.edit',$data); 
	}

	public function uploadDokumenUpdate(Request $request){
		$store = DokumenDefinitif::find($request->id);
	  	if($request->hasFile('file_dokumen')){
	  			$fileName = $request->jenis_dokumen.'_'.Carbon::now('Asia/Jakarta')->format('dmYhis').'_'.str_replace(' ', '_', $request->file_dokumen->getClientOriginalName());  
 				$store->file 			= $fileName;  
              $destinationPath = 'laporan/'; 
              $request->file_dokumen->move($destinationPath, $fileName);
       	}
 
        $store->name 	= $request->name; 
        $store->jenis 	= $request->jenis_dokumen; 
        $store->save();
 
       	$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		if(Auth::user()->group_id==12){ 
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');
			return redirect($request->prev);
		}else{
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');
			return redirect($request->prev);
		}
	}

	public function uploadDokumenDelete($id){
		$id = base64_decode($id);
		 
	}


}