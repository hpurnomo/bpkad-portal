<?php 
namespace BPKAD\Http\Controllers\Admin\Member\Proposal;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use Auth;
use Mail;
use Carbon\Carbon;
use BPKAD\Http\Requests\StoreProposalRequest;
use BPKAD\Foundation\ProposalBuilder;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\FotoProposal;
use BPKAD\Http\Entities\BerkasProposal;
use BPKAD\Http\Entities\BerkasRab;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\DetailPeriodeTahapan;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;

use BPKAD\Http\Entities\Cetak_nphd;
use BPKAD\Http\Entities\Cetak_nphd_penebalan;
use BPKAD\Http\Entities\Cetak_pakta_integritas_penebalan;

use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Foundation\CetakNPHDPenebalanBuilder;
use BPKAD\Foundation\CetakPaktaIntegritasPenebalanBuilder;

use BPKAD\Http\Entities\ProposalPenebalanTAPD;
use BPKAD\Foundation\ProposalPenebalanTAPDBuilder;
use BPKAD\Foundation\ProposalPenebalanBANGGARBuilder;
use BPKAD\Foundation\ProposalPenebalanDDNBuilder;
use BPKAD\Foundation\ProposalDefinitifAPBDBuilder;

use BPKAD\Http\Entities\ProposalPenebalanBANGGAR;


use BPKAD\Http\Entities\ProposalPenebalanDDN;


use BPKAD\Http\Entities\ProposalDefinitifAPBD;


use BPKAD\Http\Entities\Trx_rencana_penebalan;
use BPKAD\Http\Entities\Trx_rincian_penebalan;

use PDF;
use View;
use Config;
use DB;

class ProposalPenebalanController extends BaseHomeController
{
	public function create($fase,$noper,$uuid,$tahap,$lembaga){  

		return view('admin.member.proposal.penebalan.create')->with('fase',$fase)
												->with('noper',$noper)
												->with('uuid',$uuid)
												->with('tahap',$tahap)
												->with('recProposalByUUID',Proposal::where('uuid',$uuid)->first());
	}

	public function store(Request $request){
		// dd($request);

		if ($request->tahap=='tapd') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		elseif ($request->tahap=='banggar') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		elseif ($request->tahap=='ddn') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		elseif ($request->tahap=='apbd') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		

		// jika sudah ada proposal yang terkirim tidak dapat mengirim proposal lainnya
		if ($checkJumlahProposalTerkirim  == 0 ) {
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');

			$no_prop = $this->proposalPenebalanRepository->store($request);

			return redirect(route('member.proposal.penebalan.edit',['fase'=>$request->fase,'noper'=>$request->noper,'uuid'=>$request->uuid,'tahap'=>$request->tahap,'status'=>'rencana']));
		}else{
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.Maaf Lembaga Anda hanya dapat mengirim 1 proposal pada masing-masing Fase.');

			return redirect(route('member.proposal.manage',['fase'=>$request['fase'],'noper'=>$request['noper']]));
		}

	}

	public function edit($fase,$noper,$uuid,$tahap,$status=null){

		if($tahap=='tapd'){
			return view('admin.member.proposal.penebalan.edit-tapd')->with('fase',$fase)
			->with('noper',$noper)
			->with('uuid',$uuid)
			->with('tahap',$tahap)
			->with('status',$status)
			->with('recProposalByUUID',Proposal::where('uuid',$uuid)->first())
			->with('recRencanaByNoProp',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','tapd')->get())
			->with('rencana_anggaran',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','tapd')->sum('rencana_anggaran'))
			->with('recProposalPenebalanUUID',ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$uuid)->first());
		}
		elseif($tahap=='banggar'){
			return view('admin.member.proposal.penebalan.edit-banggar')->with('fase',$fase)
			->with('noper',$noper)
			->with('uuid',$uuid)
			->with('tahap',$tahap)
			->with('status',$status)
			->with('recProposalByUUID',Proposal::where('uuid',$uuid)->first())
			->with('recRencanaByNoProp',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','banggar')->get())
			->with('rencana_anggaran',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','banggar')->sum('rencana_anggaran'))
			->with('recProposalPenebalanUUID',ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$uuid)->first());
		}
		elseif($tahap=='ddn'){
			return view('admin.member.proposal.penebalan.edit-ddn')->with('fase',$fase)
			->with('noper',$noper)
			->with('uuid',$uuid)
			->with('tahap',$tahap)
			->with('status',$status)
			->with('recProposalByUUID',Proposal::where('uuid',$uuid)->first())
			->with('recRencanaByNoProp',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','ddn')->get())
			->with('rencana_anggaran',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','ddn')->sum('rencana_anggaran'))
			->with('recProposalPenebalanUUID',ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$uuid)->first());
		}
		elseif($tahap=='apbd'){
			return view('admin.member.proposal.penebalan.edit-apbd')->with('fase',$fase)
			->with('noper',$noper)
			->with('uuid',$uuid)
			->with('tahap',$tahap)
			->with('status',$status)
			->with('recProposalByUUID',Proposal::where('uuid',$uuid)->first())
			->with('recRencanaByNoProp',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','apbd')->get())
			->with('rencana_anggaran',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap','apbd')->sum('rencana_anggaran'))
			->with('recProposalPenebalanUUID',ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$uuid)->first());
		}

	}

	public function update(Request $request){

		$this->proposalPenebalanRepository->update($request);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.penebalan.edit',[ 'fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid'],'tahap'=>$request['tahap'] ]));

	}

	public function setujuiKirim(Request $request){

		// dd($request->all());

		// check jumlah proposal terkirim
		if ($request->tahap=='tapd') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		elseif ($request->tahap=='banggar') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		elseif ($request->tahap=='ddn') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}
		elseif ($request->tahap=='apbd') {
			// check jumlah proposal terkirim
			$checkJumlahProposalTerkirim = ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request->uuid)->where('status_kirim',1)->count();
		}

		// jika sudah ada proposal yang terkirim tidak dapat mengirim proposal lainnya
		if ($checkJumlahProposalTerkirim  == 0 ) {
			
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Proposal Anda berhasil terkirim. Silahkan Anda cek email Anda/Lembaga. Terima Kasih');

			$this->proposalPenebalanRepository->setujuiKirim($request);

			return redirect(route('member.proposal.show',['id'=>base64_encode($request->no_prop),'fase'=>$request->fase]));
		
		}else{
		
			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Maaf Lembaga Anda hanya dapat mengirim 1 proposal pada masing-masing Fase.');

			return redirect(route('member.proposal.show',['id'=>base64_encode($request->no_prop),'fase'=>$request->fase]));
		
		}

	}

	public function resendEmail(Request $request,$no_prop,$email,$real_name,$email_lembaga,$no_lembaga,$tahun_anggaran,$fase,$tahap){
		
		if ($tahap=='tapd') {
			$strQuery = ProposalPenebalanTAPD::where('no_prop',base64_decode($no_prop))->first();
			$detailProposal = Proposal::where('uuid',$strQuery->uuid_proposal_pengajuan	)->first();
		}
		elseif ($tahap=='banggar') {
			$strQuery = ProposalPenebalanBANGGAR::where('no_prop',base64_decode($no_prop))->first();
			$detailProposal = Proposal::where('uuid',$strQuery->uuid_proposal_pengajuan	)->first();
		}
		elseif ($tahap=='ddn') {
			$strQuery = ProposalPenebalanDDN::where('no_prop',base64_decode($no_prop))->first();
			$detailProposal = Proposal::where('uuid',$strQuery->uuid_proposal_pengajuan	)->first();
		}
		elseif ($tahap=='apbd') {
			$strQuery = ProposalDefinitifAPBD::where('no_prop',base64_decode($no_prop))->first();
			$detailProposal = Proposal::where('uuid',$strQuery->uuid_proposal_pengajuan	)->first();
		}

		$data = ['jdl_prop' => $strQuery->jdl_prop,
				 'nominal_pengajuan' => number_format($strQuery->nominal_pengajuan,2,',','.'),
				 'email' => $email,
				 // 'email_lembaga' => 'hary.purnomo87@gmail.com',
				 'email_lembaga' => $email_lembaga,
				 'tahun_anggaran' => $tahun_anggaran,
				 'no_lembaga' => $no_lembaga,
				 'real_name' => $real_name,
				 'base64'=>'data:image/png;base64'
				];

	    Mail::send('emails.kirim-proposal', ['data' => $data], function ($m) use ($data) {
	            $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

	            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Bukti Kirim Proposal');
	        });

	    $request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.show',['id'=>base64_encode($detailProposal->no_prop),'fase'=>$fase]));
	}

	// input rencana
	public function createRencana($fase,$noper,$uuid,$tahap,$status=null){
		return view('admin.member.proposal.penebalan.create-rencana')->with('fase',$fase)
												->with('noper',$noper)
												->with('uuid',$uuid)
												->with('tahap',$tahap)
												->with('status',$status);
	}

	public function storeRencana(Request $request){
		
		$store = new Trx_rencana_penebalan;
		$store->uuid_proposal_pengajuan = $request->uuid_proposal_pengajuan;
		$store->tahap = $request->tahap;
		$store->rencana = $request->rencana;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.penebalan.edit',['fase'=>$request->fase,'noper'=>$request->noper,'uuid'=>$request->uuid_proposal_pengajuan,'tahap'=>$request->tahap,'status'=>$request->status]));
	}

	public function editRencana($id,$fase,$noper,$uuid,$tahap,$status=null){
		return view('admin.member.proposal.penebalan.edit-rencana')->with('fase',$fase)
												->with('noper',$noper)
												->with('uuid',$uuid)
												->with('tahap',$tahap)
												->with('status',$status)
												->with('recRencanaByUUID',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)
													->where('tahap',$tahap)
													->where('id',$id)
													->first());
	}

	public function updateRencana(Request $request){
		$update = Trx_rencana_penebalan::find($request['id']);
		$update->rencana = $request['rencana'];
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
	}

	public function deleteRencana(Request $request){
		$update = Trx_rencana_penebalan::find($request['id']);
		$update->delete();

		$deleteAll = Trx_rincian_penebalan::where('rencanaID',$request['id'])->delete();

		$request->session()->flash('status','warning');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
	}

	// input rincian
	public function createRincian($id,$fase,$noper,$uuid,$tahap,$status=null){
		return view('admin.member.proposal.penebalan.create-rincian')->with('id',$id)
												->with('fase',$fase)
												->with('noper',$noper)
												->with('uuid',$uuid)
												->with('tahap',$tahap)
												->with('status',$status);
	}

	public function storeRincian(Request $request){

		$volume1 = str_replace(".", "", $request['volume1']);
		$volume2 = str_replace(".", "", $request['volume2']);
		$harga_satuan = str_replace(".", "", $request['harga_satuan']);

		$store = new Trx_rincian_penebalan;
		$store->rencanaID = $request['rencanaID'];
		$store->rincian = $request['rincian'];
		$store->volume1 = doubleval(str_replace(",",".",$volume1));
		$store->satuan1 = $request['satuan1'];
		$store->volume2 = doubleval(str_replace(",",".",$volume2));
		$store->satuan2 = $request['satuan2'];
		$store->harga_satuan = doubleval(str_replace(",",".",$harga_satuan));
		$store->rincian_anggaran = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));
		$store->save();

		//total rincian anggaran by rencanaID
		$sum = Trx_rincian_penebalan::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran');
		
		$update = Trx_rencana_penebalan::find($request['rencanaID']);
		$update->rencana_anggaran = $sum;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
	}

	public function editRincian($id,$fase,$noper,$uuid,$tahap,$status=null){
		return view('admin.member.proposal.penebalan.edit-rincian')
			->with('id',$id)
			->with('fase',$fase)
			->with('noper',$noper)
			->with('uuid',$uuid)
			->with('tahap',$tahap)
			->with('status',$status)
			->with('recTrxRincianByID',Trx_rincian_penebalan::find($id));
	}

	public function updateRincian(Request $request){

		$volume1 = str_replace(".", "", $request['volume1']);
		$volume2 = str_replace(".", "", $request['volume2']);
		$harga_satuan = str_replace(".", "", $request['harga_satuan']);

		$anggaranBaru = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));

		$update = Trx_rincian_penebalan::find($request['id']); 
		// dd($request->noper);
		// if($request->uuid_proposal_pengajuan != 'dd580c39-72bc-4875-af3e-9469016fcf92' && $request->noper >9){

		// 	if($request->uuid_proposal_pengajuan !='14437877-0aec-4c70-8451-a8ee2c70105a'){
		// 		if( $anggaranBaru > $update->anggaran_rekomendasi){
		// 			$request->session()->flash('status','error');
		// 			$request->session()->flash('msg','Anggaran tidak boleh lebih besar dari nilai rekomendasi.');

		// 			return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
		// 		}
		// 	}
		// }

		if($request->noper >9 && $update->anggaran_rekomendasi != null){
 	
				if( $anggaranBaru > $update->anggaran_rekomendasi){
					$request->session()->flash('status','error');
					$request->session()->flash('msg','Anggaran tidak boleh lebih besar dari nilai rekomendasi.');

					return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
				} 
		}

		
		
		$update->rincian = $request['rincian'];
		$update->volume1 = doubleval(str_replace(",",".",$volume1));
		$update->satuan1 = $request['satuan1'];
		$update->volume2 = doubleval(str_replace(",",".",$volume2));
		$update->satuan2 = $request['satuan2'];
		$update->harga_satuan = doubleval(str_replace(",",".",$harga_satuan));
		$update->rincian_anggaran = ( doubleval(str_replace(",",".",$volume1))*doubleval(str_replace(",",".",$volume2)) )*doubleval(str_replace(",",".",$harga_satuan));
		$update->save();

		//total rincian anggaran by rencanaID
		$sum = Trx_rincian_penebalan::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran');
		
		$update = Trx_rencana_penebalan::find($request['rencanaID']);
		$update->rencana_anggaran = $sum;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been sent successfully.');

		return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
	}

	public function updateRincianAllDefinitif(Request $request){ 
	 
		$request_arr = (array) $request->all(); 

		foreach ($request_arr as $key => $value) { 
			$arr = explode(",",$key);  

			if($arr[0] == "rincian_anggaran_digunakan"){  
				$value = str_replace("_","",$value); 
				$value = str_replace("__","",$value); 
				$value = str_replace("___","",$value); 
			// 	//update rincian 
				$update = Trx_rincian_penebalan::find($arr[1]); 
				$update->rincian_anggaran_digunakan 	=  doubleval(str_replace(".", "",$value));
				$update->save();  
			// 	//update nominal  
			} 
		} 
		//update total per rencana
		$rencana = Trx_rencana_penebalan::where("uuid_proposal_pengajuan",$request->uuid)->get();
		$totalRek = 0; 
		foreach ($rencana as $row) {
			$total = DB::select("SELECT 
							SUM(rincian_anggaran_digunakan) AS total
						FROM 
							trx_rincian_penebalan
						WHERE
							rencanaID='".$row->id."'"); 
			//update rencana
			$updateRencana = Trx_rencana_penebalan::find($row->id); 
			$updateRencana->anggaran_digunakan = $total[0]->total;
			$updateRencana->save();

			$totalRek = $totalRek + $total[0]->total;
			// echo $total[0]->total."<br>";
		} 


		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been update successfully.');

		return redirect()->back();
	}

	public function deleteRincian(Request $request){
		$update = Trx_rincian_penebalan::find($request['id']);
		$update->delete();

		//total rincian anggaran by rencanaID
		$sum = Trx_rincian_penebalan::where('rencanaID',$request['rencanaID'])->sum('rincian_anggaran');

		$update = Trx_rencana_penebalan::find($request['rencanaID']);
		$update->rencana_anggaran = $sum;
		$update->save();

		$request->session()->flash('status','warning');
		$request->session()->flash('msg','Your data has been delete successfully.');

		return redirect(route('member.proposal.penebalan.edit',['fase'=>$request['fase'],'noper'=>$request['noper'],'uuid'=>$request['uuid_proposal_pengajuan'],'tahap'=>$request['tahap'],'status'=>$request['status']]));
	}

	
		public function updateRincianAllPenebalan(Request $request){ 
	 
		$request_arr = (array) $request->all(); 

		foreach ($request_arr as $key => $value) { 
			$arr = explode(",",$key);  

			if($arr[0] == "anggaran_rekomendasi"){  
				$value = str_replace("_","",$value); 
				$value = str_replace("__","",$value); 
				$value = str_replace("___","",$value); 
			// 	//update rincian 
				$update = Trx_rincian_penebalan::find($arr[1]); 
				$update->anggaran_rekomendasi 	=  doubleval(str_replace(".", "",$value));
				$update->save();  
			// 	//update nominal  
			} 
		} 
		//update total per rencana
		$rencana = Trx_rencana_penebalan::where("uuid_proposal_pengajuan",$request->uuid)->get();
		$totalRek = 0; 
		foreach ($rencana as $row) {
			$total = DB::select("SELECT 
							SUM(anggaran_rekomendasi) AS total
						FROM 
							trx_rincian_penebalan
						WHERE
							rencanaID='".$row->id."'"); 
			//update rencana
			$updateRencana = Trx_rencana_penebalan::find($row->id); 
			$updateRencana->rencana_anggaran_rekomendasi = $total[0]->total;
			$updateRencana->save();

			$totalRek = $totalRek + $total[0]->total;
			// echo $total[0]->total."<br>";
		} 


		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been update successfully.');

		return redirect()->back();
	}
	
	//Perjanjian NPHD
	public function showNaskahNPHD($uuid,$fase,$tahap){
		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);
		//dd($dataCNPHD);
		// get array proposal
		$getProposal = Proposal::where('uuid',$uuid)->first();

		if ($tahap=='tapd') {
			// return view('admin.member.proposal.penebalan.show-naskah-nphd')
			// ->with('no_prop',$getProposal->no_prop)
			// ->with('fase',$fase)
			// ->with('tahap',$tahap)
			// ->with('uuid',$uuid)
			// ->with('dataCNPHD',$dataCNPHD)
			// ->with('recProposalByID',ProposalPenebalanTAPDBuilder::getByUUID($uuid));
			return 'not available';
		}
		elseif ($tahap=='banggar') {
			// return view('admin.member.proposal.penebalan.show-naskah-nphd')
			// ->with('no_prop',$getProposal->no_prop)
			// ->with('fase',$fase)
			// ->with('tahap',$tahap)
			// ->with('uuid',$uuid)
			// ->with('dataCNPHD',$dataCNPHD)
			// ->with('recProposalByID',ProposalPenebalanBANGGARBuilder::getByUUID($uuid));
			return 'not available';
		}
		elseif ($tahap=='ddn') {
			// return view('admin.member.proposal.penebalan.show-naskah-nphd')
			// ->with('no_prop',$getProposal->no_prop)
			// ->with('fase',$fase)
			// ->with('tahap',$tahap)
			// ->with('uuid',$uuid)
			// ->with('dataCNPHD',$dataCNPHD)
			// ->with('recProposalByID',ProposalPenebalanDDNBuilder::getByUUID($uuid));
			return 'not available';
		}
		elseif ($tahap=='apbd') {
			return view('admin.member.proposal.penebalan.show-naskah-nphd')
			->with('no_prop',$getProposal->no_prop)
			->with('fase',$fase)
			->with('tahap',$tahap)
			->with('uuid',$uuid)
			->with('dataCNPHD',$dataCNPHD)
			->with('recProposalByID',ProposalDefinitifAPBDBuilder::getByUUID($uuid))
			->with('recRencanaByNoProp',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)
				->where('tahap',$tahap)
				->where('rencana_anggaran','>',0)
				->get());
		}
	}

	public function updateNaskahNPHD(Request $request){
 	
		$update = Cetak_nphd_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->hari1 = $request->hari1;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nama1 = $request->nama1;
		$update->nama2 = $request->nama2;
		$update->ketPLT = $request->ketPLT;
		$update->jabatan1 = $request->jabatan1;
		$update->alamat = $request->alamat;
		$update->deskripsi_jabatan = $request->deskripsi_jabatan;
		$update->pasal = $request->pasal;
		$update->nomor1 = $request->nomor1;
		$update->hadapan = $request->hadapan;
		$update->notaris = $request->notaris;
		$update->nomor_ham = $request->nomor_ham;
		$update->nomor2 = $request->nomor2;
		$update->tahun2 = $request->tahun2;
		$update->tanggal2 = $request->tanggal2;
		$update->nomor3 = $request->nomor3;
		$update->tahun3 = $request->tahun3;
		$update->peraturan4 = $request->peraturan4;
		$update->nomor4 = $request->nomor4;
		$update->tahun4 = $request->tahun4;
		$update->tentang4 = $request->tentang4;
		$update->tahun_anggaran1 = $request->tahun_anggaran1;
		$update->nomor5 = $request->nomor5;
		$update->tahun5 = $request->tahun5;
		$update->nomor6 = $request->nomor6;
		$update->tahun6 = $request->tahun6;
		$update->tentang6 = $request->tentang6;
		$update->tahun_anggaran2 = $request->tahun_anggaran2;
		$update->nomor7 = $request->nomor7;
		$update->tanggal7 = $request->tanggal7;
		$update->bulan7 = $request->bulan7;
		$update->tahun7 = $request->tahun7;
		$update->senilai_angka = $request->senilai_angka;
		$update->senilai_teks = $request->senilai_teks;
		$update->hari2 = $request->hari2;
		$update->tanggal3 = $request->tanggal3;
		$update->kegiatan1 =   base64_encode($request->kegiatan1); 
		$update->totalTerbilangKegiatan1 = $request->terbilangTotal;

		$update->kegiatan2 = $request->kegiatan2;
		$update->kegiatan3 = $request->kegiatan3;
		$update->jabatan2 = $request->jabatan2;
		$update->nama_pihak_kedua = $request->nama_pihak_kedua;
		$update->nip_pihak_kedua = $request->nip_pihak_kedua;
		$update->nama_kepala = $request->nama_kepala;
		$update->nip_kepala = $request->nip_kepala;
		$update->total1 = $request->total1;
		$update->total2 = $request->total2;
		$update->namaSKPD = $request->namaSKPD;
 
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.penebalan.naskah.perjanjian.nphd.show',['uuid'=>$request->uuid,'fase'=>$request->fase,'tahap'=>$request->tahap]));

	}

	public function generateNPHDToPDF($uuid,$fase,$tahap){

		// update is_generate cetak nphd
		$update = Cetak_nphd_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		$update->is_generate = 1;
		$update->save();


		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);

		// get array detail proposal by no_prop
		$recProposalByID = ProposalDefinitifAPBDBuilder::getByUUID($uuid);
		// dd($recProposalByID);

		$data = ['dataCNPHD' => $dataCNPHD,
				 'recProposalByID' => $recProposalByID
				];

		// return view('pdf.naskah-perjanjian-nphd',$data);
		// exit();

		$pdf = PDF::loadView('pdf.naskah-perjanjian-nphd',$data)
				->setOption('page-width', '210')
				->setOption('page-height', '330')
				->setOption('margin-bottom', '20')
                        ->setOption('margin-left', '20')
                        ->setOption('margin-right','20')
                        ->setOption('margin-top', '20')
				->setOption('footer-html', View::make('pdf.nphd'));

		return $pdf->download('Naskah-Perjanjian-NPHD.pdf');
	}
	// End Perjanjian NPHD


	public function delete(Request $request){
		Proposal::where('no_prop', $request->no_prop)->delete();
		FotoProposal::where('no_prop', $request->no_prop)->delete();
		BerkasProposal::where('no_prop', $request->no_prop)->delete();
		BerkasRab::where('no_prop', $request->no_prop)->delete();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.manage'));
	}


	//Pakta Integritas
	public function showPaktaIntegritas($uuid,$fase,$tahap){ 
		// get array cetak Pakta Integritas
		$dataCPI = CetakPaktaIntegritasPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap); 
		// get array proposal
		$getProposal = Proposal::where('uuid',$uuid)->first();

		if ($tahap=='apbd') {
			return view('admin.member.proposal.penebalan.show-pakta-integritas')
			->with('no_prop',$getProposal->no_prop)
			->with('fase',$fase)
			->with('tahap',$tahap)
			->with('uuid',$uuid)
			->with('dataCPI',$dataCPI)
			->with('recProposalByID',ProposalDefinitifAPBDBuilder::getByUUID($uuid))
			->with('recRencanaByNoProp',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)
				->where('rencana_anggaran','>',0)
				->get());
		}
		else{
			return 'not available';
		}
	}

	public function updatePaktaIntegritas(Request $request){

		$update = Cetak_pakta_integritas_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->nomor1 = $request->nomor1;
		$update->tahun1 = $request->tahun1;
		$update->nomor2 = $request->nomor2;
		$update->tahun2 = $request->tahun2;
		$update->nama_pemohon = $request->nama_pemohon;
		$update->no_pemohon = $request->no_pemohon;
		$update->jabatan_pemohon = $request->jabatan_pemohon;
		$update->nama_lembaga = $request->nama_lembaga;
		$update->alamat_lembaga = $request->alamat_lembaga;
		$update->text1 = $request->text1;
		$update->tahun_anggaran1 = $request->tahun_anggaran1;
		$update->nominal = $request->nominal;
		$update->terbilang = $request->terbilang;
		$update->kegiatan1 = base64_encode($request->kegiatan1);  
		$update->totalTerbilangKegiatan1 = trim($request->totalTerbilangKegiatan1);
		$update->text2 = $request->text2;
		$update->nm_skpd = $request->nm_skpd;
		$update->text3 = $request->text3;
		$update->text4 = $request->text4;
		$update->text5 = $request->text5;
		$update->text6 = $request->text6;
		$update->text7 = $request->text7;
		$update->jabatan_pemberi = $request->jabatan_pemberi;
		$update->nama_pemberi = $request->nama_pemberi;
		$update->no_pemberi = $request->no_pemberi;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('member.proposal.penebalan.pakta.integritas.show',['uuid'=>$request->uuid,'fase'=>$request->fase,'tahap'=>$request->tahap]));

	}

	public function generatePaktaIntegritasToPDF($uuid,$fase,$tahap){

		// update is_generate cetak nphd
		$update = Cetak_pakta_integritas_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		$update->is_generate = 1;
		$update->save();


		// get array cetak Pakta Integritas
		$dataCPI = CetakPaktaIntegritasPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);

		// get array detail proposal by no_prop
		$recProposalByID = ProposalDefinitifAPBDBuilder::getByUUID($uuid);

		$data = ['dataCPI' => $dataCPI,
				 'recProposalByID' => $recProposalByID
				];

		// return view('pdf.pakta-integritas',$data);
		// exit();

		$pdf = PDF::loadView('pdf.pakta-integritas',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Pakta-Integritas.pdf');
	}

}