<?php 
namespace BPKAD\Http\Controllers\Admin\Skpd\Proposal;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Foundation\ProposalBuilder;
use Auth;
use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_program;
use BPKAD\Http\Entities\Mst_rekening;
use BPKAD\Http\Entities\Mst_kategori_proposal;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\UserLembagaLapangan;
use BPKAD\Http\Entities\DetailPeriodeTahapan;
use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;
use BPKAD\Foundation\CetakKelengkapanAdministrasiBuilder;
use BPKAD\Http\Entities\Proposal;

use BPKAD\Http\Entities\ProposalPenebalanTAPD;
use BPKAD\Http\Entities\ProposalPenebalanBANGGAR;
use BPKAD\Http\Entities\ProposalPenebalanDDN;
use BPKAD\Http\Entities\ProposalDefinitifAPBD;

use BPKAD\Foundation\ProposalPenebalanTAPDBuilder;
use BPKAD\Foundation\ProposalPenebalanBANGGARBuilder;
use BPKAD\Foundation\ProposalPenebalanDDNBuilder;
use BPKAD\Foundation\ProposalDefinitifAPBDBuilder;


use BPKAD\Http\Entities\Trx_proposal_administrasi_penebalan;
use BPKAD\Http\Entities\Trx_proposal_lapangan_penebalan;

use BPKAD\Http\Entities\Trx_rencana_penebalan;
use BPKAD\Http\Entities\Trx_rincian_penebalan;
use BPKAD\Foundation\UserBuilder;

use BPKAD\Http\Entities\Cetak_rekomendasi_kolektif; 
use BPKAD\Http\Entities\Cetak_rekomendasi_kolektif_detail; 
use BPKAD\Http\Entities\Skpd;

use Mail;
use PDF;
use Config;
use DB;
use Uuid;
use Datatables;
use DNS2D;        			
use Crypt;
use View;

class ProposalController extends BaseHomeController
{

	public function index(){
		return view('admin.skpd.proposal.index')->with('recPeriodeAktif',Periode::where('status',1)->orderBy('noper','asc')->get());
	}

	public function manage($fase,$noper){
		
		return view('admin.skpd.proposal.manage')->with('recProposalBySkpdID',ProposalBuilder::getBySkpdIDProposalSKPDByNoper(Auth::user()->no_skpd,$noper))
														->with('fase',$fase)
														->with('noper',$noper)
														->with('recDetailPeriodeTahapanByNoper',DetailPeriodeTahapan::where('periodeID',$noper)->orderBy('tahapanID','asc')->get());
	}

	public function getData(Request $request){ 
		$user = UserBuilder::getById($request->session()->get('userID')); 
		$noper = $request->noper;
		if($request->jenisBantuan=='Hibah'){
			$kategory='1';
		}elseif($request->jenisBantuan=='Bantuan Sosial'){
			$kategory='2'; 
		}else{
			$kategory= null;
		}
		//DB::statement('SET @num:=0;');
		$model = DB::table('mst_lembaga as a')->select('a.nomor','a.nm_lembaga as namaLembaga','a.kd_lembaga','b.nm_skpd as namaSKPD','c.nama AS tipeLembaga','d.jdl_prop')
											  ->leftJoin('mst_skpd as b','a.kd_skpd','=','b.kd_skpd')
											  ->join('mst_tipe_lembaga as c','a.tipeLembagaID','=','c.id')
											  ->join('trx_proposal as d','a.nomor','=','d.no_lembaga')
											  ->where('a.kd_skpd',$user->kd_skpd)
											  ->where('d.noper',$noper)
											  ->where('d.status_prop',1)
											  ->where('d.kategoriProposalID',$kategory)
											  ->whereRaw("a.nomor not in(select distinct id_lembaga from cetak_rekomendasi_kolektif_detail where noper=".$noper.") "); 

        return Datatables::of($model)  
        ->addColumn('chk',function(){
            return '';
        })  
        ->make(true);
    }

    public function getDataRekomendasiKolektif(Request $request){ 
		 
		$model = Cetak_rekomendasi_kolektif::getData(); 
        return Datatables::of($model) 
        ->addColumn('tanggalRekomendasi',function($model){ 
            return $model->tanggal.' '.$model->bulan.' '.$model->tahun;
        })
        ->addColumn('lembaga',function($model){ 
        	$detail = DB::table("cetak_rekomendasi_kolektif_detail")->where("id_kol_head",$model->uuid)->get();
        	$lembaga = '';
        	$i=1;
        	foreach ($detail as $row) { 
            	$dataDetail = DB::table("mst_lembaga")->select("nm_lembaga")->where("nomor",$row->id_lembaga)->first();
        		$lembaga .= $i. ". ".$dataDetail->nm_lembaga."<br>";
        		$i++;
        	}
            return $lembaga; 
        })  
        ->make(true);
    }

	public function suratRekomendasiKolektif($noper){ 
		$data['column'] = "   
                    {data: 'nomor'},
                    {data: 'nomor'},
                    {data: 'tanggalRekomendasi',orderable: false, searchable: false},
                    {data: 'lembaga',orderable: false, searchable: false}"; 

        $data['columnConf'] = "
	      {
	        'targets': 1,
	        'orderable':false, 
	      },
	      {
	        'targets': 2,
	        'className': 'text-left',
	      },
	       {
	        'targets': 3,
	        'className': 'text-left',
	      } ";
	     $data['rekomendasiKolektif'] =  Cetak_rekomendasi_kolektif::getData($noper)->get(); 
	     $data['noper'] = $noper; 
	     $data['periodeDetail'] = DB::table('mst_periode')
		->select('status','noper','definitif','rek_anggaran','pengajuan_prop','rekomendasiKolektif') 
		->where('noper',$noper)
		->first();   
		return view('admin.skpd.proposal.surat-rekomendasi-kolektif',$data);
	}

	public function suratRekomendasiKolektifCreate($noper){  
		// echo "fase closed";
		// exit;
		$data['column']     = " 
                    {data: 'chk',orderable: false, searchable: false},
                    {data: 'nomor',visible: false},
                    {data: 'namaLembaga'},
                    {data: 'jdl_prop'},
                    {data: 'kd_lembaga'},
                    {data: 'tipeLembaga'},
                    {data: 'namaSKPD'}"; 

        $data['columnConf'] = "
	      {
	        'targets': 0,
	        'orderable':false,
	        'className': 'select-checkbox', 
	      },
	      {
	        'targets': 1,
	        'className': 'text-left',
	      },
	       {
	        'targets': 2,
	        'className': 'text-left',
	      },
	      {
	        'targets': 3,
	        'className': 'text-left',
	      },
	      {
	        'targets': 4,
	        'className': 'text-left',
	      },
	      {
	        'targets': 5,
	        'className': 'text-left',
	      },
	      {
	        'targets': 6,
	        'className': 'text-left',
	      }";

        $data['tab']	='rekomendasi';
        $data['noper']	= $noper;
        $data['skpd'] 	= Skpd::where('nomor',Auth::user()->no_skpd)->first(); 
        $data['periode']= Periode::where("noper",$noper)->first();  
		return view('admin.skpd.proposal.surat-rekomendasi-kolektif-create',$data);
	}

	public function suratRekomendasiKolektifCancel($id, Request $request){
		 
		$rekomendasi 		= Cetak_rekomendasi_kolektif::find(base64_decode($id));
		$rekomendasi_detail = DB::table("cetak_rekomendasi_kolektif_detail")->where("id_kol_head",$rekomendasi->uuid)->get(); 
		//update status proposal
		foreach ($rekomendasi_detail as $key => $value) {

			$proposal = Proposal::where("no_lembaga",$value->id_lembaga)->where("noper",$value->noper)->update(
				[
					'status_prop'=> 0
					// 'status_kirim'=>0 
				]
			);  
		} 

		//delete rekomendasi kolektif & detail
		DB::select(" 
			DELETE from cetak_rekomendasi_kolektif_detail WHERE id_kol_head='".$rekomendasi->uuid."';
		");

		DB::select("
			DELETE from cetak_rekomendasi_kolektif WHERE id='".$rekomendasi->id."'; 
		");

		$periode = Periode::where("noper",$rekomendasi->noper)->first(); 
		$request->session()->flash('status','success');
		$request->session()->flash('msg','Rekomendasi kolektif periode '.$periode->keterangan.' Telah dihapus.');
		return redirect(route('skpd.proposal.surat.rekomendasi.kolektif',$rekomendasi->noper)); 

	}

	public function suratRekomendasiKolektifCetak($id){
		 
		$rekomendasi = Cetak_rekomendasi_kolektif::find(base64_decode($id));
		$data['dataCetakkomendasiPencairan'] = $rekomendasi; 

		$data['skpd'] 	= Skpd::where('nomor',Auth::user()->no_skpd)->first();  
        $data['periode']= Periode::where("noper",$rekomendasi->noper)->first(); 
        $lembaga = '';
        $detail = DB::table("cetak_rekomendasi_kolektif_detail")->where("id_kol_head",$rekomendasi->uuid)->get(); 
       
        // $textQrCode = ''.url('qrcode').'/'.Crypt::encrypt($rekomendasi->uuid).'';
        $textQrCode = ''.url('qrcode').'/'.$rekomendasi->uuid.'';
 
        $footer = 'Rekomendasi Pengusulan '.$rekomendasi->jenisBantuan.' Dalam Bentuk Uang pada '.$data['periode']->keterangan.'';
        $data['qrcode'] = DNS2D::getBarcodeHTML($textQrCode, "QRCODE",3,3); 

        $footerHTML = '<!DOCTYPE html>
						<html>
						<head>
						</head>
						<body style="font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
						<table border="0" cellpadding="0" cellspacing="0"   width="100%">
							<tr style="font-size:10px;text-align:center">
								<center>'. $footer .'</center>
							</tr>
						</table>
						</body>
						</html>';

		// return view('pdf.rekomendasi-kolektif',$data); 
		$pdf = PDF::loadView('pdf.rekomendasi-kolektif',$data)
				->setOption('page-width', '210')
				->setOption('page-height', '330')
				->setOption('margin-bottom', '20')
		        ->setOption('margin-left', '20')
		        ->setOption('margin-right','20')
		        ->setOption('margin-top', '20') 
				->setOption('footer-html',$footerHTML);

		return $pdf->download('Rekomendasi-Kolektif.pdf');	
	}

	public function setRekomendasiKolektif(Request $request){
		// echo "fase closed";
		// exit;

		//check lembaga
        $uuid 	= Uuid::generate(); 
		$store 	= new Cetak_rekomendasi_kolektif;
		$store->nomor 			= $request->nomor;
		$store->sifat 			= $request->sifat;
		$store->lampiran 		= $request->lampiran;
		$store->jenisBantuan 	= $request->jenisBantuan;
		$store->tahun 			= $request->tahun;
		$store->bulan 			= $request->bulan;
		$store->tanggal 		= $request->tanggal;
		$store->nama_kepala		= $request->nama_kepala;
		$store->nip 			= $request->nip; 
		$store->jabatan 		= $request->jabatan; 
		$store->uuid 			= $uuid; 
		$store->noper 			= $request->noper; 
		$store->plt 			= $request->plt; 
		$store->created_by 		= Auth::user()->user_id;  
		$store->save();

		$lembaga 		= '';
		$comma 			= ',';
		if($request->lembaga != null){ 
            for($i=0; $i < count($request->lembaga); $i++){   
                $store_detail 	= new Cetak_rekomendasi_kolektif_detail;  
				$store_detail->id_kol_head 		= $uuid; 
				$store_detail->id_lembaga 		= $request->lembaga[$i]['table_id']; 
				$store_detail->noper 			= $request->noper; 
				$store_detail->save(); 
            } 
        }else{ 
            return response()->json(['status'=>false,'title'=>'Error','message'=>'Lembaga belum dipilih']);
        }  
  
		return response()->json(['status'=>true,'title'=>'Success','message'=>'Rekomendasi kolektif telah disimpan','lastId'=>$store->id]); 
	}

	public function updateTable(){ 
		// 23-04-2019 ehibahbansos
		// DB::select("
		// 	CREATE TABLE `hibahdki_repo`.`cetak_rekomendasi_kolektif`(  
		// 	  `id` INT NOT NULL AUTO_INCREMENT,
		// 	  `nomor` VARCHAR(255),
		// 	  `sifat` VARCHAR(255),
		// 	  `lampiran` VARCHAR(255),
		// 	  `tanggal` VARCHAR(255),
		// 	  `bulan` VARCHAR(255),
		// 	  `tahun` VARCHAR(255),
		// 	  `nama_kepala` VARCHAR(255),
		// 	  `nip` VARCHAR(255),
		// 	  `jenisBantuan` VARCHAR(255),
		// 	  `uuid` VARCHAR(255),
		// 	  `is_generated` INT DEFAULT 0,
		// 	  `created_at` DATETIME,
		// 	  `updated_at` DATETIME,
		// 	  `created_by` INT,
		// 	  `updated_by` INT,
		// 	  PRIMARY KEY (`id`)
		// 	);");

		// DB::select("
		// 	CREATE TABLE `hibahdki_repo`.`cetak_rekomendasi_kolektif_detail`(  
		// 	  `id` INT NOT NULL AUTO_INCREMENT,
		// 	  `id_kol_head` VARCHAR(255),
		// 	  `id_lembaga` INT,
		// 	  `created_at` DATETIME,
		// 	  `updated_at` DATETIME,
		// 	  PRIMARY KEY (`id`)
		// 	);");
 
	}


	public function show($id,$fase){
		//dd(base64_decode($id));
		
	
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID(base64_decode($id))->no_lembaga;

		// get noper
		$noper = ProposalBuilder::getByID(base64_decode($id))->noper;
		
		$periodeDetail = DB::table('mst_periode')
		->select('status','noper','definitif','rek_anggaran','pengajuan_prop') 
		->where('noper',$noper)
		->first();  

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiBuilder::getArrayByNoProp(base64_decode($id));

		// dd(UserLembaga::where('no_lembaga',$no_lembaga)->first()->is_approve);
		$isApproveAdministrasi = UserLembaga::where('no_lembaga',$no_lembaga)->first();
		$isApproveLapangan = UserLembagaLapangan::where('no_lembaga',$no_lembaga)->first();

		$recProposalByID = ProposalBuilder::getByID(base64_decode($id));

		$statusRekKolektif = Cetak_rekomendasi_kolektif_detail::where('id_lembaga',$recProposalByID->no_lembaga)
		->where('noper',$recProposalByID->noper)->count();
		 
 // dd($recProposalByID);
		return view('admin.skpd.proposal.show')->with('proposalID',base64_decode($id))
		->with('periodeDetail',$periodeDetail)
		->with('statusRekKolektif',$statusRekKolektif)
		->with('recProposalByID',$recProposalByID)
		->with('recAllProgram',Mst_program::where('tahun',$recProposalByID->tahun_anggaran)->get())
		->with('recAllRekening',Mst_rekening::all())
		->with('recAllKategoriProposal',Mst_kategori_proposal::all())
		->with('recProposalAdministrasiByNoProp',Trx_proposal_administrasi::where('no_prop',base64_decode($id))->first())
		->with('recProposalLapanganByNoProp',Trx_proposal_lapangan::where('no_prop',base64_decode($id))->first())
		->with('rencana_anggaran',Trx_rencana::where('no_prop',base64_decode($id))->sum('rencana_anggaran'))
		->with('recRencanaByNoProp',Trx_rencana::where('no_prop',base64_decode($id))->get())
		->with('fase',$fase)
		->with('noper',$recProposalByID->noper)
		->with('uuid',$recProposalByID->uuid)
		->with('no_prop',$id)
		->with('statusAdministrasiLembaga',$isApproveAdministrasi)
		->with('statusLapanganLembaga',$isApproveLapangan)
		->with('dataCka',$dataCka)
		->with('recDetailPeriodeTahapanByNoper',DetailPeriodeTahapan::where('periodeID',$noper)->orderBy('tahapanID','asc')->get())

		// tapd
		->with('recProposalPenebalanTAPDByUUID',ProposalPenebalanTAPDBuilder::getByUUIDandTerkirim($recProposalByID->uuid))
		->with('recRencanaByUUIDTAPD',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->get())
		->with('rencana_anggaran_tapd',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanTAPDByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->first())
		->with('recProposalLapanganPenebalanTAPDByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','tapd')->first())

		// banggar
		->with('recProposalPenebalanBANGGARByUUID',ProposalPenebalanBANGGARBuilder::getByUUIDandTerkirim($recProposalByID->uuid))
		->with('recRencanaByUUIDBANGGAR',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->get())
		->with('rencana_anggaran_banggar',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanBANGGARByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->first())
		->with('recProposalLapanganPenebalanBANGGARByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','banggar')->first())

		// ddn
		->with('recProposalPenebalanDDNByUUID',ProposalPenebalanDDNBuilder::getByUUIDandTerkirim($recProposalByID->uuid))
		->with('recRencanaByUUIDDDN',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->get())
		->with('rencana_anggaran_ddn',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanDDNByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->first())
		->with('recProposalLapanganPenebalanDDNByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','ddn')->first())

		// APBD
		->with('recProposalDefinitifAPBDByUUID',ProposalDefinitifAPBDBuilder::getByUUIDandTerkirim($recProposalByID->uuid))
		->with('recRencanaByUUIDAPBD',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->get())
		->with('rencana_anggaran_apbd',Trx_rencana_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->sum('rencana_anggaran'))
		->with('recProposalAdministrasiPenebalanAPBDByUUIDAndTahap',Trx_proposal_administrasi_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->first())
		->with('recProposalLapanganPenebalanAPBDByUUIDAndTahap',Trx_proposal_lapangan_penebalan::where('uuid_proposal_pengajuan',$recProposalByID->uuid)->where('tahap','apbd')->first())
		;
												
	}

	// public function onlyShow($id,$fase){
	// 	return view('admin.skpd.proposal.only-show')->with('proposalID',base64_decode($id))
	// 											->with('recProposalByID',ProposalBuilder::getByID(base64_decode($id)))
	// 											->with('recAllProgram',Mst_program::all())
	// 											->with('recAllRekening',Mst_rekening::all())
	// 											->with('recAllKategoriProposal',Mst_kategori_proposal::all())
	// 											->with('recProposalAdministrasiByNoProp',Trx_proposal_administrasi::where('no_prop',base64_decode($id))->first())
	// 											->with('recProposalLapanganByNoProp',Trx_proposal_lapangan::where('no_prop',base64_decode($id))->first())
	// 											->with('rencana_anggaran',Trx_rencana::where('no_prop',base64_decode($id))->sum('rencana_anggaran'))
	// 											->with('recRencanaByNoProp',Trx_rencana::where('no_prop',base64_decode($id))->get())
	// 											->with('fase',$fase);
	// }

	public function update(Request $request){ 
	 
		if($request->status_prop ==1){
			if($request->nominal_rekomendasi == "" || $request->nominal_rekomendasi <= 0){
				$request->session()->flash('status','error');
				$request->session()->flash('msg','Nilai rekomendasi tidak boleh kosong, silahkan simpan RAB rekomendasi terlebih dahulu.');
			
				return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
			}else{ 
				$this->proposalRepository->update_status($request->all());
				
				$request->session()->flash('status','success');
				$request->session()->flash('msg','Your data has been save successfully.');
			
				return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
			}
			
		}else{
			$this->proposalRepository->update_status($request->all());
				
				$request->session()->flash('status','success');
				$request->session()->flash('msg','Your data has been save successfully.');
			
				return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
		}
		

	}

	public function updateTAPD(Request $request){		
		// convert string to double
		$nominal_rekomendasi = str_replace(".", "", $request['nominal_rekomendasi']);

		// status dikoreksi
		if ($request['status_prop'] == 3){
			ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_dikembalikan' => $request['alasan_dikembalikan'],
					  'status_kirim' => 0
					 ]);

     //      	$data = ['no_prop'=> base64_encode($request['no_prop']),
     //      			 'jdl_prop' => $request['jdl_prop'],
     //      			 'email_lembaga' => $request['email_lembaga'],
     //      			 'nm_lembaga' => $request['nm_lembaga'],
     //      			 'no_lembaga' => $request['no_lembaga'],
     //      			 'alasan_dikembalikan' => $request['alasan_dikembalikan'],
     //      			 'base64'=>'data:image/png;base64'
					// ];

		    // Mail::send('emails.kirim-alasan-dikoreksi', ['data' => $data], function ($m) use ($data) {
			   //      $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		    //         $m->to($data['email_lembaga'], $data['nm_lembaga'])->subject('Proposal Dikoreksi');
		    //     });

      //     	return true;
		}
		// status direkomendasi
		elseif($request['status_prop'] == 1){
			ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
					  'no_survei_lap' => $request['no_survei_lap'],
					  'tgl_survei_lap' => $request['tgl_survei_lap'],
					  'no_kelengkapan_administrasi' => $request['no_kelengkapan_administrasi'],
					  'tgl_kelengkapan_administrasi' => $request['tgl_kelengkapan_administrasi'],
					  'no_rekomendasi' => $request['no_rekomendasi'],
					  'tgl_rekomendasi' => $request['tgl_rekomendasi'],
					  'no_surat_skpd' => $request['no_surat_skpd'],
					  'tgl_surat_skpd' => $request['tgl_surat_skpd'],
					  'no_prg' => $request['no_prg'],
				  'no_rek' => $request['no_rek'],
				  'kategoriProposalID' => $request['kategoriProposalID'],
				  'status_kirim' => 1
					 ]);

          	// $data = ['no_prop'=> base64_encode($request['no_prop']),
          	// 		 'jdl_prop' => $request['jdl_prop'],
          	// 		 'email_lembaga' => $request['email_lembaga'],
          	// 		 'nm_lembaga' => $request['nm_lembaga'],
          	// 		 'no_lembaga' => $request['no_lembaga'],
          	// 		 'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
          	// 		 'no_rekomendasi' => $request['no_rekomendasi'],
          	// 		 'tgl_rekomendasi' => $request['tgl_rekomendasi'],
          	// 		 'base64'=>'data:image/png;base64'		
          	// 		];

		    // Mail::send('emails.kirim-alasan-direkomendasi', ['data' => $data], function ($m) use ($data) {
			   //      $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		    //         $m->to($data['email_lembaga'], $data['nm_lembaga'])->subject('Proposal Direkomendasi');
		    //     });

		   	// return true;
		}
		// status ditolak & menunggu
		else{
			ProposalPenebalanTAPD::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_ditolak' => $request['alasan_ditolak']
					 ]);
		
			// $data = ['no_prop'=> base64_encode($request['no_prop']),
   //        			 'jdl_prop' => $request['jdl_prop'],
   //        			 'email_lembaga' => $request['email_lembaga'],
   //        			 'nm_lembaga' => $request['nm_lembaga'],
   //        			 'no_lembaga' => $request['no_lembaga'],
   //        			 'alasan_ditolak' => $request['alasan_ditolak'],
   //        			 'base64'=>'data:image/png;base64'
			// 		];

		 //    Mail::send('emails.kirim-alasan-ditolak', ['data' => $data], function ($m) use ($data) {
			//         $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		 //            $m->to($data['email_lembaga'], $data['nm_lembaga'])->subject('Proposal Ditolak');
		 //        });

			// return true;
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
	
		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
	}

	public function updateBANGGAR(Request $request){		
		// convert string to double
		$nominal_rekomendasi = str_replace(".", "", $request['nominal_rekomendasi']);

		// status dikoreksi
		if ($request['status_prop'] == 3){
			ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_dikembalikan' => $request['alasan_dikembalikan'],
					  'status_kirim' => 0
					 ]);
		}
		// status direkomendasi
		elseif($request['status_prop'] == 1){
			ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
					  'no_survei_lap' => $request['no_survei_lap'],
					  'tgl_survei_lap' => $request['tgl_survei_lap'],
					  'no_kelengkapan_administrasi' => $request['no_kelengkapan_administrasi'],
					  'tgl_kelengkapan_administrasi' => $request['tgl_kelengkapan_administrasi'],
					  'no_rekomendasi' => $request['no_rekomendasi'],
					  'tgl_rekomendasi' => $request['tgl_rekomendasi'],
					  'no_surat_skpd' => $request['no_surat_skpd'],
					  'tgl_surat_skpd' => $request['tgl_surat_skpd'],
					  'no_prg' => $request['no_prg'],
				  'no_rek' => $request['no_rek'],
				  'kategoriProposalID' => $request['kategoriProposalID'],
				  'status_kirim' => 1
					 ]);
		}
		// status ditolak & menunggu
		else{
			ProposalPenebalanBANGGAR::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_ditolak' => $request['alasan_ditolak']
					 ]);
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
	
		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
	}

	public function updateDDN(Request $request){		
		// convert string to double
		$nominal_rekomendasi = str_replace(".", "", $request['nominal_rekomendasi']);

		// status dikoreksi
		if ($request['status_prop'] == 3){
			ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_dikembalikan' => $request['alasan_dikembalikan'],
					  'status_kirim' => 0
					 ]);
		}
		// status direkomendasi
		elseif($request['status_prop'] == 1){
			ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
					  'no_survei_lap' => $request['no_survei_lap'],
					  'tgl_survei_lap' => $request['tgl_survei_lap'],
					  'no_kelengkapan_administrasi' => $request['no_kelengkapan_administrasi'],
					  'tgl_kelengkapan_administrasi' => $request['tgl_kelengkapan_administrasi'],
					  'no_rekomendasi' => $request['no_rekomendasi'],
					  'tgl_rekomendasi' => $request['tgl_rekomendasi'],
					  'no_surat_skpd' => $request['no_surat_skpd'],
					  'tgl_surat_skpd' => $request['tgl_surat_skpd'],
					  'no_prg' => $request['no_prg'],
				  'no_rek' => $request['no_rek'],
				  'kategoriProposalID' => $request['kategoriProposalID'],
				  'status_kirim' => 1
					 ]);
		}
		// status ditolak & menunggu
		else{
			ProposalPenebalanDDN::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_ditolak' => $request['alasan_ditolak']
					 ]);
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
	
		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
	}

	public function updateAPBD(Request $request){		
		// convert string to double
		$nominal_rekomendasi = str_replace(".", "", $request['nominal_rekomendasi']);

		// status dikoreksi
		if ($request['status_prop'] == 3){
			ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_dikembalikan' => $request['alasan_dikembalikan'],
					  'status_kirim' => 0
					 ]);
		}
		// status direkomendasi
		elseif($request['status_prop'] == 1){
			ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'nominal_rekomendasi' => doubleval(str_replace(",",".",$nominal_rekomendasi)),
					  'no_survei_lap' => $request['no_survei_lap'],
					  'tgl_survei_lap' => $request['tgl_survei_lap'],
					  'no_kelengkapan_administrasi' => $request['no_kelengkapan_administrasi'],
					  'tgl_kelengkapan_administrasi' => $request['tgl_kelengkapan_administrasi'],
					  'no_rekomendasi' => $request['no_rekomendasi'],
					  'tgl_rekomendasi' => $request['tgl_rekomendasi'],
					  'no_surat_skpd' => $request['no_surat_skpd'],
					  'tgl_surat_skpd' => $request['tgl_surat_skpd'],
					  'no_prg' => $request['no_prg'],
				  'no_rek' => $request['no_rek'],
				  'kategoriProposalID' => $request['kategoriProposalID'],
				  'status_kirim' => 1
					 ]);
		}
		// status ditolak & menunggu
		else{
			ProposalDefinitifAPBD::where('uuid_proposal_pengajuan',$request['uuid_proposal_pengajuan'])
			->update(['status_prop' => $request['status_prop'],
					  'alasan_ditolak' => $request['alasan_ditolak']
					 ]);
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
	
		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['proposalID']),'fase'=>$request['fase']]));
	}

	public function checklistDataAdministrasi(Request $request){
	 
		$store = Trx_proposal_administrasi::firstOrCreate(['no_prop' => $request->no_prop]);
		$store->cek_aktivitas = $request->cek_aktivitas;
		$store->keterangan_cek_aktivitas = $request->keterangan_cek_aktivitas;

		$store->cek_kepengurusan = $request->cek_kepengurusan;
		$store->keterangan_cek_kepengurusan = $request->keterangan_cek_kepengurusan;

		$store->cek_rab = $request->cek_rab;
		$store->keterangan_cek_rab = $request->keterangan_cek_rab;

		$store->cek_waktu_pelaksanaan = $request->cek_waktu_pelaksanaan;
		$store->keterangan_cek_waktu_pelaksanaan = $request->keterangan_cek_waktu_pelaksanaan;
		//new
		$store->cek_identitas_alamat = $request->cek_identitas_alamat;
		$store->ket_identitas_alamat = $request->ket_identitas_alamat;

		$store->cek_latar_belakang = $request->cek_latar_belakang;
		$store->ket_latar_belakang = $request->ket_latar_belakang;

		$store->cek_maksud_tujuan = $request->cek_maksud_tujuan;
		$store->ket_maksud_tujuan = $request->ket_maksud_tujuan;

		$store->cek_jadwal_pelaksanaan = $request->cek_jadwal_pelaksanaan;
		$store->ket_jadwal_pelaksanaan = $request->ket_jadwal_pelaksanaan;

		$store->cek_rencana_rab = $request->cek_rencana_rab;
		$store->ket_rencana_rab = $request->ket_rencana_rab; 
		  

		$store->is_approve = ( $request->is_approve == 'on' )?1:0;
		$store->approve_by = Auth::user()->user_id;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['no_prop']),'fase'=>$request['fase']]));
	
	}

	public function checklistDataLapangan(Request $request){
		
		$store = Trx_proposal_lapangan::firstOrCreate(['no_prop' => $request->no_prop]);
		$store->cek_aktivitas = $request->cek_aktivitas;
		$store->keterangan_cek_aktivitas = $request->keterangan_cek_aktivitas;

		$store->cek_kepengurusan = $request->cek_kepengurusan;
		$store->keterangan_cek_kepengurusan = $request->keterangan_cek_kepengurusan;

		$store->cek_rab = $request->cek_rab;
		$store->keterangan_cek_rab = $request->keterangan_cek_rab;

		$store->cek_waktu_pelaksanaan = $request->cek_waktu_pelaksanaan;
		$store->keterangan_cek_waktu_pelaksanaan = $request->keterangan_cek_waktu_pelaksanaan;

		$store->is_approve = ( $request->is_approve == 'on' )?1:0;
		$store->approve_by = Auth::user()->user_id;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['no_prop']),'fase'=>$request['fase']]));
	
	}


	public function updateProsesAnggaran(Request $request){

		// dd($request->all());

		// convert string to double
		$nominal_tapd = str_replace(".", "", $request->nominal_tapd);
		$nominal_banggar = str_replace(".", "", $request->nominal_banggar);
		$nominal_ddn = str_replace(".", "", $request->nominal_ddn);
		$nominal_apbd = str_replace(".", "", $request->nominal_apbd);

  		Proposal::where('no_prop',$request->no_prop)
        ->update([
        	// 'nominal_tapd' => doubleval(str_replace(",",".",$nominal_tapd)),
		  	'is_penebalan_nominal_tapd' => ($request->is_penebalan_nominal_tapd=="on")? 1 : 0,
		  	// 'nominal_banggar' => doubleval(str_replace(",",".",$nominal_banggar)),
		  	'is_penebalan_nominal_banggar' => ($request->is_penebalan_nominal_banggar=="on")? 1 : 0,
		  	// 'nominal_ddn' => doubleval(str_replace(",",".",$nominal_ddn)),
		  	'is_penebalan_nominal_ddn' => ($request->is_penebalan_nominal_ddn=="on")? 1 : 0,
		  	// 'nominal_apbd' => doubleval(str_replace(",",".",$nominal_apbd)),
		  	'is_penebalan_nominal_apbd' => ($request->is_penebalan_nominal_apbd=="on")? 1 : 0
		 ]);

        if ($request->is_penebalan_nominal_tapd=="on" || $request->is_penebalan_nominal_banggar=="on" || $request->is_penebalan_nominal_ddn=="on" || $request->is_penebalan_nominal_apbd=="on") {

        	$data = ['jdl_prop' => $request['jdl_prop'],
        			 'no_lembaga' => $request['no_lembaga'],
          			 'email_lembaga' => $request['email_lembaga'],
          			 // 'email_lembaga' => 'hary.purnomo87@gmail.com',
          			 'email' => $request['email'],
          			 'real_name' => $request['real_name'],
          			 'is_penebalan_nominal_tapd' => ($request->is_penebalan_nominal_tapd=="on")? 'TAPD,' : '',
          			 'is_penebalan_nominal_banggar' => ($request->is_penebalan_nominal_banggar=="on")? 'BANGGAR,' : '',
          			 'is_penebalan_nominal_ddn' => ($request->is_penebalan_nominal_ddn=="on")? 'DDN,' : '',
          			 'is_penebalan_nominal_apbd' => ($request->is_penebalan_nominal_apbd=="on")? 'DEFINITIF' : '',
          			 'base64'=>'data:image/png;base64'		
          			];

		    Mail::send('emails.kirim-info-proposal-penebalan-kelembaga', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject('Info Input Proposal TAPD/BANGGAR/DDN/DEFINITIF');
		        });
        }

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
	
		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request->no_prop),'fase'=>$request->fase]));
	}

	public function downloadPenerimaHibah(Request $request){

		$data = ['recProposalBySkpdID' => ProposalBuilder::getBySkpdIDProposalSKPDByNoper(Auth::user()->no_skpd,$request->noper)
						];

		// Test View
		// return view('pdf.daftar-rekomendasi-penerima-hibah',$data);

		$pdf = PDF::loadView('pdf.daftar-rekomendasi-penerima-hibah',$data);
		return $pdf->download('Daftar-Rekomendasi-Penerima-Hibah.pdf');

		// switch ($request->fase) {
		// 	case 'penetapan':	
		// 		$data = ['recProposalBySkpdID' => ProposalBuilder::getBySkpdIDProposalPenetapan(Auth::user()->no_skpd)
		// 				];

		// 		// Test View
		// 		// return view('pdf.daftar-rekomendasi-penerima-hibah',$data);

		// 		$pdf = PDF::loadView('pdf.daftar-rekomendasi-penerima-hibah',$data);
		// 		return $pdf->download('Daftar-Rekomendasi-Penerima-Hibah.pdf');

		// 		break;
		// 	case 'perubahan':	
		// 		$data = ['recProposalBySkpdID' => ProposalBuilder::getBySkpdIDProposalPerubahan(Auth::user()->no_skpd)
		// 				];

		// 		// Test View
		// 		// return view('pdf.daftar-rekomendasi-penerima-hibah',$data);

		// 		$pdf = PDF::loadView('pdf.daftar-rekomendasi-penerima-hibah',$data);
		// 		return $pdf->download('Daftar-Rekomendasi-Penerima-Hibah.pdf');

		// 		break;
		// 	case 'pengusulan':
		// 		$data = ['recProposalBySkpdID' => ProposalBuilder::getBySkpdIDProposalPengusulan(Auth::user()->no_skpd)
		// 				];

		// 		// Test View
		// 		// return view('pdf.daftar-rekomendasi-penerima-hibah',$data);

		// 		$pdf = PDF::loadView('pdf.daftar-rekomendasi-penerima-hibah',$data);
		// 		return $pdf->download('Daftar-Rekomendasi-Penerima-Hibah.pdf');

		// 		break;
		// 	default:
		// 		# code...
		// 		break;
		// }
	}

	public function checklistDataAdministrasiPenebalan(Request $request){

		$store = Trx_proposal_administrasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan' => $request->uuid,'tahap' => $request->tahap]);
		$store->cek_aktivitas = $request->cek_aktivitas;
		$store->keterangan_cek_aktivitas = $request->keterangan_cek_aktivitas;

		$store->cek_kepengurusan = $request->cek_kepengurusan;
		$store->keterangan_cek_kepengurusan = $request->keterangan_cek_kepengurusan;

		$store->cek_rab = $request->cek_rab;
		$store->keterangan_cek_rab = $request->keterangan_cek_rab;

		$store->cek_waktu_pelaksanaan = $request->cek_waktu_pelaksanaan;
		$store->keterangan_cek_waktu_pelaksanaan = $request->keterangan_cek_waktu_pelaksanaan;

		$store->is_approve = ( $request->is_approve == 'on' )?1:0;
		$store->approve_by = Auth::user()->user_id;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['no_prop']),'fase'=>$request['fase']]));
	}

	public function checklistDataLapanganPenebalan(Request $request){

		$store = Trx_proposal_lapangan_penebalan::firstOrCreate(['uuid_proposal_pengajuan' => $request->uuid,'tahap' => $request->tahap]);
		$store->cek_aktivitas = $request->cek_aktivitas;
		$store->keterangan_cek_aktivitas = $request->keterangan_cek_aktivitas;

		$store->cek_kepengurusan = $request->cek_kepengurusan;
		$store->keterangan_cek_kepengurusan = $request->keterangan_cek_kepengurusan;

		$store->cek_rab = $request->cek_rab;
		$store->keterangan_cek_rab = $request->keterangan_cek_rab;

		$store->cek_waktu_pelaksanaan = $request->cek_waktu_pelaksanaan;
		$store->keterangan_cek_waktu_pelaksanaan = $request->keterangan_cek_waktu_pelaksanaan;

		$store->is_approve = ( $request->is_approve == 'on' )?1:0;
		$store->approve_by = Auth::user()->user_id;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.proposal.show',['id'=>base64_encode($request['no_prop']),'fase'=>$request['fase']]));
	}

	public function notifApproveProposal(Request $request){
 
		$proposal = ProposalBuilder::getByID($request->no_prop); 
		$periode= Periode::where("noper",$proposal->noper)->first();  
	  
        	if($request->tahapan=='Definitif'){
        		$subject = 'Info Proses Definitif untuk Pengusulan Pencairan Hibah/Bansos';
        	}else{
        		$subject = 'Info Proses Penebalan untuk Proses Pembahasan Hibah/Bansos'; 
        	}

        	$data = ['jdl_prop' 	=> $request->jdl_prop,
        			 'no_lembaga' 	=> $request->no_lembaga,
          			 'email_lembaga' => $request->email_lembaga,
          			 'email' 		=> $request->email,
          			 // 'email_lembaga'=> 'muhamad.sidik@rucika.co.id',
          			 // 'email' 		=> 'agus1302@gmail.com', 
          			 'real_name' 	=> $request->real_name,  
          			 'keterangan' 	=> $periode->keterangan, 
          			 'tahapan' 		=> $request->tahapan, 
          			 'subject' 		=> $subject 
          			];

          	try{
          		
          		//update record flag
          		if($request->tahapan=='Definitif'){
          			//cek tahapan definitip 
          			$cekProposalDefinitif =  DB::table('trx_proposal_definitif_apbd')
          			 ->where('noper',$request->noper)
          			 ->where('no_lembaga',$request->no_lembaga)
          			 ->count();

          			 if($cekProposalDefinitif < 1){ 
          				//dd($cekProposalDefinitif);
          				
          			 	$prop_cek1 = DB::table("trx_rencana_penebalan")->where('uuid_proposal_pengajuan',$proposal->uuid)->count();

				         if($prop_cek1 > 0){
				            //echo 'rencana sudah terbentuk del';
				            // DB::select("DELETE FROM trx_rencana_penebalan WHERE tahap='apbd' AND uuid_proposal_pengajuan='4003093a-eb3a-475b-8704-16c5085f0150'");
				            return response()->json(['status'=>true,'title'=>'Success','message'=>'Rencana penebalan sudah terbentuk']);  
				            exit;
				         }
				         
          				//copy rencana rab
          				$rencanaPengajuan =  DB::table('trx_rencana')
	          			->where('no_prop',$request->no_prop) 
	          			// ->where('rencana_anggaran_skpd','>',0) 
	           			->get();
	           			foreach ($rencanaPengajuan as $key => $value) {
	           				//insert into trx_rencana_penebalan
	           				$insertRencana = DB::table('trx_rencana_penebalan')->insertGetId(
							    [
							    	'uuid_proposal_pengajuan' => ''.$request->uuid.'', 
							    	'tahap' => 'apbd',
							    	'rencana'=>''.$value->rencana.'',
							    	'rencana_anggaran'=>''.$value->rencana_anggaran_skpd.'', 
							    	'rencana_anggaran_pengajuan'=>''.$value->rencana_anggaran.'', 
							    	'rencana_anggaran_rekomendasi'=>''.$value->rencana_anggaran_skpd.'', 
							   	]
							); 

							//insert detail
							$rincianPengajuan = DB::table('trx_rincian')
							->where('rencanaID',$value->id)
							 // ->where('rincian_anggaran_skpd','>',0) 
							->get();
							foreach ($rincianPengajuan as $value2) {
								//insert into detail trx_rincian_penebalan

								DB::table('trx_rincian_penebalan')->insert(
							    	[
								    	'rencanaID' => ''.$insertRencana.'',  
								    	'rincian'=>''.$value2->rincian.'',
								    	'volume1'=>''.$value2->volume1.'',
								    	'satuan1'=>''.$value2->satuan1.'', 
								    	'volume2'=>''.$value2->volume2.'', 
								    	'satuan2'=>''.$value2->satuan2.'', 
								    	'harga_satuan'=>''.$value2->harga_satuan_skpd.'', 
								    	'rincian_anggaran'=>''.$value2->rincian_anggaran_skpd.'', 
								    	'anggaran_pengajuan'=>''.$value2->rincian_anggaran.'', 
								    	'harga_satuan_pengajuan'=>''.$value2->harga_satuan.'', 
								    	'harga_satuan_rekomendasi'=>''.$value2->harga_satuan_skpd.'', 
								    	'anggaran_rekomendasi'=>''.$value2->rincian_anggaran_skpd.'', 
								    	'tahap'=>'apbd', 
							   		]
								); 
		      
							}

	           			}

	           			//copy data proposal 
          				DB::statement(" 
							INSERT INTO trx_proposal_definitif_apbd (
								noper,
								no_skpd,
								no_lembaga,
								tgl_prop,
								no_proposal,
								crb,
								crd,
								jdl_prop,
								latar_belakang,
								maksud_tujuan,
								keterangan_prop,
								nominal_pengajuan,
								uuid_proposal_pengajuan,
								status_kirim,
								latitude,
								longitude
							)  
							SELECT 
									noper,
									no_skpd,
									no_lembaga,
									tgl_prop,
									no_proposal,
									crb,
									crd,
									jdl_prop,
									latar_belakang,
									maksud_tujuan,
									keterangan_prop,
									nominal_pengajuan,
									uuid,
									0,
									latitude,
									longitude
								FROM
									trx_proposal
								WHERE 
									uuid='$request->uuid'
							 
						");
 
          			 }  

          			Proposal::where('no_prop',$request->no_prop)
			        ->update([  
					  	'is_penebalan_nominal_apbd' => 1
					 ]);
			    }elseif($request->tahapan=='TAPD'){
          			Proposal::where('no_prop',$request->no_prop)
			        ->update([ 
					  	'is_penebalan_nominal_tapd' => 1 
					 ]);
          		}elseif($request->tahapan=='BANGGAR'){
          			Proposal::where('no_prop',$request->no_prop)
			        ->update([  
					  	'is_penebalan_nominal_banggar' => 1 
					 ]);
          		}elseif($request->tahapan=='DDN'){
          			Proposal::where('no_prop',$request->no_prop)
			        ->update([   
					  	'is_penebalan_nominal_ddn' => 1 
					 ]);
          		}
          		

			   Mail::send('emails.kirim-info-proposal-penebalan-kelembaga-approval', ['data' => $data], function ($m) use ($data) {
			        $m->from(Config::get('mail.from.address'), 'eHibahbansosdki'); 
		            $m->to($data['email'], $data['real_name'])->cc($data['email_lembaga'],'')->subject($data['subject']);
		        });
		    	return response()->json(['status'=>true,'title'=>'Success','message'=>'Email telah dikirim']);  
			}
			catch(Exception $e){
			    return response()->json(['status'=>true,'title'=>'Success','message'=>$e]);  
			}
 
          
	}

}