<?php 
namespace BPKAD\Http\Controllers\Admin\Skpd\Lembaga;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Foundation\SkpdBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use BPKAD\Foundation\UserLembagaLapanganBuilder;
use BPKAD\Foundation\MstLembagaBuilder;
use BPKAD\Foundation\UserBuilder;
use BPKAD\Http\Requests\StoreLembagaRequest;
use BPKAD\Http\Entities\Lembaga;
use BPKAD\Http\Entities\mst_tipe_lembaga;
use BPKAD\Http\Entities\Kota;
use BPKAD\Http\Entities\Kecamatan;
use BPKAD\Http\Entities\Kelurahan;
use BPKAD\Http\Entities\Mst_rt;
use BPKAD\Http\Entities\Mst_rw;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\UserLembagaLapangan;
use PDF;
use Auth;
use Mail;
use Config;


class LembagaController extends BaseHomeController
{

	public function manage(Request $request){
		$user = UserBuilder::getById($request->session()->get('userID'));

		return view('admin.skpd.lembaga.manage')->with('recByKdSkpd',MstLembagaBuilder::getByKdSkpd($user->kd_skpd));
	}

	public function show($no_lembaga){
		// dd(MstLembagaBuilder::getByNomor($no_lembaga));
		return view('admin.skpd.lembaga.show')->with('recLembagaByNomor',MstLembagaBuilder::getByNomor($no_lembaga))
			->with('recUserLembagaByNoLembaga',UserLembagaBuilder::getByNoLembaga($no_lembaga))
			->with('recUserLembagaLapanganByNoLembaga',UserLembagaLapanganBuilder::getByNoLembaga($no_lembaga));
	}

	public function create(Request $request){
		return view('admin.skpd.lembaga.create')->with('recUserByID',UserBuilder::getById($request->session()->get('userID')))
												->with('recKota',Kota::orderBy('nokota','asc')->get())
												->with('recKecamatan',Kecamatan::orderBy('nokec','asc')->get())
												->with('recKelurahan',Kelurahan::orderBy('nmkel','asc')->get())
												->with('recTipeLembaga',mst_tipe_lembaga::orderBy('nama','asc')->get())
												->with('recMstRT',Mst_rt::orderBy('id','asc')->get())
												->with('recMstRW',Mst_rw::orderBy('id','asc')->get());

	}

	public function checkDocument(Request $request){
		// dd($request->all());
		$field = $request->field;
		UserLembaga::where('no_lembaga',$request->id_lembaga)->update(
				[
					"".$field."" => $request->status
				]); 
		return response()->json(['status'=>true,'title'=>'Success','message'=>'Validasi Document telah diupdate']);  
	}

	public function store(StoreLembagaRequest $request){
		date_default_timezone_set('Asia/Jakarta'); //or choose your location
		
		$checkNpwp = Lembaga::where('npwp_check',preg_replace('/\D/', '', $request->npwp))->count();

		if ($checkNpwp >= 1) {
			$request->session()->flash('status','warning');
			$request->session()->flash('msg','Data lembaga Anda tidak dapat disimpan karna NPWP yang Anda masukan sudah tersedia di-Sistem Kami.Silahkan masukan detail lembaga Anda dengan benar. Terima kasih!');

			return redirect(route('skpd.lembaga.manage'));
		}
		else{
			$store = new Lembaga;
			$store->kd_skpd = $request->kd_skpd;
			$store->tipeLembagaID = $request->tipeLembagaID;
			$store->nm_lembaga = $request->nm_lembaga;
			$store->npwp = $request->npwp;
			$store->npwp_check = preg_replace('/\D/', '', $request->npwp);
			$store->alamat = $request->alamat;
			$store->nokota = $request->nokota;
			$store->nokec = $request->nokec;
			$store->nokel = $request->nokel;
			$store->rtID = $request->rtID;
			$store->rwID = $request->rwID;
			$store->no_akta = $request->no_akta;
			$store->tgl_akta = $request->tgl_akta;
			$store->no_surat_domisili = $request->no_surat_domisili;
			$store->tgl_surat_domisili = $request->tgl_surat_domisili;
			$store->no_sertifikat = $request->no_sertifikat;
			$store->tgl_sertifikat = $request->tgl_sertifikat;
			$store->no_izin_operasional = $request->no_izin_operasional;
			$store->tgl_izin_operasional = $request->tgl_izin_operasional;
			$store->no_telepon = $request->no_telepon;
			$store->no_fax = $request->no_fax;
			$store->email = $request->email1;
			$store->nama_bank = $request->nama_bank;
			$store->no_rek = $request->no_rek;
			$store->pemilik_rek = $request->pemilik_rek;

			$store->kontak_person = $request->kontak_person;
			$store->nik_person = $request->nik_person;
			$store->email_person = $request->email_person;
			$store->alamat_person = $request->alamat_person;
			$store->no_hp_person = $request->no_hp_person;

			$store->kontak_person_sekretaris = $request->kontak_person_sekretaris;
			$store->email_person_sekretaris = $request->email_person_sekretaris;
			$store->alamat_person_sekretaris = $request->alamat_person_sekretaris;
			$store->no_hp_person_sekretaris = $request->no_hp_person_sekretaris;

			$store->kontak_person_bendahara = $request->kontak_person_bendahara;
			$store->email_person_bendahara = $request->email_person_bendahara;
			$store->alamat_person_bendahara = $request->alamat_person_bendahara;
			$store->no_hp_person_bendahara = $request->no_hp_person_bendahara;

			$store->crb = Auth::user()['user_id'];
			$store->crd = date('Y-m-d H:i:s'); 
			$store->is_verify = 1;
			$store->save();

			//store to table user lembaga
			$update = new UserLembaga;
		    $update->no_lembaga = $store->id;
		    $update->save();

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully.');

			return redirect(route('skpd.lembaga.manage'));
		}
	}

	public function edit($nomor){
		return view('admin.skpd.lembaga.edit')->with('recLembagaByNomor',MstLembagaBuilder::getByNomor(base64_decode($nomor)))
												->with('recKota',Kota::orderBy('nokota','asc')->get())
												->with('recKecamatan',Kecamatan::orderBy('nokec','asc')->get())
												->with('recKelurahan',Kelurahan::orderBy('nmkel','asc')->get())
												->with('recTipeLembaga',mst_tipe_lembaga::all())
												->with('recMstRT',Mst_rt::orderBy('id','asc')->get())
												->with('recMstRW',Mst_rw::orderBy('id','asc')->get());
	}

	public function update(Request $request){
		$check = Lembaga::where('nomor',$request->nomor)->first();

		if ($check->npwp_check == preg_replace('/\D/', '', $request->npwp)) {
			Lembaga::where('nomor',$request->nomor)->update([
													  "tipeLembagaID" => $request->tipeLembagaID,
													  "nm_lembaga" => $request->nm_lembaga,
													  "npwp" => $request->npwp,
													  "npwp_check" => preg_replace('/\D/', '', $request->npwp),
													  "alamat" => $request->alamat,
													  "nokota" => $request->nokota,
													  "nokec" => $request->nokec,
													  "nokel" => $request->nokel,
													  "rtID" => $request->rtID,
													  "rwID" => $request->rwID,
													  "no_akta" => $request->no_akta,
													  "tgl_akta" => $request->tgl_akta,
													  "no_surat_domisili" => $request->no_surat_domisili,
													  "tgl_surat_domisili" => $request->tgl_surat_domisili,
													  "no_sertifikat" => $request->no_sertifikat,
													  "tgl_sertifikat" => $request->tgl_sertifikat,
													  "no_izin_operasional" => $request->no_izin_operasional,
													  "tgl_izin_operasional" => $request->tgl_izin_operasional,
													  "no_telepon" => $request->no_telepon,
													  "no_fax" => $request->no_fax,
													  "email" => $request->email1,
													  "nama_bank" => $request->nama_bank,
													  "no_rek" => $request->no_rek,
													  "pemilik_rek" => $request->pemilik_rek,
													  "kontak_person" => $request->kontak_person,
													  "nik_person" => $request->nik_person,
													  "email_person" => $request->email_person,
													  "alamat_person" => $request->alamat_person,
													  "no_hp_person" => $request->no_hp_person,
													  "kontak_person_sekretaris" => $request->kontak_person_sekretaris,
													  "email_person_sekretaris" => $request->email_person_sekretaris,
													  "alamat_person_sekretaris" => $request->alamat_person_sekretaris,
													  "no_hp_person_sekretaris" => $request->no_hp_person_sekretaris,
													  "kontak_person_bendahara" => $request->kontak_person_bendahara,
													  "email_person_bendahara" => $request->email_person_bendahara,
													  "alamat_person_bendahara" => $request->alamat_person_bendahara,
													  "no_hp_person_bendahara" => $request->no_hp_person_bendahara,
														]);

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Thank you.');

			return redirect(route('skpd.lembaga.show',['noLembaga'=>$request->nomor]));
		}
		else{
			$checkNpwp = Lembaga::where('npwp_check',preg_replace('/\D/', '', $request->npwp))->count();
			if ($checkNpwp >= 1) {
				$request->session()->flash('status','warning');
				$request->session()->flash('msg','Data lembaga Anda tidak dapat disimpan karna NPWP yang Anda masukan sudah tersedia di-Sistem Kami.Silahkan masukan detail lembaga Anda dengan benar. Terima kasih!');
				return redirect(route('skpd.lembaga.manage'));
			}
			else{
				Lembaga::where('nomor',$request->nomor)->update([
													  "tipeLembagaID" => $request->tipeLembagaID,
													  "nm_lembaga" => $request->nm_lembaga,
													  "npwp" => $request->npwp,
													  "npwp_check" => preg_replace('/\D/', '', $request->npwp),
													  "alamat" => $request->alamat,
													  "nokel" => $request->nokel,
													  "rtID" => $request->rtID,
													  "rwID" => $request->rwID,
													  "no_akta" => $request->no_akta,
													  "tgl_akta" => $request->tgl_akta,
													  "no_surat_domisili" => $request->no_surat_domisili,
													  "tgl_surat_domisili" => $request->tgl_surat_domisili,
													  "no_sertifikat" => $request->no_sertifikat,
													  "tgl_sertifikat" => $request->tgl_sertifikat,
													  "no_izin_operasional" => $request->no_izin_operasional,
													  "tgl_izin_operasional" => $request->tgl_izin_operasional,
													  "no_telepon" => $request->no_telepon,
													  "no_fax" => $request->no_fax,
													  "email" => $request->email1,
													  "nama_bank" => $request->nama_bank,
													  "no_rek" => $request->no_rek,
													  "pemilik_rek" => $request->pemilik_rek,
													  "kontak_person" => $request->kontak_person,
													  "email_person" => $request->email_person,
													  "alamat_person" => $request->alamat_person,
													  "no_hp_person" => $request->no_hp_person
														]);

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Thank you.');

			return redirect(route('skpd.lembaga.manage'));
			}
		}
	}

	public function checklistDataAdministrasi(Request $request){

		$update = UserLembaga::find($request->id);

		$update->cek_ktp_ketua = $request->cek_ktp_ketua;
		$update->keterangan_cek_ktp_ketua = $request->keterangan_cek_ktp_ketua;

		$update->cek_alamat_lembaga = $request->cek_alamat_lembaga;
		$update->keterangan_cek_alamat_lembaga = $request->keterangan_cek_alamat_lembaga;

		$update->cek_rekening_lembaga = $request->cek_rekening_lembaga;
		$update->keterangan_cek_rekening_lembaga = $request->keterangan_cek_rekening_lembaga;

		$update->cek_bantuan_tahun_sebelum = $request->cek_bantuan_tahun_sebelum;
		$update->keterangan_cek_bantuan_tahun_sebelum = $request->keterangan_cek_bantuan_tahun_sebelum;
		
		$update->cek_npwp = $request->cek_npwp;
		$update->keterangan_cek_npwp = $request->keterangan_cek_npwp;

		$update->cek_akta = $request->cek_akta;
		$update->keterangan_cek_akta = $request->keterangan_cek_akta;

		$update->cek_skd = $request->cek_skd;
		$update->keterangan_cek_skd = $request->keterangan_cek_skd;

		$update->cek_sertifikat = $request->cek_sertifikat;
		$update->keterangan_cek_sertifikat = $request->keterangan_cek_sertifikat;

		$update->cek_izin_operasional = $request->cek_izin_operasional;
		$update->keterangan_cek_izin_operasional = $request->keterangan_cek_izin_operasional;

		$update->cek_sk_kepengurusan = $request->cek_sk_kepengurusan;
		$update->ket_sk_kepengurusan = $request->ket_sk_kepengurusan;

		$update->cek_pernyataan_materai = $request->cek_pernyataan_materai;
		$update->ket_pernyataan_materai = $request->ket_pernyataan_materai;

		$update->is_approve = ( $request->is_approve == 'on' )?1:0;
		$update->approve_by = Auth::user()['user_id'];

		$update->save();

		if ($request->is_approve != 'on') {

			$data = [

					"email" => $request->email,
					"nm_lembaga" => $request->nm_lembaga,
					"no_lembaga" => $request->no_lembaga,
					"nm_skpd" => $request->nm_skpd,

					"cek_ktp_ketua" => $request->cek_ktp_ketua,
					"keterangan_cek_ktp_ketua" => $request->keterangan_cek_ktp_ketua,

					"cek_alamat_lembaga" => $request->cek_alamat_lembaga,
					"keterangan_cek_alamat_lembaga" => $request->keterangan_cek_alamat_lembaga,

					"cek_rekening_lembaga" => $request->cek_rekening_lembaga,
					"keterangan_cek_rekening_lembaga" => $request->keterangan_cek_rekening_lembaga,

					"cek_bantuan_tahun_sebelum" => $request->cek_bantuan_tahun_sebelum,
					"keterangan_cek_bantuan_tahun_sebelum" => $request->keterangan_cek_bantuan_tahun_sebelum,

					"cek_npwp" => $request->cek_npwp,
					"keterangan_cek_npwp" => $request->keterangan_cek_npwp,

					"cek_akta" => $request->cek_akta,
					"keterangan_cek_akta" => $request->keterangan_cek_akta,

					"cek_skd" => $request->cek_skd,
					"keterangan_cek_skd" => $request->keterangan_cek_skd,

					"cek_sertifikat" => $request->cek_sertifikat,
					"keterangan_cek_sertifikat" => $request->keterangan_cek_sertifikat,

					"cek_izin_operasional" => $request->cek_izin_operasional,
					"keterangan_cek_izin_operasional" => $request->keterangan_cek_izin_operasional,

					"cek_pernyataan_materai" => $request->cek_pernyataan_materai,
					"ket_pernyataan_materai" => $request->ket_pernyataan_materai,

					"cek_sk_kepengurusan" => $request->cek_sk_kepengurusan,
					"ket_sk_kepengurusan" => $request->ket_sk_kepengurusan, 

					'base64'=>'data:image/png;base64'
					];

			if($request->email){
		    Mail::send('emails.kelengkapan-administrasi', ['data' => $data], function ($m) use ($data) {
	            	$m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
		            
		            $m->to( $data['email'], $data['nm_lembaga'] )->subject('Hasil Penelitian Kelengkapan Administrasi');
		            // $m->to('muhamad.sidik@rucika.co.id', 'muhamad sidik' )->subject('Hasil Penelitian Kelengkapan Administrasi');
		        });
			}
		}
		else{
			$data = ["email" => $request->email,
					"nm_lembaga" => $request->nm_lembaga,
					"no_lembaga" => $request->no_lembaga,
					"nm_skpd" => $request->nm_skpd,

					"cek_ktp_ketua" => $request->cek_ktp_ketua,
					"keterangan_cek_ktp_ketua" => $request->keterangan_cek_ktp_ketua,

					"cek_alamat_lembaga" => $request->cek_alamat_lembaga,
					"keterangan_cek_alamat_lembaga" => $request->keterangan_cek_alamat_lembaga,

					"cek_rekening_lembaga" => $request->cek_rekening_lembaga,
					"keterangan_cek_rekening_lembaga" => $request->keterangan_cek_rekening_lembaga,

					"cek_bantuan_tahun_sebelum" => $request->cek_bantuan_tahun_sebelum,
					"keterangan_cek_bantuan_tahun_sebelum" => $request->keterangan_cek_bantuan_tahun_sebelum,

					"cek_npwp" => $request->cek_npwp,
					"keterangan_cek_npwp" => $request->keterangan_cek_npwp,

					"cek_akta" => $request->cek_akta,
					"keterangan_cek_akta" => $request->keterangan_cek_akta,

					"cek_skd" => $request->cek_skd,
					"keterangan_cek_skd" => $request->keterangan_cek_skd,

					"cek_sertifikat" => $request->cek_sertifikat,
					"keterangan_cek_sertifikat" => $request->keterangan_cek_sertifikat,

					"cek_izin_operasional" => $request->cek_izin_operasional,
					"keterangan_cek_izin_operasional" => $request->keterangan_cek_izin_operasional,

					"cek_pernyataan_materai" => $request->cek_pernyataan_materai,
					"ket_pernyataan_materai" => $request->ket_pernyataan_materai,

					"cek_sk_kepengurusan" => $request->cek_sk_kepengurusan,
					"ket_sk_kepengurusan" => $request->ket_sk_kepengurusan,

					
					'base64'=>'data:image/png;base64'
					];

			if($request->email){
		    Mail::send('emails.lolos-kelengkapan-administrasi', ['data' => $data], function ($m) use ($data) {
	            	$m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to( $data['email'], $data['nm_lembaga'] )->subject('Hasil Penelitian Kelengkapan Administrasi');
		        });
			}
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
		
		return redirect(route('skpd.lembaga.show',['noLembaga'=>$request['no_lembaga']]));
	}

	public function checklistDataLapangan(Request $request){

		$update = UserLembagaLapangan::firstOrNew(['no_lembaga'=>$request->no_lembaga]);

		$update->no_lembaga = $request->no_lembaga;

		$update->cek_ktp_ketua = $request->cek_ktp_ketua;
		$update->keterangan_cek_ktp_ketua = $request->keterangan_cek_ktp_ketua;

		$update->cek_alamat_lembaga = $request->cek_alamat_lembaga;
		$update->keterangan_cek_alamat_lembaga = $request->keterangan_cek_alamat_lembaga;

		$update->cek_rekening_lembaga = $request->cek_rekening_lembaga;
		$update->keterangan_cek_rekening_lembaga = $request->keterangan_cek_rekening_lembaga;

		$update->cek_bantuan_tahun_sebelum = $request->cek_bantuan_tahun_sebelum;
		$update->keterangan_cek_bantuan_tahun_sebelum = $request->keterangan_cek_bantuan_tahun_sebelum;
		
		$update->cek_npwp = $request->cek_npwp;
		$update->keterangan_cek_npwp = $request->keterangan_cek_npwp;

		$update->cek_akta = $request->cek_akta;
		$update->keterangan_cek_akta = $request->keterangan_cek_akta;

		$update->cek_skd = $request->cek_skd;
		$update->keterangan_cek_skd = $request->keterangan_cek_skd;

		$update->cek_sertifikat = $request->cek_sertifikat;
		$update->keterangan_cek_sertifikat = $request->keterangan_cek_sertifikat;

		$update->cek_izin_operasional = $request->cek_izin_operasional;
		$update->keterangan_cek_izin_operasional = $request->keterangan_cek_izin_operasional;

		$update->is_approve = ( $request->is_approve == 'on' )?1:0;
		$update->approve_by = Auth::user()['user_id'];

		$update->save();

		if ($request->is_approve != 'on') {

			$data = ["email" => $request->email,
					"nm_lembaga" => $request->nm_lembaga,
					"no_lembaga" => $request->no_lembaga,
					"nm_skpd" => $request->nm_skpd,

					"cek_ktp_ketua" => $request->cek_ktp_ketua,
					"keterangan_cek_ktp_ketua" => $request->keterangan_cek_ktp_ketua,

					"cek_alamat_lembaga" => $request->cek_alamat_lembaga,
					"keterangan_cek_alamat_lembaga" => $request->keterangan_cek_alamat_lembaga,

					"cek_rekening_lembaga" => $request->cek_rekening_lembaga,
					"keterangan_cek_rekening_lembaga" => $request->keterangan_cek_rekening_lembaga,

					"cek_bantuan_tahun_sebelum" => $request->cek_bantuan_tahun_sebelum,
					"keterangan_cek_bantuan_tahun_sebelum" => $request->keterangan_cek_bantuan_tahun_sebelum,

					"cek_npwp" => $request->cek_npwp,
					"keterangan_cek_npwp" => $request->keterangan_cek_npwp,

					"cek_akta" => $request->cek_akta,
					"keterangan_cek_akta" => $request->keterangan_cek_akta,

					"cek_skd" => $request->cek_skd,
					"keterangan_cek_skd" => $request->keterangan_cek_skd,

					"cek_sertifikat" => $request->cek_sertifikat,
					"keterangan_cek_sertifikat" => $request->keterangan_cek_sertifikat,

					"cek_izin_operasional" => $request->cek_izin_operasional,
					"keterangan_cek_izin_operasional" => $request->keterangan_cek_izin_operasional,
					'base64'=>'data:image/png;base64'

					];
			if($request->email){
		    Mail::send('emails.peninjauan-lapangan', ['data' => $data], function ($m) use ($data) {
	            	$m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to( $data['email'], $data['nm_lembaga'] )->subject('Hasil Peninjauan Lapangan');
		        });
			}
		}
		else{
			$data = ["email" => $request->email,
					"nm_lembaga" => $request->nm_lembaga,
					"no_lembaga" => $request->no_lembaga,
					"nm_skpd" => $request->nm_skpd,

					"cek_ktp_ketua" => $request->cek_ktp_ketua,
					"keterangan_cek_ktp_ketua" => $request->keterangan_cek_ktp_ketua,

					"cek_alamat_lembaga" => $request->cek_alamat_lembaga,
					"keterangan_cek_alamat_lembaga" => $request->keterangan_cek_alamat_lembaga,

					"cek_rekening_lembaga" => $request->cek_rekening_lembaga,
					"keterangan_cek_rekening_lembaga" => $request->keterangan_cek_rekening_lembaga,

					"cek_bantuan_tahun_sebelum" => $request->cek_bantuan_tahun_sebelum,
					"keterangan_cek_bantuan_tahun_sebelum" => $request->keterangan_cek_bantuan_tahun_sebelum,

					"cek_npwp" => $request->cek_npwp,
					"keterangan_cek_npwp" => $request->keterangan_cek_npwp,

					"cek_akta" => $request->cek_akta,
					"keterangan_cek_akta" => $request->keterangan_cek_akta,

					"cek_skd" => $request->cek_skd,
					"keterangan_cek_skd" => $request->keterangan_cek_skd,

					"cek_sertifikat" => $request->cek_sertifikat,
					"keterangan_cek_sertifikat" => $request->keterangan_cek_sertifikat,

					"cek_izin_operasional" => $request->cek_izin_operasional,
					"keterangan_cek_izin_operasional" => $request->keterangan_cek_izin_operasional,
					'base64'=>'data:image/png;base64'

					];
			if($request->email){
		    Mail::send('emails.lolos-peninjauan-lapangan', ['data' => $data], function ($m) use ($data) {
	            	$m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

		            $m->to( $data['email'], $data['nm_lembaga'] )->subject('Hasil Peninjauan Lapangan');
		        });
			}
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
		
		return redirect(route('skpd.lembaga.show',['noLembaga'=>$request['no_lembaga']]));
	}

	public function rekapitulasiChecklist(Request $request){
		$user = UserBuilder::getById($request->session()->get('userID'));


		return view('admin.skpd.lembaga.rekapitulasi')->with('dataKelengkapanAdministrasi',UserLembagaBuilder::getByKdSkpd($user->kd_skpd));
	}

	public function downloadRekapitulasiChecklist(Request $request){
		$user = UserBuilder::getById($request->session()->get('userID'));

		// Test View
		// return view('pdf.rekapitulasi-checlist-lembaga',['dataKelengkapanAdministrasi' => UserLembagaBuilder::getByKdSkpd($user->kd_skpd)]);

		$pdf = PDF::loadView('pdf.rekapitulasi-checlist-lembaga',['dataKelengkapanAdministrasi' => UserLembagaBuilder::getByKdSkpd($user->kd_skpd)])->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('rekapitulasi-checlist-lembaga.pdf');
	}

}