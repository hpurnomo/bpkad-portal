<?php 
namespace BPKAD\Http\Controllers\Admin\Skpd\Cetak\Penebalan;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Foundation\ProposalBuilder;
use Auth;
use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_program;
use BPKAD\Http\Entities\Mst_rekening;
use BPKAD\Http\Entities\Mst_kategori_proposal;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\UserLembagaLapangan;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;
use BPKAD\Http\Entities\Cetak_kelengkapan_administrasi;
use BPKAD\Http\Entities\Cetak_detail_kelengkapan_administrasi;
use BPKAD\Http\Entities\Cetak_peninjauan_lapangan;
use BPKAD\Http\Entities\Cetak_detail_peninjauan_lapangan;
use BPKAD\Http\Entities\Cetak_rekomendasi;
use BPKAD\Http\Entities\Cetak_rekomendasi_pencairan;
use BPKAD\Http\Entities\Skpd;
use BPKAD\Foundation\CetakKelengkapanAdministrasiBuilder;
use BPKAD\Foundation\CetakPeninjauanLapanganBuilder;
use BPKAD\Foundation\CetakRekomendasiBuilder;
use BPKAD\Foundation\CetakRekomendasiPencairanBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use BPKAD\Foundation\UserLembagaLapanganBuilder;
use BPKAD\Foundation\TrxProposalAdministrasiBuilder;
use BPKAD\Foundation\TrxProposalLapanganBuilder;
use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Http\Entities\Cetak_nphd;

use BPKAD\Foundation\CetakKelengkapanAdministrasiPenebalanBuilder;
use BPKAD\Foundation\CetakPeninjauanLapanganPenebalanBuilder;
use BPKAD\Foundation\CetakRekomendasiPenebalanBuilder;
use BPKAD\Foundation\TrxProposalAdministrasiPenebalanBuilder;
use BPKAD\Foundation\TrxProposalLapanganPenebalanBuilder;
use BPKAD\Foundation\CetakNPHDPenebalanBuilder;
use BPKAD\Foundation\CetakPaktaIntegritasPenebalanBuilder;

use BPKAD\Foundation\ProposalPenebalanTAPDBuilder;
use BPKAD\Foundation\ProposalPenebalanBANGGARBuilder;
use BPKAD\Foundation\ProposalPenebalanDDNBuilder;
use BPKAD\Foundation\ProposalDefinitifAPBDBuilder;

use BPKAD\Http\Entities\Cetak_kelengkapan_administrasi_penebalan;
use BPKAD\Http\Entities\Cetak_detail_kelengkapan_administrasi_penebalan;

use BPKAD\Http\Entities\Cetak_peninjauan_lapangan_penebalan;
use BPKAD\Http\Entities\Cetak_detail_peninjauan_lapangan_penebalan;

use BPKAD\Http\Entities\Cetak_rekomendasi_penebalan;
use BPKAD\Http\Entities\Cetak_nphd_penebalan;
use BPKAD\Http\Entities\Cetak_pakta_integritas_penebalan;
use BPKAD\Http\Entities\Periode;

use Mail;
use PDF;
use View;
use Carbon\Carbon;


class CetakController extends BaseHomeController
{

	public function show($uuid,$no_prop,$fase,$tahap,$tab){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID(base64_decode($no_prop))->no_lembaga;	

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array kelengkapan administrasi lembaga
		$dataKal = UserLembagaBuilder::getArrayByNoLembaga($no_lembaga);

		// get array kelengkapan administrasi proposal
		$dataKap = TrxProposalAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);


		// get array cetak peninjuan lapangan
		$dataCpl = CetakPeninjauanLapanganPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array peninjauan lapangan lembaga
		$dataPll = UserLembagaLapanganBuilder::getArrayByNoLembaga($no_lembaga);

		// get array peninjauan lapangan proposal
		$dataPlp = TrxProposalLapanganPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array cetak rekomendasi
		$dataCrekomendasi = CetakRekomendasiPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);

		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByNoProp(base64_decode($no_prop));

		// get nominal yang telah dicairkan
		$totalNominalPencairan = Cetak_rekomendasi_pencairan::where('no_prop',base64_decode($no_prop))->sum('nominal_pencairan');

		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDPenebalanBuilder::getArrayByUUIDAndtahap($uuid,$tahap);
		
		// get array cetak Pakta Integritas
		$dataCPI = CetakPaktaIntegritasPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);

		// get array trx proposal tiap tahapan
		if ($tahap=='tapd') {
			$recProposalPenebalanByUUID = ProposalPenebalanTAPDBuilder::getByUUID($uuid);
		}
		elseif ($tahap=='banggar') {
			$recProposalPenebalanByUUID = ProposalPenebalanBANGGARBuilder::getByUUID($uuid);
		}
		elseif ($tahap=='ddn') {
			$recProposalPenebalanByUUID = ProposalPenebalanDDNBuilder::getByUUID($uuid);
		}
		elseif ($tahap=='apbd') {
			$recProposalPenebalanByUUID = ProposalDefinitifAPBDBuilder::getByUUID($uuid);
		}    

		// dd($recProposalPenebalanByUUID);
		$dataProposal = ProposalBuilder::getByID(base64_decode($no_prop));
		return view('admin.skpd.cetak.penebalan.show')
		->with('periode',Periode::where("noper",$dataProposal->noper)->first() )
		->with('skpd',Skpd::where('nomor',Auth::user()->no_skpd)->first())
		->with('proposalID',base64_decode($no_prop))
		->with('recProposalByID',$dataProposal)
		->with('uuid',$uuid)
		->with('no_prop',$no_prop)
		->with('fase',$fase)
		->with('tahap',$tahap)
		->with('tab',$tab)
		->with('dataCka',$dataCka)
		->with('dataKal',$dataKal)
		->with('dataKap',$dataKap)
		->with('dataCpl',$dataCpl)
		->with('dataPll',$dataPll)
		->with('dataPlp',$dataPlp)
		->with('totalNominalPencairan',$totalNominalPencairan)
		->with('dataCrekomendasi',$dataCrekomendasi)
		->with('dataCrekomendasiPencairan',$dataCrekomendasiPencairan)
		->with('dataDcka',Cetak_detail_kelengkapan_administrasi_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->orderBy('no_urut','asc')->get())
		->with('dataDcpl',Cetak_detail_peninjauan_lapangan_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->orderBy('no_urut','asc')->get())
		->with('recCetakRekomendasiPencairanByNoProp',Cetak_rekomendasi_pencairan::where('no_prop',base64_decode($no_prop))->get())
		->with('dataCNPHD',$dataCNPHD)
		->with('dataCPI',$dataCPI)
		->with('recProposalPenebalanByUUID',$recProposalPenebalanByUUID);
	}

	public function updateKal(Request $request)
	{				
		$update = Cetak_kelengkapan_administrasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->nomor = $request->nomor;
		$update->hari = $request->hari;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nomor2 = $request->nomor2;
		$update->tanggal2 = $request->tanggal2;
		$update->bulan2 = $request->bulan2;
		$update->tahun2 = $request->tahun2;
		$update->nama_ketua = $request->nama_ketua;
		$update->nip = $request->nip;

		// SIMPAN INPUT TEXT
        if ($request->has('nama')) {

            $resultCount = count($request->nama);

            for($i=0; $i < $resultCount; $i++){
                
               	$storeInputText = new Cetak_detail_kelengkapan_administrasi_penebalan;
                             
                $storeInputText->uuid_proposal_pengajuan = $request->uuid;
                $storeInputText->tahap = $request->tahap;
                $storeInputText->no_urut = $request->no_urut[$i];
                $storeInputText->nama = $request->nama[$i];
                $storeInputText->jabatan = $request->jabatan[$i];
                $storeInputText->Save();

            }
        }

		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function deleteDcka($id,$uuid,$no_prop,$fase,$tahap,$tab){
		$deleteDcka = Cetak_detail_kelengkapan_administrasi_penebalan::find($id);

		$deleteDcka->delete();

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab]));
	}

	public function generateKelAdministrasiToPDF(Request $request){

		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		// get array kelengkapan administrasi lembaga
		$dataKal = UserLembagaBuilder::getArrayByNoLembaga($no_lembaga);

		// get array kelengkapan administrasi proposal
		$dataKap = TrxProposalAdministrasiPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCka' => $dataCka,
				'dataKal' => $dataKal,
				'dataKap' => $dataKap,
				'dataDcka' => Cetak_detail_kelengkapan_administrasi_penebalan::where('uuid_proposal_pengajuan',$request->uuid)->where('tahap',$request->tahap)->orderBy('no_urut','asc')->get()
				];

		$update = Cetak_kelengkapan_administrasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.kelengkapan-administrasi',$data);

		$pdf = PDF::loadView('pdf.kelengkapan-administrasi-penebalan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Kelengkapan-Administrasi.pdf');
	}

	public function updatePL(Request $request)
	{
		$update = Cetak_peninjauan_lapangan_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->nomor = $request->nomor;
		$update->hari = $request->hari;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		
		// SIMPAN INPUT TEXT
        if ($request->has('nama')) {

            $resultCount = count($request->nama);

            for($i=0; $i < $resultCount; $i++){
                
               	$storeInputText = new Cetak_detail_peninjauan_lapangan_penebalan;
                             
                $storeInputText->uuid_proposal_pengajuan = $request->uuid;
                $storeInputText->tahap = $request->tahap;
                $storeInputText->no_urut = $request->no_urut[$i];
                $storeInputText->nama = $request->nama[$i];
                $storeInputText->jabatan = $request->jabatan[$i];
                $storeInputText->Save();

            }
        }

		$update->nomor2 = $request->nomor2;
		$update->tanggal2 = $request->tanggal2;
		$update->bulan2 = $request->bulan2;
		$update->tahun2 = $request->tahun2;
		$update->nama_pengusul = $request->nama_pengusul;
		$update->nama_ketua = $request->nama_ketua;
		$update->nip = $request->nip;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function deleteDcpl($id,$uuid,$no_prop,$fase,$tahap,$tab){
		$deleteDcpl = Cetak_detail_peninjauan_lapangan_penebalan::find($id);

		$deleteDcpl->delete();

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab]));
	}

	public function generatePeninjauanLapanganToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCpl = CetakPeninjauanLapanganPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		// get array peninjauan lapangan lembaga
		$dataPll = UserLembagaLapanganBuilder::getArrayByNoLembaga($no_lembaga);

		// get array peninjauan lapangan proposal
		$dataPlp = TrxProposalLapanganPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCpl' => $dataCpl,
				'dataPll' => $dataPll,
				'dataPlp' => $dataPlp,
				'dataDcpl' => Cetak_detail_peninjauan_lapangan_penebalan::where('uuid_proposal_pengajuan',$request->uuid)->where('tahap',$request->tahap)->orderBy('no_urut','asc')->get()
				];

		$update = Cetak_peninjauan_lapangan_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]);
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.peninjauan-lapangan',$data);

		$pdf = PDF::loadView('pdf.peninjauan-lapangan-penebalan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Peninjauan-Lapangan.pdf');	
	}

	public function updateRekomendasi(Request $request)
	{ 
		$update = Cetak_rekomendasi_penebalan::firstOrCreate(['uuid_proposal_pengajuan'=>$request->uuid,'tahap'=>$request->tahap]); 
		$update->nomor = $request->nomor;
		$update->sifat = $request->sifat;
		$update->lampiran = $request->lampiran;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nomor2 = $request->nomor2;
		$update->tanggal2 = $request->tanggal2;
		$update->bulan2 = $request->bulan2;
		$update->tahun2 = $request->tahun2;
		$update->hal = $request->hal;
		$update->nama_kepala = $request->nama_kepala;
		$update->nip = $request->nip;
		$update->plt = $request->plt;
		$update->jenisBantuan = $request->jenisBantuan;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function generateRekomendasiToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCrekomendasi = CetakRekomendasiPenebalanBuilder::getArrayByUUIDAndtahap($request->uuid,$request->tahap);

		// get arrau trx proposal tiap tahapan
		if ($request->tahap=='tapd') {
			$recProposalPenebalanByUUID = ProposalPenebalanTAPDBuilder::getByUUID($request->uuid);
		}
		elseif ($request->tahap=='banggar') {
			$recProposalPenebalanByUUID = ProposalPenebalanBANGGARBuilder::getByUUID($request->uuid);
		}
		elseif ($request->tahap=='ddn') {
			$recProposalPenebalanByUUID = ProposalPenebalanDDNBuilder::getByUUID($request->uuid);
		}
		elseif ($request->tahap=='apbd') {
			$recProposalPenebalanByUUID = ProposalDefinitifAPBDBuilder::getByUUID($request->uuid);
		}else{
			$recProposalPenebalanByUUID = ProposalBuilder::getByID($request->no_prop);
		}

		$data = ['recProposalByID' => $recProposalPenebalanByUUID,
				'dataCrekomendasi' => $dataCrekomendasi,
				'recProposalPenebalanByUUID'=>$recProposalPenebalanByUUID,
				'no_lembaga'=>$no_lembaga,
				'periode'=>Periode::where("noper",$recProposalPenebalanByUUID->noper)->first(),
				'skpd'=>Skpd::where('nomor',Auth::user()->no_skpd)->first(),
				'tahap'=>$request->tahap
				];

		$update = Cetak_rekomendasi_penebalan::where('uuid_proposal_pengajuan',$request->uuid)->where('tahap',$request->tahap)->first();
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.bentuk-rekomendasi-penebalan',$data);

		$pdf = PDF::loadView('pdf.bentuk-rekomendasi-penebalan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Bentuk-Rekomendasi-Penebalan.pdf');	
	}

	// TAMBAH REKOMENDASI PENCAIRAN
	public function tambahRekomendasiPencairan($no_prop,$uuid,$fase,$tahap,$tab)
	{		
		// untuk menghitung total nominal yang telah dicairkan
		$akumulasiPencairan = Cetak_rekomendasi_pencairan::where('no_prop',base64_decode($no_prop))->sum('nominal_pencairan');

		$recProposalByID = ProposalBuilder::getByID(base64_decode($no_prop));

		// untuk menghitung sisa dana
		$sisaDana = ($recProposalByID->nominal_apbd - $akumulasiPencairan);

		return view('admin.skpd.cetak.penebalan.tambah-rekomendasi-pencairan')
		->with('no_prop',base64_decode($no_prop))
		->with('uuid',$uuid)
		->with('fase',$fase)
		->with('tahap',$tahap)
		->with('tab',$tab)
		->with('recSkpdByNomor',Skpd::where('nomor',Auth::user()->no_skpd)->first())
		->with('akumulasiPencairan',$akumulasiPencairan)
		->with('sisaDana',$sisaDana)
		->with('recProposalByID',$recProposalByID);
	}

	public function storeRekomendasiPencairan(Request $request)
	{
		// dd($request->all());

		// FORMAT RUPIAH
		$nominal_pencairan = str_replace(".", "", $request['nominal_pencairan']);

		$store = new Cetak_rekomendasi_pencairan;
		$store->no_prop = $request->no_prop;
		$store->uuid_prop = $request->uuid;
		$store->judul = $request->judul;
		$store->nomor = $request->nomor;
		$store->sifat = $request->sifat;
		$store->lampiran = $request->lampiran;
		$store->text1 = $request->text1;
		$store->tanggal1 = $request->tanggal1;
		$store->bulan1 = $request->bulan1;
		$store->tahun1 = $request->tahun1;
		$store->text2 = $request->text2;
		$store->nomor2 = $request->nomor2;
		$store->tanggal2 = $request->tanggal2;
		$store->bulan2 = $request->bulan2;
		$store->tahun2 = $request->tahun2;
		$store->hal = $request->hal;
		$store->text3 = $request->text3;
		$store->nomor_gubernur = $request->nomor_gubernur;
		$store->tahun3 = $request->tahun3;
		$store->tentang = $request->tentang;
		$store->text4 = $request->text4;
		$store->text5 = $request->text5;
		$store->text6 = $request->text6;
		$store->text7 = $request->text7;
		$store->text8 = $request->text8;
		$store->text9 = $request->text9;
		$store->terbilang = $request->terbilang;
		$store->no_rekening = $request->no_rekening;
		$store->atas_nama = $request->atas_nama;
		$store->nama_kepala = $request->nama_kepala;
		$store->nip = $request->nip;
		$store->plt = $request->plt;
		$store->nominal_pencairan = doubleval(str_replace(",",".",$nominal_pencairan));
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function editRekomendasiPencairan($no_prop,$uuid,$fase,$tahap,$tab,$id)
	{
		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByID($id);

		// untuk menghitung total nominal yang telah dicairkan
		$akumulasiPencairan = Cetak_rekomendasi_pencairan::where('no_prop',base64_decode($no_prop))->sum('nominal_pencairan');

		$recProposalByID = ProposalBuilder::getByID(base64_decode($no_prop));

		// untuk menghitung sisa dana
		$sisaDana = ($recProposalByID->nominal_apbd - $akumulasiPencairan);

		return view('admin.skpd.cetak.penebalan.edit-rekomendasi-pencairan')
		->with('no_prop',base64_decode($no_prop))
		->with('uuid',$uuid)
		->with('fase',$fase)
		->with('tahap',$tahap)
		->with('tab',$tab)
		
		->with('akumulasiPencairan',$akumulasiPencairan)
		->with('sisaDana',$sisaDana)

		->with('recProposalByID',$recProposalByID)
		->with('dataCrekomendasiPencairan',$dataCrekomendasiPencairan)
		->with('id',$id);
	}

	public function updateRekomendasiPencairan(Request $request)
	{
		CetakRekomendasiPencairanBuilder::update($request->all());

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$request->uuid,'no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tahap'=>$request->tahap,'tab'=>$request->tab]));
	}

	public function generateRekomendasiPencairanToPDF(Request $request){

		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByID($request->id);

		// untuk menghitung total nominal yang telah dicairkan
		$akumulasiPencairan = Cetak_rekomendasi_pencairan::where('no_prop',$request->no_prop)->sum('nominal_pencairan');

		$recProposalByID = ProposalBuilder::getByID($request->no_prop);

		// untuk menghitung sisa dana
		$sisaDana = ($recProposalByID->nominal_apbd - $akumulasiPencairan);

		$data = ['recProposalByID' => $recProposalByID,
				'akumulasiPencairan' => $akumulasiPencairan,
				'sisaDana' => $sisaDana,
				'dataCrekomendasiPencairan' => $dataCrekomendasiPencairan
				];

		// return view('pdf.bentuk-rekomendasi-pencairan',$data);

		$update = Cetak_rekomendasi_pencairan::where('id',$request->id)->first();
		$update->is_generate = 1;
		$update->save();
		//return view('pdf.bentuk-rekomendasi-pencairan',$data);
		$pdf = PDF::loadView('pdf.bentuk-rekomendasi-pencairan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Bentuk-Rekomendasi-Pencairan.pdf');	
	}

	// public function generateNPHDToPDF($id){

	// 	// get array cetak kelengkapan administrasi
	// 	$dataCNPHD = CetakNPHDBuilder::getArrayByNoProp($id);

	// 	// get array detail proposal by no_prop
	// 	$recProposalByID = ProposalBuilder::getByID($dataCNPHD['no_prop']);

	// 	$data = ['dataCNPHD' => $dataCNPHD,
	// 			 'recProposalByID' => $recProposalByID
	// 			];

	// 	$update = Cetak_nphd::where('no_prop',$id)->first();
	// 	$update->is_generate = 0;
	// 	$update->save();

	// 	// Test View
	// 	// return view('pdf.naskah-perjanjian-nphd',$data);

	// 	$pdf = PDF::loadView('pdf.naskah-perjanjian-nphd',$data);

	// 	return $pdf->download('Naskah-Perjanjian-NPHD.pdf');
	// }

	public function generateNPHDToPDF($uuid,$fase,$tahap){

		// update is_generate cetak nphd
		$update = Cetak_nphd_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		$update->is_generate = 0;
		$update->save();


		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);

		// get array detail proposal by no_prop
		$recProposalByID = ProposalDefinitifAPBDBuilder::getByUUID($uuid);

		$data = ['dataCNPHD' => $dataCNPHD,
				 'recProposalByID' => $recProposalByID
				];

				// dd($recProposalByID);

		// return view('pdf.naskah-perjanjian-nphd',$data);
		// exit();

		$pdf = PDF::loadView('pdf.naskah-perjanjian-nphd',$data)
				->setOption('page-width', '210')
		->setOption('page-height', '330')
		->setOption('margin-bottom', '20')
        ->setOption('margin-left', '20')
        ->setOption('margin-right','20')
        ->setOption('margin-top', '20')
				->setOption('footer-html', View::make('pdf.nphd'));;

		return $pdf->download('Naskah-Perjanjian-NPHD.pdf');
	}

	public function bukaFiturEdit($uuid,$no_prop,$fase,$tahap,$tab){

		$update = Cetak_nphd_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		$update->is_generate = 0;
		$update->save();

		return redirect(route('skpd.cetak.penebalan.show',['uuid'=>$uuid,'no_prop'=>$no_prop,'fase'=>$fase,'tahap'=>$tahap,'tab'=>$tab]));

	}

	public function generatePaktaIntegritasToPDF($uuid,$fase,$tahap){

		// update is_generate cetak nphd
		$update = Cetak_pakta_integritas_penebalan::where('uuid_proposal_pengajuan',$uuid)->where('tahap',$tahap)->first();
		if($update != null){ 
			$update->is_generate = 1;
			$update->save();
		}


		// get array cetak Pakta Integritas
		$dataCPI = CetakPaktaIntegritasPenebalanBuilder::getArrayByUUIDAndTahap($uuid,$tahap);
		// get array detail proposal by no_prop
		$recProposalByID = ProposalDefinitifAPBDBuilder::getByUUID($uuid);
	 

		$data = ['dataCPI' => $dataCPI,
				 'recProposalByID' => $recProposalByID
				];

		// return view('pdf.pakta-integritas',$data);
		// exit();
		$periode= Periode::where("noper",$recProposalByID->noper)->first();  
		$footer = 'Pakta Integritas :: '.$recProposalByID->jdl_prop.' :: '.$periode->keterangan.'';
 
		$pdf = PDF::loadView('pdf.pakta-integritas',$data)
				->setOption('page-width', '210')
				->setOption('footer-html',$footer)
				->setOption('page-height', '330');

		return $pdf->download('Pakta-Integritas.pdf');
	}
}