<?php 
namespace BPKAD\Http\Controllers\Admin\Skpd\Cetak;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Foundation\ProposalBuilder;
use Auth;
use Illuminate\Http\Request;
use BPKAD\Http\Entities\Mst_program;
use BPKAD\Http\Entities\Mst_rekening;
use BPKAD\Http\Entities\Mst_kategori_proposal;
use BPKAD\Http\Entities\Proposal;
use BPKAD\Http\Entities\UserLembaga;
use BPKAD\Http\Entities\UserLembagaLapangan;
use BPKAD\Http\Entities\Trx_proposal_administrasi;
use BPKAD\Http\Entities\Trx_proposal_lapangan;
use BPKAD\Http\Entities\Trx_rencana;
use BPKAD\Http\Entities\Trx_rincian;
use BPKAD\Http\Entities\Cetak_kelengkapan_administrasi;
use BPKAD\Http\Entities\Cetak_detail_kelengkapan_administrasi;
use BPKAD\Http\Entities\Cetak_peninjauan_lapangan;
use BPKAD\Http\Entities\Cetak_detail_peninjauan_lapangan;
use BPKAD\Http\Entities\Cetak_rekomendasi;
use BPKAD\Http\Entities\Cetak_rekomendasi_pencairan;
use BPKAD\Foundation\CetakKelengkapanAdministrasiBuilder;
use BPKAD\Foundation\CetakPeninjauanLapanganBuilder;
use BPKAD\Foundation\CetakRekomendasiBuilder;
use BPKAD\Foundation\CetakRekomendasiPencairanBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use BPKAD\Foundation\UserLembagaLapanganBuilder;
use BPKAD\Foundation\TrxProposalAdministrasiBuilder;
use BPKAD\Foundation\TrxProposalLapanganBuilder;
use BPKAD\Foundation\CetakNPHDBuilder;
use BPKAD\Http\Entities\Cetak_nphd;
use Mail;
use PDF;
use Carbon\Carbon;

class CetakController extends BaseHomeController
{

	public function show($no_prop,$fase,$tab){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID(base64_decode($no_prop))->no_lembaga;	

		$prop_adminisitrasi = Trx_proposal_administrasi::where("no_prop",base64_decode($no_prop))->first();
		 

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiBuilder::getArrayByNoProp(base64_decode($no_prop));

		// get array kelengkapan administrasi lembaga
		$dataKal = UserLembagaBuilder::getArrayByNoLembaga($no_lembaga);

		// get array kelengkapan administrasi proposal
		$dataKap = TrxProposalAdministrasiBuilder::getArrayByNoProp(base64_decode($no_prop));


		// get array cetak peninjuan lapangan
		$dataCpl = CetakPeninjauanLapanganBuilder::getArrayByNoProp(base64_decode($no_prop));

		// get array peninjauan lapangan lembaga
		$dataPll = UserLembagaLapanganBuilder::getArrayByNoLembaga($no_lembaga);

		// get array peninjauan lapangan proposal
		$dataPlp = TrxProposalLapanganBuilder::getArrayByNoProp(base64_decode($no_prop));

		// get array cetak rekomendasi
		$dataCrekomendasi = CetakRekomendasiBuilder::getArrayByNoProp(base64_decode($no_prop));

		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByNoProp(base64_decode($no_prop));

		// get array cetak naskah NPHD
		$dataCNPHD = CetakNPHDBuilder::getArrayByNoProp(base64_decode($no_prop));

		return view('admin.skpd.cetak.show')->with('proposalID',base64_decode($no_prop))
		->with('recProposalByID',ProposalBuilder::getByID(base64_decode($no_prop)))
		->with('fase',$fase)
		->with('tab',$tab)
		->with('dataCka',$dataCka)
		->with('dataKal',$dataKal)
		->with('dataKap',$dataKap)
		->with('dataCpl',$dataCpl)
		->with('dataPll',$dataPll)
		->with('dataPlp',$dataPlp)
		->with('dataCrekomendasi',$dataCrekomendasi)
		->with('prop_adminisitrasi',$prop_adminisitrasi)
		->with('dataCrekomendasiPencairan',$dataCrekomendasiPencairan)
		->with('dataDcka',Cetak_detail_kelengkapan_administrasi::where('no_prop',base64_decode($no_prop))->orderBy('no_urut','asc')->get())
		->with('dataDcpl',Cetak_detail_peninjauan_lapangan::where('no_prop',base64_decode($no_prop))->orderBy('no_urut','asc')->get())
		->with('recCetakRekomendasiPencairanByNoProp',Cetak_rekomendasi_pencairan::where('no_prop',base64_decode($no_prop))->get())
		->with('dataCNPHD',$dataCNPHD);
	}

	public function updateKal(Request $request)
	{
		$update = Cetak_kelengkapan_administrasi::firstOrCreate(['no_prop'=>$request->no_prop]);
		$update->nomor = $request->nomor;
		$update->hari = $request->hari;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nomor2 = $request->nomor2;
		$update->tanggal2 = $request->tanggal2;
		$update->bulan2 = $request->bulan2;
		$update->tahun2 = $request->tahun2;
		$update->nama_ketua = $request->nama_ketua;
		$update->nip = $request->nip;

		// SIMPAN INPUT TEXT
        if ($request->has('nama')) {

            $resultCount = count($request->nama);

            for($i=0; $i < $resultCount; $i++){
                
               	$storeInputText = new Cetak_detail_kelengkapan_administrasi;
                             
                $storeInputText->no_prop = $request->no_prop;
                $storeInputText->no_urut = $request->no_urut[$i];
                $storeInputText->nama = $request->nama[$i];
                $storeInputText->jabatan = $request->jabatan[$i];
                $storeInputText->Save();

            }
        }

		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tab'=>$request->tab]));
	}

	public function deleteDcka($id,$no_prop,$fase,$tab){
		$deleteDcka = Cetak_detail_kelengkapan_administrasi::find($id);

		$deleteDcka->delete();

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tab'=>$tab]));
	}

	public function generateKelAdministrasiToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		$prop_adminisitrasi = Trx_proposal_administrasi::where("no_prop",$request->no_prop)->first();

		// get array cetak kelengkapan administrasi
		$dataCka = CetakKelengkapanAdministrasiBuilder::getArrayByNoProp($request->no_prop);

		// get array kelengkapan administrasi lembaga
		$dataKal = UserLembagaBuilder::getArrayByNoLembaga($no_lembaga);

		// get array kelengkapan administrasi proposal
		$dataKap = TrxProposalAdministrasiBuilder::getArrayByNoProp($request->no_prop);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCka' => $dataCka,
				'dataKal' => $dataKal,
				'dataKap' => $dataKap,
				'prop_adminisitrasi' => $prop_adminisitrasi,
				'dataDcka' => Cetak_detail_kelengkapan_administrasi::where('no_prop',$request->no_prop)->orderBy('no_urut','asc')->get()
				];

		$update = Cetak_kelengkapan_administrasi::where('no_prop',$request->no_prop)->first();
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.kelengkapan-administrasi',$data);

		$pdf = PDF::loadView('pdf.kelengkapan-administrasi',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Kelengkapan-Administrasi.pdf');
	}

	public function updatePL(Request $request)
	{
		$update = Cetak_peninjauan_lapangan::firstOrCreate(['no_prop'=>$request->no_prop]);
		$update->nomor = $request->nomor;
		$update->hari = $request->hari;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		
		// SIMPAN INPUT TEXT
        if ($request->has('nama')) {

            $resultCount = count($request->nama);

            for($i=0; $i < $resultCount; $i++){
                
               	$storeInputText = new Cetak_detail_peninjauan_lapangan;
                             
                $storeInputText->no_prop = $request->no_prop;
                $storeInputText->no_urut = $request->no_urut[$i];
                $storeInputText->nama = $request->nama[$i];
                $storeInputText->jabatan = $request->jabatan[$i];
                $storeInputText->Save();

            }
        }

		$update->nomor2 = $request->nomor2;
		$update->tanggal2 = $request->tanggal2;
		$update->bulan2 = $request->bulan2;
		$update->tahun2 = $request->tahun2;
		$update->nama_pengusul = $request->nama_pengusul;
		$update->nama_ketua = $request->nama_ketua;
		$update->nip = $request->nip;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tab'=>$request->tab]));
	}

	public function deleteDcpl($id,$no_prop,$fase,$tab){
		$deleteDcpl = Cetak_detail_peninjauan_lapangan::find($id);

		$deleteDcpl->delete();

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($no_prop),'fase'=>$fase,'tab'=>$tab]));
	}

	public function generatePeninjauanLapanganToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		$prop_adminisitrasi = Trx_proposal_administrasi::where("no_prop",$request->no_prop)->first();

		// get array cetak peninjuan lapangan
		$dataCpl = CetakPeninjauanLapanganBuilder::getArrayByNoProp($request->no_prop);

		// get array peninjauan lapangan lembaga
		$dataKal = UserLembagaLapanganBuilder::getArrayByNoLembaga($no_lembaga);

		// get array peninjauan lapangan proposal
		$dataPlp = TrxProposalLapanganBuilder::getArrayByNoProp($request->no_prop);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCpl' => $dataCpl,
				'dataKal' => $dataKal,
				'dataPlp' => $dataPlp,
				'prop_adminisitrasi' => $prop_adminisitrasi,
				'dataDcpl' => Cetak_detail_peninjauan_lapangan::where('no_prop',$request->no_prop)->orderBy('no_urut','asc')->get()
				];

		$update = Cetak_peninjauan_lapangan::where('no_prop',$request->no_prop)->first();
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.peninjauan-lapangan',$data);

		$pdf = PDF::loadView('pdf.peninjauan-lapangan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Peninjauan-Lapangan.pdf');	
	}

	public function updateRekomendasi(Request $request)
	{
		$update = Cetak_rekomendasi::firstOrCreate(['no_prop'=>$request->no_prop]);
		$update->nomor = $request->nomor;
		$update->sifat = $request->sifat;
		$update->lampiran = $request->lampiran;
		$update->tanggal1 = $request->tanggal1;
		$update->bulan1 = $request->bulan1;
		$update->tahun1 = $request->tahun1;
		$update->nomor2 = $request->nomor2;
		$update->tanggal2 = $request->tanggal2;
		$update->bulan2 = $request->bulan2;
		$update->tahun2 = $request->tahun2;
		$update->hal = $request->hal;
		$update->nama_kepala = $request->nama_kepala;
		$update->nip = $request->nip;
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tab'=>$request->tab]));
	}

	public function generateRekomendasiToPDF(Request $request){
		
		// get no_lembaga
		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCrekomendasi = CetakRekomendasiBuilder::getArrayByNoProp($request->no_prop);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCrekomendasi' => $dataCrekomendasi
				];

				

		$update = Cetak_rekomendasi::where('no_prop',$request->no_prop)->first();
		$update->is_generate = 1;
		$update->save();

		// Test View
		// return view('pdf.bentuk-rekomendasi',$data);

		$pdf = PDF::loadView('pdf.bentuk-rekomendasi',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Bentuk-Rekomendasi.pdf');	
	}

	// TAMBAH REKOMENDASI PENCAIRAN
	public function tambahRekomendasiPencairan($no_prop,$uuid_prop,$fase,$tab)
	{
		return view('admin.skpd.cetak.tambah-rekomendasi-pencairan')->with('no_prop',base64_decode($no_prop))
																	->with('uuid_prop',$uuid_prop)
																	->with('fase',$fase)
																	->with('tab',$tab)
																	->with('recProposalByID',ProposalBuilder::getByID(base64_decode($no_prop)));
	}

	public function storeRekomendasiPencairan(Request $request)
	{
		// dd($request->all());
		// FORMAT RUPIAH
		$nominal_pencairan = str_replace(".", "", $request['nominal_pencairan']);

		$store = new Cetak_rekomendasi_pencairan;
		$store->no_prop = $request->no_prop;
		$store->uuid_prop = $request->uuid_prop;
		$store->judul = $request->judul;
		$store->nomor = $request->nomor;
		$store->sifat = $request->sifat;
		$store->lampiran = $request->lampiran;
		$store->tanggal1 = $request->tanggal1;
		$store->bulan1 = $request->bulan1;
		$store->tahun1 = $request->tahun1;
		$store->nomor2 = $request->nomor2;
		$store->tanggal2 = $request->tanggal2;
		$store->bulan2 = $request->bulan2;
		$store->tahun2 = $request->tahun2;
		$store->hal = $request->hal;
		$store->nomor_gubernur = $request->nomor_gubernur;
		$store->tentang = $request->tentang;
		$store->nama_kepala = $request->nama_kepala;
		$store->nip = $request->nip;
		$store->nominal_pencairan = doubleval(str_replace(",",".",$nominal_pencairan));
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tab'=>$request->tab]));
	}

	public function editRekomendasiPencairan($no_prop,$uuid_prop,$fase,$tab,$id)
	{
		// get array cetak rekomendasi pencairan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByID($id);

		return view('admin.skpd.cetak.edit-rekomendasi-pencairan')->with('no_prop',base64_decode($no_prop))
																	->with('uuid_prop',$uuid_prop)
																	->with('fase',$fase)
																	->with('tab',$tab)
																	->with('recProposalByID',ProposalBuilder::getByID(base64_decode($no_prop)))
																	->with('dataCrekomendasiPencairan',$dataCrekomendasiPencairan)
																	->with('id',$id);
	}

	public function updateRekomendasiPencairan(Request $request)
	{
		CetakRekomendasiPencairanBuilder::update($request->all());

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.cetak.show',['no_prop'=>base64_encode($request->no_prop),'fase'=>$request->fase,'tab'=>$request->tab]));
	}

	public function generateRekomendasiPencairanToPDF(Request $request){
		// get no_lembaga

		$no_lembaga = ProposalBuilder::getByID($request->no_prop)->no_lembaga;	

		// get array cetak peninjuan lapangan
		$dataCrekomendasiPencairan = CetakRekomendasiPencairanBuilder::getArrayByID($request->id);

		$data = ['recProposalByID' => ProposalBuilder::getByID($request->no_prop),
				'dataCrekomendasiPencairan' => $dataCrekomendasiPencairan
				];

		$update = Cetak_rekomendasi_pencairan::where('id',$request->id)->first();
		$update->is_generate = 1;
		$update->save();
		
		$pdf = PDF::loadView('pdf.bentuk-rekomendasi-pencairan',$data)->setOption('page-width', '210')->setOption('page-height', '330');

		return $pdf->download('Bentuk-Rekomendasi-Pencairan.pdf');	
	}

	public function generateNPHDToPDF($id){

		// get array cetak kelengkapan administrasi
		$dataCNPHD = CetakNPHDBuilder::getArrayByNoProp($id);

		// get array detail proposal by no_prop
		$recProposalByID = ProposalBuilder::getByID($dataCNPHD['no_prop']);

		$data = ['dataCNPHD' => $dataCNPHD,
				 'recProposalByID' => $recProposalByID
				];

		$update = Cetak_nphd::where('no_prop',$id)->first();
		$update->is_generate = 0;
		$update->save();

		// Test View
		// return view('pdf.naskah-perjanjian-nphd',$data);

		$pdf = PDF::loadView('pdf.naskah-perjanjian-nphd',$data)->setOption('page-width', '210')
		->setOption('page-height', '330')
		->setOption('margin-bottom', '20')
        ->setOption('margin-left', '20')
        ->setOption('margin-right','20')
        ->setOption('margin-top', '20');

		return $pdf->download('Naskah-Perjanjian-NPHD.pdf');
	}
}