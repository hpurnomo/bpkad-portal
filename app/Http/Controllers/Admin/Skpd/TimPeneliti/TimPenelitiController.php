<?php 
namespace BPKAD\Http\Controllers\Admin\Skpd\TimPeneliti;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Foundation\SkpdBuilder;
use Auth;
use BPKAD\Foundation\MstLembagaBuilder;
use BPKAD\Foundation\UserBuilder;
use BPKAD\Http\Entities\Kelurahan;
use BPKAD\Http\Requests\StoreLembagaRequest;
use BPKAD\Http\Entities\Lembaga;
use BPKAD\Http\Entities\mst_tipe_lembaga;
use BPKAD\Http\Entities\Mst_rt;
use BPKAD\Http\Entities\Mst_rw;

class TimPenelitiController extends BaseHomeController
{

	public function manage(Request $request){
		$user = UserBuilder::getById($request->session()->get('userID'));

		return view('admin.skpd.lembaga.manage')->with('recByKdSkpd',MstLembagaBuilder::getByKdSkpd($user->kd_skpd));
	}

	public function create(Request $request){
		return view('admin.skpd.lembaga.create')->with('recUserByID',UserBuilder::getById($request->session()->get('userID')))
												->with('recKelurahan',Kelurahan::orderBy('nmkel','asc')->get())
												->with('recTipeLembaga',mst_tipe_lembaga::orderBy('nama','asc')->get())
												->with('recMstRT',Mst_rt::orderBy('id','asc')->get())
												->with('recMstRW',Mst_rw::orderBy('id','asc')->get());

	}

	public function store(StoreLembagaRequest $request){
		date_default_timezone_set('Asia/Jakarta');//or choose your location
		
		$checkNpwp = Lembaga::where('npwp_check',preg_replace('/\D/', '', $request->npwp))->count();
		if ($checkNpwp >= 1) {
			$request->session()->flash('status','warning');
			$request->session()->flash('msg','Data lembaga Anda tidak dapat disimpan karna NPWP yang Anda masukan sudah tersedia di-Sistem Kami.Silahkan masukan detail lembaga Anda dengan benar. Terima kasih!');

			return redirect(route('skpd.lembaga.manage'));
		}
		else{
			$store = new Lembaga;
			$store->kd_skpd = $request->kd_skpd;
			$store->tipeLembagaID = $request->tipeLembagaID;
			$store->nm_lembaga = $request->nm_lembaga;
			$store->npwp = $request->npwp;
			$store->npwp_check = preg_replace('/\D/', '', $request->npwp);
			$store->alamat = $request->alamat;
			$store->nokel = $request->nokel;
			$store->rtID = $request->rtID;
			$store->rwID = $request->rwID;
			$store->no_akta = $request->no_akta;
			$store->tgl_akta = $request->tgl_akta;
			$store->no_surat_domisili = $request->no_surat_domisili;
			$store->tgl_surat_domisili = $request->tgl_surat_domisili;
			$store->no_sertifikat = $request->no_sertifikat;
			$store->tgl_sertifikat = $request->tgl_sertifikat;
			$store->no_izin_operasional = $request->no_izin_operasional;
			$store->tgl_izin_operasional = $request->tgl_izin_operasional;
			$store->no_telepon = $request->no_telepon;
			$store->no_fax = $request->no_fax;
			$store->email = $request->email1;
			$store->nama_bank = $request->nama_bank;
			$store->no_rek = $request->no_rek;
			$store->pemilik_rek = $request->pemilik_rek;
			$store->kontak_person = $request->kontak_person;
			$store->email_person = $request->email_person;
			$store->alamat_person = $request->alamat_person;
			$store->no_hp_person = $request->no_hp_person;
			$store->crb = Auth::user()['user_id'];
			$store->crd = date('Y-m-d H:i:s'); 
			$store->is_verify = 1;
			$store->save();
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');

		return redirect(route('skpd.lembaga.manage'));

	}

	public function edit($nomor){
		return view('admin.skpd.lembaga.edit')->with('recLembagaByNomor',MstLembagaBuilder::getByNomor(base64_decode($nomor)))
												->with('recKelurahan',Kelurahan::all())
												->with('recTipeLembaga',mst_tipe_lembaga::all())
												->with('recMstRT',Mst_rt::orderBy('id','asc')->get())
												->with('recMstRW',Mst_rw::orderBy('id','asc')->get());
	}

	public function update(Request $request){
		$check = Lembaga::where('nomor',$request->nomor)->first();

		if ($check->npwp_check == preg_replace('/\D/', '', $request->npwp)) {
			Lembaga::where('nomor',$request->nomor)->update([
													  "tipeLembagaID" => $request->tipeLembagaID,
													  "nm_lembaga" => $request->nm_lembaga,
													  "npwp" => $request->npwp,
													  "npwp_check" => preg_replace('/\D/', '', $request->npwp),
													  "alamat" => $request->alamat,
													  "nokel" => $request->nokel,
													  "rtID" => $request->rtID,
													  "rwID" => $request->rwID,
													  "no_akta" => $request->no_akta,
													  "tgl_akta" => $request->tgl_akta,
													  "no_surat_domisili" => $request->no_surat_domisili,
													  "tgl_surat_domisili" => $request->tgl_surat_domisili,
													  "no_sertifikat" => $request->no_sertifikat,
													  "tgl_sertifikat" => $request->tgl_sertifikat,
													  "no_izin_operasional" => $request->no_izin_operasional,
													  "tgl_izin_operasional" => $request->tgl_izin_operasional,
													  "no_telepon" => $request->no_telepon,
													  "no_fax" => $request->no_fax,
													  "email" => $request->email1,
													  "nama_bank" => $request->nama_bank,
													  "no_rek" => $request->no_rek,
													  "pemilik_rek" => $request->pemilik_rek,
													  "kontak_person" => $request->kontak_person,
													  "email_person" => $request->email_person,
													  "alamat_person" => $request->alamat_person,
													  "no_hp_person" => $request->no_hp_person
														]);

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Thank you.');

			return redirect(route('skpd.lembaga.manage'));
		}
		else{
			$checkNpwp = Lembaga::where('npwp_check',preg_replace('/\D/', '', $request->npwp))->count();
			if ($checkNpwp >= 1) {
				$request->session()->flash('status','warning');
				$request->session()->flash('msg','Data lembaga Anda tidak dapat disimpan karna NPWP yang Anda masukan sudah tersedia di-Sistem Kami.Silahkan masukan detail lembaga Anda dengan benar. Terima kasih!');
				return redirect(route('skpd.lembaga.manage'));
			}
			else{
				Lembaga::where('nomor',$request->nomor)->update([
													  "tipeLembagaID" => $request->tipeLembagaID,
													  "nm_lembaga" => $request->nm_lembaga,
													  "npwp" => $request->npwp,
													  "npwp_check" => preg_replace('/\D/', '', $request->npwp),
													  "alamat" => $request->alamat,
													  "nokel" => $request->nokel,
													  "rtID" => $request->rtID,
													  "rwID" => $request->rwID,
													  "no_akta" => $request->no_akta,
													  "tgl_akta" => $request->tgl_akta,
													  "no_surat_domisili" => $request->no_surat_domisili,
													  "tgl_surat_domisili" => $request->tgl_surat_domisili,
													  "no_sertifikat" => $request->no_sertifikat,
													  "tgl_sertifikat" => $request->tgl_sertifikat,
													  "no_izin_operasional" => $request->no_izin_operasional,
													  "tgl_izin_operasional" => $request->tgl_izin_operasional,
													  "no_telepon" => $request->no_telepon,
													  "no_fax" => $request->no_fax,
													  "email" => $request->email1,
													  "nama_bank" => $request->nama_bank,
													  "no_rek" => $request->no_rek,
													  "pemilik_rek" => $request->pemilik_rek,
													  "kontak_person" => $request->kontak_person,
													  "email_person" => $request->email_person,
													  "alamat_person" => $request->alamat_person,
													  "no_hp_person" => $request->no_hp_person
														]);

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Thank you.');

			return redirect(route('skpd.lembaga.manage'));
			}
		}
	}


}