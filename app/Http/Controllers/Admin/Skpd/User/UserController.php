<?php 
namespace BPKAD\Http\Controllers\Admin\Skpd\User;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Foundation\UserBuilder;
use BPKAD\Foundation\UserLembagaBuilder;
use Auth;
use BPKAD\User;
use BPKAD\Http\Entities\Skpd;
use BPKAD\Http\Entities\Lembaga;
use BPKAD\Http\Requests\RegUserLembagaViaSkpdRequest;
use BPKAD\Http\Entities\UserLembaga;
use Carbon\Carbon;
use DB;
use Mail;
use Config;

class UserController extends BaseHomeController
{

	public function manage(){
		return view('admin.skpd.user.manage')->with('recUserBySkpdID',UserBuilder::getBySkpdID(Auth::user()->no_skpd));
	}

	public function show($userID){
		$user = UserBuilder::getById(base64_decode($userID));

		return view('admin.skpd.user.show')->with('recUserID',base64_decode($userID))
											->with('recUserByID',UserBuilder::getById(base64_decode($userID)))
											->with('recUserLembagaByNoLembaga',UserLembagaBuilder::getByNoLembaga($user->no_lembaga));
	}

	public function create(Request $request){
		return view('admin.skpd.user.create')->with('recUserByID',UserBuilder::getById($request->session()->get('userID')))
											 ->with('recLembagaByKdSkpd',Lembaga::where('kd_skpd',UserBuilder::getById($request->session()->get('userID'))->kd_skpd)->get());
	}

	public function store(RegUserLembagaViaSkpdRequest $request){

		$store = new User;
		$store->no_registrasi = $request->no_skpd.'.'.$request->no_lembaga.'.'.Carbon::now('Asia/Jakarta')->format('dmY').'.'.Carbon::now('Asia/Jakarta')->format('his');
		$store->no_skpd = $request->no_skpd;
		$store->no_lembaga = $request->no_lembaga;
		$store->group_id = 12;
		$store->real_name = $request->real_name;
		$store->email = $request->email;
		$store->username = $request->username;
		$store->password = bcrypt($request->password);
		$store->is_verify = 1;
		$store->is_active = 1;
		$store->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.user.management.manage'));
	}

	public function edit($userID)
	{
		return view('admin.skpd.user.edit')->with('recUserByID',UserBuilder::getById(base64_decode($userID)));
	}

	public function update(Request $request)
	{
		$countUsername = Auth::user()->where('username',$request->username)->count();
		$countEmail = Auth::user()->where('email',$request->email)->count();

		$update = Auth::user()->where('user_id',$request->user_id)->first();

		if ($request->has('real_name')) {
		    $update->real_name = $request->real_name;
		}

		if ($request->has('username') && $update->username!=$request->username && $countUsername==0 ) {
		    $update->username = $request->username;
		}

		if ($request->has('email') && $update->email!=$request->email && $countEmail==0) {
		    $update->email = $request->email;
		}

		if ($request->has('password')) {
		    $update->password = bcrypt($request->password);
		}
		
		$update->save();

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.user.management.show',['id'=>base64_encode($request->user_id)]));
	}

	public function changeStatus(Request $request){
		date_default_timezone_set('Asia/Jakarta');//or choose your location

		$update = User::where('user_id',$request['userID'])->first();
		$update->is_verify = $request['is_verify'];
		$update->is_active = 1;
		$update->save();

		$update = Lembaga::where('nomor',$request->no_lembaga)->update(['is_verify'=>1,
																		'upd'=>date('Y-m-d H:i:s')]);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
		
		return redirect(route('skpd.user.management.show',['userID'=>base64_encode($request['userID'])]));
	}

	public function nonaktifStatus($id,Request $request){
		date_default_timezone_set('Asia/Jakarta');//or choose your location
		$id = base64_decode($id);
		$update = User::where('user_id',$id)->first();
		if($update->is_active==1){
			$update->is_active = 0; 
		}else{
			$update->is_active = 1;  
		}
		
		$update->save(); 

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');
		
		return redirect(route('skpd.user.management.manage'));
	}

	public function delete(Request $request){

		// User::where('user_id', $request->userID)->delete();
		// UserLembaga::where('userID',$request->userID)->delete();
		$update = DB::table('users')->where('user_id',$request->userID)
									->update(['is_verify'=>0,
												'is_active'=>0]);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.user.management.manage'));
	}


	public function profilSKPD(){
		return view('admin.skpd.user.profil-skpd')->with('recSkpdByNomor',Skpd::where('nomor',Auth::user()->no_skpd)->first());
	}

	public function updateProfilSKPD(Request $request){
		
		Skpd::where('nomor',$request['nomor'])
		->update([
				'kontak_person'=>$request['kontak_person'],
				'nip_ketua'=>$request['nip_ketua'],
				'nm_skpd'=>$request['nm_skpd'],
				'alamat'=>$request['alamat'],
				// 'email'=>$request['email'],
				]);

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully.');

		return redirect(route('skpd.profil'));
	}

	public function emailInfo(){
		// $data = DB::select("
		// 	SELECT 
		// 	a.no_lembaga,
		// 	b.email,
		// 	b.nm_lembaga
		//  FROM 
		// 	trx_proposal a JOIN mst_lembaga b ON a.no_lembaga=b.nomor
		//  WHERE 
		// 	a.noper=9
		// 	AND  b.email != '' AND b.email != '-' AND b.email != '0' AND b.email != '00' 
		// 	AND a.status_prop = 1
		// "); 
		// // $email = ['imuhamadsidik@gmail.com','agus1302@gmail.com'];
		// $email='';
		// foreach ($data as $key => $value) {
		// 	$email.= "'".$value->email."',"; 
		// }

		// dd($email);
		// exit;
		$email = ['imuhamadsidik@gmail.com','muhamad.sidik@rucika.co.id','yayasansenibudayajakarta@gmail.com','KONI@KONIDKI.OR.ID','dkiformi@gmail.com','budihayani_krendang@yahoo.co.id','akademi.jakarta@yahoo.com','npcjakarta1@gmail.com','keuangan.dkj@gmail.com','hibah.lptq.dkijakarta@gmail.com','yayasanbenyaminsuaeb@gmail.com','lembagakebudayaanbetawi78@gmail.com','budi16properti@gmail.com','muiprovdki@gmail.com','badancendanabaktijaya12@gmail.com','bimashindudkijakarta@gmail.com','gorp2tunj@gmail.com','ptkpenmad@yahoo.com','dmi_provinsidki@yahoo.com','kpapdkijakarta@yahoo.com','kpid@jakarta.go.id','piveridkijakarta@gmail.com','bazisjakarta@gmail.com','yapenprovdkijakarta@gmail.com','pramukadki@gmail.com','azimalatief@gmail.com','ahmadnaufal053@gmail.com','madalvri2@gmail.com','bps3100@bps.go.id','dwpp_dkijakarta@yahoo.co.id','lbiq.jakarta@gmail.com','bkow.jakarta1954@gmail.com','bpsk_dki@yahoo.co.id','upt.p4op@gmail.com','uptp6o.disdikdki@gmail.com','parisadadki@yahoo.co.id','wanitaveteranridkijakarta@gmail.com','yayasankorpri2017@gmail.com','wirawati.dki@gmail.com','bossddki@gmail.com','bossmadki@gmail.com','bosdisdikdki@gmail.com','info@asmi.ac.id','komindaprovdki@gmail.com ','yayasanputrafatahillah@yahoo.com','dekranasdadkijakarta@ymail.com','almakmuryayasan@gmail.com','himpaudiprovinsidkijkt@gmail.com','bossmpdki@gmail.com','pgri_dki@yahoo.co.id','yayasan_hayatul_islam@yahoo.com','pui.dkijkt@gmail.com','pepabri.dki@gmail.com','yayasanbeasiswajakarta@gmail.com','lppd_dkijakarta@yahoo.co.id','pwmadkijakarta@gmail.com','nurulkomar2593@gmail.com','fkubprov.dki.jakarta@gmail.com','pwmdki@gmail.com','lptgdki@gmail.com','yki.dki.jakarta@gmail.com','bamusbetawi1982@gmail.com','skomenwajaya@gmail.com','Medianhasan@yahoo.com','kcvri.45@gmail.com','sekretariat@pmidkijakarta.or.id','ica_rachma2003@yahoo.com','yw.alirsyad.jp@gmail.com','yayasanalhidayahdarussalam1423@gmail.com','bekatigaesdki@gmail.com','dekopindajaktim@ymail.com','ysaijakarta@gmail.com','yayasancitrasarimandiri204@gmail.com','ra.ittihadulmuslimat@gmail.com','andri.muhaimin@ymail.com','arribath.pondokbambu@gmail.com','solawatkita@gmail.com','bebenbentar@yahoo.com','nursahabat.65@yahoo.com','triekasila@gmail.com','wirdalif@gmail.com','yetsmyet@gmail.com','mansyaulhuda03@yahoo.com','vivieyasni@yahoo.com','alhasyimybkn@gmail.com','firmadesraanugrahani@yahoo.co.id','tpqrayhanaalfirdausi@hotmail.com','sekbermpu@yahoo.co.id','yayasanalabror5810@gmail.com','madrasah.arruhaniyah@gmail.com','yyspusaka45@gmail.com','yayasanalgufronjkt@gmail.com','yapendikbangun@yahoo.com','rini@edcorpindo.com','rafasya.cr@gmail.com','yayasanaksimatapena@yahoo.co.id','tariahahmat@gmail.com','yantimirja@gmail.com','bksp.jabodetabekjur@gmail.com','anindyasaripah@gmail.com','dibo.piss@yahoo.com','diharjo.style@gmail','majlistalim.chairunnisa@gmail.com','edyyudiana@gmail.com','mushollahalbarkah@gmail.com','viraadika@gmail.com','nahroh85@gmail.com','yayasan_uswatunhasanah@yahoo.com','olgatania25@gmail.com','ibnuarma2018@yahoo.com','drd_jakarta@yahoo.co.id','nadikuswanto2@gmail.com','septianalisda@gmail.com','rehsos.lupendi@gmail.com','yusoli43@gmail.com','abenklover@gmail.com','anaklansia_dinsos@yahoo.com','awwaliyahrohiim@yahoo.com','seksijamsos2018@gmail.com','syiarislamcengkareng@gmail.com','katarprovdki2015@gmail.com','lbhjakarta@bantuanhukum.or.id','bundanina511@gmail.com','alimanserdang@gmail.com','tazkiyah1@gmail.com','yayasan.alhijrahpc18@gmail.com','ra.al.khair10@gmail.com','jakarta@sos.or.id','junaedialbatawi@gmail.com','masjiduswatunhasanah2018@gmail.com','wasaditj2@gmail.com','zainmugni08@gmail.com','masjidalikhlas2018@yahoo.com','cahayailmucendekia@gmail.com','yys.alwarizu@gmail.com','fkludkijakarta@yahoo.co.id','Adnanyuliani@gmail.com','yadiharyadyadi489@gmail.com','alhidayahmasjid@yahoo.com','bima.prasetyo337@gmail.com','majlisannaimiyah@gmail.com','mala0374@yahoo.com','abdullahzaini70@gmail.com','daniayun78@gmail.com','rustini1126@gmail.com','nuralfi89@gmail.com','opickbabe@gmail.com','dki.pwnu@gmail.com','lp3kd.dki@gmail.com','fikriansori36@gmail.com','dki@bpn.go.id','ahmad.sopandi02@gmail.com','sekretariat@jakarta.ldii.or.id','subhan.ibrahim3@gmail.com','staflogistik051@gmail.com','alihsanmasjid@gmail.com','mushollaalmaghfiroh2018@gmail.com','hajiamarullahasyari@gmail.com','sren.03@gmail.com','diapersmetro@gmail.com','sapnanasroha@gmail.com','umminovilia@gmail.com','almuttaqin2018@yahoo.com','assaadiyah2018@yahoo.com','albariyyah2018@yahoo.com','masjid.annur2018@yahoo.com','almarhamah2018@gmail.com','masjidnurulhidayah@yahoo.com','musholla.annuriyah2018@yahoo.com','atmadjarosidah@gmail.com','kukusumama@gmail.com','rb.ibasnez@gmail.com','mushollaalqosam2018@gmail.com','rofiulalaaa@gmail.com','ajajunaidi690@gmail.com','nabilahfikri.nf@gmail.com','abdulkhoir03@yahoo.com','chotimatul.ulya@yahoo.co.id','protecindo1971@gmail.com','slogartap1@gmail.com','musholla.aliman0702@gmail.com','citramascemerlang@yahoo.com','musholla.daarul.muminin@gmail.com','gkps.jemaatcikoko@gmail.com','majelistaklimikhwan@gmail.com','masjidalamien2018@gmail.com','edisuryadinata@yahoo.co.id','Masjidalmakmurklender@gmail.com','mardanibaiturrohmah@gmail.com','mushollaalmuhajirin5@gmail.com','md.fathulkhoir@gmail.com','loekmanharissuhud@gmail.com','fkppidkijaya@gmail.com','gpsisoteria@yahoo.com','suburandimuchtar@gmail.com','mpwppdki07@gmail.com','rektorat@uic.ac.id','akunmakokoarmabar@gmail.com','mtnhnurulhidayah98@gmail.com']; 
		// exit;

		 Mail::send('emails.kirim-info', [], function ($m) use ($email) {
			        $m->from(Config::get('mail.from.address'), 'eHibahbansosdki'); 
		            $m->to($email)->subject('test');
		        });

	}
}