<?php 
namespace BPKAD\Http\Controllers\Admin\Profile;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\UserGroup;
use BPKAD\User;

class ProfileStoreController extends BaseHomeController
{

	public function manage(Request $request){
		switch ($request->session()->get('group_id')) {
			case '12':
				echo $this->userLembaga($request->session()->get('group_id'), $request->session()->get('userID'));
				break;
		}
	}

	public function userLembaga($group_id, $userID){
		return view('admin.profile.user-lembaga')
				->with('recUserGroup',UserGroup::where('group_id',$group_id)->first())
				->with('recUser', $this->users->getById($userID));
	}


}?>