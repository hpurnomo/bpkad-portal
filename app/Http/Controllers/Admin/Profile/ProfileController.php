<?php 
namespace BPKAD\Http\Controllers\Admin\Profile;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Illuminate\Http\Request;
use BPKAD\Http\Entities\UserGroup;
use BPKAD\Foundation\UserBuilder;

class ProfileController extends BaseHomeController
{
	use UserBuilder;
	/**
	* Show Module Profile User, Filter By Group User.
	*
	* @param Request $request
	*/
	public function manage(Request $request){
		switch ($request->session()->get('group_id')) {
			case '8': //PPKD
				echo $this->userPpkd($request->session()->get('group_id'), $request->session()->get('userID'));
				break;
			case '12': //LEMBAGA
				echo $this->userLembaga($request->session()->get('group_id'), $request->session()->get('userID'));
				break;
			case '10': //SKPD
				echo $this->userSKPD($request->session()->get('group_id'), $request->session()->get('userID'));
				break;
		}
	}

	/**
	* Show Profile User PPKD.
	*
	* @param Integer $group_id, $userID
	*/
	public function userPpkd($group_id, $userID){
		return view('admin.profile.user-ppkd')
				->with('recUserGroup',UserGroup::where('group_id',$group_id)->first())
				->with('recUser', UserBuilder::getById($userID));
	}

	/**
	* Show Profile User Lembaga.
	*
	* @param Integer $group_id, $userID
	*/
	public function userLembaga($group_id, $userID){
		return view('admin.profile.user-lembaga')
				->with('recUserGroup',UserGroup::where('group_id',$group_id)->first())
				->with('recUser', UserBuilder::getById($userID));
	}

	/**
	* Show Profile User SKPD.
	*
	* @param Integer $group_id, $userID
	*/
	public function userSKPD($group_id, $userID){
		return view('admin.profile.user-skpd')
				->with('recUserGroup',UserGroup::where('group_id',$group_id)->first())
				->with('recUser', UserBuilder::getById($userID));
	}


}?>