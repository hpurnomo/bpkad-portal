<?php 
namespace BPKAD\Http\Controllers\Admin\Profile;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 *
 * This file is part of bpkad-portal application.
 *
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Requests\UpdateUserLembagaRequest;
use BPKAD\Http\Requests\UpdateUserLembagaAvatarRequest;
use BPKAD\Http\Requests\UpdateUserLembagaResetPassRequest;
use BPKAD\Http\Requests\UpdateUserLembagaResetEmailRequest;

class ProfileUpdateController extends BaseHomeController
{

	public function updateUserLembaga(UpdateUserLembagaRequest $request){
		if ($request->hasFile('photo')) {
               $this->users->updateUserLembaga($request->all());
		}
		else{
			$this->users->updateUserLembagaWithoutPhoto($request->all());
		}

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');
		
		return redirect(route('profile.user'));
	}

	public function updateUserLembagaAvatar(UpdateUserLembagaAvatarRequest $request){
		if($request->hasFile('avatar')){
			$this->users->updateUserLembagaAvatar($request->all());

			$request->session()->flash('status','success');
			$request->session()->flash('msg','Your data has been save successfully. Thank you.');
		}
		
		return redirect(route('profile.user'));
	}

	public function updateUserLembagaResetPass(UpdateUserLembagaResetPassRequest $request){
		$this->users->updateUserLembagaResetPass($request->all());

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');
		
		return redirect(route('profile.user'));
	}

	public function updateUserLembagaResetEmail(UpdateUserLembagaResetEmailRequest $request){
		$this->users->updateUserLembagaResetEmail($request->all());

		$request->session()->flash('status','success');
		$request->session()->flash('msg','Your data has been save successfully. Thank you.');
		
		return redirect(route('profile.user'));
	}

}?>