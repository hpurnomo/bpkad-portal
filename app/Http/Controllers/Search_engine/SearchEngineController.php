<?php 
namespace BPKAD\Http\Controllers\Search_engine;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/
use Illuminate\Http\Request;
use BPKAD\Http\Entities\Lembaga;

class SearchEngineController extends BaseHomeController
{
	
	public function index(){
		$data = array('periode' => $this->priode->getAll(),
					  'kota' => $this->kota->getAll(),
					  'kecamatan' => $this->kecamatan->getAll(),
					  'kelurahan' => $this->kelurahan->getAll(),
					  'lembaga' => $this->lembaga->getAllWithPaginate(),
					  'map' => Lembaga::whereNotNull('latitude')
					  			->where('latitude','!=',0)
					  			->where('is_verify',1)
					  			->get()
					 );
		
		return view('search_engine.master')->with('roles',$this->roles)
										   ->with('data',$data);
	}

	public function filter(Request $request){

		if(empty($request['nokota']) && empty($request['kelurahan'])){
			
			return redirect(route('search.engine'));

		}else{

			$requestKota = explode("#", $request['nokota']);

			if(!empty($request['nokota']) && empty($request['kelurahan'])){
				
				$kota = $this->kota->getAll();
				$kelurahan = $this->kelurahan->getKdKel($requestKota[1]);

			}elseif(!empty($request['nokota']) && !empty($request['kelurahan'])){
				
				$kota = $this->kota->getAll();
				$kelurahan = $this->kelurahan->getKdKel($requestKota[1]);
			
			}else{
				$kota = $this->kota->getAll();
				$kelurahan = $this->kelurahan->getAll();
			}

			$data = array('periode' => $this->priode->getAll(),
						  'kota' => $kota,
						  'kecamatan' => $this->kecamatan->getAll(),
						  'kelurahan' => $kelurahan,
						  'filter' => $this->getRecByRequest($request),
						  'request' => $request->all(),
						  'map' => Lembaga::whereNotNull('latitude')
									->Where('nokota','like','%'.$requestKota[0].'%')
									->Where('nokel','like','%'.$request['kelurahan'].'%')->get()
						 );

			return view('search_engine.filter')->with('roles',$this->roles)
											   ->with('data',$data);
		}

	}
}


?>