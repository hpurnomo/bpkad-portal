<?php 
namespace BPKAD\Http\Controllers\Search_engine;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BPKAD\Http\Repositories\proposalRepository;
use BPKAD\Http\Repositories\priodeRepository;
use BPKAD\Http\Repositories\lembagaRepository;
use BPKAD\Http\Repositories\kotaRepository;
use BPKAD\Http\Repositories\kecamatanRepository;
use BPKAD\Http\Repositories\kelurahanRepository;
use DB;

class BaseHomeController extends Controller
{
	protected $roles;
	/**
	* @var proposal
	*/
	protected $proposal;
	/**
	* @var priode
	*/
	protected $priode;
	/**
	* @var lembaga
	*/
	protected $lembaga;
	/**
	* @var kota
	*/
	protected $kota;
	/**
	* @var kecamatan
	*/
	protected $kecamatan;
	/**
	* @var kelurahan
	*/
	protected $kelurahan;


	function __construct(Request $request, proposalRepository $proposal, priodeRepository $priode, lembagaRepository $lembaga, kotaRepository $kota, kecamatanRepository $kecamatan, kelurahanRepository $kelurahan){
		
		if ($request->session()->has('roles')) {
		    $this->roles = $request->session()->get('roles');
		}
		else {
			$this->roles = 'guest';
		}

		$this->proposal = $proposal;
		$this->priode = $priode;
		$this->lembaga = $lembaga;
		$this->kota = $kota;
		$this->kecamatan = $kecamatan;
		$this->kelurahan = $kelurahan;
	}

	public function getRecByRequest($request){
		
		$requestKota = explode("#", $request['nokota']);

		$query = DB::table('mst_lembaga AS ml')->leftJoin('foto_lembaga AS fl','ml.nomor','=','fl.no_lembaga')
											   ->select(['ml.*','fl.file_name'])
											   ->where('ml.nokota','like','%'.$requestKota[0].'%')
											   ->where('ml.nokel','like','%'.$request['kelurahan'].'%')
											   ->get();
		return $query;
	}

	// public function getRecByRequest($request){
	// 	//split string
	// 	$anggaran = explode('-', $request['anggaran']);	
	
	// 	$query = DB::table('trx_proposal AS tp')
	// 						->select(['ml.nm_lembaga','ml.alamat','tp.no_lembaga','fl.file_name AS foto_lembaga'])
	// 						->leftJoin('mst_lembaga AS ml','tp.no_lembaga','=','ml.nomor')
	// 						->leftJoin('foto_lembaga AS fl','tp.no_lembaga','=','fl.no_lembaga')
	// 						->where('tp.status_prop',1)
	// 						->where('tp.noper','like','%'.$request['priode'].'%')
	// 						->Where('ml.nokel','like','%'.$request['kelurahan'].'%')
	// 						->where('ml.nm_lembaga','like','%'.$request['nm_lembaga'].'%')
	// 						->where('tp.jdl_prop','like','%'.$request['jdl_prop'].'%')
	// 						->whereBetween('tp.nominal_pengajuan',[$anggaran[0],$anggaran[1]])
	// 						->paginate(12);
								
	// 	$query->setPath('page/filter-pencarian/');
	// 	return $query;
	// 	// return $request['priode'];
	// }
}


?>