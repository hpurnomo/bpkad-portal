<?php 
namespace BPKAD\Http\Controllers\Beranda;
  
use Illuminate\Http\Request; 
use Carbon\Carbon;
use BPKAD\Http\Entities\Periode;
use BPKAD\Http\Entities\Cetak_rekomendasi_kolektif; 
use BPKAD\Http\Entities\Cetak_rekomendasi_kolektif_detail; 
use BPKAD\Http\Entities\Skpd; 
use PDF;
use DB;
use DNS2D;   
use Crypt;  
use BPKAD\Http\Entities\RunningText;
use BPKAD\Http\Entities\InfoTerkini;
use BPKAD\Http\Entities\Testimonial;
use BPKAD\Http\Entities\FaQ;

class DocumentController extends BaseHomeController
{
	public function showRekomendasiKolektif($uuid){ 
		//$uuid = Crypt::decrypt($uuid);  
		$rekomendasi = Cetak_rekomendasi_kolektif::where('uuid',$uuid)->count(); 
		if($rekomendasi > 0){
			$data['uuid'] = $uuid;
			return view('pdf.rekomendasi-kolektif-check',$data); 
		}else{    
			return 'file not found'; 
		}
		
	}

	public function showRekomendasiKolektifPdf($uuid){
		$rekomendasi = Cetak_rekomendasi_kolektif::where('uuid',$uuid)->first(); 
		$data['dataCetakkomendasiPencairan'] = $rekomendasi; 
		$user = DB::table('users')->select('no_skpd')->where('user_id',$rekomendasi->created_by)->first(); 
		$data['skpd'] 	= Skpd::where('nomor',$user->no_skpd)->first();  
        $data['periode']= Periode::where("noper",$rekomendasi->noper)->first(); 
        $lembaga = '';
        $detail = DB::table("cetak_rekomendasi_kolektif_detail")->where("id_kol_head",$rekomendasi->uuid)->get(); 
     	 $textQrCode = ''.url('qrcode').'/'.$rekomendasi->uuid.'';
        $data['qrcode'] = DNS2D::getBarcodeHTML($textQrCode, "QRCODE",3,3);   
        $footer = 'Rekomendasi Pengusulan '.$rekomendasi->jenisBantuan.' Dalam Bentuk Uang pada '.$data['periode']->keterangan.'';
         $footerHTML = '<!DOCTYPE html>
						<html>
						<head>
						</head>
						<body style="font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
						<table border="0" cellpadding="0" cellspacing="0"   width="100%">
							<tr style="font-size:10px;text-align:center">
								<center>'. $footer .'</center>
							</tr>
						</table>
						</body>
						</html>';
		$pdf = PDF::loadView('pdf.rekomendasi-kolektif',$data)
				->setOption('page-width', '210')
				->setOption('page-height', '330')
				->setOption('footer-html',$footerHTML);

		return $pdf->inline();	
	}

	 

}
?>