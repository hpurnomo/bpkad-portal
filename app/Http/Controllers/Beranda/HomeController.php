<?php 
namespace BPKAD\Http\Controllers\Beranda;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use Mail;
use Illuminate\Http\Request;
use BPKAD\Http\Entities\RunningText;
use BPKAD\Http\Entities\InfoTerkini;
use BPKAD\Http\Entities\Testimonial;
use BPKAD\Http\Entities\FaQ;
use Carbon\Carbon;
use DB;

class HomeController extends BaseHomeController
{
	public function index(Request $request){
		$data = array('penerimaTahunIni' => $this->proposal->penerimaTahunIni(),
					  'penerimaTahunDepan' => $this->proposal->penerimaTahunDepan(),
					  'penerimaDuaTahunDepan' => $this->proposal->penerimaDuaTahunDepan(),
					  'result' => NULL
					 );
		return view('beranda.master')->with('roles',$this->roles)
									 ->with('data',$data)
									 ->with('recRunningTextByID',RunningText::where('status',1)->get())
									 ->with('recInfoTerkiniByID',InfoTerkini::where('status',1)->orderBy('created_at','desc')->get())
									 ->with('recTestimonial',Testimonial::where('status',1)->orderBy('created_at','desc')->get())
									 ->with('recFaQ',FaQ::where('status',1)->orderBy('no_urut','asc')->get());
	}

	public function admin(){
		return view('beranda.admin');
	}

	public function showPenerima($tahun){
				
		$rekap = DB::table('mst_periode AS mp')->select(['mp.tahun','mp.keterangan AS priodeKeterangan',
											  	'tp.no_prop','tp.status_kirim','tp.status_prop','tp.jdl_prop','tp.nominal_pengajuan',
											  	'ml.kd_lembaga','ml.nm_lembaga','ml.kd_skpd',
											  	'ms.nm_skpd'])
				->leftJoin('trx_proposal AS tp','mp.noper','=','tp.noper')
			  	->leftJoin('mst_lembaga AS ml','tp.no_lembaga','=','ml.nomor')
			  	->leftJoin('mst_skpd AS ms','ml.kd_skpd','=','ms.kd_skpd')
			  	->where('mp.tahun','=',$tahun)
			  	->get();

		return view('beranda.show-penerima')->with('roles',$this->roles)
									 		->with('tahun',$tahun)
									 		->with('rekap',$rekap);
	}

	public function kirimPesan(Request $request){

		// send email
		$data = ['name' => $request->name,
				 'email' => $request->email,
				 'phone' => $request->phone,
				 'subject' => $request->subject,
				 'category' => $request->category,
				 'message' => $request->message
				];

	    Mail::send('emails.kirim-pesan', ['data' => $data], function ($m) use ($data) {
	            $m->from($data['email'], $data['name']);

	            $m->to('hibahbansosdki@gmail.com', 'Operator Hibah Bansos DKI Jakarta')->subject($data['subject']." kategori:: ".$data['category']);
	        });

		// $data = array('penerimaTahunIni' => $this->proposal->penerimaTahunIni(),
		// 			  'penerimaTahunDepan' => $this->proposal->penerimaTahunDepan(),
		// 			  'penerimaDuaTahunDepan' => $this->proposal->penerimaDuaTahunDepan(),
		// 			  'result' => 'success' );
		
		// return view('beranda.master')->with('roles',$this->roles)
		// 							 ->with('data',$data);
		$data = array('penerimaTahunIni' => $this->proposal->penerimaTahunIni(),
					  'penerimaTahunDepan' => $this->proposal->penerimaTahunDepan(),
					  'penerimaDuaTahunDepan' => $this->proposal->penerimaDuaTahunDepan(),
					  'result' => 'success'
					 );
		return view('beranda.master')->with('roles',$this->roles)
									 ->with('data',$data)
									 ->with('recRunningTextByID',RunningText::where('status',1)->get())
									 ->with('recInfoTerkiniByID',InfoTerkini::where('status',1)->orderBy('created_at','desc')->get());
	}

	public function kategoriPenerima(){
		return view('beranda.kategori-penerima');
	}

}
?>