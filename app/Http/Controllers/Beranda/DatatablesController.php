<?php 
namespace BPKAD\Http\Controllers\Beranda;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Controllers\Controller;
use Datatables;
use DB;

class DatatablesController extends Controller
{
	public function getTrxProposal($tahun){
			DB::statement('SET @num:=0;');
			$row = DB::table('mst_periode AS mp')->leftJoin('trx_proposal AS tp','mp.noper','=','tp.noper')
											  ->leftJoin('mst_lembaga AS ml','tp.no_lembaga','=','ml.nomor')
											  ->leftJoin('mst_skpd AS ms','ml.kd_skpd','=','ms.kd_skpd')
											  ->where('mp.tahun','=',$tahun)
											  ->select(['mp.tahun','tp.no_prop','tp.status_input','tp.jdl_prop','tp.nominal_pengajuan','ml.kd_lembaga','ml.nm_lembaga','ml.kd_skpd','ms.nm_skpd']);

			
			return Datatables::of($row)
					// ->editColumn('jumlah_bantuan',"{!! 'Rp. ' . number_format( 50000000, 0 , '' , '.' ) !!}")
					->addColumn('nominal_pengajuan',function($row){
						$nominal_pengajuan = 'Rp.'.number_format( $row->nominal_pengajuan, 0 , '' , '.' );
						return $nominal_pengajuan;
					})
					->make(true);
	}
}

?>