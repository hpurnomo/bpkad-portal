<?php

namespace BPKAD\Http\Requests;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Requests\Request;

class UpdateUserLembagaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'no_registrasi' => 'required',
            'real_name' => 'required',
            'email' => 'required',
            'ktp' => 'required',
            'telp' => 'required',
            'photo' => 'mimes:jpeg,png|max:1000',
            'address' => 'required'
        ];
    }

}
