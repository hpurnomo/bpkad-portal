<?php

namespace BPKAD\Http\Requests;

/*******************************************************
 * Created by Hary Purnomo.
 * Email: hary.purnomo87@gmail.com
 * Date: 11/5/15
 * Time: 07:00 PM
 * 
 * This file is part of bpkad-portal application.
 * 
 * The source code can not be copied and/or distributed without the express
 * permission of Hary Purnomo.
 *******************************************************/

use BPKAD\Http\Requests\Request;

class RegUserSkpdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_skpd' => 'required',
            'nrk' => 'required|unique:users,nrk',
            'real_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'ktp' => 'required|unique:users,ktp',
            'telp' => 'required',
            'photo' => 'mimes:jpeg,png|max:1000',
            'g-recaptcha-response' => 'required|captcha'
        ];
    }

    public function messages(){
        return [
            'unique' => ':attribute yang Anda masukan sudah tersedia',
            'required' => 'field :attribute harus diisi'
        ];
    }
}
